<?php
/**
 * Template Name: Blog Index Page
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

get_header(); ?>
<div class="container align-center" id="primary">
    <h1 class="gt-title">
        <?php echo single_post_title(); ?>
    </h1>
    <?php echo gt_get_field('mag_text', get_queried_object_id()); ?>
    <?php
    if (have_posts()) {
        ?> <div class="row"> <?php
        while (have_posts()) : the_post();
            ?>
            <div class="blog-post col-md-6">
                <div class="post-visual">
                    <div class="img-wrapper">
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'blogpost-gallery'); ?>" alt="<?php echo the_title(); ?>" title="<?php echo the_title(); ?>" />
                    </div>
                </div>
                <div class="post-textual">
                    <a class="title" href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>
                    <?php the_excerpt(); ?>
                    <a class="button" href="<?php the_permalink(); ?>">המשך קריאה</a>
                </div>
            </div><!-- /.blog-post -->
        <?php
        endwhile;
        ?> </div> <?php
    }
    ?>
</div>
<?php get_footer(); ?>
