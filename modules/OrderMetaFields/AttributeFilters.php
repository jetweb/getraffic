<?php

namespace Getraffic;

class AttributeFilters extends PluginBase implements ConfigurableModule
{
    public function __construct()
    {
        add_action( 'restrict_manage_posts', function ( $post_type ) {
            // Check this is the products screen
            if ($post_type == 'product') {
                foreach (wc_get_attribute_taxonomies() as $tax) {
                    $attr = wc_get_attribute($tax->attribute_id);

                    echo '<select name="'. $attr->slug .'">';
                    $terms = get_terms( array(
                        'taxonomy' => $attr->slug,
                        'hide_empty' => false,
                    ));
                    echo '<option value>סנן לפי ' . $tax->attribute_label .'</option>';

                    foreach ($terms as $term) {
                        $selected = '';
                        if ($term->term_id == $_GET[$attr->slug]) {
                            $selected = 'selected="selected"';
                        }
                        echo '<option value="'. $term->term_id .'" '. $selected .'>'. $term->name .'</option>';
                    }
                    var_dump($terms);
                    // Add your filter input here. Make sure the input name matches the $_GET value you are checking above.
                    echo '</select>';
                }
            }
        });
        add_action('pre_get_posts', function ($query) {
            global $typenow, $pagenow;
            if ( 'edit.php' !== $pagenow || 'product' !== $typenow) {
                return $query;
            }
            $taxquery = [];
            $taxquery['relation'] = 'AND';
            foreach (wc_get_attribute_taxonomies() as $tax) {
                $attr = wc_get_attribute($tax->attribute_id);
                if (isset($_GET[$attr->slug]) && !empty($_GET[$attr->slug])) {
                    $taxquery[] = [
                        'taxonomy' => $attr->slug,
                        'field'    => 'term_id',
                        'terms'    => $_GET[$attr->slug],
                        'operator' => 'IN',
                    ];
                }
            }
            $query->set('tax_query', $taxquery);
        });
    }

    public static function getSettingsPage()
    {
    }

    function getTextDomain()
    {
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        return [];
    }
}
