<?php
namespace Getraffic;

class OrderMetaFields extends PluginBase implements ConfigurableModule
{
    public function __construct()
    {
        $this->showFields();
        $this->showFieldsValues();
        $this->showColumns();
        $this->showColumnValues();
        $this->saveCustomCheckout();
        $this->saveDiscountInSession();
    }

    private function saveCustomCheckout()
    {
        add_action('woocommerce_checkout_update_order_meta',
            function ($order_id) {
                $cart_rule_uid = WC()->session->get('cart_rule_uid') ? WC()->session->get('cart_rule_uid') : '';
                if (!empty($cart_rule_uid)) {
                    update_post_meta($order_id, 'cart_rule_uid', $cart_rule_uid);
                    $rp_wcdpd_settings = get_option('rp_wcdpd_settings');
                    foreach ($rp_wcdpd_settings as $rp) {
                        foreach ($rp['product_pricing'] as $r) {
                            if ($r['uid'] == $cart_rule_uid) {
                                update_post_meta($order_id, 'cart_rule_name', $r['note']);
                            }
                        }
                    }
                }
            });
    }

    private function saveDiscountInSession()
    {
        add_action('rp_wcdpd_product_pricing_rule_applied_to_cart', function ($rule_uid, $data) {
            $rules = [$rule_uid];
            if (!empty($rule_uid)) {
                $rules[] = $rule_uid;
            }
            WC()->session->set('cart_rule_uid', $rule_uid);

            return ($rules);
        }
            , 10, 2);
    }

    private function showColumns()
    {
        add_filter('manage_edit-shop_order_columns', function ($columns) {
            $new_array               = [];
            $columns['total_orders'] = 'סה"כ הזמנות ללקוח';
            $columns['pushed_to_gtm'] = 'נספר ב-GTM';
            $columns['pushed_to_ga'] = 'נספר ב-GA';
            foreach ($columns as $key => $title) {
                if ($key == 'billing_address') {
                    $new_array['order_items'] = __('נקנה', 'woocommerce');
                }
                $new_array[$key] = $title;
            }

            return $new_array;
        });
    }

    private function showColumnValues()
    {
        add_action('manage_shop_order_posts_custom_column', function ($column) {
            global $post, $the_order;
            switch ($column) {
                case 'order_number':
                    $order         = wc_get_order($post->ID);
                    $cart_rule_uid = get_post_meta($order->get_id(), 'cart_rule_uid', true);
                    if (!empty($cart_rule_uid)) {
                        $cart_rule_name = get_post_meta($order->get_id(), 'cart_rule_name', true);
                        echo '<p><span>במסגרת מבצע: ' . $cart_rule_name . '</span></p>';
                    }
                    break;
                case 'pushed_to_gtm':
                    $order    = wc_get_order( $post->ID );
                    $pushed_to_gtm = get_post_meta($order->get_id(), 'pushed_to_gtm', true);
                    echo $pushed_to_gtm == 1 ? "כן" : "לא";
                    break;
                case 'pushed_to_ga':
                    $order    = wc_get_order( $post->ID );
                    $pushed_to_ga = get_post_meta($order->get_id(), '_ga_tracked', true);
                    echo $pushed_to_ga == 1 ? "כן" : "לא";
                    break;
                case 'total_orders':
                    $order = wc_get_order($post->ID);
                    echo wc_get_customer_order_count($order->get_customer_id());

        $order    = wc_get_order( $post->ID );
		if (function_exists ( 'wc_seq_order_number_pro' )){
			$order_id = $post->ID;
		} else {
			$order_id = $order->get_order_number();
		}
		//print_r($order_id);
		$tapuz_label_nonce = wp_create_nonce( "tapuz_create_label" );
		$tapuz_label_query = 'post.php?tapuz_pdf=create&tapuz_label_wpnonce='.$tapuz_label_nonce.'&order_id='.$order_id;
?>
				<div class="tapuz-wrapper" id="tapuz-order-id-<?php echo$order_id?>">
					<div id="tapuz_open_ship">
					<div id="tapuz_checkbox">
						<input id="tapuz_urgent" type="checkbox" name="tapuz_urgent" value="urgent"><?php _e( 'Urgent', 'tapuz-delivery' ); ?>
						<input id="tapuz_return" type="checkbox" name="tapuz_return" value="return"><?php _e( 'Return', 'tapuz-delivery' ); ?><br>
					</div>
					<span><?php _e( 'Collect amount: ', 'tapuz-delivery' ); ?></span><input id="tapuz_collect" type="text" name="tapuz_collect" value="<?php _e( 'NO', 'tapuz-delivery' ); ?>"><br>
					                    <br>
                    <input type="radio" class="tapuz_delivery_type" name="tapuz_delivery_type" value="1"
                           checked="checked"><?php _e('Regular delivery', 'woo-tapuz-delivery'); ?>
                    <br>
                    <input type="radio" class="tapuz_delivery_type" name="tapuz_delivery_type"
                           value="2"><?php _e('Collecting', 'woo-tapuz-delivery'); ?>
<br/>
					<span><?php _e( 'Delivery type:', 'tapuz-delivery' ); ?></span>
					<select id="tapuz_motor">
						<option value="1"><?php _e( 'Scooter', 'tapuz-delivery' ); ?></option>
						<option value="2"><?php _e( 'Car', 'tapuz-delivery' ); ?></option>
					</select><br>
					<span><?php _e( 'Packages:', 'tapuz-delivery' ); ?></span><input id="tapuz_packages" type="number" name="packages" value="1"><br>
					<span><?php _e( 'Deliver on:', 'tapuz-delivery' ); ?></span><input id="tapuz_exaction_date" type="date" name="date" value="<?php echo date('Y-m-d') ?>"><br>
					<div class="tapuz-button-container">
						<button style="background:#ff8e15;" class="tapuz-button tapuz-open-button-<?php echo$order_id?>" data-order="<?php echo $order_id ?>">
							<?php _e( 'Open shipment', 'tapuz-delivery' ); ?></button>
					</div>
				</div>
				<div class="tapuz-success-ship">
					<p><?php _e( 'Shipment number:', 'tapuz-delivery' ); ?><span class="tapuz-success-ship-number"></span></p>
					<div class="tapuz-button-container">
						<a class="tapuz-button tapuz-print-button" target="_blank" data-order="<?php echo $order_id ?>"
						   href="<?php echo $tapuz_label_query ?>"><?php _e( 'Print label', 'tapuz-delivery' ); ?></a>
					</div>
				</div>
			</div>
<script type='text/javascript' src='https://code.jquery.com/ui/1.12.1/jquery-ui.min.js'></script>
<link rel='stylesheet' href='https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' type='text/css' media='all' />

<script>
(function ($) {
		$(document).on("click", ".tapuz-open-button-<?php echo$order_id?>", function(event){
            event.preventDefault();
            $("#tapuz-order-id-<?php echo$order_id?> .tapuz-open-button").hide();
            $("#tapuz-order-id-<?php echo$order_id?> .tapuz-powered-by").hide();
            $("#tapuz-order-id-<?php echo$order_id?> #tapuz_open_ship").append('<img class="tapuz_loader" src="'+tapuz_delivery.tapuz_ajax_loader+'">');
            var tapuz_order_id = $("button.tapuz-open-button-<?php echo$order_id?>").attr("data-order");
            var tapuz_urgent = "1";
            if ($('#tapuz-order-id-<?php echo$order_id?> #tapuz_urgent').prop('checked')){
                tapuz_urgent = "2"
            }
            var tapuz_return = "1";
            if($('#tapuz-order-id-<?php echo$order_id?> #tapuz_return').prop('checked') == "checked"){
                tapuz_return = "2"
            }
            var tapuz_delivery_type = "1";
            if($('#tapuz-order-id-<?php echo$order_id?> .tapuz_delivery_type').prop('checked') == "checked"){
                tapuz_delivery_type = $('#tapuz-order-id-<?php echo$order_id?> .tapuz_delivery_type').val();
            }
            var tapuz_collect = $('#tapuz-order-id-<?php echo$order_id?> #tapuz_collect').val();
            if (isNaN(tapuz_collect)) {
                tapuz_collect = 'NO';
            }
            var tapuz_motor = $('#tapuz-order-id-<?php echo$order_id?> #tapuz_motor').val();
            var tapuz_packages = $('#tapuz-order-id-<?php echo$order_id?> #tapuz_packages').val();
            var tapuz_exaction_date = $('#tapuz-order-id-<?php echo$order_id?> #tapuz_exaction_date').val();
            $.ajax({
                url: tapuz_delivery.ajax_url,
                type: 'POST',
                data: {
                    action : 'tapuz_open_new_order',
                    tapuz_order_id : tapuz_order_id,
                    tapuz_urgent: tapuz_urgent,
                    tapuz_return: tapuz_return,
                    tapuz_collect: tapuz_collect,
                    tapuz_delivey_type: tapuz_delivery_type,
                    tapuz_motor: tapuz_motor,
                    tapuz_packages: tapuz_packages,
                    tapuz_exaction_date: tapuz_exaction_date,
                    tapuz_wpnonce: tapuz_delivery.tapuz_ajax_nonce
                },
                success: function( response ) {
                    $("#tapuz-order-id-<?php echo$order_id?> .tapuz_loader").hide();
                    $("#tapuz-order-id-<?php echo$order_id?> .tapuz-powered-by").show();
                    $("#tapuz-order-id-<?php echo$order_id?> .tapuz-open-button").hide();
                    if (response == '-100') {
                        $("#tapuz-order-id-<?php echo$order_id?> #tapuz_open_ship").append('<p class="tapuz_error_message">'+tapuz_delivery.tapuz_err_message_open_code+'</p>');
                    } else if (response == '-999') {
                        $("#tapuz-order-id-<?php echo$order_id?> #tapuz_open_ship").append('<p class="tapuz_error_message">'+tapuz_delivery.tapuz_err_message+'</p>');
                    } else {
                        $("#tapuz-order-id-<?php echo$order_id?> #tapuz_open_ship").hide();
                        $("#tapuz-order-id-<?php echo$order_id?> .tapuz-success-ship").show();
                        $("#tapuz-order-id-<?php echo$order_id?> .tapuz-success-ship-number").html(response);
                    }
                },
                error: function() {
                    $("#tapuz-order-id-<?php echo$order_id?> .tapuz_loader").hide();
                    $("#tapuz-order-id-<?php echo$order_id?> .tapuz-powered-by").show();
                    $("#tapuz-order-id-<?php echo$order_id?> #tapuz_open_ship").append('<p>'+tapuz_delivery.tapuz_err_message+'</p>');
                }
            })
        });

  // initalise the dialog
  jQuery('#tapuz-order-id-<?php echo$order_id?>').dialog({
    title: 'פתח משלוח',
    dialogClass: 'wp-dialog',
    autoOpen: false,
    draggable: false,
    width: 'auto',
    modal: true,
    resizable: false,
    closeOnEscape: true,
    position: {
      my: "center",
      at: "center",
      of: window
    },
    open: function () {
      // close dialog by clicking the overlay behind it
      $('.ui-widget-overlay').bind('click', function(){
        $('#tapuz-order-id-<?php echo$order_id?>').dialog('close');
      })
    },
    create: function () {
      // style fix for WordPress admin
      $('.ui-dialog-titlebar-close').addClass('ui-button');
    },
	close : function(){
            window.location.reload()
	}
});
  // bind a button or a link to open the dialog
  $('.tapuz-open-button-modal-<?php echo $order_id?>').click(function(e) {
    e.preventDefault();
    $('#tapuz-order-id-<?php echo $order_id?>').dialog('open').css('max-width','350px').css('margin','0 auto');
  });
})(jQuery);

</script>
<style>
.ui-widget.ui-widget-content {max-width:350px;margin:0 auto;}
</style>
<?php

                    break;
                case 'order_items':
                    echo '<a href="#" class="show_order_items">' . apply_filters('woocommerce_admin_order_item_count',
                            sprintf(_n('%d item', '%d items', $the_order->get_item_count(), 'woocommerce'), $the_order->get_item_count()), $the_order) . '</a>';
                    if (sizeof($the_order->get_items()) > 0) {
                        echo '<table class="order_items" cellspacing="0">';
                        foreach ($the_order->get_items() as $item) {
                            $product = apply_filters('woocommerce_order_item_product', $item->get_product(), $item);
                            ?>
                            <tr class="<?php echo apply_filters('woocommerce_admin_order_item_class', '', $item, $the_order); ?>">
                                <td class="qty"><?php echo esc_html($item->get_quantity()); ?></td>
                                <td class="name">
                                    <?php if ($product) : ?>
                                        <?php echo (wc_product_sku_enabled() && $product->get_sku()) ? $product->get_sku() . ' - ' : ''; ?><a
                                        href="<?php echo get_edit_post_link($product->get_id()); ?>"><?php echo apply_filters('woocommerce_order_item_name', $item->get_name(),
                                            $item, false); ?></a>
                                    <?php else : ?>
                                        <?php echo apply_filters('woocommerce_order_item_name', $item->get_name(), $item, false); ?>
                                    <?php endif; ?>
                                    <?php if (!empty($item_meta_html)) : ?>
                                        <?php echo wc_help_tip($item_meta_html); ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                            <?php
                        }
                        echo '</table>';
                    } else {
                        echo '&ndash;';
                    }
                    break;
            }
        }, 10, 2);
    }

    private function showFieldsValues()
    {
        add_action('woocommerce_admin_order_item_values', function ($_product, $item, $item_id = null) {
            if (is_a($_product, 'WC_Product')) {
                $value = get_post_meta($_product->get_id(), 'warehouse_location', 1);
                echo '<td>' . $value . '</td>';
                if ($_product->is_type('simple')) {
                    $value = get_post_meta($_product->get_id(), '_regular_price', 1);
                } elseif ($_product->is_type('variation')) {
                    try {
                        $value = get_post_meta($_product->get_id(), '_regular_price', 1);
                    } catch (Exception $e) {
                    }
                } else {
                    $value = '';
                }
                if ($value) {
                    echo '<td><span class="woocommerce-Price-currencySymbol">₪</span>' . $value . '</td>';
                } else {
                    echo '<td></td>';
                };
            }
        }, 10, 3);
    }

    private function showFields()
    {
        add_filter('woocommerce_admin_order_item_headers', function () {
            // set the column name
            $column_name = 'מקום במחסן';
            // display the column name
            echo '<th class="item" style="min-width:200px;">' . $column_name . '</th>';
            $column_name = 'מחיר מקורי';
            // display the column name
            echo '<th class="item" style="min-width:200px;">' . $column_name . '</th>';
        }, 10, 2);
    }

    public static function getSettingsPage()
    {
    }

    function getTextDomain()
    {
        return '';
    }

    function getCustomPostTypes()
    {
        return [];
    }

}
