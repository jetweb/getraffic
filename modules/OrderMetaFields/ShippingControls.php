<?php

namespace Getraffic;

use WP_Query;

class ShippingControls extends PluginBase implements ConfigurableModule
{
    public function __construct()
    {
        add_action('restrict_manage_posts', [$this, 'display_shipping_dropdown']);
        add_filter('posts_where', [$this, 'admin_shipping_filter'], 10, 2);
        add_filter('posts_clauses', [$this, 'order_by_shipping_methods'], 10, 2);
        add_action('manage_shop_order_posts_custom_column', [$this, 'shop_order_columns_shipping_content'], 30);
        add_filter('manage_edit-shop_order_sortable_columns', function ($columns) {
            $columns['pickup_locations'] = '_pickup_locations';

            return $columns;
        });
        add_action('pre_get_posts', function (WP_Query $query) {
            global $wpdb;
            if (!is_admin() || !$query->is_main_query()) {
                return $query;
            }
            if ('_pickup_locations' === $query->get('orderby')) {
                $query->query_orderby = 'ORDER BY (
                                SELECT order_item_name
                                FROM ' . $wpdb->prefix . 'woocommerce_order_items
                                WHERE order_item_type = "shipping"
                                AND order_id = id) shipping_type';
            }
        });
        add_filter('manage_edit-shop_order_columns', function ($columns) {
            $columns['pickup_locations'] = "שיטת משלוח";

            return $columns;
        });
        add_filter('wcdn_order_info_fields', [$this, 'shlomi_wcdn_custom_order_fields'], 10, 2);
        add_action('wcdn_head', [$this, 'shlomi_delivery_note'], 20);
        add_action('wcdn_order_item_before', [$this, 'shlomi_wcdn_product_custom']);
        add_action('manage_shop_order_posts_custom_column', [$this, 'gs_add_order_travel_date_content'], 5);
    }

    function gs_add_order_travel_date_content($column)
    {
        global $post;
        if ('pickup_locations' === $column) {
            $order = wc_get_order($post->ID);
            foreach ($order->get_shipping_methods() as $shipping_item) {
                if ('local_pickup_plus' == $shipping_item['method_id'] && isset($shipping_item['pickup_location'])) {
                    echo "<u>איסוף עצמי מחנות:</u><br>";
                }
            }
        }
    }

    function shlomi_delivery_note()
    { ?>
        <style>
            .delivery-note tfoot tr:nth-child(2) > td.total-price > span, .delivery-note dd.variation-, .delivery-note dd.variation-PickupLocation {
                font-weight: bold;
            }

            .wcdn_big {
                font-weight: bold;
                font-size: large;
            }

            tbody .product-quantity > span {
                font-weight: bold;
                font-size: 16px;
            }
        </style>
    <?php }

    function shlomi_wcdn_product_custom($product)
    {
        add_filter('woocommerce_order_items_meta_display', function ($output) {
            $output = preg_replace("/([.\n]*)([.\r\n]*)<dt class=\"variation-\">([.\n]*)([.\r\n]*)/s", "", $output);
            $output = preg_replace("/סניף לאיסוף עצמי:<\/dt>/u", "", $output);
            $output = preg_replace("/([.\n]*)([.\r\n]*)<dd class=\"variation-\">([\n<>\/ ,\p{L}0-9\!]*)([.\n]*)([.\r\n]*)/us", "", $output);

            return $output;
        });
    }

    function shlomi_wcdn_custom_order_fields($fields, $order)
    {
        $new_fields = [];
        foreach ($order->get_shipping_methods() as $shipping_item) {
            if ('local_pickup_plus' == $shipping_item['method_id'] && isset($shipping_item['pickup_location'])) {
                $location           = maybe_unserialize($shipping_item['pickup_location']);
                $pickup_locations[] = $location;
            }
        }
        if (!empty($pickup_locations)) {
            foreach ($pickup_locations as $pickup_location) {
                $formatted_pickup_location = WC()->countries->get_formatted_address(array_merge(['first_name' => null, 'last_name' => null, 'state' => null], $pickup_location));
                if (isset($pickup_location['phone']) && $pickup_location['phone']) {
                    $formatted_pickup_location .= "<br/>\n" . $pickup_location['phone'];
                }
                $my_pickup_html = esc_html(preg_replace('#<br\s*/?>#i', ', ', $formatted_pickup_location));
            }
            $new_fields['pickup_location'] = [
                'label' => 'חנות לאיסוף',
                'value' => '<span class="wcdn_big">' . $my_pickup_html . '</span>',
            ];
        } else {
            $my_shipping_ar = $order->get_shipping_methods();
            reset($my_shipping_ar);
            $first_key                     = key($my_shipping_ar);
            $my_shipping_id                = $my_shipping_ar[$first_key]['name'];
            $new_fields['pickup_location'] = [
                'label' => 'משלוח',
                'value' => '<span class="wcdn_big">' . $my_shipping_id . '</span>',
            ];
        }
        $my_coupons = $order->get_used_coupons();
        $coupon_txt = "";
        if (is_array($my_coupons) && !empty($my_coupons)) {
            foreach ($my_coupons as $coupon_name) {
                $coupon_txt .= "$coupon_name, ";
            }
            $coupon_txt            = trim($coupon_txt, " ,\t\n\r\0\x0B");
            $new_fields['coupons'] = [
                'label' => 'קופונים בשימוש',
                'value' => $coupon_txt,
            ];
        }

        return array_merge($fields, $new_fields);
    }

    public function display_shipping_dropdown()
    {
        global $wpdb;
        if (is_admin() && !empty($_GET['post_type']) && $_GET['post_type'] == 'shop_order') {
            $exp_types = [];
            $sql = 'SELECT DISTINCT(order_item_name)
                FROM ' . $wpdb->prefix . 'woocommerce_order_items
                WHERE order_item_type = "shipping"';
            $results = $wpdb->get_results($sql);
            foreach ($results as $r) {
                if (!empty($r->order_item_name)) {
                    $exp_types[] = [
                        'raw'     => $r->order_item_name,
                        'encoded' => str_replace('\'', '', str_replace('"', '', $r->order_item_name)),
                    ];
                }
            }
            ?>
            <select name="shipping_method">
                <option value=""><?php _e('סינון לפי שיטת משלוח', 'woocommerce'); ?></option>
                <?php
                $current_v = isset($_GET['shipping_method']) ? $_GET['shipping_method'] : '';
                foreach ($exp_types as $type) {
                    printf
                    (
                        '<option value="%s"%s>%s</option>',
                        $type['encoded'],
                        $type['encoded'] == $current_v ? ' selected="selected"' : '',
                        $type['raw']
                    );
                }
                ?>
            </select>
            <?php
        }
    }

    public function order_by_shipping_methods( $args, $query ) {

        $orderby = $query->get( 'orderby');
        if ($orderby === '_pickup_locations') {
            global $wpdb;
            $args['join']    .= " LEFT JOIN {$wpdb->prefix}woocommerce_order_items ON order_id = ID AND order_item_type = 'shipping'";
            $args['orderby'] = ' order_item_name ' . $query->query_vars['order'];
        }
        return $args;
    }

    public function admin_shipping_filter($where, $wp_query)
    {
        global $pagenow;
        global $wpdb;
        if (is_admin() && $pagenow == 'edit.php' && isset($_GET['shipping_method'])) {
            $method = $_GET['shipping_method'];
            if (!empty($method)) {
                $where .= $GLOBALS['wpdb']->prepare('AND ID
                            IN (
                                SELECT order_id
                                FROM ' . $wpdb->prefix . 'woocommerce_order_items
                                WHERE order_item_type = "shipping"
                                AND order_item_name = "%s"
                            )', $method);
            }
        }

        return $where;
    }

    public function shop_order_columns_shipping_content($column)
    {
        global $post;
        if ('pickup_locations' === $column) {
            $order            = wc_get_order($post->ID);
            $pickup_locations = [];
           // $test             = print_r($order->get_shipping_methods(), true);
            if (count($order->get_shipping_methods()) < 1) {
                echo "<u><b>לא נבחרה שיטת משלוח</b></u><br>";
            }
            foreach ($order->get_shipping_methods() as $shipping_item) {
                if ('local_pickup_plus' == $shipping_item['method_id'] && isset($shipping_item['pickup_location'])) {
                    $location           = maybe_unserialize($shipping_item['pickup_location']);
                    $pickup_locations[] = $location;
                } else {
                    $shipping_name = ($shipping_item['name'] != "") ? $shipping_item['name'] : $shipping_item['method_id'];
                    if (count($order->get_shipping_methods()) > 1) {
                        $shipping_name = "<br><b>נבחרה יותר משיטת משלוח אחת</b>";
                    } elseif ($shipping_name == "") {
                        $shipping_name = "<b>תקלה בשיטת משלוח</b>";
                    }
                    echo "<u>$shipping_name</u><br>";
                    if ('boxit_shipping:3' == $shipping_item['method_id']) {
                        echo get_post_meta($post->ID, 'meta_name_of_point', true) . '<br>';
                        echo get_post_meta($post->ID, 'meta_address_of_point', true);
                    } else {
                        echo $order->get_formatted_shipping_address();
                    //    echo "<span style='display: none;'>$test</span>";
                    }
                }
            }
        }
    }

    public static function getSettingsPage()
    {
        // TODO: Implement getSettingsPage() method.
    }

    function getTextDomain()
    {
        // TODO: Implement getTextDomain() method.
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        // TODO: Implement getCustomPostTypes() method.
    }
}
