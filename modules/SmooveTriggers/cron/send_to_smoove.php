<?php
/**
 * It will send the automatic reminder emails to the customers.
 *
 * @author  Tyche Softwares
 * @package Abandoned-Cart-Lite-for-WooCommerce/Cron
 */
use Getraffic\Smoove;
static $wp_load; // Since this will be called twice, hold onto it.
if ( ! isset( $wp_load ) ) {
    $wp_load = false;
    $dir     = __FILE__;
    while( '/' != ( $dir = dirname( $dir ) ) ) {
        /**
         * Comment this "If" condition for generating the developer documentations.
         */
        if( file_exists( $wp_load = "{$dir}/wp-load.php" ) ) {
            break;
        }
    }
    /*
     * In case wp-content folder is seperated from WP core folders (like Bedrock setup from Roots.io) the above while loop will not find wp-load correctly, so we must use ABSPATH
     */
    if ( ! file_exists( $wp_load ) ) {
        $wp_load = trailingslashit( ABSPATH ) . 'wp-load.php';
    }
}
$wcal_root = dirname( dirname(__FILE__) ); // go two level up for directory from this file.
require_once $wp_load;


if ( !class_exists( 'woocommerce_abandon_cart_cron' ) ) {

    /**
     * It will send the automatic reminder emails to the customers
     * @since 1.3
     */
    class woocommerce_abandon_cart_cron { 

     /**
         * It will send the reminder emails to the cutomers.
         * It will also replace the merge code to its original data.
         * it will also check if guest id is valid, and check if the order is placed before sending the reminder email.
         * @globals mixed $wpdb
         * @globals mixed $woocommerce
         * @since 1.3
         */
        function wcal_send_to_smoove() {
            global $wpdb, $woocommerce;
            if( 'on' == get_option( 'wcal_enable_cart_emails' ) ) {
                //Grab the cart abandoned cut-off time from database.
                $cart_settings             = get_option( 'ac_lite_cart_abandoned_time' );

                if( $cart_settings == '' ) {
                    $cart_settings = 10;
                }

                $cart_abandon_cut_off_time = $cart_settings * 60;

                    $carts               = $this->wcal_get_carts( $cart_abandon_cut_off_time );
					//echo"<pre>";print_r($carts);
					foreach ( $carts as $key => $value ) {


						if( isset( $value->user_id ) ) {
							$user_id            = $value->user_id;
						}
						$key                = 'billing_email';
						$single             = true;
						$user_biiling_email = get_user_meta( $user_id, $key, $single );
						if ( isset( $user_biiling_email ) && $user_biiling_email != '' ) {
						   $value->user_email = $user_biiling_email;
						}

						if( isset( $value->abandoned_cart_info ) ) {
						   $cart_info_db_field = json_decode( $value->abandoned_cart_info );
						}

						$cart = new stdClass();
						if( !empty( $cart_info_db_field->cart ) ) {
							$cart           = $cart_info_db_field->cart;
						}

						$user_first_name      = '';
						$user_first_name_temp = get_user_meta( $value->user_id, 'billing_first_name', true );
						if( isset( $user_first_name_temp ) && "" == $user_first_name_temp ) {
							$user_data       = get_userdata( $user_id );
							if ( isset( $user_data->first_name ) ) {
								$user_first_name = $user_data->first_name;
							} else {
								$user_first_name = '';
							}
						} else {
							$user_first_name = $user_first_name_temp;
						}
						$user_last_name      = '';
						$user_last_name_temp = get_user_meta( $value->user_id, 'billing_last_name', true );
						if( isset( $user_last_name_temp ) && "" == $user_last_name_temp ) {
							$user_data  = get_userdata( $user_id );
							if ( isset( $user_data->last_name) ) {
								$user_last_name = $user_data->last_name;
							} else {
								$user_last_name = '';
							}
						} else {
							$user_last_name = $user_last_name_temp;
						}
						$user_email       = $value->user_email;
						$user_phone = get_user_meta( $value->user_id, 'billing_phone', true );
						$cart_update_time = $value->abandoned_cart_time;
                        $wcap_check_cart_staus_need_to_update = $this->wcal_update_abandoned_cart_status_for_placed_orders ( $cart_update_time, $value->user_id, $value->user_type, $value->id, $value->user_email );
						$product_names ='';
						foreach ($cart as $c) {
								$product_names .= get_the_title( $c->product_id ).' (SKU: '.get_post_meta($c->product_id,'_sku',true).'), '; 
						 }
						$customfields=array(
							get_option( 'ac_customfields_date' )  => date('d/m/Y'),
							get_option( 'ac_customfields_cart' ) => $product_names
							);

						if ( false == $wcap_check_cart_staus_need_to_update ) {
							$plugin_dir_path = plugin_dir_path( __FILE__ );
							require_once( $plugin_dir_path . '../../newsletter-providers/Smoove.php' );
							$smoove =  new \Getraffic\Smoove('',get_option( 'ac_listids_aband' ));
							$send_to_smoove = $smoove->subscribe($user_email, $user_first_name, $user_last_name, $user_phone, $customfields );
							$query_sent = "UPDATE `".ac_session::getTableName()."` SET sent_to_smoove = '1' WHERE id ='" . $value->id . "'";
							$wpdb->query( $query_sent );
					}

            }
        }
		}
 
        /**
         * Get all carts which have the creation time earlier than the one that is passed.
         * @param string | timestamp $cart_abandon_cut_off_time Cutoff time
         * @globals mixed $wpdb
         * @return array | object $results
         * @since 1.3
         */
        function wcal_get_carts( $cart_abandon_cut_off_time ) {
            global $wpdb;
            $cart_time = current_time( 'timestamp' ) - $cart_abandon_cut_off_time;

            $send_to_smoove = 0;
            $query = "SELECT wpac . * , wpu.user_login, wpu.user_email
                FROM `".ac_session::getTableName()."` AS wpac
                LEFT JOIN ".$wpdb->base_prefix."users AS wpu ON wpac.user_id = wpu.id
                WHERE sent_to_smoove = %s AND abandoned_cart_time < $cart_time
                ORDER BY `id` ASC ";

            $results = $wpdb->get_results( $wpdb->prepare( $query, $send_to_smoove ) );
            return $results;
            exit;
        }

        /**
         * It will update the abandoned cart staus if the order has been placed before sending the reminder emails.
         * @param string | timestamp $wcal_cart_time Abandoned time
         * @param string | int $wcal_user_id User id
         * @param string  $wcal_user_type User type
         * @param string | int $wcal_cart_id Abandoned cart id
         * @param string  $wcal_user_email User Email
         * @globals mixed $wpdb
         * @return boolean true | false
         * @since 4.3
         */
        public  function wcal_update_abandoned_cart_status_for_placed_orders(  $wcal_cart_time, $wcal_user_id, $wcal_user_type, $wcal_cart_id, $wcal_user_email ){
            global $wpdb;

                $updated_value = $this->wcal_update_status_of_loggedin ( $wcal_cart_id, $wcal_cart_time , $wcal_user_email );
                if( 1 == $updated_value ) {
                    return true;
                }
            return false;
        }

		/**
         * It will update the Loggedin users abandoned cart staus if the order has been placed before sending the reminder emails.
         * @param string | int $cart_id Abandoned cart id
         * @param string | timestamp $abandoned_cart_time Abandoned time
         * @param string  $user_billing_email User Email
         * @globals mixed $wpdb
         * @return int 0 | 1
         * @since 4.3
         */
        public  function wcal_update_status_of_loggedin( $cart_id, $abandoned_cart_time ,  $user_billing_email ) {
            global $wpdb;

            $query_email_id   = "SELECT wpm.post_id, wpost.post_date, wpost.post_status  FROM `" . $wpdb->prefix . "postmeta` AS wpm LEFT JOIN `" . $wpdb->prefix . "posts` AS wpost ON wpm.post_id =  wpost.ID WHERE wpm.meta_key = '_billing_email' AND wpm.meta_value = %s AND wpm.post_id = wpost.ID Order BY wpm.post_id DESC LIMIT 1";
            $results_query_email = $wpdb->get_results( $wpdb->prepare( $query_email_id, $user_billing_email  ) );

            if ( count ( $results_query_email ) > 0 ) {
                $current_time     = current_time( 'timestamp' );
                $todays_date      = date( 'Y-m-d', $current_time );
                $order_date_time  = $results_query_email[0]->post_date;
                $order_date       = substr( $order_date_time, 0, 10 );

                if ( $order_date == $todays_date ){
                    $wcal_check_email_sent_to_cart = $this->wcal_get_cart_sent_data ( $cart_id );

                    if ( 0 != $wcal_check_email_sent_to_cart ) {

                    }else {
                        $query_ignored = "UPDATE `".ac_session::getTableName()."` SET cart_ignored = '1' WHERE id ='" . $cart_id . "'";
                        //$wpdb->query( $query_ignored );
                    }
                    return 0;
                }else if ( strtotime( $order_date_time ) >=  $abandoned_cart_time ) {
                    $query_ignored = "UPDATE `".ac_session::getTableName()."` SET cart_ignored = '1' WHERE id ='" . $cart_id . "'";
                    $wpdb->query( $query_ignored );
                    return 1; //We return here 1 so it indicate that the cart has been modifed so do not sent email and delete from the array.
                }else if( "wc-pending" == $results_query_email[0]->post_status || "wc-failed" == $results_query_email[0]->post_status ) {
                    return 0; //if status of the order is pending or falied then return 0 so it will not delete that cart and send reminder email
                }
            }
            return 0; // it means there are no record found to be update it.
        }

        /**
         * It will check that for abandoned cart remider email has been sent.
         * @param string | int $wcal_cart_id Abandoned cart id
         * @return int  $wcal_sent_id | 0 Email sent id
         * @globals mixed $wpdb
         * @since 4.3
         */
        public static function wcal_get_cart_sent_data ( $wcal_cart_id ) {
            global $wpdb;

            $wcal_query   = "SELECT sent_to_smoove FROM `".ac_session::getTableName()."` WHERE id = %d ORDER BY 'id' DESC LIMIT 1 ";
            $wcal_results = $wpdb->get_results ( $wpdb->prepare( $wcal_query , $wcal_cart_id ) );

            if ( count( $wcal_results ) > 0 ) {
                $wcal_sent_id = $wcal_results[0]->sent_to_smoove;
                return $wcal_sent_id;
            }
            return 0;
        }


	}
}
$woocommerce_abandon_cart_cron = new woocommerce_abandon_cart_cron();
?>