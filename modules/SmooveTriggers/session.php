<?php
/**
 * Abandoned Carts
 *
 */

class ac_session {

    /**
     * Set Cart Session variables
     * 
     * @param string $session_key Key of the session
     * @param string $session_value Value of the session
     * @since 7.11.0
     */
    public static function wcal_set_cart_session( $session_key, $session_value ) {
        WC()->session->set( $session_key, $session_value );
    }


    /**
     * Get Cart Session variables
     * 
     * @param string $session_key Key of the session
     * @return mixed Value of the session
     * @since 7.11.0
     */
    public static function wcal_get_cart_session( $session_key ) {
        if ( ! is_object( WC()->session ) ) {
            return false;
        }
        return WC()->session->get( $session_key );
    }


    public static function getTableName() {
        global $wpdb;

        return $wpdb->prefix . "ac_abandoned_cart_history_lite";

    }


}

