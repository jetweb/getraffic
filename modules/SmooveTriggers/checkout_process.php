<?php
/**
 * Checkout Process for Abandoned Cart Lite
 * 
 * @since 5.3.0
 */

include_once('session.php');
use ac_session;
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if ( !class_exists( 'Wcal_Checkout_Process' ) ) {

	/**
	 * Process recovered orders
	 */
	class Wcal_Checkout_Process {
		
		function __construct() {
			add_action( 'woocommerce_order_status_changed', array( &$this, 'wcal_update_cart_details' ), 10, 3);

			add_action( 'woocommerce_checkout_order_processed', array( &$this, 'wcal_order_placed' ), 10 , 1 );
			add_filter( 'woocommerce_payment_complete_order_status', array( &$this, 'wcal_order_complete_action' ), 10 , 2 ); 
			add_action( 'woocommerce_thankyou', array( &$this, 'send_to_smoove' ), 10, 2 ); 

		}

		/**
		 * @hook woocommerce_order_status_changed
		 * @param int | string $order_id Order id
		 * @param string $wc_old_status Old status
		 * @param string $wc_new_status New status
		 * @globals mixed $wpdb
		 * @globals mixed $woocommerce
		 * @since 2.9
		 */
		public function wcal_update_cart_details( $order_id, $wc_old_status, $wc_new_status ) {

			if( 'pending' != $wc_new_status &&
		        'failed' != $wc_new_status &&
		        'cancelled' != $wc_new_status &&
		        'trash' != $wc_new_status ) {
		
		        global $wpdb;

		    	$wcal_history_table_name    = ac_session::getTableName();

		        if( $order_id > 0 ) {
		
		                $wcal_abandoned_id = get_post_meta( $order_id, 'wcal_abandoned_cart_id', true );

		                $query_cart_data = "SELECT user_id, user_type FROM `$wcal_history_table_name` WHERE id = %d";
		                $get_cart_data = $wpdb->get_results( 
		                	$wpdb->prepare( 
		                		$query_cart_data, 
		                		$wcal_abandoned_id 
		                	) 
		                );
		
		                $wpdb->delete( $wcal_history_table_name, array( 'id' => $wcal_abandoned_id ) );
		            
		        }
		    }elseif ( 'pending' == $wc_old_status && 'cancelled' == $wc_new_status ) {
		    	global $wpdb;

		    	$wcal_history_table_name = ac_session::getTableName();
		    	$wcal_abandoned_id = get_post_meta( $order_id, 'wcal_abandoned_cart_id', true );

				$wpdb->update( $wcal_history_table_name, array( 'cart_ignored' => '1' ), array( 'id' => $wcal_abandoned_id ) );
			}
		}

		public function send_to_smoove(  $order_id ) {

			$order = wc_get_order($order_id);

			$product_names ='';

			foreach( $order->get_items() as $item_id => $item ){
				$product_names .= $item->get_name() .', '; 
			}
			$user_id=$order->get_user_id();

			$billing_email = get_user_meta( $user_id, 'billing_email', true );
			$billing_first_name = get_user_meta( $user_id, 'billing_first_name', true );
			$billing_last_name = get_user_meta( $user_id, 'billing_last_name', true );
			$billing_phone = get_user_meta( $user_id, 'billing_phone', true );
			$ac_customfields = get_option( 'ac_customfields' );
			$customfields=array(
				get_option( 'ac_customfields_date' )  => date('d/m/Y'),
				get_option( 'ac_customfields_cart' ) => $product_names
				);

			$plugin_dir_path = plugin_dir_path( __FILE__ );
			require_once( $plugin_dir_path . '../newsletter-providers/Smoove.php' );
			$smoove = new \Getraffic\Smoove('',get_option( 'ac_listids' ));
			$send_to_smoove = $smoove->subscribe($billing_email, $billing_first_name, $billing_last_name, $billing_phone, $customfields );
		}
		/**
		 * It will check the WooCommerce order status. If the order status is pending or failed the we will keep that cart record 
		 * as an abandoned cart.
		 * It will be executed after order placed.
		 * @hook woocommerce_payment_complete_order_status
		 * @param string $order_status Order Status
		 * @param int | string $order_id Order Id
		 * @return string $order_status
		 * @globals mixed $wpdb
		 * @since 3.4
		 */
		public function wcal_order_complete_action( $woo_order_status, $order_id ) {

			global $wpdb;

			$order = wc_get_order( $order_id );

			$get_abandoned_id_of_order  = get_post_meta( $order_id, 'wcal_recover_order_placed', true );

        	// Order Status passed in the function is either 
        	// 'processing' or 'complete' and may or may not reflect the actual order status.
        	// Hence, always use the status fetched from the order object.

        	$order_status = ( $order ) ? $order->get_status() : '';

        	$wcal_ac_table_name                 = ac_session::getTableName();

        	if ( 'pending' != $order_status && 'failed' != $order_status && 'cancelled' != $order_status && 'trash' != $order_status) {
            	global $wpdb;

            	if ( isset( $get_abandoned_id_of_order ) && '' != $get_abandoned_id_of_order ){

					$ac_user_id_query = "SELECT user_id, abandoned_cart_time FROM `$wcal_ac_table_name` WHERE id = %d";
              		$ac_user_id_result = $wpdb->get_results( 
              			$wpdb->prepare( 
              				$ac_user_id_query, 
              				$get_abandoned_id_of_order 
              			) 
              		);

					if ( count( $ac_user_id_result ) > 0 ){
						$user_id = $ac_user_id_result[0]->user_id;
						$wpdb->delete( $wcal_ac_table_name, array( 'id' => $get_abandoned_id_of_order ) );
					}
				}
			}


        	return $woo_order_status;
		}


		/**
		 * When customer clicks on the "Place Order" button on the checkout page, it will identify if we need to keep that cart or 
		 * delete it.
		 * @hook woocommerce_checkout_order_processed
		 * @param int | string $order_id Order id
		 * @globals mixed $wpdb
		 * @globals mixed $woocommerce
		 * 
		 * @since 3.4
		 */    
		function wcal_order_placed( $order_id ) {

			global $wpdb;
	        $abandoned_order_id    = ac_session::wcal_get_cart_session( 'abandoned_cart_id_lite' );
	        $wcal_history_table_name    = ac_session::getTableName();
	        $abandoned_order_id_to_save = $abandoned_order_id;
	        add_post_meta( $order_id, 'wcal_abandoned_cart_id', $abandoned_order_id_to_save );
		}
	}
}

return new Wcal_Checkout_Process();