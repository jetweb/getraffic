<?php

namespace Getraffic;
include_once 'checkout_process.php';
include_once 'session.php';

use ac_session;
class SmooveTriggers  extends PluginBase implements ConfigurableModule
{


    public function __construct()
	{
		    self::install();
			$this->wcal_activate();
            add_action ( 'admin_menu',                                  array( &$this, 'wcal_admin_menu' ) );
            add_action( 'woocommerce_add_to_cart',                      array( &$this, 'wcal_store_cart_timestamp' ), 100 );
            add_action( 'woocommerce_cart_item_removed',                array( &$this, 'wcal_store_cart_timestamp' ), 100 );
            add_action( 'woocommerce_cart_item_restored',               array( &$this, 'wcal_store_cart_timestamp' ), 100 );
            add_action( 'woocommerce_after_cart_item_quantity_update',  array( &$this, 'wcal_store_cart_timestamp' ), 100 );
            add_action( 'woocommerce_calculate_totals',                 array( &$this, 'wcal_store_cart_timestamp' ), 100 );
            add_action( 'admin_init',                                   array( &$this, 'wcal_initialize_plugin_options' ) );
	}

        function wcal_activate() {
			if (isset($_GET['check_cron']) && $_GET['check_cron']=='1') {
				$plugin_dir_path = plugin_dir_path( __FILE__ );
				require_once( $plugin_dir_path . 'cron/send_to_smoove.php' );
				$woocommerce_abandon_cart_cron->wcal_send_to_smoove();
				//echo'cron';
				exit;
			}
		}
		
		/**
         * Add a submenu page under the WooCommerce.
         * @hook admin_menu
         * @since 1.0
         */
        function wcal_admin_menu() {
            $page = add_submenu_page ( 'woocommerce', __( 'Abandoned Carts', 'woocommerce-abandoned-cart' ), __( 'Abandoned Carts', 'woocommerce-abandoned-cart' ), 'manage_woocommerce', 'woocommerce_ac_page', array( &$this, 'wcal_menu_page' ) );
        }

        /**
         * Capture the cart and insert the information of the cart into DataBase.
         * @hook woocommerce_cart_updated
         * @globals mixed $wpdb
         * @globals mixed $woocommerce
         * @since 1.0
         */
        function wcal_store_cart_timestamp() {
            global $wpdb,$woocommerce;
            $current_time                    = current_time( 'timestamp' );
            $cut_off_time                    = get_option( 'ac_lite_cart_abandoned_time' );
            $cart_ignored                    = 0;

            if ( isset( $cut_off_time ) ) {
                $cart_cut_off_time = intval( $cut_off_time ) * 60;
            } else {
                $cart_cut_off_time = 60 * 60;
            }
            $compare_time = $current_time - $cart_cut_off_time;

            if ( is_user_logged_in() ) {

					$user_id = get_current_user_id();

                    $query   = "SELECT * FROM ".ac_session::getTableName()."
                                WHERE user_id      = %d
                                AND cart_ignored   = %s ";
                    $results = $wpdb->get_results( $wpdb->prepare( $query, $user_id, $cart_ignored ) );

                    if ( 0 == count( $results ) ) {

                        $cart_info_meta         = array();
                        $cart_info_meta['cart'] = WC()->session->cart;
                        $cart_info_meta         = json_encode( $cart_info_meta );

                        if( '' !== $cart_info_meta && '{"cart":[]}' != $cart_info_meta && '""' !== $cart_info_meta ) {
                            $cart_info    = $cart_info_meta;
                            $user_type    = "REGISTERED";
                            $insert_query = "INSERT INTO ".ac_session::getTableName()."
                                             ( user_id, abandoned_cart_info, abandoned_cart_time, cart_ignored, user_type )
                                             VALUES ( %d, %s, %d, %s, %s )";
                            $wpdb->query( $wpdb->prepare( $insert_query, $user_id, $cart_info,$current_time, $cart_ignored, $user_type ) );

                            $abandoned_cart_id = $wpdb->insert_id;
                            ac_session::wcal_set_cart_session( 'abandoned_cart_id_lite', $abandoned_cart_id );
                        }
                    } elseif ( isset( $results[0]->abandoned_cart_time ) && $compare_time > $results[0]->abandoned_cart_time ) {

                        $updated_cart_info         = array();
                        $updated_cart_info['cart'] = WC()->session->cart;
                        $updated_cart_info         = json_encode( $updated_cart_info );

                        if ( ! $this->wcal_compare_carts( $user_id, $results[0]->abandoned_cart_info ) ) {
                            $updated_cart_ignored = 1;
                            $query_ignored = "UPDATE ".ac_session::getTableName()."
                                              SET cart_ignored = %s
                                              WHERE user_id    = %d ";
                            $wpdb->query( $wpdb->prepare( $query_ignored, $updated_cart_ignored, $user_id ) );

                            $user_type    = "REGISTERED";
                            $query_update = "INSERT INTO ".ac_session::getTableName()."
                                             (user_id, abandoned_cart_info, abandoned_cart_time, cart_ignored, user_type)
                                             VALUES (%d, %s, %d, %s, %s)";
                            $wpdb->query( $wpdb->prepare( $query_update, $user_id, $updated_cart_info, $current_time, $cart_ignored, $user_type ) );

                            update_user_meta ( $user_id, '_woocommerce_ac_modified_cart', md5( "yes" ) );

                            $abandoned_cart_id                  = $wpdb->insert_id;
                      ac_session::wcal_set_cart_session( 'abandoned_cart_id_lite', $abandoned_cart_id );
                        } else {
                            update_user_meta ( $user_id, '_woocommerce_ac_modified_cart', md5( "no" ) );
                        }
                    } else {

                        $updated_cart_info         = array();
                        $updated_cart_info['cart'] = WC()->session->cart;
                        $updated_cart_info         = json_encode( $updated_cart_info );

                        $query_update = "UPDATE ".ac_session::getTableName()."
                                         SET abandoned_cart_info = %s,
                                             abandoned_cart_time = %d
                                         WHERE user_id      = %d
                                         AND   cart_ignored = %s ";
                        $wpdb->query( $wpdb->prepare( $query_update, $updated_cart_info, $current_time, $user_id, $cart_ignored ) );

                        $query_update = "SELECT * FROM ".ac_session::getTableName()." WHERE user_id ='" . $user_id . "' AND cart_ignored='0' ";
                        $get_abandoned_record = $wpdb->get_results( $query_update );

                        if ( count( $get_abandoned_record ) > 0 ) {
                            $abandoned_cart_id   = $get_abandoned_record[0]->id;
                            ac_session::wcal_set_cart_session( 'abandoned_cart_id_lite', $abandoned_cart_id );
                        }
                    }
            }
        }

        /**
         * It will compare only loggedin users cart while capturing the cart.
         * @param int | string $user_id User id
         * @param json_encode $last_abandoned_cart Old abandoned cart details
         * @return boolean true | false
         * @since 1.0
         */
        function wcal_compare_carts( $user_id, $last_abandoned_cart ) {
            global $woocommerce;
            $current_woo_cart   = array();
            $abandoned_cart_arr = array();
            $wcal_woocommerce_persistent_cart =version_compare( $woocommerce->version, '3.1.0', ">=" ) ? '_woocommerce_persistent_cart_' . get_current_blog_id() : '_woocommerce_persistent_cart' ;
            $current_woo_cart   = get_user_meta( $user_id, $wcal_woocommerce_persistent_cart, true );
            $abandoned_cart_arr = json_decode( $last_abandoned_cart, true );
            $temp_variable      = "";
            if ( isset( $current_woo_cart['cart'] ) && isset( $abandoned_cart_arr['cart'] ) ) {
                if ( count( $current_woo_cart['cart'] ) >= count( $abandoned_cart_arr['cart'] ) ) {
                    //do nothing
                } else {
                    $temp_variable      = $current_woo_cart;
                    $current_woo_cart   = $abandoned_cart_arr;
                    $abandoned_cart_arr = $temp_variable;
                }
                if ( is_array( $current_woo_cart ) && is_array( $abandoned_cart_arr ) ) {
                    foreach ( $current_woo_cart as $key => $value ) {

                        foreach ( $value as $item_key => $item_value ) {
                            $current_cart_product_id   = $item_value['product_id'];
                            $current_cart_variation_id = $item_value['variation_id'];
                            $current_cart_quantity     = $item_value['quantity'];

                            if ( isset( $abandoned_cart_arr[$key][$item_key]['product_id'] ) ) {
                                $abandoned_cart_product_id = $abandoned_cart_arr[$key][$item_key]['product_id'];
                            } else {
                                $abandoned_cart_product_id = "";
                            }
                            if ( isset( $abandoned_cart_arr[$key][$item_key]['variation_id'] ) ) {
                                 $abandoned_cart_variation_id = $abandoned_cart_arr[$key][$item_key]['variation_id'];
                            } else {
                                $abandoned_cart_variation_id = "";
                            }
                            if ( isset( $abandoned_cart_arr[$key][$item_key]['quantity'] ) ) {
                                 $abandoned_cart_quantity = $abandoned_cart_arr[$key][$item_key]['quantity'];
                            } else {
                                 $abandoned_cart_quantity = "";
                            }
                            if ( ( $current_cart_product_id != $abandoned_cart_product_id ) ||
                                 ( $current_cart_variation_id != $abandoned_cart_variation_id ) ||
                                 ( $current_cart_quantity != $abandoned_cart_quantity ) )
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        /**
         * It will add the section, field, & registres the plugin fields using Settings API.
         * @hook admin_init
         * @since 2.5
         */
        function wcal_initialize_plugin_options() {

            // First, we register a section. This is necessary since all future options must belong to a
            add_settings_section(
                'ac_lite_general_settings_section',                 // ID used to identify this section and with which to register options
                __( 'Settings', 'woocommerce-abandoned-cart' ),     // Title to be displayed on the administration page
                array( $this, 'ac_lite_general_options_callback' ), // Callback used to render the description of the section
                'woocommerce_ac_page'                               // Page on which to add this section of options
            );

            add_settings_field(
                'wcal_enable_cart_emails',
                __( 'Enable abandoned cart emails', 'woocommerce-abandoned-cart' ),
                array( $this, 'wcal_enable_cart_emails_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( "Yes, enable the abandoned cart emails.", 'woocommerce-abandoned-cart' ) )
            );

            add_settings_field(
                'ac_lite_cart_abandoned_time',
                __( 'Cart abandoned cut-off time', 'woocommerce-abandoned-cart' ),
                array( $this, 'ac_lite_cart_abandoned_time_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Consider cart abandoned after X minutes of item being added to cart & order not placed.', 'woocommerce-abandoned-cart' ) )
            );

            add_settings_field(
                'ac_lite_delete_abandoned_order_days',
                __( 'Automatically Delete Abandoned Orders after X days', 'woocommerce-abandoned-cart' ),
                array( $this, 'wcal_delete_abandoned_orders_days_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Automatically delete abandoned cart orders after X days.', 'woocommerce-abandoned-cart' ) )
            );

            add_settings_field(
                'ac_listids',
                __( 'Smoove List ID - Purchased', 'woocommerce-abandoned-cart' ),
                array( $this, 'ac_listids_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Smoove List ID - Purchased', 'woocommerce-abandoned-cart' ) )
            );

            add_settings_field(
                'ac_listids_aband',
                __( 'Smoove List ID - Abandoned', 'woocommerce-abandoned-cart' ),
                array( $this, 'ac_listids_aband_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Smoove List ID - Abandoned', 'woocommerce-abandoned-cart' ) )
            );

            add_settings_field(
                'ac_customfields_date',
                __( 'Custom Field ID - Date', 'woocommerce-abandoned-cart' ),
                array( $this, 'ac_customfields_date_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Custom Field ID - Date', 'woocommerce-abandoned-cart' ) )
            );

             add_settings_field(
                'ac_customfields_cart',
                __( 'Custom Field ID - Cart', 'woocommerce-abandoned-cart' ),
                array( $this, 'ac_customfields_cart_callback' ),
                'woocommerce_ac_page',
                'ac_lite_general_settings_section',
                array( __( 'Custom Field ID - Cart', 'woocommerce-abandoned-cart' ) )
            );

            // Finally, we register the fields with WordPress
            register_setting(
                'woocommerce_ac_settings',
                'wcal_enable_cart_emails'
            );

            register_setting(
                'woocommerce_ac_settings',
                'ac_lite_cart_abandoned_time',
                array ( $this, 'ac_lite_cart_time_validation' )
            );

            register_setting(
                'woocommerce_ac_settings',
                'ac_lite_delete_abandoned_order_days',
                array ( $this, 'wcal_delete_days_validation' )
            );

            register_setting(
                'woocommerce_ac_settings',
                'ac_listids'
            );

            register_setting(
                'woocommerce_ac_settings',
                'ac_listids_aband'
            );

             register_setting(
                'woocommerce_ac_settings',
                'ac_customfields_date'
            );

             register_setting(
                'woocommerce_ac_settings',
                'ac_customfields_cart'
            );

           do_action ( "wcal_add_new_settings" );
        }
        /**
         * Settings API callback for section "ac_lite_general_settings_section".
         * @since 2.5
         */
        function ac_lite_general_options_callback() {

        }

        /**
         * Settings API callback for the enable cart reminder emails
         * @since 5.5
         */
        public static function wcal_enable_cart_emails_callback( $args ) {

            $enable_cart_emails = get_option( 'wcal_enable_cart_emails' );

            if  (isset( $enable_cart_emails ) &&  $enable_cart_emails == "" ) {
                $enable_cart_emails = 'off';
            }
            $html = '<input type="checkbox" id="wcal_enable_cart_emails" name="wcal_enable_cart_emails" value="on" ' . checked( 'on', $enable_cart_emails, false ) . ' />';
            $html .= '<label for="wcal_enable_cart_emails"> ' . $args[0] . '</label>';
            echo $html;
        }

        /**
         * Settings API callback for cart time field.
         * @param array $args Arguments
         * @since 2.5
         */
        function ac_lite_cart_abandoned_time_callback( $args ) {

            $cart_abandoned_time = get_option( 'ac_lite_cart_abandoned_time' );
            printf(
            '<input type="text" id="ac_lite_cart_abandoned_time" name="ac_lite_cart_abandoned_time" value="%s" />',
            isset( $cart_abandoned_time ) ? esc_attr( $cart_abandoned_time ) : ''
                );

            $html = '<label for="ac_lite_cart_abandoned_time"> '  . $args[0] . '</label>';
            echo $html;
        }

        /**
         * Settings API cart time field validation.
         * @param int | string $input
         * @return int | string $output
         * @since 2.5
         */
        function ac_lite_cart_time_validation( $input ) {
            $output = '';
            if ( '' != $input && ( is_numeric( $input) && $input > 0  ) ) {
                $output = stripslashes( $input) ;
            } else {
                add_settings_error( 'ac_lite_cart_abandoned_time', 'error found', __( 'Abandoned cart cut off time should be numeric and has to be greater than 0.', 'woocommerce-abandoned-cart' ) );
            }
            return $output;
        }

        /**
         * Validation for automatically delete abandoned carts after X days.
         * @param int | string $input input of the field Abandoned cart cut off time
         * @return int | string $output Error message or the input value
         * @since  5.0
         */
        public static function wcal_delete_days_validation( $input ) {
            $output = '';
            if ( '' == $input || ( is_numeric( $input ) && $input > 0 ) ) {
                $output = stripslashes( $input );
            } else {
                add_settings_error( 'ac_lite_delete_abandoned_order_days', 'error found', __( 'Automatically Delete Abandoned Orders after X days has to be greater than 0.', 'woocommerce-abandoned-cart' ) );
            }
            return $output;
        }

        /**
         * Callback for deleting abandoned order after X days field.
         * @param array $args Argument given while adding the field
         * @since 5.0
         */
        public static function wcal_delete_abandoned_orders_days_callback( $args ) {

            $delete_abandoned_order_days = get_option( 'ac_lite_delete_abandoned_order_days' );
            printf(
                '<input type="text" id="ac_lite_delete_abandoned_order_days" name="ac_lite_delete_abandoned_order_days" value="%s" />',
                isset( $delete_abandoned_order_days ) ? esc_attr( $delete_abandoned_order_days ) : ''
            );

            $html = '<label for="ac_lite_delete_abandoned_order_days"> ' . $args[0] . '</label>';
            echo $html;
        }

        public static function ac_listids_callback( $args ) {

            $ac_listids = get_option( 'ac_listids' );
            printf(
                '<input type="text" id="ac_listids" name="ac_listids" value="%s" />',
                isset( $ac_listids ) ? esc_attr( $ac_listids ) : ''
            );

            $html = '<label for="ac_listids"> ' . $args[0] . '</label>';
            echo $html;
        }

        public static function ac_listids_aband_callback( $args ) {

            $ac_listids_aband = get_option( 'ac_listids_aband' );
            printf(
                '<input type="text" id="ac_listids_aband" name="ac_listids_aband" value="%s" />',
                isset( $ac_listids_aband ) ? esc_attr( $ac_listids_aband ) : ''
            );

            $html = '<label for="ac_listids_aband"> ' . $args[0] . '</label>';
            echo $html;
        }

		public static function ac_customfields_date_callback( $args ) {

            $ac_customfields_date = get_option( 'ac_customfields_date' );
            printf(
                '<input type="text" id="ac_customfields_date" name="ac_customfields_date" value="%s" />',
                isset( $ac_customfields_date ) ? esc_attr( $ac_customfields_date ) : ''
            );

            $html = '<label for="ac_customfields_date"> ' . $args[0] . '</label>';
            echo $html;
        }

		public static function ac_customfields_cart_callback( $args ) {

            $ac_customfields_cart = get_option( 'ac_customfields_cart' );
            printf(
                '<input type="text" id="ac_customfields_cart" name="ac_customfields_cart" value="%s" />',
                isset( $ac_customfields_cart ) ? esc_attr( $ac_customfields_cart ) : ''
            );

            $html = '<label for="ac_customfields_cart"> ' . $args[0] . '</label>';
            echo $html;
        }


		/**
         * Abandon Cart Settings Page. It will show the tabs, notices for the plugin.
         * It will also update the template records and display the template fields.
         * It will also show the abandoned cart details page.
         * It will also show the details of all the tabs.
         * @globals mixed $wpdb
         * @since 1.0
         */
        function wcal_menu_page() {

            if ( is_user_logged_in() ) {
                global $wpdb;
                // Check the user capabilities
                if ( ! current_user_can( 'manage_woocommerce' ) ) {
                    wp_die( __( 'You do not have sufficient permissions to access this page.', 'woocommerce-abandoned-cart' ) );
                }
                ?>
                <div class="wrap">
                    <h2><?php _e( 'WooCommerce - Abandoned Carts', 'woocommerce-abandoned-cart' ); ?></h2>
                <?php


                if ( isset( $_GET['action'] ) ) {
                    $action = $_GET['action'];
                } else {
                    $action = "";
                }
                if ( isset( $_GET['mode'] ) ) {
                    $mode = $_GET['mode'];
                } else {
                    $mode = "";
                }
                $this->wcal_display_tabs();

                do_action ( 'wcal_add_tab_content' );

                /**
                 * When we delete the item from the below drop down it is registred in action 2
                */
                if ( isset( $_GET['action2'] ) ) {
                    $action_two = $_GET['action2'];
                } else {
                    $action_two = "";
                }

                 if ( '' == $action ) {
                 // Save the field values
                    ?>
                    <p><?php _e( 'Change settings for sending email notifications', 'woocommerce-abandoned-cart' ); ?></p>
                    <div id="content">
                    <?php
                        $wcal_general_settings_class = $wcal_email_setting = $wcap_sms_settings = $wcap_atc_settings = $wcap_fb_settings = "";

                        $section = isset( $_GET[ 'wcal_section' ] ) ? $_GET[ 'wcal_section' ] : '';

                        switch( $section ) {

                            case 'wcal_general_settings':
                            case '':
                                $wcal_general_settings_class = "current";
                                break;


                            default:
                                $wcal_general_settings_class = "current";
                                break;

                        }
?>
                        <ul class="subsubsub" id="wcal_general_settings_list">
                            <li>
                                <a href="admin.php?page=woocommerce_ac_page&action=emailsettings&wcal_section=wcal_general_settings" class="<?php echo $wcal_general_settings_class; ?>"><?php _e( 'General Settings', 'woocommerce-abandoned-cart' );?> </a> |
                            </li>
                        </ul>
                        <br class="clear">
                        <?php
                        if ( 'wcal_general_settings' == $section || '' == $section ) {
                        ?>
                            <form method="post" action="options.php">
                                <?php settings_fields( 'woocommerce_ac_settings' ); ?>
                                <?php do_settings_sections( 'woocommerce_ac_page' ); ?>
                                <?php settings_errors(); ?>
                                <?php submit_button(); ?>
                            </form>
                        <?php
                        } 
                        ?>
                    </div>
                  <?php
                  } 
            }
            echo( "</table>" );

            if ( isset( $_GET['action'] ) ) {
                $action = $_GET['action'];
            }
            if ( isset( $_GET['mode'] ) ) {
                $mode = $_GET['mode'];
            }
 
        }

        /**
         * It will add the tabs on the Abandoned cart page.
         * @since 1.0
         */
        function wcal_display_tabs() {

            if ( isset( $_GET['action'] ) ) {
                $action = $_GET['action'];
            } else {
                $action                = "";
                $active_settings       = "";
            }
            if ( ( 'emailsettings' == $action ) || '' == $action ) {
                $active_settings = "nav-tab-active";
            }
            ?>
            <h2 class="nav-tab-wrapper woo-nav-tab-wrapper">
                <a href="admin.php?page=woocommerce_ac_page&action=emailsettings" class="nav-tab <?php if ( isset( $active_settings ) ) echo $active_settings; ?>"> <?php _e( 'Settings', 'woocommerce-abandoned-cart' );?> </a>
                <?php do_action( 'wcal_add_settings_tab' ); ?>
            </h2>
            <?php
        }




    /**
     * @return array
     */
    static function getInstallSqlSequences()
    {
        return [
            self::getCreateSubscriptionsTableSQL()
        ];
    }


    /**
     * @return string
     */
    public static function getCreateSubscriptionsTableSQL()
    {
        $ac_history_table_name = ac_session::getTableName();

		$sql = "CREATE TABLE IF NOT EXISTS $ac_history_table_name (
						 `id` int(11) NOT NULL AUTO_INCREMENT,
						 `user_id` int(11) NOT NULL,
						 `abandoned_cart_info` text COLLATE utf8_unicode_ci NOT NULL,
						 `abandoned_cart_time` int(11) NOT NULL,
						 `cart_ignored` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
						 `sent_to_smoove` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
						 `user_type` text,
						 PRIMARY KEY (`id`)
						 ) ";


        return $sql;
    }


    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    function getTextDomain()
    {
        return '';
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        return [];
    }

    public static function getSettingsPage()
    {
	}

}