<?php

namespace Getraffic;

class MailChimp implements ConfigurableModule
{
    const BASE_URL = 'https://ssl-vp.com/rest/v1/Contacts?updateIfExists=true&restoreIfDeleted=true&restoreIfUnsubscribed=true&api_key=';
    const SETTINGS_PAGE_ID = 'mailchimp-settings-page';

    private $apiKey  = '';
    private $listIds = [];

    public function __construct($apiKey = '', $listIds = '')
    {
        $settings = get_option(self::SETTINGS_PAGE_ID);

        $this->apiKey  = $apiKey;
        $this->listIds = $listIds;

        if (!$this->apiKey) {
            $this->apiKey = $settings['mc-api-key'];
        }

        if (!$this->listIds) {
            $this->listIds = $settings['mc-contact-lists'];
        }
    }

    public function subscribe($email, $firstName = '', $lastName = '', $phone = '', $silent = false)
    {
        $merges = array(
            'FNAME'=> $firstName,
            'LNAME'=> $lastName,
            'PHONE'=> $phone
        );

        $postData = array(
            "email_address" => $email,
            "status" => "subscribed",
            'merge_fields' => $merges
        );

        $ch = curl_init('https://us12.api.mailchimp.com/3.0/lists/'.$this->listIds.'/members/');
        curl_setopt_array($ch, array(
            CURLOPT_POST => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_HTTPHEADER => array(
                'Authorization: apikey '.$this->apiKey,
                'Content-Type: application/json'
            ),
            CURLOPT_POSTFIELDS => json_encode($postData)
        ));

        $response = curl_exec($ch);
        $response = json_decode($response);

        if ($silent) {

            if ($response->title === 'Member Exists') {
                $ch = curl_init('https://us12.api.mailchimp.com/3.0/lists/'.$this->listIds.'/members/' . md5($email));
                curl_setopt_array($ch, array(
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array(
                        'Authorization: apikey '.$this->apiKey,
                        'Content-Type: application/json'
                    ),
                    CURLOPT_POSTFIELDS => json_encode($postData)
                ));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                $response = curl_exec($ch);
                $response = json_decode($response);
            }

            return $response;
        }


        if ($response->title === 'Member Exists') {
            wp_send_json([
                'error' => 'כתובת האימייל כבר קיימת במאגר'
            ]);
        }

        wp_send_json([
            'success' => 'נרשמת בהצלחה'
        ]);
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('settings-section')
            ->setTitle('Mailchimp Connection Details')
            ->setSectionInfo('Connection details')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('mc-api-key')
                        ->setTitle('API Key'),
                    (new AdminSettingsField())
                        ->setName('mc-contact-lists')
                        ->setTitle('Contact Lists, comma separated'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'Mailchimp Integration',
            'Mailchimp Integration',
            'menu-settings-mailchimp',
            [$section]);
    }
}
