<?php

namespace Getraffic;

class Smoove implements ConfigurableModule
{
    const BASE_URL = 'https://ssl-vp.com/rest/v1/Contacts?updateIfExists=true&restoreIfDeleted=true&restoreIfUnsubscribed=true&api_key=';
    const SETTINGS_PAGE_ID = 'smoove-settings-page';

    private $apiKey  = '';
    private $listIds = [];

    public function __construct($apiKey = '', $listIds = '')
    {
        $settings = get_option(self::SETTINGS_PAGE_ID);

        $this->apiKey  = $apiKey;
        $this->listIds = $listIds;

        if (!$this->apiKey) {
            $this->apiKey = $settings['smooth-api-key'];
        }

        if (!$this->listIds) {
            $this->listIds = $settings['smooth-contact-lists'];
        }
    }

    public function subscribe($email, $firstName, $lastName, $phone, $customFields = [])
    {
        $url = self::BASE_URL . $this->apiKey;
        $api_data = [
            'cellphone'             => $phone,
            'lists_ToSubscribe'     => $this->listIds,
            'email'                 => $email,
            'position'              => '',
            'firstName'             => $firstName,
            'lastName'              => $lastName,
            'canReceiveSmsMessages' => '',
            'canReceiveEmails'      => '',
            'customFields'          => $customFields
        ];

        $options = [
            'http' => [
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                'method'  => 'POST',
                'content' => http_build_query($api_data),
            ],
        ];
        $context = stream_context_create($options);
        $result  = file_get_contents($url, false, $context);
        if ($result === false) { /* Handle error */
        }

        return $result;
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('settings-section')
            ->setTitle('Smoove Connection Details')
            ->setSectionInfo('Connection details')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('smooth-api-key')
                        ->setTitle('API Key'),
                    (new AdminSettingsField())
                        ->setName('smooth-contact-lists')
                        ->setTitle('Contact Lists, comma separated'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'Smoove Integration',
            'Smoove Integration',
            'menu-settings-smoove',
            [$section]);
    }
}
