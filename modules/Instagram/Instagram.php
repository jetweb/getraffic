<?php

namespace Getraffic;

use GT;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use GuzzleHttp;

include_once('vendor/autoload.php');

/**
 * Class Instagram
 */
class Instagram
{
    public static function getFeed($username)
    {


        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', 'https://www.instagram.com/buniquedesign/', [
            'auth' => ['buniquedesign', '14b06unique2020']
        ]);
        echo $res->getStatusCode();
// "200"
        echo $res->getHeader('content-type')[0];
// 'application/json; charset=utf8'
        echo $res->getBody();
// {"type":"User"...'
        return;

            $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');

            $api = new Api($cachePool);
            $api->login('buniquedesign', '14b06unique2020'); // mandatory
            $profile = $api->getProfile($username);

            echo "nir";
            echo $profile->getUserName(); // robertdowneyjr

            echo $profile->getFullName(); // Robert Downey Jr. Official


//        $cache = new \Instagram\Storage\CacheManager(WP_CONTENT_DIR . '/cache-insta/');
//        $api   = new \Instagram\Api($cache);
//        $api->setUserName($username);
//
//        $cacheKey = 'insta__' . $username;
//        $cacheResult = GT::getFromCache($cacheKey);
//        if (false && $cacheResult) {
//            return $cacheResult;
//        }
//
//        $apiResult = $api->getFeed();
//        if(layer_is_office_ip()) {
//         //   print_r($apiResult);
//        }
//        GT::setInCache($cacheKey, $apiResult);
//
//        return $apiResult;
    }

    public static function printFeed($username) {
        $feed = self::getFeed($username);
        ?>
        <div class="insta-container">
            <div class="insta-feed">
                <?php
                foreach ($feed->getMedias() as $media) {
                    ?>
                    <div class="insta-item col-md-2 pl-0 pr-0">
                        <a href="<?php echo $media->getLink(); ?>">
                            <img class="thumb" src="<?php echo $media->getThumbnailSrc(); ?>"/>
                            <div class="insta-overlay">
                                <div class="insta-likes"><?php echo $media->likes; ?></div>
                                <div class="insta-comments"><?php echo $media->comments; ?></div>
                            </div>
                        </a>
                    </div>
                    <?php
                } ?>
            </div>
        </div>
        <style>
            .insta-feed {
                overflow: hidden;
                flex-wrap: nowrap;
            }

            .insta-feed .insta-item {
                position: relative;
            }

            .insta-feed .insta-item:hover .insta-overlay {
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .insta-feed .insta-item img.thumb {
                width: 100%;
            }
            .insta-feed .insta-item .insta-overlay {
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
/*                background-image: url(<?php echo img('insta.png'); ?>);*/
                background-repeat: no-repeat;
                background-position: center center;
                background-color: rgb(0, 0, 0, 0.5);
                display: none;
            }
            .insta-feed .insta-item .insta-overlay .insta-likes, .insta-feed .insta-item .insta-overlay .insta-comments{
                background-repeat: no-repeat;
                background-position: center center;
                background-size: 48%;
                color: #fff;
                font-size: 30px;
                font-weight: bold;
                width: 55px;                
            }
            .insta-feed .insta-item .insta-overlay .insta-likes{
                background-image: url(<?php echo img('heart.png'); ?>);
                margin-left: 10px;
                text-indent: -65px;
            }
            .insta-feed .insta-item .insta-overlay .insta-comments{
                background-image: url(<?php echo img('bubble.png'); ?>);
                margin-right: 10px;
                margin-left: -20px;
                text-indent: -55px;
            }

            @media screen and (max-width: 768px) {
                .insta-feed {
                    flex-wrap: wrap;
                }
            }
        </style>
<?php
    }
}

?>