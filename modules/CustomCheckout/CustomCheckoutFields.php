<?php

namespace Getraffic;

class CustomCheckoutFields
{
    private $customAddressFields = null;

    public $fields = [
        'billing' => [
            'billing_email'      => [
                'placeholder' => 'כתובת מייל *',
                'required'    => true,
                'custom_attributes' => ['empty-message' => 'חובה להזין כתובת אימייל'],
                'class'       => ['form-row-first', 'validate-email'],
            ],
            'billing_checkbox'   => [
                'type'  => 'checkbox',
                'class' => ['form-row-last'],
                'label' => '<span>מאשרת קבלת עידכונים</span>',
                'clear' => true,
            ],
            'billing_first_name' => [
                'placeholder' => 'שם פרטי *',
                'required'    => true,
                'class'       => ['form-row-first'],
                'clear'       => true,
            ],
            'billing_last_name'  => [
                'placeholder' => 'שם משפחה *',
                'required'    => true,
                'class'       => ['form-row-last'],
                'clear'       => true,
            ],
            'billing_phone'      => [
                'placeholder' => 'טלפון',
                'required'    => false,
                'class'       => ['form-row-first', 'validate-phone'],
                'clear'       => true,
            ],
            'account_password'   => [
                'type'        => 'password',
                'placeholder' => 'סיסמא *  (לצורך מעקב אחרי ההזמנה)',
                'required'    => true,
                'class'       => ['form-row-last'],
                'clear'       => true,
            ],
            'billing_country'    => [ // This field is mandatory
                'type'     => 'country',
                'required' => true,
                'label'    => 'ארץ',
                'class'    => ['form-row-last, hidden'],
            ],
        ],
        'order'   => [
            'order_comments' => [
                'type' => 'textarea',
                'class' => ['form-row-first'],
                'placeholder' => 'הערות להזמנה'
            ]
        ],
        'shipping' => [
            'billing_street' => [
                'placeholder' => 'רחוב',
                'required'    => false,
                'class'       => ['form-row-first'],
                'clear'       => true,
            ],
            'billing_house' => [
                'placeholder' => 'מספר בית',
                'required'    => false,
                'class'       => ['form-row-last'],
                'clear'       => true
            ],
            'billing_appartment' => [
                'placeholder' => 'דירה',
                'prefix'      => 'דירה ',
                'required'    => false,
                'class'       => ['form-row-first'],
                'clear'       => true
            ],
            'billing_floor' => [
                'placeholder' => 'קומה',
                'prefix'      => 'קומה ',
                'required'    => false,
                'class'       => ['form-row-last'],
                'clear'       => true
            ],
            'billing_city' => [
                'placeholder' => 'עיר',
                'required'    => false,
                'class'       => ['form-row-first'],
                'clear'       => true,
            ],
        ],
        'account' => []
    ];

    public function __construct(CustomAddressFields $fields)
    {
        $this->customAddressFields = $fields;
        add_filter('woocommerce_checkout_fields', function ($fields) {
            return $this->get_checkout_fields_array();
        });

        add_action('woocommerce_admin_order_data_after_shipping_address', function($order) {
            foreach ($this->fields['shipping'] as $name => $field) {
                $title = null;
                if (isset($field['title'])) {
                    $title = $field['title'];
                }
                if (!$title) {
                    $title = $field['placeholder'];
                }
                $value = get_post_meta($order->get_id(), '_' . $name, true);
                if (isset($field['type']) && $field['type'] === 'radio') {
                    $value = $field['options'][$value];
                }
                echo '<p><strong>' . $title . ':</strong> ' . $value . '</p>';
            }
        });

        add_filter('woocommerce_order_get_shipping_address_1', function($value, \WC_Order $order) {
            if (!empty($value)) {
                return $value;
            }
            return $this->getFormattedAddress($order);
        }, 10, 2);

        add_filter('woocommerce_get_order_address', function ($address, $type, $order) {
            if ($type === 'shipping') {
                if (empty($address['address_1'])) {
                    $address['address_1'] = $this->getFormattedAddress($order);
                }
            }
           return $address;
        }, 10, 3);

        add_action( 'woocommerce_checkout_create_order', function ( $order, $data ) {
            if ($order instanceof \WC_Order) {
                if (empty($order->get_shipping_first_name())) {
                    $order->set_shipping_first_name($order->get_billing_first_name());
                }

                if (empty($order->get_shipping_last_name())) {
                    $order->set_shipping_last_name($order->get_billing_last_name());
                }
            }
        }, 10, 2);
    }

    public function getFormattedAddress(\WC_Order $order) {
        if ($order instanceof \WC_Order) {
            $text = '';
            foreach ($this->fields['shipping'] as $name => $field) {
                if (isset($field['exclude'])) {
                    continue;
                }
                $value = $order->get_meta('_' . $name);
                if (empty($value)) {
                    if (is_callable([$order, 'get_' . $name])) {
                        $value = $order->{"get_{$name}"}();
                    }
                }
                if (isset($field['type']) && $field['type'] === 'radio') {
                    $value = $field['options'][$value];
                }
                if (!empty($value)) {
                    $text .= (isset($field['prefix']) ? $field['prefix'] : '') . $value . ' ';
                }
            }
        }

        return $text;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    function get_checkout_fields_array()
    {
        $retVal   = [];
        $priority = 0;
        foreach ($this->fields as $group => $array) {
            // Disable shipping fields when "pick up" method selected
            if ($group === 'shipping' &&
                isset($_REQUEST['shipping_method']) &&
                (strpos($_REQUEST['shipping_method'][0], 'pick') !== false)) {
                continue;
            }
            $arrFields = [];
            foreach ($array as $fieldName => $field) {
                $priority                        += 10;
                $field['priority']               = $priority;
                $arrFields[$fieldName] = $field;
            }
            $retVal[$group] = $arrFields;
        }

        if (is_user_logged_in()) {
            unset($retVal['billing']['billing_password']);
        }

        return $retVal;
    }

}
