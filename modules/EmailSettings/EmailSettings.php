<?php

namespace Getraffic;

class EmailSettings extends PluginBase implements ConfigurableModule
{
    const SETTINGS_PAGE_ID = 'gt-email-settings-page';
    private $settings;

    public function __construct()
    {
        $this->settings = get_option(self::SETTINGS_PAGE_ID);
        add_filter('wp_mail_from', function ($value) {
            $settings = get_option(self::SETTINGS_PAGE_ID);

            if (isset($settings['sender-address']) && !empty($settings['sender-address'])) {
                return $settings['sender-address'];
            }

            return $value;
        });
        add_filter('wp_mail_from_name', function ($value) {
            $settings = get_option(self::SETTINGS_PAGE_ID);

            if (isset($settings['sender-name']) && !empty($settings['sender-name'])) {
                return $settings['sender-name'];
            }

            return $value;
        });
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('gt_emails-settings-section')
            ->setTitle('Email Settings')
            ->setSectionInfo('Email Sender')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('sender-name')
                        ->setTitle('Email Sender Name'),
                    (new AdminSettingsField())
                        ->setName('sender-address')
                        ->setTitle('Email Sender Address'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'Email Settings',
            'Email Settings',
            'menu-settings-gt_emails-settings',
            [$section]);
    }

    function getTextDomain()
    {
        // TODO: Implement getTextDomain() method.
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        // TODO: Implement getCustomPostTypes() method.
    }
}
