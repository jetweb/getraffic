<?php

require_once(__DIR__ . '/../../vendor/autoload.php');

require_once(__DIR__ . '../../../../../../wp-load.php');



use Getraffic\AvailabilitySubscription;



try {

    $subscriptions = AvailabilitySubscription::getAllSubscriptions();

    header('Content-Type: text/csv; charset=utf-8');

    header('Content-Disposition: attachment; filename=data.csv');

// create a file pointer connected to the output stream

    $output = fopen('php://output', 'w');

// output the column headings

    fputcsv($output, ['#', 'מזהה לקוח', 'כתובת אימייל', 'מוצר', 'תאריך הרשמה', 'תאריך חזרה למלאי']);

    foreach ($subscriptions as $subscription) {

        $user     = get_user_by_email($subscription['email']);

        $userText = '';

        if ($user) {

            $userText .= 'שם: ' . $user->user_firstname . ' ' . $user->user_lastname . PHP_EOL .

                'מזהה לקוח : ' . $user->id;

        }

        fputcsv($output, [

            $subscription['id'],

            $userText,

            $subscription['email'],

            get_the_title($subscription['variation_id']),

            date('d/m/Y H:i:s', strtotime($subscription['created_at'])),

            (empty($subscription['back_to_stock'])) ? '' : (date('d/m/Y H:i:s', strtotime($subscription['back_to_stock']))),

        ]);

    }

} catch (Error $ex) {

    echo $ex->getMessage();

}

?>

