jQuery(document).ready(function() {
   let $ = jQuery;

});


function back_in_stock_subscribe(productId) {
    console.log(productId);
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let clientEmail = $('.back_in_stock_email').val();
    if (!clientEmail || !re.test(clientEmail)) {
        $('.bin-response').text('נא להזין כתובת אימייל תקינה');
        return;
    }
    $.ajax({
        type: 'post',
        url: ajax_url,
        data: {
            action: 'gt_back_in_stock_subscribe',
            productId: productId,
            email: clientEmail
        },
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            if (!response) {
                $('.bin-response').text('חלה שגיאה בהרשמה לחזרה למלאי');
            } else {
                $('.bin-response').text('תודה שנרשמת. נעדכן אותך כשהמוצר יחזור למלאי');
            }
        },
    });
}
