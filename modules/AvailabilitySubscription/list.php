<?php
$subscriptions = get_query_var('subscriptions');
?>
<h1>
    כל הרשומים למוצרים שאזלו מהמלאי
</h1>
<table>
    <tr>
        <td>
            <b>#</b>
        </td>
        <td>
            <b>מזהה לקוח</b>
        </td>
        <td>
            <b>כתובת אימייל</b>
        </td>
        <td>
            <b>מוצר</b>
        </td>
        <td>
            תאריך הרשמה
        </td>
        <td>
            תאריך חזרה למלאי
        </td>
    </tr>
    <?php
        foreach ($subscriptions as $subscription) {
            $user = get_user_by_email($subscription['email']);
            ?>
            <tr>
                <td>
                    <?= $subscription['id']; ?>
                </td>
                <td>
                    <?php
                        if ($user) { ?>
                            שם: <?= $user->user_firstname . ' ' . $user->user_lastname ?><br/>
                    כתובת אימייל : <?= $user->user_email ?><br>
                            מזהה לקוח:
                            <a href="<?= $user->user_url ?>"><?= $user->id ?></a>
                        <?php } ?>
                </td>
                <td>
                    <?php echo $subscription['email']; ?>
                </td>
                <td>
                    <a href="<?= get_permalink($subscription['variation_id']) ?>"><?= get_the_title($subscription['variation_id']); ?></a>
                </td>
                <td>
                    <?= date('d/m/Y H:i:s', strtotime($subscription['created_at'])); ?>
                </td>
                <td>
                    <?php
                    if ($subscription['back_to_stock']) {
                        echo date('d/m/Y H:i:s', strtotime($subscription['back_to_stock']));
                    }
                    ?>
                </td>
            </tr>
        <?php }
    ?>
</table>
<a href="<?= get_template_directory_uri() . '/modules/AvailabilitySubscription/export.php' ?>">ייצוא ל CSV</a>
