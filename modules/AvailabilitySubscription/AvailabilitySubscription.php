<?php
namespace Getraffic;

class AvailabilitySubscription extends PluginBase implements ConfigurableModule
{
    const SETTINGS_PAGE_ID = 'gt-bin-settings-page';
    private $settings;
    private $logger;

    public function __construct()
    {
        self::install();

        $this->registerFrontendCall('gt_back_in_stock_subscribe', 'subscribe');
        $this->interceptAvailabilityHtml();
        $this->settings = get_option(self::SETTINGS_PAGE_ID);

        add_action('wp_enqueue_scripts', [$this, 'enqueueScript']);
        add_action('wp_insert_post', [$this, 'productChanged'], 99, 3);
        add_action('woocommerce_update_product_variation', [$this, 'productChanged'], 99, 1);
        add_action('woocommerce_update_product', [$this, 'productChanged'], 99, 1);

        $this->logger = new Logger('/var/log/getraffic/back-in-stock.log');
    }

    private static function renderList()
    {
        ob_start();
        set_query_var('subscriptions', self::getAllSubscriptions());
        get_template_part('modules/AvailabilitySubscription/list');
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    private function notifySubscriber($email, $variationId)
    {
        global $wpdb;
        $product = wc_get_product($variationId);

        $subject = $this->settings['subject'];

        ob_start();
        set_query_var('queriedProduct', $product);
        set_query_var('emailSubject', $subject);
        get_template_part('modules/AvailabilitySubscription/email');
        $html = ob_get_contents();
        ob_end_clean();
        $headers = ['Content-Type: text/html; charset=UTF-8'];
        $wcMail = new \WC_Email();
        $message = apply_filters('woocommerce_mail_content', $wcMail->style_inline($html));
        wp_mail($email, $subject, $message, $headers);

        $wpdb->update(
            self::getTableName(),
            [
                'back_to_stock' => date('Y-m-d H:i:s')
            ],
            [
                'email'        => $email,
                'variation_id' => $variationId,
            ]
        );
    }

    public function enqueueScript() {
        wp_enqueue_script(
            'back-in-stock',
            get_template_directory_uri() . '/modules/AvailabilitySubscription/script.js'
        );

        wp_enqueue_style(
            'back-in-stock',
            get_template_directory_uri() . '/modules/AvailabilitySubscription/style.css'
        );
    }

    public function subscribe() {
        global $wpdb;
        $table = self::getTableName();
        $productId = $_POST['productId'];
        $email     = $_POST['email'];

        $results = $wpdb->get_var("SELECT count(*) FROM $table WHERE variation_id = $productId AND email = '$email'");

        if ($results) {
            echo $results;
            exit;
        } else {
            $updateId = $wpdb->insert(self::getTableName(), [
                'variation_id' => $productId,
                'email'        => $email,
                'created_at'   => date('Y-m-d H:i:s')
            ]);

            echo $updateId;
            exit;
        }
    }

    public static function getSubscribersForVariationId($variationId) {
        global $wpdb;
        $table = self::getTableName();
        return $wpdb->get_results("SELECT email FROM $table WHERE variation_id = $variationId AND back_to_stock is null", 'ARRAY_A');
    }

    public static function getAllSubscriptions() {
        global $wpdb;
        $table = self::getTableName();
        return $wpdb->get_results("SELECT * FROM $table", 'ARRAY_A');
    }

    public function productChanged($id) {
        $product = wc_get_product($id);

        if ($product instanceof \WC_Product_Variable) {
            foreach ($product->get_available_variations() as $variation) {
                $variationId = $variation['variation_id'];
                $inStock     = $variation['is_in_stock'];

                if ($variationId && $inStock) {
                    $subscribers = self::getSubscribersForVariationId($variationId);
                    if (count($subscribers) > 0) {
                        $this->logger->log('variation '. $variationId .' back to stock. stock is :' . $inStock . ' notifying ' . count($subscribers) . 'subscribers');
                    }
                    foreach ($subscribers as $subscriber) {
                        $this->notifySubscriber($subscriber['email'], $variationId);
                    }
                }
            }
        } else if ($product instanceof \WC_Product_Simple) {
            if ($product->is_in_stock()) {
                $subscribers = self::getSubscribersForVariationId($id);
                if (count($subscribers) > 0) {
                    $this->logger->log('product '. $id .' back to stock. notifying ' . count($subscribers) . 'subscribers');
                }
                foreach ($subscribers as $subscriber) {
                    $this->notifySubscriber($subscriber['email'], $id);
                }
            }
        }
    }

    function getTextDomain()
    {
        return '';
    }

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    function getCustomPostTypes()
    {
        return [];
    }

    /**
     * @return array
     */
    static function getInstallSqlSequences()
    {
        return [
            self::getCreateSubscriptionsTableSQL()
        ];
    }

    public static function getTableName() {
        global $wpdb;

        return $wpdb->prefix . 'gt_back_in_stock';
    }

    /**
     * @return string
     */
    public static function getCreateSubscriptionsTableSQL()
    {
        $table = self::getTableName();
        $sql   = "CREATE TABLE " . $table . " (
                    id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
                    created_at DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                    variation_id BIGINT(20) NOT NULL,
                    email VARCHAR(255) NOT NULL,
                    back_to_stock DATETIME DEFAULT NULL,
                    PRIMARY KEY (id));";

        return $sql;
    }

    private function interceptAvailabilityHtml()
    {
        add_filter( 'woocommerce_get_stock_html', function ( $html, $product ){
            if (strpos($html, 'out-of-stock') !== false) {
                $placeholder = isset($this->settings['button-placeholder']) ? $this->settings['button-placeholder'] : '';
                $buttonText  = isset($this->settings['button-text']) ? $this->settings['button-text'] : '';
                $html .= '
                <p class="oos-desc">
                     מתנצלים, המוצר שבחרת אזל. רוצה שנעדכן אותך כשיחזור למלאי?
                </p><br>
                <div class="back_in_stock_subscribe">
                    <input class="back_in_stock_email" type="email" placeholder="'. $placeholder .'">
                    <a onclick="back_in_stock_subscribe(' . $product->get_id() . '); return false;" href="#">
                        ' . $buttonText . '                    
                    </a>                
                </div>
                <div class="bin-response"></div>
            ';
            }

            return $html;
        }, 10, 2 );
    }

    public static function getAvailabilityHtml($product) {
        $settings = get_option(self::SETTINGS_PAGE_ID);
        $placeholder = isset($settings['button-placeholder']) ? $settings['button-placeholder'] : '';
        $buttonText  = isset($settings['button-text']) ? $settings['button-text'] : '';

        return '<div class="back_in_stock_subscribe">
                    <input class="back_in_stock_email" type="email" placeholder="'. $placeholder .'">
                    <a onclick="back_in_stock_subscribe(' . $product->get_id() . '); return false;" href="#">
                        ' . $buttonText . '                    
                    </a>                
                </div>
                <div class="bin-response"></div>';
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('back_in_stock-settings-section')
            ->setTitle('Back In Stock Properties')
            ->setSectionInfo('Back In Stock Properties')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('button-text')
                        ->setTitle('Button Text'),
                    (new AdminSettingsField())
                        ->setName('button-placeholder')
                        ->setTitle('Button Placeholder'),
                    (new AdminSettingsField())
                        ->setName('subject')
                        ->setTitle('Email Subject'),
                    (new AdminSettingsField())
                        ->setType('custom')
                        ->setMarkup(self::renderList())
                        ->setTitle('List'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'Back In Stock',
            'Back In Stock',
            'menu-settings-back-in-stock',
            [$section]);
    }
}
