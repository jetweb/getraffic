<?php
$product = get_query_var('queriedProduct');
$subject = get_query_var('emailSubject');
(new WC_Emails())->email_header($subject);
?>

    <h1>
        המוצר
        <?php
        if ($product instanceof WC_Product) {
            echo $product->get_title();
        }
        ?>
        חזר למלאי!
    </h1>
    <a href="<?php echo get_permalink($product->get_id()); ?>">לחץ כאן לצפייה במוצר</a>

<?php (new WC_Emails())->email_footer(); ?>
