<?php

namespace Getraffic;

class Seo extends PluginBase implements ConfigurableModule
{
    public function __construct()
    {
        $this->AddToHeader();
    }


    private function AddToHeader(){

		add_action('wp_head', function ()
		{

			global $post;
			setup_postdata($post);

			set_query_var('siteName', get_bloginfo());
			set_query_var('siteUrl', get_bloginfo('url'));
			  if ( is_product() )
			  {
					$product = wc_get_product($post->ID);
					set_query_var('productName', $product->get_name());
					set_query_var('productPrice', $product->get_price());
					set_query_var('productExcerpt', $product->get_description());
					set_query_var('productImage', wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full') );
					set_query_var('productUrl', get_permalink( $product->get_id() ) );
			  }
			get_template_part('templates/header/seo');
		}
		);

	}

    public static function getSettingsPage()
    {
    }

    function getTextDomain()
    {
        return '';
    }

    function getCustomPostTypes()
    {
        return [];
    }

}

