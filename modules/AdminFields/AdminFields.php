<?php
namespace Getraffic;

class AdminFields extends PluginBase implements ConfigurableModule
{

    public function __construct()
    {
        $this->showFields();
        $this->showFieldsValues();
        $this->showColumns();
        $this->showColumnValues();
        $this->saveCustomCheckout();
        $this->saveDiscountInSession();
  }


     private function saveCustomCheckout()
	{

		add_action( 'woocommerce_checkout_update_order_meta', 
		function ( $order_id ) {

			$cart_rule_uid = WC()->session->get( 'cart_rule_uid' ) ? WC()->session->get( 'cart_rule_uid' ) :'';

			if (!empty($cart_rule_uid)) {
				
				update_post_meta($order_id, 'cart_rule_uid', $cart_rule_uid );
				$rp_wcdpd_settings = get_option('rp_wcdpd_settings');
				foreach ($rp_wcdpd_settings as $rp) {
					foreach ($rp['product_pricing'] as $r) {
						if ($r['uid'] == $cart_rule_uid) {
							update_post_meta($order_id, 'cart_rule_name', $r['note'] );
						}
					}
				}

			}

		});
	 
	}
	
     private function saveDiscountInSession(){
		add_action('rp_wcdpd_product_pricing_rule_applied_to_cart', function ($rule_uid, $data)
			{
				$rules=array($rule_uid);
				$rule_id=WC()->session->get( 'cart_rule_uid' );
				if (!empty($rule_uid)) {
					$rules[]=$rule_uid;
				}
				//print_r($rules);
				WC()->session->set( 'cart_rule_uid' , $rule_uid );
				return($rules);
				
			}
			, 10, 2);

	 }
	
	
	private function showColumns()
	{
	add_filter('manage_edit-shop_order_columns',function ( $columns ) {
	    $new_array = array();
        $columns['total_orders'] = 'סה"כ הזמנות ללקוח';
        $columns['pushed_to_gtm'] = 'נספר ב-GTM';

	    foreach ($columns as $key => $title) {
	        if ($key == 'billing_address') {

	            $new_array['order_items'] = __('נקנה', 'woocommerce');
	        }

	        $new_array[$key] = $title;
	    }
	    return $new_array;
	});
	}

   private function showColumnValues()
	{

	add_action('manage_shop_order_posts_custom_column', function ($column) {
		
	    global $post, $woocommerce, $the_order;
	    switch ($column) {

        case 'order_number':
				$order    = wc_get_order( $post->ID );
				$cart_rule_uid = 	 get_post_meta($order->get_id(), 'cart_rule_uid', true);
				if (!empty($cart_rule_uid)) {
					$cart_rule_name = 	 get_post_meta($order->get_id(), 'cart_rule_name', true);
					echo'<p><span>במסגרת מבצע: '.$cart_rule_name.'</span></p>';
				}
			break;
        case 'total_orders':
				$order    = wc_get_order( $post->ID );
				if (function_exists ( 'wc_seq_order_number_pro' )){
					$order_id = $post->ID;
				} else {
					$order_id = $order->get_order_number();
				}
				//print_r($order_id);
			echo wc_get_customer_order_count($order->customer_id) ;
			break;
            case 'pushed_to_gtm':
                $order    = wc_get_order( $post->ID );
                $pushed_to_gtm = get_post_meta($order->get_id(), 'pushed_to_gtm', true);
                echo $pushed_to_gtm == 1 ? "כן" : "לא";
                break;

			case 'order_items':
	            $terms = $the_order->get_items();

				echo '<a href="#" class="show_order_items">' . apply_filters( 'woocommerce_admin_order_item_count', sprintf( _n( '%d item', '%d items', $the_order->get_item_count(), 'woocommerce' ), $the_order->get_item_count() ), $the_order ) . '</a>';

					if ( sizeof( $the_order->get_items() ) > 0 ) {

						echo '<table class="order_items" cellspacing="0">';

						foreach ( $the_order->get_items() as $item ) {
							$product        = apply_filters( 'woocommerce_order_item_product', $item->get_product(), $item );
						//	$item_meta      = new WC_Order_Item_Meta( $item, $product );
						//	$item_meta_html = $item_meta->display( true, true );
							?>
							<tr class="<?php echo apply_filters( 'woocommerce_admin_order_item_class', '', $item, $the_order ); ?>">
								<td class="qty"><?php echo esc_html( $item->get_quantity() ); ?></td>
								<td class="name">
									<?php  if ( $product ) : ?>
										<?php echo ( wc_product_sku_enabled() && $product->get_sku() ) ? $product->get_sku() . ' - ' : ''; ?><a href="<?php echo get_edit_post_link( $product->get_id() ); ?>"><?php echo apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ); ?></a>
									<?php else : ?>
										<?php echo apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ); ?>
									<?php endif; ?>
									<?php if ( ! empty( $item_meta_html ) ) : ?>
										<?php echo wc_help_tip( $item_meta_html ); ?>
									<?php endif; ?>
								</td>
							</tr>
							<?php
						}
						echo '</table>';
					} else echo '&ndash;';
				break;
			}
		
	}, 10, 2);


	}


   private function showFieldsValues()
    {
		add_action('woocommerce_admin_order_item_values',function ($_product, $item, $item_id = null) {
	global $post, $sitepress;
	$value='';
	//echo"<pre>";print_r($_product->get_type());
 if(  is_a( $_product, 'WC_Product' ) ) {
	$value = get_post_meta($_product->post->ID, 'warehouse_location', 1);
    echo '<td>' . $value  . '</td>';

if( $_product->is_type( 'simple' ) ){
//	echo'simple product';
  
	$value = get_post_meta($_product->post->ID, '_regular_price', 1);
  
} elseif( $_product->is_type( 'variation' ) ){
//	echo'variable product';
 // print_r($_product);
try {
	$value = get_post_meta($_product->get_variation_id(), '_regular_price', 1);
} catch(Exception $e) {}

} else {
$value='';

}
//	$value = $_product->get_price() ? $_product->get_price() : '';
	if($value) {
    echo '<td><span class="woocommerce-Price-currencySymbol">₪</span>' . $value  . '</td>';
	} else {
    echo '<td></td>';
	};
 }
			
			
			
			
			
		}, 10, 3);


	}

    private function showFields()
    {
        add_filter( 'woocommerce_admin_order_item_headers', function (  ){
    // set the column name
    $column_name = 'מקום במחסן';

    // display the column name
    echo '<th class="item" style="min-width:200px;">' . $column_name . '</th>';
    $column_name = 'מחיר מקורי';

    // display the column name
    echo '<th class="item" style="min-width:200px;">' . $column_name . '</th>';

        }, 10, 2 );
    }
    public static function getSettingsPage()
    {
	}

    function getTextDomain()
    {
        return '';
    }
    function getCustomPostTypes()
    {
        return [];
    }



}