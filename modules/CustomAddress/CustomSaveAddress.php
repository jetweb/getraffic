<?php

namespace Getraffic;

use WC_Customer;
use WC_Data_Exception;
use WC_Validation;

class CustomSaveAddress {
    public static function save_address() {
        global $wp;

        $nonce_value = wc_get_var( $_REQUEST['woocommerce-edit-address-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) ); // @codingStandardsIgnoreLine.

        if ( ! wp_verify_nonce( $nonce_value, 'woocommerce-edit_address' ) ) {
            return;
        }

        if ( empty( $_POST['action'] ) || 'edit_address' !== $_POST['action'] ) {
            return;
        }

        wc_nocache_headers();

        $user_id = get_current_user_id();

        if ( $user_id <= 0 ) {
            return;
        }

        $customer = new WC_Customer( $user_id );

        if ( ! $customer ) {
            return;
        }

        $load_address = isset( $wp->query_vars['edit-address'] ) ? wc_edit_address_i18n( sanitize_title( $wp->query_vars['edit-address'] ), true ) : 'billing';

        if ( ! isset( $_POST[ $load_address . '_country' ] ) ) {
            return;
        }

        $address = WC()->countries->get_address_fields( wc_clean( wp_unslash( $_POST[ $load_address . '_country' ] ) ), $load_address . '_' );

        foreach ( $address as $key => $field ) {
            if ( ! isset( $field['type'] ) ) {
                $field['type'] = 'text';
            }

            // Get Value.
            if ( 'checkbox' === $field['type'] ) {
                $value = (int) isset( $_POST[ $key ] );
            } else {
                $value = isset( $_POST[ $key ] ) ? wc_clean( wp_unslash( $_POST[ $key ] ) ) : '';
            }

            // Hook to allow modification of value.
            $value = apply_filters( 'woocommerce_process_myaccount_field_' . $key, $value );

            // Validation: Required fields.
            if ( ! empty( $field['required'] ) && empty( $value ) ) {
                /* translators: %s: Field name. */
                wc_add_notice( sprintf( __( '%s is a required field.', 'woocommerce' ), $field['label'] ), 'error' );
            }

            if ( ! empty( $value ) ) {
                // Validation and formatting rules.
                if ( ! empty( $field['validate'] ) && is_array( $field['validate'] ) ) {
                    foreach ( $field['validate'] as $rule ) {
                        switch ( $rule ) {
                            case 'postcode':
                                $country = wc_clean( wp_unslash( $_POST[ $load_address . '_country' ] ) );
                                $value   = wc_format_postcode( $value, $country );

                                if ( '' !== $value && ! WC_Validation::is_postcode( $value, $country ) ) {
                                    switch ( $country ) {
                                        case 'IE':
                                            $postcode_validation_notice = __( 'Please enter a valid Eircode.', 'woocommerce' );
                                            break;
                                        default:
                                            $postcode_validation_notice = __( 'Please enter a valid postcode / ZIP.', 'woocommerce' );
                                    }
                                    wc_add_notice( $postcode_validation_notice, 'error' );
                                }
                                break;
                            case 'phone':
                                if ( '' !== $value && ! WC_Validation::is_phone( $value ) ) {
                                    /* translators: %s: Phone number. */
                                    wc_add_notice( sprintf( __( '%s is not a valid phone number.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
                                }
                                break;
                            case 'email':
                                $value = strtolower( $value );

                                if ( ! is_email( $value ) ) {
                                    /* translators: %s: Email address. */
                                    wc_add_notice( sprintf( __( '%s is not a valid email address.', 'woocommerce' ), '<strong>' . $field['label'] . '</strong>' ), 'error' );
                                }
                                break;
                        }
                    }
                }
            }

            try {
                // Set prop in customer object.
                if ( is_callable( array( $customer, "set_$key" ) ) ) {
                    $customer->{"set_$key"}( $value );
                } else {
                    $customer->update_meta_data( $key, $value );
                }
            } catch ( WC_Data_Exception $e ) {
                // Set notices. Ignore invalid billing email, since is already validated.
                if ( 'customer_invalid_billing_email' !== $e->getErrorCode() ) {
                    wc_add_notice( $e->getMessage(), 'error' );
                }
            }
        }

        /**
         * Hook: woocommerce_after_save_address_validation.
         *
         * Allow developers to add custom validation logic and throw an error to prevent save.
         *
         * @param int         $user_id User ID being saved.
         * @param string      $load_address Type of address e.g. billing or shipping.
         * @param array       $address The address fields.
         * @param WC_Customer $customer The customer object being saved. @since 3.6.0
         */
        do_action( 'woocommerce_after_save_address_validation', $user_id, $load_address, $address, $customer );

        if ( 0 < wc_notice_count( 'error' ) ) {
            return;
        }

        $customer->save();

        $text = $load_address === 'billing' ? 'פרטייך עודכנו בהצלחה' : '';

        wc_add_notice( $text );

        do_action( 'woocommerce_customer_save_address', $user_id, $load_address );
    }
}
