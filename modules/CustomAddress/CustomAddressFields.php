<?php

namespace Getraffic;

class CustomAddressFields
{
    public $fields = [
        'first_name' => [
            'placeholder' => 'שם פרטי',
            'label'       => 'שם פרטי',
            'required'    => true,
            'class'       => ['form-row-first'],
            'clear'       => true,
        ],
        'last_name' => [
            'placeholder' => 'שם משפחה',
            'label'       => 'שם משפחה',
            'required'    => true,
            'class'       => ['form-row-last'],
            'clear'       => true,
        ],
        'street' => [
            'placeholder' => 'רחוב',
            'label'       => 'רחוב',
            'required'    => false,
            'class'       => ['form-row-first'],
            'clear'       => true,
        ],
        'house' => [
            'placeholder' => 'מספר בית',
            'label'       => 'מספר בית',
            'required'    => false,
            'class'       => ['form-row-last'],
            'clear'       => true
        ],
        'appartment' => [
            'placeholder' => 'דירה',
            'label'       => 'דירה',
            'required'    => false,
            'class'       => ['form-row-first'],
            'clear'       => true
        ],
        'floor' => [
            'placeholder' => 'קומה',
            'label'       => 'קומה',
            'required'    => false,
            'class'       => ['form-row-last'],
            'clear'       => true
        ],
        'city' => [
            'placeholder' => 'עיר',
            'label'       => 'עיר',
            'required'    => true,
            'class'       => ['form-row-first'],
            'clear'       => true,
        ],
        'phone' => [
            'placeholder' => 'טלפון',
            'label'       => 'טלפון',
            'required'    => false,
            'class'       => ['form-row-last'],
            'clear'       => true,
            'validate'    => ['phone']
        ],
        'country' => [ // This field is mandatory
            'type'     => 'country',
            'required' => true,
            'label'    => 'ארץ',
            'class'    => ['form-row-last, hidden'],
        ],
    ];

    public function __construct()
    {
        add_filter('woocommerce_shipping_fields', function () {
            return $this->get_address_fields_array('shipping');
        });

        add_filter('woocommerce_billing_fields', function () {
            return $this->get_address_fields_array('billing');
        });
    }

    public function setFields($fields) {
        $this->fields = $fields;
    }

    function get_address_fields_array($type) {
        $retVal = [];
        $priority = 0;
        foreach ($this->fields as $fieldName => $field) {
            $priority += 10;
            $field['priority'] = $priority;
            $retVal[$type . '_' . $fieldName] = $field;
        }

        return $retVal;
    }

}
