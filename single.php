<?php
/**
 * Template Name: Single post
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

get_header(); ?>
<?php
$recent_posts = wp_get_recent_posts( ['numberposts' => 3], ARRAY_A );
?>
<div class="container" id="primary">
    <div class="content">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                <h1>מגזין</h1>
                <div class="row margin-bottom">
                    <div class="col-sm-4 pr-4 pl-4">
                        <div class="post-sidebar">
                            <h1 class="title more-articles">מאמרים נוספים</h1>
                            <?php
                            foreach ($recent_posts as $currentPost) {
                                $b = 0;
                                ?>
                                <a href="<?php echo get_permalink($currentPost['ID']); ?>"><div class="post-teaser">
                                    <div class="post-title">
                                        <?php echo $currentPost['post_title']; ?>
                                    </div>
                                    <div class="post-image">
                                        <img src="<?php echo get_the_post_thumbnail_url($currentPost['ID']); ?>" />
                                    </div>
                                </div></a>
                                <?php
                            }
                            ?>
                            <?php if (is_active_sidebar('sidebar_1')) : ?>
                                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                                    <?php dynamic_sidebar('sidebar_1'); ?>
                                </div><!-- #primary-sidebar -->
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="post-content">
                            <div class="featured-img-wrapper">
                                <img class="featured" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full-size'); ?>"/>
                            </div>
                            <h2><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                        </div>
                        <div class="post-nav">
                            <?php
                             previous_post_link('%link', '&lt; לכתבה הקודמת');
                            ?>
                            <div class="right-auto">
                            <?php
                            next_post_link('%link', 'לכתבה הבאה &gt;');
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endwhile;
        }
        ?>
    </div>
</div>
<?php get_footer(); ?>
