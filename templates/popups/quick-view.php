<?php
$product     = get_query_var('qvProduct');
$cartItemKey = get_query_var('cartItemKey');
?>
<div class="modal fade quick-view-modal-wrapper" id="qvModal<?php echo $cartItemKey == null ? $product->get_ID() : $cartItemKey; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close qv-close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php GT::printQuickView($product, $cartItemKey); ?>
        </div>
    </div>
</div>
