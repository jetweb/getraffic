<div class="login-popup">
    <input id="loginUser" type="text" name="username" placeholder="שם משתמש" />
    <input id="loginPass" type="password" name="pass" placeholder="סיסמא" />
    <div class="login-error login-message"></div>
    <div class="login-success login-message"></div>
    <a href="" class="tony-button sharp-corners login-popup-btn">
        התחברות
    </a>
    <a href="<?php echo wp_lostpassword_url(); ?>" class="forgot-pass">
        שכחתי סיסמא
    </a>
    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
</div>

<script>
    $(document).ready(function () {
        $('#loginUser, #loginPass').keypress(function(e) {
            if ( e.keyCode === 13 ) {
                e.preventDefault();
                triggerLogin();
                return false;
            }
        });

        $('.login-popup-trigger').click(function() {
            $('.login-popup').toggle();
            return false;
        });

        $('a.login-popup-btn').click(function(e) {
            e.preventDefault();
            triggerLogin();
            return false;
        });

        $(document.body).on('login_success', function(e, message) {
            $('.login-success').text(message);
        });

        $(document.body).on('login_error', function(e, message) {
            $('.login-error').text(message);
        });
    });

    function triggerLogin() {
        $('.login-message').text('');
        logUserIn(
            $('#loginUser').val(),
            $('#loginPass').val(),
            $('#security').val(),
            true
        );
    }
</script>