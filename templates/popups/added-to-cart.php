<!-- Modal -->
<div class="modal fade gtModal" id="addedToCartModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 hidden-sm"><?php GT::getCurrentProductMainImageHTML(); ?></div>
                    <div class="col-sm-7">
                        <div class="wrapper">
                            <h1 role="presentation">נוסף לסל</h1><br/>
                            <div class="buttons-wrapper">
                                <?php woocommerce_template_single_title(); ?>
                                <a href="#" class="button" data-dismiss="modal">< להמשך קניה</a>
                                <a href="/cart" class="button">לעגלה</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-bg-blue"></div>
    </div>
</div>