<?php $fields = gt_get_field('points_and_club_settings', 'option'); ?>
<div class="modal fade" id="joinUsModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="showStep('coupons');">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="align-center">
                    <h1><?= $fields['popup_title']; ?></h1>
                    <h2><?= $fields['popup_subtitle']; ?></h2>
                    <p>
                        <?= $fields['popup_text']; ?></p>
                    <div class="input-bdate">
                        <?php GT::printDaySelect(); ?>
                        <?php GT::printMonthSelect(); ?>
                        <?php GT::printYearSelect(); ?>
                    </div>

                <div class="align-center">
                    <span class="success"><?= $fields['join_success']; ?></span>
                    <a href="#" class="join-us-button">
                        <?= $fields['popup_button_text']; ?>
                    </a>

                    <a href="#" class="no-thanks" onclick="showStep('coupons'); $('#joinUsModal').modal('hide');">
                        <?= $fields['another_time']; ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>