<?php $fields = gt_get_field('newsletter', 'option'); ?>
<div class="f-newsletter">
    <h4><?= $fields['title'] ?></h4>
    <form class="newsletter-form">
        <input class="newsletter" name="email" id="email" placeholder="<?= $fields['placeholder'] ?>"/><br/>
        <label for="approve"><input type="checkbox" name="approve" id="approve" checked/><span><?= $fields['text_terms'] ?></span></label>
        <a class="newsletter-submit">
            <?= $fields['button_text'] ?>
        </a>
        <div class="newsletter-response"></div>
    </form>
</div>
