<div class="container">
    <div class="footer-widgets">
        <div class="f-widget widget-right">
            <div class="row full-width">
                <div class="col-sm-4">
                    <?php wp_nav_menu([
                        'theme_location' => 'footer-1',
                        'menu_class'     => 'footer-menu',
                        'fallback_cb'    => false,
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <?php wp_nav_menu([
                        'theme_location' => 'footer-2',
                        'menu_class'     => 'footer-menu',
                        'fallback_cb'    => false,
                    ]); ?>
                </div>
                <div class="col-sm-4">
                    <?php wp_nav_menu([
                        'theme_location' => 'footer-3',
                        'menu_class'     => 'footer-menu last',
                        'fallback_cb'    => false,
                    ]); ?>
                </div>
            </div>
        </div>
        <div class="f-widget widget-center">

        </div>
        <div class="f-widget widget-left">
            <div class="row full-width">
                <div class="col-sm-7 align-right newsletter">

                </div>
                <div class="col-sm-5 align-right customer-service">

                </div>
            </div>
        </div>
    </div>
</div>
