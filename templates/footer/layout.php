<div class="qv-container"></div>
<footer class="footer">
    <?php get_template_part('templates/footer/widgets'); ?>
    <?php wp_footer(); ?>
</footer>

<?php get_template_part('templates/triggers/added-to-cart'); ?>
