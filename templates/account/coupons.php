<?php

GT::enqueueAsset('coupons-css', '/assets/src/scss/coupons.css', []);
$coupons = GT::getAllCouponsForCurrentUser();
if (empty($coupons)) {
    get_template_part('templates/account/coupons-empty');
    return;
}

$discounts = new WC_Discounts();
foreach ($coupons as $coupon) {
    if ($coupon instanceof WC_Coupon) {
        $valid = $discounts->is_coupon_valid($coupon);
        $invalidMessage = '';
        if (is_wp_error($valid)) {
            $invalidMessage = $valid->get_error_message();
            continue;
        }

        $couponFields = gt_get_field('coupon_design', $coupon->get_id());
        ?>
        <div class="coupon-item <?= $couponFields['css_class'] ?>">
            <div class="single-coupon"
                 style="background-color: <?= $couponFields['background-color']; ?>; background-image: url('<?= $couponFields['background-image']; ?>');">
                <div class="single-coupon-content">
                    <div class="coupon-title"><?php
                        if (!empty($couponFields['title'])) {
                            echo $couponFields['title'];
                        } else {
                            echo $coupon->get_amount();
                            if ($coupon->get_discount_type() == 'percent') {
                                echo '%';
                            } else {
                                echo '₪';
                            }

                            echo ' הנחה';
                        }
                        ?></div>
                    <div class="coupon-subtitle"><?php
                        if (!empty($couponFields['subtitle'])) {
                            echo $couponFields['subtitle'];
                        } else {
                            $expires = $coupon->get_date_expires();
                            if ($expires) {
                                echo 'בתוקף עד ' . $expires->date('d.m.Y');
                            }
                        }
                        ?></div>
                    <div class="coupon-code"><?php
                        $code = $coupon->get_code();

                        if (empty($code)) {
                            $code = get_the_title($coupon->get_id());
                        }

                        echo str_replace('[coupon]', $code, $couponFields['code_text']);

                        ?></div>
                </div>
            </div>
            <div class="coupon-bottomtext"><span><?= $couponFields['bottom_text'] ?></span></div>
            <?php if ($invalidMessage) { ?>
                <div class="invalid-message"><?= $invalidMessage ?></div>
            <?php } ?>
        </div>
        <?php
    }
}
