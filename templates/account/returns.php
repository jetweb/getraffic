<div class="orders-to-return">
    <?php
    $customer_orders = wc_get_orders(
        apply_filters(
            'woocommerce_my_account_my_orders_query',
            array(
                'customer' => get_current_user_id(),
                'paginate' => false,
            )
        )
    );

    foreach ( $customer_orders as $customer_order ) :
        $order      = wc_get_order( $customer_order );
        $item_count = $order->get_item_count();
        ?>
        <div class="grey">
            <div class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order tm-orders">
                <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                    <?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
                        <?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

                    <?php elseif ( 'order-number' === $column_id ) : ?>

                        <div class="tm-order-item">
                            <div class="tm-order-title">מספר הזמנה</div>
                            <div class="tm-order-number"><?php echo $order->get_order_number(); ?></div>
                            <div class="tm-order-status"><span>תאריך: </span><time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>&nbsp;|&nbsp;
                                <span>סטטוס: </span><?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?></div>
                        </div>

                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="tm-order-details">
                <div colspan="5">
                    <?php do_action( 'woocommerce_view_order', $order->get_order_number() ); ?>
                </div>
                <p>
                    לאחר הגעת המוצר למשרדנו ובדיקתו ניצור עמכם קשר
                    ותישלח הודעה על הזיכוי לכתובת המייל המעודכנת בחשבונכם.
                </p>
                <a href="#" class="submit-return">
                    שליחה
                </a>
            </div>
        </div>
    <?php endforeach; ?>
</div>