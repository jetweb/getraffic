<?php global $product; ?>
<div class="single-product-points">
    <?php
    $fields = gt_get_field('points_and_club_settings', 'option');
    echo str_replace('[points]', GT::getPointsByProduct($product->get_id()), $fields['product_page_text'])
    ?>
</div>
