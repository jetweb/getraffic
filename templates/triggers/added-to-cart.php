<script>
    jQuery(document).ready(function () {
        let $ = jQuery;
        $(document).on('added_to_cart', function () {
            $('.modal').modal('hide');

            if ($('.cart-container').length) {
                return;
            }

            toggleMiniCart();
            $('.mini-cart-toggle')[0].scrollIntoView();
            $('.mini-cart-wrapper span.mini-cart-added').show();

            setTimeout(function() {
                toggleMiniCart('close');
                $('.mini-cart-wrapper span.mini-cart-added').hide(400);
            }, 6000);
        });
    });
</script>
