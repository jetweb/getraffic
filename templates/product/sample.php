<?php $sampleProduct = get_query_var('productToRender'); ?>
<a href="<?php echo $sampleProduct->get_permalink(); ?>"><?php echo GT::getProductMainImageHTML($sampleProduct); ?></a>
<div class="sample-title"><?php echo $sampleProduct->get_title(); ?></div>
<div class="sample-price"><?php echo wc_price($sampleProduct->get_price()); ?></div>
<div class="sample-cart">
    <?php
    $args = GT::getLoopAddToCartArguments($sampleProduct);

    echo apply_filters( 'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
        sprintf( '<a href="%s" data-quantity="%s" class="%s" %s>%s</a>',
            esc_url( $sampleProduct->add_to_cart_url() ),
            esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
            esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
            isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
            esc_html( $sampleProduct->add_to_cart_text() )
        ),
        $sampleProduct, $args );
    ?>
</div>

