<?php
$productId = get_query_var('product_id');
$activeImage = get_query_var('active_image');
$inactiveImage = get_query_var('inactive_image');
if (class_exists('YITH_WCWL_Shortcode')) {
    $isActive = YITH_WCWL()->is_product_in_wishlist($productId);
    ?>
    <a href="#" class="whishlist" onclick="<?php echo $isActive ? 'removeFromWishlist' : 'addToWishlist' ?>($(this)); return false;"
       data-active-src="<?php echo $activeImage; ?>"
       data-inactive-src="<?php echo $inactiveImage; ?>"
       data-product-id="<?php echo $productId; ?>"><img class="removeFromWish" src="<?php echo $isActive ? $activeImage : $inactiveImage; ?>"/></a>
    <?php
}
?>