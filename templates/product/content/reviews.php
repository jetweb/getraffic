<div class="product-reviews">
    <div class="container">
        <h1>reviews</h1>
        <?php
            global $product;
        ?>
        <div class="star-rating average"><span style="width:<?php echo (round($product->get_average_rating()) / 5) * 100;?>%">דורג <strong class="rating"><?php echo $product->get_average_rating();?></strong> מתוך 5</span></div>

        <a class="tony-button-white add-comment">
            חווי דעתך על המוצר
        </a>

        <?php
        comments_template();
        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) {

        }
        ?>
    </div>
</div>


<script>
    $(document).ready(function() {
        $('a.add-comment').click(function() {
            $('#review_form').show(400);
            $('#review_form')[0].scrollIntoView({behavior: "smooth"});
        });
    });
</script>
