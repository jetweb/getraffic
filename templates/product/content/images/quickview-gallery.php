<?php
global $product;
$product = get_query_var('qvProduct');
$attachment_ids = $product->get_gallery_image_ids();

?>
<div class="qv-gallery-images">
    <div class="qv-gallery-image">
        <img src="<?php echo GT::getProductImageURL($product, 'gallery-square'); ?>" alt=""/>
    </div>
    <?php
    foreach ($attachment_ids as $attachment_id) {
        ?>
        <div class="qv-gallery-image">
            <img src="<?php echo wp_get_attachment_image_src($attachment_id, 'gallery-square')[0]; ?>" alt=""/>
        </div>
        <?php
    }
    ?>
</div>
