<div class="summary entry-summary">
    <?php
    global $product; ?>
    <div class="single-product-top">
        <div class="single-product-title"><?php woocommerce_template_single_title(); ?></div>
        <div class="single-product-stars"><?php GT::showProductAverageRating($product); ?></div>
    </div>
    <div class="mobile-gallery position-relative"></div>
    <div class="product-lite-text"><?php echo $product->get_description(); ?></div>
    <?php woocommerce_template_single_price(); ?>
    <div class="add-to-cart-form">
        <?php woocommerce_template_single_add_to_cart(); ?>
    </div>
    <div class="variation-error"></div>
    <?php get_template_part('templates/points/product-message'); ?>
    <?php
    if (class_exists('YITH_WCWL_Shortcode')) {
        echo YITH_WCWL_Shortcode::add_to_wishlist([]);
    }
    ?>
    <div class="clearfix"></div>
</div>
