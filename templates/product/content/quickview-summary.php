<div class="summary entry-summary">
    <?php
    global $product;
    $product = get_query_var('qvProduct');
    ?>
    <div class="quickview_single_title">
        <h1 class="product_title entry-title">
            <?= $product->get_title() ?>
        </h1>
    </div>
    <div class="quickview_average_rating"><?php GT::showProductAverageRating($product); ?></div>
    <div class="quickview_short_description"><?php echo $product->get_short_description(); ?></div>
    <div class="mobile-gallery position-relative"></div>
    <?php woocommerce_template_single_price(); ?>
    <div class="add-to-cart-form quickview-atc-form"><?php woocommerce_template_single_add_to_cart(); ?></div>
    <div class="variation-error"></div>

    <div class="clearfix"></div>
</div>
