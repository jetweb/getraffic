<?php
GT::enqueueAsset('similar-products', '/assets/src/scss/similar-products.css', [], false, true);

$title         = get_query_var('xsell_title');
$subtitle      = get_query_var('xsell_subtitle');
$xsellProducts = get_query_var('xsell_products');
$template      = get_query_var('xsell_template');
$elementID     = get_query_var('element_id');
$hasQuickview  = get_query_var('has_QV');
$slider_num = gt_get_field("gt_similar_products","option");
?>
<div class="container xsell-container">
    <?php if($title): ?>
        <h2 class="best-match"><?php echo $title; ?></h2>
    <?php endif; ?>

    <?php if($subtitle): ?>
        <h2 class="best-match"><?php echo $subtitle; ?></h2>
    <?php endif; ?>

    <div class="position-relative">
        <div id="<?php echo $elementID; ?>" class="xsell-products">
            <?php
            global $product;
            foreach ($xsellProducts as $xSell) {
                GT::renderProduct($xSell, true, $template, false, $hasQuickview, 'carousel-thumb');
            }
            ?>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        let $ = jQuery;

        slickInitializers['<?php echo $elementID; ?>'] =
            function initSlick() {
                let slides = <?php echo count($xsellProducts); ?>;
                initSlickCarousel($('#<?php echo $elementID; ?>'), true, true, slides, [
                    {
                        breakpoint: 5220,
                        settings: {
                            slidesToShow: Math.min(<?= $slider_num["min_size_5220"] ?>, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1920,
                        settings: {
                            slidesToShow: Math.min(<?= $slider_num["min_size_1920"] ?>, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1400,
                        settings: {
                            slidesToShow: Math.min(<?= $slider_num["min_size_1400"] ?>, slides),
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: <?= $slider_num["min_size_1200"] ?>,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: <?= $slider_num["min_size_991"] ?>,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 10,
                        settings: {
                            slidesToShow:<?= $slider_num["min_size_mob"] ?>,
                            slidesToScroll: 1
                        }
                    }
                ]);
            };

        slickInitializers['<?php echo $elementID; ?>']();
    });
</script>

