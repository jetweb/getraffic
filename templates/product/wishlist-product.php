<?php
$product          = get_query_var('productToRender');
$showControls     = get_query_var('showControls');
$thumb            = get_query_var('thumb');
$description      = get_query_var('description');
$idForWLandQV     = get_query_var('idForWLandQV');
$product_quantity = $product->get_stock_quantity();
$settingFields    = gt_get_field('account_wishlist', 'option');
?>
<div class="product-wrapper" data-product-id="<?php echo $product->get_id(); ?>">
    <div class="thumb-wrapper">
        <a href="<?php echo $product->get_permalink(); ?>" class="thumb-link">
            <img src="<?php echo $thumb; ?>" alt="" class="thumb">
        </a>
        <?php if ($showControls) { ?>
            <div class="prod-thumb-bottom">
                <?php GT::renderWishlistButton($idForWLandQV, img('trash.png'), img('heart_pink.png')); ?>
            </div>
        <?php } ?>

        <?php if (!$product->is_in_stock()) { ?>
            <div class="soldout"><img src="<?php echo img('exclamation.png'); ?>"><?php echo $settingFields['oos']; ?></div>
        <?php } ?>
        <?php if ($product_quantity && $product_quantity < 3) { ?>
            <div class="lowstock"><img src="<?php echo img('exclamation.png'); ?>"><?php echo $settingFields['almost_oos']; ?></div>
        <?php } ?>
    </div>
    <div class="product-info">
        <a href="<?php echo $product->get_permalink(); ?>"><h1 class="title"><?php echo $product->get_title(); ?></h1></a>
        <div class="product-price" data-title="<?php esc_attr_e('Price', 'woocommerce'); ?>">
            <?php echo $product->get_price_html(); ?>
        </div>
    </div>
    <div class="wishlist-add-to-cart">
        <?php woocommerce_template_single_add_to_cart(); ?>
    </div>
</div>

