<?php
$image = get_query_var('badgeImage');
if ($image) {
    ?>
    <img src="<?= $image ?>" class="product-badge badge-sale" style="background-color:<? echo get_query_var('badgeColor'); ?>" alt="">
    <?php return;
}
?>
<div class="product-badge badge-best-seller" style="background-color:<? echo get_query_var('badgeColor'); ?>"></div>
