<?php
$product      = get_query_var('productToRender');
$showControls = get_query_var('showControls');
$thumb        = get_query_var('thumb');
$thumbHover   = get_query_var('thumbHover');
$description  = get_query_var('description');
$idForWLandQV = get_query_var('idForWLandQV');
$variation    = get_query_var('variationDataArray');
?>

<div class="product-wrapper" data-product-id="<?php echo $product->get_id(); ?>" data-gtmClickPayload="<?= \Getraffic\GTM::getClickPayload($product); ?>">
    <div class="thumb-wrapper">
        <a href="<?php echo $product->get_permalink(); ?>">
            <img src="<?php echo $thumb; ?>" alt="" class="thumb" data-hover="<?php echo $thumbHover;?>" data-original-src="<?php echo $thumb; ?>">
        </a>
        <?php if ($showControls) { ?>
            <div class="prod-thumb-bottom">
                <?php GT::renderWishlistButton($idForWLandQV, img('heart_white_full.png'), img('heart_white.png')); ?>
                <?php if ($variation && $variation['attributes'] && $variation['image']) { ?>
                    <a href="#" onclick='return openQuickView(<?php echo $idForWLandQV; ?>, <?php echo json_encode($variation['attributes']); ?>, <?php echo json_encode($variation['image']['url']); ?>);' class="quickview">QUICKVIEW</a>
                <?php } else { ?>
                    <a href="#" onclick='return openQuickView(<?php echo $idForWLandQV; ?>);' class="quickview">QUICKVIEW</a>
                <?php } ?>
            </div>
        <?php } ?>
        <?php GT::printProductBadge($product); ?>
    </div>

    <div class="product-info">
        <a href="<?php echo $product->get_permalink(); ?>"><h1 class="title"><?php echo $product->get_title(); ?></h1></a>
        <h2 class="description"><?php echo $description; ?></h2>
        <?php GT::showProductAverageRating($product); ?>
        <?php echo $product->get_price_html(); ?>
    </div>
</div>

