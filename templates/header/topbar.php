<div class="topbar">
    <div class="container align-center padmenot topicons-container">
        <div class="topicons-right">
            <?php get_template_part('templates/header/topbar/search'); ?>
        </div>
        <?php get_template_part('templates/header/topbar/logo'); ?>
        <div class="topicons-left">
            <?php get_template_part('templates/header/topbar/account'); ?>
            <?php get_template_part('templates/header/topbar/wishlist'); ?>
            <?php get_template_part('templates/header/topbar/minicart'); ?>
        </div>
    </div>
    <?php get_template_part('templates/header/topbar/search-popup'); ?>
</div>


