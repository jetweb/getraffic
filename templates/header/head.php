<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Yaniv Tabibi">
    <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700|Varela+Round&amp;subset=hebrew" rel="stylesheet">
    <title><?php wp_title('');?></title>
    <?php wp_head(); ?>
</head>
