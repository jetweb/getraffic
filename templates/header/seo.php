<?php
if(false):
$siteName         = get_query_var('siteName');
$siteUrl         = get_query_var('siteUrl');
$productName         = get_query_var('productName');
$productPrice         = get_query_var('productPrice');
$productExcerpt         = get_query_var('productExcerpt');
$productImage         = get_query_var('productImage');
$productUrl         = get_query_var('productUrl');
?>
<script type="application/ld+json">
	{
	  "@context": "https://schema.org/",
	  "@type": "WebSite",
	  "name": "<?php echo $siteName?>",
	  "url": "<?php echo $siteUrl?>",
	  "potentialAction": {
		"@type": "SearchAction",
		"target": "{search_term_string}",
		"query-input": "required name=search_term_string"
	  }
	}
</script>

<?php
// Only on product pages
		  if ( is_product() )
		  {
			  ?>
			<script type="application/ld+json">
			  {
				"@context": "http://schema.org",
				"@type": "Product",
				  "name": "<?php echo $productName?>",
				  "image": "<?php echo $productImage[0]?>",
				   "description": "<?php echo $productExcerpt?>",
				    "brand": "<?php echo $siteName?>", 
				  "offers": {
					"@type": "Offer",
					"url": "<?php echo $productUrl?>",
					"priceCurrency": "ILS",
					"price": "<?php echo $productPrice?>",
					"availability": "https://schema.org/InStock",
					"itemCondition": "https://schema.org/NewCondition"
				  }
			}
			</script>
			<?php
		  }
endif;