<div class="topicon login">
    <a class="<?php echo is_user_logged_in() ? '' : 'login-popup-trigger'; ?>" href="<?php echo is_user_logged_in() ? wc_get_account_endpoint_url('dashboard') : wp_login_url(home_url()); ?>">
        <img class="topicon-image" src="<?php echo img('person.png'); ?>"/>
        <?php if (is_user_logged_in()) { ?>
            <span class="topicon-text">היי <?php echo wp_get_current_user()->display_name; ?><a class="logout-link" href="<?php echo wp_logout_url(home_url()); ?>">התנתק</a></span>
        <?php } else { ?>
            <span class="topicon-text login-link">התחבר</span>
        <?php } ?>
    </a>

    <?php get_template_part('templates/popups/login'); ?>
</div>