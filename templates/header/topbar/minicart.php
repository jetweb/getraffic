<div class="topicon minicart">
    <a class="mini-cart-toggle">
        <img class="topicon-image" src="<?php echo img('cart.png'); ?>"/>
        <span class="mini-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
        <span class="topicon-text">פריטים</span>
    </a>
    <div class="mini-cart-wrapper">
        <div class="mini-cart-handle">
            <a class="mini-cart-toggle"><img src="<?php echo img('close.jpg'); ?>" /></a>
        </div>
        <div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div>
    </div>
</div>