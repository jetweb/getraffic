<div class="topicon wishlisticon">
    <a href="<?php echo get_home_url(); ?>/wishlist">
        <img class="topicon-image" src="<?php echo img('heart.png'); ?>"/>
        <span class="topicon-text">WISHLIST</span>
        <span class="wishlist-count">
            <?php
            if (function_exists('yith_wcwl_count_all_products')) {
                echo yith_wcwl_count_all_products();
            }
            ?>
        </span>
    </a>
</div>