<div class="search-popup-wrap">
    <div class="search-popup">
        <div class="search-input">
            <input type="text" id="seacrhTerm" value="" class="search-value" title="search" placeholder="חיפוש"/>
        </div>
        <div class="results-popup-wrapper">
            <div class="results-popup">
                <!-- //fatch data from search.js line:22 -->
            </div>
        </div>
        <div class="x-wrapper">
             <div class="ws-x-btn"></div>
        </div>
      
    </div>
</div>

<?php GT::enqueueAsset('search-popup-css', '/assets/src/scss/search-popup.css', []); ?>