<div class="menubar-mobile">
    <?php get_template_part('templates/header/topbar/logo-mobile'); ?>
    <div class="hamburger hamburger--collapse">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
</div>
<div id="menu-mobile">
    <a class="mobile-menu-title">
        תפריט
    </a>
    <a href="" class="mobile-menu-close">
        <img src="<?php echo img('x.png'); ?>" alt="">
    </a>
    <div class="mobile-menu-back-button">
        <a href="" class="to-all">
        </a>
        <a href="" class="back">
            <img src="<?php echo img('arr_left.png'); ?>" alt="">
        </a>
        <div class="clearfix"></div>
    </div>
    <ul>
        <?php listMenuForMobile('header-menu-right'); ?>
    </ul>
    <div class="mobile-menu-icons">
        <?php
        $icons = gt_get_field('mobile_menu_icons', 'option');
        foreach ($icons as $icon) {
            ?>
            <a href="<?php echo $icon['link']; ?>">
                <img src="<?php echo $icon['icon']; ?>" alt="">
            </a>
            <?php
        }
        ?>
    </div>
    <?php listMobileBottomMenu(); ?>

    <?php
    /**
     * Recursive function to print mobile array
     *
     * @param $item
     * @param $map
     */
    function printMenuItem($item, $map, $parentId)
    {
        $itemIcon = gt_get_field('menu_item_icon', $item);
        $itemPluralName = gt_get_field('plural_item_name', $item);
        if (!empty($map[$item->ID])) {
            echo '<li class="has-children" data-item-id="' . $item->ID . '" onclick="openSubMenuForParent(' . $item->ID . ', \'' . str_replace('\'', '\\\'', (empty($itemPluralName) ? $item->title : $itemPluralName)) . '\', \'' . $item->url . '\');">';
        } else {
            echo '<li data-item-id="' . $item->ID . '" onclick="location.href=\'' . $item->url . '\';">';
        }
        if ($itemIcon) {
            echo '<img alt="" src="' . $itemIcon . '" />';
        }
        $linq = empty($map[$item->ID]) ? ('href="' . $item->url . '"') : '';
        echo '<a ' . $linq . '>' . $item->title . '</a>';
        echo '</li>';
        if (!empty($map[$item->ID])) {
            echo '<ul class="submenu" data-item-id="' . $item->ID . '" data-parent-id="' . $parentId . '">';
            foreach ($map[$item->ID] as $subItem) {
                printMenuItem($subItem, $map, $item->ID);
            }
            echo '</ul>';
        }
    }

    function listMenuForMobile($locationName)
    {
        $locations = get_nav_menu_locations();
        $items = wp_get_nav_menu_items($locations[$locationName]);
        $menuArray = [];
        if (empty($items)) {
            return;
        }
        // Sort the items as an associative array of parent_id->children, to be used as a map for recursive printing
        foreach ($items as $item) {
            $menuArray[$item->menu_item_parent][] = $item;
        }
        foreach ($menuArray[0] as $topLevelItem) {
            printMenuItem($topLevelItem, $menuArray, -1);
        }
    }

    function listMobileBottomMenu()
    {
        $locations = get_nav_menu_locations();
        $items = wp_get_nav_menu_items($locations['mobile-menu-bottom']);
        if (empty($items)) {
            return;
        }
        // Sort the items as an associative array of parent_id->children, to be used as a map for recursive printing
        echo '<ul class="mobile-bottom-menu">';
        foreach ($items as $item) {
            ?>
            <li>
                <a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a>
            </li>
            <?php
        }
        echo '</ul>';
    }

    ?>
</div>
