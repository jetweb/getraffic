<?php
$object = get_queried_object();

$slider_transition_time = extractFields('slider_transition_time', $object);
$auto_transition = extractFields('auto_transition', $object);

?>
<div class="slider-wrapper">
    <ul class="slides" data-auto-transition="<?php echo$auto_transition;?>" data-transition-time="<?php echo$slider_transition_time;?>">
        <?php
        $slides = extractFields('header_slider', $object);
        if ($slides) {
            foreach ($slides as $slide) {
                $bgImageUrl       = $slide['bg_image']['url'];
                $bgImageMobileUrl = $slide['bg_image_for_mobile']['url'];
                $bgImageAlt       = $slide['bg_image']['alt'];
                $mainImage        = $slide['main_image']['url'];
                $mainImageAlt     = $slide['main_image']['alt'];
                $title            = $slide['title'];
                $buttonText       = $slide['button_text'];
                $buttonLink       = $slide['button_link'];
                $bgColor          = $slide['bg_color'];
                $fullWidth        = $slide['full_width'];
                ?>
                <li>
                <?php if ($bgImageUrl) { ?>
                    <img src="<?php echo $bgImageUrl ?>" class="bg" alt="<?php echo $bgImageAlt ?>"/>
                <?php } ?>
                <?php if ($bgImageMobileUrl) { ?>
                    <img src="<?php echo $bgImageMobileUrl ?>" class="bg-mobile" alt="<?php echo $bgImageAlt ?>"/>
                <?php } else { ?>
                    <img src="<?php echo $bgImageUrl ?>" class="bg-mobile" alt="<?php echo $bgImageAlt ?>"/>
                <?php } ?>
                <div class="overlay" style="background-color: <?php echo $bgColor; ?>">
                    <img src="<?php echo $mainImage ?>" alt="<?php echo $mainImageAlt ?>" style="max-width: 100%;<?php echo $fullWidth ? 'width:100%;' : ''; ?>">
                    <h2><?php echo $title; ?></h2>
                    <?php if ($buttonText) { ?>
                        <a class="tony-button sharp-corners wide" href="<?php echo $buttonLink; ?>"><?php echo $buttonText; ?></a>
                    <?php } ?>
                </div>
                </li><?php
            }
        }
        ?>
    </ul>
</div>
