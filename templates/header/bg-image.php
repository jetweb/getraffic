<?php
$object = get_queried_object();
$bgImage = extractFields('main_bg_image', $object);
$bgMobile = $bgImage['bg_image_mobile']['url'];
if (empty($bgMobile)) {
    $bgMobile = $bgImage['bg_image']['url'];
}
if ($bgImage['bg_image']) {
    ?>
    <div class="bg-image container" data-bg="url(<?php echo $bgImage['bg_image']['url']; ?>)" data-bgmobile="url(<?php echo $bgMobile; ?>)">
        <?php if ($bgImage['title']) { ?>
            <div class="container align-center">
                <div class="align-center bg-container">
                    <h1 class="bold">
                        <?php echo $bgImage['title']; ?>
                    </h1>
                    <span class="bold-text"><?php echo $bgImage['desc']; ?></span>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php
}
?>
