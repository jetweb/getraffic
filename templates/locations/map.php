<div id="map"></div>
<script>
    var geocoder;
    var map;
    var bounds;
    function initMap() {
        bounds = new google.maps.LatLngBounds();
        map = new google.maps.Map(document.getElementById('map'));
        geocoder = new google.maps.Geocoder();
        $('span.address').each(function() {
            let myLatLng = {lat: $(this).data('lat'), lng: $(this).data('lng')};
            showAddressByLocation(map, myLatLng);
        });

        setTimeout(function() {
            map.fitBounds(bounds, 40);
        }, 2000);

    }

    function showAddressByLocation(map, location) {
        map.setCenter(location);
        let marker = new google.maps.Marker({
            map: map,
            position: location
        });
        bounds.extend(marker.getPosition());
    }

    function centerOnAddress(geocoder, map, address) {
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                let rad = $("#selectRadius").val();
                if (rad == -1) {
                    map.setZoom(12)
                } else if (rad == 5) {
                    map.setZoom(14)
                } else if (rad == 10) {
                    map.setZoom(13)
                } else if (rad == 15) {
                    map.setZoom(12)
                } else if (rad == 20) {
                    map.setZoom(11)
                }
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });

        $('.location').hide();
        setTimeout(function() {
            let bounds = map.getBounds();
            $('.location').each(function() {
                let address = $(this).find('.address');
                let lat = address.data('lat');
                let lng = address.data('lng');


                if (bounds.contains(new google.maps.LatLng(lat,lng))) {
                    $(this).show();
                }
            });
        }, 400);

    }

    jQuery(document).ready(function() {
        $('.search-btn').closest('form').bind('submit', function(e) {
            centerOnAddress(geocoder, map, $('input#address').val());
            e.preventDefault();
            return false;
        })
    });
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBc9fh8wqwaGBWTggSFVuOeNJlF2fHYgvs&callback=initMap&language=he">
</script>

<style>
    #map {
        height: 300px;
        margin: 30px 0;
    }
</style>
