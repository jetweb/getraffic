<div class="locations-bg">
    <img src="<?php echo img('stores.jpg'); ?>" alt="">
</div>
<h1 class="locations-title">
    נקודות מכירה</h1>
<form>
    <div class="row locations-filter">
        <div class="col-md-6">
            <input type="text" placeholder="כתובת לחיפוש..." id="address" />
        </div>
        <div class="col-md-3">
            <select name="radius" id="selectRadius">
                <option value="-1">-- רדיוס --</option>
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
        </div>
        <div class="col-md-3">
            <button class="search-btn">
                חפשי
            </button>
        </div>
    </div>
</form>

<?php get_template_part('templates/locations/map'); ?>

<?php
global $GT;

$args = [
    'post_type' => 'location',
    'posts_per_page' => '-1'
];

if (isset($_GET['city']) && $_GET['city'] != -1) {
    $args['tax_query'] = [
        [
            'taxonomy'         => 'tm_city',
            'terms'            => $_GET['city'],
            'include_children' => true,
            'operator'         => 'IN',
        ],
    ];
}
$posts = $GT->queryCutsomPostType($args);

$i = 0;
foreach ($posts as $location) {
    $address    = str_replace(' ', '+', $location->fields['location_address']);
    $optionName = 'location_lat_lng' . $address;
    $result     = get_option($optionName);
    $lat        = $lng = '';
    if ($result) {
        $lat = explode('X', $result)[0];
        $lng = explode('X', $result)[1];
    } else {
        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?key=AIzaSyBc9fh8wqwaGBWTggSFVuOeNJlF2fHYgvs&address=' . $address . '&sensor=false');
        $output  = json_decode($geocode);
        $lat     = $output->results[0]->geometry->location->lat;
        $lng    = $output->results[0]->geometry->location->lng;
        if ($lat && $lng)
            add_option($optionName, $lat . 'X' . $lng);
    }

    ++$i;
    echo '
            <div class="location">
                <div>
                    <span class="title">' . $location->post_title . '</span>
                    <span class="address" data-lat="'. $lat .'" data-lng="'. $lng .'">' . $location->fields['location_address'] . '</span>
                </div>
                <span class="phone">' . $location->fields['location_phone'] . '</span>
            </div>
            ';
}
?>

<style>
    .locations-bg {
        text-align: center;
    }
    .locations-title {
        border-bottom: 1px solid black;
        padding: 20px;
        font-weight: 100;
        font-size: 30px;
        padding-right: 0;
    }
    .search-btn {
        background-color: #0f0f0f;
        color: #FFF;
        font-weight: bold;
    }
    .locations-filter select, .locations-filter input {
        border: 1px solid black;
        border-radius: 0px;
        padding: 1px 10px;
        -webkit-appearance: unset;

    }
    .locations-filter select {
        background: url(/wp-content/themes/getraffic/assets/images/arrow-down-copy-3.png);

        background-repeat: no-repeat;
        background-position: 10px;
    }

    .locations-filter select, .locations-filter button, .locations-filter input {
        width: 100%;
    }

    .location {
        border-bottom: 1px solid black;
        direction: rtl;
        display: flex;
        justify-content: space-between;
        padding: 10px 0;
        margin: 10px 0;
    }
    .location span {

        font-weight: 100;
    }

    .location span.title {
        display: inline-block;
        width: 180px;
        font-weight: 800;
    }

    .container.locations {
        max-width: 720px !important;
    }
</style>
