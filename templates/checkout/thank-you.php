<?php
/**
 * Template Name: עמוד תודה
 *
 * The home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>


<?php if (have_posts()) {
    while (have_posts()) : the_post();
        the_content(); endwhile;
} ?>

<?php get_footer(); ?>

