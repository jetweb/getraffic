<?php
if (!class_exists('YITH_WC_Points_Rewards_Redemption')) {
    return;
}

?>
<div class="checkout-points">
    <?php
    YITH_WC_Points_Rewards_Frontend()->print_rewards_message_in_cart();
    $points = YITH_WC_Points_Rewards_Redemption()->get_max_points();

    ?>
    <div class="flex">
        <?php if (is_user_logged_in() && $points > 0) { ?>
            <div class="coupon-input">
            <span class="points">יש לך
                <?php echo YITH_WC_Points_Rewards_Redemption()->get_max_points(); ?> נקודות למימוש
            </span>
                <button id="apply_points" class="apply-points">לממש עכשיו</button>
            </div>
        <?php } ?>
        <div class="points-collected">
            בקנייה זו תצברו <?php echo GT::getPointsToEarnInCart(); ?> נקודות לרכישה הבאה!
        </div>
    </div>
</div>
<style>
    .checkout-points {
        display: <?php echo GT::isUserInClub() ? 'block' : 'none' ?>;
    }
</style>
