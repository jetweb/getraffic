<?php
$show_terms_checkbox    = gt_get_field('show_terms_checkbox', 'option');
$checkout_checkbox_text = gt_get_field('checkout_checkbox_text', 'option');
if ($show_terms_checkbox) {
    ?>
    <label for="accept_terms">
        <input type="checkbox" name="accept_terms" id="accept_terms">
        <span><?= str_replace('<p>', '', str_replace('</p>', '', $checkout_checkbox_text)); ?></span>
    </label>
    <?php
}
?>
<script>
    var termsErrorMessage = '<?= gt_get_field('checkout_checkbox_error_text', 'option'); ?>';
</script>
