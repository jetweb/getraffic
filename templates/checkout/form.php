<?php GT::enqueueAsset('checkout-js', '/assets/src/js/checkout.js', [], true, true); ?>
<?php get_template_part('templates/checkout/authentication'); ?>
<div id="nav-billing" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="showStep('billing');">1. פרטי לקוח</h2>
    <div class="wizard-item-content">
        <div class="checkout-message"></div>

        <div class="checkout-billing-form" id="customer_details">
            <?php do_action('woocommerce_checkout_billing'); ?>
        </div>

        <div class="full-width checkout-button-wrapper">
            <a class="checkout-button" onclick="validateCheckout();">
                המשיכי להזנת קופונים ומימוש הטבות
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="nav-coupons" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="showStep('coupons');">2. קופונים והטבות</h2>
    <div class="wizard-item-content">
        <?php
        get_template_part('templates/checkout/coupons');
        get_template_part('templates/checkout/points');
        ?>
        <div class="full-width checkout-button-wrapper">
            <a class="checkout-button" onclick="showStep('delivery');">
                המשיכי לבחירת סוג משלוח
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="nav-delivery" class="checkout-wizard-step">
    <h2 class="wizard-title" onclick="showStep('delivery');">3. שיטת משלוח</h2>
    <div class="wizard-item-content">
        <?php get_template_part('templates/checkout/shipping'); ?>

        <div class="full-width checkout-button-wrapper">
            <a class="submit-checkout checkout-button" onclick="$('form.checkout.woocommerce-checkout').submit();">
                המשיכי לתשלום מאובטח
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<script>
    let isUserLoggedIn = <?php echo is_user_logged_in() ? 'true' : 'false'; ?>;
    let isUserInClub = <?php echo GT::isUserInClub() ? 'true' : 'false' ?>;
    let enableJoinPopup = <?php echo gt_get_field('points_and_club_settings', 'option')['allow_join_club'] ? 'true' : 'false' ?>;
    let autoJoinClub = <?php echo gt_get_field('points_and_club_settings', 'option')['auto_join_club'] ? 'true' : 'false' ?>;
</script>

<?php get_template_part('templates/popups/join-us'); ?>


