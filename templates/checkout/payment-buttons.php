<div class="checkout-buttons">
    <?php get_template_part('templates/checkout/accept-terms'); ?>
    <?php
    $available_gateways = WC()->payment_gateways->get_available_payment_gateways();
    foreach ($available_gateways as $gateway) {
        ?>
        <a href="#" class="checkout-btn payment-button-<?= $gateway->id ?>" data-payment-method="<?= $gateway->id ?>">
            <?= $gateway->title; ?>
        </a>
    <?php } ?>
</div>
