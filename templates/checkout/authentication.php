<?php
if (is_user_logged_in()) { ?>
    <span class="hi">היי, <?php echo wp_get_current_user()->first_name; ?></span>
<?php } else { ?>
    <div class="auth">
        <div class="login">
            <h1>אנחנו מכירות כבר, לא?</h1>
            <a class="login-popup-trigger" href="<?php
            $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            echo get_site_url() . '/my-account?return=' . $actual_link ?>">
                לחצי כאן להתחברות וקנייה מהירה
            </a>
            <?php get_template_part('templates/popups/login'); ?>
        </div>
        <div class="facebook">
            <?php echo do_shortcode('[woocommerce_social_login_buttons return_url="' . wc_get_checkout_url() . '"]'); ?>
        </div>
    </div>
<?php }
?>
