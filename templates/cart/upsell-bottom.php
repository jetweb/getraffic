<?php $bottomProducts = gt_get_field('cart_bottom_product_stripe', 'option');
?>

<div class="container best-price-list">
    <?php
    if (!empty($bottomProducts['products'])) {
        $postIds  = $bottomProducts['products'];
        $products = [];
        foreach ($postIds as $postId) {
            $products[] = wc_get_product($postId);
        }
        GT::renderCrossSellProducts($bottomProducts['title'], $bottomProducts['subtitle'], $products, 'sample');
    }
    ?>
</div>
