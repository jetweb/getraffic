<?php
$topProducts = gt_get_field('cart_top_product_stripe', 'option');
?>
<div class="cart-upsell-top">
    <div class="samples"><?= $topProducts['title'] ?><br><span class="sample-subtitle"><?= $topProducts['subtitle'] ?></span>
        <span class="sample-arrow"></span>
    </div>
    <div class="samples-wrapper">
        <div class="product-samples">
            <?php
            if (!empty($topProducts['products'])) {
                foreach ($topProducts['products'] as $productId) {
                    echo '<div class="upsell-sample-wrapper">';
                    $product = wc_get_product($productId);
                    GT::renderProduct($product, false, 'sample');
                    echo '</div>';
                }
            }
            ?>
        </div>
    </div>
</div>
