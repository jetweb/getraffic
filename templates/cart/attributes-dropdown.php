<?php
$_product            = get_query_var('attributesViewProduct');
if (!$_product instanceof WC_Product_Variation) {
    return;
}
$textChoose      = gt_get_field('cart_attr_choose_text', 'option');
$textClear       = gt_get_field('cart_attr_clear_selection_text', 'option');
$textUpdate      = gt_get_field('cart_attr_update_text', 'option');
$cart_item_key   = get_query_var('cart_item_key');
$variations_json = wp_json_encode(wc_get_product($_product->get_parent_id())->get_available_variations());
$variations_attr = function_exists('wc_esc_json') ? wc_esc_json($variations_json) : _wp_specialchars($variations_json, ENT_QUOTES, 'UTF-8', true);
?>
<div class="cart-item-atts" data-original_variation_id="<?= $_product->get_id() ?>" data-product_variations="<?= $variations_attr ?>" data-product_id="<?= $_product->get_parent_id() ?>">
    <?php
    foreach ($_product->get_attributes() as $attribute => $attributeName) {
        $attr     = urldecode($attribute);
        $realAttr = str_replace('pa_', '', $attr);
        $terms    = wc_get_product_terms($_product->get_parent_id(), $attr, ['fields' => 'all']);
        echo '<select data-attribute-name="' . $attribute . '">';?>
        <?php
        foreach ($terms as $term) {
            $isSelected = $term->slug === $attributeName;
            echo '<option value="'. $term->slug . '" ' . ($isSelected ? 'selected="selected"' : '') . '>' . $term->name . '</option>';
        }
        echo '</select>';
    }
    if ($_product instanceof WC_Product_Variation) {
        $_product = wc_get_product($_product->get_parent_id());
    }
    $n = 0;
    ?>
    <a href="#" class="clear-selection"><?= $textClear ?></a>
    <a href="#" class="update-variation"><?= $textUpdate ?></a>
    <input type="hidden" value="<?=$cart_item_key?>" class="cartItemToReplace">
</div>
