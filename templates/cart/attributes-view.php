<div class="cart-item-atts">
    <?php
    global $quickViewProductIds;
    $textEdit      = gt_get_field('cart_attr_edit_text', 'option');
    $_product      = get_query_var('attributesViewProduct');
    $cart_item_key = get_query_var('cart_item_key');
    foreach ($_product->get_attributes() as $attribute => $attributeName) {
        $attr  = urldecode($attribute);
        $terms = wc_get_product_terms($_product->get_parent_id(), $attr, ['fields' => 'all']);
        foreach ($terms as $term) {
            $realName = str_replace('-', ' ', urldecode($attributeName));
            if ($term->name === $realName) {
                $quickViewProductIds[] = [$_product->get_parent_id(),$cart_item_key];
                $attachment_id = absint(get_term_meta($term->term_id, 'product_attribute_image', true));
                $image_size    = woo_variation_swatches()->get_option('attribute_image_size');
                $image_url     = wp_get_attachment_image_url($attachment_id, apply_filters('wvs_product_attribute_image_size', $image_size));
                $realAttr      = str_replace('pa_', '', $attr);
                if ($image_url) {
                    echo "<img class='attribute-img' src='$image_url' />";
                }
                echo "<span class='attribute-name'>$realAttr | $realName</span>";
                echo "<a href='#' class='attribute-edit-link' 
                                                onclick=\"openQuickView('". $cart_item_key ."');\">". $textEdit ."</a>";
            }
        }
    }
    ?>
</div>