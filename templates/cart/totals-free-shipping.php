<div class="cart-totals-free-shipping">
    <?php
    $shippingFrom         = gt_get_field('free_shipping_from', 'option');
    $textAddMore          = gt_get_field('free_shipping_add_more_text', 'option');
    $textAcheived         = gt_get_field('free_shipping_achieved_text', 'option');

    $subTotal = WC()->cart->get_subtotal();
    if (($shippingFrom - $subTotal) > 0) {
        echo str_replace('[amount]', '₪' . ($shippingFrom - $subTotal), $textAddMore);
    } else {
        echo $textAcheived;
    }
    ?>
</div>
