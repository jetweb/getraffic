<?php $fields = gt_get_field('category_page_settings', 'option'); ?>
<div class="container nothing-found">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <h2 dir="rtl">
                    <span>
                <?php if (!empty($s)) { ?>
                    <?= str_replace('[search]', get_search_query(false), $fields['no_search_results']); ?>
                <?php } else { ?>
                    <?=$fields['no_results']?>
                <?php } ?>
                    </span>
                </h2>
            </div>
        </div>
    </div>
</div>
<?php
$bestSellers = $fields['upsell_no_results'] ?>
<div class="container best-sellers-container">
    <h1 class="nothing-found-text"><?= $fields['maybe_upsell'] ?></h1>
    <?php
    $products = [];
    if (!empty($bestSellers)) {
        foreach ($bestSellers as $post) {
            $products[] = wc_get_product($post->ID);
        }
        GT::renderCrossSellProducts(
            '', '', $products
        );
    }
    ?>
</div>
<?php get_template_part('templates/contact/address'); ?>
