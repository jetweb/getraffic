<?php
do_action( 'woocommerce_before_shop_loop' );
$fields = gt_get_field('category_page_settings', 'option');

if (wc_get_loop_prop('total')) {
    echo "<div class='products' data-page='1'>";
    $productCount = 0;
    while (have_posts()) {
        the_post();
        do_action('woocommerce_shop_loop');
        global $product;

        GT::renderProduct($product, true, 'thumbnail', $fields['spread_variations']);

        ++$productCount;
    }
} else {
    get_template_part('templates/category/search-empty');
}

$cat = get_queried_object();
if (is_object($cat) && isset($cat->term_id)) {
    $banners = gt_get_field('page_banners', 'product_cat_' . $cat->term_id);

    if ($banners) {
        foreach ($banners as $banner) { ?>
            <div class="archive-banner" style="

            <?php if ($productCount > 2) { ?>

                    grid-column: <?php echo $banner['from_col']; ?> / <?php echo $banner['to_col']; ?>;

                    grid-row: <?php echo $banner['from_row']; ?> / <?php echo $banner['to_row']; ?>;

            <?php } ?>

                    "
                 data-show_mobile="<?php echo $banner['show_in_mobile_devices'] ? 'block' : 'none'; ?>"
                 data-mobile-column="<?php echo $banner['from_col_mobile']; ?> / <?php echo $banner['to_col_mobile']; ?>"
                 data-original-column="<?php echo $banner['from_col']; ?> / <?php echo $banner['to_col']; ?>"
            >

                <a href="<?php echo $banner['link']; ?>"><img style="width:100%;" src="<?php echo $banner['image']; ?>"/></a>
            </div>
        <?php }
    }
}

echo "</div>";


?>
