<?php
defined('ABSPATH') || exit;
do_action('woocommerce_before_single_product');
GT::enqueueAsset('product-js', '/assets/src/js/product.js', ['jquery'], true, true);

if (post_password_required()) {
    echo get_the_password_form(); // WPCS: XSS ok.
    return;
}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
<?php get_template_part('templates/popups/added-to-cart'); ?>
    <div class="container">
        <div class="row">
            <?php get_template_part('templates/product/content/details'); ?>
            <?php get_template_part('templates/product/content/images/index'); ?>
        </div>
    </div>
    <?php get_template_part('templates/product/content/reviews'); ?>
    <?php
        global $product;
        $xSells = array_filter(array_map('wc_get_product', $product->get_cross_sell_ids()), 'wc_products_array_filter_visible');
        GT::renderCrossSellProducts('YOUT BEST MATCH', 'הכי מתאים לכם', $xSells);
    ?>
</div>

<?php do_action('woocommerce_after_single_product'); ?>





