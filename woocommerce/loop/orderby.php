<?php
/**
 * Show options for ordering
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/orderby.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}
?>

<?php GT::enqueueAsset('orderby-css', '/assets/src/scss/orderby.css', [], false, false); ?>
<?php GT::enqueueAsset('orderby-js', '/assets/src/js/orderby.js', [], true, true); ?>

<div class="mobile-filter-container">
    <div class="mobile-filter-title">
        סנן מוצרים
        <a href="" class="back">
            <img src="<?php echo img('filter-back.png') ?>" alt="">
        </a>
    </div>
    <div class="mobile-filter-options">
        <?php dynamic_sidebar('archive_page_filters'); ?>
		<div class="showonsale" style="width:100%;margin:0 auto;text-align:center;"><input id="onsalechk" type="checkbox" value="1" name="onsale" style="opacity:1;" <?php echo((isset($_GET['onsale']) && $_GET['onsale']=='1')?'checked="checked"':'')?>/>הצגת הדגמים שבמבצע </div>
    </div>
    <div class="mobile-filter-close">
        <a href="" class="close-filter">
            <img src="<?php echo img('filter-close.png') ?>" alt="">
        </a>
        <br/>
        סגור
    </div>
</div>
<div class="mobile-sort-container">
    <div class="mobile-filter-title">
        מיון תוצאות
        <a href="" class="back">
            <img src="<?php echo img('filter-back.png') ?>" alt="">
        </a>
    </div>
    <div class="mobile-filter-options">
        <ul class="woocommerce-widget-layered-nav-list gt-orderby">
            <?php foreach ($catalog_orderby_options as $id => $name) : ?>
                <li class="woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?php echo ($orderby == $id ? 'chosen' : ''); ?>">
                    <a href="" data-value="<?php echo esc_attr($id); ?>">
                        <?php echo esc_html($name); ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="mobile-filter-close">
        <a href="" class="close-filter">
            <img src="<?php echo img('filter-close.png') ?>" alt="">
        </a>
        <br/>
        סגור
    </div>
</div>
<div class="category-topbar">
    <div class="category-order-by">
        <form class="woocommerce-ordering" method="get">
            <select name="orderby" class="orderby">
                <?php foreach ($catalog_orderby_options as $id => $name) : ?>
                    <option value="<?php echo esc_attr($id); ?>" <?php selected($orderby, $id); ?>><?php echo esc_html($name); ?></option>
                <?php endforeach; ?>
            </select>
            <input type="hidden" name="paged" value="1"/>
            <?php wc_query_string_form_fields(null, ['orderby', 'submit', 'paged', 'product-page']); ?>
        </form>

    </div>
    <div class="category-sort-mobile">
        <span class="filter">מיון</span>
    </div>
    <div class="category-filter-mobile">
        <span class="filter">סינון</span>
    </div>
    <div class="category-filters">
        <div class="desktop-orderby">
            <div class="archive-filter order-by">
                <div class="widgettitle archive-filter-title">
                    מיון תוצאות
                </div>
                <ul class="woocommerce-widget-layered-nav-list gt-orderby">
                    <?php foreach ($catalog_orderby_options as $id => $name) : ?>
                        <li class="order-item woocommerce-widget-layered-nav-list__item wc-layered-nav-term <?php echo ($orderby == $id ? 'chosen' : ''); ?>">
                            <a href="" data-value="<?php echo esc_attr($id); ?>">
                                <?php echo esc_html($name); ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="desktop-filtering">
            <?php dynamic_sidebar('archive_page_filters'); ?>
        </div>
    </div>
</div>
<div class="filter-tags">
</div>
<script>
    let isMultiSelectEnabled = <?php echo $fields = gt_get_field('category_page_settings', 'option')['allow_multiple_select_filter'] ? 'true' : 'false' ?>;
    let xImageUrl = '<?php echo img('x.png'); ?>';
</script>


