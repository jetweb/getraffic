<?php
/**
 * Wishlist page template
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Wishlist
 * @version 2.0.12
 */

if ( ! defined( 'YITH_WCWL' ) ) {
	exit;
} // Exit if accessed directly

$wish_list_empty = false;

?>

<?php do_action( 'yith_wcwl_before_wishlist_form', $wishlist_meta ); ?>

    <?php wp_nonce_field( 'yith-wcwl-form', 'yith_wcwl_form_nonce' ) ?>

    <!-- TITLE -->
    <?php
    do_action( 'yith_wcwl_before_wishlist_title', $wishlist_meta );

    if( ! empty( $page_title ) ) :
    ?>
        <div class="wishlist-title <?php echo ( $is_custom_list ) ? 'wishlist-title-with-form' : ''?>">
            <?php echo apply_filters( 'yith_wcwl_wishlist_title', '<h2>' . $page_title . '</h2>' ); ?>
            <?php if( $is_custom_list ): ?>
                <a class="btn button show-title-form">
                    <?php echo apply_filters( 'yith_wcwl_edit_title_icon', '<i class="fa fa-pencil"></i>' )?>
                    <?php _e( 'Edit title', 'yith-woocommerce-wishlist' ) ?>
                </a>
            <?php endif; ?>
        </div>
        <?php if( $is_custom_list ): ?>
            <div class="hidden-title-form">
                <input type="text" value="<?php echo $page_title ?>" name="wishlist_name"/>
                <button>
                    <?php echo apply_filters( 'yith_wcwl_save_wishlist_title_icon', '<i class="fa fa-check"></i>' )?>
                    <?php _e( 'Save', 'yith-woocommerce-wishlist' )?>
                </button>
                <a class="hide-title-form btn button">
                    <?php echo apply_filters( 'yith_wcwl_cancel_wishlist_title_icon', '<i class="fa fa-remove"></i>' )?>
                    <?php _e( 'Cancel', 'yith-woocommerce-wishlist' )?>
                </a>
            </div>
        <?php endif; ?>
    <?php
    endif;

    $data = gt_get_field("gt_wish_list_page_template","option");

     do_action( 'yith_wcwl_before_wishlist', $wishlist_meta ); ?>
        <?php
        if( count( $wishlist_items ) > 0 ) :
            ?><div class="products"><?php
            $added_items = array();
            foreach( $wishlist_items as $item ) :
                global $product;
            
	            $item['prod_id'] = yit_wpml_object_id ( $item['prod_id'], 'product', true );

	            if( in_array( $item['prod_id'], $added_items ) ){
		            continue;
	            }

	            $added_items[] = $item['prod_id'];
	            $product = wc_get_product( $item['prod_id'] );
	            $availability = $product->get_availability();
	            $stock_status = isset( $availability['class'] ) ? $availability['class'] : false;
                $quickproduct = get_query_var('productToRender');

                if( $product && $product->exists() ) {
                    GT::renderProduct($product);
                }
            endforeach;
            ?></div><?php
        else: ?>
        
            <div class="wishlist-empty full-width align-center">
                <?php
                $wish_list_empty = true;
                if(!empty($data["display"])): ?>
                    <?php foreach($data["display"] as $display): ?>

                        <?php switch($display): case "icon": ?>
                            <?php if($data["icon"]): ?>
                                <img src="<?= $data["icon"]["url"]; ?>" alt="<?= ($data["icon"]["alt"])? $data["icon"]["alt"] : "" ?>"><br />
                            <?php endif; ?>
                        <?php break; ?>

                        <?php case "text": ?>
                            <?php if($data["text"]): ?>
                                <p class="text">
                                    <?= $data["text"] ?>
                                </p>
                            <?php endif; ?>
                        <?php break; ?>

                        <?php case "subtext": ?>
                            <?php if($data["subtext"]): ?>
                                <p class="subtext">
                                    <?= $data["subtext"] ?>
                                </p>
                            <?php endif; ?>
                        <?php break; ?>

                        <?php case "editor": ?>
                            <?php if($data["editor"]): ?>
                                <div class="content">
                                    <?= $data["editor"] ?>
                                </div>
                            <?php endif; ?>
                        <?php break; ?>

                        <?php endswitch; ?>

                    <?php endforeach; ?>
                <?php endif; ?>


            </div>
        <?php
        endif;?>


<?php


if(!empty($data["display"]) && in_array("wish_list_slider", $data["display"])){

    $posts = gt_get_field('gt_wist_list_global_slider', 'option');
    $get_products_ids = (!empty($posts["products"]))? $posts["products"] : false;
        if ($get_products_ids) {
            $products = [];
            foreach ($get_products_ids as $id) {
                $products[] = wc_get_product($id);
            }
            $title = null;
            $sub_title = null;

            if($wish_list_empty){
                $title = $posts["empty_title"];
                $sub_title = $posts["empty_sub_title"];
            }else{
                $title = $posts["title"];
                $sub_title = $posts["sub_title"];
            }

            GT::renderCrossSellProducts($title, $sub_title, $products);
        }

}
?>