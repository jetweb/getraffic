<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */
defined('ABSPATH') || exit;
GT::enqueueAsset('account-css', '/assets/src/scss/account.css', []);
GT::enqueueAsset('account-js', '/assets/src/js/account.js', ['jquery'], true, true);

?>
<div class="my-account-container">
    <div class="my-account-nav">
        <div class="myaccount-topicon-text">שלום,
            <?php
            $user = wp_get_current_user();
            if ($user->first_name) {
                echo $user->first_name;
            } else {
                echo $user->display_name;
            }
            ?>
        </div>
        <a class="logout-link" href="<?php echo wp_logout_url(home_url()); ?>">יציאה</a>
        <?php do_action('woocommerce_account_navigation'); ?>
    </div>
    <div class="my-account-content">
        <div class="woocommerce-MyAccount-content">
            <?php do_action('woocommerce_account_content'); ?>
        </div>
    </div>
</div>

