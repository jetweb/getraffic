<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

global $customer_id;

if ( ! wc_ship_to_billing_address_only() && wc_shipping_enabled() ) {
    $get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
        'shipping' => __( 'Shipping address', 'woocommerce' ),
    ), $customer_id );
} else {
    $get_addresses = apply_filters( 'woocommerce_my_account_get_addresses', array(
        'billing' => __( 'Billing address', 'woocommerce' ),
    ), $customer_id );
}
$fields = gt_get_field('my_account_address', 'option');
?>
<div class="getraffic-edit-account">
    <div class="woocommerce-MyAccount-user">
        <img src="<?php echo $fields['icon']; ?>">
        <h3><?php echo $fields['title']; ?></h3>
    </div>
	<form method="post" action="">
        <div class="myaccount_billing_fields">
		<?php
        WC_Shortcode_My_Account::edit_address('billing');
        ?>
        </div>
        <label class="checkbox show-shipping-fields" style="position: static; display: inline-block;">
            <input type="checkbox" class="input-checkbox show-shipping-fields" name="edit_also_shipping" style="display: inline-block;">
            <span><?= $fields['shipping_differs'] ?></span>
        </label>
        <div class="myaccount_shipping_fields">
            <?php WC_Shortcode_My_Account::edit_address('shipping'); ?>
        </div>
        <p class="save_address">
            <button type="submit" class="button" name="save_address" value="<?php esc_attr_e( 'Save address', 'woocommerce' ); ?>"><?php echo $fields['button_text'] ?></button>
            <?php wp_nonce_field( 'woocommerce-edit_address', 'woocommerce-edit-address-nonce' ); ?>
            <input type="hidden" name="action" value="edit_address" />
        </p>
	</form>

    <?php do_action( "woocommerce_after_edit_address_form_billing" ); ?>


</div>

<?php do_action( 'woocommerce_after_edit_account_address_form' ); ?>
