<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * My Account navigation.
 *
 * @since 2.6.0
 */
?>
<div class="col-md-2 "><?php do_action('woocommerce_account_navigation'); ?></div>
<div class="col-md-9 col-md-offset-1">

<div class="woocommerce-wishlist-content">
    
    
    <?php 
    
    $query_args = array();

				if( ! empty( $user_id ) ){
					$query_args[ 'user_id' ] = $user_id;
					$query_args[ 'is_default' ] = 1;

					if( get_current_user_id() == $user_id ){
						$is_user_owner = true;
					}
				}
				elseif( ! is_user_logged_in() ){
					if( empty( $wishlist_id ) ){
						$query_args[ 'wishlist_id' ] = false;
						$is_user_owner = true;
					}
					else{
						$is_user_owner = false;

						$query_args[ 'wishlist_token' ] = $wishlist_id;
						$query_args[ 'wishlist_visibility' ] = 'visible';
					}
				}
				else{
					if( empty( $wishlist_id ) ){
						$query_args[ 'user_id' ] = get_current_user_id();
						$query_args[ 'is_default' ] = 1;
						$is_user_owner = true;
					}
					else{
						$wishlist = YITH_WCWL()->get_wishlist_detail_by_token( $wishlist_id );
						$is_user_owner = $wishlist['user_id'] == get_current_user_id();

						$query_args[ 'wishlist_token' ] = $wishlist_id;

						if( ! empty( $wishlist ) && $wishlist['user_id'] != get_current_user_id() ){
							$query_args[ 'user_id' ] = false;
							if( ! current_user_can( apply_filters( 'yith_wcwl_view_wishlist_capability', 'manage_options' ) ) ){
								$query_args[ 'wishlist_visibility' ] = 'visible';
							}
						}
					}
				}

    ?>
<?php $wishlist_items = YITH_WCWL()->get_products($query_args ); ?>
    
    
<?php
    if( count( $wishlist_items ) > 0 ) : ?>
        <div class="products">
            <?php $added_items = array();
            foreach( $wishlist_items as $item ) : 
                global $product;

                $item['prod_id'] = yit_wpml_object_id ( $item['prod_id'], 'product', true );

                if( in_array( $item['prod_id'], $added_items ) ){
                    continue;
                }

                $added_items[] = $item['prod_id'];
                $product = wc_get_product( $item['prod_id'] );
                $availability = $product->get_availability();
                $stock_status = isset( $availability['class'] ) ? $availability['class'] : false;
                $quickproduct = get_query_var('productToRender');
                if( $product && $product->exists() ) {
//                    GT::renderProduct($product);
                    GT::renderProduct($product, $showControls = true, $template = 'wishlist-product', $spreadVariations = false);
                    //get_template_part('templates/popups/quick-view');
                }
            endforeach; ?>
        </div><?php
        else: ?>
            <div class="wishlist-empty full-width align-center">
                <img src="<?php echo img('wishlist_empty.jpg'); ?>" alt=""><br />
                זה לא כל כך נעים לראות WISH LIST ריק...
            </div>
        <?php
        endif;?>    
</div>
</div>
<div style="clear: both"></div>

