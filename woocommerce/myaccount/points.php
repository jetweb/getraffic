<?php
/**
 * Points
 *
 * Shows points on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.2.0
 */
global $has_orders;
if (!defined('ABSPATH')) {
    exit;
}
$points = get_user_meta(get_current_user_id(), '_ywpar_user_total_points', true);
function getPointsForCurrentUser()
{
    global $wpdb;
    $user_id      = get_current_user_id();
    $orderby      = !empty($_GET["orderby"]) ? $_GET["orderby"] : 'date_earning';
    $order        = !empty($_GET["order"]) ? $_GET["order"] : 'DESC';
    $order_string = 'ORDER BY ' . $orderby . ' ' . $order;
    $table_name   = $wpdb->prefix . 'yith_ywpar_points_log';
    $query        = "SELECT ywpar_points.* FROM $table_name as ywpar_points where user_id = $user_id $order_string";

    return $wpdb->get_results($query);
}

if (!GT::isUserInClub()) { ?>
    <div class="account-points">
        עליך להצטרף למועדון בכדי לצבור נקודות
    </div>
<?php } else { ?>
<div class="account-points">
    <h2>
        ברשותך
        <?= $points ?>
        נקודות
    </h2>
    <h3>היסטוריית נקודות</h3>
    <div class="points-history-table">
        <div class="points-row-header">
            <div class="points-col col-action">
                פעולה
            </div>
            <div class="points-col col-amount">
                סכום המימוש
            </div>
            <div class="points-col col-date">
                תאריך המימוש
            </div>
            <div class="points-col col-orderid">
                מספר הזמנה
            </div>
        </div>
        <?php
        foreach (getPointsForCurrentUser() as $record) { ?>
            <div class="points-row">
                <div class="points-col col-action">
                    <?php
                    switch ($record->action) {
                        case 'redeemed_points':
                            echo 'מימוש נקודות';
                            break;
                        case 'order_completed':
                            echo 'צבירת נקודות';
                            break;
                        case 'admin_action':
                            echo 'פעולת מנהל';
                            break;
                        default:
                            echo $record->action;
                            break;
                    }
                    ?>
                </div>
                <div class="points-col col-amount">
                    <?= $record->amount ?>
                </div>
                <div class="points-col col-date">
                    <?= date('d/m/Y', strtotime($record->date_earning)); ?>
                </div>
                <div class="points-col col-orderid">
                    <?= $record->order_id ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>
