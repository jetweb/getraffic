<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.2.0
 */

if (!defined('ABSPATH')) {
    exit;
}
?>

<div class="container col-md-8 col-md-offset-2 col-sm 12">

    <?php if ($order->has_status('failed')) : ?>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.',
                'woocommerce'); ?></p>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
            <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php _e('Pay', 'woocommerce') ?></a>
            <?php if (is_user_logged_in()) : ?>
                <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php _e('My account', 'woocommerce'); ?></a>
            <?php endif; ?>
        </p>

    <?php else : ?>

    <div class="tnx-title">
        <div class="tnx-order-num">
            מספר הזמנה |
            <span><?php echo $order->get_order_number(); ?></span>
        </div>
        <div class="tnx-date">
            תאריך |
            <span><?php echo wc_format_datetime($order->get_date_created()); ?></span>
        </div>
    </div>
    <div class="tnx-text">
        <h1>
            היי
            <span><?php echo $order->get_billing_first_name(); ?></span>, תתחדשי!
        </h1>
        <p>
            ההזמנה התקבלה במערכת, ואנחנו מיד מתחילים להכין אותה עבורך.
        </p>
        <p>
            <strong>
                אישור ההזמנה נשלח גם לכתובת המייל שהזנת |
                <span>
                    <?php echo $order->get_billing_email(); ?>
                </span>
            </strong>
        </p>
    </div>

    <div class="tnx-order">
        <h1 class="tnx-order-title">
            הפריטים שלך
        </h1>
        <?php
        $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
        foreach ($order_items as $item_id => $item) {
            echo '<div class="tnx-item">';
            $product = $item->get_product();
            GT::getProductMainImageHTML($product);

            echo '<div class="blocks">';
            wc_get_template('order/order-details-item.php', [
                'order'              => $order,
                'item_id'            => $item_id,
                'item'               => $item,
                'show_purchase_note' => false,
                'purchase_note'      => $product ? $product->get_purchase_note() : '',
                'product'            => $product,
            ]);
            echo '</div>';
            echo '</div>';
        }
        ?>
    </div>


    <div class="gesammelt">
        גם התחדשת וגם
        <?php echo GT::isUserInClub() ? 'צברת' : 'היית יכולה לצבור'; ?>
        <span><?php echo GT::getEarnedPointsByOrderId($order->get_id()); ?></span>
        נקודות. דאבל כיף!
    </div>
    <div class="tnx-section">
        <h1 class="tnx-order-title">
            סיכום הזמנה
        </h1>
        <div class="order-details-summary">
            <p>סיכום ביניים: <?php echo $currency_symbol . $order->get_subtotal(); ?></p> 
            <p>הטבות מועדון / קוד קופון: <?php echo $currency_symbol . $order->get_discount_total(); ?></p>
            <p>משלוח: <?php echo $currency_symbol . $order->get_total_shipping(); ?><small>&nbsp;דרך שליח עד הבית | ₪30 ||| חינם בקנייה מעל ₪150</small></p>
            <p class="order-details-summary-total">סה״כ לתשלום: <?php echo $currency_symbol . $order->get_total(); ?></p>
        </div>
    </div>
    <div class="tnx-section">
        <h1 class="tnx-order-title">
            פרטי משלוח
        </h1>
        <div class="tnx-section-right col-md-3">
            <div class="tnx-shipping-item">
                שם : <?php echo $order->get_shipping_first_name(); ?>
            </div>
            <div class="tnx-shipping-item">
                כתובת : <?php echo $order->get_shipping_address_1() . ' ' . $order->get_shipping_address_2(); ?>
            </div>
            <div class="tnx-shipping-item">
                עיר : <?php echo $order->get_shipping_city(); ?>
            </div>
        </div>
        <div class="tnx-section-left col-md-6">
            <div class="tnx-shipping-item">
                טלפון : <?php echo $order->get_billing_phone(); ?>
            </div>
            <div class="tnx-shipping-item">
                הערות להזמנה : <?php echo isset($order->get_customer_order_notes()[0]) ? $order->get_customer_order_notes()[0] : ''; ?>
            </div>
            <div class="tnx-shipping-item">
                סוג משלוח : <?php echo $order->get_shipping_method(); ?>
            </div>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="tnx-section-contact">
        <div class="tnx-section-contact-top">
            <h1>אפשר להשיג אותנו ממש כאן</h1>
            <p>ימים א-ה 9:00-17:00</p>
        </div>
        <div class="tnx-section-contact-bottom">
            <div class="col-md-2 col-md-offset-3 col-sm-4 col-xs-4"><a href="mailto: "><img class="tnx-section-contact-email" src="<?php echo img('thx-icon-e-mail.png'); ?>"><div>דוא''ל</div></a></div>
            <div class="col-md-2 col-sm-4 col-xs-4"><a href="tel: "><img src="<?php echo img('thx-icon-phone.png'); ?>"><div>0775003000</div></a></div>
            <div class="col-md-2 col-sm-4 col-xs-4"><a href="#"><img src="<?php echo img('thx-icon-facebook.png'); ?>"><div>פייסבוק</div></a></div>
        </div>
            <div style="clear:both;"></div>

    </div>
</div>

<?php endif; ?>

<style>
    .tnx-section {
        margin: 60px 0;
    }

    .blocks img.wp-post-image {
        display: none;
    }

    .bg-image .container, .bg-image .bg-container h1.bold {
        text-align: center;
    }

    .bg-image .bg-container h1.bold {
        color: #e6a4c2;
        width: 400px;
        margin: 0;
        font-weight: 300;
    }

    .tnx-title {
        display: flex;
        justify-content: space-between;
        border-bottom: 1px solid black;
        margin-top: 40px;
        padding-bottom: 15px;
    }

    .tnx-title div, .tnx-title div span {
        font-size: 33px;
        font-weight: 100;
    }

    .tnx-text {
        text-align: center;
        font-size: 22px;
        width: 400px;
        margin: 70px auto;
    }

    .tnx-text h1, .tnx-text h1 span {
        font-size: 30px;
    }

    h1.tnx-order-title {
        border-bottom: 1px solid black;
        font-size: 28px;
        padding-bottom: 20px;
        margin-bottom: 20px;
    }

    .tnx-item {
        display: flex;
        margin: 20px 0;
    }

    .tnx-item:not(:last-child) {
        margin-bottom: 20px;
        padding-bottom: 20px;
        border-bottom: 1px solid black;
    }

    .tnx-item img.wp-post-image {
        width: 200px;
        margin-left: 50px;
    }

    .blocks strong, .blocks span.amount {
        display: block;
        font-weight: 800;
    }

    .gesammelt {
        margin: 40px 0;
        background-color: black;
        color: white;
        font-weight: 400;
        padding: 10px;
        text-align: center;
        letter-spacing: 2px;
    }

    .tnx-total-item {
        margin-bottom: 5px;
    }
    /*------------Yury Code-----------*/
    .blocks .woocommerce-product-gallery__image {
        display: none;
    }
    .tnx-section-contact{
        text-align: center;
        margin: 150px 0;
    }
    .tnx-section-contact p{
        font-size: 25px;
    }
    .tnx-section-contact-bottom .col-md-2 div{
        margin-top: 20px;
        font-size: 25px;
    }
    .tnx-section-contact-email{
        margin-bottom: 7px;
    }
    .tnx-section-contact-top{
        margin-bottom: 40px;
    }
    .tnx-section .order-details-summary p{
        margin-bottom: 5px;
    }
    .tnx-section .order-details-summary .order-details-summary-total{
        font-size: 24px;
        font-weight: bold;
    }
    @media screen and (max-width: 768px){
        .tnx-section-contact-bottom .col-md-2 div{
            font-size: 4vw;
        }
        .tnx-section-contact{
            margin: 10vw 0;
        }
        .tnx-section-contact-top h1{
            font-size: 6vw;
        }
    }
</style>