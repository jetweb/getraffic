<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}
/** @global WC_Checkout $checkout */
?>
    <div class="woocommerce-billing-fields">
        <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>
        <?php
        $fields = $checkout->get_checkout_fields('billing');
        foreach ($fields as $key => $field) {
            if (isset($field['country_field'], $fields[$field['country_field']])) {
                $field['country'] = $checkout->get_value($field['country_field']);
            }
            if (is_user_logged_in() && $key === 'account_password') {
                ?>
                <input type="hidden" class="input-text " name="account_password" id="account_password" placeholder="סיסמא | לצורך מעקב אחר הזמנה" value="password"/>
                <?php
                continue;
            }
            if (is_user_logged_in() && $key === 'accepts_marketing') {
                woocommerce_form_field($key, $field, isset(get_user_meta(get_current_user_id(), 'accepts_marketing')[0]));
            } else {
                if ($key == 'billing_postcode') {
                    $field['required'] = false;
                    $field['validate'] = [];
                }
                woocommerce_form_field($key, $field, $checkout->get_value($key));
            }
        }
        ?>
        <?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
    </div>


<?php if (!is_user_logged_in()) : ?>
    <div class="woocommerce-account-fields">
        <?php if (!$checkout->is_registration_required()) : ?>

            <p class="form-row form-row-wide create-account">
                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                    <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" type="checkbox" checked="checked" name="createaccount" value="<?php
                    $autoCreateAccount = gt_get_field('points_and_club_settings', 'option')['auto_create_account'];
                     if ($autoCreateAccount) {
                         echo '1';
                     } else {
                         echo '0';
                     }

                    ?>"/>
                </label>
            </p>

        <?php endif; ?>

        <?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

        <?php if ($checkout->get_checkout_fields('account')) : ?>

            <div class="create-account">
                <?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
                    <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
                <?php endforeach; ?>

            </div>

        <?php endif; ?>

        <?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
    </div>
<?php endif; ?>
<?php foreach ($checkout->get_checkout_fields('order') as $key => $field) : ?>
    <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
<?php endforeach; ?>

<script>
    jQuery(document).bind('ready', function (e) {
        $('span.woocommerce-input-wrapper input.input-text').each(function (i, el) {
            $(el).before('<span class="preholder">' + $(el).attr('placeholder') + '</span>');
            if ($(el).val()) {
                $(el).parent().find('.preholder').show();
            } else {
                $(el).parent().find('.preholder').hide();
            }
        });
        $('span.woocommerce-input-wrapper input.input-text').bind('change keyup keypress', function () {
            if ($(this).val()) {
                $(this).parent().find('.preholder').show();
            } else {
                $(this).parent().find('.preholder').hide();
            }
        });
    });
</script>
