<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

$summaryFields = gt_get_field('checkout_summary', 'option');

?>
<div class="woocommerce-checkout-review-order-table">
    <h1 class="order-checkout"><?=$summaryFields['title']?></h1>
    <h2 class="order-totals"><?=$summaryFields['subtitle']?></h2>
    <div class="checkout-review-summary slim-scroll">
        <?php get_template_part('templates/checkout/order-summary'); ?>
    </div>
    <div class="woocommerce cart-collaterals">
        <?php get_template_part('templates/checkout/cart-totals'); ?>
        <img src="<?php echo img('payment-logos.png'); ?>"/>
    </div>
</div>






