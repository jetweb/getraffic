<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       3.2.0
 */

if (!defined('ABSPATH')) {
    exit;
}
$currency_symbol = get_woocommerce_currency_symbol( get_woocommerce_currency() );
GT::enqueueAsset('checkout', '/assets/src/scss/checkout.css', [], false, true);
\Getraffic\GTM::pushOrderCompleted($order);
?>
<div class="contact-banner"><img src="<?php echo img('thankyou-banner.png'); ?>"/></div>

<div class="order-container ">
 <?php if ($order->has_status('failed')) : ?>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php _e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.',
                'woocommerce'); ?></p>

        <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
            <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php _e('Pay', 'woocommerce') ?></a>
            <?php if (is_user_logged_in()) : ?>
                <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php _e('My account', 'woocommerce'); ?></a>
            <?php endif; ?>
        </p>

    <?php else : ?>   
    <div class="tnx-text col-md-6 col-md-offset-3 col-sm 12">
        <h1>
            היי
            <span><?php echo $order->get_billing_first_name(); ?></span>
        </h1>
        <p>
            ההזמנתך התקבלה
        </p>
        <div class="tnx-order-num">
            מספר הזמנה:
            <span><?php echo $order->get_order_number(); ?></span>
        </div>
        <p class="tnx-mail-sent">
            <strong>
                אישור הזמנה נשלח למייל:
                <span>
                    <?php echo $order->get_billing_email(); ?>
                </span>
            </strong>
        </p>
    </div>  
</div>
<div style="clear:both"></div>
<div class="order-container orders">   
    <div class="tnx-order col-md-6 col-md-offset-3 col-sm 12">

        <?php
        $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
        foreach ($order_items as $item_id => $item) {
            echo '<div class="tnx-item">';
                $product = $item->get_product();
                GT::getProductMainImageHTML($product);

                echo '<div class="blocks">';
                wc_get_template('order/order-details-item.php', [
                    'order'              => $order,
                    'item_id'            => $item_id,
                    'item'               => $item,
                    'show_purchase_note' => false,
                    'purchase_note'      => $product ? $product->get_purchase_note() : '',
                    'product'            => $product,
                ]);
                echo '</div>';
            echo '</div>';
        }
        ?>
    </div>

</div>
<div style="clear:both"></div>    
<div class="order-container delivery">    

    <div class="tnx-section">
        <h1 class="tnx-order-title">
            פרטי משלוח
        </h1>
        <div class="tnx-section-right shipping-labels">
            <div class="tnx-shipping-item">
                <div class="shipping-item-label">
                    שם :
                </div>
                <div class="shipping-item-value">
                    <?php echo $order->get_billing_first_name() . ' ' . $order->get_billing_last_name(); ?>
                </div>
            </div>
            <?php
            global $GT;
            foreach ($GT->getCustomCheckoutFields()['shipping'] as $name => $field) {
                $title = null;
                if (isset($field['title'])) {
                    $title = $field['title'];
                }
                if (!$title) {
                    $title = $field['placeholder'];
                }
                $value = get_post_meta($order->get_id(), '_' . $name, true);
                if (isset($field['type']) && $field['type'] === 'radio') {
                    $value = $field['options'][$value];
                }

                if (!empty($value)) {
                    ?>
                    <div class="tnx-shipping-item">
                        <div class="shipping-item-label">
                            <?= $title ?> :
                        </div>
                        <div class="shipping-item-value">
                            &nbsp;<?= $value ?>
                        </div>
                    </div>
                <?php } } ?>
            <div class="tnx-shipping-item">
                <div class="shipping-item-label">
                    טלפון נייד :
                </div>
                <div class="shipping-item-value">
                    <?php echo $order->get_billing_phone(); ?>
                </div>
            </div>
            <div class="tnx-shipping-item">
                <div class="shipping-item-label">
                    סוג משלוח :
                </div>
                <div class="shipping-item-value">
                    <?php echo $order->get_shipping_method(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
<div class="order-container summary-total">    

    <div class="tnx-section col-md-6 col-md-offset-3 col-sm 12">

        <div class="order-details-summary">
            <p>סיכום ביניים: <?php echo $currency_symbol . $order->get_subtotal(); ?></p> 
            <p>הטבות מועדון / קוד קופון: <?php echo $currency_symbol . $order->get_discount_total(); ?></p>
            <p>משלוח: <?php echo $order->get_shipping_method(); ?>&nbsp;|&nbsp;<?php echo $currency_symbol . $order->get_total_shipping(); ?></p>
            <p class="order-details-summary-total">סה״כ לתשלום: <?php echo $currency_symbol . $order->get_total(); ?></p>
        </div>
    </div>
</div>

<?php endif; ?>
