<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="product grid-item col-md-4 pl-0 pr-0 <?php echo $product->get_price() ? 'available' : '' ?>" data-cat-ids="<?php echo implode($product->get_category_ids(), ','); ?>">
    <a href="<?php echo $product->get_permalink(); ?>">
        <img src="<?php echo get_the_post_thumbnail_url( $product->get_id(), 'woocommerce_thumbnail'); ?>" alt="" class="thumb">
        <span class="title"><?php echo $product->get_title(); ?></span>
        <img src="<?php echo img('star.png'); ?>" alt="" class="available">
    </a>
</div>
