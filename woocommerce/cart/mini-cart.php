<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */
if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_mini_cart'); ?>

<?php if (!WC()->cart->is_empty()) : ?>
    <p class="woocommerce-mini-cart__empty-message mini-cart-message">
        <?php $cartCount = WC()->cart->get_cart_contents_count(); ?>
        <span class="ltr mini-cart-added"><?php echo gt_get_field('minicart_item_added_text', 'option'); ?></span>
        <span class="ltr mini-cart-count"><?php echo str_replace('[count]', $cartCount, gt_get_field('minicart_item_count_text', 'option')) ?></span>
    </p>
    <ul class="woocommerce-mini-cart slim-scroll cart_list product_list_widget <?php echo esc_attr($args['list_class']); ?>">
        <?php
        do_action('woocommerce_before_mini_cart_contents');

        foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
            $_product   = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
            $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

            if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)) {
                $product_name      = apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key);
                $thumbnail         = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                $product_price     = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key);
                $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item,
                    $cart_item_key);
                ?>
                <li class="woocommerce-mini-cart-item <?php echo esc_attr(apply_filters('woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key)); ?>">
                    <a class="mini-name" href="<?php echo esc_url($product_permalink); ?>">
                        <?php echo $_product->get_title(); ?>
                        <?php
                        $values = GT::getRealAttributeValues($_product);
                        foreach ($values as $term) {
                            echo '<span class="mini-attr">' . urldecode(str_replace('pa_', '', $term->taxonomy)) . ': ' . urldecode($term->name) . '</span>';
                        }
                        ?>
                    </a>
                    <div class="mini-details">
                        <?php if (empty($product_permalink)) : ?>
                            <?php echo $thumbnail . '&nbsp;'; ?>
                        <?php else : ?>
                            <a class="mini-thumb" href="<?php echo esc_url($product_permalink); ?>">
                                <?php echo $thumbnail . '&nbsp;'; ?>
                            </a>
                        <?php endif; ?>
                        <?php
                        if ($_product->is_on_sale()) {
                            ?>
                            <span class="woocommerce-Price-amount amount old-price">
                                    <span class="woocommerce-Price-currencySymbol">₪</span><?=$_product->get_regular_price();?></span>
                            <br>
                            <?php
                        }
                        ?>
                        <?php echo apply_filters('woocommerce_widget_cart_item_quantity',
                            $product_price, $cart_item, $cart_item_key);
                        ?>
                        <div class="mini-cart-quantity">
                            <a class="set-item-quantity" data-item-key="<?php echo $cart_item['key']; ; ?>"
                               data-item-target-quantity="<?php echo $cart_item['quantity'] + 1; ?>">+</a>
                            <input class="actual-quantity" value="<?php echo $cart_item['quantity']; ?>" data-item-key="<?php echo $cart_item['key']; ?>" />
                            <a class="set-item-quantity" data-item-key="<?php echo $cart_item['key']; ?>" data-item-target-quantity="<?php echo $cart_item['quantity'] - 1; ?>">-</a>
                        </div>
                    </div>
                    <?php
                    echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
                        '<a href="%s" class="remove remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">&times;</a>',
                        esc_url(wc_get_cart_remove_url($cart_item_key)),
                        __('Remove this item', 'woocommerce'),
                        esc_attr($product_id),
                        esc_attr($cart_item_key),
                        esc_attr($_product->get_sku())
                    ), $cart_item_key);
                    ?>
                    <div class="clearfix"></div>

                </li>
                <?php
            }
        }

        do_action('woocommerce_mini_cart_contents');
        ?>
    </ul>
    <div class="cart-padding">
        <p class="woocommerce-mini-cart__total total"><strong><?php _e('Subtotal', 'woocommerce'); ?> | </strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p>

        <?php do_action('woocommerce_widget_shopping_cart_before_buttons'); ?>

        <div class="mini-cart-buttons">
            <a href="<?php echo wc_get_checkout_url(); ?>" class="bu-button sharp-corners wide">
                <?= gt_get_field('minicart_checkout_link', 'option'); ?>
            </a>
            <a href="<?php echo wc_get_cart_url(); ?>" class="mini-cart-cart-link">
                <?= gt_get_field('minicart_cart_link', 'option'); ?>
            </a>
        </div>

        <div class="free-shipping-from">
            <?php
            $shippingFrom         = gt_get_field('free_shipping_from', 'option');
            $textFreeShippingFrom = gt_get_field('free_shipping_from_text', 'option');
            $textAddMore          = gt_get_field('free_shipping_add_more_text', 'option');
            $textAcheived         = gt_get_field('free_shipping_achieved_text', 'option');
            $subTotal             = WC()->cart->get_subtotal();
            ?>
            <?php if (($shippingFrom - $subTotal) > 0) { ?>
                <span class="add-more"><?php echo str_replace('[amount]', '₪' . ($shippingFrom - $subTotal), $textAddMore); ?></span>
            <?php } else { ?>
                <span class="add-more"><?= $textAcheived ?></span>
            <?php } ?>
            <span class="free-shipping"><?php echo str_replace('[amount]', '₪' . $shippingFrom, $textFreeShippingFrom); ?></span>
        </div>
    </div>
<?php
else :
    ?>
    <div class="cart-padding">
        <p class="cart-invite">
            <?php echo gt_get_field('mini_cart_empty_text', 'option'); ?>
        </p>
        <a href="/shop" class="start-shopping-button">
            <?php echo gt_get_field('mini_cart_start_shopping_button', 'option'); ?>
        </a>
    </div>
<?php endif; ?>

<?php do_action('woocommerce_after_mini_cart'); ?>

<script>
    $(document).ready(function() {
        bindMiniCartFunctions();
    });
</script>
