<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php //printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<?php /* translators: %s: Customer first name */ ?>
<h1><?php printf( 'Let the party started!' ); ?></h1>
<p><?php printf( esc_html__( 'היי %s, הזמנתך נקלטה במערכת והצוות שלנו כבר אוסף את המוצרים, אורז ושולח לכתובת שהזנת. המשלוח יגיע אלייך עד 3 ימי עסקים אז יש לך מספיק זמן לפנות מקום על שידת האיפור והטיפוח שלך 😉 תתחדשי!', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>

<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>
<p>
<?php esc_html_e( 'תודה!', 'woocommerce' ); ?>
</p>

<style>
    table, th, td{
        border: 0 !important;
    }
    h1{
        text-align: center;
        color: #000000 !important;
    }
    h3{
        font-size: 30px;
        text-align: center;
        background-color: #000;
        color: #ffffff !important;
        margin: -20px 0 20px 0 !important;
        padding: 20px 0;
        font-weight: normal;
    }
    tfoot tr th, tfoot tr td{
        padding: 25px 8px 0 0 !important;
    }
    tfoot tr th span, tfoot tr td span{
        font-weight: normal !important;
        line-height: 36px;
    }
    tfoot tr th, tfoot tr td{
        font-weight: bold !important;
    }
    th, td{
        font-size: 20px;
    }
    p{
        margin: 0 auto 50px !important;
        font-size: 26px;
        text-align: center;
        color: #000;  
        line-height: 35px;
        width: 80%;
        
    }
    address{
        border: 0 !important;
        font-style: normal;
        color:#000000 !important;
        line-height: 34px;
    }
    h2{
        color: #000;
        font-weight: normal;
        font-size: 30px;
        padding-top: 20px;
        border-top: 3px solid #000;
    }
</style>
<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
