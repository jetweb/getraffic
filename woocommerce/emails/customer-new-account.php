<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<?php //do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s Customer username */ ?>
<p class="wellcome"><?php printf( esc_html__( 'Hi %s', 'woocommerce' ), esc_html( $user_login ) ); ?></p>
<?php /* translators: %1$s: Site title, %2$s: Username, %3$s: My account link */ ?>

<p class="creating-account"><?php printf( __( 'ברוכה הבאה למועדון החברות של %1$s!<br> אנחנו כאן כדי לעדכן אותך בחדשות, מבצעים והטבות <br>לספר לך על מוצרים שאת חייבת בתיק, וכאלה שאת חייבת על שידת האיפור<br>ולדאוג שהחיוך, לא ירד לך מהפנים.<br>מתאים?', 'woocommerce' ), esc_html( $blogname ), '<strong>' . esc_html( $user_login ) . '</strong>', make_clickable( esc_url( wc_get_page_permalink( 'myaccount' ) ) ) ); ?></p><?php // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped ?>

<?php if ( 'yes' === get_option( 'woocommerce_registration_generate_password' ) && $password_generated ) : ?>
	<?php /* translators: %s Auto generated password */ ?>
	<p><?php printf( esc_html__( 'Your password has been automatically generated: %s', 'woocommerce' ), '<strong>' . esc_html( $user_pass ) . '</strong>' ); ?></p>
<?php endif; ?>

<p class="see-you-soon"><?php printf('יאללה, מתראות שוב בקרוב<br>ועד אז, את יותר ממוזמנת לעקוב:'); ?></p>
<p class="social-icons"><a href="#"><img src="<?php echo img('mail-icon-facebook.png'); ?>"></a><a href="#"><img src="<?php echo img('mail-icon-instagram.png'); ?>"></a></p>
<p class="hashtags"><?php printf('#Ready? #Selfie!<br>#TonyMoly'); ?></p>
<div style="height: 20px; background-color: #000000; margin-top: 100px;"></div>
<p style="text-align: center; direction: rtl; font-size: 20px; margin-top: 10px;">כל הזכויות שמורות 2019</p>
<style>
    p{
        text-align: center;
        font-size: 20px;
        direction: rtl;
        color: #000000;
    }
    .wellcome{
        font-weight: bold;
    }
</style>
<?php //do_action( 'woocommerce_email_footer', $email );
