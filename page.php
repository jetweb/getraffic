<?php
/**
 * Template Name: Page without sidebar
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

get_header(); ?>

<div class="container" id="primary">
    <div class="content">
        <?php
        if (have_posts()) {
            while (have_posts()) : the_post();
                ?>
                <h1 class="page-title"><?php echo the_title(); ?></h1>
                <?php the_content(); ?>
            <?php
            endwhile;
        }
        ?>
    </div>
</div>
<?php get_footer(); ?>
