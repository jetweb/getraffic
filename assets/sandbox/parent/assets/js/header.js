let lastOpenedMobileSubMenu = -1;
jQuery(document).ready(function () {
    let $ = jQuery;
    $('.hamburger, .mobile-menu-close').bind('click', function (e) {
        toggleMobileMenu();

        return false;
    });


    $('#menu-mobile a.back').bind('click', function () {
        if (lastOpenedMobileSubMenu != -1) {
            $('#menu-mobile ul li').hide();
            $('#menu-mobile ul.submenu').hide();
            $('li[data-item-id="' + lastOpenedMobileSubMenu + '"]').trigger('click');
        } else {
            $('#menu-mobile ul li').show();
            $('#menu-mobile ul.submenu').hide();
            $('.mobile-menu-back-button').hide();
        }
        return false;
    });

    $(document).on('woocommerce_variation_has_changed', function(event) {
        $('select.woo-variation-raw-select').each(function() {
            let elem = $(this).parent().find('li[data-value="' + $(this).val() + '"]');
            setTimeout(function() {
                elem.addClass('selected');
            }, 100);

        });
    });

    $('select.woo-variation-raw-select').bind('change', function(e, value) {
        if ($(this).val() === null && $(this).data('prev-val') !== null) {
            $(this).val($(this).data('prev-val'));
        }
        $(this).data('prev-val', $(this).val());
    });

    $('ul.variable-items-wrapper li').bind('click', function(e) {
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
            return false;
        }
    });

    initHoverThumb();
    $('.mini-cart-toggle').bind('click', (e) => {toggleMiniCart();});
});

function openSubMenuForParent(parentId, backText, backUrl) {
    $('#menu-mobile ul li').hide();
    $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"]').show();
    $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"] li').show();
    lastOpenedMobileSubMenu = $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"]').data('parent-id');
    let linkToAll = $('.mobile-menu-back-button .to-all');
    linkToAll.text(backText);
    linkToAll.attr('href', backUrl);
    $('.mobile-menu-back-button').show();
}

function toggleMiniCart(mode = 'toggle') {
    if (mode === 'toggle') {
        $('.mini-cart-wrapper').toggle(400);
        $('.topicon.minicart').toggleClass('active');
    } else if (mode === 'close') {
        $('.mini-cart-wrapper').hide(400);
        $('.topicon.minicart').removeClass('active');
    }
    $(document).trigger('mini_cart_toggled');
}

function toggleMobileMenu() {
    $('.hamburger').toggleClass('is-active');
    let menu = $('#menu-mobile');
    menu.toggleClass('active');
    $(document).trigger('mobile-menu-toggled');
}
