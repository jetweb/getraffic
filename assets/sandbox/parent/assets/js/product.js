$(document).ready(function () {
    $('.info-block-title').bind('click', function () {
        let aminationDuration = 200;
        if ($(this).parent().hasClass('collapsed')) {
            $(this).parent().find('.info-block-content').show(aminationDuration);
            $(this).parent().removeClass('collapsed');
        } else {
            $(this).parent().find('.info-block-content').hide(aminationDuration);
            $(this).parent().addClass('collapsed');
        }
    });

    $(window).resize(function () {
        setGallery();
    });
    setGallery();
});

function setGallery() {
    if ($(window).width() <= 991) {
        $('.type-product .woocommerce-product-gallery').appendTo('.type-product .mobile-gallery');
    } else {
        $('.type-product .woocommerce-product-gallery').appendTo('.type-product .desktop-gallery');
    }
}