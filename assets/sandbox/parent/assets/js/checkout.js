let popupAlreadyShown = false;
let currentStep = 'billing';

$(document).on('checkout_error', function (e, data) {
    $('.checkout-message').html($('.woocommerce-NoticeGroup-checkout').html());
    $('#nav-billing-tab').click();
});

$(document).on('update_checkout', function () {
    jQuery('.woocommerce-checkout-review-order-table, .woocommerce-checkout-payment').unblock();
    jQuery('.checkout-shipping').block({
        message: null,
        overlayCSS: {
            background: '#fff',
            opacity: 0.6
        }
    });
});

$(document).on('updated_checkout', function () {
    jQuery('.checkout-shipping').unblock();
});

$(document).ready(function () {
    $('#registerModal').remove();

    $('#triggerCoupon').bind('click', function(e) {
        if (typeof(overrideCouponBehaviour) !== 'undefined' && overrideCouponBehaviour) {
            return false;
        }

        e.preventDefault();
        e.stopPropagation();

        triggerCouponAction();
        return false;
    });

    $('button#apply_points').bind('click', function(e) {
        let data = 'action=applyPoints';
        $('#ywpar_input_points_check').val(1);
        $('#yith-par-message-reward-cart input').each(function() {
            data += '&' + $(this).attr('name') + '=' + $(this).val();
        });
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: ajax_url,
            data: data,
            complete: function () {

            },
            success: function (response) {
                $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
            },
        });
        return false;
    });

    showStep('billing');
    toggleShippingFields();
    $('input.shipping_method').bind('change click', toggleShippingFields);

    initJoinUsPopup();

    $('#nav-billing input').keypress(function(e) {
        if (e.keyCode === 13) {
            validateCheckout();

            return false;
        }
    });

    $('input.input-coupon').keypress(function (e) {
        if (e.keyCode === 13) {
            $('#triggerCoupon').trigger('click');
            return false;
        }
    });

    $('#account_password').keyup(function() {
        if ($(this).val().length) {
            $('input[name=createaccount]').val('1');
        } else {
            $('input[name=createaccount]').val('0');
        }
    });
});

function initJoinUsPopup() {
    $('.join-us-button').bind('click', function() {
        var data = {
            action: 'join_club',
            bdate_day: $('#bdate_day').val(),
            bdate_month: $('#bdate_month').val(),
            bdate_year: $('#bdate_year').val()
        };

        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                $('span.success').css('display', 'block');
                setTimeout(function() {
                    $('#joinUsModal').modal('hide');
                    showStep('coupons');
                    $('.checkout-points').show();
                }, 1200);
            },
        });

        return false;
    });
}

function joinClub() {
    var data = {
        action: 'join_club',
    };

    $.ajax({
        type: 'post',
        url: wc_add_to_cart_params.ajax_url,
        data: data,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            $(document.body).trigger('joined_club');
        },
    });
}

function toggleShippingFields() {
    let shippingFields = $('.checkout-shipping-fields');
    $('input[type="radio"]').each(function () {
        if ($(this).is(':checked') && ($(this).attr('name').indexOf('shipping_method') !== -1)) {
            if (
                $(this).val().indexOf('local_pickup') !== -1 ||
                $(this).val().indexOf('boxit') !== -1 ||
                $(this).val().indexOf('epost') !== -1
            ) {
                shippingFields.hide(400);
            } else {
                $(this).closest('li').append(shippingFields);
                shippingFields.show(400);
            }
        }
    });

    $('.shipping-fields-container input').keypress(function(e) {
        if (e.keyCode === 13) {
            return false;
        }
    });
}

function proceedToSecondStep() {
    let joinPopup = $('#joinUsModal');
    let popupExists = joinPopup.length > 0;
    if (isUserLoggedIn && !isUserInClub && !popupAlreadyShown && popupExists) {
        joinPopup.modal('show');
        popupAlreadyShown = true;
    } else if (!isUserLoggedIn && !isUserInClub && popupExists) {
        joinPopup.modal('show');
    } else {
        showStep('coupons');
    }
}

// Steps are : billing, coupons, delivery, payment
function showStep(stepName) {
    if (stepName === 'coupons') {
        if (autoJoinClub) {
            joinClub();
        }
        else if (enableJoinPopup && !popupAlreadyShown && !isUserInClub) {
            proceedToSecondStep();
            return;
        }
    }
    $('.checkout-wizard-step:not(#nav-' + stepName + ')').each(function() {
        $(this).find('.wizard-item-content').hide(400);
        $(this).removeClass('active');
    });

    $('#nav-' + stepName).find('.wizard-item-content').show(400);
    $('#nav-' + stepName).addClass('active');
    $(document).trigger('showing_checkout_step', stepName);

    if (stepName === 'billing') {
        $('.order-next-stage').show();
    } else {
        $('.order-next-stage').hide();
    }

    if (stepName === 'delivery') {
        $('.order-total').show(400);
    }

    currentStep = stepName;

    return false;
}

function validateCheckout() {
    $('.checkout-message').html('');
    $('form.checkout .input-text, form.checkout select, form.checkout input:checkbox').trigger('validate').blur();
    if ($('form.checkout .woocommerce-invalid').length) {
        return;
    }
    let values = 'action=validateCheckout&' + $('form.checkout').serialize();
    $.ajax({
        type: 'post',
        url: ajax_url,
        data: values,
        success: function (response) {
            console.log(response);
            if (response.result === 'failure') {
                $('.checkout-message').html(response.messages);
                $('.checkout-message')[0].scrollIntoView({block: "end", inline: "nearest"});
            } else {
                showStep('coupons');
            }
        },
    });
    return false;
}

$(document).on('init_checkout', function () {
    setTimeout(function() {
        $('.woocommerce-remove-coupon').unbind('click');
        $( document.body ).off( 'click', '.woocommerce-remove-coupon');
        $( document.body ).on( 'click', '.woocommerce-remove-coupon', removeCouponAction );
    }, 1200)
});

function triggerCouponAction() {
    var data = {
        security:		wc_checkout_params.apply_coupon_nonce,
        coupon_code:	$( 'input[name="coupon_code"]' ).val()
    };

    $.ajax({
        type:		'POST',
        url:		wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'apply_coupon' ),
        data:		data,
        success:	function( code ) {
            $( '.woocommerce-error, .woocommerce-message' ).remove();

            if ( code ) {
                $('.coupon-message').before( code );
                $('#nav-coupons-tab').click();
                $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
            }
        },
        dataType: 'html'
    });
}

function removeCouponAction(e) {
    e.preventDefault();
    e.stopPropagation();
    var container = $( this ).parents( '.woocommerce-checkout-review-order' ),
        coupon    = $( this ).data( 'coupon' );

    container.addClass( 'processing' ).block({
        message: null,
        overlayCSS: {
            background: '#fff',
            opacity: 0.6
        }
    });

    var data = {
        security: wc_checkout_params.remove_coupon_nonce,
        coupon:   coupon
    };

    $.ajax({
        type:    'POST',
        url:     wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_coupon' ),
        data:    data,
        success: function( code ) {
            $( '.woocommerce-error, .woocommerce-message' ).remove();
            container.removeClass( 'processing' ).unblock();

            if ( code ) {
                $('.coupon-message').before( code );
                $('#nav-coupons-tab').click();

                $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );

                // Remove coupon code from coupon field
                $( 'form.checkout_coupon' ).find( 'input[name="coupon_code"]' ).val( '' );
            }
        },
        error: function ( jqXHR ) {
            if ( wc_checkout_params.debug_mode ) {
                /* jshint devel: true */
                console.log( jqXHR.responseText );
            }
        },
        dataType: 'html'
    });

    return false;
}

$(document).on('showing_checkout_step', function (e, a) {
    console.log('showing step ', a);
    $('.wizard-title').off('click');
    if (a === 'coupons' || a === 'delivery') {
        $('.wizard-title.billing').on('click', function() {
            showStep('billing');
        });
    }
    if (a === 'delivery') {
        $('.wizard-title.coupons').on('click', function() {
            showStep('coupons');
        });
    }

    switch (a) {
        case 'coupons':
            if (typeof pushCheckoutStep !== "undefined") {
                pushCheckoutStep(2);
            }
            break;
        case 'delivery':
            if (typeof pushCheckoutStep !== "undefined") {
                pushCheckoutStep(3);
            }
            break;
        default:
            break;
    }
});

$( document.body ).on( 'updated_checkout', function(e, data) {
    $('#wc-pelecard-number-of-payments').val('1');
    $('#wc-pelecard-payment-token-new').attr('checked', 'checked');
    toggleShippingFields();

    $('.checkout-buttons a.checkout-btn').click(function() {
        let method = $(this).data('payment-method');

        let valid = true;
        $('.shipping-fields-container .form-row.validate-required').each(function() {
            if (!validateFormElement($(this).find('input'))) {
                valid = false;
            }
        });

        // accept terms
        $('.has-to-accept-terms').remove();
        let acceptTerms = $('#accept_terms');
        if (acceptTerms.length) {
            if (!acceptTerms.is(':checked')) {
                acceptTerms.parent().append('<span class="has-to-accept-terms">' + termsErrorMessage + '</span>');
                valid = false;
            }
        }

        if (!valid) {
            return false;
        }

        $('input#payment_method_' + method).trigger('click').trigger('change');
        $('.submit-checkout').trigger('click');
        return false;
    });

    if (currentStep === 'delivery') {
        $('.order-total').show(400);
        $('input.shipping_method:checked')[0].parentElement.scrollIntoView({block: 'center', behavior: 'smooth'});
    }

    if (currentStep === 'billing') {
        $('.order-next-stage').show();
    }
} );

$( document.body ).on( 'checkout_error', function() {
    $('#nav-' + currentStep + ' .wizard-item-content').prepend($('ul.woocommerce-error'));
} );


$( document ).ready(function() {
    $('form.checkout').off( 'input validate change', '.input-text, select, input:checkbox');
    $('form.checkout .input-text, form.checkout select, form.checkout input:checkbox, form.checkout input:password').on('validate change blur', function(e) {
        validateFormElement($(this), e.type);
    });

    if (typeof pushCheckoutStep !== "undefined") {
        pushCheckoutStep(1);
    }
});
