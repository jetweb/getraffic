!function (e, n) {
  "object" == typeof exports && "undefined" != typeof module ? n() : "function" == typeof define && define.amd ? define(n) : n();
}(0, function () {
  "use strict";

  function e(e) {
    var n = this.constructor;
    return this.then(function (t) {
      return n.resolve(e()).then(function () {
        return t;
      });
    }, function (t) {
      return n.resolve(e()).then(function () {
        return n.reject(t);
      });
    });
  }

  function n(e) {
    return !(!e || "undefined" == typeof e.length);
  }

  function t() {}

  function o(e) {
    if (!(this instanceof o)) throw new TypeError("Promises must be constructed via new");
    if ("function" != typeof e) throw new TypeError("not a function");
    this._state = 0, this._handled = !1, this._value = undefined, this._deferreds = [], c(e, this);
  }

  function r(e, n) {
    for (; 3 === e._state;) e = e._value;

    0 !== e._state ? (e._handled = !0, o._immediateFn(function () {
      var t = 1 === e._state ? n.onFulfilled : n.onRejected;

      if (null !== t) {
        var o;

        try {
          o = t(e._value);
        } catch (r) {
          return void f(n.promise, r);
        }

        i(n.promise, o);
      } else (1 === e._state ? i : f)(n.promise, e._value);
    })) : e._deferreds.push(n);
  }

  function i(e, n) {
    try {
      if (n === e) throw new TypeError("A promise cannot be resolved with itself.");

      if (n && ("object" == typeof n || "function" == typeof n)) {
        var t = n.then;
        if (n instanceof o) return e._state = 3, e._value = n, void u(e);
        if ("function" == typeof t) return void c(function (e, n) {
          return function () {
            e.apply(n, arguments);
          };
        }(t, n), e);
      }

      e._state = 1, e._value = n, u(e);
    } catch (r) {
      f(e, r);
    }
  }

  function f(e, n) {
    e._state = 2, e._value = n, u(e);
  }

  function u(e) {
    2 === e._state && 0 === e._deferreds.length && o._immediateFn(function () {
      e._handled || o._unhandledRejectionFn(e._value);
    });

    for (var n = 0, t = e._deferreds.length; t > n; n++) r(e, e._deferreds[n]);

    e._deferreds = null;
  }

  function c(e, n) {
    var t = !1;

    try {
      e(function (e) {
        t || (t = !0, i(n, e));
      }, function (e) {
        t || (t = !0, f(n, e));
      });
    } catch (o) {
      if (t) return;
      t = !0, f(n, o);
    }
  }

  var a = setTimeout;
  o.prototype["catch"] = function (e) {
    return this.then(null, e);
  }, o.prototype.then = function (e, n) {
    var o = new this.constructor(t);
    return r(this, new function (e, n, t) {
      this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof n ? n : null, this.promise = t;
    }(e, n, o)), o;
  }, o.prototype["finally"] = e, o.all = function (e) {
    return new o(function (t, o) {
      function r(e, n) {
        try {
          if (n && ("object" == typeof n || "function" == typeof n)) {
            var u = n.then;
            if ("function" == typeof u) return void u.call(n, function (n) {
              r(e, n);
            }, o);
          }

          i[e] = n, 0 == --f && t(i);
        } catch (c) {
          o(c);
        }
      }

      if (!n(e)) return o(new TypeError("Promise.all accepts an array"));
      var i = Array.prototype.slice.call(e);
      if (0 === i.length) return t([]);

      for (var f = i.length, u = 0; i.length > u; u++) r(u, i[u]);
    });
  }, o.resolve = function (e) {
    return e && "object" == typeof e && e.constructor === o ? e : new o(function (n) {
      n(e);
    });
  }, o.reject = function (e) {
    return new o(function (n, t) {
      t(e);
    });
  }, o.race = function (e) {
    return new o(function (t, r) {
      if (!n(e)) return r(new TypeError("Promise.race accepts an array"));

      for (var i = 0, f = e.length; f > i; i++) o.resolve(e[i]).then(t, r);
    });
  }, o._immediateFn = "function" == typeof setImmediate && function (e) {
    setImmediate(e);
  } || function (e) {
    a(e, 0);
  }, o._unhandledRejectionFn = function (e) {
    void 0 !== console && console && console.warn("Possible Unhandled Promise Rejection:", e);
  };

  var l = function () {
    if ("undefined" != typeof self) return self;
    if ("undefined" != typeof window) return window;
    if ("undefined" != typeof global) return global;
    throw Error("unable to locate global object");
  }();

  "Promise" in l ? l.Promise.prototype["finally"] || (l.Promise.prototype["finally"] = e) : l.Promise = o;
});
!function r(c, a, f) {
  function o(n, t) {
    if (!a[n]) {
      if (!c[n]) {
        var e = "function" == typeof require && require;
        if (!t && e) return e(n, !0);
        if (s) return s(n, !0);
        var i = new Error("Cannot find module '" + n + "'");
        throw i.code = "MODULE_NOT_FOUND", i;
      }

      var u = a[n] = {
        exports: {}
      };
      c[n][0].call(u.exports, function (t) {
        return o(c[n][1][t] || t);
      }, u, u.exports, r, c, a, f);
    }

    return a[n].exports;
  }

  for (var s = "function" == typeof require && require, t = 0; t < f.length; t++) o(f[t]);

  return o;
}({
  1: [function (t, n, r) {
    "use strict";

    t(2);

    var e = function _interopRequireDefault(t) {
      return t && t.__esModule ? t : {
        default: t
      };
    }(t(15));

    e.default._babelPolyfill && "undefined" != typeof console && console.warn && console.warn("@babel/polyfill is loaded more than once on this page. This is probably not desirable/intended and may have consequences if different versions of the polyfills are applied sequentially. If you do need to load the polyfill more than once, use @babel/polyfill/noConflict instead to bypass the warning."), e.default._babelPolyfill = !0;
  }, {
    15: 15,
    2: 2
  }],
  2: [function (t, n, r) {
    "use strict";

    t(3), t(5), t(4), t(11), t(10), t(13), t(12), t(14), t(7), t(8), t(6), t(9), t(306), t(307);
  }, {
    10: 10,
    11: 11,
    12: 12,
    13: 13,
    14: 14,
    3: 3,
    306: 306,
    307: 307,
    4: 4,
    5: 5,
    6: 6,
    7: 7,
    8: 8,
    9: 9
  }],
  3: [function (t, n, r) {
    t(278), t(214), t(216), t(215), t(218), t(220), t(225), t(219), t(217), t(227), t(226), t(222), t(223), t(221), t(213), t(224), t(228), t(229), t(180), t(182), t(181), t(231), t(230), t(201), t(211), t(212), t(202), t(203), t(204), t(205), t(206), t(207), t(208), t(209), t(210), t(184), t(185), t(186), t(187), t(188), t(189), t(190), t(191), t(192), t(193), t(194), t(195), t(196), t(197), t(198), t(199), t(200), t(265), t(270), t(277), t(268), t(260), t(261), t(266), t(271), t(273), t(256), t(257), t(258), t(259), t(262), t(263), t(264), t(267), t(269), t(272), t(274), t(275), t(276), t(175), t(177), t(176), t(179), t(178), t(163), t(161), t(168), t(165), t(171), t(173), t(160), t(167), t(157), t(172), t(155), t(170), t(169), t(162), t(166), t(154), t(156), t(159), t(158), t(174), t(164), t(247), t(248), t(254), t(249), t(250), t(251), t(252), t(253), t(232), t(183), t(255), t(290), t(291), t(279), t(280), t(285), t(288), t(289), t(283), t(286), t(284), t(287), t(281), t(282), t(233), t(234), t(235), t(236), t(237), t(240), t(238), t(239), t(241), t(242), t(243), t(244), t(246), t(245), n.exports = t(52);
  }, {
    154: 154,
    155: 155,
    156: 156,
    157: 157,
    158: 158,
    159: 159,
    160: 160,
    161: 161,
    162: 162,
    163: 163,
    164: 164,
    165: 165,
    166: 166,
    167: 167,
    168: 168,
    169: 169,
    170: 170,
    171: 171,
    172: 172,
    173: 173,
    174: 174,
    175: 175,
    176: 176,
    177: 177,
    178: 178,
    179: 179,
    180: 180,
    181: 181,
    182: 182,
    183: 183,
    184: 184,
    185: 185,
    186: 186,
    187: 187,
    188: 188,
    189: 189,
    190: 190,
    191: 191,
    192: 192,
    193: 193,
    194: 194,
    195: 195,
    196: 196,
    197: 197,
    198: 198,
    199: 199,
    200: 200,
    201: 201,
    202: 202,
    203: 203,
    204: 204,
    205: 205,
    206: 206,
    207: 207,
    208: 208,
    209: 209,
    210: 210,
    211: 211,
    212: 212,
    213: 213,
    214: 214,
    215: 215,
    216: 216,
    217: 217,
    218: 218,
    219: 219,
    220: 220,
    221: 221,
    222: 222,
    223: 223,
    224: 224,
    225: 225,
    226: 226,
    227: 227,
    228: 228,
    229: 229,
    230: 230,
    231: 231,
    232: 232,
    233: 233,
    234: 234,
    235: 235,
    236: 236,
    237: 237,
    238: 238,
    239: 239,
    240: 240,
    241: 241,
    242: 242,
    243: 243,
    244: 244,
    245: 245,
    246: 246,
    247: 247,
    248: 248,
    249: 249,
    250: 250,
    251: 251,
    252: 252,
    253: 253,
    254: 254,
    255: 255,
    256: 256,
    257: 257,
    258: 258,
    259: 259,
    260: 260,
    261: 261,
    262: 262,
    263: 263,
    264: 264,
    265: 265,
    266: 266,
    267: 267,
    268: 268,
    269: 269,
    270: 270,
    271: 271,
    272: 272,
    273: 273,
    274: 274,
    275: 275,
    276: 276,
    277: 277,
    278: 278,
    279: 279,
    280: 280,
    281: 281,
    282: 282,
    283: 283,
    284: 284,
    285: 285,
    286: 286,
    287: 287,
    288: 288,
    289: 289,
    290: 290,
    291: 291,
    52: 52
  }],
  4: [function (t, n, r) {
    t(292), n.exports = t(52).Array.flatMap;
  }, {
    292: 292,
    52: 52
  }],
  5: [function (t, n, r) {
    t(293), n.exports = t(52).Array.includes;
  }, {
    293: 293,
    52: 52
  }],
  6: [function (t, n, r) {
    t(294), n.exports = t(52).Object.entries;
  }, {
    294: 294,
    52: 52
  }],
  7: [function (t, n, r) {
    t(295), n.exports = t(52).Object.getOwnPropertyDescriptors;
  }, {
    295: 295,
    52: 52
  }],
  8: [function (t, n, r) {
    t(296), n.exports = t(52).Object.values;
  }, {
    296: 296,
    52: 52
  }],
  9: [function (t, n, r) {
    "use strict";

    t(232), t(297), n.exports = t(52).Promise.finally;
  }, {
    232: 232,
    297: 297,
    52: 52
  }],
  10: [function (t, n, r) {
    t(298), n.exports = t(52).String.padEnd;
  }, {
    298: 298,
    52: 52
  }],
  11: [function (t, n, r) {
    t(299), n.exports = t(52).String.padStart;
  }, {
    299: 299,
    52: 52
  }],
  12: [function (t, n, r) {
    t(301), n.exports = t(52).String.trimRight;
  }, {
    301: 301,
    52: 52
  }],
  13: [function (t, n, r) {
    t(300), n.exports = t(52).String.trimLeft;
  }, {
    300: 300,
    52: 52
  }],
  14: [function (t, n, r) {
    t(302), n.exports = t(151).f("asyncIterator");
  }, {
    151: 151,
    302: 302
  }],
  15: [function (t, n, r) {
    t(32), n.exports = t(18).global;
  }, {
    18: 18,
    32: 32
  }],
  16: [function (t, n, r) {
    n.exports = function (t) {
      if ("function" != typeof t) throw TypeError(t + " is not a function!");
      return t;
    };
  }, {}],
  17: [function (t, n, r) {
    var e = t(28);

    n.exports = function (t) {
      if (!e(t)) throw TypeError(t + " is not an object!");
      return t;
    };
  }, {
    28: 28
  }],
  18: [function (t, n, r) {
    var e = n.exports = {
      version: "2.6.11"
    };
    "number" == typeof __e && (__e = e);
  }, {}],
  19: [function (t, n, r) {
    var o = t(16);

    n.exports = function (e, i, t) {
      if (o(e), void 0 === i) return e;

      switch (t) {
        case 1:
          return function (t) {
            return e.call(i, t);
          };

        case 2:
          return function (t, n) {
            return e.call(i, t, n);
          };

        case 3:
          return function (t, n, r) {
            return e.call(i, t, n, r);
          };
      }

      return function () {
        return e.apply(i, arguments);
      };
    };
  }, {
    16: 16
  }],
  20: [function (t, n, r) {
    n.exports = !t(23)(function () {
      return 7 != Object.defineProperty({}, "a", {
        get: function () {
          return 7;
        }
      }).a;
    });
  }, {
    23: 23
  }],
  21: [function (t, n, r) {
    var e = t(28),
        i = t(24).document,
        o = e(i) && e(i.createElement);

    n.exports = function (t) {
      return o ? i.createElement(t) : {};
    };
  }, {
    24: 24,
    28: 28
  }],
  22: [function (t, n, r) {
    var g = t(24),
        y = t(18),
        d = t(19),
        x = t(26),
        m = t(25),
        S = "prototype",
        b = function (t, n, r) {
      var e,
          i,
          o,
          u = t & b.F,
          c = t & b.G,
          a = t & b.S,
          f = t & b.P,
          s = t & b.B,
          l = t & b.W,
          h = c ? y : y[n] || (y[n] = {}),
          p = h[S],
          v = c ? g : a ? g[n] : (g[n] || {})[S];

      for (e in c && (r = n), r) (i = !u && v && void 0 !== v[e]) && m(h, e) || (o = i ? v[e] : r[e], h[e] = c && "function" != typeof v[e] ? r[e] : s && i ? d(o, g) : l && v[e] == o ? function (e) {
        function qb(t, n, r) {
          if (this instanceof e) {
            switch (arguments.length) {
              case 0:
                return new e();

              case 1:
                return new e(t);

              case 2:
                return new e(t, n);
            }

            return new e(t, n, r);
          }

          return e.apply(this, arguments);
        }

        return qb[S] = e[S], qb;
      }(o) : f && "function" == typeof o ? d(Function.call, o) : o, f && ((h.virtual || (h.virtual = {}))[e] = o, t & b.R && p && !p[e] && x(p, e, o)));
    };

    b.F = 1, b.G = 2, b.S = 4, b.P = 8, b.B = 16, b.W = 32, b.U = 64, b.R = 128, n.exports = b;
  }, {
    18: 18,
    19: 19,
    24: 24,
    25: 25,
    26: 26
  }],
  23: [function (t, n, r) {
    n.exports = function (t) {
      try {
        return !!t();
      } catch (t) {
        return !0;
      }
    };
  }, {}],
  24: [function (t, n, r) {
    var e = n.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")();
    "number" == typeof __g && (__g = e);
  }, {}],
  25: [function (t, n, r) {
    var e = {}.hasOwnProperty;

    n.exports = function (t, n) {
      return e.call(t, n);
    };
  }, {}],
  26: [function (t, n, r) {
    var e = t(29),
        i = t(30);
    n.exports = t(20) ? function (t, n, r) {
      return e.f(t, n, i(1, r));
    } : function (t, n, r) {
      return t[n] = r, t;
    };
  }, {
    20: 20,
    29: 29,
    30: 30
  }],
  27: [function (t, n, r) {
    n.exports = !t(20) && !t(23)(function () {
      return 7 != Object.defineProperty(t(21)("div"), "a", {
        get: function () {
          return 7;
        }
      }).a;
    });
  }, {
    20: 20,
    21: 21,
    23: 23
  }],
  28: [function (t, n, r) {
    n.exports = function (t) {
      return "object" == typeof t ? null !== t : "function" == typeof t;
    };
  }, {}],
  29: [function (t, n, r) {
    var e = t(17),
        i = t(27),
        o = t(31),
        u = Object.defineProperty;
    r.f = t(20) ? Object.defineProperty : function defineProperty(t, n, r) {
      if (e(t), n = o(n, !0), e(r), i) try {
        return u(t, n, r);
      } catch (t) {}
      if ("get" in r || "set" in r) throw TypeError("Accessors not supported!");
      return "value" in r && (t[n] = r.value), t;
    };
  }, {
    17: 17,
    20: 20,
    27: 27,
    31: 31
  }],
  30: [function (t, n, r) {
    n.exports = function (t, n) {
      return {
        enumerable: !(1 & t),
        configurable: !(2 & t),
        writable: !(4 & t),
        value: n
      };
    };
  }, {}],
  31: [function (t, n, r) {
    var i = t(28);

    n.exports = function (t, n) {
      if (!i(t)) return t;
      var r, e;
      if (n && "function" == typeof (r = t.toString) && !i(e = r.call(t))) return e;
      if ("function" == typeof (r = t.valueOf) && !i(e = r.call(t))) return e;
      if (!n && "function" == typeof (r = t.toString) && !i(e = r.call(t))) return e;
      throw TypeError("Can't convert object to primitive value");
    };
  }, {
    28: 28
  }],
  32: [function (t, n, r) {
    var e = t(22);
    e(e.G, {
      global: t(24)
    });
  }, {
    22: 22,
    24: 24
  }],
  33: [function (t, n, r) {
    arguments[4][16][0].apply(r, arguments);
  }, {
    16: 16
  }],
  34: [function (t, n, r) {
    var e = t(48);

    n.exports = function (t, n) {
      if ("number" != typeof t && "Number" != e(t)) throw TypeError(n);
      return +t;
    };
  }, {
    48: 48
  }],
  35: [function (t, n, r) {
    var e = t(152)("unscopables"),
        i = Array.prototype;
    null == i[e] && t(72)(i, e, {}), n.exports = function (t) {
      i[e][t] = !0;
    };
  }, {
    152: 152,
    72: 72
  }],
  36: [function (t, n, r) {
    "use strict";

    var e = t(129)(!0);

    n.exports = function (t, n, r) {
      return n + (r ? e(t, n).length : 1);
    };
  }, {
    129: 129
  }],
  37: [function (t, n, r) {
    n.exports = function (t, n, r, e) {
      if (!(t instanceof n) || void 0 !== e && e in t) throw TypeError(r + ": incorrect invocation!");
      return t;
    };
  }, {}],
  38: [function (t, n, r) {
    arguments[4][17][0].apply(r, arguments);
  }, {
    17: 17,
    81: 81
  }],
  39: [function (t, n, r) {
    "use strict";

    var f = t(142),
        s = t(137),
        l = t(141);

    n.exports = [].copyWithin || function copyWithin(t, n) {
      var r = f(this),
          e = l(r.length),
          i = s(t, e),
          o = s(n, e),
          u = 2 < arguments.length ? arguments[2] : void 0,
          c = Math.min((void 0 === u ? e : s(u, e)) - o, e - i),
          a = 1;

      for (o < i && i < o + c && (a = -1, o += c - 1, i += c - 1); 0 < c--;) o in r ? r[i] = r[o] : delete r[i], i += a, o += a;

      return r;
    };
  }, {
    137: 137,
    141: 141,
    142: 142
  }],
  40: [function (t, n, r) {
    "use strict";

    var c = t(142),
        a = t(137),
        f = t(141);

    n.exports = function fill(t) {
      for (var n = c(this), r = f(n.length), e = arguments.length, i = a(1 < e ? arguments[1] : void 0, r), o = 2 < e ? arguments[2] : void 0, u = void 0 === o ? r : a(o, r); i < u;) n[i++] = t;

      return n;
    };
  }, {
    137: 137,
    141: 141,
    142: 142
  }],
  41: [function (t, n, r) {
    var a = t(140),
        f = t(141),
        s = t(137);

    n.exports = function (c) {
      return function (t, n, r) {
        var e,
            i = a(t),
            o = f(i.length),
            u = s(r, o);

        if (c && n != n) {
          for (; u < o;) if ((e = i[u++]) != e) return !0;
        } else for (; u < o; u++) if ((c || u in i) && i[u] === n) return c || u || 0;

        return !c && -1;
      };
    };
  }, {
    137: 137,
    140: 140,
    141: 141
  }],
  42: [function (t, n, r) {
    var m = t(54),
        S = t(77),
        b = t(142),
        w = t(141),
        e = t(45);

    n.exports = function (l, t) {
      var h = 1 == l,
          p = 2 == l,
          v = 3 == l,
          g = 4 == l,
          y = 6 == l,
          d = 5 == l || y,
          x = t || e;
      return function (t, n, r) {
        for (var e, i, o = b(t), u = S(o), c = m(n, r, 3), a = w(u.length), f = 0, s = h ? x(t, a) : p ? x(t, 0) : void 0; f < a; f++) if ((d || f in u) && (i = c(e = u[f], f, o), l)) if (h) s[f] = i;else if (i) switch (l) {
          case 3:
            return !0;

          case 5:
            return e;

          case 6:
            return f;

          case 2:
            s.push(e);
        } else if (g) return !1;

        return y ? -1 : v || g ? g : s;
      };
    };
  }, {
    141: 141,
    142: 142,
    45: 45,
    54: 54,
    77: 77
  }],
  43: [function (t, n, r) {
    var s = t(33),
        l = t(142),
        h = t(77),
        p = t(141);

    n.exports = function (t, n, r, e, i) {
      s(n);
      var o = l(t),
          u = h(o),
          c = p(o.length),
          a = i ? c - 1 : 0,
          f = i ? -1 : 1;
      if (r < 2) for (;;) {
        if (a in u) {
          e = u[a], a += f;
          break;
        }

        if (a += f, i ? a < 0 : c <= a) throw TypeError("Reduce of empty array with no initial value");
      }

      for (; i ? 0 <= a : a < c; a += f) a in u && (e = n(e, u[a], a, o));

      return e;
    };
  }, {
    141: 141,
    142: 142,
    33: 33,
    77: 77
  }],
  44: [function (t, n, r) {
    var e = t(81),
        i = t(79),
        o = t(152)("species");

    n.exports = function (t) {
      var n;
      return i(t) && ("function" != typeof (n = t.constructor) || n !== Array && !i(n.prototype) || (n = void 0), e(n) && null === (n = n[o]) && (n = void 0)), void 0 === n ? Array : n;
    };
  }, {
    152: 152,
    79: 79,
    81: 81
  }],
  45: [function (t, n, r) {
    var e = t(44);

    n.exports = function (t, n) {
      return new (e(t))(n);
    };
  }, {
    44: 44
  }],
  46: [function (t, n, r) {
    "use strict";

    var o = t(33),
        u = t(81),
        c = t(76),
        a = [].slice,
        f = {};

    n.exports = Function.bind || function bind(n) {
      var r = o(this),
          e = a.call(arguments, 1),
          i = function () {
        var t = e.concat(a.call(arguments));
        return this instanceof i ? function (t, n, r) {
          if (!(n in f)) {
            for (var e = [], i = 0; i < n; i++) e[i] = "a[" + i + "]";

            f[n] = Function("F,a", "return new F(" + e.join(",") + ")");
          }

          return f[n](t, r);
        }(r, t.length, t) : c(r, t, n);
      };

      return u(r.prototype) && (i.prototype = r.prototype), i;
    };
  }, {
    33: 33,
    76: 76,
    81: 81
  }],
  47: [function (t, n, r) {
    var i = t(48),
        o = t(152)("toStringTag"),
        u = "Arguments" == i(function () {
      return arguments;
    }());

    n.exports = function (t) {
      var n, r, e;
      return void 0 === t ? "Undefined" : null === t ? "Null" : "string" == typeof (r = function (t, n) {
        try {
          return t[n];
        } catch (t) {}
      }(n = Object(t), o)) ? r : u ? i(n) : "Object" == (e = i(n)) && "function" == typeof n.callee ? "Arguments" : e;
    };
  }, {
    152: 152,
    48: 48
  }],
  48: [function (t, n, r) {
    var e = {}.toString;

    n.exports = function (t) {
      return e.call(t).slice(8, -1);
    };
  }, {}],
  49: [function (t, n, r) {
    "use strict";

    function ag(t, n) {
      var r,
          e = p(n);
      if ("F" !== e) return t._i[e];

      for (r = t._f; r; r = r.n) if (r.k == n) return r;
    }

    var u = t(99).f,
        c = t(98),
        a = t(117),
        f = t(54),
        s = t(37),
        l = t(68),
        e = t(85),
        i = t(87),
        o = t(123),
        h = t(58),
        p = t(94).fastKey,
        v = t(149),
        g = h ? "_s" : "size";
    n.exports = {
      getConstructor: function (t, o, r, e) {
        var i = t(function (t, n) {
          s(t, i, o, "_i"), t._t = o, t._i = c(null), t._f = void 0, t._l = void 0, t[g] = 0, null != n && l(n, r, t[e], t);
        });
        return a(i.prototype, {
          clear: function clear() {
            for (var t = v(this, o), n = t._i, r = t._f; r; r = r.n) r.r = !0, r.p && (r.p = r.p.n = void 0), delete n[r.i];

            t._f = t._l = void 0, t[g] = 0;
          },
          delete: function (t) {
            var n = v(this, o),
                r = ag(n, t);

            if (r) {
              var e = r.n,
                  i = r.p;
              delete n._i[r.i], r.r = !0, i && (i.n = e), e && (e.p = i), n._f == r && (n._f = e), n._l == r && (n._l = i), n[g]--;
            }

            return !!r;
          },
          forEach: function forEach(t) {
            v(this, o);

            for (var n, r = f(t, 1 < arguments.length ? arguments[1] : void 0, 3); n = n ? n.n : this._f;) for (r(n.v, n.k, this); n && n.r;) n = n.p;
          },
          has: function has(t) {
            return !!ag(v(this, o), t);
          }
        }), h && u(i.prototype, "size", {
          get: function () {
            return v(this, o)[g];
          }
        }), i;
      },
      def: function (t, n, r) {
        var e,
            i,
            o = ag(t, n);
        return o ? o.v = r : (t._l = o = {
          i: i = p(n, !0),
          k: n,
          v: r,
          p: e = t._l,
          n: void 0,
          r: !1
        }, t._f || (t._f = o), e && (e.n = o), t[g]++, "F" !== i && (t._i[i] = o)), t;
      },
      getEntry: ag,
      setStrong: function (t, r, n) {
        e(t, r, function (t, n) {
          this._t = v(t, r), this._k = n, this._l = void 0;
        }, function () {
          for (var t = this, n = t._k, r = t._l; r && r.r;) r = r.p;

          return t._t && (t._l = r = r ? r.n : t._t._f) ? i(0, "keys" == n ? r.k : "values" == n ? r.v : [r.k, r.v]) : (t._t = void 0, i(1));
        }, n ? "entries" : "values", !n, !0), o(r);
      }
    };
  }, {
    117: 117,
    123: 123,
    149: 149,
    37: 37,
    54: 54,
    58: 58,
    68: 68,
    85: 85,
    87: 87,
    94: 94,
    98: 98,
    99: 99
  }],
  50: [function (t, n, r) {
    "use strict";

    function _g(t) {
      return t._l || (t._l = new g());
    }

    function bh(t, n) {
      return o(t.a, function (t) {
        return t[0] === n;
      });
    }

    var u = t(117),
        c = t(94).getWeak,
        i = t(38),
        a = t(81),
        f = t(37),
        s = t(68),
        e = t(42),
        l = t(71),
        h = t(149),
        o = e(5),
        p = e(6),
        v = 0,
        g = function () {
      this.a = [];
    };

    g.prototype = {
      get: function (t) {
        var n = bh(this, t);
        if (n) return n[1];
      },
      has: function (t) {
        return !!bh(this, t);
      },
      set: function (t, n) {
        var r = bh(this, t);
        r ? r[1] = n : this.a.push([t, n]);
      },
      delete: function (n) {
        var t = p(this.a, function (t) {
          return t[0] === n;
        });
        return ~t && this.a.splice(t, 1), !!~t;
      }
    }, n.exports = {
      getConstructor: function (t, r, e, i) {
        var o = t(function (t, n) {
          f(t, o, r, "_i"), t._t = r, t._i = v++, t._l = void 0, null != n && s(n, e, t[i], t);
        });
        return u(o.prototype, {
          delete: function (t) {
            if (!a(t)) return !1;
            var n = c(t);
            return !0 === n ? _g(h(this, r)).delete(t) : n && l(n, this._i) && delete n[this._i];
          },
          has: function has(t) {
            if (!a(t)) return !1;
            var n = c(t);
            return !0 === n ? _g(h(this, r)).has(t) : n && l(n, this._i);
          }
        }), o;
      },
      def: function (t, n, r) {
        var e = c(i(n), !0);
        return !0 === e ? _g(t).set(n, r) : e[t._i] = r, t;
      },
      ufstore: _g
    };
  }, {
    117: 117,
    149: 149,
    37: 37,
    38: 38,
    42: 42,
    68: 68,
    71: 71,
    81: 81,
    94: 94
  }],
  51: [function (t, n, r) {
    "use strict";

    var y = t(70),
        d = t(62),
        x = t(118),
        m = t(117),
        S = t(94),
        b = t(68),
        w = t(37),
        _ = t(81),
        E = t(64),
        O = t(86),
        F = t(124),
        I = t(75);

    n.exports = function (e, t, n, r, i, o) {
      function ci(t) {
        var r = f[t];
        x(f, t, "delete" == t ? function (t) {
          return !(o && !_(t)) && r.call(this, 0 === t ? 0 : t);
        } : "has" == t ? function has(t) {
          return !(o && !_(t)) && r.call(this, 0 === t ? 0 : t);
        } : "get" == t ? function get(t) {
          return o && !_(t) ? void 0 : r.call(this, 0 === t ? 0 : t);
        } : "add" == t ? function add(t) {
          return r.call(this, 0 === t ? 0 : t), this;
        } : function set(t, n) {
          return r.call(this, 0 === t ? 0 : t, n), this;
        });
      }

      var u = y[e],
          c = u,
          a = i ? "set" : "add",
          f = c && c.prototype,
          s = {};

      if ("function" == typeof c && (o || f.forEach && !E(function () {
        new c().entries().next();
      }))) {
        var l = new c(),
            h = l[a](o ? {} : -0, 1) != l,
            p = E(function () {
          l.has(1);
        }),
            v = O(function (t) {
          new c(t);
        }),
            g = !o && E(function () {
          for (var t = new c(), n = 5; n--;) t[a](n, n);

          return !t.has(-0);
        });
        v || (((c = t(function (t, n) {
          w(t, c, e);
          var r = I(new u(), t, c);
          return null != n && b(n, i, r[a], r), r;
        })).prototype = f).constructor = c), (p || g) && (ci("delete"), ci("has"), i && ci("get")), (g || h) && ci(a), o && f.clear && delete f.clear;
      } else c = r.getConstructor(t, e, i, a), m(c.prototype, n), S.NEED = !0;

      return F(c, e), s[e] = c, d(d.G + d.W + d.F * (c != u), s), o || r.setStrong(c, e, i), c;
    };
  }, {
    117: 117,
    118: 118,
    124: 124,
    37: 37,
    62: 62,
    64: 64,
    68: 68,
    70: 70,
    75: 75,
    81: 81,
    86: 86,
    94: 94
  }],
  52: [function (t, n, r) {
    arguments[4][18][0].apply(r, arguments);
  }, {
    18: 18
  }],
  53: [function (t, n, r) {
    "use strict";

    var e = t(99),
        i = t(116);

    n.exports = function (t, n, r) {
      n in t ? e.f(t, n, i(0, r)) : t[n] = r;
    };
  }, {
    116: 116,
    99: 99
  }],
  54: [function (t, n, r) {
    arguments[4][19][0].apply(r, arguments);
  }, {
    19: 19,
    33: 33
  }],
  55: [function (t, n, r) {
    "use strict";

    function Qi(t) {
      return 9 < t ? t : "0" + t;
    }

    var e = t(64),
        i = Date.prototype.getTime,
        o = Date.prototype.toISOString;
    n.exports = e(function () {
      return "0385-07-25T07:06:39.999Z" != o.call(new Date(-5e13 - 1));
    }) || !e(function () {
      o.call(new Date(NaN));
    }) ? function toISOString() {
      if (!isFinite(i.call(this))) throw RangeError("Invalid time value");
      var t = this,
          n = t.getUTCFullYear(),
          r = t.getUTCMilliseconds(),
          e = n < 0 ? "-" : 9999 < n ? "+" : "";
      return e + ("00000" + Math.abs(n)).slice(e ? -6 : -4) + "-" + Qi(t.getUTCMonth() + 1) + "-" + Qi(t.getUTCDate()) + "T" + Qi(t.getUTCHours()) + ":" + Qi(t.getUTCMinutes()) + ":" + Qi(t.getUTCSeconds()) + "." + (99 < r ? r : "0" + Qi(r)) + "Z";
    } : o;
  }, {
    64: 64
  }],
  56: [function (t, n, r) {
    "use strict";

    var e = t(38),
        i = t(143);

    n.exports = function (t) {
      if ("string" !== t && "number" !== t && "default" !== t) throw TypeError("Incorrect hint");
      return i(e(this), "number" != t);
    };
  }, {
    143: 143,
    38: 38
  }],
  57: [function (t, n, r) {
    n.exports = function (t) {
      if (null == t) throw TypeError("Can't call method on  " + t);
      return t;
    };
  }, {}],
  58: [function (t, n, r) {
    arguments[4][20][0].apply(r, arguments);
  }, {
    20: 20,
    64: 64
  }],
  59: [function (t, n, r) {
    arguments[4][21][0].apply(r, arguments);
  }, {
    21: 21,
    70: 70,
    81: 81
  }],
  60: [function (t, n, r) {
    n.exports = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",");
  }, {}],
  61: [function (t, n, r) {
    var c = t(107),
        a = t(104),
        f = t(108);

    n.exports = function (t) {
      var n = c(t),
          r = a.f;
      if (r) for (var e, i = r(t), o = f.f, u = 0; i.length > u;) o.call(t, e = i[u++]) && n.push(e);
      return n;
    };
  }, {
    104: 104,
    107: 107,
    108: 108
  }],
  62: [function (t, n, r) {
    var g = t(70),
        y = t(52),
        d = t(72),
        x = t(118),
        m = t(54),
        S = "prototype",
        b = function (t, n, r) {
      var e,
          i,
          o,
          u,
          c = t & b.F,
          a = t & b.G,
          f = t & b.S,
          s = t & b.P,
          l = t & b.B,
          h = a ? g : f ? g[n] || (g[n] = {}) : (g[n] || {})[S],
          p = a ? y : y[n] || (y[n] = {}),
          v = p[S] || (p[S] = {});

      for (e in a && (r = n), r) o = ((i = !c && h && void 0 !== h[e]) ? h : r)[e], u = l && i ? m(o, g) : s && "function" == typeof o ? m(Function.call, o) : o, h && x(h, e, o, t & b.U), p[e] != o && d(p, e, u), s && v[e] != o && (v[e] = o);
    };

    g.core = y, b.F = 1, b.G = 2, b.S = 4, b.P = 8, b.B = 16, b.W = 32, b.U = 64, b.R = 128, n.exports = b;
  }, {
    118: 118,
    52: 52,
    54: 54,
    70: 70,
    72: 72
  }],
  63: [function (t, n, r) {
    var e = t(152)("match");

    n.exports = function (n) {
      var r = /./;

      try {
        "/./"[n](r);
      } catch (t) {
        try {
          return r[e] = !1, !"/./"[n](r);
        } catch (t) {}
      }

      return !0;
    };
  }, {
    152: 152
  }],
  64: [function (t, n, r) {
    arguments[4][23][0].apply(r, arguments);
  }, {
    23: 23
  }],
  65: [function (t, n, r) {
    "use strict";

    t(248);

    var s = t(118),
        l = t(72),
        h = t(64),
        p = t(57),
        v = t(152),
        g = t(120),
        y = v("species"),
        d = !h(function () {
      var t = /./;
      return t.exec = function () {
        var t = [];
        return t.groups = {
          a: "7"
        }, t;
      }, "7" !== "".replace(t, "$<a>");
    }),
        x = function () {
      var t = /(?:)/,
          n = t.exec;

      t.exec = function () {
        return n.apply(this, arguments);
      };

      var r = "ab".split(t);
      return 2 === r.length && "a" === r[0] && "b" === r[1];
    }();

    n.exports = function (r, t, n) {
      var e = v(r),
          o = !h(function () {
        var t = {};
        return t[e] = function () {
          return 7;
        }, 7 != ""[r](t);
      }),
          i = o ? !h(function () {
        var t = !1,
            n = /a/;
        return n.exec = function () {
          return t = !0, null;
        }, "split" === r && (n.constructor = {}, n.constructor[y] = function () {
          return n;
        }), n[e](""), !t;
      }) : void 0;

      if (!o || !i || "replace" === r && !d || "split" === r && !x) {
        var u = /./[e],
            c = n(p, e, ""[r], function maybeCallNative(t, n, r, e, i) {
          return n.exec === g ? o && !i ? {
            done: !0,
            value: u.call(n, r, e)
          } : {
            done: !0,
            value: t.call(r, n, e)
          } : {
            done: !1
          };
        }),
            a = c[0],
            f = c[1];
        s(String.prototype, r, a), l(RegExp.prototype, e, 2 == t ? function (t, n) {
          return f.call(t, this, n);
        } : function (t) {
          return f.call(t, this);
        });
      }
    };
  }, {
    118: 118,
    120: 120,
    152: 152,
    248: 248,
    57: 57,
    64: 64,
    72: 72
  }],
  66: [function (t, n, r) {
    "use strict";

    var e = t(38);

    n.exports = function () {
      var t = e(this),
          n = "";
      return t.global && (n += "g"), t.ignoreCase && (n += "i"), t.multiline && (n += "m"), t.unicode && (n += "u"), t.sticky && (n += "y"), n;
    };
  }, {
    38: 38
  }],
  67: [function (t, n, r) {
    "use strict";

    var p = t(79),
        v = t(81),
        g = t(141),
        y = t(54),
        d = t(152)("isConcatSpreadable");

    n.exports = function flattenIntoArray(t, n, r, e, i, o, u, c) {
      for (var a, f, s = i, l = 0, h = !!u && y(u, c, 3); l < e;) {
        if (l in r) {
          if (a = h ? h(r[l], l, n) : r[l], f = !1, v(a) && (f = void 0 !== (f = a[d]) ? !!f : p(a)), f && 0 < o) s = flattenIntoArray(t, n, a, g(a.length), s, o - 1) - 1;else {
            if (9007199254740991 <= s) throw TypeError();
            t[s] = a;
          }
          s++;
        }

        l++;
      }

      return s;
    };
  }, {
    141: 141,
    152: 152,
    54: 54,
    79: 79,
    81: 81
  }],
  68: [function (t, n, r) {
    var h = t(54),
        p = t(83),
        v = t(78),
        g = t(38),
        y = t(141),
        d = t(153),
        x = {},
        m = {};
    (r = n.exports = function (t, n, r, e, i) {
      var o,
          u,
          c,
          a,
          f = i ? function () {
        return t;
      } : d(t),
          s = h(r, e, n ? 2 : 1),
          l = 0;
      if ("function" != typeof f) throw TypeError(t + " is not iterable!");

      if (v(f)) {
        for (o = y(t.length); l < o; l++) if ((a = n ? s(g(u = t[l])[0], u[1]) : s(t[l])) === x || a === m) return a;
      } else for (c = f.call(t); !(u = c.next()).done;) if ((a = p(c, s, u.value, n)) === x || a === m) return a;
    }).BREAK = x, r.RETURN = m;
  }, {
    141: 141,
    153: 153,
    38: 38,
    54: 54,
    78: 78,
    83: 83
  }],
  69: [function (t, n, r) {
    n.exports = t(126)("native-function-to-string", Function.toString);
  }, {
    126: 126
  }],
  70: [function (t, n, r) {
    arguments[4][24][0].apply(r, arguments);
  }, {
    24: 24
  }],
  71: [function (t, n, r) {
    arguments[4][25][0].apply(r, arguments);
  }, {
    25: 25
  }],
  72: [function (t, n, r) {
    arguments[4][26][0].apply(r, arguments);
  }, {
    116: 116,
    26: 26,
    58: 58,
    99: 99
  }],
  73: [function (t, n, r) {
    var e = t(70).document;
    n.exports = e && e.documentElement;
  }, {
    70: 70
  }],
  74: [function (t, n, r) {
    arguments[4][27][0].apply(r, arguments);
  }, {
    27: 27,
    58: 58,
    59: 59,
    64: 64
  }],
  75: [function (t, n, r) {
    var o = t(81),
        u = t(122).set;

    n.exports = function (t, n, r) {
      var e,
          i = n.constructor;
      return i !== r && "function" == typeof i && (e = i.prototype) !== r.prototype && o(e) && u && u(t, e), t;
    };
  }, {
    122: 122,
    81: 81
  }],
  76: [function (t, n, r) {
    n.exports = function (t, n, r) {
      var e = void 0 === r;

      switch (n.length) {
        case 0:
          return e ? t() : t.call(r);

        case 1:
          return e ? t(n[0]) : t.call(r, n[0]);

        case 2:
          return e ? t(n[0], n[1]) : t.call(r, n[0], n[1]);

        case 3:
          return e ? t(n[0], n[1], n[2]) : t.call(r, n[0], n[1], n[2]);

        case 4:
          return e ? t(n[0], n[1], n[2], n[3]) : t.call(r, n[0], n[1], n[2], n[3]);
      }

      return t.apply(r, n);
    };
  }, {}],
  77: [function (t, n, r) {
    var e = t(48);
    n.exports = Object("z").propertyIsEnumerable(0) ? Object : function (t) {
      return "String" == e(t) ? t.split("") : Object(t);
    };
  }, {
    48: 48
  }],
  78: [function (t, n, r) {
    var e = t(88),
        i = t(152)("iterator"),
        o = Array.prototype;

    n.exports = function (t) {
      return void 0 !== t && (e.Array === t || o[i] === t);
    };
  }, {
    152: 152,
    88: 88
  }],
  79: [function (t, n, r) {
    var e = t(48);

    n.exports = Array.isArray || function isArray(t) {
      return "Array" == e(t);
    };
  }, {
    48: 48
  }],
  80: [function (t, n, r) {
    var e = t(81),
        i = Math.floor;

    n.exports = function isInteger(t) {
      return !e(t) && isFinite(t) && i(t) === t;
    };
  }, {
    81: 81
  }],
  81: [function (t, n, r) {
    arguments[4][28][0].apply(r, arguments);
  }, {
    28: 28
  }],
  82: [function (t, n, r) {
    var e = t(81),
        i = t(48),
        o = t(152)("match");

    n.exports = function (t) {
      var n;
      return e(t) && (void 0 !== (n = t[o]) ? !!n : "RegExp" == i(t));
    };
  }, {
    152: 152,
    48: 48,
    81: 81
  }],
  83: [function (t, n, r) {
    var o = t(38);

    n.exports = function (n, t, r, e) {
      try {
        return e ? t(o(r)[0], r[1]) : t(r);
      } catch (t) {
        var i = n.return;
        throw void 0 !== i && o(i.call(n)), t;
      }
    };
  }, {
    38: 38
  }],
  84: [function (t, n, r) {
    "use strict";

    var e = t(98),
        i = t(116),
        o = t(124),
        u = {};
    t(72)(u, t(152)("iterator"), function () {
      return this;
    }), n.exports = function (t, n, r) {
      t.prototype = e(u, {
        next: i(1, r)
      }), o(t, n + " Iterator");
    };
  }, {
    116: 116,
    124: 124,
    152: 152,
    72: 72,
    98: 98
  }],
  85: [function (t, n, r) {
    "use strict";

    function Qn() {
      return this;
    }

    var x = t(89),
        m = t(62),
        S = t(118),
        b = t(72),
        w = t(88),
        _ = t(84),
        E = t(124),
        O = t(105),
        F = t(152)("iterator"),
        I = !([].keys && "next" in [].keys()),
        P = "values";

    n.exports = function (t, n, r, e, i, o, u) {
      _(r, n, e);

      function Yn(t) {
        if (!I && t in p) return p[t];

        switch (t) {
          case "keys":
            return function keys() {
              return new r(this, t);
            };

          case P:
            return function values() {
              return new r(this, t);
            };
        }

        return function entries() {
          return new r(this, t);
        };
      }

      var c,
          a,
          f,
          s = n + " Iterator",
          l = i == P,
          h = !1,
          p = t.prototype,
          v = p[F] || p["@@iterator"] || i && p[i],
          g = v || Yn(i),
          y = i ? l ? Yn("entries") : g : void 0,
          d = "Array" == n && p.entries || v;
      if (d && (f = O(d.call(new t()))) !== Object.prototype && f.next && (E(f, s, !0), x || "function" == typeof f[F] || b(f, F, Qn)), l && v && v.name !== P && (h = !0, g = function values() {
        return v.call(this);
      }), x && !u || !I && !h && p[F] || b(p, F, g), w[n] = g, w[s] = Qn, i) if (c = {
        values: l ? g : Yn(P),
        keys: o ? g : Yn("keys"),
        entries: y
      }, u) for (a in c) a in p || S(p, a, c[a]);else m(m.P + m.F * (I || h), n, c);
      return c;
    };
  }, {
    105: 105,
    118: 118,
    124: 124,
    152: 152,
    62: 62,
    72: 72,
    84: 84,
    88: 88,
    89: 89
  }],
  86: [function (t, n, r) {
    var o = t(152)("iterator"),
        u = !1;

    try {
      var e = [7][o]();
      e.return = function () {
        u = !0;
      }, Array.from(e, function () {
        throw 2;
      });
    } catch (t) {}

    n.exports = function (t, n) {
      if (!n && !u) return !1;
      var r = !1;

      try {
        var e = [7],
            i = e[o]();
        i.next = function () {
          return {
            done: r = !0
          };
        }, e[o] = function () {
          return i;
        }, t(e);
      } catch (t) {}

      return r;
    };
  }, {
    152: 152
  }],
  87: [function (t, n, r) {
    n.exports = function (t, n) {
      return {
        value: n,
        done: !!t
      };
    };
  }, {}],
  88: [function (t, n, r) {
    n.exports = {};
  }, {}],
  89: [function (t, n, r) {
    n.exports = !1;
  }, {}],
  90: [function (t, n, r) {
    var e = Math.expm1;
    n.exports = !e || 22025.465794806718 < e(10) || e(10) < 22025.465794806718 || -2e-17 != e(-2e-17) ? function expm1(t) {
      return 0 == (t = +t) ? t : -1e-6 < t && t < 1e-6 ? t + t * t / 2 : Math.exp(t) - 1;
    } : e;
  }, {}],
  91: [function (t, n, r) {
    var o = t(93),
        e = Math.pow,
        u = e(2, -52),
        c = e(2, -23),
        a = e(2, 127) * (2 - c),
        f = e(2, -126);

    n.exports = Math.fround || function fround(t) {
      var n,
          r,
          e = Math.abs(t),
          i = o(t);
      return e < f ? i * (e / f / c + 1 / u - 1 / u) * f * c : a < (r = (n = (1 + c / u) * e) - (n - e)) || r != r ? i * (1 / 0) : i * r;
    };
  }, {
    93: 93
  }],
  92: [function (t, n, r) {
    n.exports = Math.log1p || function log1p(t) {
      return -1e-8 < (t = +t) && t < 1e-8 ? t - t * t / 2 : Math.log(1 + t);
    };
  }, {}],
  93: [function (t, n, r) {
    n.exports = Math.sign || function sign(t) {
      return 0 == (t = +t) || t != t ? t : t < 0 ? -1 : 1;
    };
  }, {}],
  94: [function (t, n, r) {
    function tp(t) {
      u(t, e, {
        value: {
          i: "O" + ++c,
          w: {}
        }
      });
    }

    var e = t(147)("meta"),
        i = t(81),
        o = t(71),
        u = t(99).f,
        c = 0,
        a = Object.isExtensible || function () {
      return !0;
    },
        f = !t(64)(function () {
      return a(Object.preventExtensions({}));
    }),
        s = n.exports = {
      KEY: e,
      NEED: !1,
      fastKey: function (t, n) {
        if (!i(t)) return "symbol" == typeof t ? t : ("string" == typeof t ? "S" : "P") + t;

        if (!o(t, e)) {
          if (!a(t)) return "F";
          if (!n) return "E";
          tp(t);
        }

        return t[e].i;
      },
      getWeak: function (t, n) {
        if (!o(t, e)) {
          if (!a(t)) return !0;
          if (!n) return !1;
          tp(t);
        }

        return t[e].w;
      },
      onFreeze: function (t) {
        return f && s.NEED && a(t) && !o(t, e) && tp(t), t;
      }
    };
  }, {
    147: 147,
    64: 64,
    71: 71,
    81: 81,
    99: 99
  }],
  95: [function (t, n, r) {
    var u = t(70),
        c = t(136).set,
        a = u.MutationObserver || u.WebKitMutationObserver,
        f = u.process,
        s = u.Promise,
        l = "process" == t(48)(f);

    n.exports = function () {
      function Qp() {
        var t, n;

        for (l && (t = f.domain) && t.exit(); r;) {
          n = r.fn, r = r.next;

          try {
            n();
          } catch (t) {
            throw r ? i() : e = void 0, t;
          }
        }

        e = void 0, t && t.enter();
      }

      var r, e, i;
      if (l) i = function () {
        f.nextTick(Qp);
      };else if (!a || u.navigator && u.navigator.standalone) {
        if (s && s.resolve) {
          var t = s.resolve(void 0);

          i = function () {
            t.then(Qp);
          };
        } else i = function () {
          c.call(u, Qp);
        };
      } else {
        var n = !0,
            o = document.createTextNode("");
        new a(Qp).observe(o, {
          characterData: !0
        }), i = function () {
          o.data = n = !n;
        };
      }
      return function (t) {
        var n = {
          fn: t,
          next: void 0
        };
        e && (e.next = n), r || (r = n, i()), e = n;
      };
    };
  }, {
    136: 136,
    48: 48,
    70: 70
  }],
  96: [function (t, n, r) {
    "use strict";

    var i = t(33);

    function PromiseCapability(t) {
      var r, e;
      this.promise = new t(function (t, n) {
        if (void 0 !== r || void 0 !== e) throw TypeError("Bad Promise constructor");
        r = t, e = n;
      }), this.resolve = i(r), this.reject = i(e);
    }

    n.exports.f = function (t) {
      return new PromiseCapability(t);
    };
  }, {
    33: 33
  }],
  97: [function (t, n, r) {
    "use strict";

    var h = t(58),
        p = t(107),
        v = t(104),
        g = t(108),
        y = t(142),
        d = t(77),
        i = Object.assign;
    n.exports = !i || t(64)(function () {
      var t = {},
          n = {},
          r = Symbol(),
          e = "abcdefghijklmnopqrst";
      return t[r] = 7, e.split("").forEach(function (t) {
        n[t] = t;
      }), 7 != i({}, t)[r] || Object.keys(i({}, n)).join("") != e;
    }) ? function assign(t, n) {
      for (var r = y(t), e = arguments.length, i = 1, o = v.f, u = g.f; i < e;) for (var c, a = d(arguments[i++]), f = o ? p(a).concat(o(a)) : p(a), s = f.length, l = 0; l < s;) c = f[l++], h && !u.call(a, c) || (r[c] = a[c]);

      return r;
    } : i;
  }, {
    104: 104,
    107: 107,
    108: 108,
    142: 142,
    58: 58,
    64: 64,
    77: 77
  }],
  98: [function (e, t, n) {
    function Pq() {}

    var i = e(38),
        o = e(100),
        u = e(60),
        c = e(125)("IE_PROTO"),
        a = "prototype",
        f = function () {
      var t,
          n = e(59)("iframe"),
          r = u.length;

      for (n.style.display = "none", e(73).appendChild(n), n.src = "javascript:", (t = n.contentWindow.document).open(), t.write("<script>document.F=Object<\/script>"), t.close(), f = t.F; r--;) delete f[a][u[r]];

      return f();
    };

    t.exports = Object.create || function create(t, n) {
      var r;
      return null !== t ? (Pq[a] = i(t), r = new Pq(), Pq[a] = null, r[c] = t) : r = f(), void 0 === n ? r : o(r, n);
    };
  }, {
    100: 100,
    125: 125,
    38: 38,
    59: 59,
    60: 60,
    73: 73
  }],
  99: [function (t, n, r) {
    arguments[4][29][0].apply(r, arguments);
  }, {
    143: 143,
    29: 29,
    38: 38,
    58: 58,
    74: 74
  }],
  100: [function (t, n, r) {
    var u = t(99),
        c = t(38),
        a = t(107);
    n.exports = t(58) ? Object.defineProperties : function defineProperties(t, n) {
      c(t);

      for (var r, e = a(n), i = e.length, o = 0; o < i;) u.f(t, r = e[o++], n[r]);

      return t;
    };
  }, {
    107: 107,
    38: 38,
    58: 58,
    99: 99
  }],
  101: [function (t, n, r) {
    var e = t(108),
        i = t(116),
        o = t(140),
        u = t(143),
        c = t(71),
        a = t(74),
        f = Object.getOwnPropertyDescriptor;
    r.f = t(58) ? f : function getOwnPropertyDescriptor(t, n) {
      if (t = o(t), n = u(n, !0), a) try {
        return f(t, n);
      } catch (t) {}
      if (c(t, n)) return i(!e.f.call(t, n), t[n]);
    };
  }, {
    108: 108,
    116: 116,
    140: 140,
    143: 143,
    58: 58,
    71: 71,
    74: 74
  }],
  102: [function (t, n, r) {
    var e = t(140),
        i = t(103).f,
        o = {}.toString,
        u = "object" == typeof window && window && Object.getOwnPropertyNames ? Object.getOwnPropertyNames(window) : [];

    n.exports.f = function getOwnPropertyNames(t) {
      return u && "[object Window]" == o.call(t) ? function (t) {
        try {
          return i(t);
        } catch (t) {
          return u.slice();
        }
      }(t) : i(e(t));
    };
  }, {
    103: 103,
    140: 140
  }],
  103: [function (t, n, r) {
    var e = t(106),
        i = t(60).concat("length", "prototype");

    r.f = Object.getOwnPropertyNames || function getOwnPropertyNames(t) {
      return e(t, i);
    };
  }, {
    106: 106,
    60: 60
  }],
  104: [function (t, n, r) {
    r.f = Object.getOwnPropertySymbols;
  }, {}],
  105: [function (t, n, r) {
    var e = t(71),
        i = t(142),
        o = t(125)("IE_PROTO"),
        u = Object.prototype;

    n.exports = Object.getPrototypeOf || function (t) {
      return t = i(t), e(t, o) ? t[o] : "function" == typeof t.constructor && t instanceof t.constructor ? t.constructor.prototype : t instanceof Object ? u : null;
    };
  }, {
    125: 125,
    142: 142,
    71: 71
  }],
  106: [function (t, n, r) {
    var u = t(71),
        c = t(140),
        a = t(41)(!1),
        f = t(125)("IE_PROTO");

    n.exports = function (t, n) {
      var r,
          e = c(t),
          i = 0,
          o = [];

      for (r in e) r != f && u(e, r) && o.push(r);

      for (; n.length > i;) u(e, r = n[i++]) && (~a(o, r) || o.push(r));

      return o;
    };
  }, {
    125: 125,
    140: 140,
    41: 41,
    71: 71
  }],
  107: [function (t, n, r) {
    var e = t(106),
        i = t(60);

    n.exports = Object.keys || function keys(t) {
      return e(t, i);
    };
  }, {
    106: 106,
    60: 60
  }],
  108: [function (t, n, r) {
    r.f = {}.propertyIsEnumerable;
  }, {}],
  109: [function (t, n, r) {
    var i = t(62),
        o = t(52),
        u = t(64);

    n.exports = function (t, n) {
      var r = (o.Object || {})[t] || Object[t],
          e = {};
      e[t] = n(r), i(i.S + i.F * u(function () {
        r(1);
      }), "Object", e);
    };
  }, {
    52: 52,
    62: 62,
    64: 64
  }],
  110: [function (t, n, r) {
    var a = t(58),
        f = t(107),
        s = t(140),
        l = t(108).f;

    n.exports = function (c) {
      return function (t) {
        for (var n, r = s(t), e = f(r), i = e.length, o = 0, u = []; o < i;) n = e[o++], a && !l.call(r, n) || u.push(c ? [n, r[n]] : r[n]);

        return u;
      };
    };
  }, {
    107: 107,
    108: 108,
    140: 140,
    58: 58
  }],
  111: [function (t, n, r) {
    var e = t(103),
        i = t(104),
        o = t(38),
        u = t(70).Reflect;

    n.exports = u && u.ownKeys || function ownKeys(t) {
      var n = e.f(o(t)),
          r = i.f;
      return r ? n.concat(r(t)) : n;
    };
  }, {
    103: 103,
    104: 104,
    38: 38,
    70: 70
  }],
  112: [function (t, n, r) {
    var e = t(70).parseFloat,
        i = t(134).trim;
    n.exports = 1 / e(t(135) + "-0") != -1 / 0 ? function parseFloat(t) {
      var n = i(String(t), 3),
          r = e(n);
      return 0 === r && "-" == n.charAt(0) ? -0 : r;
    } : e;
  }, {
    134: 134,
    135: 135,
    70: 70
  }],
  113: [function (t, n, r) {
    var e = t(70).parseInt,
        i = t(134).trim,
        o = t(135),
        u = /^[-+]?0[xX]/;
    n.exports = 8 !== e(o + "08") || 22 !== e(o + "0x16") ? function parseInt(t, n) {
      var r = i(String(t), 3);
      return e(r, n >>> 0 || (u.test(r) ? 16 : 10));
    } : e;
  }, {
    134: 134,
    135: 135,
    70: 70
  }],
  114: [function (t, n, r) {
    n.exports = function (t) {
      try {
        return {
          e: !1,
          v: t()
        };
      } catch (t) {
        return {
          e: !0,
          v: t
        };
      }
    };
  }, {}],
  115: [function (t, n, r) {
    var e = t(38),
        i = t(81),
        o = t(96);

    n.exports = function (t, n) {
      if (e(t), i(n) && n.constructor === t) return n;
      var r = o.f(t);
      return (0, r.resolve)(n), r.promise;
    };
  }, {
    38: 38,
    81: 81,
    96: 96
  }],
  116: [function (t, n, r) {
    arguments[4][30][0].apply(r, arguments);
  }, {
    30: 30
  }],
  117: [function (t, n, r) {
    var i = t(118);

    n.exports = function (t, n, r) {
      for (var e in n) i(t, e, n[e], r);

      return t;
    };
  }, {
    118: 118
  }],
  118: [function (t, n, r) {
    var o = t(70),
        u = t(72),
        c = t(71),
        a = t(147)("src"),
        e = t(69),
        i = "toString",
        f = ("" + e).split(i);
    t(52).inspectSource = function (t) {
      return e.call(t);
    }, (n.exports = function (t, n, r, e) {
      var i = "function" == typeof r;
      i && (c(r, "name") || u(r, "name", n)), t[n] !== r && (i && (c(r, a) || u(r, a, t[n] ? "" + t[n] : f.join(String(n)))), t === o ? t[n] = r : e ? t[n] ? t[n] = r : u(t, n, r) : (delete t[n], u(t, n, r)));
    })(Function.prototype, i, function toString() {
      return "function" == typeof this && this[a] || e.call(this);
    });
  }, {
    147: 147,
    52: 52,
    69: 69,
    70: 70,
    71: 71,
    72: 72
  }],
  119: [function (t, n, r) {
    "use strict";

    var i = t(47),
        o = RegExp.prototype.exec;

    n.exports = function (t, n) {
      var r = t.exec;

      if ("function" == typeof r) {
        var e = r.call(t, n);
        if ("object" != typeof e) throw new TypeError("RegExp exec method returned something other than an Object or null");
        return e;
      }

      if ("RegExp" !== i(t)) throw new TypeError("RegExp#exec called on incompatible receiver");
      return o.call(t, n);
    };
  }, {
    47: 47
  }],
  120: [function (t, n, r) {
    "use strict";

    var e,
        i,
        u = t(66),
        c = RegExp.prototype.exec,
        a = String.prototype.replace,
        o = c,
        f = "lastIndex",
        s = (e = /a/, i = /b*/g, c.call(e, "a"), c.call(i, "a"), 0 !== e[f] || 0 !== i[f]),
        l = void 0 !== /()??/.exec("")[1];
    (s || l) && (o = function exec(t) {
      var n,
          r,
          e,
          i,
          o = this;
      return l && (r = new RegExp("^" + o.source + "$(?!\\s)", u.call(o))), s && (n = o[f]), e = c.call(o, t), s && e && (o[f] = o.global ? e.index + e[0].length : n), l && e && 1 < e.length && a.call(e[0], r, function () {
        for (i = 1; i < arguments.length - 2; i++) void 0 === arguments[i] && (e[i] = void 0);
      }), e;
    }), n.exports = o;
  }, {
    66: 66
  }],
  121: [function (t, n, r) {
    n.exports = Object.is || function is(t, n) {
      return t === n ? 0 !== t || 1 / t == 1 / n : t != t && n != n;
    };
  }, {}],
  122: [function (n, t, r) {
    function Wu(t, n) {
      if (i(t), !e(n) && null !== n) throw TypeError(n + ": can't set as prototype!");
    }

    var e = n(81),
        i = n(38);
    t.exports = {
      set: Object.setPrototypeOf || ("__proto__" in {} ? function (t, r, e) {
        try {
          (e = n(54)(Function.call, n(101).f(Object.prototype, "__proto__").set, 2))(t, []), r = !(t instanceof Array);
        } catch (t) {
          r = !0;
        }

        return function setPrototypeOf(t, n) {
          return Wu(t, n), r ? t.__proto__ = n : e(t, n), t;
        };
      }({}, !1) : void 0),
      check: Wu
    };
  }, {
    101: 101,
    38: 38,
    54: 54,
    81: 81
  }],
  123: [function (t, n, r) {
    "use strict";

    var e = t(70),
        i = t(99),
        o = t(58),
        u = t(152)("species");

    n.exports = function (t) {
      var n = e[t];
      o && n && !n[u] && i.f(n, u, {
        configurable: !0,
        get: function () {
          return this;
        }
      });
    };
  }, {
    152: 152,
    58: 58,
    70: 70,
    99: 99
  }],
  124: [function (t, n, r) {
    var e = t(99).f,
        i = t(71),
        o = t(152)("toStringTag");

    n.exports = function (t, n, r) {
      t && !i(t = r ? t : t.prototype, o) && e(t, o, {
        configurable: !0,
        value: n
      });
    };
  }, {
    152: 152,
    71: 71,
    99: 99
  }],
  125: [function (t, n, r) {
    var e = t(126)("keys"),
        i = t(147);

    n.exports = function (t) {
      return e[t] || (e[t] = i(t));
    };
  }, {
    126: 126,
    147: 147
  }],
  126: [function (t, n, r) {
    var e = t(52),
        i = t(70),
        o = "__core-js_shared__",
        u = i[o] || (i[o] = {});
    (n.exports = function (t, n) {
      return u[t] || (u[t] = void 0 !== n ? n : {});
    })("versions", []).push({
      version: e.version,
      mode: t(89) ? "pure" : "global",
      copyright: "© 2019 Denis Pushkarev (zloirock.ru)"
    });
  }, {
    52: 52,
    70: 70,
    89: 89
  }],
  127: [function (t, n, r) {
    var i = t(38),
        o = t(33),
        u = t(152)("species");

    n.exports = function (t, n) {
      var r,
          e = i(t).constructor;
      return void 0 === e || null == (r = i(e)[u]) ? n : o(r);
    };
  }, {
    152: 152,
    33: 33,
    38: 38
  }],
  128: [function (t, n, r) {
    "use strict";

    var e = t(64);

    n.exports = function (t, n) {
      return !!t && e(function () {
        n ? t.call(null, function () {}, 1) : t.call(null);
      });
    };
  }, {
    64: 64
  }],
  129: [function (t, n, r) {
    var a = t(139),
        f = t(57);

    n.exports = function (c) {
      return function (t, n) {
        var r,
            e,
            i = String(f(t)),
            o = a(n),
            u = i.length;
        return o < 0 || u <= o ? c ? "" : void 0 : (r = i.charCodeAt(o)) < 55296 || 56319 < r || o + 1 === u || (e = i.charCodeAt(o + 1)) < 56320 || 57343 < e ? c ? i.charAt(o) : r : c ? i.slice(o, o + 2) : e - 56320 + (r - 55296 << 10) + 65536;
      };
    };
  }, {
    139: 139,
    57: 57
  }],
  130: [function (t, n, r) {
    var e = t(82),
        i = t(57);

    n.exports = function (t, n, r) {
      if (e(n)) throw TypeError("String#" + r + " doesn't accept regex!");
      return String(i(t));
    };
  }, {
    57: 57,
    82: 82
  }],
  131: [function (t, n, r) {
    function Aw(t, n, r, e) {
      var i = String(u(t)),
          o = "<" + n;
      return "" !== r && (o += " " + r + '="' + String(e).replace(c, "&quot;") + '"'), o + ">" + i + "</" + n + ">";
    }

    var e = t(62),
        i = t(64),
        u = t(57),
        c = /"/g;

    n.exports = function (n, t) {
      var r = {};
      r[n] = t(Aw), e(e.P + e.F * i(function () {
        var t = ""[n]('"');
        return t !== t.toLowerCase() || 3 < t.split('"').length;
      }), "String", r);
    };
  }, {
    57: 57,
    62: 62,
    64: 64
  }],
  132: [function (t, n, r) {
    var s = t(141),
        l = t(133),
        h = t(57);

    n.exports = function (t, n, r, e) {
      var i = String(h(t)),
          o = i.length,
          u = void 0 === r ? " " : String(r),
          c = s(n);
      if (c <= o || "" == u) return i;
      var a = c - o,
          f = l.call(u, Math.ceil(a / u.length));
      return f.length > a && (f = f.slice(0, a)), e ? f + i : i + f;
    };
  }, {
    133: 133,
    141: 141,
    57: 57
  }],
  133: [function (t, n, r) {
    "use strict";

    var i = t(139),
        o = t(57);

    n.exports = function repeat(t) {
      var n = String(o(this)),
          r = "",
          e = i(t);
      if (e < 0 || e == 1 / 0) throw RangeError("Count can't be negative");

      for (; 0 < e; (e >>>= 1) && (n += n)) 1 & e && (r += n);

      return r;
    };
  }, {
    139: 139,
    57: 57
  }],
  134: [function (t, n, r) {
    function tx(t, n, r) {
      var e = {},
          i = c(function () {
        return !!a[t]() || "​" != "​"[t]();
      }),
          o = e[t] = i ? n(s) : a[t];
      r && (e[r] = o), u(u.P + u.F * i, "String", e);
    }

    var u = t(62),
        e = t(57),
        c = t(64),
        a = t(135),
        i = "[" + a + "]",
        o = RegExp("^" + i + i + "*"),
        f = RegExp(i + i + "*$"),
        s = tx.trim = function (t, n) {
      return t = String(e(t)), 1 & n && (t = t.replace(o, "")), 2 & n && (t = t.replace(f, "")), t;
    };

    n.exports = tx;
  }, {
    135: 135,
    57: 57,
    62: 62,
    64: 64
  }],
  135: [function (t, n, r) {
    n.exports = "\t\n\v\f\r   ᠎             　\u2028\u2029\ufeff";
  }, {}],
  136: [function (t, n, r) {
    function Zx() {
      var t = +this;

      if (d.hasOwnProperty(t)) {
        var n = d[t];
        delete d[t], n();
      }
    }

    function $x(t) {
      Zx.call(t.data);
    }

    var e,
        i,
        o,
        u = t(54),
        c = t(76),
        a = t(73),
        f = t(59),
        s = t(70),
        l = s.process,
        h = s.setImmediate,
        p = s.clearImmediate,
        v = s.MessageChannel,
        g = s.Dispatch,
        y = 0,
        d = {},
        x = "onreadystatechange";
    h && p || (h = function setImmediate(t) {
      for (var n = [], r = 1; r < arguments.length;) n.push(arguments[r++]);

      return d[++y] = function () {
        c("function" == typeof t ? t : Function(t), n);
      }, e(y), y;
    }, p = function clearImmediate(t) {
      delete d[t];
    }, "process" == t(48)(l) ? e = function (t) {
      l.nextTick(u(Zx, t, 1));
    } : g && g.now ? e = function (t) {
      g.now(u(Zx, t, 1));
    } : v ? (o = (i = new v()).port2, i.port1.onmessage = $x, e = u(o.postMessage, o, 1)) : s.addEventListener && "function" == typeof postMessage && !s.importScripts ? (e = function (t) {
      s.postMessage(t + "", "*");
    }, s.addEventListener("message", $x, !1)) : e = x in f("script") ? function (t) {
      a.appendChild(f("script"))[x] = function () {
        a.removeChild(this), Zx.call(t);
      };
    } : function (t) {
      setTimeout(u(Zx, t, 1), 0);
    }), n.exports = {
      set: h,
      clear: p
    };
  }, {
    48: 48,
    54: 54,
    59: 59,
    70: 70,
    73: 73,
    76: 76
  }],
  137: [function (t, n, r) {
    var e = t(139),
        i = Math.max,
        o = Math.min;

    n.exports = function (t, n) {
      return (t = e(t)) < 0 ? i(t + n, 0) : o(t, n);
    };
  }, {
    139: 139
  }],
  138: [function (t, n, r) {
    var e = t(139),
        i = t(141);

    n.exports = function (t) {
      if (void 0 === t) return 0;
      var n = e(t),
          r = i(n);
      if (n !== r) throw RangeError("Wrong length!");
      return r;
    };
  }, {
    139: 139,
    141: 141
  }],
  139: [function (t, n, r) {
    var e = Math.ceil,
        i = Math.floor;

    n.exports = function (t) {
      return isNaN(t = +t) ? 0 : (0 < t ? i : e)(t);
    };
  }, {}],
  140: [function (t, n, r) {
    var e = t(77),
        i = t(57);

    n.exports = function (t) {
      return e(i(t));
    };
  }, {
    57: 57,
    77: 77
  }],
  141: [function (t, n, r) {
    var e = t(139),
        i = Math.min;

    n.exports = function (t) {
      return 0 < t ? i(e(t), 9007199254740991) : 0;
    };
  }, {
    139: 139
  }],
  142: [function (t, n, r) {
    var e = t(57);

    n.exports = function (t) {
      return Object(e(t));
    };
  }, {
    57: 57
  }],
  143: [function (t, n, r) {
    arguments[4][31][0].apply(r, arguments);
  }, {
    31: 31,
    81: 81
  }],
  144: [function (t, n, r) {
    "use strict";

    if (t(58)) {
      var y = t(89),
          d = t(70),
          x = t(64),
          m = t(62),
          S = t(146),
          e = t(145),
          h = t(54),
          b = t(37),
          i = t(116),
          w = t(72),
          o = t(117),
          u = t(139),
          _ = t(141),
          E = t(138),
          c = t(137),
          a = t(143),
          f = t(71),
          O = t(47),
          F = t(81),
          p = t(142),
          v = t(78),
          I = t(98),
          P = t(105),
          A = t(103).f,
          g = t(153),
          s = t(147),
          l = t(152),
          M = t(42),
          k = t(41),
          N = t(127),
          j = t(164),
          R = t(88),
          T = t(86),
          L = t(123),
          C = t(40),
          G = t(39),
          D = t(99),
          U = t(101),
          W = D.f,
          V = U.f,
          B = d.RangeError,
          q = d.TypeError,
          Y = d.Uint8Array,
          z = "ArrayBuffer",
          X = "Shared" + z,
          $ = "BYTES_PER_ELEMENT",
          Q = "prototype",
          Z = Array[Q],
          J = e.ArrayBuffer,
          H = e.DataView,
          K = M(0),
          tt = M(2),
          nt = M(3),
          rt = M(4),
          et = M(5),
          it = M(6),
          ot = k(!0),
          ut = k(!1),
          ct = j.values,
          at = j.keys,
          ft = j.entries,
          st = Z.lastIndexOf,
          lt = Z.reduce,
          ht = Z.reduceRight,
          pt = Z.join,
          vt = Z.sort,
          gt = Z.slice,
          yt = Z.toString,
          dt = Z.toLocaleString,
          xt = l("iterator"),
          mt = l("toStringTag"),
          St = s("typed_constructor"),
          bt = s("def_constructor"),
          wt = S.CONSTR,
          _t = S.TYPED,
          Et = S.VIEW,
          Ot = "Wrong length!",
          Ft = M(1, function (t, n) {
        return kt(N(t, t[bt]), n);
      }),
          It = x(function () {
        return 1 === new Y(new Uint16Array([1]).buffer)[0];
      }),
          Pt = !!Y && !!Y[Q].set && x(function () {
        new Y(1).set({});
      }),
          At = function (t, n) {
        var r = u(t);
        if (r < 0 || r % n) throw B("Wrong offset!");
        return r;
      },
          Mt = function (t) {
        if (F(t) && _t in t) return t;
        throw q(t + " is not a typed array!");
      },
          kt = function (t, n) {
        if (!(F(t) && St in t)) throw q("It is not a typed array constructor!");
        return new t(n);
      },
          Nt = function (t, n) {
        return jt(N(t, t[bt]), n);
      },
          jt = function (t, n) {
        for (var r = 0, e = n.length, i = kt(t, e); r < e;) i[r] = n[r++];

        return i;
      },
          Rt = function (t, n, r) {
        W(t, n, {
          get: function () {
            return this._d[r];
          }
        });
      },
          Tt = function from(t) {
        var n,
            r,
            e,
            i,
            o,
            u,
            c = p(t),
            a = arguments.length,
            f = 1 < a ? arguments[1] : void 0,
            s = void 0 !== f,
            l = g(c);

        if (null != l && !v(l)) {
          for (u = l.call(c), e = [], n = 0; !(o = u.next()).done; n++) e.push(o.value);

          c = e;
        }

        for (s && 2 < a && (f = h(f, arguments[2], 2)), n = 0, r = _(c.length), i = kt(this, r); n < r; n++) i[n] = s ? f(c[n], n) : c[n];

        return i;
      },
          Lt = function of() {
        for (var t = 0, n = arguments.length, r = kt(this, n); t < n;) r[t] = arguments[t++];

        return r;
      },
          Ct = !!Y && x(function () {
        dt.call(new Y(1));
      }),
          Gt = function toLocaleString() {
        return dt.apply(Ct ? gt.call(Mt(this)) : Mt(this), arguments);
      },
          Dt = {
        copyWithin: function copyWithin(t, n) {
          return G.call(Mt(this), t, n, 2 < arguments.length ? arguments[2] : void 0);
        },
        every: function every(t) {
          return rt(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        fill: function fill(t) {
          return C.apply(Mt(this), arguments);
        },
        filter: function filter(t) {
          return Nt(this, tt(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0));
        },
        find: function find(t) {
          return et(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        findIndex: function findIndex(t) {
          return it(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        forEach: function forEach(t) {
          K(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        indexOf: function indexOf(t) {
          return ut(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        includes: function includes(t) {
          return ot(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        join: function join(t) {
          return pt.apply(Mt(this), arguments);
        },
        lastIndexOf: function lastIndexOf(t) {
          return st.apply(Mt(this), arguments);
        },
        map: function map(t) {
          return Ft(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        reduce: function reduce(t) {
          return lt.apply(Mt(this), arguments);
        },
        reduceRight: function reduceRight(t) {
          return ht.apply(Mt(this), arguments);
        },
        reverse: function reverse() {
          for (var t, n = this, r = Mt(n).length, e = Math.floor(r / 2), i = 0; i < e;) t = n[i], n[i++] = n[--r], n[r] = t;

          return n;
        },
        some: function some(t) {
          return nt(Mt(this), t, 1 < arguments.length ? arguments[1] : void 0);
        },
        sort: function sort(t) {
          return vt.call(Mt(this), t);
        },
        subarray: function subarray(t, n) {
          var r = Mt(this),
              e = r.length,
              i = c(t, e);
          return new (N(r, r[bt]))(r.buffer, r.byteOffset + i * r.BYTES_PER_ELEMENT, _((void 0 === n ? e : c(n, e)) - i));
        }
      },
          Ut = function slice(t, n) {
        return Nt(this, gt.call(Mt(this), t, n));
      },
          Wt = function set(t) {
        Mt(this);

        var n = At(arguments[1], 1),
            r = this.length,
            e = p(t),
            i = _(e.length),
            o = 0;

        if (r < i + n) throw B(Ot);

        for (; o < i;) this[n + o] = e[o++];
      },
          Vt = {
        entries: function entries() {
          return ft.call(Mt(this));
        },
        keys: function keys() {
          return at.call(Mt(this));
        },
        values: function values() {
          return ct.call(Mt(this));
        }
      },
          Bt = function (t, n) {
        return F(t) && t[_t] && "symbol" != typeof n && n in t && String(+n) == String(n);
      },
          qt = function getOwnPropertyDescriptor(t, n) {
        return Bt(t, n = a(n, !0)) ? i(2, t[n]) : V(t, n);
      },
          Yt = function defineProperty(t, n, r) {
        return !(Bt(t, n = a(n, !0)) && F(r) && f(r, "value")) || f(r, "get") || f(r, "set") || r.configurable || f(r, "writable") && !r.writable || f(r, "enumerable") && !r.enumerable ? W(t, n, r) : (t[n] = r.value, t);
      };

      wt || (U.f = qt, D.f = Yt), m(m.S + m.F * !wt, "Object", {
        getOwnPropertyDescriptor: qt,
        defineProperty: Yt
      }), x(function () {
        yt.call({});
      }) && (yt = dt = function toString() {
        return pt.call(this);
      });
      var zt = o({}, Dt);
      o(zt, Vt), w(zt, xt, Vt.values), o(zt, {
        slice: Ut,
        set: Wt,
        constructor: function () {},
        toString: yt,
        toLocaleString: Gt
      }), Rt(zt, "buffer", "b"), Rt(zt, "byteOffset", "o"), Rt(zt, "byteLength", "l"), Rt(zt, "length", "e"), W(zt, mt, {
        get: function () {
          return this[_t];
        }
      }), n.exports = function (t, l, n, o) {
        function CC(t, i) {
          W(t, i, {
            get: function () {
              return t = i, (n = this._d).v[r](t * l + n.o, It);
              var t, n;
            },
            set: function (t) {
              return n = i, r = t, e = this._d, o && (r = (r = Math.round(r)) < 0 ? 0 : 255 < r ? 255 : 255 & r), void e.v[u](n * l + e.o, r, It);
              var n, r, e;
            },
            enumerable: !0
          });
        }

        var h = t + ((o = !!o) ? "Clamped" : "") + "Array",
            r = "get" + t,
            u = "set" + t,
            p = d[h],
            c = p || {},
            e = p && P(p),
            i = !p || !S.ABV,
            a = {},
            f = p && p[Q];
        i ? (p = n(function (t, n, r, e) {
          b(t, p, h, "_d");
          var i,
              o,
              u,
              c,
              a = 0,
              f = 0;

          if (F(n)) {
            if (!(n instanceof J || (c = O(n)) == z || c == X)) return _t in n ? jt(p, n) : Tt.call(p, n);
            i = n, f = At(r, l);
            var s = n.byteLength;

            if (void 0 === e) {
              if (s % l) throw B(Ot);
              if ((o = s - f) < 0) throw B(Ot);
            } else if (s < (o = _(e) * l) + f) throw B(Ot);

            u = o / l;
          } else u = E(n), i = new J(o = u * l);

          for (w(t, "_d", {
            b: i,
            o: f,
            l: o,
            e: u,
            v: new H(i)
          }); a < u;) CC(t, a++);
        }), f = p[Q] = I(zt), w(f, "constructor", p)) : x(function () {
          p(1);
        }) && x(function () {
          new p(-1);
        }) && T(function (t) {
          new p(), new p(null), new p(1.5), new p(t);
        }, !0) || (p = n(function (t, n, r, e) {
          var i;
          return b(t, p, h), F(n) ? n instanceof J || (i = O(n)) == z || i == X ? void 0 !== e ? new c(n, At(r, l), e) : void 0 !== r ? new c(n, At(r, l)) : new c(n) : _t in n ? jt(p, n) : Tt.call(p, n) : new c(E(n));
        }), K(e !== Function.prototype ? A(c).concat(A(e)) : A(c), function (t) {
          t in p || w(p, t, c[t]);
        }), p[Q] = f, y || (f.constructor = p));
        var s = f[xt],
            v = !!s && ("values" == s.name || null == s.name),
            g = Vt.values;
        w(p, St, !0), w(f, _t, h), w(f, Et, !0), w(f, bt, p), (o ? new p(1)[mt] == h : mt in f) || W(f, mt, {
          get: function () {
            return h;
          }
        }), a[h] = p, m(m.G + m.W + m.F * (p != c), a), m(m.S, h, {
          BYTES_PER_ELEMENT: l
        }), m(m.S + m.F * x(function () {
          c.of.call(p, 1);
        }), h, {
          from: Tt,
          of: Lt
        }), $ in f || w(f, $, l), m(m.P, h, Dt), L(h), m(m.P + m.F * Pt, h, {
          set: Wt
        }), m(m.P + m.F * !v, h, Vt), y || f.toString == yt || (f.toString = yt), m(m.P + m.F * x(function () {
          new p(1).slice();
        }), h, {
          slice: Ut
        }), m(m.P + m.F * (x(function () {
          return [1, 2].toLocaleString() != new p([1, 2]).toLocaleString();
        }) || !x(function () {
          f.toLocaleString.call([1, 2]);
        })), h, {
          toLocaleString: Gt
        }), R[h] = v ? s : g, y || v || w(f, xt, g);
      };
    } else n.exports = function () {};
  }, {
    101: 101,
    103: 103,
    105: 105,
    116: 116,
    117: 117,
    123: 123,
    127: 127,
    137: 137,
    138: 138,
    139: 139,
    141: 141,
    142: 142,
    143: 143,
    145: 145,
    146: 146,
    147: 147,
    152: 152,
    153: 153,
    164: 164,
    37: 37,
    39: 39,
    40: 40,
    41: 41,
    42: 42,
    47: 47,
    54: 54,
    58: 58,
    62: 62,
    64: 64,
    70: 70,
    71: 71,
    72: 72,
    78: 78,
    81: 81,
    86: 86,
    88: 88,
    89: 89,
    98: 98,
    99: 99
  }],
  145: [function (t, n, r) {
    "use strict";

    var e = t(70),
        i = t(58),
        o = t(89),
        u = t(146),
        c = t(72),
        a = t(117),
        f = t(64),
        s = t(37),
        l = t(139),
        h = t(141),
        p = t(138),
        v = t(103).f,
        g = t(99).f,
        y = t(40),
        d = t(124),
        x = "ArrayBuffer",
        m = "DataView",
        S = "prototype",
        b = "Wrong index!",
        w = e[x],
        _ = e[m],
        E = e.Math,
        O = e.RangeError,
        F = e.Infinity,
        I = w,
        P = E.abs,
        A = E.pow,
        M = E.floor,
        k = E.log,
        N = E.LN2,
        j = "byteLength",
        R = "byteOffset",
        T = i ? "_b" : "buffer",
        L = i ? "_l" : j,
        C = i ? "_o" : R;

    function packIEEE754(t, n, r) {
      var e,
          i,
          o,
          u = new Array(r),
          c = 8 * r - n - 1,
          a = (1 << c) - 1,
          f = a >> 1,
          s = 23 === n ? A(2, -24) - A(2, -77) : 0,
          l = 0,
          h = t < 0 || 0 === t && 1 / t < 0 ? 1 : 0;

      for ((t = P(t)) != t || t === F ? (i = t != t ? 1 : 0, e = a) : (e = M(k(t) / N), t * (o = A(2, -e)) < 1 && (e--, o *= 2), 2 <= (t += 1 <= e + f ? s / o : s * A(2, 1 - f)) * o && (e++, o /= 2), a <= e + f ? (i = 0, e = a) : 1 <= e + f ? (i = (t * o - 1) * A(2, n), e += f) : (i = t * A(2, f - 1) * A(2, n), e = 0)); 8 <= n; u[l++] = 255 & i, i /= 256, n -= 8);

      for (e = e << n | i, c += n; 0 < c; u[l++] = 255 & e, e /= 256, c -= 8);

      return u[--l] |= 128 * h, u;
    }

    function unpackIEEE754(t, n, r) {
      var e,
          i = 8 * r - n - 1,
          o = (1 << i) - 1,
          u = o >> 1,
          c = i - 7,
          a = r - 1,
          f = t[a--],
          s = 127 & f;

      for (f >>= 7; 0 < c; s = 256 * s + t[a], a--, c -= 8);

      for (e = s & (1 << -c) - 1, s >>= -c, c += n; 0 < c; e = 256 * e + t[a], a--, c -= 8);

      if (0 === s) s = 1 - u;else {
        if (s === o) return e ? NaN : f ? -F : F;
        e += A(2, n), s -= u;
      }
      return (f ? -1 : 1) * e * A(2, s - n);
    }

    function unpackI32(t) {
      return t[3] << 24 | t[2] << 16 | t[1] << 8 | t[0];
    }

    function packI8(t) {
      return [255 & t];
    }

    function packI16(t) {
      return [255 & t, t >> 8 & 255];
    }

    function packI32(t) {
      return [255 & t, t >> 8 & 255, t >> 16 & 255, t >> 24 & 255];
    }

    function packF64(t) {
      return packIEEE754(t, 52, 8);
    }

    function packF32(t) {
      return packIEEE754(t, 23, 4);
    }

    function addGetter(t, n, r) {
      g(t[S], n, {
        get: function () {
          return this[r];
        }
      });
    }

    function get(t, n, r, e) {
      var i = p(+r);
      if (i + n > t[L]) throw O(b);
      var o = t[T]._b,
          u = i + t[C],
          c = o.slice(u, u + n);
      return e ? c : c.reverse();
    }

    function set(t, n, r, e, i, o) {
      var u = p(+r);
      if (u + n > t[L]) throw O(b);

      for (var c = t[T]._b, a = u + t[C], f = e(+i), s = 0; s < n; s++) c[a + s] = f[o ? s : n - s - 1];
    }

    if (u.ABV) {
      if (!f(function () {
        w(1);
      }) || !f(function () {
        new w(-1);
      }) || f(function () {
        return new w(), new w(1.5), new w(NaN), w.name != x;
      })) {
        for (var G, D = (w = function ArrayBuffer(t) {
          return s(this, w), new I(p(t));
        })[S] = I[S], U = v(I), W = 0; U.length > W;) (G = U[W++]) in w || c(w, G, I[G]);

        o || (D.constructor = w);
      }

      var V = new _(new w(2)),
          B = _[S].setInt8;
      V.setInt8(0, 2147483648), V.setInt8(1, 2147483649), !V.getInt8(0) && V.getInt8(1) || a(_[S], {
        setInt8: function setInt8(t, n) {
          B.call(this, t, n << 24 >> 24);
        },
        setUint8: function setUint8(t, n) {
          B.call(this, t, n << 24 >> 24);
        }
      }, !0);
    } else w = function ArrayBuffer(t) {
      s(this, w, x);
      var n = p(t);
      this._b = y.call(new Array(n), 0), this[L] = n;
    }, _ = function DataView(t, n, r) {
      s(this, _, m), s(t, w, m);
      var e = t[L],
          i = l(n);
      if (i < 0 || e < i) throw O("Wrong offset!");
      if (e < i + (r = void 0 === r ? e - i : h(r))) throw O("Wrong length!");
      this[T] = t, this[C] = i, this[L] = r;
    }, i && (addGetter(w, j, "_l"), addGetter(_, "buffer", "_b"), addGetter(_, j, "_l"), addGetter(_, R, "_o")), a(_[S], {
      getInt8: function getInt8(t) {
        return get(this, 1, t)[0] << 24 >> 24;
      },
      getUint8: function getUint8(t) {
        return get(this, 1, t)[0];
      },
      getInt16: function getInt16(t) {
        var n = get(this, 2, t, arguments[1]);
        return (n[1] << 8 | n[0]) << 16 >> 16;
      },
      getUint16: function getUint16(t) {
        var n = get(this, 2, t, arguments[1]);
        return n[1] << 8 | n[0];
      },
      getInt32: function getInt32(t) {
        return unpackI32(get(this, 4, t, arguments[1]));
      },
      getUint32: function getUint32(t) {
        return unpackI32(get(this, 4, t, arguments[1])) >>> 0;
      },
      getFloat32: function getFloat32(t) {
        return unpackIEEE754(get(this, 4, t, arguments[1]), 23, 4);
      },
      getFloat64: function getFloat64(t) {
        return unpackIEEE754(get(this, 8, t, arguments[1]), 52, 8);
      },
      setInt8: function setInt8(t, n) {
        set(this, 1, t, packI8, n);
      },
      setUint8: function setUint8(t, n) {
        set(this, 1, t, packI8, n);
      },
      setInt16: function setInt16(t, n) {
        set(this, 2, t, packI16, n, arguments[2]);
      },
      setUint16: function setUint16(t, n) {
        set(this, 2, t, packI16, n, arguments[2]);
      },
      setInt32: function setInt32(t, n) {
        set(this, 4, t, packI32, n, arguments[2]);
      },
      setUint32: function setUint32(t, n) {
        set(this, 4, t, packI32, n, arguments[2]);
      },
      setFloat32: function setFloat32(t, n) {
        set(this, 4, t, packF32, n, arguments[2]);
      },
      setFloat64: function setFloat64(t, n) {
        set(this, 8, t, packF64, n, arguments[2]);
      }
    });

    d(w, x), d(_, m), c(_[S], u.VIEW, !0), r[x] = w, r[m] = _;
  }, {
    103: 103,
    117: 117,
    124: 124,
    138: 138,
    139: 139,
    141: 141,
    146: 146,
    37: 37,
    40: 40,
    58: 58,
    64: 64,
    70: 70,
    72: 72,
    89: 89,
    99: 99
  }],
  146: [function (t, n, r) {
    for (var e, i = t(70), o = t(72), u = t(147), c = u("typed_array"), a = u("view"), f = !(!i.ArrayBuffer || !i.DataView), s = f, l = 0, h = "Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array".split(","); l < 9;) (e = i[h[l++]]) ? (o(e.prototype, c, !0), o(e.prototype, a, !0)) : s = !1;

    n.exports = {
      ABV: f,
      CONSTR: s,
      TYPED: c,
      VIEW: a
    };
  }, {
    147: 147,
    70: 70,
    72: 72
  }],
  147: [function (t, n, r) {
    var e = 0,
        i = Math.random();

    n.exports = function (t) {
      return "Symbol(".concat(void 0 === t ? "" : t, ")_", (++e + i).toString(36));
    };
  }, {}],
  148: [function (t, n, r) {
    var e = t(70).navigator;
    n.exports = e && e.userAgent || "";
  }, {
    70: 70
  }],
  149: [function (t, n, r) {
    var e = t(81);

    n.exports = function (t, n) {
      if (!e(t) || t._t !== n) throw TypeError("Incompatible receiver, " + n + " required!");
      return t;
    };
  }, {
    81: 81
  }],
  150: [function (t, n, r) {
    var e = t(70),
        i = t(52),
        o = t(89),
        u = t(151),
        c = t(99).f;

    n.exports = function (t) {
      var n = i.Symbol || (i.Symbol = o ? {} : e.Symbol || {});
      "_" == t.charAt(0) || t in n || c(n, t, {
        value: u.f(t)
      });
    };
  }, {
    151: 151,
    52: 52,
    70: 70,
    89: 89,
    99: 99
  }],
  151: [function (t, n, r) {
    r.f = t(152);
  }, {
    152: 152
  }],
  152: [function (t, n, r) {
    var e = t(126)("wks"),
        i = t(147),
        o = t(70).Symbol,
        u = "function" == typeof o;
    (n.exports = function (t) {
      return e[t] || (e[t] = u && o[t] || (u ? o : i)("Symbol." + t));
    }).store = e;
  }, {
    126: 126,
    147: 147,
    70: 70
  }],
  153: [function (t, n, r) {
    var e = t(47),
        i = t(152)("iterator"),
        o = t(88);

    n.exports = t(52).getIteratorMethod = function (t) {
      if (null != t) return t[i] || t["@@iterator"] || o[e(t)];
    };
  }, {
    152: 152,
    47: 47,
    52: 52,
    88: 88
  }],
  154: [function (t, n, r) {
    var e = t(62);
    e(e.P, "Array", {
      copyWithin: t(39)
    }), t(35)("copyWithin");
  }, {
    35: 35,
    39: 39,
    62: 62
  }],
  155: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(4);
    e(e.P + e.F * !t(128)([].every, !0), "Array", {
      every: function every(t) {
        return i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    42: 42,
    62: 62
  }],
  156: [function (t, n, r) {
    var e = t(62);
    e(e.P, "Array", {
      fill: t(40)
    }), t(35)("fill");
  }, {
    35: 35,
    40: 40,
    62: 62
  }],
  157: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(2);
    e(e.P + e.F * !t(128)([].filter, !0), "Array", {
      filter: function filter(t) {
        return i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    42: 42,
    62: 62
  }],
  158: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(6),
        o = "findIndex",
        u = !0;
    o in [] && Array(1)[o](function () {
      u = !1;
    }), e(e.P + e.F * u, "Array", {
      findIndex: function findIndex(t) {
        return i(this, t, 1 < arguments.length ? arguments[1] : void 0);
      }
    }), t(35)(o);
  }, {
    35: 35,
    42: 42,
    62: 62
  }],
  159: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(5),
        o = "find",
        u = !0;
    o in [] && Array(1)[o](function () {
      u = !1;
    }), e(e.P + e.F * u, "Array", {
      find: function find(t) {
        return i(this, t, 1 < arguments.length ? arguments[1] : void 0);
      }
    }), t(35)(o);
  }, {
    35: 35,
    42: 42,
    62: 62
  }],
  160: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(0),
        o = t(128)([].forEach, !0);
    e(e.P + e.F * !o, "Array", {
      forEach: function forEach(t) {
        return i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    42: 42,
    62: 62
  }],
  161: [function (t, n, r) {
    "use strict";

    var h = t(54),
        e = t(62),
        p = t(142),
        v = t(83),
        g = t(78),
        y = t(141),
        d = t(53),
        x = t(153);
    e(e.S + e.F * !t(86)(function (t) {
      Array.from(t);
    }), "Array", {
      from: function from(t) {
        var n,
            r,
            e,
            i,
            o = p(t),
            u = "function" == typeof this ? this : Array,
            c = arguments.length,
            a = 1 < c ? arguments[1] : void 0,
            f = void 0 !== a,
            s = 0,
            l = x(o);
        if (f && (a = h(a, 2 < c ? arguments[2] : void 0, 2)), null == l || u == Array && g(l)) for (r = new u(n = y(o.length)); s < n; s++) d(r, s, f ? a(o[s], s) : o[s]);else for (i = l.call(o), r = new u(); !(e = i.next()).done; s++) d(r, s, f ? v(i, a, [e.value, s], !0) : e.value);
        return r.length = s, r;
      }
    });
  }, {
    141: 141,
    142: 142,
    153: 153,
    53: 53,
    54: 54,
    62: 62,
    78: 78,
    83: 83,
    86: 86
  }],
  162: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(41)(!1),
        o = [].indexOf,
        u = !!o && 1 / [1].indexOf(1, -0) < 0;
    e(e.P + e.F * (u || !t(128)(o)), "Array", {
      indexOf: function indexOf(t) {
        return u ? o.apply(this, arguments) || 0 : i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    41: 41,
    62: 62
  }],
  163: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Array", {
      isArray: t(79)
    });
  }, {
    62: 62,
    79: 79
  }],
  164: [function (t, n, r) {
    "use strict";

    var e = t(35),
        i = t(87),
        o = t(88),
        u = t(140);
    n.exports = t(85)(Array, "Array", function (t, n) {
      this._t = u(t), this._i = 0, this._k = n;
    }, function () {
      var t = this._t,
          n = this._k,
          r = this._i++;
      return !t || r >= t.length ? (this._t = void 0, i(1)) : i(0, "keys" == n ? r : "values" == n ? t[r] : [r, t[r]]);
    }, "values"), o.Arguments = o.Array, e("keys"), e("values"), e("entries");
  }, {
    140: 140,
    35: 35,
    85: 85,
    87: 87,
    88: 88
  }],
  165: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(140),
        o = [].join;
    e(e.P + e.F * (t(77) != Object || !t(128)(o)), "Array", {
      join: function join(t) {
        return o.call(i(this), void 0 === t ? "," : t);
      }
    });
  }, {
    128: 128,
    140: 140,
    62: 62,
    77: 77
  }],
  166: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(140),
        o = t(139),
        u = t(141),
        c = [].lastIndexOf,
        a = !!c && 1 / [1].lastIndexOf(1, -0) < 0;
    e(e.P + e.F * (a || !t(128)(c)), "Array", {
      lastIndexOf: function lastIndexOf(t) {
        if (a) return c.apply(this, arguments) || 0;
        var n = i(this),
            r = u(n.length),
            e = r - 1;

        for (1 < arguments.length && (e = Math.min(e, o(arguments[1]))), e < 0 && (e = r + e); 0 <= e; e--) if (e in n && n[e] === t) return e || 0;

        return -1;
      }
    });
  }, {
    128: 128,
    139: 139,
    140: 140,
    141: 141,
    62: 62
  }],
  167: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(1);
    e(e.P + e.F * !t(128)([].map, !0), "Array", {
      map: function map(t) {
        return i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    42: 42,
    62: 62
  }],
  168: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(53);
    e(e.S + e.F * t(64)(function () {
      function F() {}

      return !(Array.of.call(F) instanceof F);
    }), "Array", {
      of: function of() {
        for (var t = 0, n = arguments.length, r = new ("function" == typeof this ? this : Array)(n); t < n;) i(r, t, arguments[t++]);

        return r.length = n, r;
      }
    });
  }, {
    53: 53,
    62: 62,
    64: 64
  }],
  169: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(43);
    e(e.P + e.F * !t(128)([].reduceRight, !0), "Array", {
      reduceRight: function reduceRight(t) {
        return i(this, t, arguments.length, arguments[1], !0);
      }
    });
  }, {
    128: 128,
    43: 43,
    62: 62
  }],
  170: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(43);
    e(e.P + e.F * !t(128)([].reduce, !0), "Array", {
      reduce: function reduce(t) {
        return i(this, t, arguments.length, arguments[1], !1);
      }
    });
  }, {
    128: 128,
    43: 43,
    62: 62
  }],
  171: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(73),
        f = t(48),
        s = t(137),
        l = t(141),
        h = [].slice;
    e(e.P + e.F * t(64)(function () {
      i && h.call(i);
    }), "Array", {
      slice: function slice(t, n) {
        var r = l(this.length),
            e = f(this);
        if (n = void 0 === n ? r : n, "Array" == e) return h.call(this, t, n);

        for (var i = s(t, r), o = s(n, r), u = l(o - i), c = new Array(u), a = 0; a < u; a++) c[a] = "String" == e ? this.charAt(i + a) : this[i + a];

        return c;
      }
    });
  }, {
    137: 137,
    141: 141,
    48: 48,
    62: 62,
    64: 64,
    73: 73
  }],
  172: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(42)(3);
    e(e.P + e.F * !t(128)([].some, !0), "Array", {
      some: function some(t) {
        return i(this, t, arguments[1]);
      }
    });
  }, {
    128: 128,
    42: 42,
    62: 62
  }],
  173: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(33),
        o = t(142),
        u = t(64),
        c = [].sort,
        a = [1, 2, 3];
    e(e.P + e.F * (u(function () {
      a.sort(void 0);
    }) || !u(function () {
      a.sort(null);
    }) || !t(128)(c)), "Array", {
      sort: function sort(t) {
        return void 0 === t ? c.call(o(this)) : c.call(o(this), i(t));
      }
    });
  }, {
    128: 128,
    142: 142,
    33: 33,
    62: 62,
    64: 64
  }],
  174: [function (t, n, r) {
    t(123)("Array");
  }, {
    123: 123
  }],
  175: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Date", {
      now: function () {
        return new Date().getTime();
      }
    });
  }, {
    62: 62
  }],
  176: [function (t, n, r) {
    var e = t(62),
        i = t(55);
    e(e.P + e.F * (Date.prototype.toISOString !== i), "Date", {
      toISOString: i
    });
  }, {
    55: 55,
    62: 62
  }],
  177: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(142),
        o = t(143);
    e(e.P + e.F * t(64)(function () {
      return null !== new Date(NaN).toJSON() || 1 !== Date.prototype.toJSON.call({
        toISOString: function () {
          return 1;
        }
      });
    }), "Date", {
      toJSON: function toJSON(t) {
        var n = i(this),
            r = o(n);
        return "number" != typeof r || isFinite(r) ? n.toISOString() : null;
      }
    });
  }, {
    142: 142,
    143: 143,
    62: 62,
    64: 64
  }],
  178: [function (t, n, r) {
    var e = t(152)("toPrimitive"),
        i = Date.prototype;
    e in i || t(72)(i, e, t(56));
  }, {
    152: 152,
    56: 56,
    72: 72
  }],
  179: [function (t, n, r) {
    var e = Date.prototype,
        i = "Invalid Date",
        o = "toString",
        u = e[o],
        c = e.getTime;
    new Date(NaN) + "" != i && t(118)(e, o, function toString() {
      var t = c.call(this);
      return t == t ? u.call(this) : i;
    });
  }, {
    118: 118
  }],
  180: [function (t, n, r) {
    var e = t(62);
    e(e.P, "Function", {
      bind: t(46)
    });
  }, {
    46: 46,
    62: 62
  }],
  181: [function (t, n, r) {
    "use strict";

    var e = t(81),
        i = t(105),
        o = t(152)("hasInstance"),
        u = Function.prototype;
    o in u || t(99).f(u, o, {
      value: function (t) {
        if ("function" != typeof this || !e(t)) return !1;
        if (!e(this.prototype)) return t instanceof this;

        for (; t = i(t);) if (this.prototype === t) return !0;

        return !1;
      }
    });
  }, {
    105: 105,
    152: 152,
    81: 81,
    99: 99
  }],
  182: [function (t, n, r) {
    var e = t(99).f,
        i = Function.prototype,
        o = /^\s*function ([^ (]*)/;
    "name" in i || t(58) && e(i, "name", {
      configurable: !0,
      get: function () {
        try {
          return ("" + this).match(o)[1];
        } catch (t) {
          return "";
        }
      }
    });
  }, {
    58: 58,
    99: 99
  }],
  183: [function (t, n, r) {
    "use strict";

    var e = t(49),
        i = t(149);
    n.exports = t(51)("Map", function (t) {
      return function Map() {
        return t(this, 0 < arguments.length ? arguments[0] : void 0);
      };
    }, {
      get: function get(t) {
        var n = e.getEntry(i(this, "Map"), t);
        return n && n.v;
      },
      set: function set(t, n) {
        return e.def(i(this, "Map"), 0 === t ? 0 : t, n);
      }
    }, e, !0);
  }, {
    149: 149,
    49: 49,
    51: 51
  }],
  184: [function (t, n, r) {
    var e = t(62),
        i = t(92),
        o = Math.sqrt,
        u = Math.acosh;
    e(e.S + e.F * !(u && 710 == Math.floor(u(Number.MAX_VALUE)) && u(1 / 0) == 1 / 0), "Math", {
      acosh: function acosh(t) {
        return (t = +t) < 1 ? NaN : 94906265.62425156 < t ? Math.log(t) + Math.LN2 : i(t - 1 + o(t - 1) * o(t + 1));
      }
    });
  }, {
    62: 62,
    92: 92
  }],
  185: [function (t, n, r) {
    var e = t(62),
        i = Math.asinh;
    e(e.S + e.F * !(i && 0 < 1 / i(0)), "Math", {
      asinh: function asinh(t) {
        return isFinite(t = +t) && 0 != t ? t < 0 ? -asinh(-t) : Math.log(t + Math.sqrt(t * t + 1)) : t;
      }
    });
  }, {
    62: 62
  }],
  186: [function (t, n, r) {
    var e = t(62),
        i = Math.atanh;
    e(e.S + e.F * !(i && 1 / i(-0) < 0), "Math", {
      atanh: function atanh(t) {
        return 0 == (t = +t) ? t : Math.log((1 + t) / (1 - t)) / 2;
      }
    });
  }, {
    62: 62
  }],
  187: [function (t, n, r) {
    var e = t(62),
        i = t(93);
    e(e.S, "Math", {
      cbrt: function cbrt(t) {
        return i(t = +t) * Math.pow(Math.abs(t), 1 / 3);
      }
    });
  }, {
    62: 62,
    93: 93
  }],
  188: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      clz32: function clz32(t) {
        return (t >>>= 0) ? 31 - Math.floor(Math.log(t + .5) * Math.LOG2E) : 32;
      }
    });
  }, {
    62: 62
  }],
  189: [function (t, n, r) {
    var e = t(62),
        i = Math.exp;
    e(e.S, "Math", {
      cosh: function cosh(t) {
        return (i(t = +t) + i(-t)) / 2;
      }
    });
  }, {
    62: 62
  }],
  190: [function (t, n, r) {
    var e = t(62),
        i = t(90);
    e(e.S + e.F * (i != Math.expm1), "Math", {
      expm1: i
    });
  }, {
    62: 62,
    90: 90
  }],
  191: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      fround: t(91)
    });
  }, {
    62: 62,
    91: 91
  }],
  192: [function (t, n, r) {
    var e = t(62),
        a = Math.abs;
    e(e.S, "Math", {
      hypot: function hypot(t, n) {
        for (var r, e, i = 0, o = 0, u = arguments.length, c = 0; o < u;) c < (r = a(arguments[o++])) ? (i = i * (e = c / r) * e + 1, c = r) : i += 0 < r ? (e = r / c) * e : r;

        return c === 1 / 0 ? 1 / 0 : c * Math.sqrt(i);
      }
    });
  }, {
    62: 62
  }],
  193: [function (t, n, r) {
    var e = t(62),
        i = Math.imul;
    e(e.S + e.F * t(64)(function () {
      return -5 != i(4294967295, 5) || 2 != i.length;
    }), "Math", {
      imul: function imul(t, n) {
        var r = 65535,
            e = +t,
            i = +n,
            o = r & e,
            u = r & i;
        return 0 | o * u + ((r & e >>> 16) * u + o * (r & i >>> 16) << 16 >>> 0);
      }
    });
  }, {
    62: 62,
    64: 64
  }],
  194: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      log10: function log10(t) {
        return Math.log(t) * Math.LOG10E;
      }
    });
  }, {
    62: 62
  }],
  195: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      log1p: t(92)
    });
  }, {
    62: 62,
    92: 92
  }],
  196: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      log2: function log2(t) {
        return Math.log(t) / Math.LN2;
      }
    });
  }, {
    62: 62
  }],
  197: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      sign: t(93)
    });
  }, {
    62: 62,
    93: 93
  }],
  198: [function (t, n, r) {
    var e = t(62),
        i = t(90),
        o = Math.exp;
    e(e.S + e.F * t(64)(function () {
      return -2e-17 != !Math.sinh(-2e-17);
    }), "Math", {
      sinh: function sinh(t) {
        return Math.abs(t = +t) < 1 ? (i(t) - i(-t)) / 2 : (o(t - 1) - o(-t - 1)) * (Math.E / 2);
      }
    });
  }, {
    62: 62,
    64: 64,
    90: 90
  }],
  199: [function (t, n, r) {
    var e = t(62),
        i = t(90),
        o = Math.exp;
    e(e.S, "Math", {
      tanh: function tanh(t) {
        var n = i(t = +t),
            r = i(-t);
        return n == 1 / 0 ? 1 : r == 1 / 0 ? -1 : (n - r) / (o(t) + o(-t));
      }
    });
  }, {
    62: 62,
    90: 90
  }],
  200: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Math", {
      trunc: function trunc(t) {
        return (0 < t ? Math.floor : Math.ceil)(t);
      }
    });
  }, {
    62: 62
  }],
  201: [function (t, n, r) {
    "use strict";

    function EN(t) {
      var n = s(t, !1);

      if ("string" == typeof n && 2 < n.length) {
        var r,
            e,
            i,
            o = (n = x ? n.trim() : h(n, 3)).charCodeAt(0);

        if (43 === o || 45 === o) {
          if (88 === (r = n.charCodeAt(2)) || 120 === r) return NaN;
        } else if (48 === o) {
          switch (n.charCodeAt(1)) {
            case 66:
            case 98:
              e = 2, i = 49;
              break;

            case 79:
            case 111:
              e = 8, i = 55;
              break;

            default:
              return +n;
          }

          for (var u, c = n.slice(2), a = 0, f = c.length; a < f; a++) if ((u = c.charCodeAt(a)) < 48 || i < u) return NaN;

          return parseInt(c, e);
        }
      }

      return +n;
    }

    var e = t(70),
        i = t(71),
        o = t(48),
        u = t(75),
        s = t(143),
        c = t(64),
        a = t(103).f,
        f = t(101).f,
        l = t(99).f,
        h = t(134).trim,
        p = "Number",
        v = e[p],
        g = v,
        y = v.prototype,
        d = o(t(98)(y)) == p,
        x = ("trim" in String.prototype);

    if (!v(" 0o1") || !v("0b1") || v("+0x1")) {
      v = function Number(t) {
        var n = arguments.length < 1 ? 0 : t,
            r = this;
        return r instanceof v && (d ? c(function () {
          y.valueOf.call(r);
        }) : o(r) != p) ? u(new g(EN(n)), r, v) : EN(n);
      };

      for (var m, S = t(58) ? a(g) : "MAX_VALUE,MIN_VALUE,NaN,NEGATIVE_INFINITY,POSITIVE_INFINITY,EPSILON,isFinite,isInteger,isNaN,isSafeInteger,MAX_SAFE_INTEGER,MIN_SAFE_INTEGER,parseFloat,parseInt,isInteger".split(","), b = 0; S.length > b; b++) i(g, m = S[b]) && !i(v, m) && l(v, m, f(g, m));

      (v.prototype = y).constructor = v, t(118)(e, p, v);
    }
  }, {
    101: 101,
    103: 103,
    118: 118,
    134: 134,
    143: 143,
    48: 48,
    58: 58,
    64: 64,
    70: 70,
    71: 71,
    75: 75,
    98: 98,
    99: 99
  }],
  202: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Number", {
      EPSILON: Math.pow(2, -52)
    });
  }, {
    62: 62
  }],
  203: [function (t, n, r) {
    var e = t(62),
        i = t(70).isFinite;
    e(e.S, "Number", {
      isFinite: function isFinite(t) {
        return "number" == typeof t && i(t);
      }
    });
  }, {
    62: 62,
    70: 70
  }],
  204: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Number", {
      isInteger: t(80)
    });
  }, {
    62: 62,
    80: 80
  }],
  205: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Number", {
      isNaN: function isNaN(t) {
        return t != t;
      }
    });
  }, {
    62: 62
  }],
  206: [function (t, n, r) {
    var e = t(62),
        i = t(80),
        o = Math.abs;
    e(e.S, "Number", {
      isSafeInteger: function isSafeInteger(t) {
        return i(t) && o(t) <= 9007199254740991;
      }
    });
  }, {
    62: 62,
    80: 80
  }],
  207: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Number", {
      MAX_SAFE_INTEGER: 9007199254740991
    });
  }, {
    62: 62
  }],
  208: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Number", {
      MIN_SAFE_INTEGER: -9007199254740991
    });
  }, {
    62: 62
  }],
  209: [function (t, n, r) {
    var e = t(62),
        i = t(112);
    e(e.S + e.F * (Number.parseFloat != i), "Number", {
      parseFloat: i
    });
  }, {
    112: 112,
    62: 62
  }],
  210: [function (t, n, r) {
    var e = t(62),
        i = t(113);
    e(e.S + e.F * (Number.parseInt != i), "Number", {
      parseInt: i
    });
  }, {
    113: 113,
    62: 62
  }],
  211: [function (t, n, r) {
    "use strict";

    function XO(t, n) {
      for (var r = -1, e = n; ++r < 6;) e += t * u[r], u[r] = e % 1e7, e = o(e / 1e7);
    }

    function YO(t) {
      for (var n = 6, r = 0; 0 <= --n;) r += u[n], u[n] = o(r / t), r = r % t * 1e7;
    }

    function ZO() {
      for (var t = 6, n = ""; 0 <= --t;) if ("" !== n || 0 === t || 0 !== u[t]) {
        var r = String(u[t]);
        n = "" === n ? r : n + l.call("0", 7 - r.length) + r;
      }

      return n;
    }

    var e = t(62),
        f = t(139),
        s = t(34),
        l = t(133),
        i = 1..toFixed,
        o = Math.floor,
        u = [0, 0, 0, 0, 0, 0],
        h = "Number.toFixed: incorrect invocation!",
        p = function (t, n, r) {
      return 0 === n ? r : n % 2 == 1 ? p(t, n - 1, r * t) : p(t * t, n / 2, r);
    };

    e(e.P + e.F * (!!i && ("0.000" !== 8e-5.toFixed(3) || "1" !== .9.toFixed(0) || "1.25" !== 1.255.toFixed(2) || "1000000000000000128" !== 0xde0b6b3a7640080.toFixed(0)) || !t(64)(function () {
      i.call({});
    })), "Number", {
      toFixed: function toFixed(t) {
        var n,
            r,
            e,
            i,
            o = s(this, h),
            u = f(t),
            c = "",
            a = "0";
        if (u < 0 || 20 < u) throw RangeError(h);
        if (o != o) return "NaN";
        if (o <= -1e21 || 1e21 <= o) return String(o);
        if (o < 0 && (c = "-", o = -o), 1e-21 < o) if (r = (n = function (t) {
          for (var n = 0, r = t; 4096 <= r;) n += 12, r /= 4096;

          for (; 2 <= r;) n += 1, r /= 2;

          return n;
        }(o * p(2, 69, 1)) - 69) < 0 ? o * p(2, -n, 1) : o / p(2, n, 1), r *= 4503599627370496, 0 < (n = 52 - n)) {
          for (XO(0, r), e = u; 7 <= e;) XO(1e7, 0), e -= 7;

          for (XO(p(10, e, 1), 0), e = n - 1; 23 <= e;) YO(1 << 23), e -= 23;

          YO(1 << e), XO(1, 1), YO(2), a = ZO();
        } else XO(0, r), XO(1 << -n, 0), a = ZO() + l.call("0", u);
        return a = 0 < u ? c + ((i = a.length) <= u ? "0." + l.call("0", u - i) + a : a.slice(0, i - u) + "." + a.slice(i - u)) : c + a;
      }
    });
  }, {
    133: 133,
    139: 139,
    34: 34,
    62: 62,
    64: 64
  }],
  212: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(64),
        o = t(34),
        u = 1..toPrecision;
    e(e.P + e.F * (i(function () {
      return "1" !== u.call(1, void 0);
    }) || !i(function () {
      u.call({});
    })), "Number", {
      toPrecision: function toPrecision(t) {
        var n = o(this, "Number#toPrecision: incorrect invocation!");
        return void 0 === t ? u.call(n) : u.call(n, t);
      }
    });
  }, {
    34: 34,
    62: 62,
    64: 64
  }],
  213: [function (t, n, r) {
    var e = t(62);
    e(e.S + e.F, "Object", {
      assign: t(97)
    });
  }, {
    62: 62,
    97: 97
  }],
  214: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Object", {
      create: t(98)
    });
  }, {
    62: 62,
    98: 98
  }],
  215: [function (t, n, r) {
    var e = t(62);
    e(e.S + e.F * !t(58), "Object", {
      defineProperties: t(100)
    });
  }, {
    100: 100,
    58: 58,
    62: 62
  }],
  216: [function (t, n, r) {
    var e = t(62);
    e(e.S + e.F * !t(58), "Object", {
      defineProperty: t(99).f
    });
  }, {
    58: 58,
    62: 62,
    99: 99
  }],
  217: [function (t, n, r) {
    var e = t(81),
        i = t(94).onFreeze;
    t(109)("freeze", function (n) {
      return function freeze(t) {
        return n && e(t) ? n(i(t)) : t;
      };
    });
  }, {
    109: 109,
    81: 81,
    94: 94
  }],
  218: [function (t, n, r) {
    var e = t(140),
        i = t(101).f;
    t(109)("getOwnPropertyDescriptor", function () {
      return function getOwnPropertyDescriptor(t, n) {
        return i(e(t), n);
      };
    });
  }, {
    101: 101,
    109: 109,
    140: 140
  }],
  219: [function (t, n, r) {
    t(109)("getOwnPropertyNames", function () {
      return t(102).f;
    });
  }, {
    102: 102,
    109: 109
  }],
  220: [function (t, n, r) {
    var e = t(142),
        i = t(105);
    t(109)("getPrototypeOf", function () {
      return function getPrototypeOf(t) {
        return i(e(t));
      };
    });
  }, {
    105: 105,
    109: 109,
    142: 142
  }],
  221: [function (t, n, r) {
    var e = t(81);
    t(109)("isExtensible", function (n) {
      return function isExtensible(t) {
        return !!e(t) && (!n || n(t));
      };
    });
  }, {
    109: 109,
    81: 81
  }],
  222: [function (t, n, r) {
    var e = t(81);
    t(109)("isFrozen", function (n) {
      return function isFrozen(t) {
        return !e(t) || !!n && n(t);
      };
    });
  }, {
    109: 109,
    81: 81
  }],
  223: [function (t, n, r) {
    var e = t(81);
    t(109)("isSealed", function (n) {
      return function isSealed(t) {
        return !e(t) || !!n && n(t);
      };
    });
  }, {
    109: 109,
    81: 81
  }],
  224: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Object", {
      is: t(121)
    });
  }, {
    121: 121,
    62: 62
  }],
  225: [function (t, n, r) {
    var e = t(142),
        i = t(107);
    t(109)("keys", function () {
      return function keys(t) {
        return i(e(t));
      };
    });
  }, {
    107: 107,
    109: 109,
    142: 142
  }],
  226: [function (t, n, r) {
    var e = t(81),
        i = t(94).onFreeze;
    t(109)("preventExtensions", function (n) {
      return function preventExtensions(t) {
        return n && e(t) ? n(i(t)) : t;
      };
    });
  }, {
    109: 109,
    81: 81,
    94: 94
  }],
  227: [function (t, n, r) {
    var e = t(81),
        i = t(94).onFreeze;
    t(109)("seal", function (n) {
      return function seal(t) {
        return n && e(t) ? n(i(t)) : t;
      };
    });
  }, {
    109: 109,
    81: 81,
    94: 94
  }],
  228: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Object", {
      setPrototypeOf: t(122).set
    });
  }, {
    122: 122,
    62: 62
  }],
  229: [function (t, n, r) {
    "use strict";

    var e = t(47),
        i = {};
    i[t(152)("toStringTag")] = "z", i + "" != "[object z]" && t(118)(Object.prototype, "toString", function toString() {
      return "[object " + e(this) + "]";
    }, !0);
  }, {
    118: 118,
    152: 152,
    47: 47
  }],
  230: [function (t, n, r) {
    var e = t(62),
        i = t(112);
    e(e.G + e.F * (parseFloat != i), {
      parseFloat: i
    });
  }, {
    112: 112,
    62: 62
  }],
  231: [function (t, n, r) {
    var e = t(62),
        i = t(113);
    e(e.G + e.F * (parseInt != i), {
      parseInt: i
    });
  }, {
    113: 113,
    62: 62
  }],
  232: [function (r, t, n) {
    "use strict";

    function $R() {}

    function fS(t) {
      var n;
      return !(!h(t) || "function" != typeof (n = t.then)) && n;
    }

    function gS(s, r) {
      if (!s._n) {
        s._n = !0;
        var e = s._c;
        x(function () {
          for (var a = s._v, f = 1 == s._s, t = 0, n = function (t) {
            var n,
                r,
                e,
                i = f ? t.ok : t.fail,
                o = t.resolve,
                u = t.reject,
                c = t.domain;

            try {
              i ? (f || (2 == s._h && R(s), s._h = 1), !0 === i ? n = a : (c && c.enter(), n = i(a), c && (c.exit(), e = !0)), n === t.promise ? u(E("Promise-chain cycle")) : (r = fS(n)) ? r.call(n, o, u) : o(n)) : u(a);
            } catch (t) {
              c && !e && c.exit(), u(t);
            }
          }; e.length > t;) n(e[t++]);

          s._c = [], s._n = !1, r && !s._h && N(s);
        });
      }
    }

    function kS(t) {
      var n = this;
      n._d || (n._d = !0, (n = n._w || n)._v = t, n._s = 2, n._a || (n._a = n._c.slice()), gS(n, !0));
    }

    var e,
        i,
        o,
        u,
        c = r(89),
        a = r(70),
        f = r(54),
        s = r(47),
        l = r(62),
        h = r(81),
        p = r(33),
        v = r(37),
        g = r(68),
        y = r(127),
        d = r(136).set,
        x = r(95)(),
        m = r(96),
        S = r(114),
        b = r(148),
        w = r(115),
        _ = "Promise",
        E = a.TypeError,
        O = a.process,
        F = O && O.versions,
        I = F && F.v8 || "",
        P = a[_],
        A = "process" == s(O),
        M = i = m.f,
        k = !!function () {
      try {
        var t = P.resolve(1),
            n = (t.constructor = {})[r(152)("species")] = function (t) {
          t($R, $R);
        };

        return (A || "function" == typeof PromiseRejectionEvent) && t.then($R) instanceof n && 0 !== I.indexOf("6.6") && -1 === b.indexOf("Chrome/66");
      } catch (t) {}
    }(),
        N = function (o) {
      d.call(a, function () {
        var t,
            n,
            r,
            e = o._v,
            i = j(o);
        if (i && (t = S(function () {
          A ? O.emit("unhandledRejection", e, o) : (n = a.onunhandledrejection) ? n({
            promise: o,
            reason: e
          }) : (r = a.console) && r.error && r.error("Unhandled promise rejection", e);
        }), o._h = A || j(o) ? 2 : 1), o._a = void 0, i && t.e) throw t.v;
      });
    },
        j = function (t) {
      return 1 !== t._h && 0 === (t._a || t._c).length;
    },
        R = function (n) {
      d.call(a, function () {
        var t;
        A ? O.emit("rejectionHandled", n) : (t = a.onrejectionhandled) && t({
          promise: n,
          reason: n._v
        });
      });
    },
        T = function (t) {
      var r,
          e = this;

      if (!e._d) {
        e._d = !0, e = e._w || e;

        try {
          if (e === t) throw E("Promise can't be resolved itself");
          (r = fS(t)) ? x(function () {
            var n = {
              _w: e,
              _d: !1
            };

            try {
              r.call(t, f(T, n, 1), f(kS, n, 1));
            } catch (t) {
              kS.call(n, t);
            }
          }) : (e._v = t, e._s = 1, gS(e, !1));
        } catch (t) {
          kS.call({
            _w: e,
            _d: !1
          }, t);
        }
      }
    };

    k || (P = function Promise(t) {
      v(this, P, _, "_h"), p(t), e.call(this);

      try {
        t(f(T, this, 1), f(kS, this, 1));
      } catch (t) {
        kS.call(this, t);
      }
    }, (e = function Promise(t) {
      this._c = [], this._a = void 0, this._s = 0, this._d = !1, this._v = void 0, this._h = 0, this._n = !1;
    }).prototype = r(117)(P.prototype, {
      then: function then(t, n) {
        var r = M(y(this, P));
        return r.ok = "function" != typeof t || t, r.fail = "function" == typeof n && n, r.domain = A ? O.domain : void 0, this._c.push(r), this._a && this._a.push(r), this._s && gS(this, !1), r.promise;
      },
      catch: function (t) {
        return this.then(void 0, t);
      }
    }), o = function () {
      var t = new e();
      this.promise = t, this.resolve = f(T, t, 1), this.reject = f(kS, t, 1);
    }, m.f = M = function (t) {
      return t === P || t === u ? new o(t) : i(t);
    }), l(l.G + l.W + l.F * !k, {
      Promise: P
    }), r(124)(P, _), r(123)(_), u = r(52)[_], l(l.S + l.F * !k, _, {
      reject: function reject(t) {
        var n = M(this);
        return (0, n.reject)(t), n.promise;
      }
    }), l(l.S + l.F * (c || !k), _, {
      resolve: function resolve(t) {
        return w(c && this === u ? P : this, t);
      }
    }), l(l.S + l.F * !(k && r(86)(function (t) {
      P.all(t).catch($R);
    })), _, {
      all: function all(t) {
        var u = this,
            n = M(u),
            c = n.resolve,
            a = n.reject,
            r = S(function () {
          var e = [],
              i = 0,
              o = 1;
          g(t, !1, function (t) {
            var n = i++,
                r = !1;
            e.push(void 0), o++, u.resolve(t).then(function (t) {
              r || (r = !0, e[n] = t, --o || c(e));
            }, a);
          }), --o || c(e);
        });
        return r.e && a(r.v), n.promise;
      },
      race: function race(t) {
        var n = this,
            r = M(n),
            e = r.reject,
            i = S(function () {
          g(t, !1, function (t) {
            n.resolve(t).then(r.resolve, e);
          });
        });
        return i.e && e(i.v), r.promise;
      }
    });
  }, {
    114: 114,
    115: 115,
    117: 117,
    123: 123,
    124: 124,
    127: 127,
    136: 136,
    148: 148,
    152: 152,
    33: 33,
    37: 37,
    47: 47,
    52: 52,
    54: 54,
    62: 62,
    68: 68,
    70: 70,
    81: 81,
    86: 86,
    89: 89,
    95: 95,
    96: 96
  }],
  233: [function (t, n, r) {
    var e = t(62),
        o = t(33),
        u = t(38),
        c = (t(70).Reflect || {}).apply,
        a = Function.apply;
    e(e.S + e.F * !t(64)(function () {
      c(function () {});
    }), "Reflect", {
      apply: function apply(t, n, r) {
        var e = o(t),
            i = u(r);
        return c ? c(e, n, i) : a.call(e, n, i);
      }
    });
  }, {
    33: 33,
    38: 38,
    62: 62,
    64: 64,
    70: 70
  }],
  234: [function (t, n, r) {
    var e = t(62),
        c = t(98),
        a = t(33),
        f = t(38),
        s = t(81),
        i = t(64),
        l = t(46),
        h = (t(70).Reflect || {}).construct,
        p = i(function () {
      function F() {}

      return !(h(function () {}, [], F) instanceof F);
    }),
        v = !i(function () {
      h(function () {});
    });
    e(e.S + e.F * (p || v), "Reflect", {
      construct: function construct(t, n) {
        a(t), f(n);
        var r = arguments.length < 3 ? t : a(arguments[2]);
        if (v && !p) return h(t, n, r);

        if (t == r) {
          switch (n.length) {
            case 0:
              return new t();

            case 1:
              return new t(n[0]);

            case 2:
              return new t(n[0], n[1]);

            case 3:
              return new t(n[0], n[1], n[2]);

            case 4:
              return new t(n[0], n[1], n[2], n[3]);
          }

          var e = [null];
          return e.push.apply(e, n), new (l.apply(t, e))();
        }

        var i = r.prototype,
            o = c(s(i) ? i : Object.prototype),
            u = Function.apply.call(t, o, n);
        return s(u) ? u : o;
      }
    });
  }, {
    33: 33,
    38: 38,
    46: 46,
    62: 62,
    64: 64,
    70: 70,
    81: 81,
    98: 98
  }],
  235: [function (t, n, r) {
    var e = t(99),
        i = t(62),
        o = t(38),
        u = t(143);
    i(i.S + i.F * t(64)(function () {
      Reflect.defineProperty(e.f({}, 1, {
        value: 1
      }), 1, {
        value: 2
      });
    }), "Reflect", {
      defineProperty: function defineProperty(t, n, r) {
        o(t), n = u(n, !0), o(r);

        try {
          return e.f(t, n, r), !0;
        } catch (t) {
          return !1;
        }
      }
    });
  }, {
    143: 143,
    38: 38,
    62: 62,
    64: 64,
    99: 99
  }],
  236: [function (t, n, r) {
    var e = t(62),
        i = t(101).f,
        o = t(38);
    e(e.S, "Reflect", {
      deleteProperty: function deleteProperty(t, n) {
        var r = i(o(t), n);
        return !(r && !r.configurable) && delete t[n];
      }
    });
  }, {
    101: 101,
    38: 38,
    62: 62
  }],
  237: [function (t, n, r) {
    "use strict";

    function IU(t) {
      this._t = i(t), this._i = 0;
      var n,
          r = this._k = [];

      for (n in t) r.push(n);
    }

    var e = t(62),
        i = t(38);
    t(84)(IU, "Object", function () {
      var t,
          n = this._k;

      do {
        if (this._i >= n.length) return {
          value: void 0,
          done: !0
        };
      } while (!((t = n[this._i++]) in this._t));

      return {
        value: t,
        done: !1
      };
    }), e(e.S, "Reflect", {
      enumerate: function enumerate(t) {
        return new IU(t);
      }
    });
  }, {
    38: 38,
    62: 62,
    84: 84
  }],
  238: [function (t, n, r) {
    var e = t(101),
        i = t(62),
        o = t(38);
    i(i.S, "Reflect", {
      getOwnPropertyDescriptor: function getOwnPropertyDescriptor(t, n) {
        return e.f(o(t), n);
      }
    });
  }, {
    101: 101,
    38: 38,
    62: 62
  }],
  239: [function (t, n, r) {
    var e = t(62),
        i = t(105),
        o = t(38);
    e(e.S, "Reflect", {
      getPrototypeOf: function getPrototypeOf(t) {
        return i(o(t));
      }
    });
  }, {
    105: 105,
    38: 38,
    62: 62
  }],
  240: [function (t, n, r) {
    var o = t(101),
        u = t(105),
        c = t(71),
        e = t(62),
        a = t(81),
        f = t(38);
    e(e.S, "Reflect", {
      get: function get(t, n) {
        var r,
            e,
            i = arguments.length < 3 ? t : arguments[2];
        return f(t) === i ? t[n] : (r = o.f(t, n)) ? c(r, "value") ? r.value : void 0 !== r.get ? r.get.call(i) : void 0 : a(e = u(t)) ? get(e, n, i) : void 0;
      }
    });
  }, {
    101: 101,
    105: 105,
    38: 38,
    62: 62,
    71: 71,
    81: 81
  }],
  241: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Reflect", {
      has: function has(t, n) {
        return n in t;
      }
    });
  }, {
    62: 62
  }],
  242: [function (t, n, r) {
    var e = t(62),
        i = t(38),
        o = Object.isExtensible;
    e(e.S, "Reflect", {
      isExtensible: function isExtensible(t) {
        return i(t), !o || o(t);
      }
    });
  }, {
    38: 38,
    62: 62
  }],
  243: [function (t, n, r) {
    var e = t(62);
    e(e.S, "Reflect", {
      ownKeys: t(111)
    });
  }, {
    111: 111,
    62: 62
  }],
  244: [function (t, n, r) {
    var e = t(62),
        i = t(38),
        o = Object.preventExtensions;
    e(e.S, "Reflect", {
      preventExtensions: function preventExtensions(t) {
        i(t);

        try {
          return o && o(t), !0;
        } catch (t) {
          return !1;
        }
      }
    });
  }, {
    38: 38,
    62: 62
  }],
  245: [function (t, n, r) {
    var e = t(62),
        i = t(122);
    i && e(e.S, "Reflect", {
      setPrototypeOf: function setPrototypeOf(t, n) {
        i.check(t, n);

        try {
          return i.set(t, n), !0;
        } catch (t) {
          return !1;
        }
      }
    });
  }, {
    122: 122,
    62: 62
  }],
  246: [function (t, n, r) {
    var c = t(99),
        a = t(101),
        f = t(105),
        s = t(71),
        e = t(62),
        l = t(116),
        h = t(38),
        p = t(81);
    e(e.S, "Reflect", {
      set: function set(t, n, r) {
        var e,
            i,
            o = arguments.length < 4 ? t : arguments[3],
            u = a.f(h(t), n);

        if (!u) {
          if (p(i = f(t))) return set(i, n, r, o);
          u = l(0);
        }

        if (s(u, "value")) {
          if (!1 === u.writable || !p(o)) return !1;

          if (e = a.f(o, n)) {
            if (e.get || e.set || !1 === e.writable) return !1;
            e.value = r, c.f(o, n, e);
          } else c.f(o, n, l(0, r));

          return !0;
        }

        return void 0 !== u.set && (u.set.call(o, r), !0);
      }
    });
  }, {
    101: 101,
    105: 105,
    116: 116,
    38: 38,
    62: 62,
    71: 71,
    81: 81,
    99: 99
  }],
  247: [function (t, n, r) {
    var e = t(70),
        o = t(75),
        i = t(99).f,
        u = t(103).f,
        c = t(82),
        a = t(66),
        f = e.RegExp,
        s = f,
        l = f.prototype,
        h = /a/g,
        p = /a/g,
        v = new f(h) !== h;

    if (t(58) && (!v || t(64)(function () {
      return p[t(152)("match")] = !1, f(h) != h || f(p) == p || "/a/i" != f(h, "i");
    }))) {
      f = function RegExp(t, n) {
        var r = this instanceof f,
            e = c(t),
            i = void 0 === n;
        return !r && e && t.constructor === f && i ? t : o(v ? new s(e && !i ? t.source : t, n) : s((e = t instanceof f) ? t.source : t, e && i ? a.call(t) : n), r ? this : l, f);
      };

      function DW(n) {
        n in f || i(f, n, {
          configurable: !0,
          get: function () {
            return s[n];
          },
          set: function (t) {
            s[n] = t;
          }
        });
      }

      for (var g = u(s), y = 0; g.length > y;) DW(g[y++]);

      (l.constructor = f).prototype = l, t(118)(e, "RegExp", f);
    }

    t(123)("RegExp");
  }, {
    103: 103,
    118: 118,
    123: 123,
    152: 152,
    58: 58,
    64: 64,
    66: 66,
    70: 70,
    75: 75,
    82: 82,
    99: 99
  }],
  248: [function (t, n, r) {
    "use strict";

    var e = t(120);
    t(62)({
      target: "RegExp",
      proto: !0,
      forced: e !== /./.exec
    }, {
      exec: e
    });
  }, {
    120: 120,
    62: 62
  }],
  249: [function (t, n, r) {
    t(58) && "g" != /./g.flags && t(99).f(RegExp.prototype, "flags", {
      configurable: !0,
      get: t(66)
    });
  }, {
    58: 58,
    66: 66,
    99: 99
  }],
  250: [function (t, n, r) {
    "use strict";

    var l = t(38),
        h = t(141),
        p = t(36),
        v = t(119);
    t(65)("match", 1, function (e, i, f, s) {
      return [function match(t) {
        var n = e(this),
            r = null == t ? void 0 : t[i];
        return void 0 !== r ? r.call(t, n) : new RegExp(t)[i](String(n));
      }, function (t) {
        var n = s(f, t, this);
        if (n.done) return n.value;
        var r = l(t),
            e = String(this);
        if (!r.global) return v(r, e);

        for (var i, o = r.unicode, u = [], c = r.lastIndex = 0; null !== (i = v(r, e));) {
          var a = String(i[0]);
          "" === (u[c] = a) && (r.lastIndex = p(e, h(r.lastIndex), o)), c++;
        }

        return 0 === c ? null : u;
      }];
    });
  }, {
    119: 119,
    141: 141,
    36: 36,
    38: 38,
    65: 65
  }],
  251: [function (t, n, r) {
    "use strict";

    var _ = t(38),
        e = t(142),
        E = t(141),
        O = t(139),
        F = t(36),
        I = t(119),
        P = Math.max,
        A = Math.min,
        h = Math.floor,
        p = /\$([$&`']|\d\d?|<[^>]*>)/g,
        v = /\$([$&`']|\d\d?)/g;

    t(65)("replace", 2, function (i, o, b, w) {
      return [function replace(t, n) {
        var r = i(this),
            e = null == t ? void 0 : t[o];
        return void 0 !== e ? e.call(t, r, n) : b.call(String(r), t, n);
      }, function (t, n) {
        var r = w(b, t, this, n);
        if (r.done) return r.value;

        var e = _(t),
            i = String(this),
            o = "function" == typeof n;

        o || (n = String(n));
        var u = e.global;

        if (u) {
          var c = e.unicode;
          e.lastIndex = 0;
        }

        for (var a = [];;) {
          var f = I(e, i);
          if (null === f) break;
          if (a.push(f), !u) break;
          "" === String(f[0]) && (e.lastIndex = F(i, E(e.lastIndex), c));
        }

        for (var s, l = "", h = 0, p = 0; p < a.length; p++) {
          f = a[p];

          for (var v = String(f[0]), g = P(A(O(f.index), i.length), 0), y = [], d = 1; d < f.length; d++) y.push(void 0 === (s = f[d]) ? s : String(s));

          var x = f.groups;

          if (o) {
            var m = [v].concat(y, g, i);
            void 0 !== x && m.push(x);
            var S = String(n.apply(void 0, m));
          } else S = getSubstitution(v, i, g, y, x, n);

          h <= g && (l += i.slice(h, g) + S, h = g + v.length);
        }

        return l + i.slice(h);
      }];

      function getSubstitution(o, u, c, a, f, t) {
        var s = c + o.length,
            l = a.length,
            n = v;
        return void 0 !== f && (f = e(f), n = p), b.call(t, n, function (t, n) {
          var r;

          switch (n.charAt(0)) {
            case "$":
              return "$";

            case "&":
              return o;

            case "`":
              return u.slice(0, c);

            case "'":
              return u.slice(s);

            case "<":
              r = f[n.slice(1, -1)];
              break;

            default:
              var e = +n;
              if (0 == e) return t;

              if (l < e) {
                var i = h(e / 10);
                return 0 === i ? t : i <= l ? void 0 === a[i - 1] ? n.charAt(1) : a[i - 1] + n.charAt(1) : t;
              }

              r = a[e - 1];
          }

          return void 0 === r ? "" : r;
        });
      }
    });
  }, {
    119: 119,
    139: 139,
    141: 141,
    142: 142,
    36: 36,
    38: 38,
    65: 65
  }],
  252: [function (t, n, r) {
    "use strict";

    var a = t(38),
        f = t(121),
        s = t(119);
    t(65)("search", 1, function (e, i, u, c) {
      return [function search(t) {
        var n = e(this),
            r = null == t ? void 0 : t[i];
        return void 0 !== r ? r.call(t, n) : new RegExp(t)[i](String(n));
      }, function (t) {
        var n = c(u, t, this);
        if (n.done) return n.value;
        var r = a(t),
            e = String(this),
            i = r.lastIndex;
        f(i, 0) || (r.lastIndex = 0);
        var o = s(r, e);
        return f(r.lastIndex, i) || (r.lastIndex = i), null === o ? -1 : o.index;
      }];
    });
  }, {
    119: 119,
    121: 121,
    38: 38,
    65: 65
  }],
  253: [function (t, n, r) {
    "use strict";

    var l = t(82),
        m = t(38),
        S = t(127),
        b = t(36),
        w = t(141),
        _ = t(119),
        h = t(120),
        e = t(64),
        E = Math.min,
        p = [].push,
        u = "split",
        v = "length",
        g = "lastIndex",
        O = 4294967295,
        F = !e(function () {
      RegExp(O, "y");
    });

    t(65)("split", 2, function (i, o, y, d) {
      var x;
      return x = "c" == "abbc"[u](/(b)*/)[1] || 4 != "test"[u](/(?:)/, -1)[v] || 2 != "ab"[u](/(?:ab)*/)[v] || 4 != "."[u](/(.?)(.?)/)[v] || 1 < "."[u](/()()/)[v] || ""[u](/.?/)[v] ? function (t, n) {
        var r = String(this);
        if (void 0 === t && 0 === n) return [];
        if (!l(t)) return y.call(r, t, n);

        for (var e, i, o, u = [], c = (t.ignoreCase ? "i" : "") + (t.multiline ? "m" : "") + (t.unicode ? "u" : "") + (t.sticky ? "y" : ""), a = 0, f = void 0 === n ? O : n >>> 0, s = new RegExp(t.source, c + "g"); (e = h.call(s, r)) && !(a < (i = s[g]) && (u.push(r.slice(a, e.index)), 1 < e[v] && e.index < r[v] && p.apply(u, e.slice(1)), o = e[0][v], a = i, u[v] >= f));) s[g] === e.index && s[g]++;

        return a === r[v] ? !o && s.test("") || u.push("") : u.push(r.slice(a)), u[v] > f ? u.slice(0, f) : u;
      } : "0"[u](void 0, 0)[v] ? function (t, n) {
        return void 0 === t && 0 === n ? [] : y.call(this, t, n);
      } : y, [function split(t, n) {
        var r = i(this),
            e = null == t ? void 0 : t[o];
        return void 0 !== e ? e.call(t, r, n) : x.call(String(r), t, n);
      }, function (t, n) {
        var r = d(x, t, this, n, x !== y);
        if (r.done) return r.value;
        var e = m(t),
            i = String(this),
            o = S(e, RegExp),
            u = e.unicode,
            c = (e.ignoreCase ? "i" : "") + (e.multiline ? "m" : "") + (e.unicode ? "u" : "") + (F ? "y" : "g"),
            a = new o(F ? e : "^(?:" + e.source + ")", c),
            f = void 0 === n ? O : n >>> 0;
        if (0 == f) return [];
        if (0 === i.length) return null === _(a, i) ? [i] : [];

        for (var s = 0, l = 0, h = []; l < i.length;) {
          a.lastIndex = F ? l : 0;

          var p,
              v = _(a, F ? i : i.slice(l));

          if (null === v || (p = E(w(a.lastIndex + (F ? 0 : l)), i.length)) === s) l = b(i, l, u);else {
            if (h.push(i.slice(s, l)), h.length === f) return h;

            for (var g = 1; g <= v.length - 1; g++) if (h.push(v[g]), h.length === f) return h;

            l = s = p;
          }
        }

        return h.push(i.slice(s)), h;
      }];
    });
  }, {
    119: 119,
    120: 120,
    127: 127,
    141: 141,
    36: 36,
    38: 38,
    64: 64,
    65: 65,
    82: 82
  }],
  254: [function (n, t, r) {
    "use strict";

    n(249);

    function XZ(t) {
      n(118)(RegExp.prototype, u, t, !0);
    }

    var e = n(38),
        i = n(66),
        o = n(58),
        u = "toString",
        c = /./[u];
    n(64)(function () {
      return "/a/b" != c.call({
        source: "a",
        flags: "b"
      });
    }) ? XZ(function toString() {
      var t = e(this);
      return "/".concat(t.source, "/", "flags" in t ? t.flags : !o && t instanceof RegExp ? i.call(t) : void 0);
    }) : c.name != u && XZ(function toString() {
      return c.call(this);
    });
  }, {
    118: 118,
    249: 249,
    38: 38,
    58: 58,
    64: 64,
    66: 66
  }],
  255: [function (t, n, r) {
    "use strict";

    var e = t(49),
        i = t(149);
    n.exports = t(51)("Set", function (t) {
      return function Set() {
        return t(this, 0 < arguments.length ? arguments[0] : void 0);
      };
    }, {
      add: function add(t) {
        return e.def(i(this, "Set"), t = 0 === t ? 0 : t, t);
      }
    }, e);
  }, {
    149: 149,
    49: 49,
    51: 51
  }],
  256: [function (t, n, r) {
    "use strict";

    t(131)("anchor", function (n) {
      return function anchor(t) {
        return n(this, "a", "name", t);
      };
    });
  }, {
    131: 131
  }],
  257: [function (t, n, r) {
    "use strict";

    t(131)("big", function (t) {
      return function big() {
        return t(this, "big", "", "");
      };
    });
  }, {
    131: 131
  }],
  258: [function (t, n, r) {
    "use strict";

    t(131)("blink", function (t) {
      return function blink() {
        return t(this, "blink", "", "");
      };
    });
  }, {
    131: 131
  }],
  259: [function (t, n, r) {
    "use strict";

    t(131)("bold", function (t) {
      return function bold() {
        return t(this, "b", "", "");
      };
    });
  }, {
    131: 131
  }],
  260: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(129)(!1);
    e(e.P, "String", {
      codePointAt: function codePointAt(t) {
        return i(this, t);
      }
    });
  }, {
    129: 129,
    62: 62
  }],
  261: [function (t, n, r) {
    "use strict";

    var e = t(62),
        u = t(141),
        c = t(130),
        a = "endsWith",
        f = ""[a];
    e(e.P + e.F * t(63)(a), "String", {
      endsWith: function endsWith(t) {
        var n = c(this, t, a),
            r = 1 < arguments.length ? arguments[1] : void 0,
            e = u(n.length),
            i = void 0 === r ? e : Math.min(u(r), e),
            o = String(t);
        return f ? f.call(n, o, i) : n.slice(i - o.length, i) === o;
      }
    });
  }, {
    130: 130,
    141: 141,
    62: 62,
    63: 63
  }],
  262: [function (t, n, r) {
    "use strict";

    t(131)("fixed", function (t) {
      return function fixed() {
        return t(this, "tt", "", "");
      };
    });
  }, {
    131: 131
  }],
  263: [function (t, n, r) {
    "use strict";

    t(131)("fontcolor", function (n) {
      return function fontcolor(t) {
        return n(this, "font", "color", t);
      };
    });
  }, {
    131: 131
  }],
  264: [function (t, n, r) {
    "use strict";

    t(131)("fontsize", function (n) {
      return function fontsize(t) {
        return n(this, "font", "size", t);
      };
    });
  }, {
    131: 131
  }],
  265: [function (t, n, r) {
    var e = t(62),
        o = t(137),
        u = String.fromCharCode,
        i = String.fromCodePoint;
    e(e.S + e.F * (!!i && 1 != i.length), "String", {
      fromCodePoint: function fromCodePoint(t) {
        for (var n, r = [], e = arguments.length, i = 0; i < e;) {
          if (n = +arguments[i++], o(n, 1114111) !== n) throw RangeError(n + " is not a valid code point");
          r.push(n < 65536 ? u(n) : u(55296 + ((n -= 65536) >> 10), n % 1024 + 56320));
        }

        return r.join("");
      }
    });
  }, {
    137: 137,
    62: 62
  }],
  266: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(130),
        o = "includes";
    e(e.P + e.F * t(63)(o), "String", {
      includes: function includes(t) {
        return !!~i(this, t, o).indexOf(t, 1 < arguments.length ? arguments[1] : void 0);
      }
    });
  }, {
    130: 130,
    62: 62,
    63: 63
  }],
  267: [function (t, n, r) {
    "use strict";

    t(131)("italics", function (t) {
      return function italics() {
        return t(this, "i", "", "");
      };
    });
  }, {
    131: 131
  }],
  268: [function (t, n, r) {
    "use strict";

    var e = t(129)(!0);
    t(85)(String, "String", function (t) {
      this._t = String(t), this._i = 0;
    }, function () {
      var t,
          n = this._t,
          r = this._i;
      return r >= n.length ? {
        value: void 0,
        done: !0
      } : (t = e(n, r), this._i += t.length, {
        value: t,
        done: !1
      });
    });
  }, {
    129: 129,
    85: 85
  }],
  269: [function (t, n, r) {
    "use strict";

    t(131)("link", function (n) {
      return function link(t) {
        return n(this, "a", "href", t);
      };
    });
  }, {
    131: 131
  }],
  270: [function (t, n, r) {
    var e = t(62),
        u = t(140),
        c = t(141);
    e(e.S, "String", {
      raw: function raw(t) {
        for (var n = u(t.raw), r = c(n.length), e = arguments.length, i = [], o = 0; o < r;) i.push(String(n[o++])), o < e && i.push(String(arguments[o]));

        return i.join("");
      }
    });
  }, {
    140: 140,
    141: 141,
    62: 62
  }],
  271: [function (t, n, r) {
    var e = t(62);
    e(e.P, "String", {
      repeat: t(133)
    });
  }, {
    133: 133,
    62: 62
  }],
  272: [function (t, n, r) {
    "use strict";

    t(131)("small", function (t) {
      return function small() {
        return t(this, "small", "", "");
      };
    });
  }, {
    131: 131
  }],
  273: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(141),
        o = t(130),
        u = "startsWith",
        c = ""[u];
    e(e.P + e.F * t(63)(u), "String", {
      startsWith: function startsWith(t) {
        var n = o(this, t, u),
            r = i(Math.min(1 < arguments.length ? arguments[1] : void 0, n.length)),
            e = String(t);
        return c ? c.call(n, e, r) : n.slice(r, r + e.length) === e;
      }
    });
  }, {
    130: 130,
    141: 141,
    62: 62,
    63: 63
  }],
  274: [function (t, n, r) {
    "use strict";

    t(131)("strike", function (t) {
      return function strike() {
        return t(this, "strike", "", "");
      };
    });
  }, {
    131: 131
  }],
  275: [function (t, n, r) {
    "use strict";

    t(131)("sub", function (t) {
      return function sub() {
        return t(this, "sub", "", "");
      };
    });
  }, {
    131: 131
  }],
  276: [function (t, n, r) {
    "use strict";

    t(131)("sup", function (t) {
      return function sup() {
        return t(this, "sup", "", "");
      };
    });
  }, {
    131: 131
  }],
  277: [function (t, n, r) {
    "use strict";

    t(134)("trim", function (t) {
      return function trim() {
        return t(this, 3);
      };
    });
  }, {
    134: 134
  }],
  278: [function (t, n, r) {
    "use strict";

    function B1(t) {
      var n = W[t] = E(j[L]);
      return n._k = t, n;
    }

    function E1(t, n) {
      x(t);

      for (var r, e = y(n = b(n)), i = 0, o = e.length; i < o;) Q(t, r = e[i++], n[r]);

      return t;
    }

    function G1(t) {
      var n = D.call(this, t = w(t, !0));
      return !(this === B && u(W, t) && !u(V, t)) && (!(n || !u(this, t) || !u(W, t) || u(this, C) && this[C][t]) || n);
    }

    function H1(t, n) {
      if (t = b(t), n = w(n, !0), t !== B || !u(W, n) || u(V, n)) {
        var r = M(t, n);
        return !r || !u(W, n) || u(t, C) && t[C][n] || (r.enumerable = !0), r;
      }
    }

    function I1(t) {
      for (var n, r = N(b(t)), e = [], i = 0; r.length > i;) u(W, n = r[i++]) || n == C || n == a || e.push(n);

      return e;
    }

    function J1(t) {
      for (var n, r = t === B, e = N(r ? V : b(t)), i = [], o = 0; e.length > o;) !u(W, n = e[o++]) || r && !u(B, n) || i.push(W[n]);

      return i;
    }

    var e = t(70),
        u = t(71),
        i = t(58),
        o = t(62),
        c = t(118),
        a = t(94).KEY,
        f = t(64),
        s = t(126),
        l = t(124),
        h = t(147),
        p = t(152),
        v = t(151),
        g = t(150),
        y = t(61),
        d = t(79),
        x = t(38),
        m = t(81),
        S = t(142),
        b = t(140),
        w = t(143),
        _ = t(116),
        E = t(98),
        O = t(102),
        F = t(101),
        I = t(104),
        P = t(99),
        A = t(107),
        M = F.f,
        k = P.f,
        N = O.f,
        j = e.Symbol,
        R = e.JSON,
        T = R && R.stringify,
        L = "prototype",
        C = p("_hidden"),
        G = p("toPrimitive"),
        D = {}.propertyIsEnumerable,
        U = s("symbol-registry"),
        W = s("symbols"),
        V = s("op-symbols"),
        B = Object[L],
        q = "function" == typeof j && !!I.f,
        Y = e.QObject,
        z = !Y || !Y[L] || !Y[L].findChild,
        X = i && f(function () {
      return 7 != E(k({}, "a", {
        get: function () {
          return k(this, "a", {
            value: 7
          }).a;
        }
      })).a;
    }) ? function (t, n, r) {
      var e = M(B, n);
      e && delete B[n], k(t, n, r), e && t !== B && k(B, n, e);
    } : k,
        $ = q && "symbol" == typeof j.iterator ? function (t) {
      return "symbol" == typeof t;
    } : function (t) {
      return t instanceof j;
    },
        Q = function defineProperty(t, n, r) {
      return t === B && Q(V, n, r), x(t), n = w(n, !0), x(r), u(W, n) ? (r.enumerable ? (u(t, C) && t[C][n] && (t[C][n] = !1), r = E(r, {
        enumerable: _(0, !1)
      })) : (u(t, C) || k(t, C, _(1, {})), t[C][n] = !0), X(t, n, r)) : k(t, n, r);
    };

    q || (c((j = function Symbol() {
      if (this instanceof j) throw TypeError("Symbol is not a constructor!");

      var n = h(0 < arguments.length ? arguments[0] : void 0),
          r = function (t) {
        this === B && r.call(V, t), u(this, C) && u(this[C], n) && (this[C][n] = !1), X(this, n, _(1, t));
      };

      return i && z && X(B, n, {
        configurable: !0,
        set: r
      }), B1(n);
    })[L], "toString", function toString() {
      return this._k;
    }), F.f = H1, P.f = Q, t(103).f = O.f = I1, t(108).f = G1, I.f = J1, i && !t(89) && c(B, "propertyIsEnumerable", G1, !0), v.f = function (t) {
      return B1(p(t));
    }), o(o.G + o.W + o.F * !q, {
      Symbol: j
    });

    for (var Z = "hasInstance,isConcatSpreadable,iterator,match,replace,search,species,split,toPrimitive,toStringTag,unscopables".split(","), J = 0; Z.length > J;) p(Z[J++]);

    for (var H = A(p.store), K = 0; H.length > K;) g(H[K++]);

    o(o.S + o.F * !q, "Symbol", {
      for: function (t) {
        return u(U, t += "") ? U[t] : U[t] = j(t);
      },
      keyFor: function keyFor(t) {
        if (!$(t)) throw TypeError(t + " is not a symbol!");

        for (var n in U) if (U[n] === t) return n;
      },
      useSetter: function () {
        z = !0;
      },
      useSimple: function () {
        z = !1;
      }
    }), o(o.S + o.F * !q, "Object", {
      create: function create(t, n) {
        return void 0 === n ? E(t) : E1(E(t), n);
      },
      defineProperty: Q,
      defineProperties: E1,
      getOwnPropertyDescriptor: H1,
      getOwnPropertyNames: I1,
      getOwnPropertySymbols: J1
    });
    var tt = f(function () {
      I.f(1);
    });
    o(o.S + o.F * tt, "Object", {
      getOwnPropertySymbols: function getOwnPropertySymbols(t) {
        return I.f(S(t));
      }
    }), R && o(o.S + o.F * (!q || f(function () {
      var t = j();
      return "[null]" != T([t]) || "{}" != T({
        a: t
      }) || "{}" != T(Object(t));
    })), "JSON", {
      stringify: function stringify(t) {
        for (var n, r, e = [t], i = 1; i < arguments.length;) e.push(arguments[i++]);

        if (r = n = e[1], (m(n) || void 0 !== t) && !$(t)) return d(n) || (n = function (t, n) {
          if ("function" == typeof r && (n = r.call(this, t, n)), !$(n)) return n;
        }), e[1] = n, T.apply(R, e);
      }
    }), j[L][G] || t(72)(j[L], G, j[L].valueOf), l(j, "Symbol"), l(Math, "Math", !0), l(e.JSON, "JSON", !0);
  }, {
    101: 101,
    102: 102,
    103: 103,
    104: 104,
    107: 107,
    108: 108,
    116: 116,
    118: 118,
    124: 124,
    126: 126,
    140: 140,
    142: 142,
    143: 143,
    147: 147,
    150: 150,
    151: 151,
    152: 152,
    38: 38,
    58: 58,
    61: 61,
    62: 62,
    64: 64,
    70: 70,
    71: 71,
    72: 72,
    79: 79,
    81: 81,
    89: 89,
    94: 94,
    98: 98,
    99: 99
  }],
  279: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(146),
        o = t(145),
        f = t(38),
        s = t(137),
        l = t(141),
        u = t(81),
        c = t(70).ArrayBuffer,
        h = t(127),
        p = o.ArrayBuffer,
        v = o.DataView,
        a = i.ABV && c.isView,
        g = p.prototype.slice,
        y = i.VIEW,
        d = "ArrayBuffer";
    e(e.G + e.W + e.F * (c !== p), {
      ArrayBuffer: p
    }), e(e.S + e.F * !i.CONSTR, d, {
      isView: function isView(t) {
        return a && a(t) || u(t) && y in t;
      }
    }), e(e.P + e.U + e.F * t(64)(function () {
      return !new p(2).slice(1, void 0).byteLength;
    }), d, {
      slice: function slice(t, n) {
        if (void 0 !== g && void 0 === n) return g.call(f(this), t);

        for (var r = f(this).byteLength, e = s(t, r), i = s(void 0 === n ? r : n, r), o = new (h(this, p))(l(i - e)), u = new v(this), c = new v(o), a = 0; e < i;) c.setUint8(a++, u.getUint8(e++));

        return o;
      }
    }), t(123)(d);
  }, {
    123: 123,
    127: 127,
    137: 137,
    141: 141,
    145: 145,
    146: 146,
    38: 38,
    62: 62,
    64: 64,
    70: 70,
    81: 81
  }],
  280: [function (t, n, r) {
    var e = t(62);
    e(e.G + e.W + e.F * !t(146).ABV, {
      DataView: t(145).DataView
    });
  }, {
    145: 145,
    146: 146,
    62: 62
  }],
  281: [function (t, n, r) {
    t(144)("Float32", 4, function (e) {
      return function Float32Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  282: [function (t, n, r) {
    t(144)("Float64", 8, function (e) {
      return function Float64Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  283: [function (t, n, r) {
    t(144)("Int16", 2, function (e) {
      return function Int16Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  284: [function (t, n, r) {
    t(144)("Int32", 4, function (e) {
      return function Int32Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  285: [function (t, n, r) {
    t(144)("Int8", 1, function (e) {
      return function Int8Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  286: [function (t, n, r) {
    t(144)("Uint16", 2, function (e) {
      return function Uint16Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  287: [function (t, n, r) {
    t(144)("Uint32", 4, function (e) {
      return function Uint32Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  288: [function (t, n, r) {
    t(144)("Uint8", 1, function (e) {
      return function Uint8Array(t, n, r) {
        return e(this, t, n, r);
      };
    });
  }, {
    144: 144
  }],
  289: [function (t, n, r) {
    t(144)("Uint8", 1, function (e) {
      return function Uint8ClampedArray(t, n, r) {
        return e(this, t, n, r);
      };
    }, !0);
  }, {
    144: 144
  }],
  290: [function (t, n, r) {
    "use strict";

    function R4(t) {
      return function WeakMap() {
        return t(this, 0 < arguments.length ? arguments[0] : void 0);
      };
    }

    var o,
        e = t(70),
        i = t(42)(0),
        u = t(118),
        c = t(94),
        a = t(97),
        f = t(50),
        s = t(81),
        l = t(149),
        h = t(149),
        p = !e.ActiveXObject && "ActiveXObject" in e,
        v = "WeakMap",
        g = c.getWeak,
        y = Object.isExtensible,
        d = f.ufstore,
        x = {
      get: function get(t) {
        if (s(t)) {
          var n = g(t);
          return !0 === n ? d(l(this, v)).get(t) : n ? n[this._i] : void 0;
        }
      },
      set: function set(t, n) {
        return f.def(l(this, v), t, n);
      }
    },
        m = n.exports = t(51)(v, R4, x, f, !0, !0);
    h && p && (a((o = f.getConstructor(R4, v)).prototype, x), c.NEED = !0, i(["delete", "has", "get", "set"], function (e) {
      var t = m.prototype,
          i = t[e];
      u(t, e, function (t, n) {
        if (!s(t) || y(t)) return i.call(this, t, n);
        this._f || (this._f = new o());

        var r = this._f[e](t, n);

        return "set" == e ? this : r;
      });
    }));
  }, {
    118: 118,
    149: 149,
    42: 42,
    50: 50,
    51: 51,
    70: 70,
    81: 81,
    94: 94,
    97: 97
  }],
  291: [function (t, n, r) {
    "use strict";

    var e = t(50),
        i = t(149),
        o = "WeakSet";
    t(51)(o, function (t) {
      return function WeakSet() {
        return t(this, 0 < arguments.length ? arguments[0] : void 0);
      };
    }, {
      add: function add(t) {
        return e.def(i(this, o), t, !0);
      }
    }, e, !1, !0);
  }, {
    149: 149,
    50: 50,
    51: 51
  }],
  292: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(67),
        o = t(142),
        u = t(141),
        c = t(33),
        a = t(45);
    e(e.P, "Array", {
      flatMap: function flatMap(t) {
        var n,
            r,
            e = o(this);
        return c(t), n = u(e.length), r = a(e, 0), i(r, e, e, n, 0, 1, t, arguments[1]), r;
      }
    }), t(35)("flatMap");
  }, {
    141: 141,
    142: 142,
    33: 33,
    35: 35,
    45: 45,
    62: 62,
    67: 67
  }],
  293: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(41)(!0);
    e(e.P, "Array", {
      includes: function includes(t) {
        return i(this, t, 1 < arguments.length ? arguments[1] : void 0);
      }
    }), t(35)("includes");
  }, {
    35: 35,
    41: 41,
    62: 62
  }],
  294: [function (t, n, r) {
    var e = t(62),
        i = t(110)(!0);
    e(e.S, "Object", {
      entries: function entries(t) {
        return i(t);
      }
    });
  }, {
    110: 110,
    62: 62
  }],
  295: [function (t, n, r) {
    var e = t(62),
        a = t(111),
        f = t(140),
        s = t(101),
        l = t(53);
    e(e.S, "Object", {
      getOwnPropertyDescriptors: function getOwnPropertyDescriptors(t) {
        for (var n, r, e = f(t), i = s.f, o = a(e), u = {}, c = 0; o.length > c;) void 0 !== (r = i(e, n = o[c++])) && l(u, n, r);

        return u;
      }
    });
  }, {
    101: 101,
    111: 111,
    140: 140,
    53: 53,
    62: 62
  }],
  296: [function (t, n, r) {
    var e = t(62),
        i = t(110)(!1);
    e(e.S, "Object", {
      values: function values(t) {
        return i(t);
      }
    });
  }, {
    110: 110,
    62: 62
  }],
  297: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(52),
        o = t(70),
        u = t(127),
        c = t(115);
    e(e.P + e.R, "Promise", {
      finally: function (n) {
        var r = u(this, i.Promise || o.Promise),
            t = "function" == typeof n;
        return this.then(t ? function (t) {
          return c(r, n()).then(function () {
            return t;
          });
        } : n, t ? function (t) {
          return c(r, n()).then(function () {
            throw t;
          });
        } : n);
      }
    });
  }, {
    115: 115,
    127: 127,
    52: 52,
    62: 62,
    70: 70
  }],
  298: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(132),
        o = t(148),
        u = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    e(e.P + e.F * u, "String", {
      padEnd: function padEnd(t) {
        return i(this, t, 1 < arguments.length ? arguments[1] : void 0, !1);
      }
    });
  }, {
    132: 132,
    148: 148,
    62: 62
  }],
  299: [function (t, n, r) {
    "use strict";

    var e = t(62),
        i = t(132),
        o = t(148),
        u = /Version\/10\.\d+(\.\d+)?( Mobile\/\w+)? Safari\//.test(o);
    e(e.P + e.F * u, "String", {
      padStart: function padStart(t) {
        return i(this, t, 1 < arguments.length ? arguments[1] : void 0, !0);
      }
    });
  }, {
    132: 132,
    148: 148,
    62: 62
  }],
  300: [function (t, n, r) {
    "use strict";

    t(134)("trimLeft", function (t) {
      return function trimLeft() {
        return t(this, 1);
      };
    }, "trimStart");
  }, {
    134: 134
  }],
  301: [function (t, n, r) {
    "use strict";

    t(134)("trimRight", function (t) {
      return function trimRight() {
        return t(this, 2);
      };
    }, "trimEnd");
  }, {
    134: 134
  }],
  302: [function (t, n, r) {
    t(150)("asyncIterator");
  }, {
    150: 150
  }],
  303: [function (t, n, r) {
    for (var e = t(164), i = t(107), o = t(118), u = t(70), c = t(72), a = t(88), f = t(152), s = f("iterator"), l = f("toStringTag"), h = a.Array, p = {
      CSSRuleList: !0,
      CSSStyleDeclaration: !1,
      CSSValueList: !1,
      ClientRectList: !1,
      DOMRectList: !1,
      DOMStringList: !1,
      DOMTokenList: !0,
      DataTransferItemList: !1,
      FileList: !1,
      HTMLAllCollection: !1,
      HTMLCollection: !1,
      HTMLFormElement: !1,
      HTMLSelectElement: !1,
      MediaList: !0,
      MimeTypeArray: !1,
      NamedNodeMap: !1,
      NodeList: !0,
      PaintRequestList: !1,
      Plugin: !1,
      PluginArray: !1,
      SVGLengthList: !1,
      SVGNumberList: !1,
      SVGPathSegList: !1,
      SVGPointList: !1,
      SVGStringList: !1,
      SVGTransformList: !1,
      SourceBufferList: !1,
      StyleSheetList: !0,
      TextTrackCueList: !1,
      TextTrackList: !1,
      TouchList: !1
    }, v = i(p), g = 0; g < v.length; g++) {
      var y,
          d = v[g],
          x = p[d],
          m = u[d],
          S = m && m.prototype;
      if (S && (S[s] || c(S, s, h), S[l] || c(S, l, d), a[d] = h, x)) for (y in e) S[y] || o(S, y, e[y], !0);
    }
  }, {
    107: 107,
    118: 118,
    152: 152,
    164: 164,
    70: 70,
    72: 72,
    88: 88
  }],
  304: [function (t, n, r) {
    var e = t(62),
        i = t(136);
    e(e.G + e.B, {
      setImmediate: i.set,
      clearImmediate: i.clear
    });
  }, {
    136: 136,
    62: 62
  }],
  305: [function (t, n, r) {
    function y7(i) {
      return function (t, n) {
        var r = 2 < arguments.length,
            e = r && u.call(arguments, 2);
        return i(r ? function () {
          ("function" == typeof t ? t : Function(t)).apply(this, e);
        } : t, n);
      };
    }

    var e = t(70),
        i = t(62),
        o = t(148),
        u = [].slice,
        c = /MSIE .\./.test(o);
    i(i.G + i.B + i.F * c, {
      setTimeout: y7(e.setTimeout),
      setInterval: y7(e.setInterval)
    });
  }, {
    148: 148,
    62: 62,
    70: 70
  }],
  306: [function (t, n, r) {
    t(305), t(304), t(303), n.exports = t(52);
  }, {
    303: 303,
    304: 304,
    305: 305,
    52: 52
  }],
  307: [function (t, n, r) {
    var e = function (u) {
      "use strict";

      var c,
          t = Object.prototype,
          f = t.hasOwnProperty,
          n = "function" == typeof Symbol ? Symbol : {},
          i = n.iterator || "@@iterator",
          r = n.asyncIterator || "@@asyncIterator",
          e = n.toStringTag || "@@toStringTag";

      function wrap(t, n, r, e) {
        var i = n && n.prototype instanceof Generator ? n : Generator,
            o = Object.create(i.prototype),
            u = new Context(e || []);
        return o._invoke = function makeInvokeMethod(o, u, c) {
          var a = s;
          return function invoke(t, n) {
            if (a === h) throw new Error("Generator is already running");

            if (a === p) {
              if ("throw" === t) throw n;
              return doneResult();
            }

            for (c.method = t, c.arg = n;;) {
              var r = c.delegate;

              if (r) {
                var e = maybeInvokeDelegate(r, c);

                if (e) {
                  if (e === v) continue;
                  return e;
                }
              }

              if ("next" === c.method) c.sent = c._sent = c.arg;else if ("throw" === c.method) {
                if (a === s) throw a = p, c.arg;
                c.dispatchException(c.arg);
              } else "return" === c.method && c.abrupt("return", c.arg);
              a = h;
              var i = tryCatch(o, u, c);

              if ("normal" === i.type) {
                if (a = c.done ? p : l, i.arg === v) continue;
                return {
                  value: i.arg,
                  done: c.done
                };
              }

              "throw" === i.type && (a = p, c.method = "throw", c.arg = i.arg);
            }
          };
        }(t, r, u), o;
      }

      function tryCatch(t, n, r) {
        try {
          return {
            type: "normal",
            arg: t.call(n, r)
          };
        } catch (t) {
          return {
            type: "throw",
            arg: t
          };
        }
      }

      u.wrap = wrap;
      var s = "suspendedStart",
          l = "suspendedYield",
          h = "executing",
          p = "completed",
          v = {};

      function Generator() {}

      function GeneratorFunction() {}

      function GeneratorFunctionPrototype() {}

      var o = {};

      o[i] = function () {
        return this;
      };

      var a = Object.getPrototypeOf,
          g = a && a(a(values([])));
      g && g !== t && f.call(g, i) && (o = g);
      var y = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(o);

      function defineIteratorMethods(t) {
        ["next", "throw", "return"].forEach(function (n) {
          t[n] = function (t) {
            return this._invoke(n, t);
          };
        });
      }

      function AsyncIterator(c, a) {
        var t;

        this._invoke = function enqueue(r, e) {
          function callInvokeWithMethodAndArg() {
            return new a(function (t, n) {
              !function invoke(t, n, r, e) {
                var i = tryCatch(c[t], c, n);

                if ("throw" !== i.type) {
                  var o = i.arg,
                      u = o.value;
                  return u && "object" == typeof u && f.call(u, "__await") ? a.resolve(u.__await).then(function (t) {
                    invoke("next", t, r, e);
                  }, function (t) {
                    invoke("throw", t, r, e);
                  }) : a.resolve(u).then(function (t) {
                    o.value = t, r(o);
                  }, function (t) {
                    return invoke("throw", t, r, e);
                  });
                }

                e(i.arg);
              }(r, e, t, n);
            });
          }

          return t = t ? t.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
        };
      }

      function maybeInvokeDelegate(t, n) {
        var r = t.iterator[n.method];

        if (r === c) {
          if (n.delegate = null, "throw" === n.method) {
            if (t.iterator.return && (n.method = "return", n.arg = c, maybeInvokeDelegate(t, n), "throw" === n.method)) return v;
            n.method = "throw", n.arg = new TypeError("The iterator does not provide a 'throw' method");
          }

          return v;
        }

        var e = tryCatch(r, t.iterator, n.arg);
        if ("throw" === e.type) return n.method = "throw", n.arg = e.arg, n.delegate = null, v;
        var i = e.arg;
        return i ? i.done ? (n[t.resultName] = i.value, n.next = t.nextLoc, "return" !== n.method && (n.method = "next", n.arg = c), n.delegate = null, v) : i : (n.method = "throw", n.arg = new TypeError("iterator result is not an object"), n.delegate = null, v);
      }

      function pushTryEntry(t) {
        var n = {
          tryLoc: t[0]
        };
        1 in t && (n.catchLoc = t[1]), 2 in t && (n.finallyLoc = t[2], n.afterLoc = t[3]), this.tryEntries.push(n);
      }

      function resetTryEntry(t) {
        var n = t.completion || {};
        n.type = "normal", delete n.arg, t.completion = n;
      }

      function Context(t) {
        this.tryEntries = [{
          tryLoc: "root"
        }], t.forEach(pushTryEntry, this), this.reset(!0);
      }

      function values(t) {
        if (t) {
          var n = t[i];
          if (n) return n.call(t);
          if ("function" == typeof t.next) return t;

          if (!isNaN(t.length)) {
            var r = -1,
                e = function next() {
              for (; ++r < t.length;) if (f.call(t, r)) return next.value = t[r], next.done = !1, next;

              return next.value = c, next.done = !0, next;
            };

            return e.next = e;
          }
        }

        return {
          next: doneResult
        };
      }

      function doneResult() {
        return {
          value: c,
          done: !0
        };
      }

      return GeneratorFunction.prototype = y.constructor = GeneratorFunctionPrototype, GeneratorFunctionPrototype.constructor = GeneratorFunction, GeneratorFunctionPrototype[e] = GeneratorFunction.displayName = "GeneratorFunction", u.isGeneratorFunction = function (t) {
        var n = "function" == typeof t && t.constructor;
        return !!n && (n === GeneratorFunction || "GeneratorFunction" === (n.displayName || n.name));
      }, u.mark = function (t) {
        return Object.setPrototypeOf ? Object.setPrototypeOf(t, GeneratorFunctionPrototype) : (t.__proto__ = GeneratorFunctionPrototype, e in t || (t[e] = "GeneratorFunction")), t.prototype = Object.create(y), t;
      }, u.awrap = function (t) {
        return {
          __await: t
        };
      }, defineIteratorMethods(AsyncIterator.prototype), AsyncIterator.prototype[r] = function () {
        return this;
      }, u.AsyncIterator = AsyncIterator, u.async = function (t, n, r, e, i) {
        void 0 === i && (i = Promise);
        var o = new AsyncIterator(wrap(t, n, r, e), i);
        return u.isGeneratorFunction(n) ? o : o.next().then(function (t) {
          return t.done ? t.value : o.next();
        });
      }, defineIteratorMethods(y), y[e] = "Generator", y[i] = function () {
        return this;
      }, y.toString = function () {
        return "[object Generator]";
      }, u.keys = function (n) {
        var r = [];

        for (var t in n) r.push(t);

        return r.reverse(), function next() {
          for (; r.length;) {
            var t = r.pop();
            if (t in n) return next.value = t, next.done = !1, next;
          }

          return next.done = !0, next;
        };
      }, u.values = values, Context.prototype = {
        constructor: Context,
        reset: function (t) {
          if (this.prev = 0, this.next = 0, this.sent = this._sent = c, this.done = !1, this.delegate = null, this.method = "next", this.arg = c, this.tryEntries.forEach(resetTryEntry), !t) for (var n in this) "t" === n.charAt(0) && f.call(this, n) && !isNaN(+n.slice(1)) && (this[n] = c);
        },
        stop: function () {
          this.done = !0;
          var t = this.tryEntries[0].completion;
          if ("throw" === t.type) throw t.arg;
          return this.rval;
        },
        dispatchException: function (r) {
          if (this.done) throw r;
          var e = this;

          function handle(t, n) {
            return i.type = "throw", i.arg = r, e.next = t, n && (e.method = "next", e.arg = c), !!n;
          }

          for (var t = this.tryEntries.length - 1; 0 <= t; --t) {
            var n = this.tryEntries[t],
                i = n.completion;
            if ("root" === n.tryLoc) return handle("end");

            if (n.tryLoc <= this.prev) {
              var o = f.call(n, "catchLoc"),
                  u = f.call(n, "finallyLoc");

              if (o && u) {
                if (this.prev < n.catchLoc) return handle(n.catchLoc, !0);
                if (this.prev < n.finallyLoc) return handle(n.finallyLoc);
              } else if (o) {
                if (this.prev < n.catchLoc) return handle(n.catchLoc, !0);
              } else {
                if (!u) throw new Error("try statement without catch or finally");
                if (this.prev < n.finallyLoc) return handle(n.finallyLoc);
              }
            }
          }
        },
        abrupt: function (t, n) {
          for (var r = this.tryEntries.length - 1; 0 <= r; --r) {
            var e = this.tryEntries[r];

            if (e.tryLoc <= this.prev && f.call(e, "finallyLoc") && this.prev < e.finallyLoc) {
              var i = e;
              break;
            }
          }

          i && ("break" === t || "continue" === t) && i.tryLoc <= n && n <= i.finallyLoc && (i = null);
          var o = i ? i.completion : {};
          return o.type = t, o.arg = n, i ? (this.method = "next", this.next = i.finallyLoc, v) : this.complete(o);
        },
        complete: function (t, n) {
          if ("throw" === t.type) throw t.arg;
          return "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = this.arg = t.arg, this.method = "return", this.next = "end") : "normal" === t.type && n && (this.next = n), v;
        },
        finish: function (t) {
          for (var n = this.tryEntries.length - 1; 0 <= n; --n) {
            var r = this.tryEntries[n];
            if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), resetTryEntry(r), v;
          }
        },
        catch: function (t) {
          for (var n = this.tryEntries.length - 1; 0 <= n; --n) {
            var r = this.tryEntries[n];

            if (r.tryLoc === t) {
              var e = r.completion;

              if ("throw" === e.type) {
                var i = e.arg;
                resetTryEntry(r);
              }

              return i;
            }
          }

          throw new Error("illegal catch attempt");
        },
        delegateYield: function (t, n, r) {
          return this.delegate = {
            iterator: values(t),
            resultName: n,
            nextLoc: r
          }, "next" === this.method && (this.arg = c), v;
        }
      }, u;
    }("object" == typeof n ? n.exports : {});

    try {
      regeneratorRuntime = e;
    } catch (t) {
      Function("r", "regeneratorRuntime = r")(e);
    }
  }, {}]
}, {}, [1]);
/*! jQuery UI - v1.12.1 - 2016-09-14
* http://jqueryui.com
* Includes: widget.js, position.js, data.js, disable-selection.js, effect.js, effects/effect-blind.js, effects/effect-bounce.js, effects/effect-clip.js, effects/effect-drop.js, effects/effect-explode.js, effects/effect-fade.js, effects/effect-fold.js, effects/effect-highlight.js, effects/effect-puff.js, effects/effect-pulsate.js, effects/effect-scale.js, effects/effect-shake.js, effects/effect-size.js, effects/effect-slide.js, effects/effect-transfer.js, focusable.js, form-reset-mixin.js, jquery-1-7.js, keycode.js, labels.js, scroll-parent.js, tabbable.js, unique-id.js, widgets/accordion.js, widgets/autocomplete.js, widgets/button.js, widgets/checkboxradio.js, widgets/controlgroup.js, widgets/datepicker.js, widgets/dialog.js, widgets/draggable.js, widgets/droppable.js, widgets/menu.js, widgets/mouse.js, widgets/progressbar.js, widgets/resizable.js, widgets/selectable.js, widgets/selectmenu.js, widgets/slider.js, widgets/sortable.js, widgets/spinner.js, widgets/tabs.js, widgets/tooltip.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */
(function (t) {
  "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery);
})(function (t) {
  function e(t) {
    for (var e = t.css("visibility"); "inherit" === e;) t = t.parent(), e = t.css("visibility");

    return "hidden" !== e;
  }

  function i(t) {
    for (var e, i; t.length && t[0] !== document;) {
      if (e = t.css("position"), ("absolute" === e || "relative" === e || "fixed" === e) && (i = parseInt(t.css("zIndex"), 10), !isNaN(i) && 0 !== i)) return i;
      t = t.parent();
    }

    return 0;
  }

  function s() {
    this._curInst = null, this._keyEvent = !1, this._disabledInputs = [], this._datepickerShowing = !1, this._inDialog = !1, this._mainDivId = "ui-datepicker-div", this._inlineClass = "ui-datepicker-inline", this._appendClass = "ui-datepicker-append", this._triggerClass = "ui-datepicker-trigger", this._dialogClass = "ui-datepicker-dialog", this._disableClass = "ui-datepicker-disabled", this._unselectableClass = "ui-datepicker-unselectable", this._currentClass = "ui-datepicker-current-day", this._dayOverClass = "ui-datepicker-days-cell-over", this.regional = [], this.regional[""] = {
      closeText: "Done",
      prevText: "Prev",
      nextText: "Next",
      currentText: "Today",
      monthNames: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      dayNames: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      dayNamesShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
      dayNamesMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
      weekHeader: "Wk",
      dateFormat: "mm/dd/yy",
      firstDay: 0,
      isRTL: !1,
      showMonthAfterYear: !1,
      yearSuffix: ""
    }, this._defaults = {
      showOn: "focus",
      showAnim: "fadeIn",
      showOptions: {},
      defaultDate: null,
      appendText: "",
      buttonText: "...",
      buttonImage: "",
      buttonImageOnly: !1,
      hideIfNoPrevNext: !1,
      navigationAsDateFormat: !1,
      gotoCurrent: !1,
      changeMonth: !1,
      changeYear: !1,
      yearRange: "c-10:c+10",
      showOtherMonths: !1,
      selectOtherMonths: !1,
      showWeek: !1,
      calculateWeek: this.iso8601Week,
      shortYearCutoff: "+10",
      minDate: null,
      maxDate: null,
      duration: "fast",
      beforeShowDay: null,
      beforeShow: null,
      onSelect: null,
      onChangeMonthYear: null,
      onClose: null,
      numberOfMonths: 1,
      showCurrentAtPos: 0,
      stepMonths: 1,
      stepBigMonths: 12,
      altField: "",
      altFormat: "",
      constrainInput: !0,
      showButtonPanel: !1,
      autoSize: !1,
      disabled: !1
    }, t.extend(this._defaults, this.regional[""]), this.regional.en = t.extend(!0, {}, this.regional[""]), this.regional["en-US"] = t.extend(!0, {}, this.regional.en), this.dpDiv = n(t("<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"));
  }

  function n(e) {
    var i = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
    return e.on("mouseout", i, function () {
      t(this).removeClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).removeClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).removeClass("ui-datepicker-next-hover");
    }).on("mouseover", i, o);
  }

  function o() {
    t.datepicker._isDisabledDatepicker(m.inline ? m.dpDiv.parent()[0] : m.input[0]) || (t(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"), t(this).addClass("ui-state-hover"), -1 !== this.className.indexOf("ui-datepicker-prev") && t(this).addClass("ui-datepicker-prev-hover"), -1 !== this.className.indexOf("ui-datepicker-next") && t(this).addClass("ui-datepicker-next-hover"));
  }

  function a(e, i) {
    t.extend(e, i);

    for (var s in i) null == i[s] && (e[s] = i[s]);

    return e;
  }

  function r(t) {
    return function () {
      var e = this.element.val();
      t.apply(this, arguments), this._refresh(), e !== this.element.val() && this._trigger("change");
    };
  }

  t.ui = t.ui || {}, t.ui.version = "1.12.1";
  var h = 0,
      l = Array.prototype.slice;
  t.cleanData = function (e) {
    return function (i) {
      var s, n, o;

      for (o = 0; null != (n = i[o]); o++) try {
        s = t._data(n, "events"), s && s.remove && t(n).triggerHandler("remove");
      } catch (a) {}

      e(i);
    };
  }(t.cleanData), t.widget = function (e, i, s) {
    var n,
        o,
        a,
        r = {},
        h = e.split(".")[0];
    e = e.split(".")[1];
    var l = h + "-" + e;
    return s || (s = i, i = t.Widget), t.isArray(s) && (s = t.extend.apply(null, [{}].concat(s))), t.expr[":"][l.toLowerCase()] = function (e) {
      return !!t.data(e, l);
    }, t[h] = t[h] || {}, n = t[h][e], o = t[h][e] = function (t, e) {
      return this._createWidget ? (arguments.length && this._createWidget(t, e), void 0) : new o(t, e);
    }, t.extend(o, n, {
      version: s.version,
      _proto: t.extend({}, s),
      _childConstructors: []
    }), a = new i(), a.options = t.widget.extend({}, a.options), t.each(s, function (e, s) {
      return t.isFunction(s) ? (r[e] = function () {
        function t() {
          return i.prototype[e].apply(this, arguments);
        }

        function n(t) {
          return i.prototype[e].apply(this, t);
        }

        return function () {
          var e,
              i = this._super,
              o = this._superApply;
          return this._super = t, this._superApply = n, e = s.apply(this, arguments), this._super = i, this._superApply = o, e;
        };
      }(), void 0) : (r[e] = s, void 0);
    }), o.prototype = t.widget.extend(a, {
      widgetEventPrefix: n ? a.widgetEventPrefix || e : e
    }, r, {
      constructor: o,
      namespace: h,
      widgetName: e,
      widgetFullName: l
    }), n ? (t.each(n._childConstructors, function (e, i) {
      var s = i.prototype;
      t.widget(s.namespace + "." + s.widgetName, o, i._proto);
    }), delete n._childConstructors) : i._childConstructors.push(o), t.widget.bridge(e, o), o;
  }, t.widget.extend = function (e) {
    for (var i, s, n = l.call(arguments, 1), o = 0, a = n.length; a > o; o++) for (i in n[o]) s = n[o][i], n[o].hasOwnProperty(i) && void 0 !== s && (e[i] = t.isPlainObject(s) ? t.isPlainObject(e[i]) ? t.widget.extend({}, e[i], s) : t.widget.extend({}, s) : s);

    return e;
  }, t.widget.bridge = function (e, i) {
    var s = i.prototype.widgetFullName || e;

    t.fn[e] = function (n) {
      var o = "string" == typeof n,
          a = l.call(arguments, 1),
          r = this;
      return o ? this.length || "instance" !== n ? this.each(function () {
        var i,
            o = t.data(this, s);
        return "instance" === n ? (r = o, !1) : o ? t.isFunction(o[n]) && "_" !== n.charAt(0) ? (i = o[n].apply(o, a), i !== o && void 0 !== i ? (r = i && i.jquery ? r.pushStack(i.get()) : i, !1) : void 0) : t.error("no such method '" + n + "' for " + e + " widget instance") : t.error("cannot call methods on " + e + " prior to initialization; " + "attempted to call method '" + n + "'");
      }) : r = void 0 : (a.length && (n = t.widget.extend.apply(null, [n].concat(a))), this.each(function () {
        var e = t.data(this, s);
        e ? (e.option(n || {}), e._init && e._init()) : t.data(this, s, new i(n, this));
      })), r;
    };
  }, t.Widget = function () {}, t.Widget._childConstructors = [], t.Widget.prototype = {
    widgetName: "widget",
    widgetEventPrefix: "",
    defaultElement: "<div>",
    options: {
      classes: {},
      disabled: !1,
      create: null
    },
    _createWidget: function (e, i) {
      i = t(i || this.defaultElement || this)[0], this.element = t(i), this.uuid = h++, this.eventNamespace = "." + this.widgetName + this.uuid, this.bindings = t(), this.hoverable = t(), this.focusable = t(), this.classesElementLookup = {}, i !== this && (t.data(i, this.widgetFullName, this), this._on(!0, this.element, {
        remove: function (t) {
          t.target === i && this.destroy();
        }
      }), this.document = t(i.style ? i.ownerDocument : i.document || i), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this._create(), this.options.disabled && this._setOptionDisabled(this.options.disabled), this._trigger("create", null, this._getCreateEventData()), this._init();
    },
    _getCreateOptions: function () {
      return {};
    },
    _getCreateEventData: t.noop,
    _create: t.noop,
    _init: t.noop,
    destroy: function () {
      var e = this;
      this._destroy(), t.each(this.classesElementLookup, function (t, i) {
        e._removeClass(i, t);
      }), this.element.off(this.eventNamespace).removeData(this.widgetFullName), this.widget().off(this.eventNamespace).removeAttr("aria-disabled"), this.bindings.off(this.eventNamespace);
    },
    _destroy: t.noop,
    widget: function () {
      return this.element;
    },
    option: function (e, i) {
      var s,
          n,
          o,
          a = e;
      if (0 === arguments.length) return t.widget.extend({}, this.options);
      if ("string" == typeof e) if (a = {}, s = e.split("."), e = s.shift(), s.length) {
        for (n = a[e] = t.widget.extend({}, this.options[e]), o = 0; s.length - 1 > o; o++) n[s[o]] = n[s[o]] || {}, n = n[s[o]];

        if (e = s.pop(), 1 === arguments.length) return void 0 === n[e] ? null : n[e];
        n[e] = i;
      } else {
        if (1 === arguments.length) return void 0 === this.options[e] ? null : this.options[e];
        a[e] = i;
      }
      return this._setOptions(a), this;
    },
    _setOptions: function (t) {
      var e;

      for (e in t) this._setOption(e, t[e]);

      return this;
    },
    _setOption: function (t, e) {
      return "classes" === t && this._setOptionClasses(e), this.options[t] = e, "disabled" === t && this._setOptionDisabled(e), this;
    },
    _setOptionClasses: function (e) {
      var i, s, n;

      for (i in e) n = this.classesElementLookup[i], e[i] !== this.options.classes[i] && n && n.length && (s = t(n.get()), this._removeClass(n, i), s.addClass(this._classes({
        element: s,
        keys: i,
        classes: e,
        add: !0
      })));
    },
    _setOptionDisabled: function (t) {
      this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, !!t), t && (this._removeClass(this.hoverable, null, "ui-state-hover"), this._removeClass(this.focusable, null, "ui-state-focus"));
    },
    enable: function () {
      return this._setOptions({
        disabled: !1
      });
    },
    disable: function () {
      return this._setOptions({
        disabled: !0
      });
    },
    _classes: function (e) {
      function i(i, o) {
        var a, r;

        for (r = 0; i.length > r; r++) a = n.classesElementLookup[i[r]] || t(), a = e.add ? t(t.unique(a.get().concat(e.element.get()))) : t(a.not(e.element).get()), n.classesElementLookup[i[r]] = a, s.push(i[r]), o && e.classes[i[r]] && s.push(e.classes[i[r]]);
      }

      var s = [],
          n = this;
      return e = t.extend({
        element: this.element,
        classes: this.options.classes || {}
      }, e), this._on(e.element, {
        remove: "_untrackClassesElement"
      }), e.keys && i(e.keys.match(/\S+/g) || [], !0), e.extra && i(e.extra.match(/\S+/g) || []), s.join(" ");
    },
    _untrackClassesElement: function (e) {
      var i = this;
      t.each(i.classesElementLookup, function (s, n) {
        -1 !== t.inArray(e.target, n) && (i.classesElementLookup[s] = t(n.not(e.target).get()));
      });
    },
    _removeClass: function (t, e, i) {
      return this._toggleClass(t, e, i, !1);
    },
    _addClass: function (t, e, i) {
      return this._toggleClass(t, e, i, !0);
    },
    _toggleClass: function (t, e, i, s) {
      s = "boolean" == typeof s ? s : i;
      var n = "string" == typeof t || null === t,
          o = {
        extra: n ? e : i,
        keys: n ? t : e,
        element: n ? this.element : t,
        add: s
      };
      return o.element.toggleClass(this._classes(o), s), this;
    },
    _on: function (e, i, s) {
      var n,
          o = this;
      "boolean" != typeof e && (s = i, i = e, e = !1), s ? (i = n = t(i), this.bindings = this.bindings.add(i)) : (s = i, i = this.element, n = this.widget()), t.each(s, function (s, a) {
        function r() {
          return e || o.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? o[a] : a).apply(o, arguments) : void 0;
        }

        "string" != typeof a && (r.guid = a.guid = a.guid || r.guid || t.guid++);
        var h = s.match(/^([\w:-]*)\s*(.*)$/),
            l = h[1] + o.eventNamespace,
            c = h[2];
        c ? n.on(l, c, r) : i.on(l, r);
      });
    },
    _off: function (e, i) {
      i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, e.off(i).off(i), this.bindings = t(this.bindings.not(e).get()), this.focusable = t(this.focusable.not(e).get()), this.hoverable = t(this.hoverable.not(e).get());
    },
    _delay: function (t, e) {
      function i() {
        return ("string" == typeof t ? s[t] : t).apply(s, arguments);
      }

      var s = this;
      return setTimeout(i, e || 0);
    },
    _hoverable: function (e) {
      this.hoverable = this.hoverable.add(e), this._on(e, {
        mouseenter: function (e) {
          this._addClass(t(e.currentTarget), null, "ui-state-hover");
        },
        mouseleave: function (e) {
          this._removeClass(t(e.currentTarget), null, "ui-state-hover");
        }
      });
    },
    _focusable: function (e) {
      this.focusable = this.focusable.add(e), this._on(e, {
        focusin: function (e) {
          this._addClass(t(e.currentTarget), null, "ui-state-focus");
        },
        focusout: function (e) {
          this._removeClass(t(e.currentTarget), null, "ui-state-focus");
        }
      });
    },
    _trigger: function (e, i, s) {
      var n,
          o,
          a = this.options[e];
      if (s = s || {}, i = t.Event(i), i.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), i.target = this.element[0], o = i.originalEvent) for (n in o) n in i || (i[n] = o[n]);
      return this.element.trigger(i, s), !(t.isFunction(a) && a.apply(this.element[0], [i].concat(s)) === !1 || i.isDefaultPrevented());
    }
  }, t.each({
    show: "fadeIn",
    hide: "fadeOut"
  }, function (e, i) {
    t.Widget.prototype["_" + e] = function (s, n, o) {
      "string" == typeof n && (n = {
        effect: n
      });
      var a,
          r = n ? n === !0 || "number" == typeof n ? i : n.effect || i : e;
      n = n || {}, "number" == typeof n && (n = {
        duration: n
      }), a = !t.isEmptyObject(n), n.complete = o, n.delay && s.delay(n.delay), a && t.effects && t.effects.effect[r] ? s[e](n) : r !== e && s[r] ? s[r](n.duration, n.easing, o) : s.queue(function (i) {
        t(this)[e](), o && o.call(s[0]), i();
      });
    };
  }), t.widget, function () {
    function e(t, e, i) {
      return [parseFloat(t[0]) * (u.test(t[0]) ? e / 100 : 1), parseFloat(t[1]) * (u.test(t[1]) ? i / 100 : 1)];
    }

    function i(e, i) {
      return parseInt(t.css(e, i), 10) || 0;
    }

    function s(e) {
      var i = e[0];
      return 9 === i.nodeType ? {
        width: e.width(),
        height: e.height(),
        offset: {
          top: 0,
          left: 0
        }
      } : t.isWindow(i) ? {
        width: e.width(),
        height: e.height(),
        offset: {
          top: e.scrollTop(),
          left: e.scrollLeft()
        }
      } : i.preventDefault ? {
        width: 0,
        height: 0,
        offset: {
          top: i.pageY,
          left: i.pageX
        }
      } : {
        width: e.outerWidth(),
        height: e.outerHeight(),
        offset: e.offset()
      };
    }

    var n,
        o = Math.max,
        a = Math.abs,
        r = /left|center|right/,
        h = /top|center|bottom/,
        l = /[\+\-]\d+(\.[\d]+)?%?/,
        c = /^\w+/,
        u = /%$/,
        d = t.fn.position;
    t.position = {
      scrollbarWidth: function () {
        if (void 0 !== n) return n;
        var e,
            i,
            s = t("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),
            o = s.children()[0];
        return t("body").append(s), e = o.offsetWidth, s.css("overflow", "scroll"), i = o.offsetWidth, e === i && (i = s[0].clientWidth), s.remove(), n = e - i;
      },
      getScrollInfo: function (e) {
        var i = e.isWindow || e.isDocument ? "" : e.element.css("overflow-x"),
            s = e.isWindow || e.isDocument ? "" : e.element.css("overflow-y"),
            n = "scroll" === i || "auto" === i && e.width < e.element[0].scrollWidth,
            o = "scroll" === s || "auto" === s && e.height < e.element[0].scrollHeight;
        return {
          width: o ? t.position.scrollbarWidth() : 0,
          height: n ? t.position.scrollbarWidth() : 0
        };
      },
      getWithinInfo: function (e) {
        var i = t(e || window),
            s = t.isWindow(i[0]),
            n = !!i[0] && 9 === i[0].nodeType,
            o = !s && !n;
        return {
          element: i,
          isWindow: s,
          isDocument: n,
          offset: o ? t(e).offset() : {
            left: 0,
            top: 0
          },
          scrollLeft: i.scrollLeft(),
          scrollTop: i.scrollTop(),
          width: i.outerWidth(),
          height: i.outerHeight()
        };
      }
    }, t.fn.position = function (n) {
      if (!n || !n.of) return d.apply(this, arguments);
      n = t.extend({}, n);

      var u,
          p,
          f,
          g,
          m,
          _,
          v = t(n.of),
          b = t.position.getWithinInfo(n.within),
          y = t.position.getScrollInfo(b),
          w = (n.collision || "flip").split(" "),
          k = {};

      return _ = s(v), v[0].preventDefault && (n.at = "left top"), p = _.width, f = _.height, g = _.offset, m = t.extend({}, g), t.each(["my", "at"], function () {
        var t,
            e,
            i = (n[this] || "").split(" ");
        1 === i.length && (i = r.test(i[0]) ? i.concat(["center"]) : h.test(i[0]) ? ["center"].concat(i) : ["center", "center"]), i[0] = r.test(i[0]) ? i[0] : "center", i[1] = h.test(i[1]) ? i[1] : "center", t = l.exec(i[0]), e = l.exec(i[1]), k[this] = [t ? t[0] : 0, e ? e[0] : 0], n[this] = [c.exec(i[0])[0], c.exec(i[1])[0]];
      }), 1 === w.length && (w[1] = w[0]), "right" === n.at[0] ? m.left += p : "center" === n.at[0] && (m.left += p / 2), "bottom" === n.at[1] ? m.top += f : "center" === n.at[1] && (m.top += f / 2), u = e(k.at, p, f), m.left += u[0], m.top += u[1], this.each(function () {
        var s,
            r,
            h = t(this),
            l = h.outerWidth(),
            c = h.outerHeight(),
            d = i(this, "marginLeft"),
            _ = i(this, "marginTop"),
            x = l + d + i(this, "marginRight") + y.width,
            C = c + _ + i(this, "marginBottom") + y.height,
            D = t.extend({}, m),
            I = e(k.my, h.outerWidth(), h.outerHeight());

        "right" === n.my[0] ? D.left -= l : "center" === n.my[0] && (D.left -= l / 2), "bottom" === n.my[1] ? D.top -= c : "center" === n.my[1] && (D.top -= c / 2), D.left += I[0], D.top += I[1], s = {
          marginLeft: d,
          marginTop: _
        }, t.each(["left", "top"], function (e, i) {
          t.ui.position[w[e]] && t.ui.position[w[e]][i](D, {
            targetWidth: p,
            targetHeight: f,
            elemWidth: l,
            elemHeight: c,
            collisionPosition: s,
            collisionWidth: x,
            collisionHeight: C,
            offset: [u[0] + I[0], u[1] + I[1]],
            my: n.my,
            at: n.at,
            within: b,
            elem: h
          });
        }), n.using && (r = function (t) {
          var e = g.left - D.left,
              i = e + p - l,
              s = g.top - D.top,
              r = s + f - c,
              u = {
            target: {
              element: v,
              left: g.left,
              top: g.top,
              width: p,
              height: f
            },
            element: {
              element: h,
              left: D.left,
              top: D.top,
              width: l,
              height: c
            },
            horizontal: 0 > i ? "left" : e > 0 ? "right" : "center",
            vertical: 0 > r ? "top" : s > 0 ? "bottom" : "middle"
          };
          l > p && p > a(e + i) && (u.horizontal = "center"), c > f && f > a(s + r) && (u.vertical = "middle"), u.important = o(a(e), a(i)) > o(a(s), a(r)) ? "horizontal" : "vertical", n.using.call(this, t, u);
        }), h.offset(t.extend(D, {
          using: r
        }));
      });
    }, t.ui.position = {
      fit: {
        left: function (t, e) {
          var i,
              s = e.within,
              n = s.isWindow ? s.scrollLeft : s.offset.left,
              a = s.width,
              r = t.left - e.collisionPosition.marginLeft,
              h = n - r,
              l = r + e.collisionWidth - a - n;
          e.collisionWidth > a ? h > 0 && 0 >= l ? (i = t.left + h + e.collisionWidth - a - n, t.left += h - i) : t.left = l > 0 && 0 >= h ? n : h > l ? n + a - e.collisionWidth : n : h > 0 ? t.left += h : l > 0 ? t.left -= l : t.left = o(t.left - r, t.left);
        },
        top: function (t, e) {
          var i,
              s = e.within,
              n = s.isWindow ? s.scrollTop : s.offset.top,
              a = e.within.height,
              r = t.top - e.collisionPosition.marginTop,
              h = n - r,
              l = r + e.collisionHeight - a - n;
          e.collisionHeight > a ? h > 0 && 0 >= l ? (i = t.top + h + e.collisionHeight - a - n, t.top += h - i) : t.top = l > 0 && 0 >= h ? n : h > l ? n + a - e.collisionHeight : n : h > 0 ? t.top += h : l > 0 ? t.top -= l : t.top = o(t.top - r, t.top);
        }
      },
      flip: {
        left: function (t, e) {
          var i,
              s,
              n = e.within,
              o = n.offset.left + n.scrollLeft,
              r = n.width,
              h = n.isWindow ? n.scrollLeft : n.offset.left,
              l = t.left - e.collisionPosition.marginLeft,
              c = l - h,
              u = l + e.collisionWidth - r - h,
              d = "left" === e.my[0] ? -e.elemWidth : "right" === e.my[0] ? e.elemWidth : 0,
              p = "left" === e.at[0] ? e.targetWidth : "right" === e.at[0] ? -e.targetWidth : 0,
              f = -2 * e.offset[0];
          0 > c ? (i = t.left + d + p + f + e.collisionWidth - r - o, (0 > i || a(c) > i) && (t.left += d + p + f)) : u > 0 && (s = t.left - e.collisionPosition.marginLeft + d + p + f - h, (s > 0 || u > a(s)) && (t.left += d + p + f));
        },
        top: function (t, e) {
          var i,
              s,
              n = e.within,
              o = n.offset.top + n.scrollTop,
              r = n.height,
              h = n.isWindow ? n.scrollTop : n.offset.top,
              l = t.top - e.collisionPosition.marginTop,
              c = l - h,
              u = l + e.collisionHeight - r - h,
              d = "top" === e.my[1],
              p = d ? -e.elemHeight : "bottom" === e.my[1] ? e.elemHeight : 0,
              f = "top" === e.at[1] ? e.targetHeight : "bottom" === e.at[1] ? -e.targetHeight : 0,
              g = -2 * e.offset[1];
          0 > c ? (s = t.top + p + f + g + e.collisionHeight - r - o, (0 > s || a(c) > s) && (t.top += p + f + g)) : u > 0 && (i = t.top - e.collisionPosition.marginTop + p + f + g - h, (i > 0 || u > a(i)) && (t.top += p + f + g));
        }
      },
      flipfit: {
        left: function () {
          t.ui.position.flip.left.apply(this, arguments), t.ui.position.fit.left.apply(this, arguments);
        },
        top: function () {
          t.ui.position.flip.top.apply(this, arguments), t.ui.position.fit.top.apply(this, arguments);
        }
      }
    };
  }(), t.ui.position, t.extend(t.expr[":"], {
    data: t.expr.createPseudo ? t.expr.createPseudo(function (e) {
      return function (i) {
        return !!t.data(i, e);
      };
    }) : function (e, i, s) {
      return !!t.data(e, s[3]);
    }
  }), t.fn.extend({
    disableSelection: function () {
      var t = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
      return function () {
        return this.on(t + ".ui-disableSelection", function (t) {
          t.preventDefault();
        });
      };
    }(),
    enableSelection: function () {
      return this.off(".ui-disableSelection");
    }
  });
  var c = "ui-effects-",
      u = "ui-effects-style",
      d = "ui-effects-animated",
      p = t;
  t.effects = {
    effect: {}
  }, function (t, e) {
    function i(t, e, i) {
      var s = u[e.type] || {};
      return null == t ? i || !e.def ? null : e.def : (t = s.floor ? ~~t : parseFloat(t), isNaN(t) ? e.def : s.mod ? (t + s.mod) % s.mod : 0 > t ? 0 : t > s.max ? s.max : t);
    }

    function s(i) {
      var s = l(),
          n = s._rgba = [];
      return i = i.toLowerCase(), f(h, function (t, o) {
        var a,
            r = o.re.exec(i),
            h = r && o.parse(r),
            l = o.space || "rgba";
        return h ? (a = s[l](h), s[c[l].cache] = a[c[l].cache], n = s._rgba = a._rgba, !1) : e;
      }), n.length ? ("0,0,0,0" === n.join() && t.extend(n, o.transparent), s) : o[i];
    }

    function n(t, e, i) {
      return i = (i + 1) % 1, 1 > 6 * i ? t + 6 * (e - t) * i : 1 > 2 * i ? e : 2 > 3 * i ? t + 6 * (e - t) * (2 / 3 - i) : t;
    }

    var o,
        a = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",
        r = /^([\-+])=\s*(\d+\.?\d*)/,
        h = [{
      re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      parse: function (t) {
        return [t[1], t[2], t[3], t[4]];
      }
    }, {
      re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      parse: function (t) {
        return [2.55 * t[1], 2.55 * t[2], 2.55 * t[3], t[4]];
      }
    }, {
      re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
      parse: function (t) {
        return [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)];
      }
    }, {
      re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
      parse: function (t) {
        return [parseInt(t[1] + t[1], 16), parseInt(t[2] + t[2], 16), parseInt(t[3] + t[3], 16)];
      }
    }, {
      re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
      space: "hsla",
      parse: function (t) {
        return [t[1], t[2] / 100, t[3] / 100, t[4]];
      }
    }],
        l = t.Color = function (e, i, s, n) {
      return new t.Color.fn.parse(e, i, s, n);
    },
        c = {
      rgba: {
        props: {
          red: {
            idx: 0,
            type: "byte"
          },
          green: {
            idx: 1,
            type: "byte"
          },
          blue: {
            idx: 2,
            type: "byte"
          }
        }
      },
      hsla: {
        props: {
          hue: {
            idx: 0,
            type: "degrees"
          },
          saturation: {
            idx: 1,
            type: "percent"
          },
          lightness: {
            idx: 2,
            type: "percent"
          }
        }
      }
    },
        u = {
      "byte": {
        floor: !0,
        max: 255
      },
      percent: {
        max: 1
      },
      degrees: {
        mod: 360,
        floor: !0
      }
    },
        d = l.support = {},
        p = t("<p>")[0],
        f = t.each;

    p.style.cssText = "background-color:rgba(1,1,1,.5)", d.rgba = p.style.backgroundColor.indexOf("rgba") > -1, f(c, function (t, e) {
      e.cache = "_" + t, e.props.alpha = {
        idx: 3,
        type: "percent",
        def: 1
      };
    }), l.fn = t.extend(l.prototype, {
      parse: function (n, a, r, h) {
        if (n === e) return this._rgba = [null, null, null, null], this;
        (n.jquery || n.nodeType) && (n = t(n).css(a), a = e);
        var u = this,
            d = t.type(n),
            p = this._rgba = [];
        return a !== e && (n = [n, a, r, h], d = "array"), "string" === d ? this.parse(s(n) || o._default) : "array" === d ? (f(c.rgba.props, function (t, e) {
          p[e.idx] = i(n[e.idx], e);
        }), this) : "object" === d ? (n instanceof l ? f(c, function (t, e) {
          n[e.cache] && (u[e.cache] = n[e.cache].slice());
        }) : f(c, function (e, s) {
          var o = s.cache;
          f(s.props, function (t, e) {
            if (!u[o] && s.to) {
              if ("alpha" === t || null == n[t]) return;
              u[o] = s.to(u._rgba);
            }

            u[o][e.idx] = i(n[t], e, !0);
          }), u[o] && 0 > t.inArray(null, u[o].slice(0, 3)) && (u[o][3] = 1, s.from && (u._rgba = s.from(u[o])));
        }), this) : e;
      },
      is: function (t) {
        var i = l(t),
            s = !0,
            n = this;
        return f(c, function (t, o) {
          var a,
              r = i[o.cache];
          return r && (a = n[o.cache] || o.to && o.to(n._rgba) || [], f(o.props, function (t, i) {
            return null != r[i.idx] ? s = r[i.idx] === a[i.idx] : e;
          })), s;
        }), s;
      },
      _space: function () {
        var t = [],
            e = this;
        return f(c, function (i, s) {
          e[s.cache] && t.push(i);
        }), t.pop();
      },
      transition: function (t, e) {
        var s = l(t),
            n = s._space(),
            o = c[n],
            a = 0 === this.alpha() ? l("transparent") : this,
            r = a[o.cache] || o.to(a._rgba),
            h = r.slice();

        return s = s[o.cache], f(o.props, function (t, n) {
          var o = n.idx,
              a = r[o],
              l = s[o],
              c = u[n.type] || {};
          null !== l && (null === a ? h[o] = l : (c.mod && (l - a > c.mod / 2 ? a += c.mod : a - l > c.mod / 2 && (a -= c.mod)), h[o] = i((l - a) * e + a, n)));
        }), this[n](h);
      },
      blend: function (e) {
        if (1 === this._rgba[3]) return this;

        var i = this._rgba.slice(),
            s = i.pop(),
            n = l(e)._rgba;

        return l(t.map(i, function (t, e) {
          return (1 - s) * n[e] + s * t;
        }));
      },
      toRgbaString: function () {
        var e = "rgba(",
            i = t.map(this._rgba, function (t, e) {
          return null == t ? e > 2 ? 1 : 0 : t;
        });
        return 1 === i[3] && (i.pop(), e = "rgb("), e + i.join() + ")";
      },
      toHslaString: function () {
        var e = "hsla(",
            i = t.map(this.hsla(), function (t, e) {
          return null == t && (t = e > 2 ? 1 : 0), e && 3 > e && (t = Math.round(100 * t) + "%"), t;
        });
        return 1 === i[3] && (i.pop(), e = "hsl("), e + i.join() + ")";
      },
      toHexString: function (e) {
        var i = this._rgba.slice(),
            s = i.pop();

        return e && i.push(~~(255 * s)), "#" + t.map(i, function (t) {
          return t = (t || 0).toString(16), 1 === t.length ? "0" + t : t;
        }).join("");
      },
      toString: function () {
        return 0 === this._rgba[3] ? "transparent" : this.toRgbaString();
      }
    }), l.fn.parse.prototype = l.fn, c.hsla.to = function (t) {
      if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
      var e,
          i,
          s = t[0] / 255,
          n = t[1] / 255,
          o = t[2] / 255,
          a = t[3],
          r = Math.max(s, n, o),
          h = Math.min(s, n, o),
          l = r - h,
          c = r + h,
          u = .5 * c;
      return e = h === r ? 0 : s === r ? 60 * (n - o) / l + 360 : n === r ? 60 * (o - s) / l + 120 : 60 * (s - n) / l + 240, i = 0 === l ? 0 : .5 >= u ? l / c : l / (2 - c), [Math.round(e) % 360, i, u, null == a ? 1 : a];
    }, c.hsla.from = function (t) {
      if (null == t[0] || null == t[1] || null == t[2]) return [null, null, null, t[3]];
      var e = t[0] / 360,
          i = t[1],
          s = t[2],
          o = t[3],
          a = .5 >= s ? s * (1 + i) : s + i - s * i,
          r = 2 * s - a;
      return [Math.round(255 * n(r, a, e + 1 / 3)), Math.round(255 * n(r, a, e)), Math.round(255 * n(r, a, e - 1 / 3)), o];
    }, f(c, function (s, n) {
      var o = n.props,
          a = n.cache,
          h = n.to,
          c = n.from;
      l.fn[s] = function (s) {
        if (h && !this[a] && (this[a] = h(this._rgba)), s === e) return this[a].slice();
        var n,
            r = t.type(s),
            u = "array" === r || "object" === r ? s : arguments,
            d = this[a].slice();
        return f(o, function (t, e) {
          var s = u["object" === r ? t : e.idx];
          null == s && (s = d[e.idx]), d[e.idx] = i(s, e);
        }), c ? (n = l(c(d)), n[a] = d, n) : l(d);
      }, f(o, function (e, i) {
        l.fn[e] || (l.fn[e] = function (n) {
          var o,
              a = t.type(n),
              h = "alpha" === e ? this._hsla ? "hsla" : "rgba" : s,
              l = this[h](),
              c = l[i.idx];
          return "undefined" === a ? c : ("function" === a && (n = n.call(this, c), a = t.type(n)), null == n && i.empty ? this : ("string" === a && (o = r.exec(n), o && (n = c + parseFloat(o[2]) * ("+" === o[1] ? 1 : -1))), l[i.idx] = n, this[h](l)));
        });
      });
    }), l.hook = function (e) {
      var i = e.split(" ");
      f(i, function (e, i) {
        t.cssHooks[i] = {
          set: function (e, n) {
            var o,
                a,
                r = "";

            if ("transparent" !== n && ("string" !== t.type(n) || (o = s(n)))) {
              if (n = l(o || n), !d.rgba && 1 !== n._rgba[3]) {
                for (a = "backgroundColor" === i ? e.parentNode : e; ("" === r || "transparent" === r) && a && a.style;) try {
                  r = t.css(a, "backgroundColor"), a = a.parentNode;
                } catch (h) {}

                n = n.blend(r && "transparent" !== r ? r : "_default");
              }

              n = n.toRgbaString();
            }

            try {
              e.style[i] = n;
            } catch (h) {}
          }
        }, t.fx.step[i] = function (e) {
          e.colorInit || (e.start = l(e.elem, i), e.end = l(e.end), e.colorInit = !0), t.cssHooks[i].set(e.elem, e.start.transition(e.end, e.pos));
        };
      });
    }, l.hook(a), t.cssHooks.borderColor = {
      expand: function (t) {
        var e = {};
        return f(["Top", "Right", "Bottom", "Left"], function (i, s) {
          e["border" + s + "Color"] = t;
        }), e;
      }
    }, o = t.Color.names = {
      aqua: "#00ffff",
      black: "#000000",
      blue: "#0000ff",
      fuchsia: "#ff00ff",
      gray: "#808080",
      green: "#008000",
      lime: "#00ff00",
      maroon: "#800000",
      navy: "#000080",
      olive: "#808000",
      purple: "#800080",
      red: "#ff0000",
      silver: "#c0c0c0",
      teal: "#008080",
      white: "#ffffff",
      yellow: "#ffff00",
      transparent: [null, null, null, 0],
      _default: "#ffffff"
    };
  }(p), function () {
    function e(e) {
      var i,
          s,
          n = e.ownerDocument.defaultView ? e.ownerDocument.defaultView.getComputedStyle(e, null) : e.currentStyle,
          o = {};
      if (n && n.length && n[0] && n[n[0]]) for (s = n.length; s--;) i = n[s], "string" == typeof n[i] && (o[t.camelCase(i)] = n[i]);else for (i in n) "string" == typeof n[i] && (o[i] = n[i]);
      return o;
    }

    function i(e, i) {
      var s,
          o,
          a = {};

      for (s in i) o = i[s], e[s] !== o && (n[s] || (t.fx.step[s] || !isNaN(parseFloat(o))) && (a[s] = o));

      return a;
    }

    var s = ["add", "remove", "toggle"],
        n = {
      border: 1,
      borderBottom: 1,
      borderColor: 1,
      borderLeft: 1,
      borderRight: 1,
      borderTop: 1,
      borderWidth: 1,
      margin: 1,
      padding: 1
    };
    t.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function (e, i) {
      t.fx.step[i] = function (t) {
        ("none" !== t.end && !t.setAttr || 1 === t.pos && !t.setAttr) && (p.style(t.elem, i, t.end), t.setAttr = !0);
      };
    }), t.fn.addBack || (t.fn.addBack = function (t) {
      return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
    }), t.effects.animateClass = function (n, o, a, r) {
      var h = t.speed(o, a, r);
      return this.queue(function () {
        var o,
            a = t(this),
            r = a.attr("class") || "",
            l = h.children ? a.find("*").addBack() : a;
        l = l.map(function () {
          var i = t(this);
          return {
            el: i,
            start: e(this)
          };
        }), o = function () {
          t.each(s, function (t, e) {
            n[e] && a[e + "Class"](n[e]);
          });
        }, o(), l = l.map(function () {
          return this.end = e(this.el[0]), this.diff = i(this.start, this.end), this;
        }), a.attr("class", r), l = l.map(function () {
          var e = this,
              i = t.Deferred(),
              s = t.extend({}, h, {
            queue: !1,
            complete: function () {
              i.resolve(e);
            }
          });
          return this.el.animate(this.diff, s), i.promise();
        }), t.when.apply(t, l.get()).done(function () {
          o(), t.each(arguments, function () {
            var e = this.el;
            t.each(this.diff, function (t) {
              e.css(t, "");
            });
          }), h.complete.call(a[0]);
        });
      });
    }, t.fn.extend({
      addClass: function (e) {
        return function (i, s, n, o) {
          return s ? t.effects.animateClass.call(this, {
            add: i
          }, s, n, o) : e.apply(this, arguments);
        };
      }(t.fn.addClass),
      removeClass: function (e) {
        return function (i, s, n, o) {
          return arguments.length > 1 ? t.effects.animateClass.call(this, {
            remove: i
          }, s, n, o) : e.apply(this, arguments);
        };
      }(t.fn.removeClass),
      toggleClass: function (e) {
        return function (i, s, n, o, a) {
          return "boolean" == typeof s || void 0 === s ? n ? t.effects.animateClass.call(this, s ? {
            add: i
          } : {
            remove: i
          }, n, o, a) : e.apply(this, arguments) : t.effects.animateClass.call(this, {
            toggle: i
          }, s, n, o);
        };
      }(t.fn.toggleClass),
      switchClass: function (e, i, s, n, o) {
        return t.effects.animateClass.call(this, {
          add: i,
          remove: e
        }, s, n, o);
      }
    });
  }(), function () {
    function e(e, i, s, n) {
      return t.isPlainObject(e) && (i = e, e = e.effect), e = {
        effect: e
      }, null == i && (i = {}), t.isFunction(i) && (n = i, s = null, i = {}), ("number" == typeof i || t.fx.speeds[i]) && (n = s, s = i, i = {}), t.isFunction(s) && (n = s, s = null), i && t.extend(e, i), s = s || i.duration, e.duration = t.fx.off ? 0 : "number" == typeof s ? s : s in t.fx.speeds ? t.fx.speeds[s] : t.fx.speeds._default, e.complete = n || i.complete, e;
    }

    function i(e) {
      return !e || "number" == typeof e || t.fx.speeds[e] ? !0 : "string" != typeof e || t.effects.effect[e] ? t.isFunction(e) ? !0 : "object" != typeof e || e.effect ? !1 : !0 : !0;
    }

    function s(t, e) {
      var i = e.outerWidth(),
          s = e.outerHeight(),
          n = /^rect\((-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto)\)$/,
          o = n.exec(t) || ["", 0, i, s, 0];
      return {
        top: parseFloat(o[1]) || 0,
        right: "auto" === o[2] ? i : parseFloat(o[2]),
        bottom: "auto" === o[3] ? s : parseFloat(o[3]),
        left: parseFloat(o[4]) || 0
      };
    }

    t.expr && t.expr.filters && t.expr.filters.animated && (t.expr.filters.animated = function (e) {
      return function (i) {
        return !!t(i).data(d) || e(i);
      };
    }(t.expr.filters.animated)), t.uiBackCompat !== !1 && t.extend(t.effects, {
      save: function (t, e) {
        for (var i = 0, s = e.length; s > i; i++) null !== e[i] && t.data(c + e[i], t[0].style[e[i]]);
      },
      restore: function (t, e) {
        for (var i, s = 0, n = e.length; n > s; s++) null !== e[s] && (i = t.data(c + e[s]), t.css(e[s], i));
      },
      setMode: function (t, e) {
        return "toggle" === e && (e = t.is(":hidden") ? "show" : "hide"), e;
      },
      createWrapper: function (e) {
        if (e.parent().is(".ui-effects-wrapper")) return e.parent();
        var i = {
          width: e.outerWidth(!0),
          height: e.outerHeight(!0),
          "float": e.css("float")
        },
            s = t("<div></div>").addClass("ui-effects-wrapper").css({
          fontSize: "100%",
          background: "transparent",
          border: "none",
          margin: 0,
          padding: 0
        }),
            n = {
          width: e.width(),
          height: e.height()
        },
            o = document.activeElement;

        try {
          o.id;
        } catch (a) {
          o = document.body;
        }

        return e.wrap(s), (e[0] === o || t.contains(e[0], o)) && t(o).trigger("focus"), s = e.parent(), "static" === e.css("position") ? (s.css({
          position: "relative"
        }), e.css({
          position: "relative"
        })) : (t.extend(i, {
          position: e.css("position"),
          zIndex: e.css("z-index")
        }), t.each(["top", "left", "bottom", "right"], function (t, s) {
          i[s] = e.css(s), isNaN(parseInt(i[s], 10)) && (i[s] = "auto");
        }), e.css({
          position: "relative",
          top: 0,
          left: 0,
          right: "auto",
          bottom: "auto"
        })), e.css(n), s.css(i).show();
      },
      removeWrapper: function (e) {
        var i = document.activeElement;
        return e.parent().is(".ui-effects-wrapper") && (e.parent().replaceWith(e), (e[0] === i || t.contains(e[0], i)) && t(i).trigger("focus")), e;
      }
    }), t.extend(t.effects, {
      version: "1.12.1",
      define: function (e, i, s) {
        return s || (s = i, i = "effect"), t.effects.effect[e] = s, t.effects.effect[e].mode = i, s;
      },
      scaledDimensions: function (t, e, i) {
        if (0 === e) return {
          height: 0,
          width: 0,
          outerHeight: 0,
          outerWidth: 0
        };
        var s = "horizontal" !== i ? (e || 100) / 100 : 1,
            n = "vertical" !== i ? (e || 100) / 100 : 1;
        return {
          height: t.height() * n,
          width: t.width() * s,
          outerHeight: t.outerHeight() * n,
          outerWidth: t.outerWidth() * s
        };
      },
      clipToBox: function (t) {
        return {
          width: t.clip.right - t.clip.left,
          height: t.clip.bottom - t.clip.top,
          left: t.clip.left,
          top: t.clip.top
        };
      },
      unshift: function (t, e, i) {
        var s = t.queue();
        e > 1 && s.splice.apply(s, [1, 0].concat(s.splice(e, i))), t.dequeue();
      },
      saveStyle: function (t) {
        t.data(u, t[0].style.cssText);
      },
      restoreStyle: function (t) {
        t[0].style.cssText = t.data(u) || "", t.removeData(u);
      },
      mode: function (t, e) {
        var i = t.is(":hidden");
        return "toggle" === e && (e = i ? "show" : "hide"), (i ? "hide" === e : "show" === e) && (e = "none"), e;
      },
      getBaseline: function (t, e) {
        var i, s;

        switch (t[0]) {
          case "top":
            i = 0;
            break;

          case "middle":
            i = .5;
            break;

          case "bottom":
            i = 1;
            break;

          default:
            i = t[0] / e.height;
        }

        switch (t[1]) {
          case "left":
            s = 0;
            break;

          case "center":
            s = .5;
            break;

          case "right":
            s = 1;
            break;

          default:
            s = t[1] / e.width;
        }

        return {
          x: s,
          y: i
        };
      },
      createPlaceholder: function (e) {
        var i,
            s = e.css("position"),
            n = e.position();
        return e.css({
          marginTop: e.css("marginTop"),
          marginBottom: e.css("marginBottom"),
          marginLeft: e.css("marginLeft"),
          marginRight: e.css("marginRight")
        }).outerWidth(e.outerWidth()).outerHeight(e.outerHeight()), /^(static|relative)/.test(s) && (s = "absolute", i = t("<" + e[0].nodeName + ">").insertAfter(e).css({
          display: /^(inline|ruby)/.test(e.css("display")) ? "inline-block" : "block",
          visibility: "hidden",
          marginTop: e.css("marginTop"),
          marginBottom: e.css("marginBottom"),
          marginLeft: e.css("marginLeft"),
          marginRight: e.css("marginRight"),
          "float": e.css("float")
        }).outerWidth(e.outerWidth()).outerHeight(e.outerHeight()).addClass("ui-effects-placeholder"), e.data(c + "placeholder", i)), e.css({
          position: s,
          left: n.left,
          top: n.top
        }), i;
      },
      removePlaceholder: function (t) {
        var e = c + "placeholder",
            i = t.data(e);
        i && (i.remove(), t.removeData(e));
      },
      cleanUp: function (e) {
        t.effects.restoreStyle(e), t.effects.removePlaceholder(e);
      },
      setTransition: function (e, i, s, n) {
        return n = n || {}, t.each(i, function (t, i) {
          var o = e.cssUnit(i);
          o[0] > 0 && (n[i] = o[0] * s + o[1]);
        }), n;
      }
    }), t.fn.extend({
      effect: function () {
        function i(e) {
          function i() {
            r.removeData(d), t.effects.cleanUp(r), "hide" === s.mode && r.hide(), a();
          }

          function a() {
            t.isFunction(h) && h.call(r[0]), t.isFunction(e) && e();
          }

          var r = t(this);
          s.mode = c.shift(), t.uiBackCompat === !1 || o ? "none" === s.mode ? (r[l](), a()) : n.call(r[0], s, i) : (r.is(":hidden") ? "hide" === l : "show" === l) ? (r[l](), a()) : n.call(r[0], s, a);
        }

        var s = e.apply(this, arguments),
            n = t.effects.effect[s.effect],
            o = n.mode,
            a = s.queue,
            r = a || "fx",
            h = s.complete,
            l = s.mode,
            c = [],
            u = function (e) {
          var i = t(this),
              s = t.effects.mode(i, l) || o;
          i.data(d, !0), c.push(s), o && ("show" === s || s === o && "hide" === s) && i.show(), o && "none" === s || t.effects.saveStyle(i), t.isFunction(e) && e();
        };

        return t.fx.off || !n ? l ? this[l](s.duration, h) : this.each(function () {
          h && h.call(this);
        }) : a === !1 ? this.each(u).each(i) : this.queue(r, u).queue(r, i);
      },
      show: function (t) {
        return function (s) {
          if (i(s)) return t.apply(this, arguments);
          var n = e.apply(this, arguments);
          return n.mode = "show", this.effect.call(this, n);
        };
      }(t.fn.show),
      hide: function (t) {
        return function (s) {
          if (i(s)) return t.apply(this, arguments);
          var n = e.apply(this, arguments);
          return n.mode = "hide", this.effect.call(this, n);
        };
      }(t.fn.hide),
      toggle: function (t) {
        return function (s) {
          if (i(s) || "boolean" == typeof s) return t.apply(this, arguments);
          var n = e.apply(this, arguments);
          return n.mode = "toggle", this.effect.call(this, n);
        };
      }(t.fn.toggle),
      cssUnit: function (e) {
        var i = this.css(e),
            s = [];
        return t.each(["em", "px", "%", "pt"], function (t, e) {
          i.indexOf(e) > 0 && (s = [parseFloat(i), e]);
        }), s;
      },
      cssClip: function (t) {
        return t ? this.css("clip", "rect(" + t.top + "px " + t.right + "px " + t.bottom + "px " + t.left + "px)") : s(this.css("clip"), this);
      },
      transfer: function (e, i) {
        var s = t(this),
            n = t(e.to),
            o = "fixed" === n.css("position"),
            a = t("body"),
            r = o ? a.scrollTop() : 0,
            h = o ? a.scrollLeft() : 0,
            l = n.offset(),
            c = {
          top: l.top - r,
          left: l.left - h,
          height: n.innerHeight(),
          width: n.innerWidth()
        },
            u = s.offset(),
            d = t("<div class='ui-effects-transfer'></div>").appendTo("body").addClass(e.className).css({
          top: u.top - r,
          left: u.left - h,
          height: s.innerHeight(),
          width: s.innerWidth(),
          position: o ? "fixed" : "absolute"
        }).animate(c, e.duration, e.easing, function () {
          d.remove(), t.isFunction(i) && i();
        });
      }
    }), t.fx.step.clip = function (e) {
      e.clipInit || (e.start = t(e.elem).cssClip(), "string" == typeof e.end && (e.end = s(e.end, e.elem)), e.clipInit = !0), t(e.elem).cssClip({
        top: e.pos * (e.end.top - e.start.top) + e.start.top,
        right: e.pos * (e.end.right - e.start.right) + e.start.right,
        bottom: e.pos * (e.end.bottom - e.start.bottom) + e.start.bottom,
        left: e.pos * (e.end.left - e.start.left) + e.start.left
      });
    };
  }(), function () {
    var e = {};
    t.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function (t, i) {
      e[i] = function (e) {
        return Math.pow(e, t + 2);
      };
    }), t.extend(e, {
      Sine: function (t) {
        return 1 - Math.cos(t * Math.PI / 2);
      },
      Circ: function (t) {
        return 1 - Math.sqrt(1 - t * t);
      },
      Elastic: function (t) {
        return 0 === t || 1 === t ? t : -Math.pow(2, 8 * (t - 1)) * Math.sin((80 * (t - 1) - 7.5) * Math.PI / 15);
      },
      Back: function (t) {
        return t * t * (3 * t - 2);
      },
      Bounce: function (t) {
        for (var e, i = 4; ((e = Math.pow(2, --i)) - 1) / 11 > t;);

        return 1 / Math.pow(4, 3 - i) - 7.5625 * Math.pow((3 * e - 2) / 22 - t, 2);
      }
    }), t.each(e, function (e, i) {
      t.easing["easeIn" + e] = i, t.easing["easeOut" + e] = function (t) {
        return 1 - i(1 - t);
      }, t.easing["easeInOut" + e] = function (t) {
        return .5 > t ? i(2 * t) / 2 : 1 - i(-2 * t + 2) / 2;
      };
    });
  }();
  var f = t.effects;
  t.effects.define("blind", "hide", function (e, i) {
    var s = {
      up: ["bottom", "top"],
      vertical: ["bottom", "top"],
      down: ["top", "bottom"],
      left: ["right", "left"],
      horizontal: ["right", "left"],
      right: ["left", "right"]
    },
        n = t(this),
        o = e.direction || "up",
        a = n.cssClip(),
        r = {
      clip: t.extend({}, a)
    },
        h = t.effects.createPlaceholder(n);
    r.clip[s[o][0]] = r.clip[s[o][1]], "show" === e.mode && (n.cssClip(r.clip), h && h.css(t.effects.clipToBox(r)), r.clip = a), h && h.animate(t.effects.clipToBox(r), e.duration, e.easing), n.animate(r, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }), t.effects.define("bounce", function (e, i) {
    var s,
        n,
        o,
        a = t(this),
        r = e.mode,
        h = "hide" === r,
        l = "show" === r,
        c = e.direction || "up",
        u = e.distance,
        d = e.times || 5,
        p = 2 * d + (l || h ? 1 : 0),
        f = e.duration / p,
        g = e.easing,
        m = "up" === c || "down" === c ? "top" : "left",
        _ = "up" === c || "left" === c,
        v = 0,
        b = a.queue().length;

    for (t.effects.createPlaceholder(a), o = a.css(m), u || (u = a["top" === m ? "outerHeight" : "outerWidth"]() / 3), l && (n = {
      opacity: 1
    }, n[m] = o, a.css("opacity", 0).css(m, _ ? 2 * -u : 2 * u).animate(n, f, g)), h && (u /= Math.pow(2, d - 1)), n = {}, n[m] = o; d > v; v++) s = {}, s[m] = (_ ? "-=" : "+=") + u, a.animate(s, f, g).animate(n, f, g), u = h ? 2 * u : u / 2;

    h && (s = {
      opacity: 0
    }, s[m] = (_ ? "-=" : "+=") + u, a.animate(s, f, g)), a.queue(i), t.effects.unshift(a, b, p + 1);
  }), t.effects.define("clip", "hide", function (e, i) {
    var s,
        n = {},
        o = t(this),
        a = e.direction || "vertical",
        r = "both" === a,
        h = r || "horizontal" === a,
        l = r || "vertical" === a;
    s = o.cssClip(), n.clip = {
      top: l ? (s.bottom - s.top) / 2 : s.top,
      right: h ? (s.right - s.left) / 2 : s.right,
      bottom: l ? (s.bottom - s.top) / 2 : s.bottom,
      left: h ? (s.right - s.left) / 2 : s.left
    }, t.effects.createPlaceholder(o), "show" === e.mode && (o.cssClip(n.clip), n.clip = s), o.animate(n, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }), t.effects.define("drop", "hide", function (e, i) {
    var s,
        n = t(this),
        o = e.mode,
        a = "show" === o,
        r = e.direction || "left",
        h = "up" === r || "down" === r ? "top" : "left",
        l = "up" === r || "left" === r ? "-=" : "+=",
        c = "+=" === l ? "-=" : "+=",
        u = {
      opacity: 0
    };
    t.effects.createPlaceholder(n), s = e.distance || n["top" === h ? "outerHeight" : "outerWidth"](!0) / 2, u[h] = l + s, a && (n.css(u), u[h] = c + s, u.opacity = 1), n.animate(u, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }), t.effects.define("explode", "hide", function (e, i) {
    function s() {
      b.push(this), b.length === u * d && n();
    }

    function n() {
      p.css({
        visibility: "visible"
      }), t(b).remove(), i();
    }

    var o,
        a,
        r,
        h,
        l,
        c,
        u = e.pieces ? Math.round(Math.sqrt(e.pieces)) : 3,
        d = u,
        p = t(this),
        f = e.mode,
        g = "show" === f,
        m = p.show().css("visibility", "hidden").offset(),
        _ = Math.ceil(p.outerWidth() / d),
        v = Math.ceil(p.outerHeight() / u),
        b = [];

    for (o = 0; u > o; o++) for (h = m.top + o * v, c = o - (u - 1) / 2, a = 0; d > a; a++) r = m.left + a * _, l = a - (d - 1) / 2, p.clone().appendTo("body").wrap("<div></div>").css({
      position: "absolute",
      visibility: "visible",
      left: -a * _,
      top: -o * v
    }).parent().addClass("ui-effects-explode").css({
      position: "absolute",
      overflow: "hidden",
      width: _,
      height: v,
      left: r + (g ? l * _ : 0),
      top: h + (g ? c * v : 0),
      opacity: g ? 0 : 1
    }).animate({
      left: r + (g ? 0 : l * _),
      top: h + (g ? 0 : c * v),
      opacity: g ? 1 : 0
    }, e.duration || 500, e.easing, s);
  }), t.effects.define("fade", "toggle", function (e, i) {
    var s = "show" === e.mode;
    t(this).css("opacity", s ? 0 : 1).animate({
      opacity: s ? 1 : 0
    }, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }), t.effects.define("fold", "hide", function (e, i) {
    var s = t(this),
        n = e.mode,
        o = "show" === n,
        a = "hide" === n,
        r = e.size || 15,
        h = /([0-9]+)%/.exec(r),
        l = !!e.horizFirst,
        c = l ? ["right", "bottom"] : ["bottom", "right"],
        u = e.duration / 2,
        d = t.effects.createPlaceholder(s),
        p = s.cssClip(),
        f = {
      clip: t.extend({}, p)
    },
        g = {
      clip: t.extend({}, p)
    },
        m = [p[c[0]], p[c[1]]],
        _ = s.queue().length;
    h && (r = parseInt(h[1], 10) / 100 * m[a ? 0 : 1]), f.clip[c[0]] = r, g.clip[c[0]] = r, g.clip[c[1]] = 0, o && (s.cssClip(g.clip), d && d.css(t.effects.clipToBox(g)), g.clip = p), s.queue(function (i) {
      d && d.animate(t.effects.clipToBox(f), u, e.easing).animate(t.effects.clipToBox(g), u, e.easing), i();
    }).animate(f, u, e.easing).animate(g, u, e.easing).queue(i), t.effects.unshift(s, _, 4);
  }), t.effects.define("highlight", "show", function (e, i) {
    var s = t(this),
        n = {
      backgroundColor: s.css("backgroundColor")
    };
    "hide" === e.mode && (n.opacity = 0), t.effects.saveStyle(s), s.css({
      backgroundImage: "none",
      backgroundColor: e.color || "#ffff99"
    }).animate(n, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  }), t.effects.define("size", function (e, i) {
    var s,
        n,
        o,
        a = t(this),
        r = ["fontSize"],
        h = ["borderTopWidth", "borderBottomWidth", "paddingTop", "paddingBottom"],
        l = ["borderLeftWidth", "borderRightWidth", "paddingLeft", "paddingRight"],
        c = e.mode,
        u = "effect" !== c,
        d = e.scale || "both",
        p = e.origin || ["middle", "center"],
        f = a.css("position"),
        g = a.position(),
        m = t.effects.scaledDimensions(a),
        _ = e.from || m,
        v = e.to || t.effects.scaledDimensions(a, 0);

    t.effects.createPlaceholder(a), "show" === c && (o = _, _ = v, v = o), n = {
      from: {
        y: _.height / m.height,
        x: _.width / m.width
      },
      to: {
        y: v.height / m.height,
        x: v.width / m.width
      }
    }, ("box" === d || "both" === d) && (n.from.y !== n.to.y && (_ = t.effects.setTransition(a, h, n.from.y, _), v = t.effects.setTransition(a, h, n.to.y, v)), n.from.x !== n.to.x && (_ = t.effects.setTransition(a, l, n.from.x, _), v = t.effects.setTransition(a, l, n.to.x, v))), ("content" === d || "both" === d) && n.from.y !== n.to.y && (_ = t.effects.setTransition(a, r, n.from.y, _), v = t.effects.setTransition(a, r, n.to.y, v)), p && (s = t.effects.getBaseline(p, m), _.top = (m.outerHeight - _.outerHeight) * s.y + g.top, _.left = (m.outerWidth - _.outerWidth) * s.x + g.left, v.top = (m.outerHeight - v.outerHeight) * s.y + g.top, v.left = (m.outerWidth - v.outerWidth) * s.x + g.left), a.css(_), ("content" === d || "both" === d) && (h = h.concat(["marginTop", "marginBottom"]).concat(r), l = l.concat(["marginLeft", "marginRight"]), a.find("*[width]").each(function () {
      var i = t(this),
          s = t.effects.scaledDimensions(i),
          o = {
        height: s.height * n.from.y,
        width: s.width * n.from.x,
        outerHeight: s.outerHeight * n.from.y,
        outerWidth: s.outerWidth * n.from.x
      },
          a = {
        height: s.height * n.to.y,
        width: s.width * n.to.x,
        outerHeight: s.height * n.to.y,
        outerWidth: s.width * n.to.x
      };
      n.from.y !== n.to.y && (o = t.effects.setTransition(i, h, n.from.y, o), a = t.effects.setTransition(i, h, n.to.y, a)), n.from.x !== n.to.x && (o = t.effects.setTransition(i, l, n.from.x, o), a = t.effects.setTransition(i, l, n.to.x, a)), u && t.effects.saveStyle(i), i.css(o), i.animate(a, e.duration, e.easing, function () {
        u && t.effects.restoreStyle(i);
      });
    })), a.animate(v, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: function () {
        var e = a.offset();
        0 === v.opacity && a.css("opacity", _.opacity), u || (a.css("position", "static" === f ? "relative" : f).offset(e), t.effects.saveStyle(a)), i();
      }
    });
  }), t.effects.define("scale", function (e, i) {
    var s = t(this),
        n = e.mode,
        o = parseInt(e.percent, 10) || (0 === parseInt(e.percent, 10) ? 0 : "effect" !== n ? 0 : 100),
        a = t.extend(!0, {
      from: t.effects.scaledDimensions(s),
      to: t.effects.scaledDimensions(s, o, e.direction || "both"),
      origin: e.origin || ["middle", "center"]
    }, e);
    e.fade && (a.from.opacity = 1, a.to.opacity = 0), t.effects.effect.size.call(this, a, i);
  }), t.effects.define("puff", "hide", function (e, i) {
    var s = t.extend(!0, {}, e, {
      fade: !0,
      percent: parseInt(e.percent, 10) || 150
    });
    t.effects.effect.scale.call(this, s, i);
  }), t.effects.define("pulsate", "show", function (e, i) {
    var s = t(this),
        n = e.mode,
        o = "show" === n,
        a = "hide" === n,
        r = o || a,
        h = 2 * (e.times || 5) + (r ? 1 : 0),
        l = e.duration / h,
        c = 0,
        u = 1,
        d = s.queue().length;

    for ((o || !s.is(":visible")) && (s.css("opacity", 0).show(), c = 1); h > u; u++) s.animate({
      opacity: c
    }, l, e.easing), c = 1 - c;

    s.animate({
      opacity: c
    }, l, e.easing), s.queue(i), t.effects.unshift(s, d, h + 1);
  }), t.effects.define("shake", function (e, i) {
    var s = 1,
        n = t(this),
        o = e.direction || "left",
        a = e.distance || 20,
        r = e.times || 3,
        h = 2 * r + 1,
        l = Math.round(e.duration / h),
        c = "up" === o || "down" === o ? "top" : "left",
        u = "up" === o || "left" === o,
        d = {},
        p = {},
        f = {},
        g = n.queue().length;

    for (t.effects.createPlaceholder(n), d[c] = (u ? "-=" : "+=") + a, p[c] = (u ? "+=" : "-=") + 2 * a, f[c] = (u ? "-=" : "+=") + 2 * a, n.animate(d, l, e.easing); r > s; s++) n.animate(p, l, e.easing).animate(f, l, e.easing);

    n.animate(p, l, e.easing).animate(d, l / 2, e.easing).queue(i), t.effects.unshift(n, g, h + 1);
  }), t.effects.define("slide", "show", function (e, i) {
    var s,
        n,
        o = t(this),
        a = {
      up: ["bottom", "top"],
      down: ["top", "bottom"],
      left: ["right", "left"],
      right: ["left", "right"]
    },
        r = e.mode,
        h = e.direction || "left",
        l = "up" === h || "down" === h ? "top" : "left",
        c = "up" === h || "left" === h,
        u = e.distance || o["top" === l ? "outerHeight" : "outerWidth"](!0),
        d = {};
    t.effects.createPlaceholder(o), s = o.cssClip(), n = o.position()[l], d[l] = (c ? -1 : 1) * u + n, d.clip = o.cssClip(), d.clip[a[h][1]] = d.clip[a[h][0]], "show" === r && (o.cssClip(d.clip), o.css(l, d[l]), d.clip = s, d[l] = n), o.animate(d, {
      queue: !1,
      duration: e.duration,
      easing: e.easing,
      complete: i
    });
  });
  var f;
  t.uiBackCompat !== !1 && (f = t.effects.define("transfer", function (e, i) {
    t(this).transfer(e, i);
  })), t.ui.focusable = function (i, s) {
    var n,
        o,
        a,
        r,
        h,
        l = i.nodeName.toLowerCase();
    return "area" === l ? (n = i.parentNode, o = n.name, i.href && o && "map" === n.nodeName.toLowerCase() ? (a = t("img[usemap='#" + o + "']"), a.length > 0 && a.is(":visible")) : !1) : (/^(input|select|textarea|button|object)$/.test(l) ? (r = !i.disabled, r && (h = t(i).closest("fieldset")[0], h && (r = !h.disabled))) : r = "a" === l ? i.href || s : s, r && t(i).is(":visible") && e(t(i)));
  }, t.extend(t.expr[":"], {
    focusable: function (e) {
      return t.ui.focusable(e, null != t.attr(e, "tabindex"));
    }
  }), t.ui.focusable, t.fn.form = function () {
    return "string" == typeof this[0].form ? this.closest("form") : t(this[0].form);
  }, t.ui.formResetMixin = {
    _formResetHandler: function () {
      var e = t(this);
      setTimeout(function () {
        var i = e.data("ui-form-reset-instances");
        t.each(i, function () {
          this.refresh();
        });
      });
    },
    _bindFormResetHandler: function () {
      if (this.form = this.element.form(), this.form.length) {
        var t = this.form.data("ui-form-reset-instances") || [];
        t.length || this.form.on("reset.ui-form-reset", this._formResetHandler), t.push(this), this.form.data("ui-form-reset-instances", t);
      }
    },
    _unbindFormResetHandler: function () {
      if (this.form.length) {
        var e = this.form.data("ui-form-reset-instances");
        e.splice(t.inArray(this, e), 1), e.length ? this.form.data("ui-form-reset-instances", e) : this.form.removeData("ui-form-reset-instances").off("reset.ui-form-reset");
      }
    }
  }, "1.7" === t.fn.jquery.substring(0, 3) && (t.each(["Width", "Height"], function (e, i) {
    function s(e, i, s, o) {
      return t.each(n, function () {
        i -= parseFloat(t.css(e, "padding" + this)) || 0, s && (i -= parseFloat(t.css(e, "border" + this + "Width")) || 0), o && (i -= parseFloat(t.css(e, "margin" + this)) || 0);
      }), i;
    }

    var n = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
        o = i.toLowerCase(),
        a = {
      innerWidth: t.fn.innerWidth,
      innerHeight: t.fn.innerHeight,
      outerWidth: t.fn.outerWidth,
      outerHeight: t.fn.outerHeight
    };
    t.fn["inner" + i] = function (e) {
      return void 0 === e ? a["inner" + i].call(this) : this.each(function () {
        t(this).css(o, s(this, e) + "px");
      });
    }, t.fn["outer" + i] = function (e, n) {
      return "number" != typeof e ? a["outer" + i].call(this, e) : this.each(function () {
        t(this).css(o, s(this, e, !0, n) + "px");
      });
    };
  }), t.fn.addBack = function (t) {
    return this.add(null == t ? this.prevObject : this.prevObject.filter(t));
  }), t.ui.keyCode = {
    BACKSPACE: 8,
    COMMA: 188,
    DELETE: 46,
    DOWN: 40,
    END: 35,
    ENTER: 13,
    ESCAPE: 27,
    HOME: 36,
    LEFT: 37,
    PAGE_DOWN: 34,
    PAGE_UP: 33,
    PERIOD: 190,
    RIGHT: 39,
    SPACE: 32,
    TAB: 9,
    UP: 38
  }, t.ui.escapeSelector = function () {
    var t = /([!"#$%&'()*+,./:;<=>?@[\]^`{|}~])/g;
    return function (e) {
      return e.replace(t, "\\$1");
    };
  }(), t.fn.labels = function () {
    var e, i, s, n, o;
    return this[0].labels && this[0].labels.length ? this.pushStack(this[0].labels) : (n = this.eq(0).parents("label"), s = this.attr("id"), s && (e = this.eq(0).parents().last(), o = e.add(e.length ? e.siblings() : this.siblings()), i = "label[for='" + t.ui.escapeSelector(s) + "']", n = n.add(o.find(i).addBack(i))), this.pushStack(n));
  }, t.fn.scrollParent = function (e) {
    var i = this.css("position"),
        s = "absolute" === i,
        n = e ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
        o = this.parents().filter(function () {
      var e = t(this);
      return s && "static" === e.css("position") ? !1 : n.test(e.css("overflow") + e.css("overflow-y") + e.css("overflow-x"));
    }).eq(0);
    return "fixed" !== i && o.length ? o : t(this[0].ownerDocument || document);
  }, t.extend(t.expr[":"], {
    tabbable: function (e) {
      var i = t.attr(e, "tabindex"),
          s = null != i;
      return (!s || i >= 0) && t.ui.focusable(e, s);
    }
  }), t.fn.extend({
    uniqueId: function () {
      var t = 0;
      return function () {
        return this.each(function () {
          this.id || (this.id = "ui-id-" + ++t);
        });
      };
    }(),
    removeUniqueId: function () {
      return this.each(function () {
        /^ui-id-\d+$/.test(this.id) && t(this).removeAttr("id");
      });
    }
  }), t.widget("ui.accordion", {
    version: "1.12.1",
    options: {
      active: 0,
      animate: {},
      classes: {
        "ui-accordion-header": "ui-corner-top",
        "ui-accordion-header-collapsed": "ui-corner-all",
        "ui-accordion-content": "ui-corner-bottom"
      },
      collapsible: !1,
      event: "click",
      header: "> li > :first-child, > :not(li):even",
      heightStyle: "auto",
      icons: {
        activeHeader: "ui-icon-triangle-1-s",
        header: "ui-icon-triangle-1-e"
      },
      activate: null,
      beforeActivate: null
    },
    hideProps: {
      borderTopWidth: "hide",
      borderBottomWidth: "hide",
      paddingTop: "hide",
      paddingBottom: "hide",
      height: "hide"
    },
    showProps: {
      borderTopWidth: "show",
      borderBottomWidth: "show",
      paddingTop: "show",
      paddingBottom: "show",
      height: "show"
    },
    _create: function () {
      var e = this.options;
      this.prevShow = this.prevHide = t(), this._addClass("ui-accordion", "ui-widget ui-helper-reset"), this.element.attr("role", "tablist"), e.collapsible || e.active !== !1 && null != e.active || (e.active = 0), this._processPanels(), 0 > e.active && (e.active += this.headers.length), this._refresh();
    },
    _getCreateEventData: function () {
      return {
        header: this.active,
        panel: this.active.length ? this.active.next() : t()
      };
    },
    _createIcons: function () {
      var e,
          i,
          s = this.options.icons;
      s && (e = t("<span>"), this._addClass(e, "ui-accordion-header-icon", "ui-icon " + s.header), e.prependTo(this.headers), i = this.active.children(".ui-accordion-header-icon"), this._removeClass(i, s.header)._addClass(i, null, s.activeHeader)._addClass(this.headers, "ui-accordion-icons"));
    },
    _destroyIcons: function () {
      this._removeClass(this.headers, "ui-accordion-icons"), this.headers.children(".ui-accordion-header-icon").remove();
    },
    _destroy: function () {
      var t;
      this.element.removeAttr("role"), this.headers.removeAttr("role aria-expanded aria-selected aria-controls tabIndex").removeUniqueId(), this._destroyIcons(), t = this.headers.next().css("display", "").removeAttr("role aria-hidden aria-labelledby").removeUniqueId(), "content" !== this.options.heightStyle && t.css("height", "");
    },
    _setOption: function (t, e) {
      return "active" === t ? (this._activate(e), void 0) : ("event" === t && (this.options.event && this._off(this.headers, this.options.event), this._setupEvents(e)), this._super(t, e), "collapsible" !== t || e || this.options.active !== !1 || this._activate(0), "icons" === t && (this._destroyIcons(), e && this._createIcons()), void 0);
    },
    _setOptionDisabled: function (t) {
      this._super(t), this.element.attr("aria-disabled", t), this._toggleClass(null, "ui-state-disabled", !!t), this._toggleClass(this.headers.add(this.headers.next()), null, "ui-state-disabled", !!t);
    },
    _keydown: function (e) {
      if (!e.altKey && !e.ctrlKey) {
        var i = t.ui.keyCode,
            s = this.headers.length,
            n = this.headers.index(e.target),
            o = !1;

        switch (e.keyCode) {
          case i.RIGHT:
          case i.DOWN:
            o = this.headers[(n + 1) % s];
            break;

          case i.LEFT:
          case i.UP:
            o = this.headers[(n - 1 + s) % s];
            break;

          case i.SPACE:
          case i.ENTER:
            this._eventHandler(e);

            break;

          case i.HOME:
            o = this.headers[0];
            break;

          case i.END:
            o = this.headers[s - 1];
        }

        o && (t(e.target).attr("tabIndex", -1), t(o).attr("tabIndex", 0), t(o).trigger("focus"), e.preventDefault());
      }
    },
    _panelKeyDown: function (e) {
      e.keyCode === t.ui.keyCode.UP && e.ctrlKey && t(e.currentTarget).prev().trigger("focus");
    },
    refresh: function () {
      var e = this.options;
      this._processPanels(), e.active === !1 && e.collapsible === !0 || !this.headers.length ? (e.active = !1, this.active = t()) : e.active === !1 ? this._activate(0) : this.active.length && !t.contains(this.element[0], this.active[0]) ? this.headers.length === this.headers.find(".ui-state-disabled").length ? (e.active = !1, this.active = t()) : this._activate(Math.max(0, e.active - 1)) : e.active = this.headers.index(this.active), this._destroyIcons(), this._refresh();
    },
    _processPanels: function () {
      var t = this.headers,
          e = this.panels;
      this.headers = this.element.find(this.options.header), this._addClass(this.headers, "ui-accordion-header ui-accordion-header-collapsed", "ui-state-default"), this.panels = this.headers.next().filter(":not(.ui-accordion-content-active)").hide(), this._addClass(this.panels, "ui-accordion-content", "ui-helper-reset ui-widget-content"), e && (this._off(t.not(this.headers)), this._off(e.not(this.panels)));
    },
    _refresh: function () {
      var e,
          i = this.options,
          s = i.heightStyle,
          n = this.element.parent();
      this.active = this._findActive(i.active), this._addClass(this.active, "ui-accordion-header-active", "ui-state-active")._removeClass(this.active, "ui-accordion-header-collapsed"), this._addClass(this.active.next(), "ui-accordion-content-active"), this.active.next().show(), this.headers.attr("role", "tab").each(function () {
        var e = t(this),
            i = e.uniqueId().attr("id"),
            s = e.next(),
            n = s.uniqueId().attr("id");
        e.attr("aria-controls", n), s.attr("aria-labelledby", i);
      }).next().attr("role", "tabpanel"), this.headers.not(this.active).attr({
        "aria-selected": "false",
        "aria-expanded": "false",
        tabIndex: -1
      }).next().attr({
        "aria-hidden": "true"
      }).hide(), this.active.length ? this.active.attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      }).next().attr({
        "aria-hidden": "false"
      }) : this.headers.eq(0).attr("tabIndex", 0), this._createIcons(), this._setupEvents(i.event), "fill" === s ? (e = n.height(), this.element.siblings(":visible").each(function () {
        var i = t(this),
            s = i.css("position");
        "absolute" !== s && "fixed" !== s && (e -= i.outerHeight(!0));
      }), this.headers.each(function () {
        e -= t(this).outerHeight(!0);
      }), this.headers.next().each(function () {
        t(this).height(Math.max(0, e - t(this).innerHeight() + t(this).height()));
      }).css("overflow", "auto")) : "auto" === s && (e = 0, this.headers.next().each(function () {
        var i = t(this).is(":visible");
        i || t(this).show(), e = Math.max(e, t(this).css("height", "").height()), i || t(this).hide();
      }).height(e));
    },
    _activate: function (e) {
      var i = this._findActive(e)[0];

      i !== this.active[0] && (i = i || this.active[0], this._eventHandler({
        target: i,
        currentTarget: i,
        preventDefault: t.noop
      }));
    },
    _findActive: function (e) {
      return "number" == typeof e ? this.headers.eq(e) : t();
    },
    _setupEvents: function (e) {
      var i = {
        keydown: "_keydown"
      };
      e && t.each(e.split(" "), function (t, e) {
        i[e] = "_eventHandler";
      }), this._off(this.headers.add(this.headers.next())), this._on(this.headers, i), this._on(this.headers.next(), {
        keydown: "_panelKeyDown"
      }), this._hoverable(this.headers), this._focusable(this.headers);
    },
    _eventHandler: function (e) {
      var i,
          s,
          n = this.options,
          o = this.active,
          a = t(e.currentTarget),
          r = a[0] === o[0],
          h = r && n.collapsible,
          l = h ? t() : a.next(),
          c = o.next(),
          u = {
        oldHeader: o,
        oldPanel: c,
        newHeader: h ? t() : a,
        newPanel: l
      };
      e.preventDefault(), r && !n.collapsible || this._trigger("beforeActivate", e, u) === !1 || (n.active = h ? !1 : this.headers.index(a), this.active = r ? t() : a, this._toggle(u), this._removeClass(o, "ui-accordion-header-active", "ui-state-active"), n.icons && (i = o.children(".ui-accordion-header-icon"), this._removeClass(i, null, n.icons.activeHeader)._addClass(i, null, n.icons.header)), r || (this._removeClass(a, "ui-accordion-header-collapsed")._addClass(a, "ui-accordion-header-active", "ui-state-active"), n.icons && (s = a.children(".ui-accordion-header-icon"), this._removeClass(s, null, n.icons.header)._addClass(s, null, n.icons.activeHeader)), this._addClass(a.next(), "ui-accordion-content-active")));
    },
    _toggle: function (e) {
      var i = e.newPanel,
          s = this.prevShow.length ? this.prevShow : e.oldPanel;
      this.prevShow.add(this.prevHide).stop(!0, !0), this.prevShow = i, this.prevHide = s, this.options.animate ? this._animate(i, s, e) : (s.hide(), i.show(), this._toggleComplete(e)), s.attr({
        "aria-hidden": "true"
      }), s.prev().attr({
        "aria-selected": "false",
        "aria-expanded": "false"
      }), i.length && s.length ? s.prev().attr({
        tabIndex: -1,
        "aria-expanded": "false"
      }) : i.length && this.headers.filter(function () {
        return 0 === parseInt(t(this).attr("tabIndex"), 10);
      }).attr("tabIndex", -1), i.attr("aria-hidden", "false").prev().attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      });
    },
    _animate: function (t, e, i) {
      var s,
          n,
          o,
          a = this,
          r = 0,
          h = t.css("box-sizing"),
          l = t.length && (!e.length || t.index() < e.index()),
          c = this.options.animate || {},
          u = l && c.down || c,
          d = function () {
        a._toggleComplete(i);
      };

      return "number" == typeof u && (o = u), "string" == typeof u && (n = u), n = n || u.easing || c.easing, o = o || u.duration || c.duration, e.length ? t.length ? (s = t.show().outerHeight(), e.animate(this.hideProps, {
        duration: o,
        easing: n,
        step: function (t, e) {
          e.now = Math.round(t);
        }
      }), t.hide().animate(this.showProps, {
        duration: o,
        easing: n,
        complete: d,
        step: function (t, i) {
          i.now = Math.round(t), "height" !== i.prop ? "content-box" === h && (r += i.now) : "content" !== a.options.heightStyle && (i.now = Math.round(s - e.outerHeight() - r), r = 0);
        }
      }), void 0) : e.animate(this.hideProps, o, n, d) : t.animate(this.showProps, o, n, d);
    },
    _toggleComplete: function (t) {
      var e = t.oldPanel,
          i = e.prev();
      this._removeClass(e, "ui-accordion-content-active"), this._removeClass(i, "ui-accordion-header-active")._addClass(i, "ui-accordion-header-collapsed"), e.length && (e.parent()[0].className = e.parent()[0].className), this._trigger("activate", null, t);
    }
  }), t.ui.safeActiveElement = function (t) {
    var e;

    try {
      e = t.activeElement;
    } catch (i) {
      e = t.body;
    }

    return e || (e = t.body), e.nodeName || (e = t.body), e;
  }, t.widget("ui.menu", {
    version: "1.12.1",
    defaultElement: "<ul>",
    delay: 300,
    options: {
      icons: {
        submenu: "ui-icon-caret-1-e"
      },
      items: "> *",
      menus: "ul",
      position: {
        my: "left top",
        at: "right top"
      },
      role: "menu",
      blur: null,
      focus: null,
      select: null
    },
    _create: function () {
      this.activeMenu = this.element, this.mouseHandled = !1, this.element.uniqueId().attr({
        role: this.options.role,
        tabIndex: 0
      }), this._addClass("ui-menu", "ui-widget ui-widget-content"), this._on({
        "mousedown .ui-menu-item": function (t) {
          t.preventDefault();
        },
        "click .ui-menu-item": function (e) {
          var i = t(e.target),
              s = t(t.ui.safeActiveElement(this.document[0]));
          !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(e), e.isPropagationStopped() || (this.mouseHandled = !0), i.has(".ui-menu").length ? this.expand(e) : !this.element.is(":focus") && s.closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)));
        },
        "mouseenter .ui-menu-item": function (e) {
          if (!this.previousFilter) {
            var i = t(e.target).closest(".ui-menu-item"),
                s = t(e.currentTarget);
            i[0] === s[0] && (this._removeClass(s.siblings().children(".ui-state-active"), null, "ui-state-active"), this.focus(e, s));
          }
        },
        mouseleave: "collapseAll",
        "mouseleave .ui-menu": "collapseAll",
        focus: function (t, e) {
          var i = this.active || this.element.find(this.options.items).eq(0);
          e || this.focus(t, i);
        },
        blur: function (e) {
          this._delay(function () {
            var i = !t.contains(this.element[0], t.ui.safeActiveElement(this.document[0]));
            i && this.collapseAll(e);
          });
        },
        keydown: "_keydown"
      }), this.refresh(), this._on(this.document, {
        click: function (t) {
          this._closeOnDocumentClick(t) && this.collapseAll(t), this.mouseHandled = !1;
        }
      });
    },
    _destroy: function () {
      var e = this.element.find(".ui-menu-item").removeAttr("role aria-disabled"),
          i = e.children(".ui-menu-item-wrapper").removeUniqueId().removeAttr("tabIndex role aria-haspopup");
      this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeAttr("role aria-labelledby aria-expanded aria-hidden aria-disabled tabIndex").removeUniqueId().show(), i.children().each(function () {
        var e = t(this);
        e.data("ui-menu-submenu-caret") && e.remove();
      });
    },
    _keydown: function (e) {
      var i,
          s,
          n,
          o,
          a = !0;

      switch (e.keyCode) {
        case t.ui.keyCode.PAGE_UP:
          this.previousPage(e);
          break;

        case t.ui.keyCode.PAGE_DOWN:
          this.nextPage(e);
          break;

        case t.ui.keyCode.HOME:
          this._move("first", "first", e);

          break;

        case t.ui.keyCode.END:
          this._move("last", "last", e);

          break;

        case t.ui.keyCode.UP:
          this.previous(e);
          break;

        case t.ui.keyCode.DOWN:
          this.next(e);
          break;

        case t.ui.keyCode.LEFT:
          this.collapse(e);
          break;

        case t.ui.keyCode.RIGHT:
          this.active && !this.active.is(".ui-state-disabled") && this.expand(e);
          break;

        case t.ui.keyCode.ENTER:
        case t.ui.keyCode.SPACE:
          this._activate(e);

          break;

        case t.ui.keyCode.ESCAPE:
          this.collapse(e);
          break;

        default:
          a = !1, s = this.previousFilter || "", o = !1, n = e.keyCode >= 96 && 105 >= e.keyCode ? "" + (e.keyCode - 96) : String.fromCharCode(e.keyCode), clearTimeout(this.filterTimer), n === s ? o = !0 : n = s + n, i = this._filterMenuItems(n), i = o && -1 !== i.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : i, i.length || (n = String.fromCharCode(e.keyCode), i = this._filterMenuItems(n)), i.length ? (this.focus(e, i), this.previousFilter = n, this.filterTimer = this._delay(function () {
            delete this.previousFilter;
          }, 1e3)) : delete this.previousFilter;
      }

      a && e.preventDefault();
    },
    _activate: function (t) {
      this.active && !this.active.is(".ui-state-disabled") && (this.active.children("[aria-haspopup='true']").length ? this.expand(t) : this.select(t));
    },
    refresh: function () {
      var e,
          i,
          s,
          n,
          o,
          a = this,
          r = this.options.icons.submenu,
          h = this.element.find(this.options.menus);
      this._toggleClass("ui-menu-icons", null, !!this.element.find(".ui-icon").length), s = h.filter(":not(.ui-menu)").hide().attr({
        role: this.options.role,
        "aria-hidden": "true",
        "aria-expanded": "false"
      }).each(function () {
        var e = t(this),
            i = e.prev(),
            s = t("<span>").data("ui-menu-submenu-caret", !0);
        a._addClass(s, "ui-menu-icon", "ui-icon " + r), i.attr("aria-haspopup", "true").prepend(s), e.attr("aria-labelledby", i.attr("id"));
      }), this._addClass(s, "ui-menu", "ui-widget ui-widget-content ui-front"), e = h.add(this.element), i = e.find(this.options.items), i.not(".ui-menu-item").each(function () {
        var e = t(this);
        a._isDivider(e) && a._addClass(e, "ui-menu-divider", "ui-widget-content");
      }), n = i.not(".ui-menu-item, .ui-menu-divider"), o = n.children().not(".ui-menu").uniqueId().attr({
        tabIndex: -1,
        role: this._itemRole()
      }), this._addClass(n, "ui-menu-item")._addClass(o, "ui-menu-item-wrapper"), i.filter(".ui-state-disabled").attr("aria-disabled", "true"), this.active && !t.contains(this.element[0], this.active[0]) && this.blur();
    },
    _itemRole: function () {
      return {
        menu: "menuitem",
        listbox: "option"
      }[this.options.role];
    },
    _setOption: function (t, e) {
      if ("icons" === t) {
        var i = this.element.find(".ui-menu-icon");

        this._removeClass(i, null, this.options.icons.submenu)._addClass(i, null, e.submenu);
      }

      this._super(t, e);
    },
    _setOptionDisabled: function (t) {
      this._super(t), this.element.attr("aria-disabled", t + ""), this._toggleClass(null, "ui-state-disabled", !!t);
    },
    focus: function (t, e) {
      var i, s, n;
      this.blur(t, t && "focus" === t.type), this._scrollIntoView(e), this.active = e.first(), s = this.active.children(".ui-menu-item-wrapper"), this._addClass(s, null, "ui-state-active"), this.options.role && this.element.attr("aria-activedescendant", s.attr("id")), n = this.active.parent().closest(".ui-menu-item").children(".ui-menu-item-wrapper"), this._addClass(n, null, "ui-state-active"), t && "keydown" === t.type ? this._close() : this.timer = this._delay(function () {
        this._close();
      }, this.delay), i = e.children(".ui-menu"), i.length && t && /^mouse/.test(t.type) && this._startOpening(i), this.activeMenu = e.parent(), this._trigger("focus", t, {
        item: e
      });
    },
    _scrollIntoView: function (e) {
      var i, s, n, o, a, r;
      this._hasScroll() && (i = parseFloat(t.css(this.activeMenu[0], "borderTopWidth")) || 0, s = parseFloat(t.css(this.activeMenu[0], "paddingTop")) || 0, n = e.offset().top - this.activeMenu.offset().top - i - s, o = this.activeMenu.scrollTop(), a = this.activeMenu.height(), r = e.outerHeight(), 0 > n ? this.activeMenu.scrollTop(o + n) : n + r > a && this.activeMenu.scrollTop(o + n - a + r));
    },
    blur: function (t, e) {
      e || clearTimeout(this.timer), this.active && (this._removeClass(this.active.children(".ui-menu-item-wrapper"), null, "ui-state-active"), this._trigger("blur", t, {
        item: this.active
      }), this.active = null);
    },
    _startOpening: function (t) {
      clearTimeout(this.timer), "true" === t.attr("aria-hidden") && (this.timer = this._delay(function () {
        this._close(), this._open(t);
      }, this.delay));
    },
    _open: function (e) {
      var i = t.extend({
        of: this.active
      }, this.options.position);
      clearTimeout(this.timer), this.element.find(".ui-menu").not(e.parents(".ui-menu")).hide().attr("aria-hidden", "true"), e.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i);
    },
    collapseAll: function (e, i) {
      clearTimeout(this.timer), this.timer = this._delay(function () {
        var s = i ? this.element : t(e && e.target).closest(this.element.find(".ui-menu"));
        s.length || (s = this.element), this._close(s), this.blur(e), this._removeClass(s.find(".ui-state-active"), null, "ui-state-active"), this.activeMenu = s;
      }, this.delay);
    },
    _close: function (t) {
      t || (t = this.active ? this.active.parent() : this.element), t.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false");
    },
    _closeOnDocumentClick: function (e) {
      return !t(e.target).closest(".ui-menu").length;
    },
    _isDivider: function (t) {
      return !/[^\-\u2014\u2013\s]/.test(t.text());
    },
    collapse: function (t) {
      var e = this.active && this.active.parent().closest(".ui-menu-item", this.element);
      e && e.length && (this._close(), this.focus(t, e));
    },
    expand: function (t) {
      var e = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
      e && e.length && (this._open(e.parent()), this._delay(function () {
        this.focus(t, e);
      }));
    },
    next: function (t) {
      this._move("next", "first", t);
    },
    previous: function (t) {
      this._move("prev", "last", t);
    },
    isFirstItem: function () {
      return this.active && !this.active.prevAll(".ui-menu-item").length;
    },
    isLastItem: function () {
      return this.active && !this.active.nextAll(".ui-menu-item").length;
    },
    _move: function (t, e, i) {
      var s;
      this.active && (s = "first" === t || "last" === t ? this.active["first" === t ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[t + "All"](".ui-menu-item").eq(0)), s && s.length && this.active || (s = this.activeMenu.find(this.options.items)[e]()), this.focus(i, s);
    },
    nextPage: function (e) {
      var i, s, n;
      return this.active ? (this.isLastItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.nextAll(".ui-menu-item").each(function () {
        return i = t(this), 0 > i.offset().top - s - n;
      }), this.focus(e, i)) : this.focus(e, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]())), void 0) : (this.next(e), void 0);
    },
    previousPage: function (e) {
      var i, s, n;
      return this.active ? (this.isFirstItem() || (this._hasScroll() ? (s = this.active.offset().top, n = this.element.height(), this.active.prevAll(".ui-menu-item").each(function () {
        return i = t(this), i.offset().top - s + n > 0;
      }), this.focus(e, i)) : this.focus(e, this.activeMenu.find(this.options.items).first())), void 0) : (this.next(e), void 0);
    },
    _hasScroll: function () {
      return this.element.outerHeight() < this.element.prop("scrollHeight");
    },
    select: function (e) {
      this.active = this.active || t(e.target).closest(".ui-menu-item");
      var i = {
        item: this.active
      };
      this.active.has(".ui-menu").length || this.collapseAll(e, !0), this._trigger("select", e, i);
    },
    _filterMenuItems: function (e) {
      var i = e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"),
          s = RegExp("^" + i, "i");
      return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function () {
        return s.test(t.trim(t(this).children(".ui-menu-item-wrapper").text()));
      });
    }
  }), t.widget("ui.autocomplete", {
    version: "1.12.1",
    defaultElement: "<input>",
    options: {
      appendTo: null,
      autoFocus: !1,
      delay: 300,
      minLength: 1,
      position: {
        my: "left top",
        at: "left bottom",
        collision: "none"
      },
      source: null,
      change: null,
      close: null,
      focus: null,
      open: null,
      response: null,
      search: null,
      select: null
    },
    requestIndex: 0,
    pending: 0,
    _create: function () {
      var e,
          i,
          s,
          n = this.element[0].nodeName.toLowerCase(),
          o = "textarea" === n,
          a = "input" === n;
      this.isMultiLine = o || !a && this._isContentEditable(this.element), this.valueMethod = this.element[o || a ? "val" : "text"], this.isNewMenu = !0, this._addClass("ui-autocomplete-input"), this.element.attr("autocomplete", "off"), this._on(this.element, {
        keydown: function (n) {
          if (this.element.prop("readOnly")) return e = !0, s = !0, i = !0, void 0;
          e = !1, s = !1, i = !1;
          var o = t.ui.keyCode;

          switch (n.keyCode) {
            case o.PAGE_UP:
              e = !0, this._move("previousPage", n);
              break;

            case o.PAGE_DOWN:
              e = !0, this._move("nextPage", n);
              break;

            case o.UP:
              e = !0, this._keyEvent("previous", n);
              break;

            case o.DOWN:
              e = !0, this._keyEvent("next", n);
              break;

            case o.ENTER:
              this.menu.active && (e = !0, n.preventDefault(), this.menu.select(n));
              break;

            case o.TAB:
              this.menu.active && this.menu.select(n);
              break;

            case o.ESCAPE:
              this.menu.element.is(":visible") && (this.isMultiLine || this._value(this.term), this.close(n), n.preventDefault());
              break;

            default:
              i = !0, this._searchTimeout(n);
          }
        },
        keypress: function (s) {
          if (e) return e = !1, (!this.isMultiLine || this.menu.element.is(":visible")) && s.preventDefault(), void 0;

          if (!i) {
            var n = t.ui.keyCode;

            switch (s.keyCode) {
              case n.PAGE_UP:
                this._move("previousPage", s);

                break;

              case n.PAGE_DOWN:
                this._move("nextPage", s);

                break;

              case n.UP:
                this._keyEvent("previous", s);

                break;

              case n.DOWN:
                this._keyEvent("next", s);

            }
          }
        },
        input: function (t) {
          return s ? (s = !1, t.preventDefault(), void 0) : (this._searchTimeout(t), void 0);
        },
        focus: function () {
          this.selectedItem = null, this.previous = this._value();
        },
        blur: function (t) {
          return this.cancelBlur ? (delete this.cancelBlur, void 0) : (clearTimeout(this.searching), this.close(t), this._change(t), void 0);
        }
      }), this._initSource(), this.menu = t("<ul>").appendTo(this._appendTo()).menu({
        role: null
      }).hide().menu("instance"), this._addClass(this.menu.element, "ui-autocomplete", "ui-front"), this._on(this.menu.element, {
        mousedown: function (e) {
          e.preventDefault(), this.cancelBlur = !0, this._delay(function () {
            delete this.cancelBlur, this.element[0] !== t.ui.safeActiveElement(this.document[0]) && this.element.trigger("focus");
          });
        },
        menufocus: function (e, i) {
          var s, n;
          return this.isNewMenu && (this.isNewMenu = !1, e.originalEvent && /^mouse/.test(e.originalEvent.type)) ? (this.menu.blur(), this.document.one("mousemove", function () {
            t(e.target).trigger(e.originalEvent);
          }), void 0) : (n = i.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", e, {
            item: n
          }) && e.originalEvent && /^key/.test(e.originalEvent.type) && this._value(n.value), s = i.item.attr("aria-label") || n.value, s && t.trim(s).length && (this.liveRegion.children().hide(), t("<div>").text(s).appendTo(this.liveRegion)), void 0);
        },
        menuselect: function (e, i) {
          var s = i.item.data("ui-autocomplete-item"),
              n = this.previous;
          this.element[0] !== t.ui.safeActiveElement(this.document[0]) && (this.element.trigger("focus"), this.previous = n, this._delay(function () {
            this.previous = n, this.selectedItem = s;
          })), !1 !== this._trigger("select", e, {
            item: s
          }) && this._value(s.value), this.term = this._value(), this.close(e), this.selectedItem = s;
        }
      }), this.liveRegion = t("<div>", {
        role: "status",
        "aria-live": "assertive",
        "aria-relevant": "additions"
      }).appendTo(this.document[0].body), this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"), this._on(this.window, {
        beforeunload: function () {
          this.element.removeAttr("autocomplete");
        }
      });
    },
    _destroy: function () {
      clearTimeout(this.searching), this.element.removeAttr("autocomplete"), this.menu.element.remove(), this.liveRegion.remove();
    },
    _setOption: function (t, e) {
      this._super(t, e), "source" === t && this._initSource(), "appendTo" === t && this.menu.element.appendTo(this._appendTo()), "disabled" === t && e && this.xhr && this.xhr.abort();
    },
    _isEventTargetInWidget: function (e) {
      var i = this.menu.element[0];
      return e.target === this.element[0] || e.target === i || t.contains(i, e.target);
    },
    _closeOnClickOutside: function (t) {
      this._isEventTargetInWidget(t) || this.close();
    },
    _appendTo: function () {
      var e = this.options.appendTo;
      return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e && e[0] || (e = this.element.closest(".ui-front, dialog")), e.length || (e = this.document[0].body), e;
    },
    _initSource: function () {
      var e,
          i,
          s = this;
      t.isArray(this.options.source) ? (e = this.options.source, this.source = function (i, s) {
        s(t.ui.autocomplete.filter(e, i.term));
      }) : "string" == typeof this.options.source ? (i = this.options.source, this.source = function (e, n) {
        s.xhr && s.xhr.abort(), s.xhr = t.ajax({
          url: i,
          data: e,
          dataType: "json",
          success: function (t) {
            n(t);
          },
          error: function () {
            n([]);
          }
        });
      }) : this.source = this.options.source;
    },
    _searchTimeout: function (t) {
      clearTimeout(this.searching), this.searching = this._delay(function () {
        var e = this.term === this._value(),
            i = this.menu.element.is(":visible"),
            s = t.altKey || t.ctrlKey || t.metaKey || t.shiftKey;

        (!e || e && !i && !s) && (this.selectedItem = null, this.search(null, t));
      }, this.options.delay);
    },
    search: function (t, e) {
      return t = null != t ? t : this._value(), this.term = this._value(), t.length < this.options.minLength ? this.close(e) : this._trigger("search", e) !== !1 ? this._search(t) : void 0;
    },
    _search: function (t) {
      this.pending++, this._addClass("ui-autocomplete-loading"), this.cancelSearch = !1, this.source({
        term: t
      }, this._response());
    },
    _response: function () {
      var e = ++this.requestIndex;
      return t.proxy(function (t) {
        e === this.requestIndex && this.__response(t), this.pending--, this.pending || this._removeClass("ui-autocomplete-loading");
      }, this);
    },
    __response: function (t) {
      t && (t = this._normalize(t)), this._trigger("response", null, {
        content: t
      }), !this.options.disabled && t && t.length && !this.cancelSearch ? (this._suggest(t), this._trigger("open")) : this._close();
    },
    close: function (t) {
      this.cancelSearch = !0, this._close(t);
    },
    _close: function (t) {
      this._off(this.document, "mousedown"), this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", t));
    },
    _change: function (t) {
      this.previous !== this._value() && this._trigger("change", t, {
        item: this.selectedItem
      });
    },
    _normalize: function (e) {
      return e.length && e[0].label && e[0].value ? e : t.map(e, function (e) {
        return "string" == typeof e ? {
          label: e,
          value: e
        } : t.extend({}, e, {
          label: e.label || e.value,
          value: e.value || e.label
        });
      });
    },
    _suggest: function (e) {
      var i = this.menu.element.empty();
      this._renderMenu(i, e), this.isNewMenu = !0, this.menu.refresh(), i.show(), this._resizeMenu(), i.position(t.extend({
        of: this.element
      }, this.options.position)), this.options.autoFocus && this.menu.next(), this._on(this.document, {
        mousedown: "_closeOnClickOutside"
      });
    },
    _resizeMenu: function () {
      var t = this.menu.element;
      t.outerWidth(Math.max(t.width("").outerWidth() + 1, this.element.outerWidth()));
    },
    _renderMenu: function (e, i) {
      var s = this;
      t.each(i, function (t, i) {
        s._renderItemData(e, i);
      });
    },
    _renderItemData: function (t, e) {
      return this._renderItem(t, e).data("ui-autocomplete-item", e);
    },
    _renderItem: function (e, i) {
      return t("<li>").append(t("<div>").text(i.label)).appendTo(e);
    },
    _move: function (t, e) {
      return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(t) || this.menu.isLastItem() && /^next/.test(t) ? (this.isMultiLine || this._value(this.term), this.menu.blur(), void 0) : (this.menu[t](e), void 0) : (this.search(null, e), void 0);
    },
    widget: function () {
      return this.menu.element;
    },
    _value: function () {
      return this.valueMethod.apply(this.element, arguments);
    },
    _keyEvent: function (t, e) {
      (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(t, e), e.preventDefault());
    },
    _isContentEditable: function (t) {
      if (!t.length) return !1;
      var e = t.prop("contentEditable");
      return "inherit" === e ? this._isContentEditable(t.parent()) : "true" === e;
    }
  }), t.extend(t.ui.autocomplete, {
    escapeRegex: function (t) {
      return t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&");
    },
    filter: function (e, i) {
      var s = RegExp(t.ui.autocomplete.escapeRegex(i), "i");
      return t.grep(e, function (t) {
        return s.test(t.label || t.value || t);
      });
    }
  }), t.widget("ui.autocomplete", t.ui.autocomplete, {
    options: {
      messages: {
        noResults: "No search results.",
        results: function (t) {
          return t + (t > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate.";
        }
      }
    },
    __response: function (e) {
      var i;
      this._superApply(arguments), this.options.disabled || this.cancelSearch || (i = e && e.length ? this.options.messages.results(e.length) : this.options.messages.noResults, this.liveRegion.children().hide(), t("<div>").text(i).appendTo(this.liveRegion));
    }
  }), t.ui.autocomplete;
  var g = /ui-corner-([a-z]){2,6}/g;
  t.widget("ui.controlgroup", {
    version: "1.12.1",
    defaultElement: "<div>",
    options: {
      direction: "horizontal",
      disabled: null,
      onlyVisible: !0,
      items: {
        button: "input[type=button], input[type=submit], input[type=reset], button, a",
        controlgroupLabel: ".ui-controlgroup-label",
        checkboxradio: "input[type='checkbox'], input[type='radio']",
        selectmenu: "select",
        spinner: ".ui-spinner-input"
      }
    },
    _create: function () {
      this._enhance();
    },
    _enhance: function () {
      this.element.attr("role", "toolbar"), this.refresh();
    },
    _destroy: function () {
      this._callChildMethod("destroy"), this.childWidgets.removeData("ui-controlgroup-data"), this.element.removeAttr("role"), this.options.items.controlgroupLabel && this.element.find(this.options.items.controlgroupLabel).find(".ui-controlgroup-label-contents").contents().unwrap();
    },
    _initWidgets: function () {
      var e = this,
          i = [];
      t.each(this.options.items, function (s, n) {
        var o,
            a = {};
        return n ? "controlgroupLabel" === s ? (o = e.element.find(n), o.each(function () {
          var e = t(this);
          e.children(".ui-controlgroup-label-contents").length || e.contents().wrapAll("<span class='ui-controlgroup-label-contents'></span>");
        }), e._addClass(o, null, "ui-widget ui-widget-content ui-state-default"), i = i.concat(o.get()), void 0) : (t.fn[s] && (a = e["_" + s + "Options"] ? e["_" + s + "Options"]("middle") : {
          classes: {}
        }, e.element.find(n).each(function () {
          var n = t(this),
              o = n[s]("instance"),
              r = t.widget.extend({}, a);

          if ("button" !== s || !n.parent(".ui-spinner").length) {
            o || (o = n[s]()[s]("instance")), o && (r.classes = e._resolveClassesValues(r.classes, o)), n[s](r);
            var h = n[s]("widget");
            t.data(h[0], "ui-controlgroup-data", o ? o : n[s]("instance")), i.push(h[0]);
          }
        })), void 0) : void 0;
      }), this.childWidgets = t(t.unique(i)), this._addClass(this.childWidgets, "ui-controlgroup-item");
    },
    _callChildMethod: function (e) {
      this.childWidgets.each(function () {
        var i = t(this),
            s = i.data("ui-controlgroup-data");
        s && s[e] && s[e]();
      });
    },
    _updateCornerClass: function (t, e) {
      var i = "ui-corner-top ui-corner-bottom ui-corner-left ui-corner-right ui-corner-all",
          s = this._buildSimpleOptions(e, "label").classes.label;

      this._removeClass(t, null, i), this._addClass(t, null, s);
    },
    _buildSimpleOptions: function (t, e) {
      var i = "vertical" === this.options.direction,
          s = {
        classes: {}
      };
      return s.classes[e] = {
        middle: "",
        first: "ui-corner-" + (i ? "top" : "left"),
        last: "ui-corner-" + (i ? "bottom" : "right"),
        only: "ui-corner-all"
      }[t], s;
    },
    _spinnerOptions: function (t) {
      var e = this._buildSimpleOptions(t, "ui-spinner");

      return e.classes["ui-spinner-up"] = "", e.classes["ui-spinner-down"] = "", e;
    },
    _buttonOptions: function (t) {
      return this._buildSimpleOptions(t, "ui-button");
    },
    _checkboxradioOptions: function (t) {
      return this._buildSimpleOptions(t, "ui-checkboxradio-label");
    },
    _selectmenuOptions: function (t) {
      var e = "vertical" === this.options.direction;
      return {
        width: e ? "auto" : !1,
        classes: {
          middle: {
            "ui-selectmenu-button-open": "",
            "ui-selectmenu-button-closed": ""
          },
          first: {
            "ui-selectmenu-button-open": "ui-corner-" + (e ? "top" : "tl"),
            "ui-selectmenu-button-closed": "ui-corner-" + (e ? "top" : "left")
          },
          last: {
            "ui-selectmenu-button-open": e ? "" : "ui-corner-tr",
            "ui-selectmenu-button-closed": "ui-corner-" + (e ? "bottom" : "right")
          },
          only: {
            "ui-selectmenu-button-open": "ui-corner-top",
            "ui-selectmenu-button-closed": "ui-corner-all"
          }
        }[t]
      };
    },
    _resolveClassesValues: function (e, i) {
      var s = {};
      return t.each(e, function (n) {
        var o = i.options.classes[n] || "";
        o = t.trim(o.replace(g, "")), s[n] = (o + " " + e[n]).replace(/\s+/g, " ");
      }), s;
    },
    _setOption: function (t, e) {
      return "direction" === t && this._removeClass("ui-controlgroup-" + this.options.direction), this._super(t, e), "disabled" === t ? (this._callChildMethod(e ? "disable" : "enable"), void 0) : (this.refresh(), void 0);
    },
    refresh: function () {
      var e,
          i = this;
      this._addClass("ui-controlgroup ui-controlgroup-" + this.options.direction), "horizontal" === this.options.direction && this._addClass(null, "ui-helper-clearfix"), this._initWidgets(), e = this.childWidgets, this.options.onlyVisible && (e = e.filter(":visible")), e.length && (t.each(["first", "last"], function (t, s) {
        var n = e[s]().data("ui-controlgroup-data");

        if (n && i["_" + n.widgetName + "Options"]) {
          var o = i["_" + n.widgetName + "Options"](1 === e.length ? "only" : s);
          o.classes = i._resolveClassesValues(o.classes, n), n.element[n.widgetName](o);
        } else i._updateCornerClass(e[s](), s);
      }), this._callChildMethod("refresh"));
    }
  }), t.widget("ui.checkboxradio", [t.ui.formResetMixin, {
    version: "1.12.1",
    options: {
      disabled: null,
      label: null,
      icon: !0,
      classes: {
        "ui-checkboxradio-label": "ui-corner-all",
        "ui-checkboxradio-icon": "ui-corner-all"
      }
    },
    _getCreateOptions: function () {
      var e,
          i,
          s = this,
          n = this._super() || {};
      return this._readType(), i = this.element.labels(), this.label = t(i[i.length - 1]), this.label.length || t.error("No label found for checkboxradio widget"), this.originalLabel = "", this.label.contents().not(this.element[0]).each(function () {
        s.originalLabel += 3 === this.nodeType ? t(this).text() : this.outerHTML;
      }), this.originalLabel && (n.label = this.originalLabel), e = this.element[0].disabled, null != e && (n.disabled = e), n;
    },
    _create: function () {
      var t = this.element[0].checked;
      this._bindFormResetHandler(), null == this.options.disabled && (this.options.disabled = this.element[0].disabled), this._setOption("disabled", this.options.disabled), this._addClass("ui-checkboxradio", "ui-helper-hidden-accessible"), this._addClass(this.label, "ui-checkboxradio-label", "ui-button ui-widget"), "radio" === this.type && this._addClass(this.label, "ui-checkboxradio-radio-label"), this.options.label && this.options.label !== this.originalLabel ? this._updateLabel() : this.originalLabel && (this.options.label = this.originalLabel), this._enhance(), t && (this._addClass(this.label, "ui-checkboxradio-checked", "ui-state-active"), this.icon && this._addClass(this.icon, null, "ui-state-hover")), this._on({
        change: "_toggleClasses",
        focus: function () {
          this._addClass(this.label, null, "ui-state-focus ui-visual-focus");
        },
        blur: function () {
          this._removeClass(this.label, null, "ui-state-focus ui-visual-focus");
        }
      });
    },
    _readType: function () {
      var e = this.element[0].nodeName.toLowerCase();
      this.type = this.element[0].type, "input" === e && /radio|checkbox/.test(this.type) || t.error("Can't create checkboxradio on element.nodeName=" + e + " and element.type=" + this.type);
    },
    _enhance: function () {
      this._updateIcon(this.element[0].checked);
    },
    widget: function () {
      return this.label;
    },
    _getRadioGroup: function () {
      var e,
          i = this.element[0].name,
          s = "input[name='" + t.ui.escapeSelector(i) + "']";
      return i ? (e = this.form.length ? t(this.form[0].elements).filter(s) : t(s).filter(function () {
        return 0 === t(this).form().length;
      }), e.not(this.element)) : t([]);
    },
    _toggleClasses: function () {
      var e = this.element[0].checked;
      this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", e), this.options.icon && "checkbox" === this.type && this._toggleClass(this.icon, null, "ui-icon-check ui-state-checked", e)._toggleClass(this.icon, null, "ui-icon-blank", !e), "radio" === this.type && this._getRadioGroup().each(function () {
        var e = t(this).checkboxradio("instance");
        e && e._removeClass(e.label, "ui-checkboxradio-checked", "ui-state-active");
      });
    },
    _destroy: function () {
      this._unbindFormResetHandler(), this.icon && (this.icon.remove(), this.iconSpace.remove());
    },
    _setOption: function (t, e) {
      return "label" !== t || e ? (this._super(t, e), "disabled" === t ? (this._toggleClass(this.label, null, "ui-state-disabled", e), this.element[0].disabled = e, void 0) : (this.refresh(), void 0)) : void 0;
    },
    _updateIcon: function (e) {
      var i = "ui-icon ui-icon-background ";
      this.options.icon ? (this.icon || (this.icon = t("<span>"), this.iconSpace = t("<span> </span>"), this._addClass(this.iconSpace, "ui-checkboxradio-icon-space")), "checkbox" === this.type ? (i += e ? "ui-icon-check ui-state-checked" : "ui-icon-blank", this._removeClass(this.icon, null, e ? "ui-icon-blank" : "ui-icon-check")) : i += "ui-icon-blank", this._addClass(this.icon, "ui-checkboxradio-icon", i), e || this._removeClass(this.icon, null, "ui-icon-check ui-state-checked"), this.icon.prependTo(this.label).after(this.iconSpace)) : void 0 !== this.icon && (this.icon.remove(), this.iconSpace.remove(), delete this.icon);
    },
    _updateLabel: function () {
      var t = this.label.contents().not(this.element[0]);
      this.icon && (t = t.not(this.icon[0])), this.iconSpace && (t = t.not(this.iconSpace[0])), t.remove(), this.label.append(this.options.label);
    },
    refresh: function () {
      var t = this.element[0].checked,
          e = this.element[0].disabled;
      this._updateIcon(t), this._toggleClass(this.label, "ui-checkboxradio-checked", "ui-state-active", t), null !== this.options.label && this._updateLabel(), e !== this.options.disabled && this._setOptions({
        disabled: e
      });
    }
  }]), t.ui.checkboxradio, t.widget("ui.button", {
    version: "1.12.1",
    defaultElement: "<button>",
    options: {
      classes: {
        "ui-button": "ui-corner-all"
      },
      disabled: null,
      icon: null,
      iconPosition: "beginning",
      label: null,
      showLabel: !0
    },
    _getCreateOptions: function () {
      var t,
          e = this._super() || {};
      return this.isInput = this.element.is("input"), t = this.element[0].disabled, null != t && (e.disabled = t), this.originalLabel = this.isInput ? this.element.val() : this.element.html(), this.originalLabel && (e.label = this.originalLabel), e;
    },
    _create: function () {
      !this.option.showLabel & !this.options.icon && (this.options.showLabel = !0), null == this.options.disabled && (this.options.disabled = this.element[0].disabled || !1), this.hasTitle = !!this.element.attr("title"), this.options.label && this.options.label !== this.originalLabel && (this.isInput ? this.element.val(this.options.label) : this.element.html(this.options.label)), this._addClass("ui-button", "ui-widget"), this._setOption("disabled", this.options.disabled), this._enhance(), this.element.is("a") && this._on({
        keyup: function (e) {
          e.keyCode === t.ui.keyCode.SPACE && (e.preventDefault(), this.element[0].click ? this.element[0].click() : this.element.trigger("click"));
        }
      });
    },
    _enhance: function () {
      this.element.is("button") || this.element.attr("role", "button"), this.options.icon && (this._updateIcon("icon", this.options.icon), this._updateTooltip());
    },
    _updateTooltip: function () {
      this.title = this.element.attr("title"), this.options.showLabel || this.title || this.element.attr("title", this.options.label);
    },
    _updateIcon: function (e, i) {
      var s = "iconPosition" !== e,
          n = s ? this.options.iconPosition : i,
          o = "top" === n || "bottom" === n;
      this.icon ? s && this._removeClass(this.icon, null, this.options.icon) : (this.icon = t("<span>"), this._addClass(this.icon, "ui-button-icon", "ui-icon"), this.options.showLabel || this._addClass("ui-button-icon-only")), s && this._addClass(this.icon, null, i), this._attachIcon(n), o ? (this._addClass(this.icon, null, "ui-widget-icon-block"), this.iconSpace && this.iconSpace.remove()) : (this.iconSpace || (this.iconSpace = t("<span> </span>"), this._addClass(this.iconSpace, "ui-button-icon-space")), this._removeClass(this.icon, null, "ui-wiget-icon-block"), this._attachIconSpace(n));
    },
    _destroy: function () {
      this.element.removeAttr("role"), this.icon && this.icon.remove(), this.iconSpace && this.iconSpace.remove(), this.hasTitle || this.element.removeAttr("title");
    },
    _attachIconSpace: function (t) {
      this.icon[/^(?:end|bottom)/.test(t) ? "before" : "after"](this.iconSpace);
    },
    _attachIcon: function (t) {
      this.element[/^(?:end|bottom)/.test(t) ? "append" : "prepend"](this.icon);
    },
    _setOptions: function (t) {
      var e = void 0 === t.showLabel ? this.options.showLabel : t.showLabel,
          i = void 0 === t.icon ? this.options.icon : t.icon;
      e || i || (t.showLabel = !0), this._super(t);
    },
    _setOption: function (t, e) {
      "icon" === t && (e ? this._updateIcon(t, e) : this.icon && (this.icon.remove(), this.iconSpace && this.iconSpace.remove())), "iconPosition" === t && this._updateIcon(t, e), "showLabel" === t && (this._toggleClass("ui-button-icon-only", null, !e), this._updateTooltip()), "label" === t && (this.isInput ? this.element.val(e) : (this.element.html(e), this.icon && (this._attachIcon(this.options.iconPosition), this._attachIconSpace(this.options.iconPosition)))), this._super(t, e), "disabled" === t && (this._toggleClass(null, "ui-state-disabled", e), this.element[0].disabled = e, e && this.element.blur());
    },
    refresh: function () {
      var t = this.element.is("input, button") ? this.element[0].disabled : this.element.hasClass("ui-button-disabled");
      t !== this.options.disabled && this._setOptions({
        disabled: t
      }), this._updateTooltip();
    }
  }), t.uiBackCompat !== !1 && (t.widget("ui.button", t.ui.button, {
    options: {
      text: !0,
      icons: {
        primary: null,
        secondary: null
      }
    },
    _create: function () {
      this.options.showLabel && !this.options.text && (this.options.showLabel = this.options.text), !this.options.showLabel && this.options.text && (this.options.text = this.options.showLabel), this.options.icon || !this.options.icons.primary && !this.options.icons.secondary ? this.options.icon && (this.options.icons.primary = this.options.icon) : this.options.icons.primary ? this.options.icon = this.options.icons.primary : (this.options.icon = this.options.icons.secondary, this.options.iconPosition = "end"), this._super();
    },
    _setOption: function (t, e) {
      return "text" === t ? (this._super("showLabel", e), void 0) : ("showLabel" === t && (this.options.text = e), "icon" === t && (this.options.icons.primary = e), "icons" === t && (e.primary ? (this._super("icon", e.primary), this._super("iconPosition", "beginning")) : e.secondary && (this._super("icon", e.secondary), this._super("iconPosition", "end"))), this._superApply(arguments), void 0);
    }
  }), t.fn.button = function (e) {
    return function () {
      return !this.length || this.length && "INPUT" !== this[0].tagName || this.length && "INPUT" === this[0].tagName && "checkbox" !== this.attr("type") && "radio" !== this.attr("type") ? e.apply(this, arguments) : (t.ui.checkboxradio || t.error("Checkboxradio widget missing"), 0 === arguments.length ? this.checkboxradio({
        icon: !1
      }) : this.checkboxradio.apply(this, arguments));
    };
  }(t.fn.button), t.fn.buttonset = function () {
    return t.ui.controlgroup || t.error("Controlgroup widget missing"), "option" === arguments[0] && "items" === arguments[1] && arguments[2] ? this.controlgroup.apply(this, [arguments[0], "items.button", arguments[2]]) : "option" === arguments[0] && "items" === arguments[1] ? this.controlgroup.apply(this, [arguments[0], "items.button"]) : ("object" == typeof arguments[0] && arguments[0].items && (arguments[0].items = {
      button: arguments[0].items
    }), this.controlgroup.apply(this, arguments));
  }), t.ui.button, t.extend(t.ui, {
    datepicker: {
      version: "1.12.1"
    }
  });
  var m;
  t.extend(s.prototype, {
    markerClassName: "hasDatepicker",
    maxRows: 4,
    _widgetDatepicker: function () {
      return this.dpDiv;
    },
    setDefaults: function (t) {
      return a(this._defaults, t || {}), this;
    },
    _attachDatepicker: function (e, i) {
      var s, n, o;
      s = e.nodeName.toLowerCase(), n = "div" === s || "span" === s, e.id || (this.uuid += 1, e.id = "dp" + this.uuid), o = this._newInst(t(e), n), o.settings = t.extend({}, i || {}), "input" === s ? this._connectDatepicker(e, o) : n && this._inlineDatepicker(e, o);
    },
    _newInst: function (e, i) {
      var s = e[0].id.replace(/([^A-Za-z0-9_\-])/g, "\\\\$1");
      return {
        id: s,
        input: e,
        selectedDay: 0,
        selectedMonth: 0,
        selectedYear: 0,
        drawMonth: 0,
        drawYear: 0,
        inline: i,
        dpDiv: i ? n(t("<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")) : this.dpDiv
      };
    },
    _connectDatepicker: function (e, i) {
      var s = t(e);
      i.append = t([]), i.trigger = t([]), s.hasClass(this.markerClassName) || (this._attachments(s, i), s.addClass(this.markerClassName).on("keydown", this._doKeyDown).on("keypress", this._doKeyPress).on("keyup", this._doKeyUp), this._autoSize(i), t.data(e, "datepicker", i), i.settings.disabled && this._disableDatepicker(e));
    },
    _attachments: function (e, i) {
      var s,
          n,
          o,
          a = this._get(i, "appendText"),
          r = this._get(i, "isRTL");

      i.append && i.append.remove(), a && (i.append = t("<span class='" + this._appendClass + "'>" + a + "</span>"), e[r ? "before" : "after"](i.append)), e.off("focus", this._showDatepicker), i.trigger && i.trigger.remove(), s = this._get(i, "showOn"), ("focus" === s || "both" === s) && e.on("focus", this._showDatepicker), ("button" === s || "both" === s) && (n = this._get(i, "buttonText"), o = this._get(i, "buttonImage"), i.trigger = t(this._get(i, "buttonImageOnly") ? t("<img/>").addClass(this._triggerClass).attr({
        src: o,
        alt: n,
        title: n
      }) : t("<button type='button'></button>").addClass(this._triggerClass).html(o ? t("<img/>").attr({
        src: o,
        alt: n,
        title: n
      }) : n)), e[r ? "before" : "after"](i.trigger), i.trigger.on("click", function () {
        return t.datepicker._datepickerShowing && t.datepicker._lastInput === e[0] ? t.datepicker._hideDatepicker() : t.datepicker._datepickerShowing && t.datepicker._lastInput !== e[0] ? (t.datepicker._hideDatepicker(), t.datepicker._showDatepicker(e[0])) : t.datepicker._showDatepicker(e[0]), !1;
      }));
    },
    _autoSize: function (t) {
      if (this._get(t, "autoSize") && !t.inline) {
        var e,
            i,
            s,
            n,
            o = new Date(2009, 11, 20),
            a = this._get(t, "dateFormat");

        a.match(/[DM]/) && (e = function (t) {
          for (i = 0, s = 0, n = 0; t.length > n; n++) t[n].length > i && (i = t[n].length, s = n);

          return s;
        }, o.setMonth(e(this._get(t, a.match(/MM/) ? "monthNames" : "monthNamesShort"))), o.setDate(e(this._get(t, a.match(/DD/) ? "dayNames" : "dayNamesShort")) + 20 - o.getDay())), t.input.attr("size", this._formatDate(t, o).length);
      }
    },
    _inlineDatepicker: function (e, i) {
      var s = t(e);
      s.hasClass(this.markerClassName) || (s.addClass(this.markerClassName).append(i.dpDiv), t.data(e, "datepicker", i), this._setDate(i, this._getDefaultDate(i), !0), this._updateDatepicker(i), this._updateAlternate(i), i.settings.disabled && this._disableDatepicker(e), i.dpDiv.css("display", "block"));
    },
    _dialogDatepicker: function (e, i, s, n, o) {
      var r,
          h,
          l,
          c,
          u,
          d = this._dialogInst;
      return d || (this.uuid += 1, r = "dp" + this.uuid, this._dialogInput = t("<input type='text' id='" + r + "' style='position: absolute; top: -100px; width: 0px;'/>"), this._dialogInput.on("keydown", this._doKeyDown), t("body").append(this._dialogInput), d = this._dialogInst = this._newInst(this._dialogInput, !1), d.settings = {}, t.data(this._dialogInput[0], "datepicker", d)), a(d.settings, n || {}), i = i && i.constructor === Date ? this._formatDate(d, i) : i, this._dialogInput.val(i), this._pos = o ? o.length ? o : [o.pageX, o.pageY] : null, this._pos || (h = document.documentElement.clientWidth, l = document.documentElement.clientHeight, c = document.documentElement.scrollLeft || document.body.scrollLeft, u = document.documentElement.scrollTop || document.body.scrollTop, this._pos = [h / 2 - 100 + c, l / 2 - 150 + u]), this._dialogInput.css("left", this._pos[0] + 20 + "px").css("top", this._pos[1] + "px"), d.settings.onSelect = s, this._inDialog = !0, this.dpDiv.addClass(this._dialogClass), this._showDatepicker(this._dialogInput[0]), t.blockUI && t.blockUI(this.dpDiv), t.data(this._dialogInput[0], "datepicker", d), this;
    },
    _destroyDatepicker: function (e) {
      var i,
          s = t(e),
          n = t.data(e, "datepicker");
      s.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), t.removeData(e, "datepicker"), "input" === i ? (n.append.remove(), n.trigger.remove(), s.removeClass(this.markerClassName).off("focus", this._showDatepicker).off("keydown", this._doKeyDown).off("keypress", this._doKeyPress).off("keyup", this._doKeyUp)) : ("div" === i || "span" === i) && s.removeClass(this.markerClassName).empty(), m === n && (m = null));
    },
    _enableDatepicker: function (e) {
      var i,
          s,
          n = t(e),
          o = t.data(e, "datepicker");
      n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !1, o.trigger.filter("button").each(function () {
        this.disabled = !1;
      }).end().filter("img").css({
        opacity: "1.0",
        cursor: ""
      })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().removeClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !1)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
        return t === e ? null : t;
      }));
    },
    _disableDatepicker: function (e) {
      var i,
          s,
          n = t(e),
          o = t.data(e, "datepicker");
      n.hasClass(this.markerClassName) && (i = e.nodeName.toLowerCase(), "input" === i ? (e.disabled = !0, o.trigger.filter("button").each(function () {
        this.disabled = !0;
      }).end().filter("img").css({
        opacity: "0.5",
        cursor: "default"
      })) : ("div" === i || "span" === i) && (s = n.children("." + this._inlineClass), s.children().addClass("ui-state-disabled"), s.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled", !0)), this._disabledInputs = t.map(this._disabledInputs, function (t) {
        return t === e ? null : t;
      }), this._disabledInputs[this._disabledInputs.length] = e);
    },
    _isDisabledDatepicker: function (t) {
      if (!t) return !1;

      for (var e = 0; this._disabledInputs.length > e; e++) if (this._disabledInputs[e] === t) return !0;

      return !1;
    },
    _getInst: function (e) {
      try {
        return t.data(e, "datepicker");
      } catch (i) {
        throw "Missing instance data for this datepicker";
      }
    },
    _optionDatepicker: function (e, i, s) {
      var n,
          o,
          r,
          h,
          l = this._getInst(e);

      return 2 === arguments.length && "string" == typeof i ? "defaults" === i ? t.extend({}, t.datepicker._defaults) : l ? "all" === i ? t.extend({}, l.settings) : this._get(l, i) : null : (n = i || {}, "string" == typeof i && (n = {}, n[i] = s), l && (this._curInst === l && this._hideDatepicker(), o = this._getDateDatepicker(e, !0), r = this._getMinMaxDate(l, "min"), h = this._getMinMaxDate(l, "max"), a(l.settings, n), null !== r && void 0 !== n.dateFormat && void 0 === n.minDate && (l.settings.minDate = this._formatDate(l, r)), null !== h && void 0 !== n.dateFormat && void 0 === n.maxDate && (l.settings.maxDate = this._formatDate(l, h)), "disabled" in n && (n.disabled ? this._disableDatepicker(e) : this._enableDatepicker(e)), this._attachments(t(e), l), this._autoSize(l), this._setDate(l, o), this._updateAlternate(l), this._updateDatepicker(l)), void 0);
    },
    _changeDatepicker: function (t, e, i) {
      this._optionDatepicker(t, e, i);
    },
    _refreshDatepicker: function (t) {
      var e = this._getInst(t);

      e && this._updateDatepicker(e);
    },
    _setDateDatepicker: function (t, e) {
      var i = this._getInst(t);

      i && (this._setDate(i, e), this._updateDatepicker(i), this._updateAlternate(i));
    },
    _getDateDatepicker: function (t, e) {
      var i = this._getInst(t);

      return i && !i.inline && this._setDateFromField(i, e), i ? this._getDate(i) : null;
    },
    _doKeyDown: function (e) {
      var i,
          s,
          n,
          o = t.datepicker._getInst(e.target),
          a = !0,
          r = o.dpDiv.is(".ui-datepicker-rtl");

      if (o._keyEvent = !0, t.datepicker._datepickerShowing) switch (e.keyCode) {
        case 9:
          t.datepicker._hideDatepicker(), a = !1;
          break;

        case 13:
          return n = t("td." + t.datepicker._dayOverClass + ":not(." + t.datepicker._currentClass + ")", o.dpDiv), n[0] && t.datepicker._selectDay(e.target, o.selectedMonth, o.selectedYear, n[0]), i = t.datepicker._get(o, "onSelect"), i ? (s = t.datepicker._formatDate(o), i.apply(o.input ? o.input[0] : null, [s, o])) : t.datepicker._hideDatepicker(), !1;

        case 27:
          t.datepicker._hideDatepicker();

          break;

        case 33:
          t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");

          break;

        case 34:
          t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");

          break;

        case 35:
          (e.ctrlKey || e.metaKey) && t.datepicker._clearDate(e.target), a = e.ctrlKey || e.metaKey;
          break;

        case 36:
          (e.ctrlKey || e.metaKey) && t.datepicker._gotoToday(e.target), a = e.ctrlKey || e.metaKey;
          break;

        case 37:
          (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? 1 : -1, "D"), a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? -t.datepicker._get(o, "stepBigMonths") : -t.datepicker._get(o, "stepMonths"), "M");
          break;

        case 38:
          (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, -7, "D"), a = e.ctrlKey || e.metaKey;
          break;

        case 39:
          (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, r ? -1 : 1, "D"), a = e.ctrlKey || e.metaKey, e.originalEvent.altKey && t.datepicker._adjustDate(e.target, e.ctrlKey ? +t.datepicker._get(o, "stepBigMonths") : +t.datepicker._get(o, "stepMonths"), "M");
          break;

        case 40:
          (e.ctrlKey || e.metaKey) && t.datepicker._adjustDate(e.target, 7, "D"), a = e.ctrlKey || e.metaKey;
          break;

        default:
          a = !1;
      } else 36 === e.keyCode && e.ctrlKey ? t.datepicker._showDatepicker(this) : a = !1;
      a && (e.preventDefault(), e.stopPropagation());
    },
    _doKeyPress: function (e) {
      var i,
          s,
          n = t.datepicker._getInst(e.target);

      return t.datepicker._get(n, "constrainInput") ? (i = t.datepicker._possibleChars(t.datepicker._get(n, "dateFormat")), s = String.fromCharCode(null == e.charCode ? e.keyCode : e.charCode), e.ctrlKey || e.metaKey || " " > s || !i || i.indexOf(s) > -1) : void 0;
    },
    _doKeyUp: function (e) {
      var i,
          s = t.datepicker._getInst(e.target);

      if (s.input.val() !== s.lastVal) try {
        i = t.datepicker.parseDate(t.datepicker._get(s, "dateFormat"), s.input ? s.input.val() : null, t.datepicker._getFormatConfig(s)), i && (t.datepicker._setDateFromField(s), t.datepicker._updateAlternate(s), t.datepicker._updateDatepicker(s));
      } catch (n) {}
      return !0;
    },
    _showDatepicker: function (e) {
      if (e = e.target || e, "input" !== e.nodeName.toLowerCase() && (e = t("input", e.parentNode)[0]), !t.datepicker._isDisabledDatepicker(e) && t.datepicker._lastInput !== e) {
        var s, n, o, r, h, l, c;
        s = t.datepicker._getInst(e), t.datepicker._curInst && t.datepicker._curInst !== s && (t.datepicker._curInst.dpDiv.stop(!0, !0), s && t.datepicker._datepickerShowing && t.datepicker._hideDatepicker(t.datepicker._curInst.input[0])), n = t.datepicker._get(s, "beforeShow"), o = n ? n.apply(e, [e, s]) : {}, o !== !1 && (a(s.settings, o), s.lastVal = null, t.datepicker._lastInput = e, t.datepicker._setDateFromField(s), t.datepicker._inDialog && (e.value = ""), t.datepicker._pos || (t.datepicker._pos = t.datepicker._findPos(e), t.datepicker._pos[1] += e.offsetHeight), r = !1, t(e).parents().each(function () {
          return r |= "fixed" === t(this).css("position"), !r;
        }), h = {
          left: t.datepicker._pos[0],
          top: t.datepicker._pos[1]
        }, t.datepicker._pos = null, s.dpDiv.empty(), s.dpDiv.css({
          position: "absolute",
          display: "block",
          top: "-1000px"
        }), t.datepicker._updateDatepicker(s), h = t.datepicker._checkOffset(s, h, r), s.dpDiv.css({
          position: t.datepicker._inDialog && t.blockUI ? "static" : r ? "fixed" : "absolute",
          display: "none",
          left: h.left + "px",
          top: h.top + "px"
        }), s.inline || (l = t.datepicker._get(s, "showAnim"), c = t.datepicker._get(s, "duration"), s.dpDiv.css("z-index", i(t(e)) + 1), t.datepicker._datepickerShowing = !0, t.effects && t.effects.effect[l] ? s.dpDiv.show(l, t.datepicker._get(s, "showOptions"), c) : s.dpDiv[l || "show"](l ? c : null), t.datepicker._shouldFocusInput(s) && s.input.trigger("focus"), t.datepicker._curInst = s));
      }
    },
    _updateDatepicker: function (e) {
      this.maxRows = 4, m = e, e.dpDiv.empty().append(this._generateHTML(e)), this._attachHandlers(e);

      var i,
          s = this._getNumberOfMonths(e),
          n = s[1],
          a = 17,
          r = e.dpDiv.find("." + this._dayOverClass + " a");

      r.length > 0 && o.apply(r.get(0)), e.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""), n > 1 && e.dpDiv.addClass("ui-datepicker-multi-" + n).css("width", a * n + "em"), e.dpDiv[(1 !== s[0] || 1 !== s[1] ? "add" : "remove") + "Class"]("ui-datepicker-multi"), e.dpDiv[(this._get(e, "isRTL") ? "add" : "remove") + "Class"]("ui-datepicker-rtl"), e === t.datepicker._curInst && t.datepicker._datepickerShowing && t.datepicker._shouldFocusInput(e) && e.input.trigger("focus"), e.yearshtml && (i = e.yearshtml, setTimeout(function () {
        i === e.yearshtml && e.yearshtml && e.dpDiv.find("select.ui-datepicker-year:first").replaceWith(e.yearshtml), i = e.yearshtml = null;
      }, 0));
    },
    _shouldFocusInput: function (t) {
      return t.input && t.input.is(":visible") && !t.input.is(":disabled") && !t.input.is(":focus");
    },
    _checkOffset: function (e, i, s) {
      var n = e.dpDiv.outerWidth(),
          o = e.dpDiv.outerHeight(),
          a = e.input ? e.input.outerWidth() : 0,
          r = e.input ? e.input.outerHeight() : 0,
          h = document.documentElement.clientWidth + (s ? 0 : t(document).scrollLeft()),
          l = document.documentElement.clientHeight + (s ? 0 : t(document).scrollTop());
      return i.left -= this._get(e, "isRTL") ? n - a : 0, i.left -= s && i.left === e.input.offset().left ? t(document).scrollLeft() : 0, i.top -= s && i.top === e.input.offset().top + r ? t(document).scrollTop() : 0, i.left -= Math.min(i.left, i.left + n > h && h > n ? Math.abs(i.left + n - h) : 0), i.top -= Math.min(i.top, i.top + o > l && l > o ? Math.abs(o + r) : 0), i;
    },
    _findPos: function (e) {
      for (var i, s = this._getInst(e), n = this._get(s, "isRTL"); e && ("hidden" === e.type || 1 !== e.nodeType || t.expr.filters.hidden(e));) e = e[n ? "previousSibling" : "nextSibling"];

      return i = t(e).offset(), [i.left, i.top];
    },
    _hideDatepicker: function (e) {
      var i,
          s,
          n,
          o,
          a = this._curInst;
      !a || e && a !== t.data(e, "datepicker") || this._datepickerShowing && (i = this._get(a, "showAnim"), s = this._get(a, "duration"), n = function () {
        t.datepicker._tidyDialog(a);
      }, t.effects && (t.effects.effect[i] || t.effects[i]) ? a.dpDiv.hide(i, t.datepicker._get(a, "showOptions"), s, n) : a.dpDiv["slideDown" === i ? "slideUp" : "fadeIn" === i ? "fadeOut" : "hide"](i ? s : null, n), i || n(), this._datepickerShowing = !1, o = this._get(a, "onClose"), o && o.apply(a.input ? a.input[0] : null, [a.input ? a.input.val() : "", a]), this._lastInput = null, this._inDialog && (this._dialogInput.css({
        position: "absolute",
        left: "0",
        top: "-100px"
      }), t.blockUI && (t.unblockUI(), t("body").append(this.dpDiv))), this._inDialog = !1);
    },
    _tidyDialog: function (t) {
      t.dpDiv.removeClass(this._dialogClass).off(".ui-datepicker-calendar");
    },
    _checkExternalClick: function (e) {
      if (t.datepicker._curInst) {
        var i = t(e.target),
            s = t.datepicker._getInst(i[0]);

        (i[0].id !== t.datepicker._mainDivId && 0 === i.parents("#" + t.datepicker._mainDivId).length && !i.hasClass(t.datepicker.markerClassName) && !i.closest("." + t.datepicker._triggerClass).length && t.datepicker._datepickerShowing && (!t.datepicker._inDialog || !t.blockUI) || i.hasClass(t.datepicker.markerClassName) && t.datepicker._curInst !== s) && t.datepicker._hideDatepicker();
      }
    },
    _adjustDate: function (e, i, s) {
      var n = t(e),
          o = this._getInst(n[0]);

      this._isDisabledDatepicker(n[0]) || (this._adjustInstDate(o, i + ("M" === s ? this._get(o, "showCurrentAtPos") : 0), s), this._updateDatepicker(o));
    },
    _gotoToday: function (e) {
      var i,
          s = t(e),
          n = this._getInst(s[0]);

      this._get(n, "gotoCurrent") && n.currentDay ? (n.selectedDay = n.currentDay, n.drawMonth = n.selectedMonth = n.currentMonth, n.drawYear = n.selectedYear = n.currentYear) : (i = new Date(), n.selectedDay = i.getDate(), n.drawMonth = n.selectedMonth = i.getMonth(), n.drawYear = n.selectedYear = i.getFullYear()), this._notifyChange(n), this._adjustDate(s);
    },
    _selectMonthYear: function (e, i, s) {
      var n = t(e),
          o = this._getInst(n[0]);

      o["selected" + ("M" === s ? "Month" : "Year")] = o["draw" + ("M" === s ? "Month" : "Year")] = parseInt(i.options[i.selectedIndex].value, 10), this._notifyChange(o), this._adjustDate(n);
    },
    _selectDay: function (e, i, s, n) {
      var o,
          a = t(e);
      t(n).hasClass(this._unselectableClass) || this._isDisabledDatepicker(a[0]) || (o = this._getInst(a[0]), o.selectedDay = o.currentDay = t("a", n).html(), o.selectedMonth = o.currentMonth = i, o.selectedYear = o.currentYear = s, this._selectDate(e, this._formatDate(o, o.currentDay, o.currentMonth, o.currentYear)));
    },
    _clearDate: function (e) {
      var i = t(e);

      this._selectDate(i, "");
    },
    _selectDate: function (e, i) {
      var s,
          n = t(e),
          o = this._getInst(n[0]);

      i = null != i ? i : this._formatDate(o), o.input && o.input.val(i), this._updateAlternate(o), s = this._get(o, "onSelect"), s ? s.apply(o.input ? o.input[0] : null, [i, o]) : o.input && o.input.trigger("change"), o.inline ? this._updateDatepicker(o) : (this._hideDatepicker(), this._lastInput = o.input[0], "object" != typeof o.input[0] && o.input.trigger("focus"), this._lastInput = null);
    },
    _updateAlternate: function (e) {
      var i,
          s,
          n,
          o = this._get(e, "altField");

      o && (i = this._get(e, "altFormat") || this._get(e, "dateFormat"), s = this._getDate(e), n = this.formatDate(i, s, this._getFormatConfig(e)), t(o).val(n));
    },
    noWeekends: function (t) {
      var e = t.getDay();
      return [e > 0 && 6 > e, ""];
    },
    iso8601Week: function (t) {
      var e,
          i = new Date(t.getTime());
      return i.setDate(i.getDate() + 4 - (i.getDay() || 7)), e = i.getTime(), i.setMonth(0), i.setDate(1), Math.floor(Math.round((e - i) / 864e5) / 7) + 1;
    },
    parseDate: function (e, i, s) {
      if (null == e || null == i) throw "Invalid arguments";
      if (i = "object" == typeof i ? "" + i : i + "", "" === i) return null;

      var n,
          o,
          a,
          r,
          h = 0,
          l = (s ? s.shortYearCutoff : null) || this._defaults.shortYearCutoff,
          c = "string" != typeof l ? l : new Date().getFullYear() % 100 + parseInt(l, 10),
          u = (s ? s.dayNamesShort : null) || this._defaults.dayNamesShort,
          d = (s ? s.dayNames : null) || this._defaults.dayNames,
          p = (s ? s.monthNamesShort : null) || this._defaults.monthNamesShort,
          f = (s ? s.monthNames : null) || this._defaults.monthNames,
          g = -1,
          m = -1,
          _ = -1,
          v = -1,
          b = !1,
          y = function (t) {
        var i = e.length > n + 1 && e.charAt(n + 1) === t;
        return i && n++, i;
      },
          w = function (t) {
        var e = y(t),
            s = "@" === t ? 14 : "!" === t ? 20 : "y" === t && e ? 4 : "o" === t ? 3 : 2,
            n = "y" === t ? s : 1,
            o = RegExp("^\\d{" + n + "," + s + "}"),
            a = i.substring(h).match(o);
        if (!a) throw "Missing number at position " + h;
        return h += a[0].length, parseInt(a[0], 10);
      },
          k = function (e, s, n) {
        var o = -1,
            a = t.map(y(e) ? n : s, function (t, e) {
          return [[e, t]];
        }).sort(function (t, e) {
          return -(t[1].length - e[1].length);
        });
        if (t.each(a, function (t, e) {
          var s = e[1];
          return i.substr(h, s.length).toLowerCase() === s.toLowerCase() ? (o = e[0], h += s.length, !1) : void 0;
        }), -1 !== o) return o + 1;
        throw "Unknown name at position " + h;
      },
          x = function () {
        if (i.charAt(h) !== e.charAt(n)) throw "Unexpected literal at position " + h;
        h++;
      };

      for (n = 0; e.length > n; n++) if (b) "'" !== e.charAt(n) || y("'") ? x() : b = !1;else switch (e.charAt(n)) {
        case "d":
          _ = w("d");
          break;

        case "D":
          k("D", u, d);
          break;

        case "o":
          v = w("o");
          break;

        case "m":
          m = w("m");
          break;

        case "M":
          m = k("M", p, f);
          break;

        case "y":
          g = w("y");
          break;

        case "@":
          r = new Date(w("@")), g = r.getFullYear(), m = r.getMonth() + 1, _ = r.getDate();
          break;

        case "!":
          r = new Date((w("!") - this._ticksTo1970) / 1e4), g = r.getFullYear(), m = r.getMonth() + 1, _ = r.getDate();
          break;

        case "'":
          y("'") ? x() : b = !0;
          break;

        default:
          x();
      }

      if (i.length > h && (a = i.substr(h), !/^\s+/.test(a))) throw "Extra/unparsed characters found in date: " + a;
      if (-1 === g ? g = new Date().getFullYear() : 100 > g && (g += new Date().getFullYear() - new Date().getFullYear() % 100 + (c >= g ? 0 : -100)), v > -1) for (m = 1, _ = v;;) {
        if (o = this._getDaysInMonth(g, m - 1), o >= _) break;
        m++, _ -= o;
      }
      if (r = this._daylightSavingAdjust(new Date(g, m - 1, _)), r.getFullYear() !== g || r.getMonth() + 1 !== m || r.getDate() !== _) throw "Invalid date";
      return r;
    },
    ATOM: "yy-mm-dd",
    COOKIE: "D, dd M yy",
    ISO_8601: "yy-mm-dd",
    RFC_822: "D, d M y",
    RFC_850: "DD, dd-M-y",
    RFC_1036: "D, d M y",
    RFC_1123: "D, d M yy",
    RFC_2822: "D, d M yy",
    RSS: "D, d M y",
    TICKS: "!",
    TIMESTAMP: "@",
    W3C: "yy-mm-dd",
    _ticksTo1970: 1e7 * 60 * 60 * 24 * (718685 + Math.floor(492.5) - Math.floor(19.7) + Math.floor(4.925)),
    formatDate: function (t, e, i) {
      if (!e) return "";

      var s,
          n = (i ? i.dayNamesShort : null) || this._defaults.dayNamesShort,
          o = (i ? i.dayNames : null) || this._defaults.dayNames,
          a = (i ? i.monthNamesShort : null) || this._defaults.monthNamesShort,
          r = (i ? i.monthNames : null) || this._defaults.monthNames,
          h = function (e) {
        var i = t.length > s + 1 && t.charAt(s + 1) === e;
        return i && s++, i;
      },
          l = function (t, e, i) {
        var s = "" + e;
        if (h(t)) for (; i > s.length;) s = "0" + s;
        return s;
      },
          c = function (t, e, i, s) {
        return h(t) ? s[e] : i[e];
      },
          u = "",
          d = !1;

      if (e) for (s = 0; t.length > s; s++) if (d) "'" !== t.charAt(s) || h("'") ? u += t.charAt(s) : d = !1;else switch (t.charAt(s)) {
        case "d":
          u += l("d", e.getDate(), 2);
          break;

        case "D":
          u += c("D", e.getDay(), n, o);
          break;

        case "o":
          u += l("o", Math.round((new Date(e.getFullYear(), e.getMonth(), e.getDate()).getTime() - new Date(e.getFullYear(), 0, 0).getTime()) / 864e5), 3);
          break;

        case "m":
          u += l("m", e.getMonth() + 1, 2);
          break;

        case "M":
          u += c("M", e.getMonth(), a, r);
          break;

        case "y":
          u += h("y") ? e.getFullYear() : (10 > e.getFullYear() % 100 ? "0" : "") + e.getFullYear() % 100;
          break;

        case "@":
          u += e.getTime();
          break;

        case "!":
          u += 1e4 * e.getTime() + this._ticksTo1970;
          break;

        case "'":
          h("'") ? u += "'" : d = !0;
          break;

        default:
          u += t.charAt(s);
      }
      return u;
    },
    _possibleChars: function (t) {
      var e,
          i = "",
          s = !1,
          n = function (i) {
        var s = t.length > e + 1 && t.charAt(e + 1) === i;
        return s && e++, s;
      };

      for (e = 0; t.length > e; e++) if (s) "'" !== t.charAt(e) || n("'") ? i += t.charAt(e) : s = !1;else switch (t.charAt(e)) {
        case "d":
        case "m":
        case "y":
        case "@":
          i += "0123456789";
          break;

        case "D":
        case "M":
          return null;

        case "'":
          n("'") ? i += "'" : s = !0;
          break;

        default:
          i += t.charAt(e);
      }

      return i;
    },
    _get: function (t, e) {
      return void 0 !== t.settings[e] ? t.settings[e] : this._defaults[e];
    },
    _setDateFromField: function (t, e) {
      if (t.input.val() !== t.lastVal) {
        var i = this._get(t, "dateFormat"),
            s = t.lastVal = t.input ? t.input.val() : null,
            n = this._getDefaultDate(t),
            o = n,
            a = this._getFormatConfig(t);

        try {
          o = this.parseDate(i, s, a) || n;
        } catch (r) {
          s = e ? "" : s;
        }

        t.selectedDay = o.getDate(), t.drawMonth = t.selectedMonth = o.getMonth(), t.drawYear = t.selectedYear = o.getFullYear(), t.currentDay = s ? o.getDate() : 0, t.currentMonth = s ? o.getMonth() : 0, t.currentYear = s ? o.getFullYear() : 0, this._adjustInstDate(t);
      }
    },
    _getDefaultDate: function (t) {
      return this._restrictMinMax(t, this._determineDate(t, this._get(t, "defaultDate"), new Date()));
    },
    _determineDate: function (e, i, s) {
      var n = function (t) {
        var e = new Date();
        return e.setDate(e.getDate() + t), e;
      },
          o = function (i) {
        try {
          return t.datepicker.parseDate(t.datepicker._get(e, "dateFormat"), i, t.datepicker._getFormatConfig(e));
        } catch (s) {}

        for (var n = (i.toLowerCase().match(/^c/) ? t.datepicker._getDate(e) : null) || new Date(), o = n.getFullYear(), a = n.getMonth(), r = n.getDate(), h = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g, l = h.exec(i); l;) {
          switch (l[2] || "d") {
            case "d":
            case "D":
              r += parseInt(l[1], 10);
              break;

            case "w":
            case "W":
              r += 7 * parseInt(l[1], 10);
              break;

            case "m":
            case "M":
              a += parseInt(l[1], 10), r = Math.min(r, t.datepicker._getDaysInMonth(o, a));
              break;

            case "y":
            case "Y":
              o += parseInt(l[1], 10), r = Math.min(r, t.datepicker._getDaysInMonth(o, a));
          }

          l = h.exec(i);
        }

        return new Date(o, a, r);
      },
          a = null == i || "" === i ? s : "string" == typeof i ? o(i) : "number" == typeof i ? isNaN(i) ? s : n(i) : new Date(i.getTime());

      return a = a && "Invalid Date" == "" + a ? s : a, a && (a.setHours(0), a.setMinutes(0), a.setSeconds(0), a.setMilliseconds(0)), this._daylightSavingAdjust(a);
    },
    _daylightSavingAdjust: function (t) {
      return t ? (t.setHours(t.getHours() > 12 ? t.getHours() + 2 : 0), t) : null;
    },
    _setDate: function (t, e, i) {
      var s = !e,
          n = t.selectedMonth,
          o = t.selectedYear,
          a = this._restrictMinMax(t, this._determineDate(t, e, new Date()));

      t.selectedDay = t.currentDay = a.getDate(), t.drawMonth = t.selectedMonth = t.currentMonth = a.getMonth(), t.drawYear = t.selectedYear = t.currentYear = a.getFullYear(), n === t.selectedMonth && o === t.selectedYear || i || this._notifyChange(t), this._adjustInstDate(t), t.input && t.input.val(s ? "" : this._formatDate(t));
    },
    _getDate: function (t) {
      var e = !t.currentYear || t.input && "" === t.input.val() ? null : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
      return e;
    },
    _attachHandlers: function (e) {
      var i = this._get(e, "stepMonths"),
          s = "#" + e.id.replace(/\\\\/g, "\\");

      e.dpDiv.find("[data-handler]").map(function () {
        var e = {
          prev: function () {
            t.datepicker._adjustDate(s, -i, "M");
          },
          next: function () {
            t.datepicker._adjustDate(s, +i, "M");
          },
          hide: function () {
            t.datepicker._hideDatepicker();
          },
          today: function () {
            t.datepicker._gotoToday(s);
          },
          selectDay: function () {
            return t.datepicker._selectDay(s, +this.getAttribute("data-month"), +this.getAttribute("data-year"), this), !1;
          },
          selectMonth: function () {
            return t.datepicker._selectMonthYear(s, this, "M"), !1;
          },
          selectYear: function () {
            return t.datepicker._selectMonthYear(s, this, "Y"), !1;
          }
        };
        t(this).on(this.getAttribute("data-event"), e[this.getAttribute("data-handler")]);
      });
    },
    _generateHTML: function (t) {
      var e,
          i,
          s,
          n,
          o,
          a,
          r,
          h,
          l,
          c,
          u,
          d,
          p,
          f,
          g,
          m,
          _,
          v,
          b,
          y,
          w,
          k,
          x,
          C,
          D,
          I,
          T,
          P,
          M,
          S,
          H,
          z,
          O,
          A,
          N,
          W,
          E,
          F,
          L,
          R = new Date(),
          B = this._daylightSavingAdjust(new Date(R.getFullYear(), R.getMonth(), R.getDate())),
          Y = this._get(t, "isRTL"),
          j = this._get(t, "showButtonPanel"),
          q = this._get(t, "hideIfNoPrevNext"),
          K = this._get(t, "navigationAsDateFormat"),
          U = this._getNumberOfMonths(t),
          V = this._get(t, "showCurrentAtPos"),
          $ = this._get(t, "stepMonths"),
          X = 1 !== U[0] || 1 !== U[1],
          G = this._daylightSavingAdjust(t.currentDay ? new Date(t.currentYear, t.currentMonth, t.currentDay) : new Date(9999, 9, 9)),
          Q = this._getMinMaxDate(t, "min"),
          J = this._getMinMaxDate(t, "max"),
          Z = t.drawMonth - V,
          te = t.drawYear;

      if (0 > Z && (Z += 12, te--), J) for (e = this._daylightSavingAdjust(new Date(J.getFullYear(), J.getMonth() - U[0] * U[1] + 1, J.getDate())), e = Q && Q > e ? Q : e; this._daylightSavingAdjust(new Date(te, Z, 1)) > e;) Z--, 0 > Z && (Z = 11, te--);

      for (t.drawMonth = Z, t.drawYear = te, i = this._get(t, "prevText"), i = K ? this.formatDate(i, this._daylightSavingAdjust(new Date(te, Z - $, 1)), this._getFormatConfig(t)) : i, s = this._canAdjustMonth(t, -1, te, Z) ? "<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>" : q ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + i + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "e" : "w") + "'>" + i + "</span></a>", n = this._get(t, "nextText"), n = K ? this.formatDate(n, this._daylightSavingAdjust(new Date(te, Z + $, 1)), this._getFormatConfig(t)) : n, o = this._canAdjustMonth(t, 1, te, Z) ? "<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>" : q ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + n + "'><span class='ui-icon ui-icon-circle-triangle-" + (Y ? "w" : "e") + "'>" + n + "</span></a>", a = this._get(t, "currentText"), r = this._get(t, "gotoCurrent") && t.currentDay ? G : B, a = K ? this.formatDate(a, r, this._getFormatConfig(t)) : a, h = t.inline ? "" : "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" + this._get(t, "closeText") + "</button>", l = j ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + (Y ? h : "") + (this._isInRange(t, r) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>" + a + "</button>" : "") + (Y ? "" : h) + "</div>" : "", c = parseInt(this._get(t, "firstDay"), 10), c = isNaN(c) ? 0 : c, u = this._get(t, "showWeek"), d = this._get(t, "dayNames"), p = this._get(t, "dayNamesMin"), f = this._get(t, "monthNames"), g = this._get(t, "monthNamesShort"), m = this._get(t, "beforeShowDay"), _ = this._get(t, "showOtherMonths"), v = this._get(t, "selectOtherMonths"), b = this._getDefaultDate(t), y = "", k = 0; U[0] > k; k++) {
        for (x = "", this.maxRows = 4, C = 0; U[1] > C; C++) {
          if (D = this._daylightSavingAdjust(new Date(te, Z, t.selectedDay)), I = " ui-corner-all", T = "", X) {
            if (T += "<div class='ui-datepicker-group", U[1] > 1) switch (C) {
              case 0:
                T += " ui-datepicker-group-first", I = " ui-corner-" + (Y ? "right" : "left");
                break;

              case U[1] - 1:
                T += " ui-datepicker-group-last", I = " ui-corner-" + (Y ? "left" : "right");
                break;

              default:
                T += " ui-datepicker-group-middle", I = "";
            }
            T += "'>";
          }

          for (T += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + I + "'>" + (/all|left/.test(I) && 0 === k ? Y ? o : s : "") + (/all|right/.test(I) && 0 === k ? Y ? s : o : "") + this._generateMonthYearHeader(t, Z, te, Q, J, k > 0 || C > 0, f, g) + "</div><table class='ui-datepicker-calendar'><thead>" + "<tr>", P = u ? "<th class='ui-datepicker-week-col'>" + this._get(t, "weekHeader") + "</th>" : "", w = 0; 7 > w; w++) M = (w + c) % 7, P += "<th scope='col'" + ((w + c + 6) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "") + ">" + "<span title='" + d[M] + "'>" + p[M] + "</span></th>";

          for (T += P + "</tr></thead><tbody>", S = this._getDaysInMonth(te, Z), te === t.selectedYear && Z === t.selectedMonth && (t.selectedDay = Math.min(t.selectedDay, S)), H = (this._getFirstDayOfMonth(te, Z) - c + 7) % 7, z = Math.ceil((H + S) / 7), O = X ? this.maxRows > z ? this.maxRows : z : z, this.maxRows = O, A = this._daylightSavingAdjust(new Date(te, Z, 1 - H)), N = 0; O > N; N++) {
            for (T += "<tr>", W = u ? "<td class='ui-datepicker-week-col'>" + this._get(t, "calculateWeek")(A) + "</td>" : "", w = 0; 7 > w; w++) E = m ? m.apply(t.input ? t.input[0] : null, [A]) : [!0, ""], F = A.getMonth() !== Z, L = F && !v || !E[0] || Q && Q > A || J && A > J, W += "<td class='" + ((w + c + 6) % 7 >= 5 ? " ui-datepicker-week-end" : "") + (F ? " ui-datepicker-other-month" : "") + (A.getTime() === D.getTime() && Z === t.selectedMonth && t._keyEvent || b.getTime() === A.getTime() && b.getTime() === D.getTime() ? " " + this._dayOverClass : "") + (L ? " " + this._unselectableClass + " ui-state-disabled" : "") + (F && !_ ? "" : " " + E[1] + (A.getTime() === G.getTime() ? " " + this._currentClass : "") + (A.getTime() === B.getTime() ? " ui-datepicker-today" : "")) + "'" + (F && !_ || !E[2] ? "" : " title='" + E[2].replace(/'/g, "&#39;") + "'") + (L ? "" : " data-handler='selectDay' data-event='click' data-month='" + A.getMonth() + "' data-year='" + A.getFullYear() + "'") + ">" + (F && !_ ? "&#xa0;" : L ? "<span class='ui-state-default'>" + A.getDate() + "</span>" : "<a class='ui-state-default" + (A.getTime() === B.getTime() ? " ui-state-highlight" : "") + (A.getTime() === G.getTime() ? " ui-state-active" : "") + (F ? " ui-priority-secondary" : "") + "' href='#'>" + A.getDate() + "</a>") + "</td>", A.setDate(A.getDate() + 1), A = this._daylightSavingAdjust(A);

            T += W + "</tr>";
          }

          Z++, Z > 11 && (Z = 0, te++), T += "</tbody></table>" + (X ? "</div>" + (U[0] > 0 && C === U[1] - 1 ? "<div class='ui-datepicker-row-break'></div>" : "") : ""), x += T;
        }

        y += x;
      }

      return y += l, t._keyEvent = !1, y;
    },
    _generateMonthYearHeader: function (t, e, i, s, n, o, a, r) {
      var h,
          l,
          c,
          u,
          d,
          p,
          f,
          g,
          m = this._get(t, "changeMonth"),
          _ = this._get(t, "changeYear"),
          v = this._get(t, "showMonthAfterYear"),
          b = "<div class='ui-datepicker-title'>",
          y = "";

      if (o || !m) y += "<span class='ui-datepicker-month'>" + a[e] + "</span>";else {
        for (h = s && s.getFullYear() === i, l = n && n.getFullYear() === i, y += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>", c = 0; 12 > c; c++) (!h || c >= s.getMonth()) && (!l || n.getMonth() >= c) && (y += "<option value='" + c + "'" + (c === e ? " selected='selected'" : "") + ">" + r[c] + "</option>");

        y += "</select>";
      }
      if (v || (b += y + (!o && m && _ ? "" : "&#xa0;")), !t.yearshtml) if (t.yearshtml = "", o || !_) b += "<span class='ui-datepicker-year'>" + i + "</span>";else {
        for (u = this._get(t, "yearRange").split(":"), d = new Date().getFullYear(), p = function (t) {
          var e = t.match(/c[+\-].*/) ? i + parseInt(t.substring(1), 10) : t.match(/[+\-].*/) ? d + parseInt(t, 10) : parseInt(t, 10);
          return isNaN(e) ? d : e;
        }, f = p(u[0]), g = Math.max(f, p(u[1] || "")), f = s ? Math.max(f, s.getFullYear()) : f, g = n ? Math.min(g, n.getFullYear()) : g, t.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>"; g >= f; f++) t.yearshtml += "<option value='" + f + "'" + (f === i ? " selected='selected'" : "") + ">" + f + "</option>";

        t.yearshtml += "</select>", b += t.yearshtml, t.yearshtml = null;
      }
      return b += this._get(t, "yearSuffix"), v && (b += (!o && m && _ ? "" : "&#xa0;") + y), b += "</div>";
    },
    _adjustInstDate: function (t, e, i) {
      var s = t.selectedYear + ("Y" === i ? e : 0),
          n = t.selectedMonth + ("M" === i ? e : 0),
          o = Math.min(t.selectedDay, this._getDaysInMonth(s, n)) + ("D" === i ? e : 0),
          a = this._restrictMinMax(t, this._daylightSavingAdjust(new Date(s, n, o)));

      t.selectedDay = a.getDate(), t.drawMonth = t.selectedMonth = a.getMonth(), t.drawYear = t.selectedYear = a.getFullYear(), ("M" === i || "Y" === i) && this._notifyChange(t);
    },
    _restrictMinMax: function (t, e) {
      var i = this._getMinMaxDate(t, "min"),
          s = this._getMinMaxDate(t, "max"),
          n = i && i > e ? i : e;

      return s && n > s ? s : n;
    },
    _notifyChange: function (t) {
      var e = this._get(t, "onChangeMonthYear");

      e && e.apply(t.input ? t.input[0] : null, [t.selectedYear, t.selectedMonth + 1, t]);
    },
    _getNumberOfMonths: function (t) {
      var e = this._get(t, "numberOfMonths");

      return null == e ? [1, 1] : "number" == typeof e ? [1, e] : e;
    },
    _getMinMaxDate: function (t, e) {
      return this._determineDate(t, this._get(t, e + "Date"), null);
    },
    _getDaysInMonth: function (t, e) {
      return 32 - this._daylightSavingAdjust(new Date(t, e, 32)).getDate();
    },
    _getFirstDayOfMonth: function (t, e) {
      return new Date(t, e, 1).getDay();
    },
    _canAdjustMonth: function (t, e, i, s) {
      var n = this._getNumberOfMonths(t),
          o = this._daylightSavingAdjust(new Date(i, s + (0 > e ? e : n[0] * n[1]), 1));

      return 0 > e && o.setDate(this._getDaysInMonth(o.getFullYear(), o.getMonth())), this._isInRange(t, o);
    },
    _isInRange: function (t, e) {
      var i,
          s,
          n = this._getMinMaxDate(t, "min"),
          o = this._getMinMaxDate(t, "max"),
          a = null,
          r = null,
          h = this._get(t, "yearRange");

      return h && (i = h.split(":"), s = new Date().getFullYear(), a = parseInt(i[0], 10), r = parseInt(i[1], 10), i[0].match(/[+\-].*/) && (a += s), i[1].match(/[+\-].*/) && (r += s)), (!n || e.getTime() >= n.getTime()) && (!o || e.getTime() <= o.getTime()) && (!a || e.getFullYear() >= a) && (!r || r >= e.getFullYear());
    },
    _getFormatConfig: function (t) {
      var e = this._get(t, "shortYearCutoff");

      return e = "string" != typeof e ? e : new Date().getFullYear() % 100 + parseInt(e, 10), {
        shortYearCutoff: e,
        dayNamesShort: this._get(t, "dayNamesShort"),
        dayNames: this._get(t, "dayNames"),
        monthNamesShort: this._get(t, "monthNamesShort"),
        monthNames: this._get(t, "monthNames")
      };
    },
    _formatDate: function (t, e, i, s) {
      e || (t.currentDay = t.selectedDay, t.currentMonth = t.selectedMonth, t.currentYear = t.selectedYear);
      var n = e ? "object" == typeof e ? e : this._daylightSavingAdjust(new Date(s, i, e)) : this._daylightSavingAdjust(new Date(t.currentYear, t.currentMonth, t.currentDay));
      return this.formatDate(this._get(t, "dateFormat"), n, this._getFormatConfig(t));
    }
  }), t.fn.datepicker = function (e) {
    if (!this.length) return this;
    t.datepicker.initialized || (t(document).on("mousedown", t.datepicker._checkExternalClick), t.datepicker.initialized = !0), 0 === t("#" + t.datepicker._mainDivId).length && t("body").append(t.datepicker.dpDiv);
    var i = Array.prototype.slice.call(arguments, 1);
    return "string" != typeof e || "isDisabled" !== e && "getDate" !== e && "widget" !== e ? "option" === e && 2 === arguments.length && "string" == typeof arguments[1] ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i)) : this.each(function () {
      "string" == typeof e ? t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this].concat(i)) : t.datepicker._attachDatepicker(this, e);
    }) : t.datepicker["_" + e + "Datepicker"].apply(t.datepicker, [this[0]].concat(i));
  }, t.datepicker = new s(), t.datepicker.initialized = !1, t.datepicker.uuid = new Date().getTime(), t.datepicker.version = "1.12.1", t.datepicker, t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());

  var _ = !1;

  t(document).on("mouseup", function () {
    _ = !1;
  }), t.widget("ui.mouse", {
    version: "1.12.1",
    options: {
      cancel: "input, textarea, button, select, option",
      distance: 1,
      delay: 0
    },
    _mouseInit: function () {
      var e = this;
      this.element.on("mousedown." + this.widgetName, function (t) {
        return e._mouseDown(t);
      }).on("click." + this.widgetName, function (i) {
        return !0 === t.data(i.target, e.widgetName + ".preventClickEvent") ? (t.removeData(i.target, e.widgetName + ".preventClickEvent"), i.stopImmediatePropagation(), !1) : void 0;
      }), this.started = !1;
    },
    _mouseDestroy: function () {
      this.element.off("." + this.widgetName), this._mouseMoveDelegate && this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate);
    },
    _mouseDown: function (e) {
      if (!_) {
        this._mouseMoved = !1, this._mouseStarted && this._mouseUp(e), this._mouseDownEvent = e;
        var i = this,
            s = 1 === e.which,
            n = "string" == typeof this.options.cancel && e.target.nodeName ? t(e.target).closest(this.options.cancel).length : !1;
        return s && !n && this._mouseCapture(e) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
          i.mouseDelayMet = !0;
        }, this.options.delay)), this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(e) !== !1, !this._mouseStarted) ? (e.preventDefault(), !0) : (!0 === t.data(e.target, this.widgetName + ".preventClickEvent") && t.removeData(e.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (t) {
          return i._mouseMove(t);
        }, this._mouseUpDelegate = function (t) {
          return i._mouseUp(t);
        }, this.document.on("mousemove." + this.widgetName, this._mouseMoveDelegate).on("mouseup." + this.widgetName, this._mouseUpDelegate), e.preventDefault(), _ = !0, !0)) : !0;
      }
    },
    _mouseMove: function (e) {
      if (this._mouseMoved) {
        if (t.ui.ie && (!document.documentMode || 9 > document.documentMode) && !e.button) return this._mouseUp(e);
        if (!e.which) if (e.originalEvent.altKey || e.originalEvent.ctrlKey || e.originalEvent.metaKey || e.originalEvent.shiftKey) this.ignoreMissingWhich = !0;else if (!this.ignoreMissingWhich) return this._mouseUp(e);
      }

      return (e.which || e.button) && (this._mouseMoved = !0), this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted);
    },
    _mouseUp: function (e) {
      this.document.off("mousemove." + this.widgetName, this._mouseMoveDelegate).off("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), this._mouseDelayTimer && (clearTimeout(this._mouseDelayTimer), delete this._mouseDelayTimer), this.ignoreMissingWhich = !1, _ = !1, e.preventDefault();
    },
    _mouseDistanceMet: function (t) {
      return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance;
    },
    _mouseDelayMet: function () {
      return this.mouseDelayMet;
    },
    _mouseStart: function () {},
    _mouseDrag: function () {},
    _mouseStop: function () {},
    _mouseCapture: function () {
      return !0;
    }
  }), t.ui.plugin = {
    add: function (e, i, s) {
      var n,
          o = t.ui[e].prototype;

      for (n in s) o.plugins[n] = o.plugins[n] || [], o.plugins[n].push([i, s[n]]);
    },
    call: function (t, e, i, s) {
      var n,
          o = t.plugins[e];
      if (o && (s || t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)) for (n = 0; o.length > n; n++) t.options[o[n][0]] && o[n][1].apply(t.element, i);
    }
  }, t.ui.safeBlur = function (e) {
    e && "body" !== e.nodeName.toLowerCase() && t(e).trigger("blur");
  }, t.widget("ui.draggable", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "drag",
    options: {
      addClasses: !0,
      appendTo: "parent",
      axis: !1,
      connectToSortable: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      iframeFix: !1,
      opacity: !1,
      refreshPositions: !1,
      revert: !1,
      revertDuration: 500,
      scope: "default",
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      snap: !1,
      snapMode: "both",
      snapTolerance: 20,
      stack: !1,
      zIndex: !1,
      drag: null,
      start: null,
      stop: null
    },
    _create: function () {
      "original" === this.options.helper && this._setPositionRelative(), this.options.addClasses && this._addClass("ui-draggable"), this._setHandleClassName(), this._mouseInit();
    },
    _setOption: function (t, e) {
      this._super(t, e), "handle" === t && (this._removeHandleClassName(), this._setHandleClassName());
    },
    _destroy: function () {
      return (this.helper || this.element).is(".ui-draggable-dragging") ? (this.destroyOnClear = !0, void 0) : (this._removeHandleClassName(), this._mouseDestroy(), void 0);
    },
    _mouseCapture: function (e) {
      var i = this.options;
      return this.helper || i.disabled || t(e.target).closest(".ui-resizable-handle").length > 0 ? !1 : (this.handle = this._getHandle(e), this.handle ? (this._blurActiveElement(e), this._blockFrames(i.iframeFix === !0 ? "iframe" : i.iframeFix), !0) : !1);
    },
    _blockFrames: function (e) {
      this.iframeBlocks = this.document.find(e).map(function () {
        var e = t(this);
        return t("<div>").css("position", "absolute").appendTo(e.parent()).outerWidth(e.outerWidth()).outerHeight(e.outerHeight()).offset(e.offset())[0];
      });
    },
    _unblockFrames: function () {
      this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
    },
    _blurActiveElement: function (e) {
      var i = t.ui.safeActiveElement(this.document[0]),
          s = t(e.target);
      s.closest(i).length || t.ui.safeBlur(i);
    },
    _mouseStart: function (e) {
      var i = this.options;
      return this.helper = this._createHelper(e), this._addClass(this.helper, "ui-draggable-dragging"), this._cacheHelperProportions(), t.ui.ddmanager && (t.ui.ddmanager.current = this), this._cacheMargins(), this.cssPosition = this.helper.css("position"), this.scrollParent = this.helper.scrollParent(!0), this.offsetParent = this.helper.offsetParent(), this.hasFixedAncestor = this.helper.parents().filter(function () {
        return "fixed" === t(this).css("position");
      }).length > 0, this.positionAbs = this.element.offset(), this._refreshOffsets(e), this.originalPosition = this.position = this._generatePosition(e, !1), this.originalPageX = e.pageX, this.originalPageY = e.pageY, i.cursorAt && this._adjustOffsetFromHelper(i.cursorAt), this._setContainment(), this._trigger("start", e) === !1 ? (this._clear(), !1) : (this._cacheHelperProportions(), t.ui.ddmanager && !i.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this._mouseDrag(e, !0), t.ui.ddmanager && t.ui.ddmanager.dragStart(this, e), !0);
    },
    _refreshOffsets: function (t) {
      this.offset = {
        top: this.positionAbs.top - this.margins.top,
        left: this.positionAbs.left - this.margins.left,
        scroll: !1,
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }, this.offset.click = {
        left: t.pageX - this.offset.left,
        top: t.pageY - this.offset.top
      };
    },
    _mouseDrag: function (e, i) {
      if (this.hasFixedAncestor && (this.offset.parent = this._getParentOffset()), this.position = this._generatePosition(e, !0), this.positionAbs = this._convertPositionTo("absolute"), !i) {
        var s = this._uiHash();

        if (this._trigger("drag", e, s) === !1) return this._mouseUp(new t.Event("mouseup", e)), !1;
        this.position = s.position;
      }

      return this.helper[0].style.left = this.position.left + "px", this.helper[0].style.top = this.position.top + "px", t.ui.ddmanager && t.ui.ddmanager.drag(this, e), !1;
    },
    _mouseStop: function (e) {
      var i = this,
          s = !1;
      return t.ui.ddmanager && !this.options.dropBehaviour && (s = t.ui.ddmanager.drop(this, e)), this.dropped && (s = this.dropped, this.dropped = !1), "invalid" === this.options.revert && !s || "valid" === this.options.revert && s || this.options.revert === !0 || t.isFunction(this.options.revert) && this.options.revert.call(this.element, s) ? t(this.helper).animate(this.originalPosition, parseInt(this.options.revertDuration, 10), function () {
        i._trigger("stop", e) !== !1 && i._clear();
      }) : this._trigger("stop", e) !== !1 && this._clear(), !1;
    },
    _mouseUp: function (e) {
      return this._unblockFrames(), t.ui.ddmanager && t.ui.ddmanager.dragStop(this, e), this.handleElement.is(e.target) && this.element.trigger("focus"), t.ui.mouse.prototype._mouseUp.call(this, e);
    },
    cancel: function () {
      return this.helper.is(".ui-draggable-dragging") ? this._mouseUp(new t.Event("mouseup", {
        target: this.element[0]
      })) : this._clear(), this;
    },
    _getHandle: function (e) {
      return this.options.handle ? !!t(e.target).closest(this.element.find(this.options.handle)).length : !0;
    },
    _setHandleClassName: function () {
      this.handleElement = this.options.handle ? this.element.find(this.options.handle) : this.element, this._addClass(this.handleElement, "ui-draggable-handle");
    },
    _removeHandleClassName: function () {
      this._removeClass(this.handleElement, "ui-draggable-handle");
    },
    _createHelper: function (e) {
      var i = this.options,
          s = t.isFunction(i.helper),
          n = s ? t(i.helper.apply(this.element[0], [e])) : "clone" === i.helper ? this.element.clone().removeAttr("id") : this.element;
      return n.parents("body").length || n.appendTo("parent" === i.appendTo ? this.element[0].parentNode : i.appendTo), s && n[0] === this.element[0] && this._setPositionRelative(), n[0] === this.element[0] || /(fixed|absolute)/.test(n.css("position")) || n.css("position", "absolute"), n;
    },
    _setPositionRelative: function () {
      /^(?:r|a|f)/.test(this.element.css("position")) || (this.element[0].style.position = "relative");
    },
    _adjustOffsetFromHelper: function (e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top);
    },
    _isRootNode: function (t) {
      return /(html|body)/i.test(t.tagName) || t === this.document[0];
    },
    _getParentOffset: function () {
      var e = this.offsetParent.offset(),
          i = this.document[0];
      return "absolute" === this.cssPosition && this.scrollParent[0] !== i && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), this._isRootNode(this.offsetParent[0]) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      };
    },
    _getRelativeOffset: function () {
      if ("relative" !== this.cssPosition) return {
        top: 0,
        left: 0
      };

      var t = this.element.position(),
          e = this._isRootNode(this.scrollParent[0]);

      return {
        top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + (e ? 0 : this.scrollParent.scrollTop()),
        left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + (e ? 0 : this.scrollParent.scrollLeft())
      };
    },
    _cacheMargins: function () {
      this.margins = {
        left: parseInt(this.element.css("marginLeft"), 10) || 0,
        top: parseInt(this.element.css("marginTop"), 10) || 0,
        right: parseInt(this.element.css("marginRight"), 10) || 0,
        bottom: parseInt(this.element.css("marginBottom"), 10) || 0
      };
    },
    _cacheHelperProportions: function () {
      this.helperProportions = {
        width: this.helper.outerWidth(),
        height: this.helper.outerHeight()
      };
    },
    _setContainment: function () {
      var e,
          i,
          s,
          n = this.options,
          o = this.document[0];
      return this.relativeContainer = null, n.containment ? "window" === n.containment ? (this.containment = [t(window).scrollLeft() - this.offset.relative.left - this.offset.parent.left, t(window).scrollTop() - this.offset.relative.top - this.offset.parent.top, t(window).scrollLeft() + t(window).width() - this.helperProportions.width - this.margins.left, t(window).scrollTop() + (t(window).height() || o.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top], void 0) : "document" === n.containment ? (this.containment = [0, 0, t(o).width() - this.helperProportions.width - this.margins.left, (t(o).height() || o.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top], void 0) : n.containment.constructor === Array ? (this.containment = n.containment, void 0) : ("parent" === n.containment && (n.containment = this.helper[0].parentNode), i = t(n.containment), s = i[0], s && (e = /(scroll|auto)/.test(i.css("overflow")), this.containment = [(parseInt(i.css("borderLeftWidth"), 10) || 0) + (parseInt(i.css("paddingLeft"), 10) || 0), (parseInt(i.css("borderTopWidth"), 10) || 0) + (parseInt(i.css("paddingTop"), 10) || 0), (e ? Math.max(s.scrollWidth, s.offsetWidth) : s.offsetWidth) - (parseInt(i.css("borderRightWidth"), 10) || 0) - (parseInt(i.css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left - this.margins.right, (e ? Math.max(s.scrollHeight, s.offsetHeight) : s.offsetHeight) - (parseInt(i.css("borderBottomWidth"), 10) || 0) - (parseInt(i.css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top - this.margins.bottom], this.relativeContainer = i), void 0) : (this.containment = null, void 0);
    },
    _convertPositionTo: function (t, e) {
      e || (e = this.position);

      var i = "absolute" === t ? 1 : -1,
          s = this._isRootNode(this.scrollParent[0]);

      return {
        top: e.top + this.offset.relative.top * i + this.offset.parent.top * i - ("fixed" === this.cssPosition ? -this.offset.scroll.top : s ? 0 : this.offset.scroll.top) * i,
        left: e.left + this.offset.relative.left * i + this.offset.parent.left * i - ("fixed" === this.cssPosition ? -this.offset.scroll.left : s ? 0 : this.offset.scroll.left) * i
      };
    },
    _generatePosition: function (t, e) {
      var i,
          s,
          n,
          o,
          a = this.options,
          r = this._isRootNode(this.scrollParent[0]),
          h = t.pageX,
          l = t.pageY;

      return r && this.offset.scroll || (this.offset.scroll = {
        top: this.scrollParent.scrollTop(),
        left: this.scrollParent.scrollLeft()
      }), e && (this.containment && (this.relativeContainer ? (s = this.relativeContainer.offset(), i = [this.containment[0] + s.left, this.containment[1] + s.top, this.containment[2] + s.left, this.containment[3] + s.top]) : i = this.containment, t.pageX - this.offset.click.left < i[0] && (h = i[0] + this.offset.click.left), t.pageY - this.offset.click.top < i[1] && (l = i[1] + this.offset.click.top), t.pageX - this.offset.click.left > i[2] && (h = i[2] + this.offset.click.left), t.pageY - this.offset.click.top > i[3] && (l = i[3] + this.offset.click.top)), a.grid && (n = a.grid[1] ? this.originalPageY + Math.round((l - this.originalPageY) / a.grid[1]) * a.grid[1] : this.originalPageY, l = i ? n - this.offset.click.top >= i[1] || n - this.offset.click.top > i[3] ? n : n - this.offset.click.top >= i[1] ? n - a.grid[1] : n + a.grid[1] : n, o = a.grid[0] ? this.originalPageX + Math.round((h - this.originalPageX) / a.grid[0]) * a.grid[0] : this.originalPageX, h = i ? o - this.offset.click.left >= i[0] || o - this.offset.click.left > i[2] ? o : o - this.offset.click.left >= i[0] ? o - a.grid[0] : o + a.grid[0] : o), "y" === a.axis && (h = this.originalPageX), "x" === a.axis && (l = this.originalPageY)), {
        top: l - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.offset.scroll.top : r ? 0 : this.offset.scroll.top),
        left: h - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.offset.scroll.left : r ? 0 : this.offset.scroll.left)
      };
    },
    _clear: function () {
      this._removeClass(this.helper, "ui-draggable-dragging"), this.helper[0] === this.element[0] || this.cancelHelperRemoval || this.helper.remove(), this.helper = null, this.cancelHelperRemoval = !1, this.destroyOnClear && this.destroy();
    },
    _trigger: function (e, i, s) {
      return s = s || this._uiHash(), t.ui.plugin.call(this, e, [i, s, this], !0), /^(drag|start|stop)/.test(e) && (this.positionAbs = this._convertPositionTo("absolute"), s.offset = this.positionAbs), t.Widget.prototype._trigger.call(this, e, i, s);
    },
    plugins: {},
    _uiHash: function () {
      return {
        helper: this.helper,
        position: this.position,
        originalPosition: this.originalPosition,
        offset: this.positionAbs
      };
    }
  }), t.ui.plugin.add("draggable", "connectToSortable", {
    start: function (e, i, s) {
      var n = t.extend({}, i, {
        item: s.element
      });
      s.sortables = [], t(s.options.connectToSortable).each(function () {
        var i = t(this).sortable("instance");
        i && !i.options.disabled && (s.sortables.push(i), i.refreshPositions(), i._trigger("activate", e, n));
      });
    },
    stop: function (e, i, s) {
      var n = t.extend({}, i, {
        item: s.element
      });
      s.cancelHelperRemoval = !1, t.each(s.sortables, function () {
        var t = this;
        t.isOver ? (t.isOver = 0, s.cancelHelperRemoval = !0, t.cancelHelperRemoval = !1, t._storedCSS = {
          position: t.placeholder.css("position"),
          top: t.placeholder.css("top"),
          left: t.placeholder.css("left")
        }, t._mouseStop(e), t.options.helper = t.options._helper) : (t.cancelHelperRemoval = !0, t._trigger("deactivate", e, n));
      });
    },
    drag: function (e, i, s) {
      t.each(s.sortables, function () {
        var n = !1,
            o = this;
        o.positionAbs = s.positionAbs, o.helperProportions = s.helperProportions, o.offset.click = s.offset.click, o._intersectsWith(o.containerCache) && (n = !0, t.each(s.sortables, function () {
          return this.positionAbs = s.positionAbs, this.helperProportions = s.helperProportions, this.offset.click = s.offset.click, this !== o && this._intersectsWith(this.containerCache) && t.contains(o.element[0], this.element[0]) && (n = !1), n;
        })), n ? (o.isOver || (o.isOver = 1, s._parent = i.helper.parent(), o.currentItem = i.helper.appendTo(o.element).data("ui-sortable-item", !0), o.options._helper = o.options.helper, o.options.helper = function () {
          return i.helper[0];
        }, e.target = o.currentItem[0], o._mouseCapture(e, !0), o._mouseStart(e, !0, !0), o.offset.click.top = s.offset.click.top, o.offset.click.left = s.offset.click.left, o.offset.parent.left -= s.offset.parent.left - o.offset.parent.left, o.offset.parent.top -= s.offset.parent.top - o.offset.parent.top, s._trigger("toSortable", e), s.dropped = o.element, t.each(s.sortables, function () {
          this.refreshPositions();
        }), s.currentItem = s.element, o.fromOutside = s), o.currentItem && (o._mouseDrag(e), i.position = o.position)) : o.isOver && (o.isOver = 0, o.cancelHelperRemoval = !0, o.options._revert = o.options.revert, o.options.revert = !1, o._trigger("out", e, o._uiHash(o)), o._mouseStop(e, !0), o.options.revert = o.options._revert, o.options.helper = o.options._helper, o.placeholder && o.placeholder.remove(), i.helper.appendTo(s._parent), s._refreshOffsets(e), i.position = s._generatePosition(e, !0), s._trigger("fromSortable", e), s.dropped = !1, t.each(s.sortables, function () {
          this.refreshPositions();
        }));
      });
    }
  }), t.ui.plugin.add("draggable", "cursor", {
    start: function (e, i, s) {
      var n = t("body"),
          o = s.options;
      n.css("cursor") && (o._cursor = n.css("cursor")), n.css("cursor", o.cursor);
    },
    stop: function (e, i, s) {
      var n = s.options;
      n._cursor && t("body").css("cursor", n._cursor);
    }
  }), t.ui.plugin.add("draggable", "opacity", {
    start: function (e, i, s) {
      var n = t(i.helper),
          o = s.options;
      n.css("opacity") && (o._opacity = n.css("opacity")), n.css("opacity", o.opacity);
    },
    stop: function (e, i, s) {
      var n = s.options;
      n._opacity && t(i.helper).css("opacity", n._opacity);
    }
  }), t.ui.plugin.add("draggable", "scroll", {
    start: function (t, e, i) {
      i.scrollParentNotHidden || (i.scrollParentNotHidden = i.helper.scrollParent(!1)), i.scrollParentNotHidden[0] !== i.document[0] && "HTML" !== i.scrollParentNotHidden[0].tagName && (i.overflowOffset = i.scrollParentNotHidden.offset());
    },
    drag: function (e, i, s) {
      var n = s.options,
          o = !1,
          a = s.scrollParentNotHidden[0],
          r = s.document[0];
      a !== r && "HTML" !== a.tagName ? (n.axis && "x" === n.axis || (s.overflowOffset.top + a.offsetHeight - e.pageY < n.scrollSensitivity ? a.scrollTop = o = a.scrollTop + n.scrollSpeed : e.pageY - s.overflowOffset.top < n.scrollSensitivity && (a.scrollTop = o = a.scrollTop - n.scrollSpeed)), n.axis && "y" === n.axis || (s.overflowOffset.left + a.offsetWidth - e.pageX < n.scrollSensitivity ? a.scrollLeft = o = a.scrollLeft + n.scrollSpeed : e.pageX - s.overflowOffset.left < n.scrollSensitivity && (a.scrollLeft = o = a.scrollLeft - n.scrollSpeed))) : (n.axis && "x" === n.axis || (e.pageY - t(r).scrollTop() < n.scrollSensitivity ? o = t(r).scrollTop(t(r).scrollTop() - n.scrollSpeed) : t(window).height() - (e.pageY - t(r).scrollTop()) < n.scrollSensitivity && (o = t(r).scrollTop(t(r).scrollTop() + n.scrollSpeed))), n.axis && "y" === n.axis || (e.pageX - t(r).scrollLeft() < n.scrollSensitivity ? o = t(r).scrollLeft(t(r).scrollLeft() - n.scrollSpeed) : t(window).width() - (e.pageX - t(r).scrollLeft()) < n.scrollSensitivity && (o = t(r).scrollLeft(t(r).scrollLeft() + n.scrollSpeed)))), o !== !1 && t.ui.ddmanager && !n.dropBehaviour && t.ui.ddmanager.prepareOffsets(s, e);
    }
  }), t.ui.plugin.add("draggable", "snap", {
    start: function (e, i, s) {
      var n = s.options;
      s.snapElements = [], t(n.snap.constructor !== String ? n.snap.items || ":data(ui-draggable)" : n.snap).each(function () {
        var e = t(this),
            i = e.offset();
        this !== s.element[0] && s.snapElements.push({
          item: this,
          width: e.outerWidth(),
          height: e.outerHeight(),
          top: i.top,
          left: i.left
        });
      });
    },
    drag: function (e, i, s) {
      var n,
          o,
          a,
          r,
          h,
          l,
          c,
          u,
          d,
          p,
          f = s.options,
          g = f.snapTolerance,
          m = i.offset.left,
          _ = m + s.helperProportions.width,
          v = i.offset.top,
          b = v + s.helperProportions.height;

      for (d = s.snapElements.length - 1; d >= 0; d--) h = s.snapElements[d].left - s.margins.left, l = h + s.snapElements[d].width, c = s.snapElements[d].top - s.margins.top, u = c + s.snapElements[d].height, h - g > _ || m > l + g || c - g > b || v > u + g || !t.contains(s.snapElements[d].item.ownerDocument, s.snapElements[d].item) ? (s.snapElements[d].snapping && s.options.snap.release && s.options.snap.release.call(s.element, e, t.extend(s._uiHash(), {
        snapItem: s.snapElements[d].item
      })), s.snapElements[d].snapping = !1) : ("inner" !== f.snapMode && (n = g >= Math.abs(c - b), o = g >= Math.abs(u - v), a = g >= Math.abs(h - _), r = g >= Math.abs(l - m), n && (i.position.top = s._convertPositionTo("relative", {
        top: c - s.helperProportions.height,
        left: 0
      }).top), o && (i.position.top = s._convertPositionTo("relative", {
        top: u,
        left: 0
      }).top), a && (i.position.left = s._convertPositionTo("relative", {
        top: 0,
        left: h - s.helperProportions.width
      }).left), r && (i.position.left = s._convertPositionTo("relative", {
        top: 0,
        left: l
      }).left)), p = n || o || a || r, "outer" !== f.snapMode && (n = g >= Math.abs(c - v), o = g >= Math.abs(u - b), a = g >= Math.abs(h - m), r = g >= Math.abs(l - _), n && (i.position.top = s._convertPositionTo("relative", {
        top: c,
        left: 0
      }).top), o && (i.position.top = s._convertPositionTo("relative", {
        top: u - s.helperProportions.height,
        left: 0
      }).top), a && (i.position.left = s._convertPositionTo("relative", {
        top: 0,
        left: h
      }).left), r && (i.position.left = s._convertPositionTo("relative", {
        top: 0,
        left: l - s.helperProportions.width
      }).left)), !s.snapElements[d].snapping && (n || o || a || r || p) && s.options.snap.snap && s.options.snap.snap.call(s.element, e, t.extend(s._uiHash(), {
        snapItem: s.snapElements[d].item
      })), s.snapElements[d].snapping = n || o || a || r || p);
    }
  }), t.ui.plugin.add("draggable", "stack", {
    start: function (e, i, s) {
      var n,
          o = s.options,
          a = t.makeArray(t(o.stack)).sort(function (e, i) {
        return (parseInt(t(e).css("zIndex"), 10) || 0) - (parseInt(t(i).css("zIndex"), 10) || 0);
      });
      a.length && (n = parseInt(t(a[0]).css("zIndex"), 10) || 0, t(a).each(function (e) {
        t(this).css("zIndex", n + e);
      }), this.css("zIndex", n + a.length));
    }
  }), t.ui.plugin.add("draggable", "zIndex", {
    start: function (e, i, s) {
      var n = t(i.helper),
          o = s.options;
      n.css("zIndex") && (o._zIndex = n.css("zIndex")), n.css("zIndex", o.zIndex);
    },
    stop: function (e, i, s) {
      var n = s.options;
      n._zIndex && t(i.helper).css("zIndex", n._zIndex);
    }
  }), t.ui.draggable, t.widget("ui.resizable", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "resize",
    options: {
      alsoResize: !1,
      animate: !1,
      animateDuration: "slow",
      animateEasing: "swing",
      aspectRatio: !1,
      autoHide: !1,
      classes: {
        "ui-resizable-se": "ui-icon ui-icon-gripsmall-diagonal-se"
      },
      containment: !1,
      ghost: !1,
      grid: !1,
      handles: "e,s,se",
      helper: !1,
      maxHeight: null,
      maxWidth: null,
      minHeight: 10,
      minWidth: 10,
      zIndex: 90,
      resize: null,
      start: null,
      stop: null
    },
    _num: function (t) {
      return parseFloat(t) || 0;
    },
    _isNumber: function (t) {
      return !isNaN(parseFloat(t));
    },
    _hasScroll: function (e, i) {
      if ("hidden" === t(e).css("overflow")) return !1;
      var s = i && "left" === i ? "scrollLeft" : "scrollTop",
          n = !1;
      return e[s] > 0 ? !0 : (e[s] = 1, n = e[s] > 0, e[s] = 0, n);
    },
    _create: function () {
      var e,
          i = this.options,
          s = this;
      this._addClass("ui-resizable"), t.extend(this, {
        _aspectRatio: !!i.aspectRatio,
        aspectRatio: i.aspectRatio,
        originalElement: this.element,
        _proportionallyResizeElements: [],
        _helper: i.helper || i.ghost || i.animate ? i.helper || "ui-resizable-helper" : null
      }), this.element[0].nodeName.match(/^(canvas|textarea|input|select|button|img)$/i) && (this.element.wrap(t("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({
        position: this.element.css("position"),
        width: this.element.outerWidth(),
        height: this.element.outerHeight(),
        top: this.element.css("top"),
        left: this.element.css("left")
      })), this.element = this.element.parent().data("ui-resizable", this.element.resizable("instance")), this.elementIsWrapper = !0, e = {
        marginTop: this.originalElement.css("marginTop"),
        marginRight: this.originalElement.css("marginRight"),
        marginBottom: this.originalElement.css("marginBottom"),
        marginLeft: this.originalElement.css("marginLeft")
      }, this.element.css(e), this.originalElement.css("margin", 0), this.originalResizeStyle = this.originalElement.css("resize"), this.originalElement.css("resize", "none"), this._proportionallyResizeElements.push(this.originalElement.css({
        position: "static",
        zoom: 1,
        display: "block"
      })), this.originalElement.css(e), this._proportionallyResize()), this._setupHandles(), i.autoHide && t(this.element).on("mouseenter", function () {
        i.disabled || (s._removeClass("ui-resizable-autohide"), s._handles.show());
      }).on("mouseleave", function () {
        i.disabled || s.resizing || (s._addClass("ui-resizable-autohide"), s._handles.hide());
      }), this._mouseInit();
    },
    _destroy: function () {
      this._mouseDestroy();

      var e,
          i = function (e) {
        t(e).removeData("resizable").removeData("ui-resizable").off(".resizable").find(".ui-resizable-handle").remove();
      };

      return this.elementIsWrapper && (i(this.element), e = this.element, this.originalElement.css({
        position: e.css("position"),
        width: e.outerWidth(),
        height: e.outerHeight(),
        top: e.css("top"),
        left: e.css("left")
      }).insertAfter(e), e.remove()), this.originalElement.css("resize", this.originalResizeStyle), i(this.originalElement), this;
    },
    _setOption: function (t, e) {
      switch (this._super(t, e), t) {
        case "handles":
          this._removeHandles(), this._setupHandles();
          break;

        default:
      }
    },
    _setupHandles: function () {
      var e,
          i,
          s,
          n,
          o,
          a = this.options,
          r = this;
      if (this.handles = a.handles || (t(".ui-resizable-handle", this.element).length ? {
        n: ".ui-resizable-n",
        e: ".ui-resizable-e",
        s: ".ui-resizable-s",
        w: ".ui-resizable-w",
        se: ".ui-resizable-se",
        sw: ".ui-resizable-sw",
        ne: ".ui-resizable-ne",
        nw: ".ui-resizable-nw"
      } : "e,s,se"), this._handles = t(), this.handles.constructor === String) for ("all" === this.handles && (this.handles = "n,e,s,w,se,sw,ne,nw"), s = this.handles.split(","), this.handles = {}, i = 0; s.length > i; i++) e = t.trim(s[i]), n = "ui-resizable-" + e, o = t("<div>"), this._addClass(o, "ui-resizable-handle " + n), o.css({
        zIndex: a.zIndex
      }), this.handles[e] = ".ui-resizable-" + e, this.element.append(o);
      this._renderAxis = function (e) {
        var i, s, n, o;
        e = e || this.element;

        for (i in this.handles) this.handles[i].constructor === String ? this.handles[i] = this.element.children(this.handles[i]).first().show() : (this.handles[i].jquery || this.handles[i].nodeType) && (this.handles[i] = t(this.handles[i]), this._on(this.handles[i], {
          mousedown: r._mouseDown
        })), this.elementIsWrapper && this.originalElement[0].nodeName.match(/^(textarea|input|select|button)$/i) && (s = t(this.handles[i], this.element), o = /sw|ne|nw|se|n|s/.test(i) ? s.outerHeight() : s.outerWidth(), n = ["padding", /ne|nw|n/.test(i) ? "Top" : /se|sw|s/.test(i) ? "Bottom" : /^e$/.test(i) ? "Right" : "Left"].join(""), e.css(n, o), this._proportionallyResize()), this._handles = this._handles.add(this.handles[i]);
      }, this._renderAxis(this.element), this._handles = this._handles.add(this.element.find(".ui-resizable-handle")), this._handles.disableSelection(), this._handles.on("mouseover", function () {
        r.resizing || (this.className && (o = this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)), r.axis = o && o[1] ? o[1] : "se");
      }), a.autoHide && (this._handles.hide(), this._addClass("ui-resizable-autohide"));
    },
    _removeHandles: function () {
      this._handles.remove();
    },
    _mouseCapture: function (e) {
      var i,
          s,
          n = !1;

      for (i in this.handles) s = t(this.handles[i])[0], (s === e.target || t.contains(s, e.target)) && (n = !0);

      return !this.options.disabled && n;
    },
    _mouseStart: function (e) {
      var i,
          s,
          n,
          o = this.options,
          a = this.element;
      return this.resizing = !0, this._renderProxy(), i = this._num(this.helper.css("left")), s = this._num(this.helper.css("top")), o.containment && (i += t(o.containment).scrollLeft() || 0, s += t(o.containment).scrollTop() || 0), this.offset = this.helper.offset(), this.position = {
        left: i,
        top: s
      }, this.size = this._helper ? {
        width: this.helper.width(),
        height: this.helper.height()
      } : {
        width: a.width(),
        height: a.height()
      }, this.originalSize = this._helper ? {
        width: a.outerWidth(),
        height: a.outerHeight()
      } : {
        width: a.width(),
        height: a.height()
      }, this.sizeDiff = {
        width: a.outerWidth() - a.width(),
        height: a.outerHeight() - a.height()
      }, this.originalPosition = {
        left: i,
        top: s
      }, this.originalMousePosition = {
        left: e.pageX,
        top: e.pageY
      }, this.aspectRatio = "number" == typeof o.aspectRatio ? o.aspectRatio : this.originalSize.width / this.originalSize.height || 1, n = t(".ui-resizable-" + this.axis).css("cursor"), t("body").css("cursor", "auto" === n ? this.axis + "-resize" : n), this._addClass("ui-resizable-resizing"), this._propagate("start", e), !0;
    },
    _mouseDrag: function (e) {
      var i,
          s,
          n = this.originalMousePosition,
          o = this.axis,
          a = e.pageX - n.left || 0,
          r = e.pageY - n.top || 0,
          h = this._change[o];
      return this._updatePrevProperties(), h ? (i = h.apply(this, [e, a, r]), this._updateVirtualBoundaries(e.shiftKey), (this._aspectRatio || e.shiftKey) && (i = this._updateRatio(i, e)), i = this._respectSize(i, e), this._updateCache(i), this._propagate("resize", e), s = this._applyChanges(), !this._helper && this._proportionallyResizeElements.length && this._proportionallyResize(), t.isEmptyObject(s) || (this._updatePrevProperties(), this._trigger("resize", e, this.ui()), this._applyChanges()), !1) : !1;
    },
    _mouseStop: function (e) {
      this.resizing = !1;
      var i,
          s,
          n,
          o,
          a,
          r,
          h,
          l = this.options,
          c = this;
      return this._helper && (i = this._proportionallyResizeElements, s = i.length && /textarea/i.test(i[0].nodeName), n = s && this._hasScroll(i[0], "left") ? 0 : c.sizeDiff.height, o = s ? 0 : c.sizeDiff.width, a = {
        width: c.helper.width() - o,
        height: c.helper.height() - n
      }, r = parseFloat(c.element.css("left")) + (c.position.left - c.originalPosition.left) || null, h = parseFloat(c.element.css("top")) + (c.position.top - c.originalPosition.top) || null, l.animate || this.element.css(t.extend(a, {
        top: h,
        left: r
      })), c.helper.height(c.size.height), c.helper.width(c.size.width), this._helper && !l.animate && this._proportionallyResize()), t("body").css("cursor", "auto"), this._removeClass("ui-resizable-resizing"), this._propagate("stop", e), this._helper && this.helper.remove(), !1;
    },
    _updatePrevProperties: function () {
      this.prevPosition = {
        top: this.position.top,
        left: this.position.left
      }, this.prevSize = {
        width: this.size.width,
        height: this.size.height
      };
    },
    _applyChanges: function () {
      var t = {};
      return this.position.top !== this.prevPosition.top && (t.top = this.position.top + "px"), this.position.left !== this.prevPosition.left && (t.left = this.position.left + "px"), this.size.width !== this.prevSize.width && (t.width = this.size.width + "px"), this.size.height !== this.prevSize.height && (t.height = this.size.height + "px"), this.helper.css(t), t;
    },
    _updateVirtualBoundaries: function (t) {
      var e,
          i,
          s,
          n,
          o,
          a = this.options;
      o = {
        minWidth: this._isNumber(a.minWidth) ? a.minWidth : 0,
        maxWidth: this._isNumber(a.maxWidth) ? a.maxWidth : 1 / 0,
        minHeight: this._isNumber(a.minHeight) ? a.minHeight : 0,
        maxHeight: this._isNumber(a.maxHeight) ? a.maxHeight : 1 / 0
      }, (this._aspectRatio || t) && (e = o.minHeight * this.aspectRatio, s = o.minWidth / this.aspectRatio, i = o.maxHeight * this.aspectRatio, n = o.maxWidth / this.aspectRatio, e > o.minWidth && (o.minWidth = e), s > o.minHeight && (o.minHeight = s), o.maxWidth > i && (o.maxWidth = i), o.maxHeight > n && (o.maxHeight = n)), this._vBoundaries = o;
    },
    _updateCache: function (t) {
      this.offset = this.helper.offset(), this._isNumber(t.left) && (this.position.left = t.left), this._isNumber(t.top) && (this.position.top = t.top), this._isNumber(t.height) && (this.size.height = t.height), this._isNumber(t.width) && (this.size.width = t.width);
    },
    _updateRatio: function (t) {
      var e = this.position,
          i = this.size,
          s = this.axis;
      return this._isNumber(t.height) ? t.width = t.height * this.aspectRatio : this._isNumber(t.width) && (t.height = t.width / this.aspectRatio), "sw" === s && (t.left = e.left + (i.width - t.width), t.top = null), "nw" === s && (t.top = e.top + (i.height - t.height), t.left = e.left + (i.width - t.width)), t;
    },
    _respectSize: function (t) {
      var e = this._vBoundaries,
          i = this.axis,
          s = this._isNumber(t.width) && e.maxWidth && e.maxWidth < t.width,
          n = this._isNumber(t.height) && e.maxHeight && e.maxHeight < t.height,
          o = this._isNumber(t.width) && e.minWidth && e.minWidth > t.width,
          a = this._isNumber(t.height) && e.minHeight && e.minHeight > t.height,
          r = this.originalPosition.left + this.originalSize.width,
          h = this.originalPosition.top + this.originalSize.height,
          l = /sw|nw|w/.test(i),
          c = /nw|ne|n/.test(i);
      return o && (t.width = e.minWidth), a && (t.height = e.minHeight), s && (t.width = e.maxWidth), n && (t.height = e.maxHeight), o && l && (t.left = r - e.minWidth), s && l && (t.left = r - e.maxWidth), a && c && (t.top = h - e.minHeight), n && c && (t.top = h - e.maxHeight), t.width || t.height || t.left || !t.top ? t.width || t.height || t.top || !t.left || (t.left = null) : t.top = null, t;
    },
    _getPaddingPlusBorderDimensions: function (t) {
      for (var e = 0, i = [], s = [t.css("borderTopWidth"), t.css("borderRightWidth"), t.css("borderBottomWidth"), t.css("borderLeftWidth")], n = [t.css("paddingTop"), t.css("paddingRight"), t.css("paddingBottom"), t.css("paddingLeft")]; 4 > e; e++) i[e] = parseFloat(s[e]) || 0, i[e] += parseFloat(n[e]) || 0;

      return {
        height: i[0] + i[2],
        width: i[1] + i[3]
      };
    },
    _proportionallyResize: function () {
      if (this._proportionallyResizeElements.length) for (var t, e = 0, i = this.helper || this.element; this._proportionallyResizeElements.length > e; e++) t = this._proportionallyResizeElements[e], this.outerDimensions || (this.outerDimensions = this._getPaddingPlusBorderDimensions(t)), t.css({
        height: i.height() - this.outerDimensions.height || 0,
        width: i.width() - this.outerDimensions.width || 0
      });
    },
    _renderProxy: function () {
      var e = this.element,
          i = this.options;
      this.elementOffset = e.offset(), this._helper ? (this.helper = this.helper || t("<div style='overflow:hidden;'></div>"), this._addClass(this.helper, this._helper), this.helper.css({
        width: this.element.outerWidth(),
        height: this.element.outerHeight(),
        position: "absolute",
        left: this.elementOffset.left + "px",
        top: this.elementOffset.top + "px",
        zIndex: ++i.zIndex
      }), this.helper.appendTo("body").disableSelection()) : this.helper = this.element;
    },
    _change: {
      e: function (t, e) {
        return {
          width: this.originalSize.width + e
        };
      },
      w: function (t, e) {
        var i = this.originalSize,
            s = this.originalPosition;
        return {
          left: s.left + e,
          width: i.width - e
        };
      },
      n: function (t, e, i) {
        var s = this.originalSize,
            n = this.originalPosition;
        return {
          top: n.top + i,
          height: s.height - i
        };
      },
      s: function (t, e, i) {
        return {
          height: this.originalSize.height + i
        };
      },
      se: function (e, i, s) {
        return t.extend(this._change.s.apply(this, arguments), this._change.e.apply(this, [e, i, s]));
      },
      sw: function (e, i, s) {
        return t.extend(this._change.s.apply(this, arguments), this._change.w.apply(this, [e, i, s]));
      },
      ne: function (e, i, s) {
        return t.extend(this._change.n.apply(this, arguments), this._change.e.apply(this, [e, i, s]));
      },
      nw: function (e, i, s) {
        return t.extend(this._change.n.apply(this, arguments), this._change.w.apply(this, [e, i, s]));
      }
    },
    _propagate: function (e, i) {
      t.ui.plugin.call(this, e, [i, this.ui()]), "resize" !== e && this._trigger(e, i, this.ui());
    },
    plugins: {},
    ui: function () {
      return {
        originalElement: this.originalElement,
        element: this.element,
        helper: this.helper,
        position: this.position,
        size: this.size,
        originalSize: this.originalSize,
        originalPosition: this.originalPosition
      };
    }
  }), t.ui.plugin.add("resizable", "animate", {
    stop: function (e) {
      var i = t(this).resizable("instance"),
          s = i.options,
          n = i._proportionallyResizeElements,
          o = n.length && /textarea/i.test(n[0].nodeName),
          a = o && i._hasScroll(n[0], "left") ? 0 : i.sizeDiff.height,
          r = o ? 0 : i.sizeDiff.width,
          h = {
        width: i.size.width - r,
        height: i.size.height - a
      },
          l = parseFloat(i.element.css("left")) + (i.position.left - i.originalPosition.left) || null,
          c = parseFloat(i.element.css("top")) + (i.position.top - i.originalPosition.top) || null;
      i.element.animate(t.extend(h, c && l ? {
        top: c,
        left: l
      } : {}), {
        duration: s.animateDuration,
        easing: s.animateEasing,
        step: function () {
          var s = {
            width: parseFloat(i.element.css("width")),
            height: parseFloat(i.element.css("height")),
            top: parseFloat(i.element.css("top")),
            left: parseFloat(i.element.css("left"))
          };
          n && n.length && t(n[0]).css({
            width: s.width,
            height: s.height
          }), i._updateCache(s), i._propagate("resize", e);
        }
      });
    }
  }), t.ui.plugin.add("resizable", "containment", {
    start: function () {
      var e,
          i,
          s,
          n,
          o,
          a,
          r,
          h = t(this).resizable("instance"),
          l = h.options,
          c = h.element,
          u = l.containment,
          d = u instanceof t ? u.get(0) : /parent/.test(u) ? c.parent().get(0) : u;
      d && (h.containerElement = t(d), /document/.test(u) || u === document ? (h.containerOffset = {
        left: 0,
        top: 0
      }, h.containerPosition = {
        left: 0,
        top: 0
      }, h.parentData = {
        element: t(document),
        left: 0,
        top: 0,
        width: t(document).width(),
        height: t(document).height() || document.body.parentNode.scrollHeight
      }) : (e = t(d), i = [], t(["Top", "Right", "Left", "Bottom"]).each(function (t, s) {
        i[t] = h._num(e.css("padding" + s));
      }), h.containerOffset = e.offset(), h.containerPosition = e.position(), h.containerSize = {
        height: e.innerHeight() - i[3],
        width: e.innerWidth() - i[1]
      }, s = h.containerOffset, n = h.containerSize.height, o = h.containerSize.width, a = h._hasScroll(d, "left") ? d.scrollWidth : o, r = h._hasScroll(d) ? d.scrollHeight : n, h.parentData = {
        element: d,
        left: s.left,
        top: s.top,
        width: a,
        height: r
      }));
    },
    resize: function (e) {
      var i,
          s,
          n,
          o,
          a = t(this).resizable("instance"),
          r = a.options,
          h = a.containerOffset,
          l = a.position,
          c = a._aspectRatio || e.shiftKey,
          u = {
        top: 0,
        left: 0
      },
          d = a.containerElement,
          p = !0;
      d[0] !== document && /static/.test(d.css("position")) && (u = h), l.left < (a._helper ? h.left : 0) && (a.size.width = a.size.width + (a._helper ? a.position.left - h.left : a.position.left - u.left), c && (a.size.height = a.size.width / a.aspectRatio, p = !1), a.position.left = r.helper ? h.left : 0), l.top < (a._helper ? h.top : 0) && (a.size.height = a.size.height + (a._helper ? a.position.top - h.top : a.position.top), c && (a.size.width = a.size.height * a.aspectRatio, p = !1), a.position.top = a._helper ? h.top : 0), n = a.containerElement.get(0) === a.element.parent().get(0), o = /relative|absolute/.test(a.containerElement.css("position")), n && o ? (a.offset.left = a.parentData.left + a.position.left, a.offset.top = a.parentData.top + a.position.top) : (a.offset.left = a.element.offset().left, a.offset.top = a.element.offset().top), i = Math.abs(a.sizeDiff.width + (a._helper ? a.offset.left - u.left : a.offset.left - h.left)), s = Math.abs(a.sizeDiff.height + (a._helper ? a.offset.top - u.top : a.offset.top - h.top)), i + a.size.width >= a.parentData.width && (a.size.width = a.parentData.width - i, c && (a.size.height = a.size.width / a.aspectRatio, p = !1)), s + a.size.height >= a.parentData.height && (a.size.height = a.parentData.height - s, c && (a.size.width = a.size.height * a.aspectRatio, p = !1)), p || (a.position.left = a.prevPosition.left, a.position.top = a.prevPosition.top, a.size.width = a.prevSize.width, a.size.height = a.prevSize.height);
    },
    stop: function () {
      var e = t(this).resizable("instance"),
          i = e.options,
          s = e.containerOffset,
          n = e.containerPosition,
          o = e.containerElement,
          a = t(e.helper),
          r = a.offset(),
          h = a.outerWidth() - e.sizeDiff.width,
          l = a.outerHeight() - e.sizeDiff.height;
      e._helper && !i.animate && /relative/.test(o.css("position")) && t(this).css({
        left: r.left - n.left - s.left,
        width: h,
        height: l
      }), e._helper && !i.animate && /static/.test(o.css("position")) && t(this).css({
        left: r.left - n.left - s.left,
        width: h,
        height: l
      });
    }
  }), t.ui.plugin.add("resizable", "alsoResize", {
    start: function () {
      var e = t(this).resizable("instance"),
          i = e.options;
      t(i.alsoResize).each(function () {
        var e = t(this);
        e.data("ui-resizable-alsoresize", {
          width: parseFloat(e.width()),
          height: parseFloat(e.height()),
          left: parseFloat(e.css("left")),
          top: parseFloat(e.css("top"))
        });
      });
    },
    resize: function (e, i) {
      var s = t(this).resizable("instance"),
          n = s.options,
          o = s.originalSize,
          a = s.originalPosition,
          r = {
        height: s.size.height - o.height || 0,
        width: s.size.width - o.width || 0,
        top: s.position.top - a.top || 0,
        left: s.position.left - a.left || 0
      };
      t(n.alsoResize).each(function () {
        var e = t(this),
            s = t(this).data("ui-resizable-alsoresize"),
            n = {},
            o = e.parents(i.originalElement[0]).length ? ["width", "height"] : ["width", "height", "top", "left"];
        t.each(o, function (t, e) {
          var i = (s[e] || 0) + (r[e] || 0);
          i && i >= 0 && (n[e] = i || null);
        }), e.css(n);
      });
    },
    stop: function () {
      t(this).removeData("ui-resizable-alsoresize");
    }
  }), t.ui.plugin.add("resizable", "ghost", {
    start: function () {
      var e = t(this).resizable("instance"),
          i = e.size;
      e.ghost = e.originalElement.clone(), e.ghost.css({
        opacity: .25,
        display: "block",
        position: "relative",
        height: i.height,
        width: i.width,
        margin: 0,
        left: 0,
        top: 0
      }), e._addClass(e.ghost, "ui-resizable-ghost"), t.uiBackCompat !== !1 && "string" == typeof e.options.ghost && e.ghost.addClass(this.options.ghost), e.ghost.appendTo(e.helper);
    },
    resize: function () {
      var e = t(this).resizable("instance");
      e.ghost && e.ghost.css({
        position: "relative",
        height: e.size.height,
        width: e.size.width
      });
    },
    stop: function () {
      var e = t(this).resizable("instance");
      e.ghost && e.helper && e.helper.get(0).removeChild(e.ghost.get(0));
    }
  }), t.ui.plugin.add("resizable", "grid", {
    resize: function () {
      var e,
          i = t(this).resizable("instance"),
          s = i.options,
          n = i.size,
          o = i.originalSize,
          a = i.originalPosition,
          r = i.axis,
          h = "number" == typeof s.grid ? [s.grid, s.grid] : s.grid,
          l = h[0] || 1,
          c = h[1] || 1,
          u = Math.round((n.width - o.width) / l) * l,
          d = Math.round((n.height - o.height) / c) * c,
          p = o.width + u,
          f = o.height + d,
          g = s.maxWidth && p > s.maxWidth,
          m = s.maxHeight && f > s.maxHeight,
          _ = s.minWidth && s.minWidth > p,
          v = s.minHeight && s.minHeight > f;

      s.grid = h, _ && (p += l), v && (f += c), g && (p -= l), m && (f -= c), /^(se|s|e)$/.test(r) ? (i.size.width = p, i.size.height = f) : /^(ne)$/.test(r) ? (i.size.width = p, i.size.height = f, i.position.top = a.top - d) : /^(sw)$/.test(r) ? (i.size.width = p, i.size.height = f, i.position.left = a.left - u) : ((0 >= f - c || 0 >= p - l) && (e = i._getPaddingPlusBorderDimensions(this)), f - c > 0 ? (i.size.height = f, i.position.top = a.top - d) : (f = c - e.height, i.size.height = f, i.position.top = a.top + o.height - f), p - l > 0 ? (i.size.width = p, i.position.left = a.left - u) : (p = l - e.width, i.size.width = p, i.position.left = a.left + o.width - p));
    }
  }), t.ui.resizable, t.widget("ui.dialog", {
    version: "1.12.1",
    options: {
      appendTo: "body",
      autoOpen: !0,
      buttons: [],
      classes: {
        "ui-dialog": "ui-corner-all",
        "ui-dialog-titlebar": "ui-corner-all"
      },
      closeOnEscape: !0,
      closeText: "Close",
      draggable: !0,
      hide: null,
      height: "auto",
      maxHeight: null,
      maxWidth: null,
      minHeight: 150,
      minWidth: 150,
      modal: !1,
      position: {
        my: "center",
        at: "center",
        of: window,
        collision: "fit",
        using: function (e) {
          var i = t(this).css(e).offset().top;
          0 > i && t(this).css("top", e.top - i);
        }
      },
      resizable: !0,
      show: null,
      title: null,
      width: 300,
      beforeClose: null,
      close: null,
      drag: null,
      dragStart: null,
      dragStop: null,
      focus: null,
      open: null,
      resize: null,
      resizeStart: null,
      resizeStop: null
    },
    sizeRelatedOptions: {
      buttons: !0,
      height: !0,
      maxHeight: !0,
      maxWidth: !0,
      minHeight: !0,
      minWidth: !0,
      width: !0
    },
    resizableRelatedOptions: {
      maxHeight: !0,
      maxWidth: !0,
      minHeight: !0,
      minWidth: !0
    },
    _create: function () {
      this.originalCss = {
        display: this.element[0].style.display,
        width: this.element[0].style.width,
        minHeight: this.element[0].style.minHeight,
        maxHeight: this.element[0].style.maxHeight,
        height: this.element[0].style.height
      }, this.originalPosition = {
        parent: this.element.parent(),
        index: this.element.parent().children().index(this.element)
      }, this.originalTitle = this.element.attr("title"), null == this.options.title && null != this.originalTitle && (this.options.title = this.originalTitle), this.options.disabled && (this.options.disabled = !1), this._createWrapper(), this.element.show().removeAttr("title").appendTo(this.uiDialog), this._addClass("ui-dialog-content", "ui-widget-content"), this._createTitlebar(), this._createButtonPane(), this.options.draggable && t.fn.draggable && this._makeDraggable(), this.options.resizable && t.fn.resizable && this._makeResizable(), this._isOpen = !1, this._trackFocus();
    },
    _init: function () {
      this.options.autoOpen && this.open();
    },
    _appendTo: function () {
      var e = this.options.appendTo;
      return e && (e.jquery || e.nodeType) ? t(e) : this.document.find(e || "body").eq(0);
    },
    _destroy: function () {
      var t,
          e = this.originalPosition;
      this._untrackInstance(), this._destroyOverlay(), this.element.removeUniqueId().css(this.originalCss).detach(), this.uiDialog.remove(), this.originalTitle && this.element.attr("title", this.originalTitle), t = e.parent.children().eq(e.index), t.length && t[0] !== this.element[0] ? t.before(this.element) : e.parent.append(this.element);
    },
    widget: function () {
      return this.uiDialog;
    },
    disable: t.noop,
    enable: t.noop,
    close: function (e) {
      var i = this;
      this._isOpen && this._trigger("beforeClose", e) !== !1 && (this._isOpen = !1, this._focusedElement = null, this._destroyOverlay(), this._untrackInstance(), this.opener.filter(":focusable").trigger("focus").length || t.ui.safeBlur(t.ui.safeActiveElement(this.document[0])), this._hide(this.uiDialog, this.options.hide, function () {
        i._trigger("close", e);
      }));
    },
    isOpen: function () {
      return this._isOpen;
    },
    moveToTop: function () {
      this._moveToTop();
    },
    _moveToTop: function (e, i) {
      var s = !1,
          n = this.uiDialog.siblings(".ui-front:visible").map(function () {
        return +t(this).css("z-index");
      }).get(),
          o = Math.max.apply(null, n);
      return o >= +this.uiDialog.css("z-index") && (this.uiDialog.css("z-index", o + 1), s = !0), s && !i && this._trigger("focus", e), s;
    },
    open: function () {
      var e = this;
      return this._isOpen ? (this._moveToTop() && this._focusTabbable(), void 0) : (this._isOpen = !0, this.opener = t(t.ui.safeActiveElement(this.document[0])), this._size(), this._position(), this._createOverlay(), this._moveToTop(null, !0), this.overlay && this.overlay.css("z-index", this.uiDialog.css("z-index") - 1), this._show(this.uiDialog, this.options.show, function () {
        e._focusTabbable(), e._trigger("focus");
      }), this._makeFocusTarget(), this._trigger("open"), void 0);
    },
    _focusTabbable: function () {
      var t = this._focusedElement;
      t || (t = this.element.find("[autofocus]")), t.length || (t = this.element.find(":tabbable")), t.length || (t = this.uiDialogButtonPane.find(":tabbable")), t.length || (t = this.uiDialogTitlebarClose.filter(":tabbable")), t.length || (t = this.uiDialog), t.eq(0).trigger("focus");
    },
    _keepFocus: function (e) {
      function i() {
        var e = t.ui.safeActiveElement(this.document[0]),
            i = this.uiDialog[0] === e || t.contains(this.uiDialog[0], e);
        i || this._focusTabbable();
      }

      e.preventDefault(), i.call(this), this._delay(i);
    },
    _createWrapper: function () {
      this.uiDialog = t("<div>").hide().attr({
        tabIndex: -1,
        role: "dialog"
      }).appendTo(this._appendTo()), this._addClass(this.uiDialog, "ui-dialog", "ui-widget ui-widget-content ui-front"), this._on(this.uiDialog, {
        keydown: function (e) {
          if (this.options.closeOnEscape && !e.isDefaultPrevented() && e.keyCode && e.keyCode === t.ui.keyCode.ESCAPE) return e.preventDefault(), this.close(e), void 0;

          if (e.keyCode === t.ui.keyCode.TAB && !e.isDefaultPrevented()) {
            var i = this.uiDialog.find(":tabbable"),
                s = i.filter(":first"),
                n = i.filter(":last");
            e.target !== n[0] && e.target !== this.uiDialog[0] || e.shiftKey ? e.target !== s[0] && e.target !== this.uiDialog[0] || !e.shiftKey || (this._delay(function () {
              n.trigger("focus");
            }), e.preventDefault()) : (this._delay(function () {
              s.trigger("focus");
            }), e.preventDefault());
          }
        },
        mousedown: function (t) {
          this._moveToTop(t) && this._focusTabbable();
        }
      }), this.element.find("[aria-describedby]").length || this.uiDialog.attr({
        "aria-describedby": this.element.uniqueId().attr("id")
      });
    },
    _createTitlebar: function () {
      var e;
      this.uiDialogTitlebar = t("<div>"), this._addClass(this.uiDialogTitlebar, "ui-dialog-titlebar", "ui-widget-header ui-helper-clearfix"), this._on(this.uiDialogTitlebar, {
        mousedown: function (e) {
          t(e.target).closest(".ui-dialog-titlebar-close") || this.uiDialog.trigger("focus");
        }
      }), this.uiDialogTitlebarClose = t("<button type='button'></button>").button({
        label: t("<a>").text(this.options.closeText).html(),
        icon: "ui-icon-closethick",
        showLabel: !1
      }).appendTo(this.uiDialogTitlebar), this._addClass(this.uiDialogTitlebarClose, "ui-dialog-titlebar-close"), this._on(this.uiDialogTitlebarClose, {
        click: function (t) {
          t.preventDefault(), this.close(t);
        }
      }), e = t("<span>").uniqueId().prependTo(this.uiDialogTitlebar), this._addClass(e, "ui-dialog-title"), this._title(e), this.uiDialogTitlebar.prependTo(this.uiDialog), this.uiDialog.attr({
        "aria-labelledby": e.attr("id")
      });
    },
    _title: function (t) {
      this.options.title ? t.text(this.options.title) : t.html("&#160;");
    },
    _createButtonPane: function () {
      this.uiDialogButtonPane = t("<div>"), this._addClass(this.uiDialogButtonPane, "ui-dialog-buttonpane", "ui-widget-content ui-helper-clearfix"), this.uiButtonSet = t("<div>").appendTo(this.uiDialogButtonPane), this._addClass(this.uiButtonSet, "ui-dialog-buttonset"), this._createButtons();
    },
    _createButtons: function () {
      var e = this,
          i = this.options.buttons;
      return this.uiDialogButtonPane.remove(), this.uiButtonSet.empty(), t.isEmptyObject(i) || t.isArray(i) && !i.length ? (this._removeClass(this.uiDialog, "ui-dialog-buttons"), void 0) : (t.each(i, function (i, s) {
        var n, o;
        s = t.isFunction(s) ? {
          click: s,
          text: i
        } : s, s = t.extend({
          type: "button"
        }, s), n = s.click, o = {
          icon: s.icon,
          iconPosition: s.iconPosition,
          showLabel: s.showLabel,
          icons: s.icons,
          text: s.text
        }, delete s.click, delete s.icon, delete s.iconPosition, delete s.showLabel, delete s.icons, "boolean" == typeof s.text && delete s.text, t("<button></button>", s).button(o).appendTo(e.uiButtonSet).on("click", function () {
          n.apply(e.element[0], arguments);
        });
      }), this._addClass(this.uiDialog, "ui-dialog-buttons"), this.uiDialogButtonPane.appendTo(this.uiDialog), void 0);
    },
    _makeDraggable: function () {
      function e(t) {
        return {
          position: t.position,
          offset: t.offset
        };
      }

      var i = this,
          s = this.options;
      this.uiDialog.draggable({
        cancel: ".ui-dialog-content, .ui-dialog-titlebar-close",
        handle: ".ui-dialog-titlebar",
        containment: "document",
        start: function (s, n) {
          i._addClass(t(this), "ui-dialog-dragging"), i._blockFrames(), i._trigger("dragStart", s, e(n));
        },
        drag: function (t, s) {
          i._trigger("drag", t, e(s));
        },
        stop: function (n, o) {
          var a = o.offset.left - i.document.scrollLeft(),
              r = o.offset.top - i.document.scrollTop();
          s.position = {
            my: "left top",
            at: "left" + (a >= 0 ? "+" : "") + a + " " + "top" + (r >= 0 ? "+" : "") + r,
            of: i.window
          }, i._removeClass(t(this), "ui-dialog-dragging"), i._unblockFrames(), i._trigger("dragStop", n, e(o));
        }
      });
    },
    _makeResizable: function () {
      function e(t) {
        return {
          originalPosition: t.originalPosition,
          originalSize: t.originalSize,
          position: t.position,
          size: t.size
        };
      }

      var i = this,
          s = this.options,
          n = s.resizable,
          o = this.uiDialog.css("position"),
          a = "string" == typeof n ? n : "n,e,s,w,se,sw,ne,nw";
      this.uiDialog.resizable({
        cancel: ".ui-dialog-content",
        containment: "document",
        alsoResize: this.element,
        maxWidth: s.maxWidth,
        maxHeight: s.maxHeight,
        minWidth: s.minWidth,
        minHeight: this._minHeight(),
        handles: a,
        start: function (s, n) {
          i._addClass(t(this), "ui-dialog-resizing"), i._blockFrames(), i._trigger("resizeStart", s, e(n));
        },
        resize: function (t, s) {
          i._trigger("resize", t, e(s));
        },
        stop: function (n, o) {
          var a = i.uiDialog.offset(),
              r = a.left - i.document.scrollLeft(),
              h = a.top - i.document.scrollTop();
          s.height = i.uiDialog.height(), s.width = i.uiDialog.width(), s.position = {
            my: "left top",
            at: "left" + (r >= 0 ? "+" : "") + r + " " + "top" + (h >= 0 ? "+" : "") + h,
            of: i.window
          }, i._removeClass(t(this), "ui-dialog-resizing"), i._unblockFrames(), i._trigger("resizeStop", n, e(o));
        }
      }).css("position", o);
    },
    _trackFocus: function () {
      this._on(this.widget(), {
        focusin: function (e) {
          this._makeFocusTarget(), this._focusedElement = t(e.target);
        }
      });
    },
    _makeFocusTarget: function () {
      this._untrackInstance(), this._trackingInstances().unshift(this);
    },
    _untrackInstance: function () {
      var e = this._trackingInstances(),
          i = t.inArray(this, e);

      -1 !== i && e.splice(i, 1);
    },
    _trackingInstances: function () {
      var t = this.document.data("ui-dialog-instances");
      return t || (t = [], this.document.data("ui-dialog-instances", t)), t;
    },
    _minHeight: function () {
      var t = this.options;
      return "auto" === t.height ? t.minHeight : Math.min(t.minHeight, t.height);
    },
    _position: function () {
      var t = this.uiDialog.is(":visible");
      t || this.uiDialog.show(), this.uiDialog.position(this.options.position), t || this.uiDialog.hide();
    },
    _setOptions: function (e) {
      var i = this,
          s = !1,
          n = {};
      t.each(e, function (t, e) {
        i._setOption(t, e), t in i.sizeRelatedOptions && (s = !0), t in i.resizableRelatedOptions && (n[t] = e);
      }), s && (this._size(), this._position()), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", n);
    },
    _setOption: function (e, i) {
      var s,
          n,
          o = this.uiDialog;
      "disabled" !== e && (this._super(e, i), "appendTo" === e && this.uiDialog.appendTo(this._appendTo()), "buttons" === e && this._createButtons(), "closeText" === e && this.uiDialogTitlebarClose.button({
        label: t("<a>").text("" + this.options.closeText).html()
      }), "draggable" === e && (s = o.is(":data(ui-draggable)"), s && !i && o.draggable("destroy"), !s && i && this._makeDraggable()), "position" === e && this._position(), "resizable" === e && (n = o.is(":data(ui-resizable)"), n && !i && o.resizable("destroy"), n && "string" == typeof i && o.resizable("option", "handles", i), n || i === !1 || this._makeResizable()), "title" === e && this._title(this.uiDialogTitlebar.find(".ui-dialog-title")));
    },
    _size: function () {
      var t,
          e,
          i,
          s = this.options;
      this.element.show().css({
        width: "auto",
        minHeight: 0,
        maxHeight: "none",
        height: 0
      }), s.minWidth > s.width && (s.width = s.minWidth), t = this.uiDialog.css({
        height: "auto",
        width: s.width
      }).outerHeight(), e = Math.max(0, s.minHeight - t), i = "number" == typeof s.maxHeight ? Math.max(0, s.maxHeight - t) : "none", "auto" === s.height ? this.element.css({
        minHeight: e,
        maxHeight: i,
        height: "auto"
      }) : this.element.height(Math.max(0, s.height - t)), this.uiDialog.is(":data(ui-resizable)") && this.uiDialog.resizable("option", "minHeight", this._minHeight());
    },
    _blockFrames: function () {
      this.iframeBlocks = this.document.find("iframe").map(function () {
        var e = t(this);
        return t("<div>").css({
          position: "absolute",
          width: e.outerWidth(),
          height: e.outerHeight()
        }).appendTo(e.parent()).offset(e.offset())[0];
      });
    },
    _unblockFrames: function () {
      this.iframeBlocks && (this.iframeBlocks.remove(), delete this.iframeBlocks);
    },
    _allowInteraction: function (e) {
      return t(e.target).closest(".ui-dialog").length ? !0 : !!t(e.target).closest(".ui-datepicker").length;
    },
    _createOverlay: function () {
      if (this.options.modal) {
        var e = !0;
        this._delay(function () {
          e = !1;
        }), this.document.data("ui-dialog-overlays") || this._on(this.document, {
          focusin: function (t) {
            e || this._allowInteraction(t) || (t.preventDefault(), this._trackingInstances()[0]._focusTabbable());
          }
        }), this.overlay = t("<div>").appendTo(this._appendTo()), this._addClass(this.overlay, null, "ui-widget-overlay ui-front"), this._on(this.overlay, {
          mousedown: "_keepFocus"
        }), this.document.data("ui-dialog-overlays", (this.document.data("ui-dialog-overlays") || 0) + 1);
      }
    },
    _destroyOverlay: function () {
      if (this.options.modal && this.overlay) {
        var t = this.document.data("ui-dialog-overlays") - 1;
        t ? this.document.data("ui-dialog-overlays", t) : (this._off(this.document, "focusin"), this.document.removeData("ui-dialog-overlays")), this.overlay.remove(), this.overlay = null;
      }
    }
  }), t.uiBackCompat !== !1 && t.widget("ui.dialog", t.ui.dialog, {
    options: {
      dialogClass: ""
    },
    _createWrapper: function () {
      this._super(), this.uiDialog.addClass(this.options.dialogClass);
    },
    _setOption: function (t, e) {
      "dialogClass" === t && this.uiDialog.removeClass(this.options.dialogClass).addClass(e), this._superApply(arguments);
    }
  }), t.ui.dialog, t.widget("ui.droppable", {
    version: "1.12.1",
    widgetEventPrefix: "drop",
    options: {
      accept: "*",
      addClasses: !0,
      greedy: !1,
      scope: "default",
      tolerance: "intersect",
      activate: null,
      deactivate: null,
      drop: null,
      out: null,
      over: null
    },
    _create: function () {
      var e,
          i = this.options,
          s = i.accept;
      this.isover = !1, this.isout = !0, this.accept = t.isFunction(s) ? s : function (t) {
        return t.is(s);
      }, this.proportions = function () {
        return arguments.length ? (e = arguments[0], void 0) : e ? e : e = {
          width: this.element[0].offsetWidth,
          height: this.element[0].offsetHeight
        };
      }, this._addToManager(i.scope), i.addClasses && this._addClass("ui-droppable");
    },
    _addToManager: function (e) {
      t.ui.ddmanager.droppables[e] = t.ui.ddmanager.droppables[e] || [], t.ui.ddmanager.droppables[e].push(this);
    },
    _splice: function (t) {
      for (var e = 0; t.length > e; e++) t[e] === this && t.splice(e, 1);
    },
    _destroy: function () {
      var e = t.ui.ddmanager.droppables[this.options.scope];

      this._splice(e);
    },
    _setOption: function (e, i) {
      if ("accept" === e) this.accept = t.isFunction(i) ? i : function (t) {
        return t.is(i);
      };else if ("scope" === e) {
        var s = t.ui.ddmanager.droppables[this.options.scope];
        this._splice(s), this._addToManager(i);
      }

      this._super(e, i);
    },
    _activate: function (e) {
      var i = t.ui.ddmanager.current;
      this._addActiveClass(), i && this._trigger("activate", e, this.ui(i));
    },
    _deactivate: function (e) {
      var i = t.ui.ddmanager.current;
      this._removeActiveClass(), i && this._trigger("deactivate", e, this.ui(i));
    },
    _over: function (e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._addHoverClass(), this._trigger("over", e, this.ui(i)));
    },
    _out: function (e) {
      var i = t.ui.ddmanager.current;
      i && (i.currentItem || i.element)[0] !== this.element[0] && this.accept.call(this.element[0], i.currentItem || i.element) && (this._removeHoverClass(), this._trigger("out", e, this.ui(i)));
    },
    _drop: function (e, i) {
      var s = i || t.ui.ddmanager.current,
          n = !1;
      return s && (s.currentItem || s.element)[0] !== this.element[0] ? (this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function () {
        var i = t(this).droppable("instance");
        return i.options.greedy && !i.options.disabled && i.options.scope === s.options.scope && i.accept.call(i.element[0], s.currentItem || s.element) && v(s, t.extend(i, {
          offset: i.element.offset()
        }), i.options.tolerance, e) ? (n = !0, !1) : void 0;
      }), n ? !1 : this.accept.call(this.element[0], s.currentItem || s.element) ? (this._removeActiveClass(), this._removeHoverClass(), this._trigger("drop", e, this.ui(s)), this.element) : !1) : !1;
    },
    ui: function (t) {
      return {
        draggable: t.currentItem || t.element,
        helper: t.helper,
        position: t.position,
        offset: t.positionAbs
      };
    },
    _addHoverClass: function () {
      this._addClass("ui-droppable-hover");
    },
    _removeHoverClass: function () {
      this._removeClass("ui-droppable-hover");
    },
    _addActiveClass: function () {
      this._addClass("ui-droppable-active");
    },
    _removeActiveClass: function () {
      this._removeClass("ui-droppable-active");
    }
  });

  var v = t.ui.intersect = function () {
    function t(t, e, i) {
      return t >= e && e + i > t;
    }

    return function (e, i, s, n) {
      if (!i.offset) return !1;
      var o = (e.positionAbs || e.position.absolute).left + e.margins.left,
          a = (e.positionAbs || e.position.absolute).top + e.margins.top,
          r = o + e.helperProportions.width,
          h = a + e.helperProportions.height,
          l = i.offset.left,
          c = i.offset.top,
          u = l + i.proportions().width,
          d = c + i.proportions().height;

      switch (s) {
        case "fit":
          return o >= l && u >= r && a >= c && d >= h;

        case "intersect":
          return o + e.helperProportions.width / 2 > l && u > r - e.helperProportions.width / 2 && a + e.helperProportions.height / 2 > c && d > h - e.helperProportions.height / 2;

        case "pointer":
          return t(n.pageY, c, i.proportions().height) && t(n.pageX, l, i.proportions().width);

        case "touch":
          return (a >= c && d >= a || h >= c && d >= h || c > a && h > d) && (o >= l && u >= o || r >= l && u >= r || l > o && r > u);

        default:
          return !1;
      }
    };
  }();

  t.ui.ddmanager = {
    current: null,
    droppables: {
      "default": []
    },
    prepareOffsets: function (e, i) {
      var s,
          n,
          o = t.ui.ddmanager.droppables[e.options.scope] || [],
          a = i ? i.type : null,
          r = (e.currentItem || e.element).find(":data(ui-droppable)").addBack();

      t: for (s = 0; o.length > s; s++) if (!(o[s].options.disabled || e && !o[s].accept.call(o[s].element[0], e.currentItem || e.element))) {
        for (n = 0; r.length > n; n++) if (r[n] === o[s].element[0]) {
          o[s].proportions().height = 0;
          continue t;
        }

        o[s].visible = "none" !== o[s].element.css("display"), o[s].visible && ("mousedown" === a && o[s]._activate.call(o[s], i), o[s].offset = o[s].element.offset(), o[s].proportions({
          width: o[s].element[0].offsetWidth,
          height: o[s].element[0].offsetHeight
        }));
      }
    },
    drop: function (e, i) {
      var s = !1;
      return t.each((t.ui.ddmanager.droppables[e.options.scope] || []).slice(), function () {
        this.options && (!this.options.disabled && this.visible && v(e, this, this.options.tolerance, i) && (s = this._drop.call(this, i) || s), !this.options.disabled && this.visible && this.accept.call(this.element[0], e.currentItem || e.element) && (this.isout = !0, this.isover = !1, this._deactivate.call(this, i)));
      }), s;
    },
    dragStart: function (e, i) {
      e.element.parentsUntil("body").on("scroll.droppable", function () {
        e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i);
      });
    },
    drag: function (e, i) {
      e.options.refreshPositions && t.ui.ddmanager.prepareOffsets(e, i), t.each(t.ui.ddmanager.droppables[e.options.scope] || [], function () {
        if (!this.options.disabled && !this.greedyChild && this.visible) {
          var s,
              n,
              o,
              a = v(e, this, this.options.tolerance, i),
              r = !a && this.isover ? "isout" : a && !this.isover ? "isover" : null;
          r && (this.options.greedy && (n = this.options.scope, o = this.element.parents(":data(ui-droppable)").filter(function () {
            return t(this).droppable("instance").options.scope === n;
          }), o.length && (s = t(o[0]).droppable("instance"), s.greedyChild = "isover" === r)), s && "isover" === r && (s.isover = !1, s.isout = !0, s._out.call(s, i)), this[r] = !0, this["isout" === r ? "isover" : "isout"] = !1, this["isover" === r ? "_over" : "_out"].call(this, i), s && "isout" === r && (s.isout = !1, s.isover = !0, s._over.call(s, i)));
        }
      });
    },
    dragStop: function (e, i) {
      e.element.parentsUntil("body").off("scroll.droppable"), e.options.refreshPositions || t.ui.ddmanager.prepareOffsets(e, i);
    }
  }, t.uiBackCompat !== !1 && t.widget("ui.droppable", t.ui.droppable, {
    options: {
      hoverClass: !1,
      activeClass: !1
    },
    _addActiveClass: function () {
      this._super(), this.options.activeClass && this.element.addClass(this.options.activeClass);
    },
    _removeActiveClass: function () {
      this._super(), this.options.activeClass && this.element.removeClass(this.options.activeClass);
    },
    _addHoverClass: function () {
      this._super(), this.options.hoverClass && this.element.addClass(this.options.hoverClass);
    },
    _removeHoverClass: function () {
      this._super(), this.options.hoverClass && this.element.removeClass(this.options.hoverClass);
    }
  }), t.ui.droppable, t.widget("ui.progressbar", {
    version: "1.12.1",
    options: {
      classes: {
        "ui-progressbar": "ui-corner-all",
        "ui-progressbar-value": "ui-corner-left",
        "ui-progressbar-complete": "ui-corner-right"
      },
      max: 100,
      value: 0,
      change: null,
      complete: null
    },
    min: 0,
    _create: function () {
      this.oldValue = this.options.value = this._constrainedValue(), this.element.attr({
        role: "progressbar",
        "aria-valuemin": this.min
      }), this._addClass("ui-progressbar", "ui-widget ui-widget-content"), this.valueDiv = t("<div>").appendTo(this.element), this._addClass(this.valueDiv, "ui-progressbar-value", "ui-widget-header"), this._refreshValue();
    },
    _destroy: function () {
      this.element.removeAttr("role aria-valuemin aria-valuemax aria-valuenow"), this.valueDiv.remove();
    },
    value: function (t) {
      return void 0 === t ? this.options.value : (this.options.value = this._constrainedValue(t), this._refreshValue(), void 0);
    },
    _constrainedValue: function (t) {
      return void 0 === t && (t = this.options.value), this.indeterminate = t === !1, "number" != typeof t && (t = 0), this.indeterminate ? !1 : Math.min(this.options.max, Math.max(this.min, t));
    },
    _setOptions: function (t) {
      var e = t.value;
      delete t.value, this._super(t), this.options.value = this._constrainedValue(e), this._refreshValue();
    },
    _setOption: function (t, e) {
      "max" === t && (e = Math.max(this.min, e)), this._super(t, e);
    },
    _setOptionDisabled: function (t) {
      this._super(t), this.element.attr("aria-disabled", t), this._toggleClass(null, "ui-state-disabled", !!t);
    },
    _percentage: function () {
      return this.indeterminate ? 100 : 100 * (this.options.value - this.min) / (this.options.max - this.min);
    },
    _refreshValue: function () {
      var e = this.options.value,
          i = this._percentage();

      this.valueDiv.toggle(this.indeterminate || e > this.min).width(i.toFixed(0) + "%"), this._toggleClass(this.valueDiv, "ui-progressbar-complete", null, e === this.options.max)._toggleClass("ui-progressbar-indeterminate", null, this.indeterminate), this.indeterminate ? (this.element.removeAttr("aria-valuenow"), this.overlayDiv || (this.overlayDiv = t("<div>").appendTo(this.valueDiv), this._addClass(this.overlayDiv, "ui-progressbar-overlay"))) : (this.element.attr({
        "aria-valuemax": this.options.max,
        "aria-valuenow": e
      }), this.overlayDiv && (this.overlayDiv.remove(), this.overlayDiv = null)), this.oldValue !== e && (this.oldValue = e, this._trigger("change")), e === this.options.max && this._trigger("complete");
    }
  }), t.widget("ui.selectable", t.ui.mouse, {
    version: "1.12.1",
    options: {
      appendTo: "body",
      autoRefresh: !0,
      distance: 0,
      filter: "*",
      tolerance: "touch",
      selected: null,
      selecting: null,
      start: null,
      stop: null,
      unselected: null,
      unselecting: null
    },
    _create: function () {
      var e = this;
      this._addClass("ui-selectable"), this.dragged = !1, this.refresh = function () {
        e.elementPos = t(e.element[0]).offset(), e.selectees = t(e.options.filter, e.element[0]), e._addClass(e.selectees, "ui-selectee"), e.selectees.each(function () {
          var i = t(this),
              s = i.offset(),
              n = {
            left: s.left - e.elementPos.left,
            top: s.top - e.elementPos.top
          };
          t.data(this, "selectable-item", {
            element: this,
            $element: i,
            left: n.left,
            top: n.top,
            right: n.left + i.outerWidth(),
            bottom: n.top + i.outerHeight(),
            startselected: !1,
            selected: i.hasClass("ui-selected"),
            selecting: i.hasClass("ui-selecting"),
            unselecting: i.hasClass("ui-unselecting")
          });
        });
      }, this.refresh(), this._mouseInit(), this.helper = t("<div>"), this._addClass(this.helper, "ui-selectable-helper");
    },
    _destroy: function () {
      this.selectees.removeData("selectable-item"), this._mouseDestroy();
    },
    _mouseStart: function (e) {
      var i = this,
          s = this.options;
      this.opos = [e.pageX, e.pageY], this.elementPos = t(this.element[0]).offset(), this.options.disabled || (this.selectees = t(s.filter, this.element[0]), this._trigger("start", e), t(s.appendTo).append(this.helper), this.helper.css({
        left: e.pageX,
        top: e.pageY,
        width: 0,
        height: 0
      }), s.autoRefresh && this.refresh(), this.selectees.filter(".ui-selected").each(function () {
        var s = t.data(this, "selectable-item");
        s.startselected = !0, e.metaKey || e.ctrlKey || (i._removeClass(s.$element, "ui-selected"), s.selected = !1, i._addClass(s.$element, "ui-unselecting"), s.unselecting = !0, i._trigger("unselecting", e, {
          unselecting: s.element
        }));
      }), t(e.target).parents().addBack().each(function () {
        var s,
            n = t.data(this, "selectable-item");
        return n ? (s = !e.metaKey && !e.ctrlKey || !n.$element.hasClass("ui-selected"), i._removeClass(n.$element, s ? "ui-unselecting" : "ui-selected")._addClass(n.$element, s ? "ui-selecting" : "ui-unselecting"), n.unselecting = !s, n.selecting = s, n.selected = s, s ? i._trigger("selecting", e, {
          selecting: n.element
        }) : i._trigger("unselecting", e, {
          unselecting: n.element
        }), !1) : void 0;
      }));
    },
    _mouseDrag: function (e) {
      if (this.dragged = !0, !this.options.disabled) {
        var i,
            s = this,
            n = this.options,
            o = this.opos[0],
            a = this.opos[1],
            r = e.pageX,
            h = e.pageY;
        return o > r && (i = r, r = o, o = i), a > h && (i = h, h = a, a = i), this.helper.css({
          left: o,
          top: a,
          width: r - o,
          height: h - a
        }), this.selectees.each(function () {
          var i = t.data(this, "selectable-item"),
              l = !1,
              c = {};
          i && i.element !== s.element[0] && (c.left = i.left + s.elementPos.left, c.right = i.right + s.elementPos.left, c.top = i.top + s.elementPos.top, c.bottom = i.bottom + s.elementPos.top, "touch" === n.tolerance ? l = !(c.left > r || o > c.right || c.top > h || a > c.bottom) : "fit" === n.tolerance && (l = c.left > o && r > c.right && c.top > a && h > c.bottom), l ? (i.selected && (s._removeClass(i.$element, "ui-selected"), i.selected = !1), i.unselecting && (s._removeClass(i.$element, "ui-unselecting"), i.unselecting = !1), i.selecting || (s._addClass(i.$element, "ui-selecting"), i.selecting = !0, s._trigger("selecting", e, {
            selecting: i.element
          }))) : (i.selecting && ((e.metaKey || e.ctrlKey) && i.startselected ? (s._removeClass(i.$element, "ui-selecting"), i.selecting = !1, s._addClass(i.$element, "ui-selected"), i.selected = !0) : (s._removeClass(i.$element, "ui-selecting"), i.selecting = !1, i.startselected && (s._addClass(i.$element, "ui-unselecting"), i.unselecting = !0), s._trigger("unselecting", e, {
            unselecting: i.element
          }))), i.selected && (e.metaKey || e.ctrlKey || i.startselected || (s._removeClass(i.$element, "ui-selected"), i.selected = !1, s._addClass(i.$element, "ui-unselecting"), i.unselecting = !0, s._trigger("unselecting", e, {
            unselecting: i.element
          })))));
        }), !1;
      }
    },
    _mouseStop: function (e) {
      var i = this;
      return this.dragged = !1, t(".ui-unselecting", this.element[0]).each(function () {
        var s = t.data(this, "selectable-item");
        i._removeClass(s.$element, "ui-unselecting"), s.unselecting = !1, s.startselected = !1, i._trigger("unselected", e, {
          unselected: s.element
        });
      }), t(".ui-selecting", this.element[0]).each(function () {
        var s = t.data(this, "selectable-item");
        i._removeClass(s.$element, "ui-selecting")._addClass(s.$element, "ui-selected"), s.selecting = !1, s.selected = !0, s.startselected = !0, i._trigger("selected", e, {
          selected: s.element
        });
      }), this._trigger("stop", e), this.helper.remove(), !1;
    }
  }), t.widget("ui.selectmenu", [t.ui.formResetMixin, {
    version: "1.12.1",
    defaultElement: "<select>",
    options: {
      appendTo: null,
      classes: {
        "ui-selectmenu-button-open": "ui-corner-top",
        "ui-selectmenu-button-closed": "ui-corner-all"
      },
      disabled: null,
      icons: {
        button: "ui-icon-triangle-1-s"
      },
      position: {
        my: "left top",
        at: "left bottom",
        collision: "none"
      },
      width: !1,
      change: null,
      close: null,
      focus: null,
      open: null,
      select: null
    },
    _create: function () {
      var e = this.element.uniqueId().attr("id");
      this.ids = {
        element: e,
        button: e + "-button",
        menu: e + "-menu"
      }, this._drawButton(), this._drawMenu(), this._bindFormResetHandler(), this._rendered = !1, this.menuItems = t();
    },
    _drawButton: function () {
      var e,
          i = this,
          s = this._parseOption(this.element.find("option:selected"), this.element[0].selectedIndex);

      this.labels = this.element.labels().attr("for", this.ids.button), this._on(this.labels, {
        click: function (t) {
          this.button.focus(), t.preventDefault();
        }
      }), this.element.hide(), this.button = t("<span>", {
        tabindex: this.options.disabled ? -1 : 0,
        id: this.ids.button,
        role: "combobox",
        "aria-expanded": "false",
        "aria-autocomplete": "list",
        "aria-owns": this.ids.menu,
        "aria-haspopup": "true",
        title: this.element.attr("title")
      }).insertAfter(this.element), this._addClass(this.button, "ui-selectmenu-button ui-selectmenu-button-closed", "ui-button ui-widget"), e = t("<span>").appendTo(this.button), this._addClass(e, "ui-selectmenu-icon", "ui-icon " + this.options.icons.button), this.buttonItem = this._renderButtonItem(s).appendTo(this.button), this.options.width !== !1 && this._resizeButton(), this._on(this.button, this._buttonEvents), this.button.one("focusin", function () {
        i._rendered || i._refreshMenu();
      });
    },
    _drawMenu: function () {
      var e = this;
      this.menu = t("<ul>", {
        "aria-hidden": "true",
        "aria-labelledby": this.ids.button,
        id: this.ids.menu
      }), this.menuWrap = t("<div>").append(this.menu), this._addClass(this.menuWrap, "ui-selectmenu-menu", "ui-front"), this.menuWrap.appendTo(this._appendTo()), this.menuInstance = this.menu.menu({
        classes: {
          "ui-menu": "ui-corner-bottom"
        },
        role: "listbox",
        select: function (t, i) {
          t.preventDefault(), e._setSelection(), e._select(i.item.data("ui-selectmenu-item"), t);
        },
        focus: function (t, i) {
          var s = i.item.data("ui-selectmenu-item");
          null != e.focusIndex && s.index !== e.focusIndex && (e._trigger("focus", t, {
            item: s
          }), e.isOpen || e._select(s, t)), e.focusIndex = s.index, e.button.attr("aria-activedescendant", e.menuItems.eq(s.index).attr("id"));
        }
      }).menu("instance"), this.menuInstance._off(this.menu, "mouseleave"), this.menuInstance._closeOnDocumentClick = function () {
        return !1;
      }, this.menuInstance._isDivider = function () {
        return !1;
      };
    },
    refresh: function () {
      this._refreshMenu(), this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(this._getSelectedItem().data("ui-selectmenu-item") || {})), null === this.options.width && this._resizeButton();
    },
    _refreshMenu: function () {
      var t,
          e = this.element.find("option");
      this.menu.empty(), this._parseOptions(e), this._renderMenu(this.menu, this.items), this.menuInstance.refresh(), this.menuItems = this.menu.find("li").not(".ui-selectmenu-optgroup").find(".ui-menu-item-wrapper"), this._rendered = !0, e.length && (t = this._getSelectedItem(), this.menuInstance.focus(null, t), this._setAria(t.data("ui-selectmenu-item")), this._setOption("disabled", this.element.prop("disabled")));
    },
    open: function (t) {
      this.options.disabled || (this._rendered ? (this._removeClass(this.menu.find(".ui-state-active"), null, "ui-state-active"), this.menuInstance.focus(null, this._getSelectedItem())) : this._refreshMenu(), this.menuItems.length && (this.isOpen = !0, this._toggleAttr(), this._resizeMenu(), this._position(), this._on(this.document, this._documentClick), this._trigger("open", t)));
    },
    _position: function () {
      this.menuWrap.position(t.extend({
        of: this.button
      }, this.options.position));
    },
    close: function (t) {
      this.isOpen && (this.isOpen = !1, this._toggleAttr(), this.range = null, this._off(this.document), this._trigger("close", t));
    },
    widget: function () {
      return this.button;
    },
    menuWidget: function () {
      return this.menu;
    },
    _renderButtonItem: function (e) {
      var i = t("<span>");
      return this._setText(i, e.label), this._addClass(i, "ui-selectmenu-text"), i;
    },
    _renderMenu: function (e, i) {
      var s = this,
          n = "";
      t.each(i, function (i, o) {
        var a;
        o.optgroup !== n && (a = t("<li>", {
          text: o.optgroup
        }), s._addClass(a, "ui-selectmenu-optgroup", "ui-menu-divider" + (o.element.parent("optgroup").prop("disabled") ? " ui-state-disabled" : "")), a.appendTo(e), n = o.optgroup), s._renderItemData(e, o);
      });
    },
    _renderItemData: function (t, e) {
      return this._renderItem(t, e).data("ui-selectmenu-item", e);
    },
    _renderItem: function (e, i) {
      var s = t("<li>"),
          n = t("<div>", {
        title: i.element.attr("title")
      });
      return i.disabled && this._addClass(s, null, "ui-state-disabled"), this._setText(n, i.label), s.append(n).appendTo(e);
    },
    _setText: function (t, e) {
      e ? t.text(e) : t.html("&#160;");
    },
    _move: function (t, e) {
      var i,
          s,
          n = ".ui-menu-item";
      this.isOpen ? i = this.menuItems.eq(this.focusIndex).parent("li") : (i = this.menuItems.eq(this.element[0].selectedIndex).parent("li"), n += ":not(.ui-state-disabled)"), s = "first" === t || "last" === t ? i["first" === t ? "prevAll" : "nextAll"](n).eq(-1) : i[t + "All"](n).eq(0), s.length && this.menuInstance.focus(e, s);
    },
    _getSelectedItem: function () {
      return this.menuItems.eq(this.element[0].selectedIndex).parent("li");
    },
    _toggle: function (t) {
      this[this.isOpen ? "close" : "open"](t);
    },
    _setSelection: function () {
      var t;
      this.range && (window.getSelection ? (t = window.getSelection(), t.removeAllRanges(), t.addRange(this.range)) : this.range.select(), this.button.focus());
    },
    _documentClick: {
      mousedown: function (e) {
        this.isOpen && (t(e.target).closest(".ui-selectmenu-menu, #" + t.ui.escapeSelector(this.ids.button)).length || this.close(e));
      }
    },
    _buttonEvents: {
      mousedown: function () {
        var t;
        window.getSelection ? (t = window.getSelection(), t.rangeCount && (this.range = t.getRangeAt(0))) : this.range = document.selection.createRange();
      },
      click: function (t) {
        this._setSelection(), this._toggle(t);
      },
      keydown: function (e) {
        var i = !0;

        switch (e.keyCode) {
          case t.ui.keyCode.TAB:
          case t.ui.keyCode.ESCAPE:
            this.close(e), i = !1;
            break;

          case t.ui.keyCode.ENTER:
            this.isOpen && this._selectFocusedItem(e);
            break;

          case t.ui.keyCode.UP:
            e.altKey ? this._toggle(e) : this._move("prev", e);
            break;

          case t.ui.keyCode.DOWN:
            e.altKey ? this._toggle(e) : this._move("next", e);
            break;

          case t.ui.keyCode.SPACE:
            this.isOpen ? this._selectFocusedItem(e) : this._toggle(e);
            break;

          case t.ui.keyCode.LEFT:
            this._move("prev", e);

            break;

          case t.ui.keyCode.RIGHT:
            this._move("next", e);

            break;

          case t.ui.keyCode.HOME:
          case t.ui.keyCode.PAGE_UP:
            this._move("first", e);

            break;

          case t.ui.keyCode.END:
          case t.ui.keyCode.PAGE_DOWN:
            this._move("last", e);

            break;

          default:
            this.menu.trigger(e), i = !1;
        }

        i && e.preventDefault();
      }
    },
    _selectFocusedItem: function (t) {
      var e = this.menuItems.eq(this.focusIndex).parent("li");
      e.hasClass("ui-state-disabled") || this._select(e.data("ui-selectmenu-item"), t);
    },
    _select: function (t, e) {
      var i = this.element[0].selectedIndex;
      this.element[0].selectedIndex = t.index, this.buttonItem.replaceWith(this.buttonItem = this._renderButtonItem(t)), this._setAria(t), this._trigger("select", e, {
        item: t
      }), t.index !== i && this._trigger("change", e, {
        item: t
      }), this.close(e);
    },
    _setAria: function (t) {
      var e = this.menuItems.eq(t.index).attr("id");
      this.button.attr({
        "aria-labelledby": e,
        "aria-activedescendant": e
      }), this.menu.attr("aria-activedescendant", e);
    },
    _setOption: function (t, e) {
      if ("icons" === t) {
        var i = this.button.find("span.ui-icon");

        this._removeClass(i, null, this.options.icons.button)._addClass(i, null, e.button);
      }

      this._super(t, e), "appendTo" === t && this.menuWrap.appendTo(this._appendTo()), "width" === t && this._resizeButton();
    },
    _setOptionDisabled: function (t) {
      this._super(t), this.menuInstance.option("disabled", t), this.button.attr("aria-disabled", t), this._toggleClass(this.button, null, "ui-state-disabled", t), this.element.prop("disabled", t), t ? (this.button.attr("tabindex", -1), this.close()) : this.button.attr("tabindex", 0);
    },
    _appendTo: function () {
      var e = this.options.appendTo;
      return e && (e = e.jquery || e.nodeType ? t(e) : this.document.find(e).eq(0)), e && e[0] || (e = this.element.closest(".ui-front, dialog")), e.length || (e = this.document[0].body), e;
    },
    _toggleAttr: function () {
      this.button.attr("aria-expanded", this.isOpen), this._removeClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "closed" : "open"))._addClass(this.button, "ui-selectmenu-button-" + (this.isOpen ? "open" : "closed"))._toggleClass(this.menuWrap, "ui-selectmenu-open", null, this.isOpen), this.menu.attr("aria-hidden", !this.isOpen);
    },
    _resizeButton: function () {
      var t = this.options.width;
      return t === !1 ? (this.button.css("width", ""), void 0) : (null === t && (t = this.element.show().outerWidth(), this.element.hide()), this.button.outerWidth(t), void 0);
    },
    _resizeMenu: function () {
      this.menu.outerWidth(Math.max(this.button.outerWidth(), this.menu.width("").outerWidth() + 1));
    },
    _getCreateOptions: function () {
      var t = this._super();

      return t.disabled = this.element.prop("disabled"), t;
    },
    _parseOptions: function (e) {
      var i = this,
          s = [];
      e.each(function (e, n) {
        s.push(i._parseOption(t(n), e));
      }), this.items = s;
    },
    _parseOption: function (t, e) {
      var i = t.parent("optgroup");
      return {
        element: t,
        index: e,
        value: t.val(),
        label: t.text(),
        optgroup: i.attr("label") || "",
        disabled: i.prop("disabled") || t.prop("disabled")
      };
    },
    _destroy: function () {
      this._unbindFormResetHandler(), this.menuWrap.remove(), this.button.remove(), this.element.show(), this.element.removeUniqueId(), this.labels.attr("for", this.ids.element);
    }
  }]), t.widget("ui.slider", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "slide",
    options: {
      animate: !1,
      classes: {
        "ui-slider": "ui-corner-all",
        "ui-slider-handle": "ui-corner-all",
        "ui-slider-range": "ui-corner-all ui-widget-header"
      },
      distance: 0,
      max: 100,
      min: 0,
      orientation: "horizontal",
      range: !1,
      step: 1,
      value: 0,
      values: null,
      change: null,
      slide: null,
      start: null,
      stop: null
    },
    numPages: 5,
    _create: function () {
      this._keySliding = !1, this._mouseSliding = !1, this._animateOff = !0, this._handleIndex = null, this._detectOrientation(), this._mouseInit(), this._calculateNewMax(), this._addClass("ui-slider ui-slider-" + this.orientation, "ui-widget ui-widget-content"), this._refresh(), this._animateOff = !1;
    },
    _refresh: function () {
      this._createRange(), this._createHandles(), this._setupEvents(), this._refreshValue();
    },
    _createHandles: function () {
      var e,
          i,
          s = this.options,
          n = this.element.find(".ui-slider-handle"),
          o = "<span tabindex='0'></span>",
          a = [];

      for (i = s.values && s.values.length || 1, n.length > i && (n.slice(i).remove(), n = n.slice(0, i)), e = n.length; i > e; e++) a.push(o);

      this.handles = n.add(t(a.join("")).appendTo(this.element)), this._addClass(this.handles, "ui-slider-handle", "ui-state-default"), this.handle = this.handles.eq(0), this.handles.each(function (e) {
        t(this).data("ui-slider-handle-index", e).attr("tabIndex", 0);
      });
    },
    _createRange: function () {
      var e = this.options;
      e.range ? (e.range === !0 && (e.values ? e.values.length && 2 !== e.values.length ? e.values = [e.values[0], e.values[0]] : t.isArray(e.values) && (e.values = e.values.slice(0)) : e.values = [this._valueMin(), this._valueMin()]), this.range && this.range.length ? (this._removeClass(this.range, "ui-slider-range-min ui-slider-range-max"), this.range.css({
        left: "",
        bottom: ""
      })) : (this.range = t("<div>").appendTo(this.element), this._addClass(this.range, "ui-slider-range")), ("min" === e.range || "max" === e.range) && this._addClass(this.range, "ui-slider-range-" + e.range)) : (this.range && this.range.remove(), this.range = null);
    },
    _setupEvents: function () {
      this._off(this.handles), this._on(this.handles, this._handleEvents), this._hoverable(this.handles), this._focusable(this.handles);
    },
    _destroy: function () {
      this.handles.remove(), this.range && this.range.remove(), this._mouseDestroy();
    },
    _mouseCapture: function (e) {
      var i,
          s,
          n,
          o,
          a,
          r,
          h,
          l,
          c = this,
          u = this.options;
      return u.disabled ? !1 : (this.elementSize = {
        width: this.element.outerWidth(),
        height: this.element.outerHeight()
      }, this.elementOffset = this.element.offset(), i = {
        x: e.pageX,
        y: e.pageY
      }, s = this._normValueFromMouse(i), n = this._valueMax() - this._valueMin() + 1, this.handles.each(function (e) {
        var i = Math.abs(s - c.values(e));
        (n > i || n === i && (e === c._lastChangedValue || c.values(e) === u.min)) && (n = i, o = t(this), a = e);
      }), r = this._start(e, a), r === !1 ? !1 : (this._mouseSliding = !0, this._handleIndex = a, this._addClass(o, null, "ui-state-active"), o.trigger("focus"), h = o.offset(), l = !t(e.target).parents().addBack().is(".ui-slider-handle"), this._clickOffset = l ? {
        left: 0,
        top: 0
      } : {
        left: e.pageX - h.left - o.width() / 2,
        top: e.pageY - h.top - o.height() / 2 - (parseInt(o.css("borderTopWidth"), 10) || 0) - (parseInt(o.css("borderBottomWidth"), 10) || 0) + (parseInt(o.css("marginTop"), 10) || 0)
      }, this.handles.hasClass("ui-state-hover") || this._slide(e, a, s), this._animateOff = !0, !0));
    },
    _mouseStart: function () {
      return !0;
    },
    _mouseDrag: function (t) {
      var e = {
        x: t.pageX,
        y: t.pageY
      },
          i = this._normValueFromMouse(e);

      return this._slide(t, this._handleIndex, i), !1;
    },
    _mouseStop: function (t) {
      return this._removeClass(this.handles, null, "ui-state-active"), this._mouseSliding = !1, this._stop(t, this._handleIndex), this._change(t, this._handleIndex), this._handleIndex = null, this._clickOffset = null, this._animateOff = !1, !1;
    },
    _detectOrientation: function () {
      this.orientation = "vertical" === this.options.orientation ? "vertical" : "horizontal";
    },
    _normValueFromMouse: function (t) {
      var e, i, s, n, o;
      return "horizontal" === this.orientation ? (e = this.elementSize.width, i = t.x - this.elementOffset.left - (this._clickOffset ? this._clickOffset.left : 0)) : (e = this.elementSize.height, i = t.y - this.elementOffset.top - (this._clickOffset ? this._clickOffset.top : 0)), s = i / e, s > 1 && (s = 1), 0 > s && (s = 0), "vertical" === this.orientation && (s = 1 - s), n = this._valueMax() - this._valueMin(), o = this._valueMin() + s * n, this._trimAlignValue(o);
    },
    _uiHash: function (t, e, i) {
      var s = {
        handle: this.handles[t],
        handleIndex: t,
        value: void 0 !== e ? e : this.value()
      };
      return this._hasMultipleValues() && (s.value = void 0 !== e ? e : this.values(t), s.values = i || this.values()), s;
    },
    _hasMultipleValues: function () {
      return this.options.values && this.options.values.length;
    },
    _start: function (t, e) {
      return this._trigger("start", t, this._uiHash(e));
    },
    _slide: function (t, e, i) {
      var s,
          n,
          o = this.value(),
          a = this.values();
      this._hasMultipleValues() && (n = this.values(e ? 0 : 1), o = this.values(e), 2 === this.options.values.length && this.options.range === !0 && (i = 0 === e ? Math.min(n, i) : Math.max(n, i)), a[e] = i), i !== o && (s = this._trigger("slide", t, this._uiHash(e, i, a)), s !== !1 && (this._hasMultipleValues() ? this.values(e, i) : this.value(i)));
    },
    _stop: function (t, e) {
      this._trigger("stop", t, this._uiHash(e));
    },
    _change: function (t, e) {
      this._keySliding || this._mouseSliding || (this._lastChangedValue = e, this._trigger("change", t, this._uiHash(e)));
    },
    value: function (t) {
      return arguments.length ? (this.options.value = this._trimAlignValue(t), this._refreshValue(), this._change(null, 0), void 0) : this._value();
    },
    values: function (e, i) {
      var s, n, o;
      if (arguments.length > 1) return this.options.values[e] = this._trimAlignValue(i), this._refreshValue(), this._change(null, e), void 0;
      if (!arguments.length) return this._values();
      if (!t.isArray(arguments[0])) return this._hasMultipleValues() ? this._values(e) : this.value();

      for (s = this.options.values, n = arguments[0], o = 0; s.length > o; o += 1) s[o] = this._trimAlignValue(n[o]), this._change(null, o);

      this._refreshValue();
    },
    _setOption: function (e, i) {
      var s,
          n = 0;

      switch ("range" === e && this.options.range === !0 && ("min" === i ? (this.options.value = this._values(0), this.options.values = null) : "max" === i && (this.options.value = this._values(this.options.values.length - 1), this.options.values = null)), t.isArray(this.options.values) && (n = this.options.values.length), this._super(e, i), e) {
        case "orientation":
          this._detectOrientation(), this._removeClass("ui-slider-horizontal ui-slider-vertical")._addClass("ui-slider-" + this.orientation), this._refreshValue(), this.options.range && this._refreshRange(i), this.handles.css("horizontal" === i ? "bottom" : "left", "");
          break;

        case "value":
          this._animateOff = !0, this._refreshValue(), this._change(null, 0), this._animateOff = !1;
          break;

        case "values":
          for (this._animateOff = !0, this._refreshValue(), s = n - 1; s >= 0; s--) this._change(null, s);

          this._animateOff = !1;
          break;

        case "step":
        case "min":
        case "max":
          this._animateOff = !0, this._calculateNewMax(), this._refreshValue(), this._animateOff = !1;
          break;

        case "range":
          this._animateOff = !0, this._refresh(), this._animateOff = !1;
      }
    },
    _setOptionDisabled: function (t) {
      this._super(t), this._toggleClass(null, "ui-state-disabled", !!t);
    },
    _value: function () {
      var t = this.options.value;
      return t = this._trimAlignValue(t);
    },
    _values: function (t) {
      var e, i, s;
      if (arguments.length) return e = this.options.values[t], e = this._trimAlignValue(e);

      if (this._hasMultipleValues()) {
        for (i = this.options.values.slice(), s = 0; i.length > s; s += 1) i[s] = this._trimAlignValue(i[s]);

        return i;
      }

      return [];
    },
    _trimAlignValue: function (t) {
      if (this._valueMin() >= t) return this._valueMin();
      if (t >= this._valueMax()) return this._valueMax();
      var e = this.options.step > 0 ? this.options.step : 1,
          i = (t - this._valueMin()) % e,
          s = t - i;
      return 2 * Math.abs(i) >= e && (s += i > 0 ? e : -e), parseFloat(s.toFixed(5));
    },
    _calculateNewMax: function () {
      var t = this.options.max,
          e = this._valueMin(),
          i = this.options.step,
          s = Math.round((t - e) / i) * i;

      t = s + e, t > this.options.max && (t -= i), this.max = parseFloat(t.toFixed(this._precision()));
    },
    _precision: function () {
      var t = this._precisionOf(this.options.step);

      return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), t;
    },
    _precisionOf: function (t) {
      var e = "" + t,
          i = e.indexOf(".");
      return -1 === i ? 0 : e.length - i - 1;
    },
    _valueMin: function () {
      return this.options.min;
    },
    _valueMax: function () {
      return this.max;
    },
    _refreshRange: function (t) {
      "vertical" === t && this.range.css({
        width: "",
        left: ""
      }), "horizontal" === t && this.range.css({
        height: "",
        bottom: ""
      });
    },
    _refreshValue: function () {
      var e,
          i,
          s,
          n,
          o,
          a = this.options.range,
          r = this.options,
          h = this,
          l = this._animateOff ? !1 : r.animate,
          c = {};
      this._hasMultipleValues() ? this.handles.each(function (s) {
        i = 100 * ((h.values(s) - h._valueMin()) / (h._valueMax() - h._valueMin())), c["horizontal" === h.orientation ? "left" : "bottom"] = i + "%", t(this).stop(1, 1)[l ? "animate" : "css"](c, r.animate), h.options.range === !0 && ("horizontal" === h.orientation ? (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({
          left: i + "%"
        }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({
          width: i - e + "%"
        }, {
          queue: !1,
          duration: r.animate
        })) : (0 === s && h.range.stop(1, 1)[l ? "animate" : "css"]({
          bottom: i + "%"
        }, r.animate), 1 === s && h.range[l ? "animate" : "css"]({
          height: i - e + "%"
        }, {
          queue: !1,
          duration: r.animate
        }))), e = i;
      }) : (s = this.value(), n = this._valueMin(), o = this._valueMax(), i = o !== n ? 100 * ((s - n) / (o - n)) : 0, c["horizontal" === this.orientation ? "left" : "bottom"] = i + "%", this.handle.stop(1, 1)[l ? "animate" : "css"](c, r.animate), "min" === a && "horizontal" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        width: i + "%"
      }, r.animate), "max" === a && "horizontal" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        width: 100 - i + "%"
      }, r.animate), "min" === a && "vertical" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        height: i + "%"
      }, r.animate), "max" === a && "vertical" === this.orientation && this.range.stop(1, 1)[l ? "animate" : "css"]({
        height: 100 - i + "%"
      }, r.animate));
    },
    _handleEvents: {
      keydown: function (e) {
        var i,
            s,
            n,
            o,
            a = t(e.target).data("ui-slider-handle-index");

        switch (e.keyCode) {
          case t.ui.keyCode.HOME:
          case t.ui.keyCode.END:
          case t.ui.keyCode.PAGE_UP:
          case t.ui.keyCode.PAGE_DOWN:
          case t.ui.keyCode.UP:
          case t.ui.keyCode.RIGHT:
          case t.ui.keyCode.DOWN:
          case t.ui.keyCode.LEFT:
            if (e.preventDefault(), !this._keySliding && (this._keySliding = !0, this._addClass(t(e.target), null, "ui-state-active"), i = this._start(e, a), i === !1)) return;
        }

        switch (o = this.options.step, s = n = this._hasMultipleValues() ? this.values(a) : this.value(), e.keyCode) {
          case t.ui.keyCode.HOME:
            n = this._valueMin();
            break;

          case t.ui.keyCode.END:
            n = this._valueMax();
            break;

          case t.ui.keyCode.PAGE_UP:
            n = this._trimAlignValue(s + (this._valueMax() - this._valueMin()) / this.numPages);
            break;

          case t.ui.keyCode.PAGE_DOWN:
            n = this._trimAlignValue(s - (this._valueMax() - this._valueMin()) / this.numPages);
            break;

          case t.ui.keyCode.UP:
          case t.ui.keyCode.RIGHT:
            if (s === this._valueMax()) return;
            n = this._trimAlignValue(s + o);
            break;

          case t.ui.keyCode.DOWN:
          case t.ui.keyCode.LEFT:
            if (s === this._valueMin()) return;
            n = this._trimAlignValue(s - o);
        }

        this._slide(e, a, n);
      },
      keyup: function (e) {
        var i = t(e.target).data("ui-slider-handle-index");
        this._keySliding && (this._keySliding = !1, this._stop(e, i), this._change(e, i), this._removeClass(t(e.target), null, "ui-state-active"));
      }
    }
  }), t.widget("ui.sortable", t.ui.mouse, {
    version: "1.12.1",
    widgetEventPrefix: "sort",
    ready: !1,
    options: {
      appendTo: "parent",
      axis: !1,
      connectWith: !1,
      containment: !1,
      cursor: "auto",
      cursorAt: !1,
      dropOnEmpty: !0,
      forcePlaceholderSize: !1,
      forceHelperSize: !1,
      grid: !1,
      handle: !1,
      helper: "original",
      items: "> *",
      opacity: !1,
      placeholder: !1,
      revert: !1,
      scroll: !0,
      scrollSensitivity: 20,
      scrollSpeed: 20,
      scope: "default",
      tolerance: "intersect",
      zIndex: 1e3,
      activate: null,
      beforeStop: null,
      change: null,
      deactivate: null,
      out: null,
      over: null,
      receive: null,
      remove: null,
      sort: null,
      start: null,
      stop: null,
      update: null
    },
    _isOverAxis: function (t, e, i) {
      return t >= e && e + i > t;
    },
    _isFloating: function (t) {
      return /left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"));
    },
    _create: function () {
      this.containerCache = {}, this._addClass("ui-sortable"), this.refresh(), this.offset = this.element.offset(), this._mouseInit(), this._setHandleClassName(), this.ready = !0;
    },
    _setOption: function (t, e) {
      this._super(t, e), "handle" === t && this._setHandleClassName();
    },
    _setHandleClassName: function () {
      var e = this;
      this._removeClass(this.element.find(".ui-sortable-handle"), "ui-sortable-handle"), t.each(this.items, function () {
        e._addClass(this.instance.options.handle ? this.item.find(this.instance.options.handle) : this.item, "ui-sortable-handle");
      });
    },
    _destroy: function () {
      this._mouseDestroy();

      for (var t = this.items.length - 1; t >= 0; t--) this.items[t].item.removeData(this.widgetName + "-item");

      return this;
    },
    _mouseCapture: function (e, i) {
      var s = null,
          n = !1,
          o = this;
      return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(e), t(e.target).parents().each(function () {
        return t.data(this, o.widgetName + "-item") === o ? (s = t(this), !1) : void 0;
      }), t.data(e.target, o.widgetName + "-item") === o && (s = t(e.target)), s ? !this.options.handle || i || (t(this.options.handle, s).find("*").addBack().each(function () {
        this === e.target && (n = !0);
      }), n) ? (this.currentItem = s, this._removeCurrentsFromItems(), !0) : !1 : !1);
    },
    _mouseStart: function (e, i, s) {
      var n,
          o,
          a = this.options;
      if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {
        top: this.offset.top - this.margins.top,
        left: this.offset.left - this.margins.left
      }, t.extend(this.offset, {
        click: {
          left: e.pageX - this.offset.left,
          top: e.pageY - this.offset.top
        },
        parent: this._getParentOffset(),
        relative: this._getRelativeOffset()
      }), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, a.cursorAt && this._adjustOffsetFromHelper(a.cursorAt), this.domPosition = {
        prev: this.currentItem.prev()[0],
        parent: this.currentItem.parent()[0]
      }, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), a.containment && this._setContainment(), a.cursor && "auto" !== a.cursor && (o = this.document.find("body"), this.storedCursor = o.css("cursor"), o.css("cursor", a.cursor), this.storedStylesheet = t("<style>*{ cursor: " + a.cursor + " !important; }</style>").appendTo(o)), a.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", a.opacity)), a.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", a.zIndex)), this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !s) for (n = this.containers.length - 1; n >= 0; n--) this.containers[n]._trigger("activate", e, this._uiHash(this));
      return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this._addClass(this.helper, "ui-sortable-helper"), this._mouseDrag(e), !0;
    },
    _mouseDrag: function (e) {
      var i,
          s,
          n,
          o,
          a = this.options,
          r = !1;

      for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== this.document[0] && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < a.scrollSensitivity ? this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop + a.scrollSpeed : e.pageY - this.overflowOffset.top < a.scrollSensitivity && (this.scrollParent[0].scrollTop = r = this.scrollParent[0].scrollTop - a.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < a.scrollSensitivity ? this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft + a.scrollSpeed : e.pageX - this.overflowOffset.left < a.scrollSensitivity && (this.scrollParent[0].scrollLeft = r = this.scrollParent[0].scrollLeft - a.scrollSpeed)) : (e.pageY - this.document.scrollTop() < a.scrollSensitivity ? r = this.document.scrollTop(this.document.scrollTop() - a.scrollSpeed) : this.window.height() - (e.pageY - this.document.scrollTop()) < a.scrollSensitivity && (r = this.document.scrollTop(this.document.scrollTop() + a.scrollSpeed)), e.pageX - this.document.scrollLeft() < a.scrollSensitivity ? r = this.document.scrollLeft(this.document.scrollLeft() - a.scrollSpeed) : this.window.width() - (e.pageX - this.document.scrollLeft()) < a.scrollSensitivity && (r = this.document.scrollLeft(this.document.scrollLeft() + a.scrollSpeed))), r !== !1 && t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), i = this.items.length - 1; i >= 0; i--) if (s = this.items[i], n = s.item[0], o = this._intersectsWithPointer(s), o && s.instance === this.currentContainer && n !== this.currentItem[0] && this.placeholder[1 === o ? "next" : "prev"]()[0] !== n && !t.contains(this.placeholder[0], n) && ("semi-dynamic" === this.options.type ? !t.contains(this.element[0], n) : !0)) {
        if (this.direction = 1 === o ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(s)) break;
        this._rearrange(e, s), this._trigger("change", e, this._uiHash());
        break;
      }

      return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1;
    },
    _mouseStop: function (e, i) {
      if (e) {
        if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
          var s = this,
              n = this.placeholder.offset(),
              o = this.options.axis,
              a = {};
          o && "x" !== o || (a.left = n.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollLeft)), o && "y" !== o || (a.top = n.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === this.document[0].body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, t(this.helper).animate(a, parseInt(this.options.revert, 10) || 500, function () {
            s._clear(e);
          });
        } else this._clear(e, i);

        return !1;
      }
    },
    cancel: function () {
      if (this.dragging) {
        this._mouseUp(new t.Event("mouseup", {
          target: null
        })), "original" === this.options.helper ? (this.currentItem.css(this._storedCSS), this._removeClass(this.currentItem, "ui-sortable-helper")) : this.currentItem.show();

        for (var e = this.containers.length - 1; e >= 0; e--) this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0);
      }

      return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {
        helper: null,
        dragging: !1,
        reverting: !1,
        _noFinalSort: null
      }), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this;
    },
    serialize: function (e) {
      var i = this._getItemsAsjQuery(e && e.connected),
          s = [];

      return e = e || {}, t(i).each(function () {
        var i = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
        i && s.push((e.key || i[1] + "[]") + "=" + (e.key && e.expression ? i[1] : i[2]));
      }), !s.length && e.key && s.push(e.key + "="), s.join("&");
    },
    toArray: function (e) {
      var i = this._getItemsAsjQuery(e && e.connected),
          s = [];

      return e = e || {}, i.each(function () {
        s.push(t(e.item || this).attr(e.attribute || "id") || "");
      }), s;
    },
    _intersectsWith: function (t) {
      var e = this.positionAbs.left,
          i = e + this.helperProportions.width,
          s = this.positionAbs.top,
          n = s + this.helperProportions.height,
          o = t.left,
          a = o + t.width,
          r = t.top,
          h = r + t.height,
          l = this.offset.click.top,
          c = this.offset.click.left,
          u = "x" === this.options.axis || s + l > r && h > s + l,
          d = "y" === this.options.axis || e + c > o && a > e + c,
          p = u && d;
      return "pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? p : e + this.helperProportions.width / 2 > o && a > i - this.helperProportions.width / 2 && s + this.helperProportions.height / 2 > r && h > n - this.helperProportions.height / 2;
    },
    _intersectsWithPointer: function (t) {
      var e,
          i,
          s = "x" === this.options.axis || this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top, t.height),
          n = "y" === this.options.axis || this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left, t.width),
          o = s && n;

      return o ? (e = this._getDragVerticalDirection(), i = this._getDragHorizontalDirection(), this.floating ? "right" === i || "down" === e ? 2 : 1 : e && ("down" === e ? 2 : 1)) : !1;
    },
    _intersectsWithSides: function (t) {
      var e = this._isOverAxis(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height),
          i = this._isOverAxis(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width),
          s = this._getDragVerticalDirection(),
          n = this._getDragHorizontalDirection();

      return this.floating && n ? "right" === n && i || "left" === n && !i : s && ("down" === s && e || "up" === s && !e);
    },
    _getDragVerticalDirection: function () {
      var t = this.positionAbs.top - this.lastPositionAbs.top;
      return 0 !== t && (t > 0 ? "down" : "up");
    },
    _getDragHorizontalDirection: function () {
      var t = this.positionAbs.left - this.lastPositionAbs.left;
      return 0 !== t && (t > 0 ? "right" : "left");
    },
    refresh: function (t) {
      return this._refreshItems(t), this._setHandleClassName(), this.refreshPositions(), this;
    },
    _connectWith: function () {
      var t = this.options;
      return t.connectWith.constructor === String ? [t.connectWith] : t.connectWith;
    },
    _getItemsAsjQuery: function (e) {
      function i() {
        r.push(this);
      }

      var s,
          n,
          o,
          a,
          r = [],
          h = [],
          l = this._connectWith();

      if (l && e) for (s = l.length - 1; s >= 0; s--) for (o = t(l[s], this.document[0]), n = o.length - 1; n >= 0; n--) a = t.data(o[n], this.widgetFullName), a && a !== this && !a.options.disabled && h.push([t.isFunction(a.options.items) ? a.options.items.call(a.element) : t(a.options.items, a.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), a]);

      for (h.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {
        options: this.options,
        item: this.currentItem
      }) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), s = h.length - 1; s >= 0; s--) h[s][0].each(i);

      return t(r);
    },
    _removeCurrentsFromItems: function () {
      var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
      this.items = t.grep(this.items, function (t) {
        for (var i = 0; e.length > i; i++) if (e[i] === t.item[0]) return !1;

        return !0;
      });
    },
    _refreshItems: function (e) {
      this.items = [], this.containers = [this];

      var i,
          s,
          n,
          o,
          a,
          r,
          h,
          l,
          c = this.items,
          u = [[t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {
        item: this.currentItem
      }) : t(this.options.items, this.element), this]],
          d = this._connectWith();

      if (d && this.ready) for (i = d.length - 1; i >= 0; i--) for (n = t(d[i], this.document[0]), s = n.length - 1; s >= 0; s--) o = t.data(n[s], this.widgetFullName), o && o !== this && !o.options.disabled && (u.push([t.isFunction(o.options.items) ? o.options.items.call(o.element[0], e, {
        item: this.currentItem
      }) : t(o.options.items, o.element), o]), this.containers.push(o));

      for (i = u.length - 1; i >= 0; i--) for (a = u[i][1], r = u[i][0], s = 0, l = r.length; l > s; s++) h = t(r[s]), h.data(this.widgetName + "-item", a), c.push({
        item: h,
        instance: a,
        width: 0,
        height: 0,
        left: 0,
        top: 0
      });
    },
    refreshPositions: function (e) {
      this.floating = this.items.length ? "x" === this.options.axis || this._isFloating(this.items[0].item) : !1, this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
      var i, s, n, o;

      for (i = this.items.length - 1; i >= 0; i--) s = this.items[i], s.instance !== this.currentContainer && this.currentContainer && s.item[0] !== this.currentItem[0] || (n = this.options.toleranceElement ? t(this.options.toleranceElement, s.item) : s.item, e || (s.width = n.outerWidth(), s.height = n.outerHeight()), o = n.offset(), s.left = o.left, s.top = o.top);

      if (this.options.custom && this.options.custom.refreshContainers) this.options.custom.refreshContainers.call(this);else for (i = this.containers.length - 1; i >= 0; i--) o = this.containers[i].element.offset(), this.containers[i].containerCache.left = o.left, this.containers[i].containerCache.top = o.top, this.containers[i].containerCache.width = this.containers[i].element.outerWidth(), this.containers[i].containerCache.height = this.containers[i].element.outerHeight();
      return this;
    },
    _createPlaceholder: function (e) {
      e = e || this;
      var i,
          s = e.options;
      s.placeholder && s.placeholder.constructor !== String || (i = s.placeholder, s.placeholder = {
        element: function () {
          var s = e.currentItem[0].nodeName.toLowerCase(),
              n = t("<" + s + ">", e.document[0]);
          return e._addClass(n, "ui-sortable-placeholder", i || e.currentItem[0].className)._removeClass(n, "ui-sortable-helper"), "tbody" === s ? e._createTrPlaceholder(e.currentItem.find("tr").eq(0), t("<tr>", e.document[0]).appendTo(n)) : "tr" === s ? e._createTrPlaceholder(e.currentItem, n) : "img" === s && n.attr("src", e.currentItem.attr("src")), i || n.css("visibility", "hidden"), n;
        },
        update: function (t, n) {
          (!i || s.forcePlaceholderSize) && (n.height() || n.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), n.width() || n.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)));
        }
      }), e.placeholder = t(s.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), s.placeholder.update(e, e.placeholder);
    },
    _createTrPlaceholder: function (e, i) {
      var s = this;
      e.children().each(function () {
        t("<td>&#160;</td>", s.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(i);
      });
    },
    _contactContainers: function (e) {
      var i,
          s,
          n,
          o,
          a,
          r,
          h,
          l,
          c,
          u,
          d = null,
          p = null;

      for (i = this.containers.length - 1; i >= 0; i--) if (!t.contains(this.currentItem[0], this.containers[i].element[0])) if (this._intersectsWith(this.containers[i].containerCache)) {
        if (d && t.contains(this.containers[i].element[0], d.element[0])) continue;
        d = this.containers[i], p = i;
      } else this.containers[i].containerCache.over && (this.containers[i]._trigger("out", e, this._uiHash(this)), this.containers[i].containerCache.over = 0);

      if (d) if (1 === this.containers.length) this.containers[p].containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash(this)), this.containers[p].containerCache.over = 1);else {
        for (n = 1e4, o = null, c = d.floating || this._isFloating(this.currentItem), a = c ? "left" : "top", r = c ? "width" : "height", u = c ? "pageX" : "pageY", s = this.items.length - 1; s >= 0; s--) t.contains(this.containers[p].element[0], this.items[s].item[0]) && this.items[s].item[0] !== this.currentItem[0] && (h = this.items[s].item.offset()[a], l = !1, e[u] - h > this.items[s][r] / 2 && (l = !0), n > Math.abs(e[u] - h) && (n = Math.abs(e[u] - h), o = this.items[s], this.direction = l ? "up" : "down"));

        if (!o && !this.options.dropOnEmpty) return;
        if (this.currentContainer === this.containers[p]) return this.currentContainer.containerCache.over || (this.containers[p]._trigger("over", e, this._uiHash()), this.currentContainer.containerCache.over = 1), void 0;
        o ? this._rearrange(e, o, null, !0) : this._rearrange(e, null, this.containers[p].element, !0), this._trigger("change", e, this._uiHash()), this.containers[p]._trigger("change", e, this._uiHash(this)), this.currentContainer = this.containers[p], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[p]._trigger("over", e, this._uiHash(this)), this.containers[p].containerCache.over = 1;
      }
    },
    _createHelper: function (e) {
      var i = this.options,
          s = t.isFunction(i.helper) ? t(i.helper.apply(this.element[0], [e, this.currentItem])) : "clone" === i.helper ? this.currentItem.clone() : this.currentItem;
      return s.parents("body").length || t("parent" !== i.appendTo ? i.appendTo : this.currentItem[0].parentNode)[0].appendChild(s[0]), s[0] === this.currentItem[0] && (this._storedCSS = {
        width: this.currentItem[0].style.width,
        height: this.currentItem[0].style.height,
        position: this.currentItem.css("position"),
        top: this.currentItem.css("top"),
        left: this.currentItem.css("left")
      }), (!s[0].style.width || i.forceHelperSize) && s.width(this.currentItem.width()), (!s[0].style.height || i.forceHelperSize) && s.height(this.currentItem.height()), s;
    },
    _adjustOffsetFromHelper: function (e) {
      "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {
        left: +e[0],
        top: +e[1] || 0
      }), "left" in e && (this.offset.click.left = e.left + this.margins.left), "right" in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top" in e && (this.offset.click.top = e.top + this.margins.top), "bottom" in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top);
    },
    _getParentOffset: function () {
      this.offsetParent = this.helper.offsetParent();
      var e = this.offsetParent.offset();
      return "absolute" === this.cssPosition && this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === this.document[0].body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {
        top: 0,
        left: 0
      }), {
        top: e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0),
        left: e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)
      };
    },
    _getRelativeOffset: function () {
      if ("relative" === this.cssPosition) {
        var t = this.currentItem.position();
        return {
          top: t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(),
          left: t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()
        };
      }

      return {
        top: 0,
        left: 0
      };
    },
    _cacheMargins: function () {
      this.margins = {
        left: parseInt(this.currentItem.css("marginLeft"), 10) || 0,
        top: parseInt(this.currentItem.css("marginTop"), 10) || 0
      };
    },
    _cacheHelperProportions: function () {
      this.helperProportions = {
        width: this.helper.outerWidth(),
        height: this.helper.outerHeight()
      };
    },
    _setContainment: function () {
      var e,
          i,
          s,
          n = this.options;
      "parent" === n.containment && (n.containment = this.helper[0].parentNode), ("document" === n.containment || "window" === n.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, "document" === n.containment ? this.document.width() : this.window.width() - this.helperProportions.width - this.margins.left, ("document" === n.containment ? this.document.height() || document.body.parentNode.scrollHeight : this.window.height() || this.document[0].body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(n.containment) || (e = t(n.containment)[0], i = t(n.containment).offset(), s = "hidden" !== t(e).css("overflow"), this.containment = [i.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, i.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, i.left + (s ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, i.top + (s ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top]);
    },
    _convertPositionTo: function (e, i) {
      i || (i = this.position);
      var s = "absolute" === e ? 1 : -1,
          n = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
          o = /(html|body)/i.test(n[0].tagName);
      return {
        top: i.top + this.offset.relative.top * s + this.offset.parent.top * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : o ? 0 : n.scrollTop()) * s,
        left: i.left + this.offset.relative.left * s + this.offset.parent.left * s - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : o ? 0 : n.scrollLeft()) * s
      };
    },
    _generatePosition: function (e) {
      var i,
          s,
          n = this.options,
          o = e.pageX,
          a = e.pageY,
          r = "absolute" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent,
          h = /(html|body)/i.test(r[0].tagName);
      return "relative" !== this.cssPosition || this.scrollParent[0] !== this.document[0] && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (o = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (a = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (o = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (a = this.containment[3] + this.offset.click.top)), n.grid && (i = this.originalPageY + Math.round((a - this.originalPageY) / n.grid[1]) * n.grid[1], a = this.containment ? i - this.offset.click.top >= this.containment[1] && i - this.offset.click.top <= this.containment[3] ? i : i - this.offset.click.top >= this.containment[1] ? i - n.grid[1] : i + n.grid[1] : i, s = this.originalPageX + Math.round((o - this.originalPageX) / n.grid[0]) * n.grid[0], o = this.containment ? s - this.offset.click.left >= this.containment[0] && s - this.offset.click.left <= this.containment[2] ? s : s - this.offset.click.left >= this.containment[0] ? s - n.grid[0] : s + n.grid[0] : s)), {
        top: a - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : h ? 0 : r.scrollTop()),
        left: o - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : h ? 0 : r.scrollLeft())
      };
    },
    _rearrange: function (t, e, i, s) {
      i ? i[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
      var n = this.counter;

      this._delay(function () {
        n === this.counter && this.refreshPositions(!s);
      });
    },
    _clear: function (t, e) {
      function i(t, e, i) {
        return function (s) {
          i._trigger(t, s, e._uiHash(e));
        };
      }

      this.reverting = !1;
      var s,
          n = [];

      if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
        for (s in this._storedCSS) ("auto" === this._storedCSS[s] || "static" === this._storedCSS[s]) && (this._storedCSS[s] = "");

        this.currentItem.css(this._storedCSS), this._removeClass(this.currentItem, "ui-sortable-helper");
      } else this.currentItem.show();

      for (this.fromOutside && !e && n.push(function (t) {
        this._trigger("receive", t, this._uiHash(this.fromOutside));
      }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || n.push(function (t) {
        this._trigger("update", t, this._uiHash());
      }), this !== this.currentContainer && (e || (n.push(function (t) {
        this._trigger("remove", t, this._uiHash());
      }), n.push(function (t) {
        return function (e) {
          t._trigger("receive", e, this._uiHash(this));
        };
      }.call(this, this.currentContainer)), n.push(function (t) {
        return function (e) {
          t._trigger("update", e, this._uiHash(this));
        };
      }.call(this, this.currentContainer)))), s = this.containers.length - 1; s >= 0; s--) e || n.push(i("deactivate", this, this.containers[s])), this.containers[s].containerCache.over && (n.push(i("out", this, this.containers[s])), this.containers[s].containerCache.over = 0);

      if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.cancelHelperRemoval || (this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null), !e) {
        for (s = 0; n.length > s; s++) n[s].call(this, t);

        this._trigger("stop", t, this._uiHash());
      }

      return this.fromOutside = !1, !this.cancelHelperRemoval;
    },
    _trigger: function () {
      t.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel();
    },
    _uiHash: function (e) {
      var i = e || this;
      return {
        helper: i.helper,
        placeholder: i.placeholder || t([]),
        position: i.position,
        originalPosition: i.originalPosition,
        offset: i.positionAbs,
        item: i.currentItem,
        sender: e ? e.element : null
      };
    }
  }), t.widget("ui.spinner", {
    version: "1.12.1",
    defaultElement: "<input>",
    widgetEventPrefix: "spin",
    options: {
      classes: {
        "ui-spinner": "ui-corner-all",
        "ui-spinner-down": "ui-corner-br",
        "ui-spinner-up": "ui-corner-tr"
      },
      culture: null,
      icons: {
        down: "ui-icon-triangle-1-s",
        up: "ui-icon-triangle-1-n"
      },
      incremental: !0,
      max: null,
      min: null,
      numberFormat: null,
      page: 10,
      step: 1,
      change: null,
      spin: null,
      start: null,
      stop: null
    },
    _create: function () {
      this._setOption("max", this.options.max), this._setOption("min", this.options.min), this._setOption("step", this.options.step), "" !== this.value() && this._value(this.element.val(), !0), this._draw(), this._on(this._events), this._refresh(), this._on(this.window, {
        beforeunload: function () {
          this.element.removeAttr("autocomplete");
        }
      });
    },
    _getCreateOptions: function () {
      var e = this._super(),
          i = this.element;

      return t.each(["min", "max", "step"], function (t, s) {
        var n = i.attr(s);
        null != n && n.length && (e[s] = n);
      }), e;
    },
    _events: {
      keydown: function (t) {
        this._start(t) && this._keydown(t) && t.preventDefault();
      },
      keyup: "_stop",
      focus: function () {
        this.previous = this.element.val();
      },
      blur: function (t) {
        return this.cancelBlur ? (delete this.cancelBlur, void 0) : (this._stop(), this._refresh(), this.previous !== this.element.val() && this._trigger("change", t), void 0);
      },
      mousewheel: function (t, e) {
        if (e) {
          if (!this.spinning && !this._start(t)) return !1;
          this._spin((e > 0 ? 1 : -1) * this.options.step, t), clearTimeout(this.mousewheelTimer), this.mousewheelTimer = this._delay(function () {
            this.spinning && this._stop(t);
          }, 100), t.preventDefault();
        }
      },
      "mousedown .ui-spinner-button": function (e) {
        function i() {
          var e = this.element[0] === t.ui.safeActiveElement(this.document[0]);
          e || (this.element.trigger("focus"), this.previous = s, this._delay(function () {
            this.previous = s;
          }));
        }

        var s;
        s = this.element[0] === t.ui.safeActiveElement(this.document[0]) ? this.previous : this.element.val(), e.preventDefault(), i.call(this), this.cancelBlur = !0, this._delay(function () {
          delete this.cancelBlur, i.call(this);
        }), this._start(e) !== !1 && this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e);
      },
      "mouseup .ui-spinner-button": "_stop",
      "mouseenter .ui-spinner-button": function (e) {
        return t(e.currentTarget).hasClass("ui-state-active") ? this._start(e) === !1 ? !1 : (this._repeat(null, t(e.currentTarget).hasClass("ui-spinner-up") ? 1 : -1, e), void 0) : void 0;
      },
      "mouseleave .ui-spinner-button": "_stop"
    },
    _enhance: function () {
      this.uiSpinner = this.element.attr("autocomplete", "off").wrap("<span>").parent().append("<a></a><a></a>");
    },
    _draw: function () {
      this._enhance(), this._addClass(this.uiSpinner, "ui-spinner", "ui-widget ui-widget-content"), this._addClass("ui-spinner-input"), this.element.attr("role", "spinbutton"), this.buttons = this.uiSpinner.children("a").attr("tabIndex", -1).attr("aria-hidden", !0).button({
        classes: {
          "ui-button": ""
        }
      }), this._removeClass(this.buttons, "ui-corner-all"), this._addClass(this.buttons.first(), "ui-spinner-button ui-spinner-up"), this._addClass(this.buttons.last(), "ui-spinner-button ui-spinner-down"), this.buttons.first().button({
        icon: this.options.icons.up,
        showLabel: !1
      }), this.buttons.last().button({
        icon: this.options.icons.down,
        showLabel: !1
      }), this.buttons.height() > Math.ceil(.5 * this.uiSpinner.height()) && this.uiSpinner.height() > 0 && this.uiSpinner.height(this.uiSpinner.height());
    },
    _keydown: function (e) {
      var i = this.options,
          s = t.ui.keyCode;

      switch (e.keyCode) {
        case s.UP:
          return this._repeat(null, 1, e), !0;

        case s.DOWN:
          return this._repeat(null, -1, e), !0;

        case s.PAGE_UP:
          return this._repeat(null, i.page, e), !0;

        case s.PAGE_DOWN:
          return this._repeat(null, -i.page, e), !0;
      }

      return !1;
    },
    _start: function (t) {
      return this.spinning || this._trigger("start", t) !== !1 ? (this.counter || (this.counter = 1), this.spinning = !0, !0) : !1;
    },
    _repeat: function (t, e, i) {
      t = t || 500, clearTimeout(this.timer), this.timer = this._delay(function () {
        this._repeat(40, e, i);
      }, t), this._spin(e * this.options.step, i);
    },
    _spin: function (t, e) {
      var i = this.value() || 0;
      this.counter || (this.counter = 1), i = this._adjustValue(i + t * this._increment(this.counter)), this.spinning && this._trigger("spin", e, {
        value: i
      }) === !1 || (this._value(i), this.counter++);
    },
    _increment: function (e) {
      var i = this.options.incremental;
      return i ? t.isFunction(i) ? i(e) : Math.floor(e * e * e / 5e4 - e * e / 500 + 17 * e / 200 + 1) : 1;
    },
    _precision: function () {
      var t = this._precisionOf(this.options.step);

      return null !== this.options.min && (t = Math.max(t, this._precisionOf(this.options.min))), t;
    },
    _precisionOf: function (t) {
      var e = "" + t,
          i = e.indexOf(".");
      return -1 === i ? 0 : e.length - i - 1;
    },
    _adjustValue: function (t) {
      var e,
          i,
          s = this.options;
      return e = null !== s.min ? s.min : 0, i = t - e, i = Math.round(i / s.step) * s.step, t = e + i, t = parseFloat(t.toFixed(this._precision())), null !== s.max && t > s.max ? s.max : null !== s.min && s.min > t ? s.min : t;
    },
    _stop: function (t) {
      this.spinning && (clearTimeout(this.timer), clearTimeout(this.mousewheelTimer), this.counter = 0, this.spinning = !1, this._trigger("stop", t));
    },
    _setOption: function (t, e) {
      var i, s, n;
      return "culture" === t || "numberFormat" === t ? (i = this._parse(this.element.val()), this.options[t] = e, this.element.val(this._format(i)), void 0) : (("max" === t || "min" === t || "step" === t) && "string" == typeof e && (e = this._parse(e)), "icons" === t && (s = this.buttons.first().find(".ui-icon"), this._removeClass(s, null, this.options.icons.up), this._addClass(s, null, e.up), n = this.buttons.last().find(".ui-icon"), this._removeClass(n, null, this.options.icons.down), this._addClass(n, null, e.down)), this._super(t, e), void 0);
    },
    _setOptionDisabled: function (t) {
      this._super(t), this._toggleClass(this.uiSpinner, null, "ui-state-disabled", !!t), this.element.prop("disabled", !!t), this.buttons.button(t ? "disable" : "enable");
    },
    _setOptions: r(function (t) {
      this._super(t);
    }),
    _parse: function (t) {
      return "string" == typeof t && "" !== t && (t = window.Globalize && this.options.numberFormat ? Globalize.parseFloat(t, 10, this.options.culture) : +t), "" === t || isNaN(t) ? null : t;
    },
    _format: function (t) {
      return "" === t ? "" : window.Globalize && this.options.numberFormat ? Globalize.format(t, this.options.numberFormat, this.options.culture) : t;
    },
    _refresh: function () {
      this.element.attr({
        "aria-valuemin": this.options.min,
        "aria-valuemax": this.options.max,
        "aria-valuenow": this._parse(this.element.val())
      });
    },
    isValid: function () {
      var t = this.value();
      return null === t ? !1 : t === this._adjustValue(t);
    },
    _value: function (t, e) {
      var i;
      "" !== t && (i = this._parse(t), null !== i && (e || (i = this._adjustValue(i)), t = this._format(i))), this.element.val(t), this._refresh();
    },
    _destroy: function () {
      this.element.prop("disabled", !1).removeAttr("autocomplete role aria-valuemin aria-valuemax aria-valuenow"), this.uiSpinner.replaceWith(this.element);
    },
    stepUp: r(function (t) {
      this._stepUp(t);
    }),
    _stepUp: function (t) {
      this._start() && (this._spin((t || 1) * this.options.step), this._stop());
    },
    stepDown: r(function (t) {
      this._stepDown(t);
    }),
    _stepDown: function (t) {
      this._start() && (this._spin((t || 1) * -this.options.step), this._stop());
    },
    pageUp: r(function (t) {
      this._stepUp((t || 1) * this.options.page);
    }),
    pageDown: r(function (t) {
      this._stepDown((t || 1) * this.options.page);
    }),
    value: function (t) {
      return arguments.length ? (r(this._value).call(this, t), void 0) : this._parse(this.element.val());
    },
    widget: function () {
      return this.uiSpinner;
    }
  }), t.uiBackCompat !== !1 && t.widget("ui.spinner", t.ui.spinner, {
    _enhance: function () {
      this.uiSpinner = this.element.attr("autocomplete", "off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());
    },
    _uiSpinnerHtml: function () {
      return "<span>";
    },
    _buttonHtml: function () {
      return "<a></a><a></a>";
    }
  }), t.ui.spinner, t.widget("ui.tabs", {
    version: "1.12.1",
    delay: 300,
    options: {
      active: null,
      classes: {
        "ui-tabs": "ui-corner-all",
        "ui-tabs-nav": "ui-corner-all",
        "ui-tabs-panel": "ui-corner-bottom",
        "ui-tabs-tab": "ui-corner-top"
      },
      collapsible: !1,
      event: "click",
      heightStyle: "content",
      hide: null,
      show: null,
      activate: null,
      beforeActivate: null,
      beforeLoad: null,
      load: null
    },
    _isLocal: function () {
      var t = /#.*$/;
      return function (e) {
        var i, s;
        i = e.href.replace(t, ""), s = location.href.replace(t, "");

        try {
          i = decodeURIComponent(i);
        } catch (n) {}

        try {
          s = decodeURIComponent(s);
        } catch (n) {}

        return e.hash.length > 1 && i === s;
      };
    }(),
    _create: function () {
      var e = this,
          i = this.options;
      this.running = !1, this._addClass("ui-tabs", "ui-widget ui-widget-content"), this._toggleClass("ui-tabs-collapsible", null, i.collapsible), this._processTabs(), i.active = this._initialActive(), t.isArray(i.disabled) && (i.disabled = t.unique(i.disabled.concat(t.map(this.tabs.filter(".ui-state-disabled"), function (t) {
        return e.tabs.index(t);
      }))).sort()), this.active = this.options.active !== !1 && this.anchors.length ? this._findActive(i.active) : t(), this._refresh(), this.active.length && this.load(i.active);
    },
    _initialActive: function () {
      var e = this.options.active,
          i = this.options.collapsible,
          s = location.hash.substring(1);
      return null === e && (s && this.tabs.each(function (i, n) {
        return t(n).attr("aria-controls") === s ? (e = i, !1) : void 0;
      }), null === e && (e = this.tabs.index(this.tabs.filter(".ui-tabs-active"))), (null === e || -1 === e) && (e = this.tabs.length ? 0 : !1)), e !== !1 && (e = this.tabs.index(this.tabs.eq(e)), -1 === e && (e = i ? !1 : 0)), !i && e === !1 && this.anchors.length && (e = 0), e;
    },
    _getCreateEventData: function () {
      return {
        tab: this.active,
        panel: this.active.length ? this._getPanelForTab(this.active) : t()
      };
    },
    _tabKeydown: function (e) {
      var i = t(t.ui.safeActiveElement(this.document[0])).closest("li"),
          s = this.tabs.index(i),
          n = !0;

      if (!this._handlePageNav(e)) {
        switch (e.keyCode) {
          case t.ui.keyCode.RIGHT:
          case t.ui.keyCode.DOWN:
            s++;
            break;

          case t.ui.keyCode.UP:
          case t.ui.keyCode.LEFT:
            n = !1, s--;
            break;

          case t.ui.keyCode.END:
            s = this.anchors.length - 1;
            break;

          case t.ui.keyCode.HOME:
            s = 0;
            break;

          case t.ui.keyCode.SPACE:
            return e.preventDefault(), clearTimeout(this.activating), this._activate(s), void 0;

          case t.ui.keyCode.ENTER:
            return e.preventDefault(), clearTimeout(this.activating), this._activate(s === this.options.active ? !1 : s), void 0;

          default:
            return;
        }

        e.preventDefault(), clearTimeout(this.activating), s = this._focusNextTab(s, n), e.ctrlKey || e.metaKey || (i.attr("aria-selected", "false"), this.tabs.eq(s).attr("aria-selected", "true"), this.activating = this._delay(function () {
          this.option("active", s);
        }, this.delay));
      }
    },
    _panelKeydown: function (e) {
      this._handlePageNav(e) || e.ctrlKey && e.keyCode === t.ui.keyCode.UP && (e.preventDefault(), this.active.trigger("focus"));
    },
    _handlePageNav: function (e) {
      return e.altKey && e.keyCode === t.ui.keyCode.PAGE_UP ? (this._activate(this._focusNextTab(this.options.active - 1, !1)), !0) : e.altKey && e.keyCode === t.ui.keyCode.PAGE_DOWN ? (this._activate(this._focusNextTab(this.options.active + 1, !0)), !0) : void 0;
    },
    _findNextTab: function (e, i) {
      function s() {
        return e > n && (e = 0), 0 > e && (e = n), e;
      }

      for (var n = this.tabs.length - 1; -1 !== t.inArray(s(), this.options.disabled);) e = i ? e + 1 : e - 1;

      return e;
    },
    _focusNextTab: function (t, e) {
      return t = this._findNextTab(t, e), this.tabs.eq(t).trigger("focus"), t;
    },
    _setOption: function (t, e) {
      return "active" === t ? (this._activate(e), void 0) : (this._super(t, e), "collapsible" === t && (this._toggleClass("ui-tabs-collapsible", null, e), e || this.options.active !== !1 || this._activate(0)), "event" === t && this._setupEvents(e), "heightStyle" === t && this._setupHeightStyle(e), void 0);
    },
    _sanitizeSelector: function (t) {
      return t ? t.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g, "\\$&") : "";
    },
    refresh: function () {
      var e = this.options,
          i = this.tablist.children(":has(a[href])");
      e.disabled = t.map(i.filter(".ui-state-disabled"), function (t) {
        return i.index(t);
      }), this._processTabs(), e.active !== !1 && this.anchors.length ? this.active.length && !t.contains(this.tablist[0], this.active[0]) ? this.tabs.length === e.disabled.length ? (e.active = !1, this.active = t()) : this._activate(this._findNextTab(Math.max(0, e.active - 1), !1)) : e.active = this.tabs.index(this.active) : (e.active = !1, this.active = t()), this._refresh();
    },
    _refresh: function () {
      this._setOptionDisabled(this.options.disabled), this._setupEvents(this.options.event), this._setupHeightStyle(this.options.heightStyle), this.tabs.not(this.active).attr({
        "aria-selected": "false",
        "aria-expanded": "false",
        tabIndex: -1
      }), this.panels.not(this._getPanelForTab(this.active)).hide().attr({
        "aria-hidden": "true"
      }), this.active.length ? (this.active.attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      }), this._addClass(this.active, "ui-tabs-active", "ui-state-active"), this._getPanelForTab(this.active).show().attr({
        "aria-hidden": "false"
      })) : this.tabs.eq(0).attr("tabIndex", 0);
    },
    _processTabs: function () {
      var e = this,
          i = this.tabs,
          s = this.anchors,
          n = this.panels;
      this.tablist = this._getList().attr("role", "tablist"), this._addClass(this.tablist, "ui-tabs-nav", "ui-helper-reset ui-helper-clearfix ui-widget-header"), this.tablist.on("mousedown" + this.eventNamespace, "> li", function (e) {
        t(this).is(".ui-state-disabled") && e.preventDefault();
      }).on("focus" + this.eventNamespace, ".ui-tabs-anchor", function () {
        t(this).closest("li").is(".ui-state-disabled") && this.blur();
      }), this.tabs = this.tablist.find("> li:has(a[href])").attr({
        role: "tab",
        tabIndex: -1
      }), this._addClass(this.tabs, "ui-tabs-tab", "ui-state-default"), this.anchors = this.tabs.map(function () {
        return t("a", this)[0];
      }).attr({
        role: "presentation",
        tabIndex: -1
      }), this._addClass(this.anchors, "ui-tabs-anchor"), this.panels = t(), this.anchors.each(function (i, s) {
        var n,
            o,
            a,
            r = t(s).uniqueId().attr("id"),
            h = t(s).closest("li"),
            l = h.attr("aria-controls");
        e._isLocal(s) ? (n = s.hash, a = n.substring(1), o = e.element.find(e._sanitizeSelector(n))) : (a = h.attr("aria-controls") || t({}).uniqueId()[0].id, n = "#" + a, o = e.element.find(n), o.length || (o = e._createPanel(a), o.insertAfter(e.panels[i - 1] || e.tablist)), o.attr("aria-live", "polite")), o.length && (e.panels = e.panels.add(o)), l && h.data("ui-tabs-aria-controls", l), h.attr({
          "aria-controls": a,
          "aria-labelledby": r
        }), o.attr("aria-labelledby", r);
      }), this.panels.attr("role", "tabpanel"), this._addClass(this.panels, "ui-tabs-panel", "ui-widget-content"), i && (this._off(i.not(this.tabs)), this._off(s.not(this.anchors)), this._off(n.not(this.panels)));
    },
    _getList: function () {
      return this.tablist || this.element.find("ol, ul").eq(0);
    },
    _createPanel: function (e) {
      return t("<div>").attr("id", e).data("ui-tabs-destroy", !0);
    },
    _setOptionDisabled: function (e) {
      var i, s, n;

      for (t.isArray(e) && (e.length ? e.length === this.anchors.length && (e = !0) : e = !1), n = 0; s = this.tabs[n]; n++) i = t(s), e === !0 || -1 !== t.inArray(n, e) ? (i.attr("aria-disabled", "true"), this._addClass(i, null, "ui-state-disabled")) : (i.removeAttr("aria-disabled"), this._removeClass(i, null, "ui-state-disabled"));

      this.options.disabled = e, this._toggleClass(this.widget(), this.widgetFullName + "-disabled", null, e === !0);
    },
    _setupEvents: function (e) {
      var i = {};
      e && t.each(e.split(" "), function (t, e) {
        i[e] = "_eventHandler";
      }), this._off(this.anchors.add(this.tabs).add(this.panels)), this._on(!0, this.anchors, {
        click: function (t) {
          t.preventDefault();
        }
      }), this._on(this.anchors, i), this._on(this.tabs, {
        keydown: "_tabKeydown"
      }), this._on(this.panels, {
        keydown: "_panelKeydown"
      }), this._focusable(this.tabs), this._hoverable(this.tabs);
    },
    _setupHeightStyle: function (e) {
      var i,
          s = this.element.parent();
      "fill" === e ? (i = s.height(), i -= this.element.outerHeight() - this.element.height(), this.element.siblings(":visible").each(function () {
        var e = t(this),
            s = e.css("position");
        "absolute" !== s && "fixed" !== s && (i -= e.outerHeight(!0));
      }), this.element.children().not(this.panels).each(function () {
        i -= t(this).outerHeight(!0);
      }), this.panels.each(function () {
        t(this).height(Math.max(0, i - t(this).innerHeight() + t(this).height()));
      }).css("overflow", "auto")) : "auto" === e && (i = 0, this.panels.each(function () {
        i = Math.max(i, t(this).height("").height());
      }).height(i));
    },
    _eventHandler: function (e) {
      var i = this.options,
          s = this.active,
          n = t(e.currentTarget),
          o = n.closest("li"),
          a = o[0] === s[0],
          r = a && i.collapsible,
          h = r ? t() : this._getPanelForTab(o),
          l = s.length ? this._getPanelForTab(s) : t(),
          c = {
        oldTab: s,
        oldPanel: l,
        newTab: r ? t() : o,
        newPanel: h
      };
      e.preventDefault(), o.hasClass("ui-state-disabled") || o.hasClass("ui-tabs-loading") || this.running || a && !i.collapsible || this._trigger("beforeActivate", e, c) === !1 || (i.active = r ? !1 : this.tabs.index(o), this.active = a ? t() : o, this.xhr && this.xhr.abort(), l.length || h.length || t.error("jQuery UI Tabs: Mismatching fragment identifier."), h.length && this.load(this.tabs.index(o), e), this._toggle(e, c));
    },
    _toggle: function (e, i) {
      function s() {
        o.running = !1, o._trigger("activate", e, i);
      }

      function n() {
        o._addClass(i.newTab.closest("li"), "ui-tabs-active", "ui-state-active"), a.length && o.options.show ? o._show(a, o.options.show, s) : (a.show(), s());
      }

      var o = this,
          a = i.newPanel,
          r = i.oldPanel;
      this.running = !0, r.length && this.options.hide ? this._hide(r, this.options.hide, function () {
        o._removeClass(i.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"), n();
      }) : (this._removeClass(i.oldTab.closest("li"), "ui-tabs-active", "ui-state-active"), r.hide(), n()), r.attr("aria-hidden", "true"), i.oldTab.attr({
        "aria-selected": "false",
        "aria-expanded": "false"
      }), a.length && r.length ? i.oldTab.attr("tabIndex", -1) : a.length && this.tabs.filter(function () {
        return 0 === t(this).attr("tabIndex");
      }).attr("tabIndex", -1), a.attr("aria-hidden", "false"), i.newTab.attr({
        "aria-selected": "true",
        "aria-expanded": "true",
        tabIndex: 0
      });
    },
    _activate: function (e) {
      var i,
          s = this._findActive(e);

      s[0] !== this.active[0] && (s.length || (s = this.active), i = s.find(".ui-tabs-anchor")[0], this._eventHandler({
        target: i,
        currentTarget: i,
        preventDefault: t.noop
      }));
    },
    _findActive: function (e) {
      return e === !1 ? t() : this.tabs.eq(e);
    },
    _getIndex: function (e) {
      return "string" == typeof e && (e = this.anchors.index(this.anchors.filter("[href$='" + t.ui.escapeSelector(e) + "']"))), e;
    },
    _destroy: function () {
      this.xhr && this.xhr.abort(), this.tablist.removeAttr("role").off(this.eventNamespace), this.anchors.removeAttr("role tabIndex").removeUniqueId(), this.tabs.add(this.panels).each(function () {
        t.data(this, "ui-tabs-destroy") ? t(this).remove() : t(this).removeAttr("role tabIndex aria-live aria-busy aria-selected aria-labelledby aria-hidden aria-expanded");
      }), this.tabs.each(function () {
        var e = t(this),
            i = e.data("ui-tabs-aria-controls");
        i ? e.attr("aria-controls", i).removeData("ui-tabs-aria-controls") : e.removeAttr("aria-controls");
      }), this.panels.show(), "content" !== this.options.heightStyle && this.panels.css("height", "");
    },
    enable: function (e) {
      var i = this.options.disabled;
      i !== !1 && (void 0 === e ? i = !1 : (e = this._getIndex(e), i = t.isArray(i) ? t.map(i, function (t) {
        return t !== e ? t : null;
      }) : t.map(this.tabs, function (t, i) {
        return i !== e ? i : null;
      })), this._setOptionDisabled(i));
    },
    disable: function (e) {
      var i = this.options.disabled;

      if (i !== !0) {
        if (void 0 === e) i = !0;else {
          if (e = this._getIndex(e), -1 !== t.inArray(e, i)) return;
          i = t.isArray(i) ? t.merge([e], i).sort() : [e];
        }

        this._setOptionDisabled(i);
      }
    },
    load: function (e, i) {
      e = this._getIndex(e);

      var s = this,
          n = this.tabs.eq(e),
          o = n.find(".ui-tabs-anchor"),
          a = this._getPanelForTab(n),
          r = {
        tab: n,
        panel: a
      },
          h = function (t, e) {
        "abort" === e && s.panels.stop(!1, !0), s._removeClass(n, "ui-tabs-loading"), a.removeAttr("aria-busy"), t === s.xhr && delete s.xhr;
      };

      this._isLocal(o[0]) || (this.xhr = t.ajax(this._ajaxSettings(o, i, r)), this.xhr && "canceled" !== this.xhr.statusText && (this._addClass(n, "ui-tabs-loading"), a.attr("aria-busy", "true"), this.xhr.done(function (t, e, n) {
        setTimeout(function () {
          a.html(t), s._trigger("load", i, r), h(n, e);
        }, 1);
      }).fail(function (t, e) {
        setTimeout(function () {
          h(t, e);
        }, 1);
      })));
    },
    _ajaxSettings: function (e, i, s) {
      var n = this;
      return {
        url: e.attr("href").replace(/#.*$/, ""),
        beforeSend: function (e, o) {
          return n._trigger("beforeLoad", i, t.extend({
            jqXHR: e,
            ajaxSettings: o
          }, s));
        }
      };
    },
    _getPanelForTab: function (e) {
      var i = t(e).attr("aria-controls");
      return this.element.find(this._sanitizeSelector("#" + i));
    }
  }), t.uiBackCompat !== !1 && t.widget("ui.tabs", t.ui.tabs, {
    _processTabs: function () {
      this._superApply(arguments), this._addClass(this.tabs, "ui-tab");
    }
  }), t.ui.tabs, t.widget("ui.tooltip", {
    version: "1.12.1",
    options: {
      classes: {
        "ui-tooltip": "ui-corner-all ui-widget-shadow"
      },
      content: function () {
        var e = t(this).attr("title") || "";
        return t("<a>").text(e).html();
      },
      hide: !0,
      items: "[title]:not([disabled])",
      position: {
        my: "left top+15",
        at: "left bottom",
        collision: "flipfit flip"
      },
      show: !0,
      track: !1,
      close: null,
      open: null
    },
    _addDescribedBy: function (e, i) {
      var s = (e.attr("aria-describedby") || "").split(/\s+/);
      s.push(i), e.data("ui-tooltip-id", i).attr("aria-describedby", t.trim(s.join(" ")));
    },
    _removeDescribedBy: function (e) {
      var i = e.data("ui-tooltip-id"),
          s = (e.attr("aria-describedby") || "").split(/\s+/),
          n = t.inArray(i, s);
      -1 !== n && s.splice(n, 1), e.removeData("ui-tooltip-id"), s = t.trim(s.join(" ")), s ? e.attr("aria-describedby", s) : e.removeAttr("aria-describedby");
    },
    _create: function () {
      this._on({
        mouseover: "open",
        focusin: "open"
      }), this.tooltips = {}, this.parents = {}, this.liveRegion = t("<div>").attr({
        role: "log",
        "aria-live": "assertive",
        "aria-relevant": "additions"
      }).appendTo(this.document[0].body), this._addClass(this.liveRegion, null, "ui-helper-hidden-accessible"), this.disabledTitles = t([]);
    },
    _setOption: function (e, i) {
      var s = this;
      this._super(e, i), "content" === e && t.each(this.tooltips, function (t, e) {
        s._updateContent(e.element);
      });
    },
    _setOptionDisabled: function (t) {
      this[t ? "_disable" : "_enable"]();
    },
    _disable: function () {
      var e = this;
      t.each(this.tooltips, function (i, s) {
        var n = t.Event("blur");
        n.target = n.currentTarget = s.element[0], e.close(n, !0);
      }), this.disabledTitles = this.disabledTitles.add(this.element.find(this.options.items).addBack().filter(function () {
        var e = t(this);
        return e.is("[title]") ? e.data("ui-tooltip-title", e.attr("title")).removeAttr("title") : void 0;
      }));
    },
    _enable: function () {
      this.disabledTitles.each(function () {
        var e = t(this);
        e.data("ui-tooltip-title") && e.attr("title", e.data("ui-tooltip-title"));
      }), this.disabledTitles = t([]);
    },
    open: function (e) {
      var i = this,
          s = t(e ? e.target : this.element).closest(this.options.items);
      s.length && !s.data("ui-tooltip-id") && (s.attr("title") && s.data("ui-tooltip-title", s.attr("title")), s.data("ui-tooltip-open", !0), e && "mouseover" === e.type && s.parents().each(function () {
        var e,
            s = t(this);
        s.data("ui-tooltip-open") && (e = t.Event("blur"), e.target = e.currentTarget = this, i.close(e, !0)), s.attr("title") && (s.uniqueId(), i.parents[this.id] = {
          element: this,
          title: s.attr("title")
        }, s.attr("title", ""));
      }), this._registerCloseHandlers(e, s), this._updateContent(s, e));
    },
    _updateContent: function (t, e) {
      var i,
          s = this.options.content,
          n = this,
          o = e ? e.type : null;
      return "string" == typeof s || s.nodeType || s.jquery ? this._open(e, t, s) : (i = s.call(t[0], function (i) {
        n._delay(function () {
          t.data("ui-tooltip-open") && (e && (e.type = o), this._open(e, t, i));
        });
      }), i && this._open(e, t, i), void 0);
    },
    _open: function (e, i, s) {
      function n(t) {
        l.of = t, a.is(":hidden") || a.position(l);
      }

      var o,
          a,
          r,
          h,
          l = t.extend({}, this.options.position);

      if (s) {
        if (o = this._find(i)) return o.tooltip.find(".ui-tooltip-content").html(s), void 0;
        i.is("[title]") && (e && "mouseover" === e.type ? i.attr("title", "") : i.removeAttr("title")), o = this._tooltip(i), a = o.tooltip, this._addDescribedBy(i, a.attr("id")), a.find(".ui-tooltip-content").html(s), this.liveRegion.children().hide(), h = t("<div>").html(a.find(".ui-tooltip-content").html()), h.removeAttr("name").find("[name]").removeAttr("name"), h.removeAttr("id").find("[id]").removeAttr("id"), h.appendTo(this.liveRegion), this.options.track && e && /^mouse/.test(e.type) ? (this._on(this.document, {
          mousemove: n
        }), n(e)) : a.position(t.extend({
          of: i
        }, this.options.position)), a.hide(), this._show(a, this.options.show), this.options.track && this.options.show && this.options.show.delay && (r = this.delayedShow = setInterval(function () {
          a.is(":visible") && (n(l.of), clearInterval(r));
        }, t.fx.interval)), this._trigger("open", e, {
          tooltip: a
        });
      }
    },
    _registerCloseHandlers: function (e, i) {
      var s = {
        keyup: function (e) {
          if (e.keyCode === t.ui.keyCode.ESCAPE) {
            var s = t.Event(e);
            s.currentTarget = i[0], this.close(s, !0);
          }
        }
      };
      i[0] !== this.element[0] && (s.remove = function () {
        this._removeTooltip(this._find(i).tooltip);
      }), e && "mouseover" !== e.type || (s.mouseleave = "close"), e && "focusin" !== e.type || (s.focusout = "close"), this._on(!0, i, s);
    },
    close: function (e) {
      var i,
          s = this,
          n = t(e ? e.currentTarget : this.element),
          o = this._find(n);

      return o ? (i = o.tooltip, o.closing || (clearInterval(this.delayedShow), n.data("ui-tooltip-title") && !n.attr("title") && n.attr("title", n.data("ui-tooltip-title")), this._removeDescribedBy(n), o.hiding = !0, i.stop(!0), this._hide(i, this.options.hide, function () {
        s._removeTooltip(t(this));
      }), n.removeData("ui-tooltip-open"), this._off(n, "mouseleave focusout keyup"), n[0] !== this.element[0] && this._off(n, "remove"), this._off(this.document, "mousemove"), e && "mouseleave" === e.type && t.each(this.parents, function (e, i) {
        t(i.element).attr("title", i.title), delete s.parents[e];
      }), o.closing = !0, this._trigger("close", e, {
        tooltip: i
      }), o.hiding || (o.closing = !1)), void 0) : (n.removeData("ui-tooltip-open"), void 0);
    },
    _tooltip: function (e) {
      var i = t("<div>").attr("role", "tooltip"),
          s = t("<div>").appendTo(i),
          n = i.uniqueId().attr("id");
      return this._addClass(s, "ui-tooltip-content"), this._addClass(i, "ui-tooltip", "ui-widget ui-widget-content"), i.appendTo(this._appendTo(e)), this.tooltips[n] = {
        element: e,
        tooltip: i
      };
    },
    _find: function (t) {
      var e = t.data("ui-tooltip-id");
      return e ? this.tooltips[e] : null;
    },
    _removeTooltip: function (t) {
      t.remove(), delete this.tooltips[t.attr("id")];
    },
    _appendTo: function (t) {
      var e = t.closest(".ui-front, dialog");
      return e.length || (e = this.document[0].body), e;
    },
    _destroy: function () {
      var e = this;
      t.each(this.tooltips, function (i, s) {
        var n = t.Event("blur"),
            o = s.element;
        n.target = n.currentTarget = o[0], e.close(n, !0), t("#" + i).remove(), o.data("ui-tooltip-title") && (o.attr("title") || o.attr("title", o.data("ui-tooltip-title")), o.removeData("ui-tooltip-title"));
      }), this.liveRegion.remove();
    }
  }), t.uiBackCompat !== !1 && t.widget("ui.tooltip", t.ui.tooltip, {
    options: {
      tooltipClass: null
    },
    _tooltip: function () {
      var t = this._superApply(arguments);

      return this.options.tooltipClass && t.tooltip.addClass(this.options.tooltipClass), t;
    }
  }), t.ui.tooltip;
});
/*
 Copyright (C) Federico Zivolo 2018
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */
(function (e, t) {
  'object' == typeof exports && 'undefined' != typeof module ? module.exports = t() : 'function' == typeof define && define.amd ? define(t) : e.Popper = t();
})(this, function () {
  'use strict';

  function e(e) {
    return e && '[object Function]' === {}.toString.call(e);
  }

  function t(e, t) {
    if (1 !== e.nodeType) return [];
    var o = getComputedStyle(e, null);
    return t ? o[t] : o;
  }

  function o(e) {
    return 'HTML' === e.nodeName ? e : e.parentNode || e.host;
  }

  function n(e) {
    if (!e) return document.body;

    switch (e.nodeName) {
      case 'HTML':
      case 'BODY':
        return e.ownerDocument.body;

      case '#document':
        return e.body;
    }

    var i = t(e),
        r = i.overflow,
        p = i.overflowX,
        s = i.overflowY;
    return /(auto|scroll|overlay)/.test(r + s + p) ? e : n(o(e));
  }

  function r(e) {
    return 11 === e ? re : 10 === e ? pe : re || pe;
  }

  function p(e) {
    if (!e) return document.documentElement;

    for (var o = r(10) ? document.body : null, n = e.offsetParent; n === o && e.nextElementSibling;) n = (e = e.nextElementSibling).offsetParent;

    var i = n && n.nodeName;
    return i && 'BODY' !== i && 'HTML' !== i ? -1 !== ['TD', 'TABLE'].indexOf(n.nodeName) && 'static' === t(n, 'position') ? p(n) : n : e ? e.ownerDocument.documentElement : document.documentElement;
  }

  function s(e) {
    var t = e.nodeName;
    return 'BODY' !== t && ('HTML' === t || p(e.firstElementChild) === e);
  }

  function d(e) {
    return null === e.parentNode ? e : d(e.parentNode);
  }

  function a(e, t) {
    if (!e || !e.nodeType || !t || !t.nodeType) return document.documentElement;
    var o = e.compareDocumentPosition(t) & Node.DOCUMENT_POSITION_FOLLOWING,
        n = o ? e : t,
        i = o ? t : e,
        r = document.createRange();
    r.setStart(n, 0), r.setEnd(i, 0);
    var l = r.commonAncestorContainer;
    if (e !== l && t !== l || n.contains(i)) return s(l) ? l : p(l);
    var f = d(e);
    return f.host ? a(f.host, t) : a(e, d(t).host);
  }

  function l(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : 'top',
        o = 'top' === t ? 'scrollTop' : 'scrollLeft',
        n = e.nodeName;

    if ('BODY' === n || 'HTML' === n) {
      var i = e.ownerDocument.documentElement,
          r = e.ownerDocument.scrollingElement || i;
      return r[o];
    }

    return e[o];
  }

  function f(e, t) {
    var o = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        n = l(t, 'top'),
        i = l(t, 'left'),
        r = o ? -1 : 1;
    return e.top += n * r, e.bottom += n * r, e.left += i * r, e.right += i * r, e;
  }

  function m(e, t) {
    var o = 'x' === t ? 'Left' : 'Top',
        n = 'Left' == o ? 'Right' : 'Bottom';
    return parseFloat(e['border' + o + 'Width'], 10) + parseFloat(e['border' + n + 'Width'], 10);
  }

  function h(e, t, o, n) {
    return J(t['offset' + e], t['scroll' + e], o['client' + e], o['offset' + e], o['scroll' + e], r(10) ? parseInt(o['offset' + e]) + parseInt(n['margin' + ('Height' === e ? 'Top' : 'Left')]) + parseInt(n['margin' + ('Height' === e ? 'Bottom' : 'Right')]) : 0);
  }

  function c(e) {
    var t = e.body,
        o = e.documentElement,
        n = r(10) && getComputedStyle(o);
    return {
      height: h('Height', t, o, n),
      width: h('Width', t, o, n)
    };
  }

  function g(e) {
    return le({}, e, {
      right: e.left + e.width,
      bottom: e.top + e.height
    });
  }

  function u(e) {
    var o = {};

    try {
      if (r(10)) {
        o = e.getBoundingClientRect();
        var n = l(e, 'top'),
            i = l(e, 'left');
        o.top += n, o.left += i, o.bottom += n, o.right += i;
      } else o = e.getBoundingClientRect();
    } catch (t) {}

    var p = {
      left: o.left,
      top: o.top,
      width: o.right - o.left,
      height: o.bottom - o.top
    },
        s = 'HTML' === e.nodeName ? c(e.ownerDocument) : {},
        d = s.width || e.clientWidth || p.right - p.left,
        a = s.height || e.clientHeight || p.bottom - p.top,
        f = e.offsetWidth - d,
        h = e.offsetHeight - a;

    if (f || h) {
      var u = t(e);
      f -= m(u, 'x'), h -= m(u, 'y'), p.width -= f, p.height -= h;
    }

    return g(p);
  }

  function b(e, o) {
    var i = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
        p = r(10),
        s = 'HTML' === o.nodeName,
        d = u(e),
        a = u(o),
        l = n(e),
        m = t(o),
        h = parseFloat(m.borderTopWidth, 10),
        c = parseFloat(m.borderLeftWidth, 10);
    i && s && (a.top = J(a.top, 0), a.left = J(a.left, 0));
    var b = g({
      top: d.top - a.top - h,
      left: d.left - a.left - c,
      width: d.width,
      height: d.height
    });

    if (b.marginTop = 0, b.marginLeft = 0, !p && s) {
      var y = parseFloat(m.marginTop, 10),
          w = parseFloat(m.marginLeft, 10);
      b.top -= h - y, b.bottom -= h - y, b.left -= c - w, b.right -= c - w, b.marginTop = y, b.marginLeft = w;
    }

    return (p && !i ? o.contains(l) : o === l && 'BODY' !== l.nodeName) && (b = f(b, o)), b;
  }

  function y(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        o = e.ownerDocument.documentElement,
        n = b(e, o),
        i = J(o.clientWidth, window.innerWidth || 0),
        r = J(o.clientHeight, window.innerHeight || 0),
        p = t ? 0 : l(o),
        s = t ? 0 : l(o, 'left'),
        d = {
      top: p - n.top + n.marginTop,
      left: s - n.left + n.marginLeft,
      width: i,
      height: r
    };
    return g(d);
  }

  function w(e) {
    var n = e.nodeName;
    return 'BODY' === n || 'HTML' === n ? !1 : 'fixed' === t(e, 'position') || w(o(e));
  }

  function E(e) {
    if (!e || !e.parentElement || r()) return document.documentElement;

    for (var o = e.parentElement; o && 'none' === t(o, 'transform');) o = o.parentElement;

    return o || document.documentElement;
  }

  function v(e, t, i, r) {
    var p = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
        s = {
      top: 0,
      left: 0
    },
        d = p ? E(e) : a(e, t);
    if ('viewport' === r) s = y(d, p);else {
      var l;
      'scrollParent' === r ? (l = n(o(t)), 'BODY' === l.nodeName && (l = e.ownerDocument.documentElement)) : 'window' === r ? l = e.ownerDocument.documentElement : l = r;
      var f = b(l, d, p);

      if ('HTML' === l.nodeName && !w(d)) {
        var m = c(e.ownerDocument),
            h = m.height,
            g = m.width;
        s.top += f.top - f.marginTop, s.bottom = h + f.top, s.left += f.left - f.marginLeft, s.right = g + f.left;
      } else s = f;
    }
    i = i || 0;
    var u = 'number' == typeof i;
    return s.left += u ? i : i.left || 0, s.top += u ? i : i.top || 0, s.right -= u ? i : i.right || 0, s.bottom -= u ? i : i.bottom || 0, s;
  }

  function x(e) {
    var t = e.width,
        o = e.height;
    return t * o;
  }

  function O(e, t, o, n, i) {
    var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
    if (-1 === e.indexOf('auto')) return e;
    var p = v(o, n, r, i),
        s = {
      top: {
        width: p.width,
        height: t.top - p.top
      },
      right: {
        width: p.right - t.right,
        height: p.height
      },
      bottom: {
        width: p.width,
        height: p.bottom - t.bottom
      },
      left: {
        width: t.left - p.left,
        height: p.height
      }
    },
        d = Object.keys(s).map(function (e) {
      return le({
        key: e
      }, s[e], {
        area: x(s[e])
      });
    }).sort(function (e, t) {
      return t.area - e.area;
    }),
        a = d.filter(function (e) {
      var t = e.width,
          n = e.height;
      return t >= o.clientWidth && n >= o.clientHeight;
    }),
        l = 0 < a.length ? a[0].key : d[0].key,
        f = e.split('-')[1];
    return l + (f ? '-' + f : '');
  }

  function L(e, t, o) {
    var n = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null,
        i = n ? E(t) : a(t, o);
    return b(o, i, n);
  }

  function S(e) {
    var t = getComputedStyle(e),
        o = parseFloat(t.marginTop) + parseFloat(t.marginBottom),
        n = parseFloat(t.marginLeft) + parseFloat(t.marginRight),
        i = {
      width: e.offsetWidth + n,
      height: e.offsetHeight + o
    };
    return i;
  }

  function T(e) {
    var t = {
      left: 'right',
      right: 'left',
      bottom: 'top',
      top: 'bottom'
    };
    return e.replace(/left|right|bottom|top/g, function (e) {
      return t[e];
    });
  }

  function D(e, t, o) {
    o = o.split('-')[0];
    var n = S(e),
        i = {
      width: n.width,
      height: n.height
    },
        r = -1 !== ['right', 'left'].indexOf(o),
        p = r ? 'top' : 'left',
        s = r ? 'left' : 'top',
        d = r ? 'height' : 'width',
        a = r ? 'width' : 'height';
    return i[p] = t[p] + t[d] / 2 - n[d] / 2, i[s] = o === s ? t[s] - n[a] : t[T(s)], i;
  }

  function C(e, t) {
    return Array.prototype.find ? e.find(t) : e.filter(t)[0];
  }

  function N(e, t, o) {
    if (Array.prototype.findIndex) return e.findIndex(function (e) {
      return e[t] === o;
    });
    var n = C(e, function (e) {
      return e[t] === o;
    });
    return e.indexOf(n);
  }

  function P(t, o, n) {
    var i = void 0 === n ? t : t.slice(0, N(t, 'name', n));
    return i.forEach(function (t) {
      t['function'] && console.warn('`modifier.function` is deprecated, use `modifier.fn`!');
      var n = t['function'] || t.fn;
      t.enabled && e(n) && (o.offsets.popper = g(o.offsets.popper), o.offsets.reference = g(o.offsets.reference), o = n(o, t));
    }), o;
  }

  function k() {
    if (!this.state.isDestroyed) {
      var e = {
        instance: this,
        styles: {},
        arrowStyles: {},
        attributes: {},
        flipped: !1,
        offsets: {}
      };
      e.offsets.reference = L(this.state, this.popper, this.reference, this.options.positionFixed), e.placement = O(this.options.placement, e.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding), e.originalPlacement = e.placement, e.positionFixed = this.options.positionFixed, e.offsets.popper = D(this.popper, e.offsets.reference, e.placement), e.offsets.popper.position = this.options.positionFixed ? 'fixed' : 'absolute', e = P(this.modifiers, e), this.state.isCreated ? this.options.onUpdate(e) : (this.state.isCreated = !0, this.options.onCreate(e));
    }
  }

  function W(e, t) {
    return e.some(function (e) {
      var o = e.name,
          n = e.enabled;
      return n && o === t;
    });
  }

  function H(e) {
    for (var t = [!1, 'ms', 'Webkit', 'Moz', 'O'], o = e.charAt(0).toUpperCase() + e.slice(1), n = 0; n < t.length; n++) {
      var i = t[n],
          r = i ? '' + i + o : e;
      if ('undefined' != typeof document.body.style[r]) return r;
    }

    return null;
  }

  function B() {
    return this.state.isDestroyed = !0, W(this.modifiers, 'applyStyle') && (this.popper.removeAttribute('x-placement'), this.popper.style.position = '', this.popper.style.top = '', this.popper.style.left = '', this.popper.style.right = '', this.popper.style.bottom = '', this.popper.style.willChange = '', this.popper.style[H('transform')] = ''), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
  }

  function A(e) {
    var t = e.ownerDocument;
    return t ? t.defaultView : window;
  }

  function M(e, t, o, i) {
    var r = 'BODY' === e.nodeName,
        p = r ? e.ownerDocument.defaultView : e;
    p.addEventListener(t, o, {
      passive: !0
    }), r || M(n(p.parentNode), t, o, i), i.push(p);
  }

  function F(e, t, o, i) {
    o.updateBound = i, A(e).addEventListener('resize', o.updateBound, {
      passive: !0
    });
    var r = n(e);
    return M(r, 'scroll', o.updateBound, o.scrollParents), o.scrollElement = r, o.eventsEnabled = !0, o;
  }

  function I() {
    this.state.eventsEnabled || (this.state = F(this.reference, this.options, this.state, this.scheduleUpdate));
  }

  function R(e, t) {
    return A(e).removeEventListener('resize', t.updateBound), t.scrollParents.forEach(function (e) {
      e.removeEventListener('scroll', t.updateBound);
    }), t.updateBound = null, t.scrollParents = [], t.scrollElement = null, t.eventsEnabled = !1, t;
  }

  function U() {
    this.state.eventsEnabled && (cancelAnimationFrame(this.scheduleUpdate), this.state = R(this.reference, this.state));
  }

  function Y(e) {
    return '' !== e && !isNaN(parseFloat(e)) && isFinite(e);
  }

  function j(e, t) {
    Object.keys(t).forEach(function (o) {
      var n = '';
      -1 !== ['width', 'height', 'top', 'right', 'bottom', 'left'].indexOf(o) && Y(t[o]) && (n = 'px'), e.style[o] = t[o] + n;
    });
  }

  function K(e, t) {
    Object.keys(t).forEach(function (o) {
      var n = t[o];
      !1 === n ? e.removeAttribute(o) : e.setAttribute(o, t[o]);
    });
  }

  function q(e, t, o) {
    var n = C(e, function (e) {
      var o = e.name;
      return o === t;
    }),
        i = !!n && e.some(function (e) {
      return e.name === o && e.enabled && e.order < n.order;
    });

    if (!i) {
      var r = '`' + t + '`';
      console.warn('`' + o + '`' + ' modifier is required by ' + r + ' modifier in order to work, be sure to include it before ' + r + '!');
    }

    return i;
  }

  function G(e) {
    return 'end' === e ? 'start' : 'start' === e ? 'end' : e;
  }

  function V(e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
        o = me.indexOf(e),
        n = me.slice(o + 1).concat(me.slice(0, o));
    return t ? n.reverse() : n;
  }

  function z(e, t, o, n) {
    var i = e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
        r = +i[1],
        p = i[2];
    if (!r) return e;

    if (0 === p.indexOf('%')) {
      var s;

      switch (p) {
        case '%p':
          s = o;
          break;

        case '%':
        case '%r':
        default:
          s = n;
      }

      var d = g(s);
      return d[t] / 100 * r;
    }

    if ('vh' === p || 'vw' === p) {
      var a;
      return a = 'vh' === p ? J(document.documentElement.clientHeight, window.innerHeight || 0) : J(document.documentElement.clientWidth, window.innerWidth || 0), a / 100 * r;
    }

    return r;
  }

  function _(e, t, o, n) {
    var i = [0, 0],
        r = -1 !== ['right', 'left'].indexOf(n),
        p = e.split(/(\+|\-)/).map(function (e) {
      return e.trim();
    }),
        s = p.indexOf(C(p, function (e) {
      return -1 !== e.search(/,|\s/);
    }));
    p[s] && -1 === p[s].indexOf(',') && console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');
    var d = /\s*,\s*|\s+/,
        a = -1 === s ? [p] : [p.slice(0, s).concat([p[s].split(d)[0]]), [p[s].split(d)[1]].concat(p.slice(s + 1))];
    return a = a.map(function (e, n) {
      var i = (1 === n ? !r : r) ? 'height' : 'width',
          p = !1;
      return e.reduce(function (e, t) {
        return '' === e[e.length - 1] && -1 !== ['+', '-'].indexOf(t) ? (e[e.length - 1] = t, p = !0, e) : p ? (e[e.length - 1] += t, p = !1, e) : e.concat(t);
      }, []).map(function (e) {
        return z(e, i, t, o);
      });
    }), a.forEach(function (e, t) {
      e.forEach(function (o, n) {
        Y(o) && (i[t] += o * ('-' === e[n - 1] ? -1 : 1));
      });
    }), i;
  }

  function X(e, t) {
    var o,
        n = t.offset,
        i = e.placement,
        r = e.offsets,
        p = r.popper,
        s = r.reference,
        d = i.split('-')[0];
    return o = Y(+n) ? [+n, 0] : _(n, p, s, d), 'left' === d ? (p.top += o[0], p.left -= o[1]) : 'right' === d ? (p.top += o[0], p.left += o[1]) : 'top' === d ? (p.left += o[0], p.top -= o[1]) : 'bottom' === d && (p.left += o[0], p.top += o[1]), e.popper = p, e;
  }

  for (var Q = Math.min, Z = Math.round, $ = Math.floor, J = Math.max, ee = 'undefined' != typeof window && 'undefined' != typeof document, te = ['Edge', 'Trident', 'Firefox'], oe = 0, ne = 0; ne < te.length; ne += 1) if (ee && 0 <= navigator.userAgent.indexOf(te[ne])) {
    oe = 1;
    break;
  }

  var i = ee && window.Promise,
      ie = i ? function (e) {
    var t = !1;
    return function () {
      t || (t = !0, window.Promise.resolve().then(function () {
        t = !1, e();
      }));
    };
  } : function (e) {
    var t = !1;
    return function () {
      t || (t = !0, setTimeout(function () {
        t = !1, e();
      }, oe));
    };
  },
      re = ee && !!(window.MSInputMethodContext && document.documentMode),
      pe = ee && /MSIE 10/.test(navigator.userAgent),
      se = function (e, t) {
    if (!(e instanceof t)) throw new TypeError('Cannot call a class as a function');
  },
      de = function () {
    function e(e, t) {
      for (var o, n = 0; n < t.length; n++) o = t[n], o.enumerable = o.enumerable || !1, o.configurable = !0, 'value' in o && (o.writable = !0), Object.defineProperty(e, o.key, o);
    }

    return function (t, o, n) {
      return o && e(t.prototype, o), n && e(t, n), t;
    };
  }(),
      ae = function (e, t, o) {
    return t in e ? Object.defineProperty(e, t, {
      value: o,
      enumerable: !0,
      configurable: !0,
      writable: !0
    }) : e[t] = o, e;
  },
      le = Object.assign || function (e) {
    for (var t, o = 1; o < arguments.length; o++) for (var n in t = arguments[o], t) Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);

    return e;
  },
      fe = ['auto-start', 'auto', 'auto-end', 'top-start', 'top', 'top-end', 'right-start', 'right', 'right-end', 'bottom-end', 'bottom', 'bottom-start', 'left-end', 'left', 'left-start'],
      me = fe.slice(3),
      he = {
    FLIP: 'flip',
    CLOCKWISE: 'clockwise',
    COUNTERCLOCKWISE: 'counterclockwise'
  },
      ce = function () {
    function t(o, n) {
      var i = this,
          r = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
      se(this, t), this.scheduleUpdate = function () {
        return requestAnimationFrame(i.update);
      }, this.update = ie(this.update.bind(this)), this.options = le({}, t.Defaults, r), this.state = {
        isDestroyed: !1,
        isCreated: !1,
        scrollParents: []
      }, this.reference = o && o.jquery ? o[0] : o, this.popper = n && n.jquery ? n[0] : n, this.options.modifiers = {}, Object.keys(le({}, t.Defaults.modifiers, r.modifiers)).forEach(function (e) {
        i.options.modifiers[e] = le({}, t.Defaults.modifiers[e] || {}, r.modifiers ? r.modifiers[e] : {});
      }), this.modifiers = Object.keys(this.options.modifiers).map(function (e) {
        return le({
          name: e
        }, i.options.modifiers[e]);
      }).sort(function (e, t) {
        return e.order - t.order;
      }), this.modifiers.forEach(function (t) {
        t.enabled && e(t.onLoad) && t.onLoad(i.reference, i.popper, i.options, t, i.state);
      }), this.update();
      var p = this.options.eventsEnabled;
      p && this.enableEventListeners(), this.state.eventsEnabled = p;
    }

    return de(t, [{
      key: 'update',
      value: function () {
        return k.call(this);
      }
    }, {
      key: 'destroy',
      value: function () {
        return B.call(this);
      }
    }, {
      key: 'enableEventListeners',
      value: function () {
        return I.call(this);
      }
    }, {
      key: 'disableEventListeners',
      value: function () {
        return U.call(this);
      }
    }]), t;
  }();

  return ce.Utils = ('undefined' == typeof window ? global : window).PopperUtils, ce.placements = fe, ce.Defaults = {
    placement: 'bottom',
    positionFixed: !1,
    eventsEnabled: !0,
    removeOnDestroy: !1,
    onCreate: function () {},
    onUpdate: function () {},
    modifiers: {
      shift: {
        order: 100,
        enabled: !0,
        fn: function (e) {
          var t = e.placement,
              o = t.split('-')[0],
              n = t.split('-')[1];

          if (n) {
            var i = e.offsets,
                r = i.reference,
                p = i.popper,
                s = -1 !== ['bottom', 'top'].indexOf(o),
                d = s ? 'left' : 'top',
                a = s ? 'width' : 'height',
                l = {
              start: ae({}, d, r[d]),
              end: ae({}, d, r[d] + r[a] - p[a])
            };
            e.offsets.popper = le({}, p, l[n]);
          }

          return e;
        }
      },
      offset: {
        order: 200,
        enabled: !0,
        fn: X,
        offset: 0
      },
      preventOverflow: {
        order: 300,
        enabled: !0,
        fn: function (e, t) {
          var o = t.boundariesElement || p(e.instance.popper);
          e.instance.reference === o && (o = p(o));
          var n = H('transform'),
              i = e.instance.popper.style,
              r = i.top,
              s = i.left,
              d = i[n];
          i.top = '', i.left = '', i[n] = '';
          var a = v(e.instance.popper, e.instance.reference, t.padding, o, e.positionFixed);
          i.top = r, i.left = s, i[n] = d, t.boundaries = a;
          var l = t.priority,
              f = e.offsets.popper,
              m = {
            primary: function (e) {
              var o = f[e];
              return f[e] < a[e] && !t.escapeWithReference && (o = J(f[e], a[e])), ae({}, e, o);
            },
            secondary: function (e) {
              var o = 'right' === e ? 'left' : 'top',
                  n = f[o];
              return f[e] > a[e] && !t.escapeWithReference && (n = Q(f[o], a[e] - ('right' === e ? f.width : f.height))), ae({}, o, n);
            }
          };
          return l.forEach(function (e) {
            var t = -1 === ['left', 'top'].indexOf(e) ? 'secondary' : 'primary';
            f = le({}, f, m[t](e));
          }), e.offsets.popper = f, e;
        },
        priority: ['left', 'right', 'top', 'bottom'],
        padding: 5,
        boundariesElement: 'scrollParent'
      },
      keepTogether: {
        order: 400,
        enabled: !0,
        fn: function (e) {
          var t = e.offsets,
              o = t.popper,
              n = t.reference,
              i = e.placement.split('-')[0],
              r = $,
              p = -1 !== ['top', 'bottom'].indexOf(i),
              s = p ? 'right' : 'bottom',
              d = p ? 'left' : 'top',
              a = p ? 'width' : 'height';
          return o[s] < r(n[d]) && (e.offsets.popper[d] = r(n[d]) - o[a]), o[d] > r(n[s]) && (e.offsets.popper[d] = r(n[s])), e;
        }
      },
      arrow: {
        order: 500,
        enabled: !0,
        fn: function (e, o) {
          var n;
          if (!q(e.instance.modifiers, 'arrow', 'keepTogether')) return e;
          var i = o.element;

          if ('string' == typeof i) {
            if (i = e.instance.popper.querySelector(i), !i) return e;
          } else if (!e.instance.popper.contains(i)) return console.warn('WARNING: `arrow.element` must be child of its popper element!'), e;

          var r = e.placement.split('-')[0],
              p = e.offsets,
              s = p.popper,
              d = p.reference,
              a = -1 !== ['left', 'right'].indexOf(r),
              l = a ? 'height' : 'width',
              f = a ? 'Top' : 'Left',
              m = f.toLowerCase(),
              h = a ? 'left' : 'top',
              c = a ? 'bottom' : 'right',
              u = S(i)[l];
          d[c] - u < s[m] && (e.offsets.popper[m] -= s[m] - (d[c] - u)), d[m] + u > s[c] && (e.offsets.popper[m] += d[m] + u - s[c]), e.offsets.popper = g(e.offsets.popper);
          var b = d[m] + d[l] / 2 - u / 2,
              y = t(e.instance.popper),
              w = parseFloat(y['margin' + f], 10),
              E = parseFloat(y['border' + f + 'Width'], 10),
              v = b - e.offsets.popper[m] - w - E;
          return v = J(Q(s[l] - u, v), 0), e.arrowElement = i, e.offsets.arrow = (n = {}, ae(n, m, Z(v)), ae(n, h, ''), n), e;
        },
        element: '[x-arrow]'
      },
      flip: {
        order: 600,
        enabled: !0,
        fn: function (e, t) {
          if (W(e.instance.modifiers, 'inner')) return e;
          if (e.flipped && e.placement === e.originalPlacement) return e;
          var o = v(e.instance.popper, e.instance.reference, t.padding, t.boundariesElement, e.positionFixed),
              n = e.placement.split('-')[0],
              i = T(n),
              r = e.placement.split('-')[1] || '',
              p = [];

          switch (t.behavior) {
            case he.FLIP:
              p = [n, i];
              break;

            case he.CLOCKWISE:
              p = V(n);
              break;

            case he.COUNTERCLOCKWISE:
              p = V(n, !0);
              break;

            default:
              p = t.behavior;
          }

          return p.forEach(function (s, d) {
            if (n !== s || p.length === d + 1) return e;
            n = e.placement.split('-')[0], i = T(n);
            var a = e.offsets.popper,
                l = e.offsets.reference,
                f = $,
                m = 'left' === n && f(a.right) > f(l.left) || 'right' === n && f(a.left) < f(l.right) || 'top' === n && f(a.bottom) > f(l.top) || 'bottom' === n && f(a.top) < f(l.bottom),
                h = f(a.left) < f(o.left),
                c = f(a.right) > f(o.right),
                g = f(a.top) < f(o.top),
                u = f(a.bottom) > f(o.bottom),
                b = 'left' === n && h || 'right' === n && c || 'top' === n && g || 'bottom' === n && u,
                y = -1 !== ['top', 'bottom'].indexOf(n),
                w = !!t.flipVariations && (y && 'start' === r && h || y && 'end' === r && c || !y && 'start' === r && g || !y && 'end' === r && u);
            (m || b || w) && (e.flipped = !0, (m || b) && (n = p[d + 1]), w && (r = G(r)), e.placement = n + (r ? '-' + r : ''), e.offsets.popper = le({}, e.offsets.popper, D(e.instance.popper, e.offsets.reference, e.placement)), e = P(e.instance.modifiers, e, 'flip'));
          }), e;
        },
        behavior: 'flip',
        padding: 5,
        boundariesElement: 'viewport'
      },
      inner: {
        order: 700,
        enabled: !1,
        fn: function (e) {
          var t = e.placement,
              o = t.split('-')[0],
              n = e.offsets,
              i = n.popper,
              r = n.reference,
              p = -1 !== ['left', 'right'].indexOf(o),
              s = -1 === ['top', 'left'].indexOf(o);
          return i[p ? 'left' : 'top'] = r[o] - (s ? i[p ? 'width' : 'height'] : 0), e.placement = T(t), e.offsets.popper = g(i), e;
        }
      },
      hide: {
        order: 800,
        enabled: !0,
        fn: function (e) {
          if (!q(e.instance.modifiers, 'hide', 'preventOverflow')) return e;
          var t = e.offsets.reference,
              o = C(e.instance.modifiers, function (e) {
            return 'preventOverflow' === e.name;
          }).boundaries;

          if (t.bottom < o.top || t.left > o.right || t.top > o.bottom || t.right < o.left) {
            if (!0 === e.hide) return e;
            e.hide = !0, e.attributes['x-out-of-boundaries'] = '';
          } else {
            if (!1 === e.hide) return e;
            e.hide = !1, e.attributes['x-out-of-boundaries'] = !1;
          }

          return e;
        }
      },
      computeStyle: {
        order: 850,
        enabled: !0,
        fn: function (e, t) {
          var o = t.x,
              n = t.y,
              i = e.offsets.popper,
              r = C(e.instance.modifiers, function (e) {
            return 'applyStyle' === e.name;
          }).gpuAcceleration;
          void 0 !== r && console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');
          var s,
              d,
              a = void 0 === r ? t.gpuAcceleration : r,
              l = p(e.instance.popper),
              f = u(l),
              m = {
            position: i.position
          },
              h = {
            left: $(i.left),
            top: Z(i.top),
            bottom: Z(i.bottom),
            right: $(i.right)
          },
              c = 'bottom' === o ? 'top' : 'bottom',
              g = 'right' === n ? 'left' : 'right',
              b = H('transform');
          if (d = 'bottom' == c ? 'HTML' === l.nodeName ? -l.clientHeight + h.bottom : -f.height + h.bottom : h.top, s = 'right' == g ? 'HTML' === l.nodeName ? -l.clientWidth + h.right : -f.width + h.right : h.left, a && b) m[b] = 'translate3d(' + s + 'px, ' + d + 'px, 0)', m[c] = 0, m[g] = 0, m.willChange = 'transform';else {
            var y = 'bottom' == c ? -1 : 1,
                w = 'right' == g ? -1 : 1;
            m[c] = d * y, m[g] = s * w, m.willChange = c + ', ' + g;
          }
          var E = {
            "x-placement": e.placement
          };
          return e.attributes = le({}, E, e.attributes), e.styles = le({}, m, e.styles), e.arrowStyles = le({}, e.offsets.arrow, e.arrowStyles), e;
        },
        gpuAcceleration: !0,
        x: 'bottom',
        y: 'right'
      },
      applyStyle: {
        order: 900,
        enabled: !0,
        fn: function (e) {
          return j(e.instance.popper, e.styles), K(e.instance.popper, e.attributes), e.arrowElement && Object.keys(e.arrowStyles).length && j(e.arrowElement, e.arrowStyles), e;
        },
        onLoad: function (e, t, o, n, i) {
          var r = L(i, t, e, o.positionFixed),
              p = O(o.placement, r, t, e, o.modifiers.flip.boundariesElement, o.modifiers.flip.padding);
          return t.setAttribute('x-placement', p), j(t, {
            position: o.positionFixed ? 'fixed' : 'absolute'
          }), o;
        },
        gpuAcceleration: void 0
      }
    }
  }, ce;
});
/*!
 * Bootstrap v4.0.0-beta (https://getbootstrap.com)
 * Copyright 2011-2017 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
!function (t) {
  var e = jQuery.fn.jquery.split(" ")[0].split(".");
  if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] >= 4) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
}(), function () {
  function t(t, e) {
    if (!t) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    return !e || "object" != typeof e && "function" != typeof e ? t : e;
  }

  function e(t, e) {
    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + typeof e);
    t.prototype = Object.create(e && e.prototype, {
      constructor: {
        value: t,
        enumerable: !1,
        writable: !0,
        configurable: !0
      }
    }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e);
  }

  function n(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  }

  var i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (t) {
    return typeof t;
  } : function (t) {
    return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t;
  },
      o = function () {
    function t(t, e) {
      for (var n = 0; n < e.length; n++) {
        var i = e[n];
        i.enumerable = i.enumerable || !1, i.configurable = !0, "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
      }
    }

    return function (e, n, i) {
      return n && t(e.prototype, n), i && t(e, i), e;
    };
  }(),
      r = function (t) {
    function e(t) {
      return {}.toString.call(t).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }

    function n(t) {
      return (t[0] || t).nodeType;
    }

    function i() {
      return {
        bindType: s.end,
        delegateType: s.end,
        handle: function (e) {
          if (t(e.target).is(this)) return e.handleObj.handler.apply(this, arguments);
        }
      };
    }

    function o() {
      if (window.QUnit) return !1;
      var t = document.createElement("bootstrap");

      for (var e in a) if (void 0 !== t.style[e]) return {
        end: a[e]
      };

      return !1;
    }

    function r(e) {
      var n = this,
          i = !1;
      return t(this).one(l.TRANSITION_END, function () {
        i = !0;
      }), setTimeout(function () {
        i || l.triggerTransitionEnd(n);
      }, e), this;
    }

    var s = !1,
        a = {
      WebkitTransition: "webkitTransitionEnd",
      MozTransition: "transitionend",
      OTransition: "oTransitionEnd otransitionend",
      transition: "transitionend"
    },
        l = {
      TRANSITION_END: "bsTransitionEnd",
      getUID: function (t) {
        do {
          t += ~~(1e6 * Math.random());
        } while (document.getElementById(t));

        return t;
      },
      getSelectorFromElement: function (e) {
        var n = e.getAttribute("data-target");
        n && "#" !== n || (n = e.getAttribute("href") || "");

        try {
          return t(n).length > 0 ? n : null;
        } catch (t) {
          return null;
        }
      },
      reflow: function (t) {
        return t.offsetHeight;
      },
      triggerTransitionEnd: function (e) {
        t(e).trigger(s.end);
      },
      supportsTransitionEnd: function () {
        return Boolean(s);
      },
      typeCheckConfig: function (t, i, o) {
        for (var r in o) if (o.hasOwnProperty(r)) {
          var s = o[r],
              a = i[r],
              l = a && n(a) ? "element" : e(a);
          if (!new RegExp(s).test(l)) throw new Error(t.toUpperCase() + ': Option "' + r + '" provided type "' + l + '" but expected type "' + s + '".');
        }
      }
    };
    return s = o(), t.fn.emulateTransitionEnd = r, l.supportsTransitionEnd() && (t.event.special[l.TRANSITION_END] = i()), l;
  }(jQuery),
      s = (function (t) {
    var e = "alert",
        i = t.fn[e],
        s = {
      DISMISS: '[data-dismiss="alert"]'
    },
        a = {
      CLOSE: "close.bs.alert",
      CLOSED: "closed.bs.alert",
      CLICK_DATA_API: "click.bs.alert.data-api"
    },
        l = {
      ALERT: "alert",
      FADE: "fade",
      SHOW: "show"
    },
        h = function () {
      function e(t) {
        n(this, e), this._element = t;
      }

      return e.prototype.close = function (t) {
        t = t || this._element;

        var e = this._getRootElement(t);

        this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
      }, e.prototype.dispose = function () {
        t.removeData(this._element, "bs.alert"), this._element = null;
      }, e.prototype._getRootElement = function (e) {
        var n = r.getSelectorFromElement(e),
            i = !1;
        return n && (i = t(n)[0]), i || (i = t(e).closest("." + l.ALERT)[0]), i;
      }, e.prototype._triggerCloseEvent = function (e) {
        var n = t.Event(a.CLOSE);
        return t(e).trigger(n), n;
      }, e.prototype._removeElement = function (e) {
        var n = this;
        t(e).removeClass(l.SHOW), r.supportsTransitionEnd() && t(e).hasClass(l.FADE) ? t(e).one(r.TRANSITION_END, function (t) {
          return n._destroyElement(e, t);
        }).emulateTransitionEnd(150) : this._destroyElement(e);
      }, e.prototype._destroyElement = function (e) {
        t(e).detach().trigger(a.CLOSED).remove();
      }, e._jQueryInterface = function (n) {
        return this.each(function () {
          var i = t(this),
              o = i.data("bs.alert");
          o || (o = new e(this), i.data("bs.alert", o)), "close" === n && o[n](this);
        });
      }, e._handleDismiss = function (t) {
        return function (e) {
          e && e.preventDefault(), t.close(this);
        };
      }, o(e, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }]), e;
    }();

    t(document).on(a.CLICK_DATA_API, s.DISMISS, h._handleDismiss(new h())), t.fn[e] = h._jQueryInterface, t.fn[e].Constructor = h, t.fn[e].noConflict = function () {
      return t.fn[e] = i, h._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = "button",
        i = t.fn[e],
        r = {
      ACTIVE: "active",
      BUTTON: "btn",
      FOCUS: "focus"
    },
        s = {
      DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
      DATA_TOGGLE: '[data-toggle="buttons"]',
      INPUT: "input",
      ACTIVE: ".active",
      BUTTON: ".btn"
    },
        a = {
      CLICK_DATA_API: "click.bs.button.data-api",
      FOCUS_BLUR_DATA_API: "focus.bs.button.data-api blur.bs.button.data-api"
    },
        l = function () {
      function e(t) {
        n(this, e), this._element = t;
      }

      return e.prototype.toggle = function () {
        var e = !0,
            n = !0,
            i = t(this._element).closest(s.DATA_TOGGLE)[0];

        if (i) {
          var o = t(this._element).find(s.INPUT)[0];

          if (o) {
            if ("radio" === o.type) if (o.checked && t(this._element).hasClass(r.ACTIVE)) e = !1;else {
              var a = t(i).find(s.ACTIVE)[0];
              a && t(a).removeClass(r.ACTIVE);
            }

            if (e) {
              if (o.hasAttribute("disabled") || i.hasAttribute("disabled") || o.classList.contains("disabled") || i.classList.contains("disabled")) return;
              o.checked = !t(this._element).hasClass(r.ACTIVE), t(o).trigger("change");
            }

            o.focus(), n = !1;
          }
        }

        n && this._element.setAttribute("aria-pressed", !t(this._element).hasClass(r.ACTIVE)), e && t(this._element).toggleClass(r.ACTIVE);
      }, e.prototype.dispose = function () {
        t.removeData(this._element, "bs.button"), this._element = null;
      }, e._jQueryInterface = function (n) {
        return this.each(function () {
          var i = t(this).data("bs.button");
          i || (i = new e(this), t(this).data("bs.button", i)), "toggle" === n && i[n]();
        });
      }, o(e, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }]), e;
    }();

    t(document).on(a.CLICK_DATA_API, s.DATA_TOGGLE_CARROT, function (e) {
      e.preventDefault();
      var n = e.target;
      t(n).hasClass(r.BUTTON) || (n = t(n).closest(s.BUTTON)), l._jQueryInterface.call(t(n), "toggle");
    }).on(a.FOCUS_BLUR_DATA_API, s.DATA_TOGGLE_CARROT, function (e) {
      var n = t(e.target).closest(s.BUTTON)[0];
      t(n).toggleClass(r.FOCUS, /^focus(in)?$/.test(e.type));
    }), t.fn[e] = l._jQueryInterface, t.fn[e].Constructor = l, t.fn[e].noConflict = function () {
      return t.fn[e] = i, l._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = "carousel",
        s = "bs.carousel",
        a = "." + s,
        l = t.fn[e],
        h = {
      interval: 5e3,
      keyboard: !0,
      slide: !1,
      pause: "hover",
      wrap: !0
    },
        c = {
      interval: "(number|boolean)",
      keyboard: "boolean",
      slide: "(boolean|string)",
      pause: "(string|boolean)",
      wrap: "boolean"
    },
        u = {
      NEXT: "next",
      PREV: "prev",
      LEFT: "left",
      RIGHT: "right"
    },
        d = {
      SLIDE: "slide" + a,
      SLID: "slid" + a,
      KEYDOWN: "keydown" + a,
      MOUSEENTER: "mouseenter" + a,
      MOUSELEAVE: "mouseleave" + a,
      TOUCHEND: "touchend" + a,
      LOAD_DATA_API: "load.bs.carousel.data-api",
      CLICK_DATA_API: "click.bs.carousel.data-api"
    },
        f = {
      CAROUSEL: "carousel",
      ACTIVE: "active",
      SLIDE: "slide",
      RIGHT: "carousel-item-right",
      LEFT: "carousel-item-left",
      NEXT: "carousel-item-next",
      PREV: "carousel-item-prev",
      ITEM: "carousel-item"
    },
        p = {
      ACTIVE: ".active",
      ACTIVE_ITEM: ".active.carousel-item",
      ITEM: ".carousel-item",
      NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
      INDICATORS: ".carousel-indicators",
      DATA_SLIDE: "[data-slide], [data-slide-to]",
      DATA_RIDE: '[data-ride="carousel"]'
    },
        _ = function () {
      function l(e, i) {
        n(this, l), this._items = null, this._interval = null, this._activeElement = null, this._isPaused = !1, this._isSliding = !1, this.touchTimeout = null, this._config = this._getConfig(i), this._element = t(e)[0], this._indicatorsElement = t(this._element).find(p.INDICATORS)[0], this._addEventListeners();
      }

      return l.prototype.next = function () {
        this._isSliding || this._slide(u.NEXT);
      }, l.prototype.nextWhenVisible = function () {
        document.hidden || this.next();
      }, l.prototype.prev = function () {
        this._isSliding || this._slide(u.PREV);
      }, l.prototype.pause = function (e) {
        e || (this._isPaused = !0), t(this._element).find(p.NEXT_PREV)[0] && r.supportsTransitionEnd() && (r.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), this._interval = null;
      }, l.prototype.cycle = function (t) {
        t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), this._interval = null), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
      }, l.prototype.to = function (e) {
        var n = this;
        this._activeElement = t(this._element).find(p.ACTIVE_ITEM)[0];

        var i = this._getItemIndex(this._activeElement);

        if (!(e > this._items.length - 1 || e < 0)) if (this._isSliding) t(this._element).one(d.SLID, function () {
          return n.to(e);
        });else {
          if (i === e) return this.pause(), void this.cycle();
          var o = e > i ? u.NEXT : u.PREV;

          this._slide(o, this._items[e]);
        }
      }, l.prototype.dispose = function () {
        t(this._element).off(a), t.removeData(this._element, s), this._items = null, this._config = null, this._element = null, this._interval = null, this._isPaused = null, this._isSliding = null, this._activeElement = null, this._indicatorsElement = null;
      }, l.prototype._getConfig = function (n) {
        return n = t.extend({}, h, n), r.typeCheckConfig(e, n, c), n;
      }, l.prototype._addEventListeners = function () {
        var e = this;
        this._config.keyboard && t(this._element).on(d.KEYDOWN, function (t) {
          return e._keydown(t);
        }), "hover" === this._config.pause && (t(this._element).on(d.MOUSEENTER, function (t) {
          return e.pause(t);
        }).on(d.MOUSELEAVE, function (t) {
          return e.cycle(t);
        }), "ontouchstart" in document.documentElement && t(this._element).on(d.TOUCHEND, function () {
          e.pause(), e.touchTimeout && clearTimeout(e.touchTimeout), e.touchTimeout = setTimeout(function (t) {
            return e.cycle(t);
          }, 500 + e._config.interval);
        }));
      }, l.prototype._keydown = function (t) {
        if (!/input|textarea/i.test(t.target.tagName)) switch (t.which) {
          case 37:
            t.preventDefault(), this.prev();
            break;

          case 39:
            t.preventDefault(), this.next();
            break;

          default:
            return;
        }
      }, l.prototype._getItemIndex = function (e) {
        return this._items = t.makeArray(t(e).parent().find(p.ITEM)), this._items.indexOf(e);
      }, l.prototype._getItemByDirection = function (t, e) {
        var n = t === u.NEXT,
            i = t === u.PREV,
            o = this._getItemIndex(e),
            r = this._items.length - 1;

        if ((i && 0 === o || n && o === r) && !this._config.wrap) return e;
        var s = (o + (t === u.PREV ? -1 : 1)) % this._items.length;
        return -1 === s ? this._items[this._items.length - 1] : this._items[s];
      }, l.prototype._triggerSlideEvent = function (e, n) {
        var i = this._getItemIndex(e),
            o = this._getItemIndex(t(this._element).find(p.ACTIVE_ITEM)[0]),
            r = t.Event(d.SLIDE, {
          relatedTarget: e,
          direction: n,
          from: o,
          to: i
        });

        return t(this._element).trigger(r), r;
      }, l.prototype._setActiveIndicatorElement = function (e) {
        if (this._indicatorsElement) {
          t(this._indicatorsElement).find(p.ACTIVE).removeClass(f.ACTIVE);

          var n = this._indicatorsElement.children[this._getItemIndex(e)];

          n && t(n).addClass(f.ACTIVE);
        }
      }, l.prototype._slide = function (e, n) {
        var i = this,
            o = t(this._element).find(p.ACTIVE_ITEM)[0],
            s = this._getItemIndex(o),
            a = n || o && this._getItemByDirection(e, o),
            l = this._getItemIndex(a),
            h = Boolean(this._interval),
            c = void 0,
            _ = void 0,
            g = void 0;

        if (e === u.NEXT ? (c = f.LEFT, _ = f.NEXT, g = u.LEFT) : (c = f.RIGHT, _ = f.PREV, g = u.RIGHT), a && t(a).hasClass(f.ACTIVE)) this._isSliding = !1;else if (!this._triggerSlideEvent(a, g).isDefaultPrevented() && o && a) {
          this._isSliding = !0, h && this.pause(), this._setActiveIndicatorElement(a);
          var m = t.Event(d.SLID, {
            relatedTarget: a,
            direction: g,
            from: s,
            to: l
          });
          r.supportsTransitionEnd() && t(this._element).hasClass(f.SLIDE) ? (t(a).addClass(_), r.reflow(a), t(o).addClass(c), t(a).addClass(c), t(o).one(r.TRANSITION_END, function () {
            t(a).removeClass(c + " " + _).addClass(f.ACTIVE), t(o).removeClass(f.ACTIVE + " " + _ + " " + c), i._isSliding = !1, setTimeout(function () {
              return t(i._element).trigger(m);
            }, 0);
          }).emulateTransitionEnd(600)) : (t(o).removeClass(f.ACTIVE), t(a).addClass(f.ACTIVE), this._isSliding = !1, t(this._element).trigger(m)), h && this.cycle();
        }
      }, l._jQueryInterface = function (e) {
        return this.each(function () {
          var n = t(this).data(s),
              o = t.extend({}, h, t(this).data());
          "object" === (void 0 === e ? "undefined" : i(e)) && t.extend(o, e);
          var r = "string" == typeof e ? e : o.slide;
          if (n || (n = new l(this, o), t(this).data(s, n)), "number" == typeof e) n.to(e);else if ("string" == typeof r) {
            if (void 0 === n[r]) throw new Error('No method named "' + r + '"');
            n[r]();
          } else o.interval && (n.pause(), n.cycle());
        });
      }, l._dataApiClickHandler = function (e) {
        var n = r.getSelectorFromElement(this);

        if (n) {
          var i = t(n)[0];

          if (i && t(i).hasClass(f.CAROUSEL)) {
            var o = t.extend({}, t(i).data(), t(this).data()),
                a = this.getAttribute("data-slide-to");
            a && (o.interval = !1), l._jQueryInterface.call(t(i), o), a && t(i).data(s).to(a), e.preventDefault();
          }
        }
      }, o(l, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return h;
        }
      }]), l;
    }();

    t(document).on(d.CLICK_DATA_API, p.DATA_SLIDE, _._dataApiClickHandler), t(window).on(d.LOAD_DATA_API, function () {
      t(p.DATA_RIDE).each(function () {
        var e = t(this);

        _._jQueryInterface.call(e, e.data());
      });
    }), t.fn[e] = _._jQueryInterface, t.fn[e].Constructor = _, t.fn[e].noConflict = function () {
      return t.fn[e] = l, _._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = "collapse",
        s = "bs.collapse",
        a = t.fn[e],
        l = {
      toggle: !0,
      parent: ""
    },
        h = {
      toggle: "boolean",
      parent: "string"
    },
        c = {
      SHOW: "show.bs.collapse",
      SHOWN: "shown.bs.collapse",
      HIDE: "hide.bs.collapse",
      HIDDEN: "hidden.bs.collapse",
      CLICK_DATA_API: "click.bs.collapse.data-api"
    },
        u = {
      SHOW: "show",
      COLLAPSE: "collapse",
      COLLAPSING: "collapsing",
      COLLAPSED: "collapsed"
    },
        d = {
      WIDTH: "width",
      HEIGHT: "height"
    },
        f = {
      ACTIVES: ".show, .collapsing",
      DATA_TOGGLE: '[data-toggle="collapse"]'
    },
        p = function () {
      function a(e, i) {
        n(this, a), this._isTransitioning = !1, this._element = e, this._config = this._getConfig(i), this._triggerArray = t.makeArray(t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]'));

        for (var o = t(f.DATA_TOGGLE), s = 0; s < o.length; s++) {
          var l = o[s],
              h = r.getSelectorFromElement(l);
          null !== h && t(h).filter(e).length > 0 && this._triggerArray.push(l);
        }

        this._parent = this._config.parent ? this._getParent() : null, this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
      }

      return a.prototype.toggle = function () {
        t(this._element).hasClass(u.SHOW) ? this.hide() : this.show();
      }, a.prototype.show = function () {
        var e = this;

        if (!this._isTransitioning && !t(this._element).hasClass(u.SHOW)) {
          var n = void 0,
              i = void 0;

          if (this._parent && ((n = t.makeArray(t(this._parent).children().children(f.ACTIVES))).length || (n = null)), !(n && (i = t(n).data(s)) && i._isTransitioning)) {
            var o = t.Event(c.SHOW);

            if (t(this._element).trigger(o), !o.isDefaultPrevented()) {
              n && (a._jQueryInterface.call(t(n), "hide"), i || t(n).data(s, null));

              var l = this._getDimension();

              t(this._element).removeClass(u.COLLAPSE).addClass(u.COLLAPSING), this._element.style[l] = 0, this._triggerArray.length && t(this._triggerArray).removeClass(u.COLLAPSED).attr("aria-expanded", !0), this.setTransitioning(!0);

              var h = function () {
                t(e._element).removeClass(u.COLLAPSING).addClass(u.COLLAPSE).addClass(u.SHOW), e._element.style[l] = "", e.setTransitioning(!1), t(e._element).trigger(c.SHOWN);
              };

              if (r.supportsTransitionEnd()) {
                var d = "scroll" + (l[0].toUpperCase() + l.slice(1));
                t(this._element).one(r.TRANSITION_END, h).emulateTransitionEnd(600), this._element.style[l] = this._element[d] + "px";
              } else h();
            }
          }
        }
      }, a.prototype.hide = function () {
        var e = this;

        if (!this._isTransitioning && t(this._element).hasClass(u.SHOW)) {
          var n = t.Event(c.HIDE);

          if (t(this._element).trigger(n), !n.isDefaultPrevented()) {
            var i = this._getDimension();

            if (this._element.style[i] = this._element.getBoundingClientRect()[i] + "px", r.reflow(this._element), t(this._element).addClass(u.COLLAPSING).removeClass(u.COLLAPSE).removeClass(u.SHOW), this._triggerArray.length) for (var o = 0; o < this._triggerArray.length; o++) {
              var s = this._triggerArray[o],
                  a = r.getSelectorFromElement(s);
              null !== a && (t(a).hasClass(u.SHOW) || t(s).addClass(u.COLLAPSED).attr("aria-expanded", !1));
            }
            this.setTransitioning(!0);

            var l = function () {
              e.setTransitioning(!1), t(e._element).removeClass(u.COLLAPSING).addClass(u.COLLAPSE).trigger(c.HIDDEN);
            };

            this._element.style[i] = "", r.supportsTransitionEnd() ? t(this._element).one(r.TRANSITION_END, l).emulateTransitionEnd(600) : l();
          }
        }
      }, a.prototype.setTransitioning = function (t) {
        this._isTransitioning = t;
      }, a.prototype.dispose = function () {
        t.removeData(this._element, s), this._config = null, this._parent = null, this._element = null, this._triggerArray = null, this._isTransitioning = null;
      }, a.prototype._getConfig = function (n) {
        return n = t.extend({}, l, n), n.toggle = Boolean(n.toggle), r.typeCheckConfig(e, n, h), n;
      }, a.prototype._getDimension = function () {
        return t(this._element).hasClass(d.WIDTH) ? d.WIDTH : d.HEIGHT;
      }, a.prototype._getParent = function () {
        var e = this,
            n = t(this._config.parent)[0],
            i = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
        return t(n).find(i).each(function (t, n) {
          e._addAriaAndCollapsedClass(a._getTargetFromElement(n), [n]);
        }), n;
      }, a.prototype._addAriaAndCollapsedClass = function (e, n) {
        if (e) {
          var i = t(e).hasClass(u.SHOW);
          n.length && t(n).toggleClass(u.COLLAPSED, !i).attr("aria-expanded", i);
        }
      }, a._getTargetFromElement = function (e) {
        var n = r.getSelectorFromElement(e);
        return n ? t(n)[0] : null;
      }, a._jQueryInterface = function (e) {
        return this.each(function () {
          var n = t(this),
              o = n.data(s),
              r = t.extend({}, l, n.data(), "object" === (void 0 === e ? "undefined" : i(e)) && e);

          if (!o && r.toggle && /show|hide/.test(e) && (r.toggle = !1), o || (o = new a(this, r), n.data(s, o)), "string" == typeof e) {
            if (void 0 === o[e]) throw new Error('No method named "' + e + '"');
            o[e]();
          }
        });
      }, o(a, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return l;
        }
      }]), a;
    }();

    t(document).on(c.CLICK_DATA_API, f.DATA_TOGGLE, function (e) {
      /input|textarea/i.test(e.target.tagName) || e.preventDefault();
      var n = t(this),
          i = r.getSelectorFromElement(this);
      t(i).each(function () {
        var e = t(this),
            i = e.data(s) ? "toggle" : n.data();

        p._jQueryInterface.call(e, i);
      });
    }), t.fn[e] = p._jQueryInterface, t.fn[e].Constructor = p, t.fn[e].noConflict = function () {
      return t.fn[e] = a, p._jQueryInterface;
    };
  }(jQuery), function (t) {
    if ("undefined" == typeof Popper) throw new Error("Bootstrap dropdown require Popper.js (https://popper.js.org)");

    var e = "dropdown",
        s = "bs.dropdown",
        a = "." + s,
        l = t.fn[e],
        h = new RegExp("38|40|27"),
        c = {
      HIDE: "hide" + a,
      HIDDEN: "hidden" + a,
      SHOW: "show" + a,
      SHOWN: "shown" + a,
      CLICK: "click" + a,
      CLICK_DATA_API: "click.bs.dropdown.data-api",
      KEYDOWN_DATA_API: "keydown.bs.dropdown.data-api",
      KEYUP_DATA_API: "keyup.bs.dropdown.data-api"
    },
        u = {
      DISABLED: "disabled",
      SHOW: "show",
      DROPUP: "dropup",
      MENURIGHT: "dropdown-menu-right",
      MENULEFT: "dropdown-menu-left"
    },
        d = {
      DATA_TOGGLE: '[data-toggle="dropdown"]',
      FORM_CHILD: ".dropdown form",
      MENU: ".dropdown-menu",
      NAVBAR_NAV: ".navbar-nav",
      VISIBLE_ITEMS: ".dropdown-menu .dropdown-item:not(.disabled)"
    },
        f = {
      TOP: "top-start",
      TOPEND: "top-end",
      BOTTOM: "bottom-start",
      BOTTOMEND: "bottom-end"
    },
        p = {
      placement: f.BOTTOM,
      offset: 0,
      flip: !0
    },
        _ = {
      placement: "string",
      offset: "(number|string)",
      flip: "boolean"
    },
        g = function () {
      function l(t, e) {
        n(this, l), this._element = t, this._popper = null, this._config = this._getConfig(e), this._menu = this._getMenuElement(), this._inNavbar = this._detectNavbar(), this._addEventListeners();
      }

      return l.prototype.toggle = function () {
        if (!this._element.disabled && !t(this._element).hasClass(u.DISABLED)) {
          var e = l._getParentFromElement(this._element),
              n = t(this._menu).hasClass(u.SHOW);

          if (l._clearMenus(), !n) {
            var i = {
              relatedTarget: this._element
            },
                o = t.Event(c.SHOW, i);

            if (t(e).trigger(o), !o.isDefaultPrevented()) {
              var r = this._element;
              t(e).hasClass(u.DROPUP) && (t(this._menu).hasClass(u.MENULEFT) || t(this._menu).hasClass(u.MENURIGHT)) && (r = e), this._popper = new Popper(r, this._menu, this._getPopperConfig()), "ontouchstart" in document.documentElement && !t(e).closest(d.NAVBAR_NAV).length && t("body").children().on("mouseover", null, t.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), t(this._menu).toggleClass(u.SHOW), t(e).toggleClass(u.SHOW).trigger(t.Event(c.SHOWN, i));
            }
          }
        }
      }, l.prototype.dispose = function () {
        t.removeData(this._element, s), t(this._element).off(a), this._element = null, this._menu = null, null !== this._popper && this._popper.destroy(), this._popper = null;
      }, l.prototype.update = function () {
        this._inNavbar = this._detectNavbar(), null !== this._popper && this._popper.scheduleUpdate();
      }, l.prototype._addEventListeners = function () {
        var e = this;
        t(this._element).on(c.CLICK, function (t) {
          t.preventDefault(), t.stopPropagation(), e.toggle();
        });
      }, l.prototype._getConfig = function (n) {
        var i = t(this._element).data();
        return void 0 !== i.placement && (i.placement = f[i.placement.toUpperCase()]), n = t.extend({}, this.constructor.Default, t(this._element).data(), n), r.typeCheckConfig(e, n, this.constructor.DefaultType), n;
      }, l.prototype._getMenuElement = function () {
        if (!this._menu) {
          var e = l._getParentFromElement(this._element);

          this._menu = t(e).find(d.MENU)[0];
        }

        return this._menu;
      }, l.prototype._getPlacement = function () {
        var e = t(this._element).parent(),
            n = this._config.placement;
        return e.hasClass(u.DROPUP) || this._config.placement === f.TOP ? (n = f.TOP, t(this._menu).hasClass(u.MENURIGHT) && (n = f.TOPEND)) : t(this._menu).hasClass(u.MENURIGHT) && (n = f.BOTTOMEND), n;
      }, l.prototype._detectNavbar = function () {
        return t(this._element).closest(".navbar").length > 0;
      }, l.prototype._getPopperConfig = function () {
        var t = {
          placement: this._getPlacement(),
          modifiers: {
            offset: {
              offset: this._config.offset
            },
            flip: {
              enabled: this._config.flip
            }
          }
        };
        return this._inNavbar && (t.modifiers.applyStyle = {
          enabled: !this._inNavbar
        }), t;
      }, l._jQueryInterface = function (e) {
        return this.each(function () {
          var n = t(this).data(s),
              o = "object" === (void 0 === e ? "undefined" : i(e)) ? e : null;

          if (n || (n = new l(this, o), t(this).data(s, n)), "string" == typeof e) {
            if (void 0 === n[e]) throw new Error('No method named "' + e + '"');
            n[e]();
          }
        });
      }, l._clearMenus = function (e) {
        if (!e || 3 !== e.which && ("keyup" !== e.type || 9 === e.which)) for (var n = t.makeArray(t(d.DATA_TOGGLE)), i = 0; i < n.length; i++) {
          var o = l._getParentFromElement(n[i]),
              r = t(n[i]).data(s),
              a = {
            relatedTarget: n[i]
          };

          if (r) {
            var h = r._menu;

            if (t(o).hasClass(u.SHOW) && !(e && ("click" === e.type && /input|textarea/i.test(e.target.tagName) || "keyup" === e.type && 9 === e.which) && t.contains(o, e.target))) {
              var f = t.Event(c.HIDE, a);
              t(o).trigger(f), f.isDefaultPrevented() || ("ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), n[i].setAttribute("aria-expanded", "false"), t(h).removeClass(u.SHOW), t(o).removeClass(u.SHOW).trigger(t.Event(c.HIDDEN, a)));
            }
          }
        }
      }, l._getParentFromElement = function (e) {
        var n = void 0,
            i = r.getSelectorFromElement(e);
        return i && (n = t(i)[0]), n || e.parentNode;
      }, l._dataApiKeydownHandler = function (e) {
        if (!(!h.test(e.which) || /button/i.test(e.target.tagName) && 32 === e.which || /input|textarea/i.test(e.target.tagName) || (e.preventDefault(), e.stopPropagation(), this.disabled || t(this).hasClass(u.DISABLED)))) {
          var n = l._getParentFromElement(this),
              i = t(n).hasClass(u.SHOW);

          if ((i || 27 === e.which && 32 === e.which) && (!i || 27 !== e.which && 32 !== e.which)) {
            var o = t(n).find(d.VISIBLE_ITEMS).get();

            if (o.length) {
              var r = o.indexOf(e.target);
              38 === e.which && r > 0 && r--, 40 === e.which && r < o.length - 1 && r++, r < 0 && (r = 0), o[r].focus();
            }
          } else {
            if (27 === e.which) {
              var s = t(n).find(d.DATA_TOGGLE)[0];
              t(s).trigger("focus");
            }

            t(this).trigger("click");
          }
        }
      }, o(l, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return p;
        }
      }, {
        key: "DefaultType",
        get: function () {
          return _;
        }
      }]), l;
    }();

    t(document).on(c.KEYDOWN_DATA_API, d.DATA_TOGGLE, g._dataApiKeydownHandler).on(c.KEYDOWN_DATA_API, d.MENU, g._dataApiKeydownHandler).on(c.CLICK_DATA_API + " " + c.KEYUP_DATA_API, g._clearMenus).on(c.CLICK_DATA_API, d.DATA_TOGGLE, function (e) {
      e.preventDefault(), e.stopPropagation(), g._jQueryInterface.call(t(this), "toggle");
    }).on(c.CLICK_DATA_API, d.FORM_CHILD, function (t) {
      t.stopPropagation();
    }), t.fn[e] = g._jQueryInterface, t.fn[e].Constructor = g, t.fn[e].noConflict = function () {
      return t.fn[e] = l, g._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = "modal",
        s = ".bs.modal",
        a = t.fn[e],
        l = {
      backdrop: !0,
      keyboard: !0,
      focus: !0,
      show: !0
    },
        h = {
      backdrop: "(boolean|string)",
      keyboard: "boolean",
      focus: "boolean",
      show: "boolean"
    },
        c = {
      HIDE: "hide.bs.modal",
      HIDDEN: "hidden.bs.modal",
      SHOW: "show.bs.modal",
      SHOWN: "shown.bs.modal",
      FOCUSIN: "focusin.bs.modal",
      RESIZE: "resize.bs.modal",
      CLICK_DISMISS: "click.dismiss.bs.modal",
      KEYDOWN_DISMISS: "keydown.dismiss.bs.modal",
      MOUSEUP_DISMISS: "mouseup.dismiss.bs.modal",
      MOUSEDOWN_DISMISS: "mousedown.dismiss.bs.modal",
      CLICK_DATA_API: "click.bs.modal.data-api"
    },
        u = {
      SCROLLBAR_MEASURER: "modal-scrollbar-measure",
      BACKDROP: "modal-backdrop",
      OPEN: "modal-open",
      FADE: "fade",
      SHOW: "show"
    },
        d = {
      DIALOG: ".modal-dialog",
      DATA_TOGGLE: '[data-toggle="modal"]',
      DATA_DISMISS: '[data-dismiss="modal"]',
      FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
      NAVBAR_TOGGLER: ".navbar-toggler"
    },
        f = function () {
      function a(e, i) {
        n(this, a), this._config = this._getConfig(i), this._element = e, this._dialog = t(e).find(d.DIALOG)[0], this._backdrop = null, this._isShown = !1, this._isBodyOverflowing = !1, this._ignoreBackdropClick = !1, this._originalBodyPadding = 0, this._scrollbarWidth = 0;
      }

      return a.prototype.toggle = function (t) {
        return this._isShown ? this.hide() : this.show(t);
      }, a.prototype.show = function (e) {
        var n = this;

        if (!this._isTransitioning) {
          r.supportsTransitionEnd() && t(this._element).hasClass(u.FADE) && (this._isTransitioning = !0);
          var i = t.Event(c.SHOW, {
            relatedTarget: e
          });
          t(this._element).trigger(i), this._isShown || i.isDefaultPrevented() || (this._isShown = !0, this._checkScrollbar(), this._setScrollbar(), t(document.body).addClass(u.OPEN), this._setEscapeEvent(), this._setResizeEvent(), t(this._element).on(c.CLICK_DISMISS, d.DATA_DISMISS, function (t) {
            return n.hide(t);
          }), t(this._dialog).on(c.MOUSEDOWN_DISMISS, function () {
            t(n._element).one(c.MOUSEUP_DISMISS, function (e) {
              t(e.target).is(n._element) && (n._ignoreBackdropClick = !0);
            });
          }), this._showBackdrop(function () {
            return n._showElement(e);
          }));
        }
      }, a.prototype.hide = function (e) {
        var n = this;

        if (e && e.preventDefault(), !this._isTransitioning && this._isShown) {
          var i = r.supportsTransitionEnd() && t(this._element).hasClass(u.FADE);
          i && (this._isTransitioning = !0);
          var o = t.Event(c.HIDE);
          t(this._element).trigger(o), this._isShown && !o.isDefaultPrevented() && (this._isShown = !1, this._setEscapeEvent(), this._setResizeEvent(), t(document).off(c.FOCUSIN), t(this._element).removeClass(u.SHOW), t(this._element).off(c.CLICK_DISMISS), t(this._dialog).off(c.MOUSEDOWN_DISMISS), i ? t(this._element).one(r.TRANSITION_END, function (t) {
            return n._hideModal(t);
          }).emulateTransitionEnd(300) : this._hideModal());
        }
      }, a.prototype.dispose = function () {
        t.removeData(this._element, "bs.modal"), t(window, document, this._element, this._backdrop).off(s), this._config = null, this._element = null, this._dialog = null, this._backdrop = null, this._isShown = null, this._isBodyOverflowing = null, this._ignoreBackdropClick = null, this._scrollbarWidth = null;
      }, a.prototype.handleUpdate = function () {
        this._adjustDialog();
      }, a.prototype._getConfig = function (n) {
        return n = t.extend({}, l, n), r.typeCheckConfig(e, n, h), n;
      }, a.prototype._showElement = function (e) {
        var n = this,
            i = r.supportsTransitionEnd() && t(this._element).hasClass(u.FADE);
        this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element), this._element.style.display = "block", this._element.removeAttribute("aria-hidden"), this._element.scrollTop = 0, i && r.reflow(this._element), t(this._element).addClass(u.SHOW), this._config.focus && this._enforceFocus();

        var o = t.Event(c.SHOWN, {
          relatedTarget: e
        }),
            s = function () {
          n._config.focus && n._element.focus(), n._isTransitioning = !1, t(n._element).trigger(o);
        };

        i ? t(this._dialog).one(r.TRANSITION_END, s).emulateTransitionEnd(300) : s();
      }, a.prototype._enforceFocus = function () {
        var e = this;
        t(document).off(c.FOCUSIN).on(c.FOCUSIN, function (n) {
          document === n.target || e._element === n.target || t(e._element).has(n.target).length || e._element.focus();
        });
      }, a.prototype._setEscapeEvent = function () {
        var e = this;
        this._isShown && this._config.keyboard ? t(this._element).on(c.KEYDOWN_DISMISS, function (t) {
          27 === t.which && (t.preventDefault(), e.hide());
        }) : this._isShown || t(this._element).off(c.KEYDOWN_DISMISS);
      }, a.prototype._setResizeEvent = function () {
        var e = this;
        this._isShown ? t(window).on(c.RESIZE, function (t) {
          return e.handleUpdate(t);
        }) : t(window).off(c.RESIZE);
      }, a.prototype._hideModal = function () {
        var e = this;
        this._element.style.display = "none", this._element.setAttribute("aria-hidden", !0), this._isTransitioning = !1, this._showBackdrop(function () {
          t(document.body).removeClass(u.OPEN), e._resetAdjustments(), e._resetScrollbar(), t(e._element).trigger(c.HIDDEN);
        });
      }, a.prototype._removeBackdrop = function () {
        this._backdrop && (t(this._backdrop).remove(), this._backdrop = null);
      }, a.prototype._showBackdrop = function (e) {
        var n = this,
            i = t(this._element).hasClass(u.FADE) ? u.FADE : "";

        if (this._isShown && this._config.backdrop) {
          var o = r.supportsTransitionEnd() && i;
          if (this._backdrop = document.createElement("div"), this._backdrop.className = u.BACKDROP, i && t(this._backdrop).addClass(i), t(this._backdrop).appendTo(document.body), t(this._element).on(c.CLICK_DISMISS, function (t) {
            n._ignoreBackdropClick ? n._ignoreBackdropClick = !1 : t.target === t.currentTarget && ("static" === n._config.backdrop ? n._element.focus() : n.hide());
          }), o && r.reflow(this._backdrop), t(this._backdrop).addClass(u.SHOW), !e) return;
          if (!o) return void e();
          t(this._backdrop).one(r.TRANSITION_END, e).emulateTransitionEnd(150);
        } else if (!this._isShown && this._backdrop) {
          t(this._backdrop).removeClass(u.SHOW);

          var s = function () {
            n._removeBackdrop(), e && e();
          };

          r.supportsTransitionEnd() && t(this._element).hasClass(u.FADE) ? t(this._backdrop).one(r.TRANSITION_END, s).emulateTransitionEnd(150) : s();
        } else e && e();
      }, a.prototype._adjustDialog = function () {
        var t = this._element.scrollHeight > document.documentElement.clientHeight;
        !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
      }, a.prototype._resetAdjustments = function () {
        this._element.style.paddingLeft = "", this._element.style.paddingRight = "";
      }, a.prototype._checkScrollbar = function () {
        this._isBodyOverflowing = document.body.clientWidth < window.innerWidth, this._scrollbarWidth = this._getScrollbarWidth();
      }, a.prototype._setScrollbar = function () {
        var e = this;

        if (this._isBodyOverflowing) {
          t(d.FIXED_CONTENT).each(function (n, i) {
            var o = t(i)[0].style.paddingRight,
                r = t(i).css("padding-right");
            t(i).data("padding-right", o).css("padding-right", parseFloat(r) + e._scrollbarWidth + "px");
          }), t(d.NAVBAR_TOGGLER).each(function (n, i) {
            var o = t(i)[0].style.marginRight,
                r = t(i).css("margin-right");
            t(i).data("margin-right", o).css("margin-right", parseFloat(r) + e._scrollbarWidth + "px");
          });
          var n = document.body.style.paddingRight,
              i = t("body").css("padding-right");
          t("body").data("padding-right", n).css("padding-right", parseFloat(i) + this._scrollbarWidth + "px");
        }
      }, a.prototype._resetScrollbar = function () {
        t(d.FIXED_CONTENT).each(function (e, n) {
          var i = t(n).data("padding-right");
          void 0 !== i && t(n).css("padding-right", i).removeData("padding-right");
        }), t(d.NAVBAR_TOGGLER).each(function (e, n) {
          var i = t(n).data("margin-right");
          void 0 !== i && t(n).css("margin-right", i).removeData("margin-right");
        });
        var e = t("body").data("padding-right");
        void 0 !== e && t("body").css("padding-right", e).removeData("padding-right");
      }, a.prototype._getScrollbarWidth = function () {
        var t = document.createElement("div");
        t.className = u.SCROLLBAR_MEASURER, document.body.appendChild(t);
        var e = t.getBoundingClientRect().width - t.clientWidth;
        return document.body.removeChild(t), e;
      }, a._jQueryInterface = function (e, n) {
        return this.each(function () {
          var o = t(this).data("bs.modal"),
              r = t.extend({}, a.Default, t(this).data(), "object" === (void 0 === e ? "undefined" : i(e)) && e);

          if (o || (o = new a(this, r), t(this).data("bs.modal", o)), "string" == typeof e) {
            if (void 0 === o[e]) throw new Error('No method named "' + e + '"');
            o[e](n);
          } else r.show && o.show(n);
        });
      }, o(a, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return l;
        }
      }]), a;
    }();

    t(document).on(c.CLICK_DATA_API, d.DATA_TOGGLE, function (e) {
      var n = this,
          i = void 0,
          o = r.getSelectorFromElement(this);
      o && (i = t(o)[0]);
      var s = t(i).data("bs.modal") ? "toggle" : t.extend({}, t(i).data(), t(this).data());
      "A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();
      var a = t(i).one(c.SHOW, function (e) {
        e.isDefaultPrevented() || a.one(c.HIDDEN, function () {
          t(n).is(":visible") && n.focus();
        });
      });

      f._jQueryInterface.call(t(i), s, this);
    }), t.fn[e] = f._jQueryInterface, t.fn[e].Constructor = f, t.fn[e].noConflict = function () {
      return t.fn[e] = a, f._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = "scrollspy",
        s = t.fn[e],
        a = {
      offset: 10,
      method: "auto",
      target: ""
    },
        l = {
      offset: "number",
      method: "string",
      target: "(string|element)"
    },
        h = {
      ACTIVATE: "activate.bs.scrollspy",
      SCROLL: "scroll.bs.scrollspy",
      LOAD_DATA_API: "load.bs.scrollspy.data-api"
    },
        c = {
      DROPDOWN_ITEM: "dropdown-item",
      DROPDOWN_MENU: "dropdown-menu",
      ACTIVE: "active"
    },
        u = {
      DATA_SPY: '[data-spy="scroll"]',
      ACTIVE: ".active",
      NAV_LIST_GROUP: ".nav, .list-group",
      NAV_LINKS: ".nav-link",
      LIST_ITEMS: ".list-group-item",
      DROPDOWN: ".dropdown",
      DROPDOWN_ITEMS: ".dropdown-item",
      DROPDOWN_TOGGLE: ".dropdown-toggle"
    },
        d = {
      OFFSET: "offset",
      POSITION: "position"
    },
        f = function () {
      function s(e, i) {
        var o = this;
        n(this, s), this._element = e, this._scrollElement = "BODY" === e.tagName ? window : e, this._config = this._getConfig(i), this._selector = this._config.target + " " + u.NAV_LINKS + "," + this._config.target + " " + u.LIST_ITEMS + "," + this._config.target + " " + u.DROPDOWN_ITEMS, this._offsets = [], this._targets = [], this._activeTarget = null, this._scrollHeight = 0, t(this._scrollElement).on(h.SCROLL, function (t) {
          return o._process(t);
        }), this.refresh(), this._process();
      }

      return s.prototype.refresh = function () {
        var e = this,
            n = this._scrollElement !== this._scrollElement.window ? d.POSITION : d.OFFSET,
            i = "auto" === this._config.method ? n : this._config.method,
            o = i === d.POSITION ? this._getScrollTop() : 0;
        this._offsets = [], this._targets = [], this._scrollHeight = this._getScrollHeight(), t.makeArray(t(this._selector)).map(function (e) {
          var n = void 0,
              s = r.getSelectorFromElement(e);

          if (s && (n = t(s)[0]), n) {
            var a = n.getBoundingClientRect();
            if (a.width || a.height) return [t(n)[i]().top + o, s];
          }

          return null;
        }).filter(function (t) {
          return t;
        }).sort(function (t, e) {
          return t[0] - e[0];
        }).forEach(function (t) {
          e._offsets.push(t[0]), e._targets.push(t[1]);
        });
      }, s.prototype.dispose = function () {
        t.removeData(this._element, "bs.scrollspy"), t(this._scrollElement).off(".bs.scrollspy"), this._element = null, this._scrollElement = null, this._config = null, this._selector = null, this._offsets = null, this._targets = null, this._activeTarget = null, this._scrollHeight = null;
      }, s.prototype._getConfig = function (n) {
        if ("string" != typeof (n = t.extend({}, a, n)).target) {
          var i = t(n.target).attr("id");
          i || (i = r.getUID(e), t(n.target).attr("id", i)), n.target = "#" + i;
        }

        return r.typeCheckConfig(e, n, l), n;
      }, s.prototype._getScrollTop = function () {
        return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
      }, s.prototype._getScrollHeight = function () {
        return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
      }, s.prototype._getOffsetHeight = function () {
        return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
      }, s.prototype._process = function () {
        var t = this._getScrollTop() + this._config.offset,
            e = this._getScrollHeight(),
            n = this._config.offset + e - this._getOffsetHeight();

        if (this._scrollHeight !== e && this.refresh(), t >= n) {
          var i = this._targets[this._targets.length - 1];
          this._activeTarget !== i && this._activate(i);
        } else {
          if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0) return this._activeTarget = null, void this._clear();

          for (var o = this._offsets.length; o--;) this._activeTarget !== this._targets[o] && t >= this._offsets[o] && (void 0 === this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
        }
      }, s.prototype._activate = function (e) {
        this._activeTarget = e, this._clear();

        var n = this._selector.split(",");

        n = n.map(function (t) {
          return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
        });
        var i = t(n.join(","));
        i.hasClass(c.DROPDOWN_ITEM) ? (i.closest(u.DROPDOWN).find(u.DROPDOWN_TOGGLE).addClass(c.ACTIVE), i.addClass(c.ACTIVE)) : (i.addClass(c.ACTIVE), i.parents(u.NAV_LIST_GROUP).prev(u.NAV_LINKS + ", " + u.LIST_ITEMS).addClass(c.ACTIVE)), t(this._scrollElement).trigger(h.ACTIVATE, {
          relatedTarget: e
        });
      }, s.prototype._clear = function () {
        t(this._selector).filter(u.ACTIVE).removeClass(c.ACTIVE);
      }, s._jQueryInterface = function (e) {
        return this.each(function () {
          var n = t(this).data("bs.scrollspy"),
              o = "object" === (void 0 === e ? "undefined" : i(e)) && e;

          if (n || (n = new s(this, o), t(this).data("bs.scrollspy", n)), "string" == typeof e) {
            if (void 0 === n[e]) throw new Error('No method named "' + e + '"');
            n[e]();
          }
        });
      }, o(s, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return a;
        }
      }]), s;
    }();

    t(window).on(h.LOAD_DATA_API, function () {
      for (var e = t.makeArray(t(u.DATA_SPY)), n = e.length; n--;) {
        var i = t(e[n]);

        f._jQueryInterface.call(i, i.data());
      }
    }), t.fn[e] = f._jQueryInterface, t.fn[e].Constructor = f, t.fn[e].noConflict = function () {
      return t.fn[e] = s, f._jQueryInterface;
    };
  }(jQuery), function (t) {
    var e = t.fn.tab,
        i = {
      HIDE: "hide.bs.tab",
      HIDDEN: "hidden.bs.tab",
      SHOW: "show.bs.tab",
      SHOWN: "shown.bs.tab",
      CLICK_DATA_API: "click.bs.tab.data-api"
    },
        s = {
      DROPDOWN_MENU: "dropdown-menu",
      ACTIVE: "active",
      DISABLED: "disabled",
      FADE: "fade",
      SHOW: "show"
    },
        a = {
      DROPDOWN: ".dropdown",
      NAV_LIST_GROUP: ".nav, .list-group",
      ACTIVE: ".active",
      DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
      DROPDOWN_TOGGLE: ".dropdown-toggle",
      DROPDOWN_ACTIVE_CHILD: "> .dropdown-menu .active"
    },
        l = function () {
      function e(t) {
        n(this, e), this._element = t;
      }

      return e.prototype.show = function () {
        var e = this;

        if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && t(this._element).hasClass(s.ACTIVE) || t(this._element).hasClass(s.DISABLED))) {
          var n = void 0,
              o = void 0,
              l = t(this._element).closest(a.NAV_LIST_GROUP)[0],
              h = r.getSelectorFromElement(this._element);
          l && (o = t.makeArray(t(l).find(a.ACTIVE)), o = o[o.length - 1]);
          var c = t.Event(i.HIDE, {
            relatedTarget: this._element
          }),
              u = t.Event(i.SHOW, {
            relatedTarget: o
          });

          if (o && t(o).trigger(c), t(this._element).trigger(u), !u.isDefaultPrevented() && !c.isDefaultPrevented()) {
            h && (n = t(h)[0]), this._activate(this._element, l);

            var d = function () {
              var n = t.Event(i.HIDDEN, {
                relatedTarget: e._element
              }),
                  r = t.Event(i.SHOWN, {
                relatedTarget: o
              });
              t(o).trigger(n), t(e._element).trigger(r);
            };

            n ? this._activate(n, n.parentNode, d) : d();
          }
        }
      }, e.prototype.dispose = function () {
        t.removeData(this._element, "bs.tab"), this._element = null;
      }, e.prototype._activate = function (e, n, i) {
        var o = this,
            l = t(n).find(a.ACTIVE)[0],
            h = i && r.supportsTransitionEnd() && l && t(l).hasClass(s.FADE),
            c = function () {
          return o._transitionComplete(e, l, h, i);
        };

        l && h ? t(l).one(r.TRANSITION_END, c).emulateTransitionEnd(150) : c(), l && t(l).removeClass(s.SHOW);
      }, e.prototype._transitionComplete = function (e, n, i, o) {
        if (n) {
          t(n).removeClass(s.ACTIVE);
          var l = t(n.parentNode).find(a.DROPDOWN_ACTIVE_CHILD)[0];
          l && t(l).removeClass(s.ACTIVE), n.setAttribute("aria-expanded", !1);
        }

        if (t(e).addClass(s.ACTIVE), e.setAttribute("aria-expanded", !0), i ? (r.reflow(e), t(e).addClass(s.SHOW)) : t(e).removeClass(s.FADE), e.parentNode && t(e.parentNode).hasClass(s.DROPDOWN_MENU)) {
          var h = t(e).closest(a.DROPDOWN)[0];
          h && t(h).find(a.DROPDOWN_TOGGLE).addClass(s.ACTIVE), e.setAttribute("aria-expanded", !0);
        }

        o && o();
      }, e._jQueryInterface = function (n) {
        return this.each(function () {
          var i = t(this),
              o = i.data("bs.tab");

          if (o || (o = new e(this), i.data("bs.tab", o)), "string" == typeof n) {
            if (void 0 === o[n]) throw new Error('No method named "' + n + '"');
            o[n]();
          }
        });
      }, o(e, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }]), e;
    }();

    t(document).on(i.CLICK_DATA_API, a.DATA_TOGGLE, function (e) {
      e.preventDefault(), l._jQueryInterface.call(t(this), "show");
    }), t.fn.tab = l._jQueryInterface, t.fn.tab.Constructor = l, t.fn.tab.noConflict = function () {
      return t.fn.tab = e, l._jQueryInterface;
    };
  }(jQuery), function (t) {
    if ("undefined" == typeof Popper) throw new Error("Bootstrap tooltips require Popper.js (https://popper.js.org)");

    var e = "tooltip",
        s = ".bs.tooltip",
        a = t.fn[e],
        l = new RegExp("(^|\\s)bs-tooltip\\S+", "g"),
        h = {
      animation: "boolean",
      template: "string",
      title: "(string|element|function)",
      trigger: "string",
      delay: "(number|object)",
      html: "boolean",
      selector: "(string|boolean)",
      placement: "(string|function)",
      offset: "(number|string)",
      container: "(string|element|boolean)",
      fallbackPlacement: "(string|array)"
    },
        c = {
      AUTO: "auto",
      TOP: "top",
      RIGHT: "right",
      BOTTOM: "bottom",
      LEFT: "left"
    },
        u = {
      animation: !0,
      template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
      trigger: "hover focus",
      title: "",
      delay: 0,
      html: !1,
      selector: !1,
      placement: "top",
      offset: 0,
      container: !1,
      fallbackPlacement: "flip"
    },
        d = {
      SHOW: "show",
      OUT: "out"
    },
        f = {
      HIDE: "hide" + s,
      HIDDEN: "hidden" + s,
      SHOW: "show" + s,
      SHOWN: "shown" + s,
      INSERTED: "inserted" + s,
      CLICK: "click" + s,
      FOCUSIN: "focusin" + s,
      FOCUSOUT: "focusout" + s,
      MOUSEENTER: "mouseenter" + s,
      MOUSELEAVE: "mouseleave" + s
    },
        p = {
      FADE: "fade",
      SHOW: "show"
    },
        _ = {
      TOOLTIP: ".tooltip",
      TOOLTIP_INNER: ".tooltip-inner",
      ARROW: ".arrow"
    },
        g = {
      HOVER: "hover",
      FOCUS: "focus",
      CLICK: "click",
      MANUAL: "manual"
    },
        m = function () {
      function a(t, e) {
        n(this, a), this._isEnabled = !0, this._timeout = 0, this._hoverState = "", this._activeTrigger = {}, this._popper = null, this.element = t, this.config = this._getConfig(e), this.tip = null, this._setListeners();
      }

      return a.prototype.enable = function () {
        this._isEnabled = !0;
      }, a.prototype.disable = function () {
        this._isEnabled = !1;
      }, a.prototype.toggleEnabled = function () {
        this._isEnabled = !this._isEnabled;
      }, a.prototype.toggle = function (e) {
        if (e) {
          var n = this.constructor.DATA_KEY,
              i = t(e.currentTarget).data(n);
          i || (i = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(n, i)), i._activeTrigger.click = !i._activeTrigger.click, i._isWithActiveTrigger() ? i._enter(null, i) : i._leave(null, i);
        } else {
          if (t(this.getTipElement()).hasClass(p.SHOW)) return void this._leave(null, this);

          this._enter(null, this);
        }
      }, a.prototype.dispose = function () {
        clearTimeout(this._timeout), t.removeData(this.element, this.constructor.DATA_KEY), t(this.element).off(this.constructor.EVENT_KEY), t(this.element).closest(".modal").off("hide.bs.modal"), this.tip && t(this.tip).remove(), this._isEnabled = null, this._timeout = null, this._hoverState = null, this._activeTrigger = null, null !== this._popper && this._popper.destroy(), this._popper = null, this.element = null, this.config = null, this.tip = null;
      }, a.prototype.show = function () {
        var e = this;
        if ("none" === t(this.element).css("display")) throw new Error("Please use show on visible elements");
        var n = t.Event(this.constructor.Event.SHOW);

        if (this.isWithContent() && this._isEnabled) {
          t(this.element).trigger(n);
          var i = t.contains(this.element.ownerDocument.documentElement, this.element);
          if (n.isDefaultPrevented() || !i) return;
          var o = this.getTipElement(),
              s = r.getUID(this.constructor.NAME);
          o.setAttribute("id", s), this.element.setAttribute("aria-describedby", s), this.setContent(), this.config.animation && t(o).addClass(p.FADE);

          var l = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
              h = this._getAttachment(l);

          this.addAttachmentClass(h);
          var c = !1 === this.config.container ? document.body : t(this.config.container);
          t(o).data(this.constructor.DATA_KEY, this), t.contains(this.element.ownerDocument.documentElement, this.tip) || t(o).appendTo(c), t(this.element).trigger(this.constructor.Event.INSERTED), this._popper = new Popper(this.element, o, {
            placement: h,
            modifiers: {
              offset: {
                offset: this.config.offset
              },
              flip: {
                behavior: this.config.fallbackPlacement
              },
              arrow: {
                element: _.ARROW
              }
            },
            onCreate: function (t) {
              t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
            },
            onUpdate: function (t) {
              e._handlePopperPlacementChange(t);
            }
          }), t(o).addClass(p.SHOW), "ontouchstart" in document.documentElement && t("body").children().on("mouseover", null, t.noop);

          var u = function () {
            e.config.animation && e._fixTransition();
            var n = e._hoverState;
            e._hoverState = null, t(e.element).trigger(e.constructor.Event.SHOWN), n === d.OUT && e._leave(null, e);
          };

          r.supportsTransitionEnd() && t(this.tip).hasClass(p.FADE) ? t(this.tip).one(r.TRANSITION_END, u).emulateTransitionEnd(a._TRANSITION_DURATION) : u();
        }
      }, a.prototype.hide = function (e) {
        var n = this,
            i = this.getTipElement(),
            o = t.Event(this.constructor.Event.HIDE),
            s = function () {
          n._hoverState !== d.SHOW && i.parentNode && i.parentNode.removeChild(i), n._cleanTipClass(), n.element.removeAttribute("aria-describedby"), t(n.element).trigger(n.constructor.Event.HIDDEN), null !== n._popper && n._popper.destroy(), e && e();
        };

        t(this.element).trigger(o), o.isDefaultPrevented() || (t(i).removeClass(p.SHOW), "ontouchstart" in document.documentElement && t("body").children().off("mouseover", null, t.noop), this._activeTrigger[g.CLICK] = !1, this._activeTrigger[g.FOCUS] = !1, this._activeTrigger[g.HOVER] = !1, r.supportsTransitionEnd() && t(this.tip).hasClass(p.FADE) ? t(i).one(r.TRANSITION_END, s).emulateTransitionEnd(150) : s(), this._hoverState = "");
      }, a.prototype.update = function () {
        null !== this._popper && this._popper.scheduleUpdate();
      }, a.prototype.isWithContent = function () {
        return Boolean(this.getTitle());
      }, a.prototype.addAttachmentClass = function (e) {
        t(this.getTipElement()).addClass("bs-tooltip-" + e);
      }, a.prototype.getTipElement = function () {
        return this.tip = this.tip || t(this.config.template)[0];
      }, a.prototype.setContent = function () {
        var e = t(this.getTipElement());
        this.setElementContent(e.find(_.TOOLTIP_INNER), this.getTitle()), e.removeClass(p.FADE + " " + p.SHOW);
      }, a.prototype.setElementContent = function (e, n) {
        var o = this.config.html;
        "object" === (void 0 === n ? "undefined" : i(n)) && (n.nodeType || n.jquery) ? o ? t(n).parent().is(e) || e.empty().append(n) : e.text(t(n).text()) : e[o ? "html" : "text"](n);
      }, a.prototype.getTitle = function () {
        var t = this.element.getAttribute("data-original-title");
        return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
      }, a.prototype._getAttachment = function (t) {
        return c[t.toUpperCase()];
      }, a.prototype._setListeners = function () {
        var e = this;
        this.config.trigger.split(" ").forEach(function (n) {
          if ("click" === n) t(e.element).on(e.constructor.Event.CLICK, e.config.selector, function (t) {
            return e.toggle(t);
          });else if (n !== g.MANUAL) {
            var i = n === g.HOVER ? e.constructor.Event.MOUSEENTER : e.constructor.Event.FOCUSIN,
                o = n === g.HOVER ? e.constructor.Event.MOUSELEAVE : e.constructor.Event.FOCUSOUT;
            t(e.element).on(i, e.config.selector, function (t) {
              return e._enter(t);
            }).on(o, e.config.selector, function (t) {
              return e._leave(t);
            });
          }
          t(e.element).closest(".modal").on("hide.bs.modal", function () {
            return e.hide();
          });
        }), this.config.selector ? this.config = t.extend({}, this.config, {
          trigger: "manual",
          selector: ""
        }) : this._fixTitle();
      }, a.prototype._fixTitle = function () {
        var t = i(this.element.getAttribute("data-original-title"));
        (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
      }, a.prototype._enter = function (e, n) {
        var i = this.constructor.DATA_KEY;
        (n = n || t(e.currentTarget).data(i)) || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), e && (n._activeTrigger["focusin" === e.type ? g.FOCUS : g.HOVER] = !0), t(n.getTipElement()).hasClass(p.SHOW) || n._hoverState === d.SHOW ? n._hoverState = d.SHOW : (clearTimeout(n._timeout), n._hoverState = d.SHOW, n.config.delay && n.config.delay.show ? n._timeout = setTimeout(function () {
          n._hoverState === d.SHOW && n.show();
        }, n.config.delay.show) : n.show());
      }, a.prototype._leave = function (e, n) {
        var i = this.constructor.DATA_KEY;
        (n = n || t(e.currentTarget).data(i)) || (n = new this.constructor(e.currentTarget, this._getDelegateConfig()), t(e.currentTarget).data(i, n)), e && (n._activeTrigger["focusout" === e.type ? g.FOCUS : g.HOVER] = !1), n._isWithActiveTrigger() || (clearTimeout(n._timeout), n._hoverState = d.OUT, n.config.delay && n.config.delay.hide ? n._timeout = setTimeout(function () {
          n._hoverState === d.OUT && n.hide();
        }, n.config.delay.hide) : n.hide());
      }, a.prototype._isWithActiveTrigger = function () {
        for (var t in this._activeTrigger) if (this._activeTrigger[t]) return !0;

        return !1;
      }, a.prototype._getConfig = function (n) {
        return (n = t.extend({}, this.constructor.Default, t(this.element).data(), n)).delay && "number" == typeof n.delay && (n.delay = {
          show: n.delay,
          hide: n.delay
        }), n.title && "number" == typeof n.title && (n.title = n.title.toString()), n.content && "number" == typeof n.content && (n.content = n.content.toString()), r.typeCheckConfig(e, n, this.constructor.DefaultType), n;
      }, a.prototype._getDelegateConfig = function () {
        var t = {};
        if (this.config) for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
        return t;
      }, a.prototype._cleanTipClass = function () {
        var e = t(this.getTipElement()),
            n = e.attr("class").match(l);
        null !== n && n.length > 0 && e.removeClass(n.join(""));
      }, a.prototype._handlePopperPlacementChange = function (t) {
        this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
      }, a.prototype._fixTransition = function () {
        var e = this.getTipElement(),
            n = this.config.animation;
        null === e.getAttribute("x-placement") && (t(e).removeClass(p.FADE), this.config.animation = !1, this.hide(), this.show(), this.config.animation = n);
      }, a._jQueryInterface = function (e) {
        return this.each(function () {
          var n = t(this).data("bs.tooltip"),
              o = "object" === (void 0 === e ? "undefined" : i(e)) && e;

          if ((n || !/dispose|hide/.test(e)) && (n || (n = new a(this, o), t(this).data("bs.tooltip", n)), "string" == typeof e)) {
            if (void 0 === n[e]) throw new Error('No method named "' + e + '"');
            n[e]();
          }
        });
      }, o(a, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return u;
        }
      }, {
        key: "NAME",
        get: function () {
          return e;
        }
      }, {
        key: "DATA_KEY",
        get: function () {
          return "bs.tooltip";
        }
      }, {
        key: "Event",
        get: function () {
          return f;
        }
      }, {
        key: "EVENT_KEY",
        get: function () {
          return s;
        }
      }, {
        key: "DefaultType",
        get: function () {
          return h;
        }
      }]), a;
    }();

    return t.fn[e] = m._jQueryInterface, t.fn[e].Constructor = m, t.fn[e].noConflict = function () {
      return t.fn[e] = a, m._jQueryInterface;
    }, m;
  }(jQuery));

  !function (r) {
    var a = "popover",
        l = ".bs.popover",
        h = r.fn[a],
        c = new RegExp("(^|\\s)bs-popover\\S+", "g"),
        u = r.extend({}, s.Default, {
      placement: "right",
      trigger: "click",
      content: "",
      template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'
    }),
        d = r.extend({}, s.DefaultType, {
      content: "(string|element|function)"
    }),
        f = {
      FADE: "fade",
      SHOW: "show"
    },
        p = {
      TITLE: ".popover-header",
      CONTENT: ".popover-body"
    },
        _ = {
      HIDE: "hide" + l,
      HIDDEN: "hidden" + l,
      SHOW: "show" + l,
      SHOWN: "shown" + l,
      INSERTED: "inserted" + l,
      CLICK: "click" + l,
      FOCUSIN: "focusin" + l,
      FOCUSOUT: "focusout" + l,
      MOUSEENTER: "mouseenter" + l,
      MOUSELEAVE: "mouseleave" + l
    },
        g = function (s) {
      function h() {
        return n(this, h), t(this, s.apply(this, arguments));
      }

      return e(h, s), h.prototype.isWithContent = function () {
        return this.getTitle() || this._getContent();
      }, h.prototype.addAttachmentClass = function (t) {
        r(this.getTipElement()).addClass("bs-popover-" + t);
      }, h.prototype.getTipElement = function () {
        return this.tip = this.tip || r(this.config.template)[0];
      }, h.prototype.setContent = function () {
        var t = r(this.getTipElement());
        this.setElementContent(t.find(p.TITLE), this.getTitle()), this.setElementContent(t.find(p.CONTENT), this._getContent()), t.removeClass(f.FADE + " " + f.SHOW);
      }, h.prototype._getContent = function () {
        return this.element.getAttribute("data-content") || ("function" == typeof this.config.content ? this.config.content.call(this.element) : this.config.content);
      }, h.prototype._cleanTipClass = function () {
        var t = r(this.getTipElement()),
            e = t.attr("class").match(c);
        null !== e && e.length > 0 && t.removeClass(e.join(""));
      }, h._jQueryInterface = function (t) {
        return this.each(function () {
          var e = r(this).data("bs.popover"),
              n = "object" === (void 0 === t ? "undefined" : i(t)) ? t : null;

          if ((e || !/destroy|hide/.test(t)) && (e || (e = new h(this, n), r(this).data("bs.popover", e)), "string" == typeof t)) {
            if (void 0 === e[t]) throw new Error('No method named "' + t + '"');
            e[t]();
          }
        });
      }, o(h, null, [{
        key: "VERSION",
        get: function () {
          return "4.0.0-beta";
        }
      }, {
        key: "Default",
        get: function () {
          return u;
        }
      }, {
        key: "NAME",
        get: function () {
          return a;
        }
      }, {
        key: "DATA_KEY",
        get: function () {
          return "bs.popover";
        }
      }, {
        key: "Event",
        get: function () {
          return _;
        }
      }, {
        key: "EVENT_KEY",
        get: function () {
          return l;
        }
      }, {
        key: "DefaultType",
        get: function () {
          return d;
        }
      }]), h;
    }(s);

    r.fn[a] = g._jQueryInterface, r.fn[a].Constructor = g, r.fn[a].noConflict = function () {
      return r.fn[a] = h, g._jQueryInterface;
    };
  }(jQuery);
}();
/*!
 * Lightbox for Bootstrap by @ashleydw
 * https://github.com/ashleydw/lightbox
 *
 * License: https://github.com/ashleydw/lightbox/blob/master/LICENSE
 */
+function ($) {
  'use strict';

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ('value' in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError('Cannot call a class as a function');
    }
  }

  var Lightbox = function ($) {
    var NAME = 'ekkoLightbox';
    var JQUERY_NO_CONFLICT = $.fn[NAME];
    var Default = {
      title: '',
      footer: '',
      maxWidth: 9999,
      maxHeight: 9999,
      showArrows: true,
      //display the left / right arrows or not
      wrapping: true,
      //if true, gallery loops infinitely
      type: null,
      //force the lightbox into image / youtube mode. if null, or not image|youtube|vimeo; detect it
      alwaysShowClose: false,
      //always show the close button, even if there is no title
      loadingMessage: '<div class="ekko-lightbox-loader"><div><div></div><div></div></div></div>',
      // http://tobiasahlin.com/spinkit/
      leftArrow: '<span>&#10094;</span>',
      rightArrow: '<span>&#10095;</span>',
      strings: {
        close: 'Close',
        fail: 'Failed to load image:',
        type: 'Could not detect remote target type. Force the type using data-type'
      },
      doc: document,
      // if in an iframe can specify top.document
      onShow: function onShow() {},
      onShown: function onShown() {},
      onHide: function onHide() {},
      onHidden: function onHidden() {},
      onNavigate: function onNavigate() {},
      onContentLoaded: function onContentLoaded() {}
    };

    var Lightbox = function () {
      _createClass(Lightbox, null, [{
        key: 'Default',

        /**
         Class properties:
         _$element: null -> the <a> element currently being displayed
         _$modal: The bootstrap modal generated
         _$modalDialog: The .modal-dialog
         _$modalContent: The .modal-content
         _$modalBody: The .modal-body
         _$modalHeader: The .modal-header
         _$modalFooter: The .modal-footer
         _$lightboxContainerOne: Container of the first lightbox element
         _$lightboxContainerTwo: Container of the second lightbox element
         _$lightboxBody: First element in the container
         _$modalArrows: The overlayed arrows container
         _$galleryItems: Other <a>'s available for this gallery
         _galleryName: Name of the current data('gallery') showing
         _galleryIndex: The current index of the _$galleryItems being shown
         _config: {} the options for the modal
         _modalId: unique id for the current lightbox
         _padding / _border: CSS properties for the modal container; these are used to calculate the available space for the content
         */
        get: function get() {
          return Default;
        }
      }]);

      function Lightbox($element, config) {
        var _this = this;

        _classCallCheck(this, Lightbox);

        this._config = $.extend({}, Default, config);
        this._$modalArrows = null;
        this._galleryIndex = 0;
        this._galleryName = null;
        this._padding = 5;
        this._border = null;
        this._titleIsShown = false;
        this._footerIsShown = false;
        this._wantedWidth = 0;
        this._wantedHeight = 0;
        this._touchstartX = 0;
        this._touchendX = 0;
        this._modalId = 'ekkoLightbox-' + Math.floor(Math.random() * 1000 + 1);
        this._$element = $element instanceof jQuery ? $element : $($element);
        this._isBootstrap3 = $.fn.modal.Constructor.VERSION[0] == 3;
        var h4 = '<h4 class="modal-title">' + (this._config.title || "&nbsp;") + '</h4>';
        var btn = '<button type="button" class="close" data-dismiss="modal" aria-label="' + this._config.strings.close + '"><span aria-hidden="true">&times;</span></button>';
        var header = btn;
        var footer = '<div class="modal-footer' + (this._config.footer ? '' : ' hide') + '">' + (this._config.footer || "&nbsp;") + '</div>';
        var body = '<div class="modal-body"><div class="ekko-lightbox-container"><div class="ekko-lightbox-item fade in show"></div><div class="ekko-lightbox-item fade"></div></div></div>';
        var dialog = '<div class="modal-dialog" role="document"><div class="modal-content">' + header + body + footer + '</div></div>';
        $(this._config.doc.body).append('<div id="' + this._modalId + '" class="ekko-lightbox modal fade" tabindex="-1" tabindex="-1" role="dialog" aria-hidden="true">' + dialog + '</div>');
        this._$modal = $('#' + this._modalId, this._config.doc);
        this._$modalDialog = this._$modal.find('.modal-dialog').first();
        this._$modalContent = this._$modal.find('.modal-content').first();
        this._$modalBody = this._$modal.find('.modal-body').first();
        this._$modalHeader = this._$modal.find('.modal-header').first();
        this._$modalFooter = this._$modal.find('.modal-footer').first();
        this._$lightboxContainer = this._$modalBody.find('.ekko-lightbox-container').first();
        this._$lightboxBodyOne = this._$lightboxContainer.find('> div:first-child').first();
        this._$lightboxBodyTwo = this._$lightboxContainer.find('> div:last-child').first();
        this._border = this._calculateBorders();
        this._padding = this._calculatePadding();
        this._galleryName = this._$element.data('gallery');

        if (this._galleryName) {
          this._$galleryItems = $(document.body).find('*[data-gallery="' + this._galleryName + '"]');
          this._galleryIndex = this._$galleryItems.index(this._$element);
          $(document).on('keydown.ekkoLightbox', this._navigationalBinder.bind(this)); // add the directional arrows to the modal

          if (this._config.showArrows && this._$galleryItems.length > 1) {
            this._$lightboxContainer.append('<div class="ekko-lightbox-nav-overlay"><a href="#">' + this._config.leftArrow + '</a><a href="#">' + this._config.rightArrow + '</a></div>');

            this._$modalArrows = this._$lightboxContainer.find('div.ekko-lightbox-nav-overlay').first();

            this._$lightboxContainer.on('click', 'a:first-child', function (event) {
              event.preventDefault();
              return _this.navigateLeft();
            });

            this._$lightboxContainer.on('click', 'a:last-child', function (event) {
              event.preventDefault();
              return _this.navigateRight();
            });

            this.updateNavigation();
          }
        }

        this._$modal.on('show.bs.modal', this._config.onShow.bind(this)).on('shown.bs.modal', function () {
          _this._toggleLoading(true);

          _this._handle();

          return _this._config.onShown.call(_this);
        }).on('hide.bs.modal', this._config.onHide.bind(this)).on('hidden.bs.modal', function () {
          if (_this._galleryName) {
            $(document).off('keydown.ekkoLightbox');
            $(window).off('resize.ekkoLightbox');
          }

          _this._$modal.remove();

          return _this._config.onHidden.call(_this);
        }).modal(this._config);

        $(window).on('resize.ekkoLightbox', function () {
          _this._resize(_this._wantedWidth, _this._wantedHeight);
        });

        this._$lightboxContainer.on('touchstart', function () {
          _this._touchstartX = event.changedTouches[0].screenX;
        }).on('touchend', function () {
          _this._touchendX = event.changedTouches[0].screenX;

          _this._swipeGesure();
        });
      }

      _createClass(Lightbox, [{
        key: 'element',
        value: function element() {
          return this._$element;
        }
      }, {
        key: 'modal',
        value: function modal() {
          return this._$modal;
        }
      }, {
        key: 'navigateTo',
        value: function navigateTo(index) {
          if (index < 0 || index > this._$galleryItems.length - 1) return this;
          this._galleryIndex = index;
          this.updateNavigation();
          this._$element = $(this._$galleryItems.get(this._galleryIndex));

          this._handle();
        }
      }, {
        key: 'navigateLeft',
        value: function navigateLeft() {
          if (!this._$galleryItems) return;
          if (this._$galleryItems.length === 1) return;

          if (this._galleryIndex === 0) {
            if (this._config.wrapping) this._galleryIndex = this._$galleryItems.length - 1;else return;
          } else //circular
            this._galleryIndex--;

          this._config.onNavigate.call(this, 'left', this._galleryIndex);

          return this.navigateTo(this._galleryIndex);
        }
      }, {
        key: 'navigateRight',
        value: function navigateRight() {
          if (!this._$galleryItems) return;
          if (this._$galleryItems.length === 1) return;

          if (this._galleryIndex === this._$galleryItems.length - 1) {
            if (this._config.wrapping) this._galleryIndex = 0;else return;
          } else //circular
            this._galleryIndex++;

          this._config.onNavigate.call(this, 'right', this._galleryIndex);

          return this.navigateTo(this._galleryIndex);
        }
      }, {
        key: 'updateNavigation',
        value: function updateNavigation() {
          if (!this._config.wrapping) {
            var $nav = this._$lightboxContainer.find('div.ekko-lightbox-nav-overlay');

            if (this._galleryIndex === 0) $nav.find('a:first-child').addClass('disabled');else $nav.find('a:first-child').removeClass('disabled');
            if (this._galleryIndex === this._$galleryItems.length - 1) $nav.find('a:last-child').addClass('disabled');else $nav.find('a:last-child').removeClass('disabled');
          }
        }
      }, {
        key: 'close',
        value: function close() {
          return this._$modal.modal('hide');
        } // helper private methods

      }, {
        key: '_navigationalBinder',
        value: function _navigationalBinder(event) {
          event = event || window.event;
          if (event.keyCode === 39) return this.navigateRight();
          if (event.keyCode === 37) return this.navigateLeft();
        } // type detection private methods

      }, {
        key: '_detectRemoteType',
        value: function _detectRemoteType(src, type) {
          type = type || false;
          if (!type && this._isImage(src)) type = 'image';
          if (!type && this._getYoutubeId(src)) type = 'youtube';
          if (!type && this._getVimeoId(src)) type = 'vimeo';
          if (!type && this._getInstagramId(src)) type = 'instagram';
          if (!type || ['image', 'youtube', 'vimeo', 'instagram', 'video', 'url'].indexOf(type) < 0) type = 'url';
          return type;
        }
      }, {
        key: '_isImage',
        value: function _isImage(string) {
          return string && string.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
        }
      }, {
        key: '_containerToUse',
        value: function _containerToUse() {
          var _this2 = this; // if currently showing an image, fade it out and remove


          var $toUse = this._$lightboxBodyTwo;
          var $current = this._$lightboxBodyOne;

          if (this._$lightboxBodyTwo.hasClass('in')) {
            $toUse = this._$lightboxBodyOne;
            $current = this._$lightboxBodyTwo;
          }

          $current.removeClass('in show');
          setTimeout(function () {
            if (!_this2._$lightboxBodyTwo.hasClass('in')) _this2._$lightboxBodyTwo.empty();
            if (!_this2._$lightboxBodyOne.hasClass('in')) _this2._$lightboxBodyOne.empty();
          }, 500);
          $toUse.addClass('in show');
          return $toUse;
        }
      }, {
        key: '_handle',
        value: function _handle() {
          var $toUse = this._containerToUse();

          this._updateTitleAndFooter();

          var currentRemote = this._$element.attr('data-remote') || this._$element.attr('href');

          var currentType = this._detectRemoteType(currentRemote, this._$element.attr('data-type') || false);

          if (['image', 'youtube', 'vimeo', 'instagram', 'video', 'url'].indexOf(currentType) < 0) return this._error(this._config.strings.type);

          switch (currentType) {
            case 'image':
              this._preloadImage(currentRemote, $toUse);

              this._preloadImageByIndex(this._galleryIndex, 3);

              break;

            case 'youtube':
              this._showYoutubeVideo(currentRemote, $toUse);

              break;

            case 'vimeo':
              this._showVimeoVideo(this._getVimeoId(currentRemote), $toUse);

              break;

            case 'instagram':
              this._showInstagramVideo(this._getInstagramId(currentRemote), $toUse);

              break;

            case 'video':
              this._showHtml5Video(currentRemote, $toUse);

              break;

            default:
              // url
              this._loadRemoteContent(currentRemote, $toUse);

              break;
          }

          return this;
        }
      }, {
        key: '_getYoutubeId',
        value: function _getYoutubeId(string) {
          if (!string) return false;
          var matches = string.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
          return matches && matches[2].length === 11 ? matches[2] : false;
        }
      }, {
        key: '_getVimeoId',
        value: function _getVimeoId(string) {
          return string && string.indexOf('vimeo') > 0 ? string : false;
        }
      }, {
        key: '_getInstagramId',
        value: function _getInstagramId(string) {
          return string && string.indexOf('instagram') > 0 ? string : false;
        } // layout private methods

      }, {
        key: '_toggleLoading',
        value: function _toggleLoading(show) {
          show = show || false;

          if (show) {
            this._$modalDialog.css('display', 'none');

            this._$modal.removeClass('in show');

            $('.modal-backdrop').append(this._config.loadingMessage);
          } else {
            this._$modalDialog.css('display', 'block');

            this._$modal.addClass('in show');

            $('.modal-backdrop').find('.ekko-lightbox-loader').remove();
          }

          return this;
        }
      }, {
        key: '_calculateBorders',
        value: function _calculateBorders() {
          return {
            top: this._totalCssByAttribute('border-top-width'),
            right: this._totalCssByAttribute('border-right-width'),
            bottom: this._totalCssByAttribute('border-bottom-width'),
            left: this._totalCssByAttribute('border-left-width')
          };
        }
      }, {
        key: '_calculatePadding',
        value: function _calculatePadding() {
          return {
            top: this._totalCssByAttribute('padding-top'),
            right: this._totalCssByAttribute('padding-right'),
            bottom: this._totalCssByAttribute('padding-bottom'),
            left: this._totalCssByAttribute('padding-left')
          };
        }
      }, {
        key: '_totalCssByAttribute',
        value: function _totalCssByAttribute(attribute) {
          return parseInt(this._$modalDialog.css(attribute), 10) + parseInt(this._$modalContent.css(attribute), 10) + parseInt(this._$modalBody.css(attribute), 10);
        }
      }, {
        key: '_updateTitleAndFooter',
        value: function _updateTitleAndFooter() {
          var title = this._$element.data('title') || "";
          var caption = this._$element.data('footer') || "";
          this._titleIsShown = false;

          if (title || this._config.alwaysShowClose) {
            this._titleIsShown = true;

            this._$modalHeader.css('display', '').find('.modal-title').html(title || "&nbsp;");
          } else this._$modalHeader.css('display', 'none');

          this._footerIsShown = false;

          if (caption) {
            this._footerIsShown = true;

            this._$modalFooter.css('display', '').html(caption);
          } else this._$modalFooter.css('display', 'none');

          return this;
        }
      }, {
        key: '_showYoutubeVideo',
        value: function _showYoutubeVideo(remote, $containerForElement) {
          var id = this._getYoutubeId(remote);

          var query = remote.indexOf('&') > 0 ? remote.substr(remote.indexOf('&')) : '';
          var width = this._$element.data('width') || 560;
          var height = this._$element.data('height') || width / (560 / 315);
          return this._showVideoIframe('//www.youtube.com/embed/' + id + '?badge=0&autoplay=1&html5=1' + query, width, height, $containerForElement);
        }
      }, {
        key: '_showVimeoVideo',
        value: function _showVimeoVideo(id, $containerForElement) {
          var width = this._$element.data('width') || 500;
          var height = this._$element.data('height') || width / (560 / 315);
          return this._showVideoIframe(id + '?autoplay=1', width, height, $containerForElement);
        }
      }, {
        key: '_showInstagramVideo',
        value: function _showInstagramVideo(id, $containerForElement) {
          // instagram load their content into iframe's so this can be put straight into the element
          var width = this._$element.data('width') || 612;
          var height = width + 80;
          id = id.substr(-1) !== '/' ? id + '/' : id; // ensure id has trailing slash

          $containerForElement.html('<iframe width="' + width + '" height="' + height + '" src="' + id + 'embed/" frameborder="0" allowfullscreen></iframe>');

          this._resize(width, height);

          this._config.onContentLoaded.call(this);

          if (this._$modalArrows) //hide the arrows when showing video
            this._$modalArrows.css('display', 'none');

          this._toggleLoading(false);

          return this;
        }
      }, {
        key: '_showVideoIframe',
        value: function _showVideoIframe(url, width, height, $containerForElement) {
          // should be used for videos only. for remote content use loadRemoteContent (data-type=url)
          height = height || width; // default to square

          $containerForElement.html('<div class="embed-responsive embed-responsive-16by9"><iframe width="' + width + '" height="' + height + '" src="' + url + '" frameborder="0" allowfullscreen class="embed-responsive-item"></iframe></div>');

          this._resize(width, height);

          this._config.onContentLoaded.call(this);

          if (this._$modalArrows) this._$modalArrows.css('display', 'none'); //hide the arrows when showing video

          this._toggleLoading(false);

          return this;
        }
      }, {
        key: '_showHtml5Video',
        value: function _showHtml5Video(url, $containerForElement) {
          // should be used for videos only. for remote content use loadRemoteContent (data-type=url)
          var width = this._$element.data('width') || 560;
          var height = this._$element.data('height') || width / (560 / 315);
          $containerForElement.html('<div class="embed-responsive embed-responsive-16by9"><video width="' + width + '" height="' + height + '" src="' + url + '" preload="auto" autoplay controls class="embed-responsive-item"></video></div>');

          this._resize(width, height);

          this._config.onContentLoaded.call(this);

          if (this._$modalArrows) this._$modalArrows.css('display', 'none'); //hide the arrows when showing video

          this._toggleLoading(false);

          return this;
        }
      }, {
        key: '_loadRemoteContent',
        value: function _loadRemoteContent(url, $containerForElement) {
          var _this3 = this;

          var width = this._$element.data('width') || 560;
          var height = this._$element.data('height') || 560;
          var disableExternalCheck = this._$element.data('disableExternalCheck') || false;

          this._toggleLoading(false); // external urls are loading into an iframe
          // local ajax can be loaded into the container itself


          if (!disableExternalCheck && !this._isExternal(url)) {
            $containerForElement.load(url, $.proxy(function () {
              return _this3._$element.trigger('loaded.bs.modal');
              l;
            }));
          } else {
            $containerForElement.html('<iframe src="' + url + '" frameborder="0" allowfullscreen></iframe>');

            this._config.onContentLoaded.call(this);
          }

          if (this._$modalArrows) //hide the arrows when remote content
            this._$modalArrows.css('display', 'none');

          this._resize(width, height);

          return this;
        }
      }, {
        key: '_isExternal',
        value: function _isExternal(url) {
          var match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
          if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) return true;
          if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(':(' + {
            "http:": 80,
            "https:": 443
          }[location.protocol] + ')?$'), "") !== location.host) return true;
          return false;
        }
      }, {
        key: '_error',
        value: function _error(message) {
          console.error(message);

          this._containerToUse().html(message);

          this._resize(300, 300);

          return this;
        }
      }, {
        key: '_preloadImageByIndex',
        value: function _preloadImageByIndex(startIndex, numberOfTimes) {
          if (!this._$galleryItems) return;
          var next = $(this._$galleryItems.get(startIndex), false);
          if (typeof next == 'undefined') return;
          var src = next.attr('data-remote') || next.attr('href');
          if (next.attr('data-type') === 'image' || this._isImage(src)) this._preloadImage(src, false);
          if (numberOfTimes > 0) return this._preloadImageByIndex(startIndex + 1, numberOfTimes - 1);
        }
      }, {
        key: '_preloadImage',
        value: function _preloadImage(src, $containerForImage) {
          var _this4 = this;

          $containerForImage = $containerForImage || false;
          var img = new Image();

          if ($containerForImage) {
            (function () {
              // if loading takes > 200ms show a loader
              var loadingTimeout = setTimeout(function () {
                $containerForImage.append(_this4._config.loadingMessage);
              }, 200);

              img.onload = function () {
                if (loadingTimeout) clearTimeout(loadingTimeout);
                loadingTimeout = null;
                var image = $('<img />');
                image.attr('src', img.src);
                image.addClass('img-fluid'); // backward compatibility for bootstrap v3

                image.css('width', '100%');
                $containerForImage.html(image);
                if (_this4._$modalArrows) _this4._$modalArrows.css('display', ''); // remove display to default to css property

                _this4._resize(img.width, img.height);

                _this4._toggleLoading(false);

                return _this4._config.onContentLoaded.call(_this4);
              };

              img.onerror = function () {
                _this4._toggleLoading(false);

                return _this4._error(_this4._config.strings.fail + ('  ' + src));
              };
            })();
          }

          img.src = src;
          return img;
        }
      }, {
        key: '_swipeGesure',
        value: function _swipeGesure() {
          if (this._touchendX < this._touchstartX) {
            return this.navigateRight();
          }

          if (this._touchendX > this._touchstartX) {
            return this.navigateLeft();
          }
        }
      }, {
        key: '_resize',
        value: function _resize(width, height) {
          height = height || width;
          this._wantedWidth = width;
          this._wantedHeight = height;
          var imageAspecRatio = width / height; // if width > the available space, scale down the expected width and height

          var widthBorderAndPadding = this._padding.left + this._padding.right + this._border.left + this._border.right; // force 10px margin if window size > 575px

          var addMargin = this._config.doc.body.clientWidth > 575 ? 20 : 0;
          var discountMargin = this._config.doc.body.clientWidth > 575 ? 0 : 20;
          var maxWidth = Math.min(width + widthBorderAndPadding, this._config.doc.body.clientWidth - addMargin, this._config.maxWidth);

          if (width + widthBorderAndPadding > maxWidth) {
            height = (maxWidth - widthBorderAndPadding - discountMargin) / imageAspecRatio;
            width = maxWidth;
          } else width = width + widthBorderAndPadding;

          var headerHeight = 0,
              footerHeight = 0; // as the resize is performed the modal is show, the calculate might fail
          // if so, default to the default sizes

          if (this._footerIsShown) footerHeight = this._$modalFooter.outerHeight(true) || 55;
          if (this._titleIsShown) headerHeight = this._$modalHeader.outerHeight(true) || 67;
          var borderPadding = this._padding.top + this._padding.bottom + this._border.bottom + this._border.top; //calculated each time as resizing the window can cause them to change due to Bootstraps fluid margins

          var margins = parseFloat(this._$modalDialog.css('margin-top')) + parseFloat(this._$modalDialog.css('margin-bottom'));
          var maxHeight = Math.min(height, $(window).height() - borderPadding - margins - headerHeight - footerHeight, this._config.maxHeight - borderPadding - headerHeight - footerHeight);

          if (height > maxHeight) {
            // if height > the available height, scale down the width
            width = Math.ceil(maxHeight * imageAspecRatio) + widthBorderAndPadding;
          }

          this._$lightboxContainer.css('height', maxHeight);

          this._$modalDialog.css('flex', 1).css('maxWidth', width);

          var modal = this._$modal.data('bs.modal');

          if (modal) {
            // v4 method is mistakenly protected
            try {
              modal._handleUpdate();
            } catch (Exception) {
              modal.handleUpdate();
            }
          }

          return this;
        }
      }], [{
        key: '_jQueryInterface',
        value: function _jQueryInterface(config) {
          var _this5 = this;

          config = config || {};
          return this.each(function () {
            var $this = $(_this5);

            var _config = $.extend({}, Lightbox.Default, $this.data(), typeof config === 'object' && config);

            new Lightbox(_this5, _config);
          });
        }
      }]);

      return Lightbox;
    }();

    $.fn[NAME] = Lightbox._jQueryInterface;
    $.fn[NAME].Constructor = Lightbox;

    $.fn[NAME].noConflict = function () {
      $.fn[NAME] = JQUERY_NO_CONFLICT;
      return Lightbox._jQueryInterface;
    };

    return Lightbox;
  }(jQuery);
}(jQuery);
/*!
 * SlickNav Responsive Mobile Menu v1.0.10
 * (c) 2016 Josh Cope
 * licensed under MIT
 */
!function (e, t, n) {
  function a(t, n) {
    this.element = t, this.settings = e.extend({}, i, n), this.settings.duplicate || n.hasOwnProperty("removeIds") || (this.settings.removeIds = !1), this._defaults = i, this._name = s, this.init();
  }

  var i = {
    label: "MENU",
    duplicate: !0,
    duration: 200,
    easingOpen: "swing",
    easingClose: "swing",
    closedSymbol: "&#9658;",
    openedSymbol: "&#9660;",
    prependTo: "body",
    appendTo: "",
    parentTag: "a",
    closeOnClick: !1,
    allowParentLinks: !1,
    nestedParentLinks: !0,
    showChildren: !1,
    removeIds: !0,
    removeClasses: !1,
    removeStyles: !1,
    brand: "",
    animations: "jquery",
    init: function () {},
    beforeOpen: function () {},
    beforeClose: function () {},
    afterOpen: function () {},
    afterClose: function () {}
  },
      s = "slicknav",
      o = "slicknav",
      l = {
    DOWN: 40,
    ENTER: 13,
    ESCAPE: 27,
    LEFT: 37,
    RIGHT: 39,
    SPACE: 32,
    TAB: 9,
    UP: 38
  };
  a.prototype.init = function () {
    var n,
        a,
        i = this,
        s = e(this.element),
        r = this.settings;

    if (r.duplicate ? i.mobileNav = s.clone() : i.mobileNav = s, r.removeIds && (i.mobileNav.removeAttr("id"), i.mobileNav.find("*").each(function (t, n) {
      e(n).removeAttr("id");
    })), r.removeClasses && (i.mobileNav.removeAttr("class"), i.mobileNav.find("*").each(function (t, n) {
      e(n).removeAttr("class");
    })), r.removeStyles && (i.mobileNav.removeAttr("style"), i.mobileNav.find("*").each(function (t, n) {
      e(n).removeAttr("style");
    })), n = o + "_icon", "" === r.label && (n += " " + o + "_no-text"), "a" == r.parentTag && (r.parentTag = 'a href="#"'), i.mobileNav.attr("class", o + "_nav"), a = e('<div class="' + o + '_menu"></div>'), "" !== r.brand) {
      var c = e('<div class="' + o + '_brand">' + r.brand + "</div>");
      e(a).append(c);
    }

    i.btn = e(["<" + r.parentTag + ' aria-haspopup="true" role="button" tabindex="0" class="' + o + "_btn " + o + '_collapsed">', '<span class="' + o + '_menutxt">' + r.label + "</span>", '<span class="' + n + '">', '<span class="' + o + '_icon-bar"></span>', '<span class="' + o + '_icon-bar"></span>', '<span class="' + o + '_icon-bar"></span>', "</span>", "</" + r.parentTag + ">"].join("")), e(a).append(i.btn), "" !== r.appendTo ? e(r.appendTo).append(a) : e(r.prependTo).prepend(a), a.append(i.mobileNav);
    var p = i.mobileNav.find("li");
    e(p).each(function () {
      var t = e(this),
          n = {};

      if (n.children = t.children("ul").attr("role", "menu"), t.data("menu", n), n.children.length > 0) {
        var a = t.contents(),
            s = !1,
            l = [];
        e(a).each(function () {
          return e(this).is("ul") ? !1 : (l.push(this), void (e(this).is("a") && (s = !0)));
        });
        var c = e("<" + r.parentTag + ' role="menuitem" aria-haspopup="true" tabindex="-1" class="' + o + '_item"/>');
        if (r.allowParentLinks && !r.nestedParentLinks && s) e(l).wrapAll('<span class="' + o + "_parent-link " + o + '_row"/>').parent();else {
          var p = e(l).wrapAll(c).parent();
          p.addClass(o + "_row");
        }
        r.showChildren ? t.addClass(o + "_open") : t.addClass(o + "_collapsed"), t.addClass(o + "_parent");
        var d = e('<span class="' + o + '_arrow">' + (r.showChildren ? r.openedSymbol : r.closedSymbol) + "</span>");
        r.allowParentLinks && !r.nestedParentLinks && s && (d = d.wrap(c).parent()), e(l).last().after(d);
      } else 0 === t.children().length && t.addClass(o + "_txtnode");

      t.children("a").attr("role", "menuitem").click(function (t) {
        r.closeOnClick && !e(t.target).parent().closest("li").hasClass(o + "_parent") && e(i.btn).click();
      }), r.closeOnClick && r.allowParentLinks && (t.children("a").children("a").click(function (t) {
        e(i.btn).click();
      }), t.find("." + o + "_parent-link a:not(." + o + "_item)").click(function (t) {
        e(i.btn).click();
      }));
    }), e(p).each(function () {
      var t = e(this).data("menu");
      r.showChildren || i._visibilityToggle(t.children, null, !1, null, !0);
    }), i._visibilityToggle(i.mobileNav, null, !1, "init", !0), i.mobileNav.attr("role", "menu"), e(t).mousedown(function () {
      i._outlines(!1);
    }), e(t).keyup(function () {
      i._outlines(!0);
    }), e(i.btn).click(function (e) {
      e.preventDefault(), i._menuToggle();
    }), i.mobileNav.on("click", "." + o + "_item", function (t) {
      t.preventDefault(), i._itemClick(e(this));
    }), e(i.btn).keydown(function (t) {
      var n = t || event;

      switch (n.keyCode) {
        case l.ENTER:
        case l.SPACE:
        case l.DOWN:
          t.preventDefault(), n.keyCode === l.DOWN && e(i.btn).hasClass(o + "_open") || i._menuToggle(), e(i.btn).next().find('[role="menuitem"]').first().focus();
      }
    }), i.mobileNav.on("keydown", "." + o + "_item", function (t) {
      var n = t || event;

      switch (n.keyCode) {
        case l.ENTER:
          t.preventDefault(), i._itemClick(e(t.target));
          break;

        case l.RIGHT:
          t.preventDefault(), e(t.target).parent().hasClass(o + "_collapsed") && i._itemClick(e(t.target)), e(t.target).next().find('[role="menuitem"]').first().focus();
      }
    }), i.mobileNav.on("keydown", '[role="menuitem"]', function (t) {
      var n = t || event;

      switch (n.keyCode) {
        case l.DOWN:
          t.preventDefault();
          var a = e(t.target).parent().parent().children().children('[role="menuitem"]:visible'),
              s = a.index(t.target),
              r = s + 1;
          a.length <= r && (r = 0);
          var c = a.eq(r);
          c.focus();
          break;

        case l.UP:
          t.preventDefault();
          var a = e(t.target).parent().parent().children().children('[role="menuitem"]:visible'),
              s = a.index(t.target),
              c = a.eq(s - 1);
          c.focus();
          break;

        case l.LEFT:
          if (t.preventDefault(), e(t.target).parent().parent().parent().hasClass(o + "_open")) {
            var p = e(t.target).parent().parent().prev();
            p.focus(), i._itemClick(p);
          } else e(t.target).parent().parent().hasClass(o + "_nav") && (i._menuToggle(), e(i.btn).focus());

          break;

        case l.ESCAPE:
          t.preventDefault(), i._menuToggle(), e(i.btn).focus();
      }
    }), r.allowParentLinks && r.nestedParentLinks && e("." + o + "_item a").click(function (e) {
      e.stopImmediatePropagation();
    });
  }, a.prototype._menuToggle = function (e) {
    var t = this,
        n = t.btn,
        a = t.mobileNav;
    n.hasClass(o + "_collapsed") ? (n.removeClass(o + "_collapsed"), n.addClass(o + "_open")) : (n.removeClass(o + "_open"), n.addClass(o + "_collapsed")), n.addClass(o + "_animating"), t._visibilityToggle(a, n.parent(), !0, n);
  }, a.prototype._itemClick = function (e) {
    var t = this,
        n = t.settings,
        a = e.data("menu");
    a || (a = {}, a.arrow = e.children("." + o + "_arrow"), a.ul = e.next("ul"), a.parent = e.parent(), a.parent.hasClass(o + "_parent-link") && (a.parent = e.parent().parent(), a.ul = e.parent().next("ul")), e.data("menu", a)), a.parent.hasClass(o + "_collapsed") ? (a.arrow.html(n.openedSymbol), a.parent.removeClass(o + "_collapsed"), a.parent.addClass(o + "_open"), a.parent.addClass(o + "_animating"), t._visibilityToggle(a.ul, a.parent, !0, e)) : (a.arrow.html(n.closedSymbol), a.parent.addClass(o + "_collapsed"), a.parent.removeClass(o + "_open"), a.parent.addClass(o + "_animating"), t._visibilityToggle(a.ul, a.parent, !0, e));
  }, a.prototype._visibilityToggle = function (t, n, a, i, s) {
    function l(t, n) {
      e(t).removeClass(o + "_animating"), e(n).removeClass(o + "_animating"), s || p.afterOpen(t);
    }

    function r(n, a) {
      t.attr("aria-hidden", "true"), d.attr("tabindex", "-1"), c._setVisAttr(t, !0), t.hide(), e(n).removeClass(o + "_animating"), e(a).removeClass(o + "_animating"), s ? "init" == n && p.init() : p.afterClose(n);
    }

    var c = this,
        p = c.settings,
        d = c._getActionItems(t),
        u = 0;

    a && (u = p.duration), t.hasClass(o + "_hidden") ? (t.removeClass(o + "_hidden"), s || p.beforeOpen(i), "jquery" === p.animations ? t.stop(!0, !0).slideDown(u, p.easingOpen, function () {
      l(i, n);
    }) : "velocity" === p.animations && t.velocity("finish").velocity("slideDown", {
      duration: u,
      easing: p.easingOpen,
      complete: function () {
        l(i, n);
      }
    }), t.attr("aria-hidden", "false"), d.attr("tabindex", "0"), c._setVisAttr(t, !1)) : (t.addClass(o + "_hidden"), s || p.beforeClose(i), "jquery" === p.animations ? t.stop(!0, !0).slideUp(u, this.settings.easingClose, function () {
      r(i, n);
    }) : "velocity" === p.animations && t.velocity("finish").velocity("slideUp", {
      duration: u,
      easing: p.easingClose,
      complete: function () {
        r(i, n);
      }
    }));
  }, a.prototype._setVisAttr = function (t, n) {
    var a = this,
        i = t.children("li").children("ul").not("." + o + "_hidden");
    n ? i.each(function () {
      var t = e(this);
      t.attr("aria-hidden", "true");

      var i = a._getActionItems(t);

      i.attr("tabindex", "-1"), a._setVisAttr(t, n);
    }) : i.each(function () {
      var t = e(this);
      t.attr("aria-hidden", "false");

      var i = a._getActionItems(t);

      i.attr("tabindex", "0"), a._setVisAttr(t, n);
    });
  }, a.prototype._getActionItems = function (e) {
    var t = e.data("menu");

    if (!t) {
      t = {};
      var n = e.children("li"),
          a = n.find("a");
      t.links = a.add(n.find("." + o + "_item")), e.data("menu", t);
    }

    return t.links;
  }, a.prototype._outlines = function (t) {
    t ? e("." + o + "_item, ." + o + "_btn").css("outline", "") : e("." + o + "_item, ." + o + "_btn").css("outline", "none");
  }, a.prototype.toggle = function () {
    var e = this;

    e._menuToggle();
  }, a.prototype.open = function () {
    var e = this;
    e.btn.hasClass(o + "_collapsed") && e._menuToggle();
  }, a.prototype.close = function () {
    var e = this;
    e.btn.hasClass(o + "_open") && e._menuToggle();
  }, e.fn[s] = function (t) {
    var n = arguments;
    if (void 0 === t || "object" == typeof t) return this.each(function () {
      e.data(this, "plugin_" + s) || e.data(this, "plugin_" + s, new a(this, t));
    });

    if ("string" == typeof t && "_" !== t[0] && "init" !== t) {
      var i;
      return this.each(function () {
        var o = e.data(this, "plugin_" + s);
        o instanceof a && "function" == typeof o[t] && (i = o[t].apply(o, Array.prototype.slice.call(n, 1)));
      }), void 0 !== i ? i : this;
    }
  };
}(jQuery, document, window);
//! moment.js
;

(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : global.moment = factory();
})(this, function () {
  'use strict';

  var hookCallback;

  function hooks() {
    return hookCallback.apply(null, arguments);
  } // This is done to register the method called with moment()
  // without creating circular dependencies.


  function setHookCallback(callback) {
    hookCallback = callback;
  }

  function isArray(input) {
    return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
  }

  function isObject(input) {
    // IE8 will treat undefined and null as object if it wasn't for
    // input != null
    return input != null && Object.prototype.toString.call(input) === '[object Object]';
  }

  function isObjectEmpty(obj) {
    if (Object.getOwnPropertyNames) {
      return Object.getOwnPropertyNames(obj).length === 0;
    } else {
      var k;

      for (k in obj) {
        if (obj.hasOwnProperty(k)) {
          return false;
        }
      }

      return true;
    }
  }

  function isUndefined(input) {
    return input === void 0;
  }

  function isNumber(input) {
    return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
  }

  function isDate(input) {
    return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
  }

  function map(arr, fn) {
    var res = [],
        i;

    for (i = 0; i < arr.length; ++i) {
      res.push(fn(arr[i], i));
    }

    return res;
  }

  function hasOwnProp(a, b) {
    return Object.prototype.hasOwnProperty.call(a, b);
  }

  function extend(a, b) {
    for (var i in b) {
      if (hasOwnProp(b, i)) {
        a[i] = b[i];
      }
    }

    if (hasOwnProp(b, 'toString')) {
      a.toString = b.toString;
    }

    if (hasOwnProp(b, 'valueOf')) {
      a.valueOf = b.valueOf;
    }

    return a;
  }

  function createUTC(input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
  }

  function defaultParsingFlags() {
    // We need to deep clone this object.
    return {
      empty: false,
      unusedTokens: [],
      unusedInput: [],
      overflow: -2,
      charsLeftOver: 0,
      nullInput: false,
      invalidMonth: null,
      invalidFormat: false,
      userInvalidated: false,
      iso: false,
      parsedDateParts: [],
      meridiem: null,
      rfc2822: false,
      weekdayMismatch: false
    };
  }

  function getParsingFlags(m) {
    if (m._pf == null) {
      m._pf = defaultParsingFlags();
    }

    return m._pf;
  }

  var some;

  if (Array.prototype.some) {
    some = Array.prototype.some;
  } else {
    some = function (fun) {
      var t = Object(this);
      var len = t.length >>> 0;

      for (var i = 0; i < len; i++) {
        if (i in t && fun.call(this, t[i], i, t)) {
          return true;
        }
      }

      return false;
    };
  }

  function isValid(m) {
    if (m._isValid == null) {
      var flags = getParsingFlags(m);
      var parsedParts = some.call(flags.parsedDateParts, function (i) {
        return i != null;
      });
      var isNowValid = !isNaN(m._d.getTime()) && flags.overflow < 0 && !flags.empty && !flags.invalidMonth && !flags.invalidWeekday && !flags.weekdayMismatch && !flags.nullInput && !flags.invalidFormat && !flags.userInvalidated && (!flags.meridiem || flags.meridiem && parsedParts);

      if (m._strict) {
        isNowValid = isNowValid && flags.charsLeftOver === 0 && flags.unusedTokens.length === 0 && flags.bigHour === undefined;
      }

      if (Object.isFrozen == null || !Object.isFrozen(m)) {
        m._isValid = isNowValid;
      } else {
        return isNowValid;
      }
    }

    return m._isValid;
  }

  function createInvalid(flags) {
    var m = createUTC(NaN);

    if (flags != null) {
      extend(getParsingFlags(m), flags);
    } else {
      getParsingFlags(m).userInvalidated = true;
    }

    return m;
  } // Plugins that add properties should also add the key here (null value),
  // so we can properly clone ourselves.


  var momentProperties = hooks.momentProperties = [];

  function copyConfig(to, from) {
    var i, prop, val;

    if (!isUndefined(from._isAMomentObject)) {
      to._isAMomentObject = from._isAMomentObject;
    }

    if (!isUndefined(from._i)) {
      to._i = from._i;
    }

    if (!isUndefined(from._f)) {
      to._f = from._f;
    }

    if (!isUndefined(from._l)) {
      to._l = from._l;
    }

    if (!isUndefined(from._strict)) {
      to._strict = from._strict;
    }

    if (!isUndefined(from._tzm)) {
      to._tzm = from._tzm;
    }

    if (!isUndefined(from._isUTC)) {
      to._isUTC = from._isUTC;
    }

    if (!isUndefined(from._offset)) {
      to._offset = from._offset;
    }

    if (!isUndefined(from._pf)) {
      to._pf = getParsingFlags(from);
    }

    if (!isUndefined(from._locale)) {
      to._locale = from._locale;
    }

    if (momentProperties.length > 0) {
      for (i = 0; i < momentProperties.length; i++) {
        prop = momentProperties[i];
        val = from[prop];

        if (!isUndefined(val)) {
          to[prop] = val;
        }
      }
    }

    return to;
  }

  var updateInProgress = false; // Moment prototype object

  function Moment(config) {
    copyConfig(this, config);
    this._d = new Date(config._d != null ? config._d.getTime() : NaN);

    if (!this.isValid()) {
      this._d = new Date(NaN);
    } // Prevent infinite loop in case updateOffset creates new moment
    // objects.


    if (updateInProgress === false) {
      updateInProgress = true;
      hooks.updateOffset(this);
      updateInProgress = false;
    }
  }

  function isMoment(obj) {
    return obj instanceof Moment || obj != null && obj._isAMomentObject != null;
  }

  function absFloor(number) {
    if (number < 0) {
      // -0 -> 0
      return Math.ceil(number) || 0;
    } else {
      return Math.floor(number);
    }
  }

  function toInt(argumentForCoercion) {
    var coercedNumber = +argumentForCoercion,
        value = 0;

    if (coercedNumber !== 0 && isFinite(coercedNumber)) {
      value = absFloor(coercedNumber);
    }

    return value;
  } // compare two arrays, return the number of differences


  function compareArrays(array1, array2, dontConvert) {
    var len = Math.min(array1.length, array2.length),
        lengthDiff = Math.abs(array1.length - array2.length),
        diffs = 0,
        i;

    for (i = 0; i < len; i++) {
      if (dontConvert && array1[i] !== array2[i] || !dontConvert && toInt(array1[i]) !== toInt(array2[i])) {
        diffs++;
      }
    }

    return diffs + lengthDiff;
  }

  function warn(msg) {
    if (hooks.suppressDeprecationWarnings === false && typeof console !== 'undefined' && console.warn) {
      console.warn('Deprecation warning: ' + msg);
    }
  }

  function deprecate(msg, fn) {
    var firstTime = true;
    return extend(function () {
      if (hooks.deprecationHandler != null) {
        hooks.deprecationHandler(null, msg);
      }

      if (firstTime) {
        var args = [];
        var arg;

        for (var i = 0; i < arguments.length; i++) {
          arg = '';

          if (typeof arguments[i] === 'object') {
            arg += '\n[' + i + '] ';

            for (var key in arguments[0]) {
              arg += key + ': ' + arguments[0][key] + ', ';
            }

            arg = arg.slice(0, -2); // Remove trailing comma and space
          } else {
            arg = arguments[i];
          }

          args.push(arg);
        }

        warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + new Error().stack);
        firstTime = false;
      }

      return fn.apply(this, arguments);
    }, fn);
  }

  var deprecations = {};

  function deprecateSimple(name, msg) {
    if (hooks.deprecationHandler != null) {
      hooks.deprecationHandler(name, msg);
    }

    if (!deprecations[name]) {
      warn(msg);
      deprecations[name] = true;
    }
  }

  hooks.suppressDeprecationWarnings = false;
  hooks.deprecationHandler = null;

  function isFunction(input) {
    return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
  }

  function set(config) {
    var prop, i;

    for (i in config) {
      prop = config[i];

      if (isFunction(prop)) {
        this[i] = prop;
      } else {
        this['_' + i] = prop;
      }
    }

    this._config = config; // Lenient ordinal parsing accepts just a number in addition to
    // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
    // TODO: Remove "ordinalParse" fallback in next major release.

    this._dayOfMonthOrdinalParseLenient = new RegExp((this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) + '|' + /\d{1,2}/.source);
  }

  function mergeConfigs(parentConfig, childConfig) {
    var res = extend({}, parentConfig),
        prop;

    for (prop in childConfig) {
      if (hasOwnProp(childConfig, prop)) {
        if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
          res[prop] = {};
          extend(res[prop], parentConfig[prop]);
          extend(res[prop], childConfig[prop]);
        } else if (childConfig[prop] != null) {
          res[prop] = childConfig[prop];
        } else {
          delete res[prop];
        }
      }
    }

    for (prop in parentConfig) {
      if (hasOwnProp(parentConfig, prop) && !hasOwnProp(childConfig, prop) && isObject(parentConfig[prop])) {
        // make sure changes to properties don't modify parent config
        res[prop] = extend({}, res[prop]);
      }
    }

    return res;
  }

  function Locale(config) {
    if (config != null) {
      this.set(config);
    }
  }

  var keys;

  if (Object.keys) {
    keys = Object.keys;
  } else {
    keys = function (obj) {
      var i,
          res = [];

      for (i in obj) {
        if (hasOwnProp(obj, i)) {
          res.push(i);
        }
      }

      return res;
    };
  }

  var defaultCalendar = {
    sameDay: '[Today at] LT',
    nextDay: '[Tomorrow at] LT',
    nextWeek: 'dddd [at] LT',
    lastDay: '[Yesterday at] LT',
    lastWeek: '[Last] dddd [at] LT',
    sameElse: 'L'
  };

  function calendar(key, mom, now) {
    var output = this._calendar[key] || this._calendar['sameElse'];
    return isFunction(output) ? output.call(mom, now) : output;
  }

  var defaultLongDateFormat = {
    LTS: 'h:mm:ss A',
    LT: 'h:mm A',
    L: 'MM/DD/YYYY',
    LL: 'MMMM D, YYYY',
    LLL: 'MMMM D, YYYY h:mm A',
    LLLL: 'dddd, MMMM D, YYYY h:mm A'
  };

  function longDateFormat(key) {
    var format = this._longDateFormat[key],
        formatUpper = this._longDateFormat[key.toUpperCase()];

    if (format || !formatUpper) {
      return format;
    }

    this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
      return val.slice(1);
    });
    return this._longDateFormat[key];
  }

  var defaultInvalidDate = 'Invalid date';

  function invalidDate() {
    return this._invalidDate;
  }

  var defaultOrdinal = '%d';
  var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

  function ordinal(number) {
    return this._ordinal.replace('%d', number);
  }

  var defaultRelativeTime = {
    future: 'in %s',
    past: '%s ago',
    s: 'a few seconds',
    ss: '%d seconds',
    m: 'a minute',
    mm: '%d minutes',
    h: 'an hour',
    hh: '%d hours',
    d: 'a day',
    dd: '%d days',
    M: 'a month',
    MM: '%d months',
    y: 'a year',
    yy: '%d years'
  };

  function relativeTime(number, withoutSuffix, string, isFuture) {
    var output = this._relativeTime[string];
    return isFunction(output) ? output(number, withoutSuffix, string, isFuture) : output.replace(/%d/i, number);
  }

  function pastFuture(diff, output) {
    var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
    return isFunction(format) ? format(output) : format.replace(/%s/i, output);
  }

  var aliases = {};

  function addUnitAlias(unit, shorthand) {
    var lowerCase = unit.toLowerCase();
    aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
  }

  function normalizeUnits(units) {
    return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
  }

  function normalizeObjectUnits(inputObject) {
    var normalizedInput = {},
        normalizedProp,
        prop;

    for (prop in inputObject) {
      if (hasOwnProp(inputObject, prop)) {
        normalizedProp = normalizeUnits(prop);

        if (normalizedProp) {
          normalizedInput[normalizedProp] = inputObject[prop];
        }
      }
    }

    return normalizedInput;
  }

  var priorities = {};

  function addUnitPriority(unit, priority) {
    priorities[unit] = priority;
  }

  function getPrioritizedUnits(unitsObj) {
    var units = [];

    for (var u in unitsObj) {
      units.push({
        unit: u,
        priority: priorities[u]
      });
    }

    units.sort(function (a, b) {
      return a.priority - b.priority;
    });
    return units;
  }

  function zeroFill(number, targetLength, forceSign) {
    var absNumber = '' + Math.abs(number),
        zerosToFill = targetLength - absNumber.length,
        sign = number >= 0;
    return (sign ? forceSign ? '+' : '' : '-') + Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
  }

  var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;
  var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;
  var formatFunctions = {};
  var formatTokenFunctions = {}; // token:    'M'
  // padded:   ['MM', 2]
  // ordinal:  'Mo'
  // callback: function () { this.month() + 1 }

  function addFormatToken(token, padded, ordinal, callback) {
    var func = callback;

    if (typeof callback === 'string') {
      func = function () {
        return this[callback]();
      };
    }

    if (token) {
      formatTokenFunctions[token] = func;
    }

    if (padded) {
      formatTokenFunctions[padded[0]] = function () {
        return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
      };
    }

    if (ordinal) {
      formatTokenFunctions[ordinal] = function () {
        return this.localeData().ordinal(func.apply(this, arguments), token);
      };
    }
  }

  function removeFormattingTokens(input) {
    if (input.match(/\[[\s\S]/)) {
      return input.replace(/^\[|\]$/g, '');
    }

    return input.replace(/\\/g, '');
  }

  function makeFormatFunction(format) {
    var array = format.match(formattingTokens),
        i,
        length;

    for (i = 0, length = array.length; i < length; i++) {
      if (formatTokenFunctions[array[i]]) {
        array[i] = formatTokenFunctions[array[i]];
      } else {
        array[i] = removeFormattingTokens(array[i]);
      }
    }

    return function (mom) {
      var output = '',
          i;

      for (i = 0; i < length; i++) {
        output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
      }

      return output;
    };
  } // format date using native date object


  function formatMoment(m, format) {
    if (!m.isValid()) {
      return m.localeData().invalidDate();
    }

    format = expandFormat(format, m.localeData());
    formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);
    return formatFunctions[format](m);
  }

  function expandFormat(format, locale) {
    var i = 5;

    function replaceLongDateFormatTokens(input) {
      return locale.longDateFormat(input) || input;
    }

    localFormattingTokens.lastIndex = 0;

    while (i >= 0 && localFormattingTokens.test(format)) {
      format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
      localFormattingTokens.lastIndex = 0;
      i -= 1;
    }

    return format;
  }

  var match1 = /\d/; //       0 - 9

  var match2 = /\d\d/; //      00 - 99

  var match3 = /\d{3}/; //     000 - 999

  var match4 = /\d{4}/; //    0000 - 9999

  var match6 = /[+-]?\d{6}/; // -999999 - 999999

  var match1to2 = /\d\d?/; //       0 - 99

  var match3to4 = /\d\d\d\d?/; //     999 - 9999

  var match5to6 = /\d\d\d\d\d\d?/; //   99999 - 999999

  var match1to3 = /\d{1,3}/; //       0 - 999

  var match1to4 = /\d{1,4}/; //       0 - 9999

  var match1to6 = /[+-]?\d{1,6}/; // -999999 - 999999

  var matchUnsigned = /\d+/; //       0 - inf

  var matchSigned = /[+-]?\d+/; //    -inf - inf

  var matchOffset = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z

  var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

  var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123
  // any word (or two) characters or numbers including two/three word month in arabic.
  // includes scottish gaelic two word and hyphenated months

  var matchWord = /[0-9]{0,256}['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFF07\uFF10-\uFFEF]{1,256}|[\u0600-\u06FF\/]{1,256}(\s*?[\u0600-\u06FF]{1,256}){1,2}/i;
  var regexes = {};

  function addRegexToken(token, regex, strictRegex) {
    regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
      return isStrict && strictRegex ? strictRegex : regex;
    };
  }

  function getParseRegexForToken(token, config) {
    if (!hasOwnProp(regexes, token)) {
      return new RegExp(unescapeFormat(token));
    }

    return regexes[token](config._strict, config._locale);
  } // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript


  function unescapeFormat(s) {
    return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
      return p1 || p2 || p3 || p4;
    }));
  }

  function regexEscape(s) {
    return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
  }

  var tokens = {};

  function addParseToken(token, callback) {
    var i,
        func = callback;

    if (typeof token === 'string') {
      token = [token];
    }

    if (isNumber(callback)) {
      func = function (input, array) {
        array[callback] = toInt(input);
      };
    }

    for (i = 0; i < token.length; i++) {
      tokens[token[i]] = func;
    }
  }

  function addWeekParseToken(token, callback) {
    addParseToken(token, function (input, array, config, token) {
      config._w = config._w || {};
      callback(input, config._w, config, token);
    });
  }

  function addTimeToArrayFromToken(token, input, config) {
    if (input != null && hasOwnProp(tokens, token)) {
      tokens[token](input, config._a, config, token);
    }
  }

  var YEAR = 0;
  var MONTH = 1;
  var DATE = 2;
  var HOUR = 3;
  var MINUTE = 4;
  var SECOND = 5;
  var MILLISECOND = 6;
  var WEEK = 7;
  var WEEKDAY = 8; // FORMATTING

  addFormatToken('Y', 0, 0, function () {
    var y = this.year();
    return y <= 9999 ? '' + y : '+' + y;
  });
  addFormatToken(0, ['YY', 2], 0, function () {
    return this.year() % 100;
  });
  addFormatToken(0, ['YYYY', 4], 0, 'year');
  addFormatToken(0, ['YYYYY', 5], 0, 'year');
  addFormatToken(0, ['YYYYYY', 6, true], 0, 'year'); // ALIASES

  addUnitAlias('year', 'y'); // PRIORITIES

  addUnitPriority('year', 1); // PARSING

  addRegexToken('Y', matchSigned);
  addRegexToken('YY', match1to2, match2);
  addRegexToken('YYYY', match1to4, match4);
  addRegexToken('YYYYY', match1to6, match6);
  addRegexToken('YYYYYY', match1to6, match6);
  addParseToken(['YYYYY', 'YYYYYY'], YEAR);
  addParseToken('YYYY', function (input, array) {
    array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
  });
  addParseToken('YY', function (input, array) {
    array[YEAR] = hooks.parseTwoDigitYear(input);
  });
  addParseToken('Y', function (input, array) {
    array[YEAR] = parseInt(input, 10);
  }); // HELPERS

  function daysInYear(year) {
    return isLeapYear(year) ? 366 : 365;
  }

  function isLeapYear(year) {
    return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
  } // HOOKS


  hooks.parseTwoDigitYear = function (input) {
    return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
  }; // MOMENTS


  var getSetYear = makeGetSet('FullYear', true);

  function getIsLeapYear() {
    return isLeapYear(this.year());
  }

  function makeGetSet(unit, keepTime) {
    return function (value) {
      if (value != null) {
        set$1(this, unit, value);
        hooks.updateOffset(this, keepTime);
        return this;
      } else {
        return get(this, unit);
      }
    };
  }

  function get(mom, unit) {
    return mom.isValid() ? mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
  }

  function set$1(mom, unit, value) {
    if (mom.isValid() && !isNaN(value)) {
      if (unit === 'FullYear' && isLeapYear(mom.year()) && mom.month() === 1 && mom.date() === 29) {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
      } else {
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
      }
    }
  } // MOMENTS


  function stringGet(units) {
    units = normalizeUnits(units);

    if (isFunction(this[units])) {
      return this[units]();
    }

    return this;
  }

  function stringSet(units, value) {
    if (typeof units === 'object') {
      units = normalizeObjectUnits(units);
      var prioritized = getPrioritizedUnits(units);

      for (var i = 0; i < prioritized.length; i++) {
        this[prioritized[i].unit](units[prioritized[i].unit]);
      }
    } else {
      units = normalizeUnits(units);

      if (isFunction(this[units])) {
        return this[units](value);
      }
    }

    return this;
  }

  function mod(n, x) {
    return (n % x + x) % x;
  }

  var indexOf;

  if (Array.prototype.indexOf) {
    indexOf = Array.prototype.indexOf;
  } else {
    indexOf = function (o) {
      // I know
      var i;

      for (i = 0; i < this.length; ++i) {
        if (this[i] === o) {
          return i;
        }
      }

      return -1;
    };
  }

  function daysInMonth(year, month) {
    if (isNaN(year) || isNaN(month)) {
      return NaN;
    }

    var modMonth = mod(month, 12);
    year += (month - modMonth) / 12;
    return modMonth === 1 ? isLeapYear(year) ? 29 : 28 : 31 - modMonth % 7 % 2;
  } // FORMATTING


  addFormatToken('M', ['MM', 2], 'Mo', function () {
    return this.month() + 1;
  });
  addFormatToken('MMM', 0, 0, function (format) {
    return this.localeData().monthsShort(this, format);
  });
  addFormatToken('MMMM', 0, 0, function (format) {
    return this.localeData().months(this, format);
  }); // ALIASES

  addUnitAlias('month', 'M'); // PRIORITY

  addUnitPriority('month', 8); // PARSING

  addRegexToken('M', match1to2);
  addRegexToken('MM', match1to2, match2);
  addRegexToken('MMM', function (isStrict, locale) {
    return locale.monthsShortRegex(isStrict);
  });
  addRegexToken('MMMM', function (isStrict, locale) {
    return locale.monthsRegex(isStrict);
  });
  addParseToken(['M', 'MM'], function (input, array) {
    array[MONTH] = toInt(input) - 1;
  });
  addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
    var month = config._locale.monthsParse(input, token, config._strict); // if we didn't find a month name, mark the date as invalid.


    if (month != null) {
      array[MONTH] = month;
    } else {
      getParsingFlags(config).invalidMonth = input;
    }
  }); // LOCALES

  var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
  var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');

  function localeMonths(m, format) {
    if (!m) {
      return isArray(this._months) ? this._months : this._months['standalone'];
    }

    return isArray(this._months) ? this._months[m.month()] : this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
  }

  var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');

  function localeMonthsShort(m, format) {
    if (!m) {
      return isArray(this._monthsShort) ? this._monthsShort : this._monthsShort['standalone'];
    }

    return isArray(this._monthsShort) ? this._monthsShort[m.month()] : this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
  }

  function handleStrictParse(monthName, format, strict) {
    var i,
        ii,
        mom,
        llc = monthName.toLocaleLowerCase();

    if (!this._monthsParse) {
      // this is not used
      this._monthsParse = [];
      this._longMonthsParse = [];
      this._shortMonthsParse = [];

      for (i = 0; i < 12; ++i) {
        mom = createUTC([2000, i]);
        this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
        this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
      }
    }

    if (strict) {
      if (format === 'MMM') {
        ii = indexOf.call(this._shortMonthsParse, llc);
        return ii !== -1 ? ii : null;
      } else {
        ii = indexOf.call(this._longMonthsParse, llc);
        return ii !== -1 ? ii : null;
      }
    } else {
      if (format === 'MMM') {
        ii = indexOf.call(this._shortMonthsParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._longMonthsParse, llc);
        return ii !== -1 ? ii : null;
      } else {
        ii = indexOf.call(this._longMonthsParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._shortMonthsParse, llc);
        return ii !== -1 ? ii : null;
      }
    }
  }

  function localeMonthsParse(monthName, format, strict) {
    var i, mom, regex;

    if (this._monthsParseExact) {
      return handleStrictParse.call(this, monthName, format, strict);
    }

    if (!this._monthsParse) {
      this._monthsParse = [];
      this._longMonthsParse = [];
      this._shortMonthsParse = [];
    } // TODO: add sorting
    // Sorting makes sure if one month (or abbr) is a prefix of another
    // see sorting in computeMonthsParse


    for (i = 0; i < 12; i++) {
      // make the regex if we don't have it already
      mom = createUTC([2000, i]);

      if (strict && !this._longMonthsParse[i]) {
        this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
        this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
      }

      if (!strict && !this._monthsParse[i]) {
        regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
        this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
      } // test the regex


      if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
        return i;
      } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
        return i;
      } else if (!strict && this._monthsParse[i].test(monthName)) {
        return i;
      }
    }
  } // MOMENTS


  function setMonth(mom, value) {
    var dayOfMonth;

    if (!mom.isValid()) {
      // No op
      return mom;
    }

    if (typeof value === 'string') {
      if (/^\d+$/.test(value)) {
        value = toInt(value);
      } else {
        value = mom.localeData().monthsParse(value); // TODO: Another silent failure?

        if (!isNumber(value)) {
          return mom;
        }
      }
    }

    dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));

    mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);

    return mom;
  }

  function getSetMonth(value) {
    if (value != null) {
      setMonth(this, value);
      hooks.updateOffset(this, true);
      return this;
    } else {
      return get(this, 'Month');
    }
  }

  function getDaysInMonth() {
    return daysInMonth(this.year(), this.month());
  }

  var defaultMonthsShortRegex = matchWord;

  function monthsShortRegex(isStrict) {
    if (this._monthsParseExact) {
      if (!hasOwnProp(this, '_monthsRegex')) {
        computeMonthsParse.call(this);
      }

      if (isStrict) {
        return this._monthsShortStrictRegex;
      } else {
        return this._monthsShortRegex;
      }
    } else {
      if (!hasOwnProp(this, '_monthsShortRegex')) {
        this._monthsShortRegex = defaultMonthsShortRegex;
      }

      return this._monthsShortStrictRegex && isStrict ? this._monthsShortStrictRegex : this._monthsShortRegex;
    }
  }

  var defaultMonthsRegex = matchWord;

  function monthsRegex(isStrict) {
    if (this._monthsParseExact) {
      if (!hasOwnProp(this, '_monthsRegex')) {
        computeMonthsParse.call(this);
      }

      if (isStrict) {
        return this._monthsStrictRegex;
      } else {
        return this._monthsRegex;
      }
    } else {
      if (!hasOwnProp(this, '_monthsRegex')) {
        this._monthsRegex = defaultMonthsRegex;
      }

      return this._monthsStrictRegex && isStrict ? this._monthsStrictRegex : this._monthsRegex;
    }
  }

  function computeMonthsParse() {
    function cmpLenRev(a, b) {
      return b.length - a.length;
    }

    var shortPieces = [],
        longPieces = [],
        mixedPieces = [],
        i,
        mom;

    for (i = 0; i < 12; i++) {
      // make the regex if we don't have it already
      mom = createUTC([2000, i]);
      shortPieces.push(this.monthsShort(mom, ''));
      longPieces.push(this.months(mom, ''));
      mixedPieces.push(this.months(mom, ''));
      mixedPieces.push(this.monthsShort(mom, ''));
    } // Sorting makes sure if one month (or abbr) is a prefix of another it
    // will match the longer piece.


    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);

    for (i = 0; i < 12; i++) {
      shortPieces[i] = regexEscape(shortPieces[i]);
      longPieces[i] = regexEscape(longPieces[i]);
    }

    for (i = 0; i < 24; i++) {
      mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._monthsShortRegex = this._monthsRegex;
    this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
  }

  function createDate(y, m, d, h, M, s, ms) {
    // can't just apply() to create a date:
    // https://stackoverflow.com/q/181348
    var date; // the date constructor remaps years 0-99 to 1900-1999

    if (y < 100 && y >= 0) {
      // preserve leap years using a full 400 year cycle, then reset
      date = new Date(y + 400, m, d, h, M, s, ms);

      if (isFinite(date.getFullYear())) {
        date.setFullYear(y);
      }
    } else {
      date = new Date(y, m, d, h, M, s, ms);
    }

    return date;
  }

  function createUTCDate(y) {
    var date; // the Date.UTC function remaps years 0-99 to 1900-1999

    if (y < 100 && y >= 0) {
      var args = Array.prototype.slice.call(arguments); // preserve leap years using a full 400 year cycle, then reset

      args[0] = y + 400;
      date = new Date(Date.UTC.apply(null, args));

      if (isFinite(date.getUTCFullYear())) {
        date.setUTCFullYear(y);
      }
    } else {
      date = new Date(Date.UTC.apply(null, arguments));
    }

    return date;
  } // start-of-first-week - start-of-year


  function firstWeekOffset(year, dow, doy) {
    var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
    fwd = 7 + dow - doy,
        // first-week day local weekday -- which local weekday is fwd
    fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;
    return -fwdlw + fwd - 1;
  } // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday


  function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
    var localWeekday = (7 + weekday - dow) % 7,
        weekOffset = firstWeekOffset(year, dow, doy),
        dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
        resYear,
        resDayOfYear;

    if (dayOfYear <= 0) {
      resYear = year - 1;
      resDayOfYear = daysInYear(resYear) + dayOfYear;
    } else if (dayOfYear > daysInYear(year)) {
      resYear = year + 1;
      resDayOfYear = dayOfYear - daysInYear(year);
    } else {
      resYear = year;
      resDayOfYear = dayOfYear;
    }

    return {
      year: resYear,
      dayOfYear: resDayOfYear
    };
  }

  function weekOfYear(mom, dow, doy) {
    var weekOffset = firstWeekOffset(mom.year(), dow, doy),
        week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
        resWeek,
        resYear;

    if (week < 1) {
      resYear = mom.year() - 1;
      resWeek = week + weeksInYear(resYear, dow, doy);
    } else if (week > weeksInYear(mom.year(), dow, doy)) {
      resWeek = week - weeksInYear(mom.year(), dow, doy);
      resYear = mom.year() + 1;
    } else {
      resYear = mom.year();
      resWeek = week;
    }

    return {
      week: resWeek,
      year: resYear
    };
  }

  function weeksInYear(year, dow, doy) {
    var weekOffset = firstWeekOffset(year, dow, doy),
        weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
    return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
  } // FORMATTING


  addFormatToken('w', ['ww', 2], 'wo', 'week');
  addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek'); // ALIASES

  addUnitAlias('week', 'w');
  addUnitAlias('isoWeek', 'W'); // PRIORITIES

  addUnitPriority('week', 5);
  addUnitPriority('isoWeek', 5); // PARSING

  addRegexToken('w', match1to2);
  addRegexToken('ww', match1to2, match2);
  addRegexToken('W', match1to2);
  addRegexToken('WW', match1to2, match2);
  addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
    week[token.substr(0, 1)] = toInt(input);
  }); // HELPERS
  // LOCALES

  function localeWeek(mom) {
    return weekOfYear(mom, this._week.dow, this._week.doy).week;
  }

  var defaultLocaleWeek = {
    dow: 0,
    // Sunday is the first day of the week.
    doy: 6 // The week that contains Jan 6th is the first week of the year.

  };

  function localeFirstDayOfWeek() {
    return this._week.dow;
  }

  function localeFirstDayOfYear() {
    return this._week.doy;
  } // MOMENTS


  function getSetWeek(input) {
    var week = this.localeData().week(this);
    return input == null ? week : this.add((input - week) * 7, 'd');
  }

  function getSetISOWeek(input) {
    var week = weekOfYear(this, 1, 4).week;
    return input == null ? week : this.add((input - week) * 7, 'd');
  } // FORMATTING


  addFormatToken('d', 0, 'do', 'day');
  addFormatToken('dd', 0, 0, function (format) {
    return this.localeData().weekdaysMin(this, format);
  });
  addFormatToken('ddd', 0, 0, function (format) {
    return this.localeData().weekdaysShort(this, format);
  });
  addFormatToken('dddd', 0, 0, function (format) {
    return this.localeData().weekdays(this, format);
  });
  addFormatToken('e', 0, 0, 'weekday');
  addFormatToken('E', 0, 0, 'isoWeekday'); // ALIASES

  addUnitAlias('day', 'd');
  addUnitAlias('weekday', 'e');
  addUnitAlias('isoWeekday', 'E'); // PRIORITY

  addUnitPriority('day', 11);
  addUnitPriority('weekday', 11);
  addUnitPriority('isoWeekday', 11); // PARSING

  addRegexToken('d', match1to2);
  addRegexToken('e', match1to2);
  addRegexToken('E', match1to2);
  addRegexToken('dd', function (isStrict, locale) {
    return locale.weekdaysMinRegex(isStrict);
  });
  addRegexToken('ddd', function (isStrict, locale) {
    return locale.weekdaysShortRegex(isStrict);
  });
  addRegexToken('dddd', function (isStrict, locale) {
    return locale.weekdaysRegex(isStrict);
  });
  addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
    var weekday = config._locale.weekdaysParse(input, token, config._strict); // if we didn't get a weekday name, mark the date as invalid


    if (weekday != null) {
      week.d = weekday;
    } else {
      getParsingFlags(config).invalidWeekday = input;
    }
  });
  addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
    week[token] = toInt(input);
  }); // HELPERS

  function parseWeekday(input, locale) {
    if (typeof input !== 'string') {
      return input;
    }

    if (!isNaN(input)) {
      return parseInt(input, 10);
    }

    input = locale.weekdaysParse(input);

    if (typeof input === 'number') {
      return input;
    }

    return null;
  }

  function parseIsoWeekday(input, locale) {
    if (typeof input === 'string') {
      return locale.weekdaysParse(input) % 7 || 7;
    }

    return isNaN(input) ? null : input;
  } // LOCALES


  function shiftWeekdays(ws, n) {
    return ws.slice(n, 7).concat(ws.slice(0, n));
  }

  var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');

  function localeWeekdays(m, format) {
    var weekdays = isArray(this._weekdays) ? this._weekdays : this._weekdays[m && m !== true && this._weekdays.isFormat.test(format) ? 'format' : 'standalone'];
    return m === true ? shiftWeekdays(weekdays, this._week.dow) : m ? weekdays[m.day()] : weekdays;
  }

  var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');

  function localeWeekdaysShort(m) {
    return m === true ? shiftWeekdays(this._weekdaysShort, this._week.dow) : m ? this._weekdaysShort[m.day()] : this._weekdaysShort;
  }

  var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');

  function localeWeekdaysMin(m) {
    return m === true ? shiftWeekdays(this._weekdaysMin, this._week.dow) : m ? this._weekdaysMin[m.day()] : this._weekdaysMin;
  }

  function handleStrictParse$1(weekdayName, format, strict) {
    var i,
        ii,
        mom,
        llc = weekdayName.toLocaleLowerCase();

    if (!this._weekdaysParse) {
      this._weekdaysParse = [];
      this._shortWeekdaysParse = [];
      this._minWeekdaysParse = [];

      for (i = 0; i < 7; ++i) {
        mom = createUTC([2000, 1]).day(i);
        this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
        this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
        this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
      }
    }

    if (strict) {
      if (format === 'dddd') {
        ii = indexOf.call(this._weekdaysParse, llc);
        return ii !== -1 ? ii : null;
      } else if (format === 'ddd') {
        ii = indexOf.call(this._shortWeekdaysParse, llc);
        return ii !== -1 ? ii : null;
      } else {
        ii = indexOf.call(this._minWeekdaysParse, llc);
        return ii !== -1 ? ii : null;
      }
    } else {
      if (format === 'dddd') {
        ii = indexOf.call(this._weekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._shortWeekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._minWeekdaysParse, llc);
        return ii !== -1 ? ii : null;
      } else if (format === 'ddd') {
        ii = indexOf.call(this._shortWeekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._weekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._minWeekdaysParse, llc);
        return ii !== -1 ? ii : null;
      } else {
        ii = indexOf.call(this._minWeekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._weekdaysParse, llc);

        if (ii !== -1) {
          return ii;
        }

        ii = indexOf.call(this._shortWeekdaysParse, llc);
        return ii !== -1 ? ii : null;
      }
    }
  }

  function localeWeekdaysParse(weekdayName, format, strict) {
    var i, mom, regex;

    if (this._weekdaysParseExact) {
      return handleStrictParse$1.call(this, weekdayName, format, strict);
    }

    if (!this._weekdaysParse) {
      this._weekdaysParse = [];
      this._minWeekdaysParse = [];
      this._shortWeekdaysParse = [];
      this._fullWeekdaysParse = [];
    }

    for (i = 0; i < 7; i++) {
      // make the regex if we don't have it already
      mom = createUTC([2000, 1]).day(i);

      if (strict && !this._fullWeekdaysParse[i]) {
        this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\\.?') + '$', 'i');
        this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\\.?') + '$', 'i');
        this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\\.?') + '$', 'i');
      }

      if (!this._weekdaysParse[i]) {
        regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
        this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
      } // test the regex


      if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
        return i;
      } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
        return i;
      } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
        return i;
      } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
        return i;
      }
    }
  } // MOMENTS


  function getSetDayOfWeek(input) {
    if (!this.isValid()) {
      return input != null ? this : NaN;
    }

    var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();

    if (input != null) {
      input = parseWeekday(input, this.localeData());
      return this.add(input - day, 'd');
    } else {
      return day;
    }
  }

  function getSetLocaleDayOfWeek(input) {
    if (!this.isValid()) {
      return input != null ? this : NaN;
    }

    var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
    return input == null ? weekday : this.add(input - weekday, 'd');
  }

  function getSetISODayOfWeek(input) {
    if (!this.isValid()) {
      return input != null ? this : NaN;
    } // behaves the same as moment#day except
    // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
    // as a setter, sunday should belong to the previous week.


    if (input != null) {
      var weekday = parseIsoWeekday(input, this.localeData());
      return this.day(this.day() % 7 ? weekday : weekday - 7);
    } else {
      return this.day() || 7;
    }
  }

  var defaultWeekdaysRegex = matchWord;

  function weekdaysRegex(isStrict) {
    if (this._weekdaysParseExact) {
      if (!hasOwnProp(this, '_weekdaysRegex')) {
        computeWeekdaysParse.call(this);
      }

      if (isStrict) {
        return this._weekdaysStrictRegex;
      } else {
        return this._weekdaysRegex;
      }
    } else {
      if (!hasOwnProp(this, '_weekdaysRegex')) {
        this._weekdaysRegex = defaultWeekdaysRegex;
      }

      return this._weekdaysStrictRegex && isStrict ? this._weekdaysStrictRegex : this._weekdaysRegex;
    }
  }

  var defaultWeekdaysShortRegex = matchWord;

  function weekdaysShortRegex(isStrict) {
    if (this._weekdaysParseExact) {
      if (!hasOwnProp(this, '_weekdaysRegex')) {
        computeWeekdaysParse.call(this);
      }

      if (isStrict) {
        return this._weekdaysShortStrictRegex;
      } else {
        return this._weekdaysShortRegex;
      }
    } else {
      if (!hasOwnProp(this, '_weekdaysShortRegex')) {
        this._weekdaysShortRegex = defaultWeekdaysShortRegex;
      }

      return this._weekdaysShortStrictRegex && isStrict ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
    }
  }

  var defaultWeekdaysMinRegex = matchWord;

  function weekdaysMinRegex(isStrict) {
    if (this._weekdaysParseExact) {
      if (!hasOwnProp(this, '_weekdaysRegex')) {
        computeWeekdaysParse.call(this);
      }

      if (isStrict) {
        return this._weekdaysMinStrictRegex;
      } else {
        return this._weekdaysMinRegex;
      }
    } else {
      if (!hasOwnProp(this, '_weekdaysMinRegex')) {
        this._weekdaysMinRegex = defaultWeekdaysMinRegex;
      }

      return this._weekdaysMinStrictRegex && isStrict ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
    }
  }

  function computeWeekdaysParse() {
    function cmpLenRev(a, b) {
      return b.length - a.length;
    }

    var minPieces = [],
        shortPieces = [],
        longPieces = [],
        mixedPieces = [],
        i,
        mom,
        minp,
        shortp,
        longp;

    for (i = 0; i < 7; i++) {
      // make the regex if we don't have it already
      mom = createUTC([2000, 1]).day(i);
      minp = this.weekdaysMin(mom, '');
      shortp = this.weekdaysShort(mom, '');
      longp = this.weekdays(mom, '');
      minPieces.push(minp);
      shortPieces.push(shortp);
      longPieces.push(longp);
      mixedPieces.push(minp);
      mixedPieces.push(shortp);
      mixedPieces.push(longp);
    } // Sorting makes sure if one weekday (or abbr) is a prefix of another it
    // will match the longer piece.


    minPieces.sort(cmpLenRev);
    shortPieces.sort(cmpLenRev);
    longPieces.sort(cmpLenRev);
    mixedPieces.sort(cmpLenRev);

    for (i = 0; i < 7; i++) {
      shortPieces[i] = regexEscape(shortPieces[i]);
      longPieces[i] = regexEscape(longPieces[i]);
      mixedPieces[i] = regexEscape(mixedPieces[i]);
    }

    this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
    this._weekdaysShortRegex = this._weekdaysRegex;
    this._weekdaysMinRegex = this._weekdaysRegex;
    this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
    this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
  } // FORMATTING


  function hFormat() {
    return this.hours() % 12 || 12;
  }

  function kFormat() {
    return this.hours() || 24;
  }

  addFormatToken('H', ['HH', 2], 0, 'hour');
  addFormatToken('h', ['hh', 2], 0, hFormat);
  addFormatToken('k', ['kk', 2], 0, kFormat);
  addFormatToken('hmm', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
  });
  addFormatToken('hmmss', 0, 0, function () {
    return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
  });
  addFormatToken('Hmm', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2);
  });
  addFormatToken('Hmmss', 0, 0, function () {
    return '' + this.hours() + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
  });

  function meridiem(token, lowercase) {
    addFormatToken(token, 0, 0, function () {
      return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
    });
  }

  meridiem('a', true);
  meridiem('A', false); // ALIASES

  addUnitAlias('hour', 'h'); // PRIORITY

  addUnitPriority('hour', 13); // PARSING

  function matchMeridiem(isStrict, locale) {
    return locale._meridiemParse;
  }

  addRegexToken('a', matchMeridiem);
  addRegexToken('A', matchMeridiem);
  addRegexToken('H', match1to2);
  addRegexToken('h', match1to2);
  addRegexToken('k', match1to2);
  addRegexToken('HH', match1to2, match2);
  addRegexToken('hh', match1to2, match2);
  addRegexToken('kk', match1to2, match2);
  addRegexToken('hmm', match3to4);
  addRegexToken('hmmss', match5to6);
  addRegexToken('Hmm', match3to4);
  addRegexToken('Hmmss', match5to6);
  addParseToken(['H', 'HH'], HOUR);
  addParseToken(['k', 'kk'], function (input, array, config) {
    var kInput = toInt(input);
    array[HOUR] = kInput === 24 ? 0 : kInput;
  });
  addParseToken(['a', 'A'], function (input, array, config) {
    config._isPm = config._locale.isPM(input);
    config._meridiem = input;
  });
  addParseToken(['h', 'hh'], function (input, array, config) {
    array[HOUR] = toInt(input);
    getParsingFlags(config).bigHour = true;
  });
  addParseToken('hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
    getParsingFlags(config).bigHour = true;
  });
  addParseToken('hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
    getParsingFlags(config).bigHour = true;
  });
  addParseToken('Hmm', function (input, array, config) {
    var pos = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos));
    array[MINUTE] = toInt(input.substr(pos));
  });
  addParseToken('Hmmss', function (input, array, config) {
    var pos1 = input.length - 4;
    var pos2 = input.length - 2;
    array[HOUR] = toInt(input.substr(0, pos1));
    array[MINUTE] = toInt(input.substr(pos1, 2));
    array[SECOND] = toInt(input.substr(pos2));
  }); // LOCALES

  function localeIsPM(input) {
    // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
    // Using charAt should be more compatible.
    return (input + '').toLowerCase().charAt(0) === 'p';
  }

  var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;

  function localeMeridiem(hours, minutes, isLower) {
    if (hours > 11) {
      return isLower ? 'pm' : 'PM';
    } else {
      return isLower ? 'am' : 'AM';
    }
  } // MOMENTS
  // Setting the hour should keep the time, because the user explicitly
  // specified which hour they want. So trying to maintain the same hour (in
  // a new timezone) makes sense. Adding/subtracting hours does not follow
  // this rule.


  var getSetHour = makeGetSet('Hours', true);
  var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
    relativeTime: defaultRelativeTime,
    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,
    week: defaultLocaleWeek,
    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,
    meridiemParse: defaultLocaleMeridiemParse
  }; // internal storage for locale config files

  var locales = {};
  var localeFamilies = {};
  var globalLocale;

  function normalizeLocale(key) {
    return key ? key.toLowerCase().replace('_', '-') : key;
  } // pick the locale from the array
  // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
  // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root


  function chooseLocale(names) {
    var i = 0,
        j,
        next,
        locale,
        split;

    while (i < names.length) {
      split = normalizeLocale(names[i]).split('-');
      j = split.length;
      next = normalizeLocale(names[i + 1]);
      next = next ? next.split('-') : null;

      while (j > 0) {
        locale = loadLocale(split.slice(0, j).join('-'));

        if (locale) {
          return locale;
        }

        if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
          //the next array item is better than a shallower substring of this one
          break;
        }

        j--;
      }

      i++;
    }

    return globalLocale;
  }

  function loadLocale(name) {
    var oldLocale = null; // TODO: Find a better way to register and load all the locales in Node

    if (!locales[name] && typeof module !== 'undefined' && module && module.exports) {
      try {
        oldLocale = globalLocale._abbr;
        var aliasedRequire = require;
        aliasedRequire('./locale/' + name);
        getSetGlobalLocale(oldLocale);
      } catch (e) {}
    }

    return locales[name];
  } // This function will load locale and then set the global locale.  If
  // no arguments are passed in, it will simply return the current global
  // locale key.


  function getSetGlobalLocale(key, values) {
    var data;

    if (key) {
      if (isUndefined(values)) {
        data = getLocale(key);
      } else {
        data = defineLocale(key, values);
      }

      if (data) {
        // moment.duration._locale = moment._locale = data;
        globalLocale = data;
      } else {
        if (typeof console !== 'undefined' && console.warn) {
          //warn user if arguments are passed but the locale could not be set
          console.warn('Locale ' + key + ' not found. Did you forget to load it?');
        }
      }
    }

    return globalLocale._abbr;
  }

  function defineLocale(name, config) {
    if (config !== null) {
      var locale,
          parentConfig = baseConfig;
      config.abbr = name;

      if (locales[name] != null) {
        deprecateSimple('defineLocaleOverride', 'use moment.updateLocale(localeName, config) to change ' + 'an existing locale. moment.defineLocale(localeName, ' + 'config) should only be used for creating a new locale ' + 'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
        parentConfig = locales[name]._config;
      } else if (config.parentLocale != null) {
        if (locales[config.parentLocale] != null) {
          parentConfig = locales[config.parentLocale]._config;
        } else {
          locale = loadLocale(config.parentLocale);

          if (locale != null) {
            parentConfig = locale._config;
          } else {
            if (!localeFamilies[config.parentLocale]) {
              localeFamilies[config.parentLocale] = [];
            }

            localeFamilies[config.parentLocale].push({
              name: name,
              config: config
            });
            return null;
          }
        }
      }

      locales[name] = new Locale(mergeConfigs(parentConfig, config));

      if (localeFamilies[name]) {
        localeFamilies[name].forEach(function (x) {
          defineLocale(x.name, x.config);
        });
      } // backwards compat for now: also set the locale
      // make sure we set the locale AFTER all child locales have been
      // created, so we won't end up with the child locale set.


      getSetGlobalLocale(name);
      return locales[name];
    } else {
      // useful for testing
      delete locales[name];
      return null;
    }
  }

  function updateLocale(name, config) {
    if (config != null) {
      var locale,
          tmpLocale,
          parentConfig = baseConfig; // MERGE

      tmpLocale = loadLocale(name);

      if (tmpLocale != null) {
        parentConfig = tmpLocale._config;
      }

      config = mergeConfigs(parentConfig, config);
      locale = new Locale(config);
      locale.parentLocale = locales[name];
      locales[name] = locale; // backwards compat for now: also set the locale

      getSetGlobalLocale(name);
    } else {
      // pass null for config to unupdate, useful for tests
      if (locales[name] != null) {
        if (locales[name].parentLocale != null) {
          locales[name] = locales[name].parentLocale;
        } else if (locales[name] != null) {
          delete locales[name];
        }
      }
    }

    return locales[name];
  } // returns locale data


  function getLocale(key) {
    var locale;

    if (key && key._locale && key._locale._abbr) {
      key = key._locale._abbr;
    }

    if (!key) {
      return globalLocale;
    }

    if (!isArray(key)) {
      //short-circuit everything else
      locale = loadLocale(key);

      if (locale) {
        return locale;
      }

      key = [key];
    }

    return chooseLocale(key);
  }

  function listLocales() {
    return keys(locales);
  }

  function checkOverflow(m) {
    var overflow;
    var a = m._a;

    if (a && getParsingFlags(m).overflow === -2) {
      overflow = a[MONTH] < 0 || a[MONTH] > 11 ? MONTH : a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH]) ? DATE : a[HOUR] < 0 || a[HOUR] > 24 || a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0) ? HOUR : a[MINUTE] < 0 || a[MINUTE] > 59 ? MINUTE : a[SECOND] < 0 || a[SECOND] > 59 ? SECOND : a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND : -1;

      if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
        overflow = DATE;
      }

      if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
        overflow = WEEK;
      }

      if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
        overflow = WEEKDAY;
      }

      getParsingFlags(m).overflow = overflow;
    }

    return m;
  } // Pick the first defined of two or three arguments.


  function defaults(a, b, c) {
    if (a != null) {
      return a;
    }

    if (b != null) {
      return b;
    }

    return c;
  }

  function currentDateArray(config) {
    // hooks is actually the exported moment object
    var nowValue = new Date(hooks.now());

    if (config._useUTC) {
      return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
    }

    return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
  } // convert an array to a date.
  // the array should mirror the parameters below
  // note: all values past the year are optional and will default to the lowest possible value.
  // [year, month, day , hour, minute, second, millisecond]


  function configFromArray(config) {
    var i,
        date,
        input = [],
        currentDate,
        expectedWeekday,
        yearToUse;

    if (config._d) {
      return;
    }

    currentDate = currentDateArray(config); //compute day of the year from weeks and weekdays

    if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
      dayOfYearFromWeekInfo(config);
    } //if the day of the year is set, figure out what it is


    if (config._dayOfYear != null) {
      yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

      if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
        getParsingFlags(config)._overflowDayOfYear = true;
      }

      date = createUTCDate(yearToUse, 0, config._dayOfYear);
      config._a[MONTH] = date.getUTCMonth();
      config._a[DATE] = date.getUTCDate();
    } // Default to current date.
    // * if no year, month, day of month are given, default to today
    // * if day of month is given, default month and year
    // * if month is given, default only year
    // * if year is given, don't default anything


    for (i = 0; i < 3 && config._a[i] == null; ++i) {
      config._a[i] = input[i] = currentDate[i];
    } // Zero out whatever was not defaulted, including time


    for (; i < 7; i++) {
      config._a[i] = input[i] = config._a[i] == null ? i === 2 ? 1 : 0 : config._a[i];
    } // Check for 24:00:00.000


    if (config._a[HOUR] === 24 && config._a[MINUTE] === 0 && config._a[SECOND] === 0 && config._a[MILLISECOND] === 0) {
      config._nextDay = true;
      config._a[HOUR] = 0;
    }

    config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
    expectedWeekday = config._useUTC ? config._d.getUTCDay() : config._d.getDay(); // Apply timezone offset from input. The actual utcOffset can be changed
    // with parseZone.

    if (config._tzm != null) {
      config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
    }

    if (config._nextDay) {
      config._a[HOUR] = 24;
    } // check for mismatching day of week


    if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== expectedWeekday) {
      getParsingFlags(config).weekdayMismatch = true;
    }
  }

  function dayOfYearFromWeekInfo(config) {
    var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;
    w = config._w;

    if (w.GG != null || w.W != null || w.E != null) {
      dow = 1;
      doy = 4; // TODO: We need to take the current isoWeekYear, but that depends on
      // how we interpret now (local, utc, fixed offset). So create
      // a now version of current config (take local/utc/offset flags, and
      // create now).

      weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
      week = defaults(w.W, 1);
      weekday = defaults(w.E, 1);

      if (weekday < 1 || weekday > 7) {
        weekdayOverflow = true;
      }
    } else {
      dow = config._locale._week.dow;
      doy = config._locale._week.doy;
      var curWeek = weekOfYear(createLocal(), dow, doy);
      weekYear = defaults(w.gg, config._a[YEAR], curWeek.year); // Default to current week.

      week = defaults(w.w, curWeek.week);

      if (w.d != null) {
        // weekday -- low day numbers are considered next week
        weekday = w.d;

        if (weekday < 0 || weekday > 6) {
          weekdayOverflow = true;
        }
      } else if (w.e != null) {
        // local weekday -- counting starts from beginning of week
        weekday = w.e + dow;

        if (w.e < 0 || w.e > 6) {
          weekdayOverflow = true;
        }
      } else {
        // default to beginning of week
        weekday = dow;
      }
    }

    if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
      getParsingFlags(config)._overflowWeeks = true;
    } else if (weekdayOverflow != null) {
      getParsingFlags(config)._overflowWeekday = true;
    } else {
      temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
      config._a[YEAR] = temp.year;
      config._dayOfYear = temp.dayOfYear;
    }
  } // iso 8601 regex
  // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)


  var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
  var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
  var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;
  var isoDates = [['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/], ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/], ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/], ['GGGG-[W]WW', /\d{4}-W\d\d/, false], ['YYYY-DDD', /\d{4}-\d{3}/], ['YYYY-MM', /\d{4}-\d\d/, false], ['YYYYYYMMDD', /[+-]\d{10}/], ['YYYYMMDD', /\d{8}/], // YYYYMM is NOT allowed by the standard
  ['GGGG[W]WWE', /\d{4}W\d{3}/], ['GGGG[W]WW', /\d{4}W\d{2}/, false], ['YYYYDDD', /\d{7}/]]; // iso time formats and regexes

  var isoTimes = [['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/], ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/], ['HH:mm:ss', /\d\d:\d\d:\d\d/], ['HH:mm', /\d\d:\d\d/], ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/], ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/], ['HHmmss', /\d\d\d\d\d\d/], ['HHmm', /\d\d\d\d/], ['HH', /\d\d/]];
  var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i; // date from iso format

  function configFromISO(config) {
    var i,
        l,
        string = config._i,
        match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
        allowTime,
        dateFormat,
        timeFormat,
        tzFormat;

    if (match) {
      getParsingFlags(config).iso = true;

      for (i = 0, l = isoDates.length; i < l; i++) {
        if (isoDates[i][1].exec(match[1])) {
          dateFormat = isoDates[i][0];
          allowTime = isoDates[i][2] !== false;
          break;
        }
      }

      if (dateFormat == null) {
        config._isValid = false;
        return;
      }

      if (match[3]) {
        for (i = 0, l = isoTimes.length; i < l; i++) {
          if (isoTimes[i][1].exec(match[3])) {
            // match[2] should be 'T' or space
            timeFormat = (match[2] || ' ') + isoTimes[i][0];
            break;
          }
        }

        if (timeFormat == null) {
          config._isValid = false;
          return;
        }
      }

      if (!allowTime && timeFormat != null) {
        config._isValid = false;
        return;
      }

      if (match[4]) {
        if (tzRegex.exec(match[4])) {
          tzFormat = 'Z';
        } else {
          config._isValid = false;
          return;
        }
      }

      config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
      configFromStringAndFormat(config);
    } else {
      config._isValid = false;
    }
  } // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3


  var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

  function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
    var result = [untruncateYear(yearStr), defaultLocaleMonthsShort.indexOf(monthStr), parseInt(dayStr, 10), parseInt(hourStr, 10), parseInt(minuteStr, 10)];

    if (secondStr) {
      result.push(parseInt(secondStr, 10));
    }

    return result;
  }

  function untruncateYear(yearStr) {
    var year = parseInt(yearStr, 10);

    if (year <= 49) {
      return 2000 + year;
    } else if (year <= 999) {
      return 1900 + year;
    }

    return year;
  }

  function preprocessRFC2822(s) {
    // Remove comments and folding whitespace and replace multiple-spaces with a single space
    return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '');
  }

  function checkWeekday(weekdayStr, parsedInput, config) {
    if (weekdayStr) {
      // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
      var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
          weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();

      if (weekdayProvided !== weekdayActual) {
        getParsingFlags(config).weekdayMismatch = true;
        config._isValid = false;
        return false;
      }
    }

    return true;
  }

  var obsOffsets = {
    UT: 0,
    GMT: 0,
    EDT: -4 * 60,
    EST: -5 * 60,
    CDT: -5 * 60,
    CST: -6 * 60,
    MDT: -6 * 60,
    MST: -7 * 60,
    PDT: -7 * 60,
    PST: -8 * 60
  };

  function calculateOffset(obsOffset, militaryOffset, numOffset) {
    if (obsOffset) {
      return obsOffsets[obsOffset];
    } else if (militaryOffset) {
      // the only allowed military tz is Z
      return 0;
    } else {
      var hm = parseInt(numOffset, 10);
      var m = hm % 100,
          h = (hm - m) / 100;
      return h * 60 + m;
    }
  } // date and time from ref 2822 format


  function configFromRFC2822(config) {
    var match = rfc2822.exec(preprocessRFC2822(config._i));

    if (match) {
      var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);

      if (!checkWeekday(match[1], parsedArray, config)) {
        return;
      }

      config._a = parsedArray;
      config._tzm = calculateOffset(match[8], match[9], match[10]);
      config._d = createUTCDate.apply(null, config._a);

      config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

      getParsingFlags(config).rfc2822 = true;
    } else {
      config._isValid = false;
    }
  } // date from iso format or fallback


  function configFromString(config) {
    var matched = aspNetJsonRegex.exec(config._i);

    if (matched !== null) {
      config._d = new Date(+matched[1]);
      return;
    }

    configFromISO(config);

    if (config._isValid === false) {
      delete config._isValid;
    } else {
      return;
    }

    configFromRFC2822(config);

    if (config._isValid === false) {
      delete config._isValid;
    } else {
      return;
    } // Final attempt, use Input Fallback


    hooks.createFromInputFallback(config);
  }

  hooks.createFromInputFallback = deprecate('value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' + 'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' + 'discouraged and will be removed in an upcoming major release. Please refer to ' + 'http://momentjs.com/guides/#/warnings/js-date/ for more info.', function (config) {
    config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
  }); // constant that refers to the ISO standard

  hooks.ISO_8601 = function () {}; // constant that refers to the RFC 2822 form


  hooks.RFC_2822 = function () {}; // date from string and format string


  function configFromStringAndFormat(config) {
    // TODO: Move this to another part of the creation flow to prevent circular deps
    if (config._f === hooks.ISO_8601) {
      configFromISO(config);
      return;
    }

    if (config._f === hooks.RFC_2822) {
      configFromRFC2822(config);
      return;
    }

    config._a = [];
    getParsingFlags(config).empty = true; // This array is used to make a Date, either with `new Date` or `Date.UTC`

    var string = '' + config._i,
        i,
        parsedInput,
        tokens,
        token,
        skipped,
        stringLength = string.length,
        totalParsedInputLength = 0;
    tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

    for (i = 0; i < tokens.length; i++) {
      token = tokens[i];
      parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0]; // console.log('token', token, 'parsedInput', parsedInput,
      //         'regex', getParseRegexForToken(token, config));

      if (parsedInput) {
        skipped = string.substr(0, string.indexOf(parsedInput));

        if (skipped.length > 0) {
          getParsingFlags(config).unusedInput.push(skipped);
        }

        string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
        totalParsedInputLength += parsedInput.length;
      } // don't parse if it's not a known token


      if (formatTokenFunctions[token]) {
        if (parsedInput) {
          getParsingFlags(config).empty = false;
        } else {
          getParsingFlags(config).unusedTokens.push(token);
        }

        addTimeToArrayFromToken(token, parsedInput, config);
      } else if (config._strict && !parsedInput) {
        getParsingFlags(config).unusedTokens.push(token);
      }
    } // add remaining unparsed input length to the string


    getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;

    if (string.length > 0) {
      getParsingFlags(config).unusedInput.push(string);
    } // clear _12h flag if hour is <= 12


    if (config._a[HOUR] <= 12 && getParsingFlags(config).bigHour === true && config._a[HOUR] > 0) {
      getParsingFlags(config).bigHour = undefined;
    }

    getParsingFlags(config).parsedDateParts = config._a.slice(0);
    getParsingFlags(config).meridiem = config._meridiem; // handle meridiem

    config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);
    configFromArray(config);
    checkOverflow(config);
  }

  function meridiemFixWrap(locale, hour, meridiem) {
    var isPm;

    if (meridiem == null) {
      // nothing to do
      return hour;
    }

    if (locale.meridiemHour != null) {
      return locale.meridiemHour(hour, meridiem);
    } else if (locale.isPM != null) {
      // Fallback
      isPm = locale.isPM(meridiem);

      if (isPm && hour < 12) {
        hour += 12;
      }

      if (!isPm && hour === 12) {
        hour = 0;
      }

      return hour;
    } else {
      // this is not supposed to happen
      return hour;
    }
  } // date from string and array of format strings


  function configFromStringAndArray(config) {
    var tempConfig, bestMoment, scoreToBeat, i, currentScore;

    if (config._f.length === 0) {
      getParsingFlags(config).invalidFormat = true;
      config._d = new Date(NaN);
      return;
    }

    for (i = 0; i < config._f.length; i++) {
      currentScore = 0;
      tempConfig = copyConfig({}, config);

      if (config._useUTC != null) {
        tempConfig._useUTC = config._useUTC;
      }

      tempConfig._f = config._f[i];
      configFromStringAndFormat(tempConfig);

      if (!isValid(tempConfig)) {
        continue;
      } // if there is any input that was not parsed add a penalty for that format


      currentScore += getParsingFlags(tempConfig).charsLeftOver; //or tokens

      currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;
      getParsingFlags(tempConfig).score = currentScore;

      if (scoreToBeat == null || currentScore < scoreToBeat) {
        scoreToBeat = currentScore;
        bestMoment = tempConfig;
      }
    }

    extend(config, bestMoment || tempConfig);
  }

  function configFromObject(config) {
    if (config._d) {
      return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
      return obj && parseInt(obj, 10);
    });
    configFromArray(config);
  }

  function createFromConfig(config) {
    var res = new Moment(checkOverflow(prepareConfig(config)));

    if (res._nextDay) {
      // Adding is smart enough around DST
      res.add(1, 'd');
      res._nextDay = undefined;
    }

    return res;
  }

  function prepareConfig(config) {
    var input = config._i,
        format = config._f;
    config._locale = config._locale || getLocale(config._l);

    if (input === null || format === undefined && input === '') {
      return createInvalid({
        nullInput: true
      });
    }

    if (typeof input === 'string') {
      config._i = input = config._locale.preparse(input);
    }

    if (isMoment(input)) {
      return new Moment(checkOverflow(input));
    } else if (isDate(input)) {
      config._d = input;
    } else if (isArray(format)) {
      configFromStringAndArray(config);
    } else if (format) {
      configFromStringAndFormat(config);
    } else {
      configFromInput(config);
    }

    if (!isValid(config)) {
      config._d = null;
    }

    return config;
  }

  function configFromInput(config) {
    var input = config._i;

    if (isUndefined(input)) {
      config._d = new Date(hooks.now());
    } else if (isDate(input)) {
      config._d = new Date(input.valueOf());
    } else if (typeof input === 'string') {
      configFromString(config);
    } else if (isArray(input)) {
      config._a = map(input.slice(0), function (obj) {
        return parseInt(obj, 10);
      });
      configFromArray(config);
    } else if (isObject(input)) {
      configFromObject(config);
    } else if (isNumber(input)) {
      // from milliseconds
      config._d = new Date(input);
    } else {
      hooks.createFromInputFallback(config);
    }
  }

  function createLocalOrUTC(input, format, locale, strict, isUTC) {
    var c = {};

    if (locale === true || locale === false) {
      strict = locale;
      locale = undefined;
    }

    if (isObject(input) && isObjectEmpty(input) || isArray(input) && input.length === 0) {
      input = undefined;
    } // object construction must be done this way.
    // https://github.com/moment/moment/issues/1423


    c._isAMomentObject = true;
    c._useUTC = c._isUTC = isUTC;
    c._l = locale;
    c._i = input;
    c._f = format;
    c._strict = strict;
    return createFromConfig(c);
  }

  function createLocal(input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
  }

  var prototypeMin = deprecate('moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
    var other = createLocal.apply(null, arguments);

    if (this.isValid() && other.isValid()) {
      return other < this ? this : other;
    } else {
      return createInvalid();
    }
  });
  var prototypeMax = deprecate('moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
    var other = createLocal.apply(null, arguments);

    if (this.isValid() && other.isValid()) {
      return other > this ? this : other;
    } else {
      return createInvalid();
    }
  }); // Pick a moment m from moments so that m[fn](other) is true for all
  // other. This relies on the function fn to be transitive.
  //
  // moments should either be an array of moment objects or an array, whose
  // first element is an array of moment objects.

  function pickBy(fn, moments) {
    var res, i;

    if (moments.length === 1 && isArray(moments[0])) {
      moments = moments[0];
    }

    if (!moments.length) {
      return createLocal();
    }

    res = moments[0];

    for (i = 1; i < moments.length; ++i) {
      if (!moments[i].isValid() || moments[i][fn](res)) {
        res = moments[i];
      }
    }

    return res;
  } // TODO: Use [].sort instead?


  function min() {
    var args = [].slice.call(arguments, 0);
    return pickBy('isBefore', args);
  }

  function max() {
    var args = [].slice.call(arguments, 0);
    return pickBy('isAfter', args);
  }

  var now = function () {
    return Date.now ? Date.now() : +new Date();
  };

  var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

  function isDurationValid(m) {
    for (var key in m) {
      if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
        return false;
      }
    }

    var unitHasDecimal = false;

    for (var i = 0; i < ordering.length; ++i) {
      if (m[ordering[i]]) {
        if (unitHasDecimal) {
          return false; // only allow non-integers for smallest unit
        }

        if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
          unitHasDecimal = true;
        }
      }
    }

    return true;
  }

  function isValid$1() {
    return this._isValid;
  }

  function createInvalid$1() {
    return createDuration(NaN);
  }

  function Duration(duration) {
    var normalizedInput = normalizeObjectUnits(duration),
        years = normalizedInput.year || 0,
        quarters = normalizedInput.quarter || 0,
        months = normalizedInput.month || 0,
        weeks = normalizedInput.week || normalizedInput.isoWeek || 0,
        days = normalizedInput.day || 0,
        hours = normalizedInput.hour || 0,
        minutes = normalizedInput.minute || 0,
        seconds = normalizedInput.second || 0,
        milliseconds = normalizedInput.millisecond || 0;
    this._isValid = isDurationValid(normalizedInput); // representation for dateAddRemove

    this._milliseconds = +milliseconds + seconds * 1e3 + // 1000
    minutes * 6e4 + // 1000 * 60
    hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
    // Because of dateAddRemove treats 24 hours as different from a
    // day when working around DST, we need to store them separately

    this._days = +days + weeks * 7; // It is impossible to translate months into days without knowing
    // which months you are are talking about, so we have to store
    // it separately.

    this._months = +months + quarters * 3 + years * 12;
    this._data = {};
    this._locale = getLocale();

    this._bubble();
  }

  function isDuration(obj) {
    return obj instanceof Duration;
  }

  function absRound(number) {
    if (number < 0) {
      return Math.round(-1 * number) * -1;
    } else {
      return Math.round(number);
    }
  } // FORMATTING


  function offset(token, separator) {
    addFormatToken(token, 0, 0, function () {
      var offset = this.utcOffset();
      var sign = '+';

      if (offset < 0) {
        offset = -offset;
        sign = '-';
      }

      return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~offset % 60, 2);
    });
  }

  offset('Z', ':');
  offset('ZZ', ''); // PARSING

  addRegexToken('Z', matchShortOffset);
  addRegexToken('ZZ', matchShortOffset);
  addParseToken(['Z', 'ZZ'], function (input, array, config) {
    config._useUTC = true;
    config._tzm = offsetFromString(matchShortOffset, input);
  }); // HELPERS
  // timezone chunker
  // '+10:00' > ['10',  '00']
  // '-1530'  > ['-15', '30']

  var chunkOffset = /([\+\-]|\d\d)/gi;

  function offsetFromString(matcher, string) {
    var matches = (string || '').match(matcher);

    if (matches === null) {
      return null;
    }

    var chunk = matches[matches.length - 1] || [];
    var parts = (chunk + '').match(chunkOffset) || ['-', 0, 0];
    var minutes = +(parts[1] * 60) + toInt(parts[2]);
    return minutes === 0 ? 0 : parts[0] === '+' ? minutes : -minutes;
  } // Return a moment from input, that is local/utc/zone equivalent to model.


  function cloneWithOffset(input, model) {
    var res, diff;

    if (model._isUTC) {
      res = model.clone();
      diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf(); // Use low-level api, because this fn is low-level api.

      res._d.setTime(res._d.valueOf() + diff);

      hooks.updateOffset(res, false);
      return res;
    } else {
      return createLocal(input).local();
    }
  }

  function getDateOffset(m) {
    // On Firefox.24 Date#getTimezoneOffset returns a floating point.
    // https://github.com/moment/moment/pull/1871
    return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
  } // HOOKS
  // This function will be called whenever a moment is mutated.
  // It is intended to keep the offset in sync with the timezone.


  hooks.updateOffset = function () {}; // MOMENTS
  // keepLocalTime = true means only change the timezone, without
  // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
  // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
  // +0200, so we adjust the time as needed, to be valid.
  //
  // Keeping the time actually adds/subtracts (one hour)
  // from the actual represented time. That is why we call updateOffset
  // a second time. In case it wants us to change the offset again
  // _changeInProgress == true case, then we have to adjust, because
  // there is no such time in the given timezone.


  function getSetOffset(input, keepLocalTime, keepMinutes) {
    var offset = this._offset || 0,
        localAdjust;

    if (!this.isValid()) {
      return input != null ? this : NaN;
    }

    if (input != null) {
      if (typeof input === 'string') {
        input = offsetFromString(matchShortOffset, input);

        if (input === null) {
          return this;
        }
      } else if (Math.abs(input) < 16 && !keepMinutes) {
        input = input * 60;
      }

      if (!this._isUTC && keepLocalTime) {
        localAdjust = getDateOffset(this);
      }

      this._offset = input;
      this._isUTC = true;

      if (localAdjust != null) {
        this.add(localAdjust, 'm');
      }

      if (offset !== input) {
        if (!keepLocalTime || this._changeInProgress) {
          addSubtract(this, createDuration(input - offset, 'm'), 1, false);
        } else if (!this._changeInProgress) {
          this._changeInProgress = true;
          hooks.updateOffset(this, true);
          this._changeInProgress = null;
        }
      }

      return this;
    } else {
      return this._isUTC ? offset : getDateOffset(this);
    }
  }

  function getSetZone(input, keepLocalTime) {
    if (input != null) {
      if (typeof input !== 'string') {
        input = -input;
      }

      this.utcOffset(input, keepLocalTime);
      return this;
    } else {
      return -this.utcOffset();
    }
  }

  function setOffsetToUTC(keepLocalTime) {
    return this.utcOffset(0, keepLocalTime);
  }

  function setOffsetToLocal(keepLocalTime) {
    if (this._isUTC) {
      this.utcOffset(0, keepLocalTime);
      this._isUTC = false;

      if (keepLocalTime) {
        this.subtract(getDateOffset(this), 'm');
      }
    }

    return this;
  }

  function setOffsetToParsedOffset() {
    if (this._tzm != null) {
      this.utcOffset(this._tzm, false, true);
    } else if (typeof this._i === 'string') {
      var tZone = offsetFromString(matchOffset, this._i);

      if (tZone != null) {
        this.utcOffset(tZone);
      } else {
        this.utcOffset(0, true);
      }
    }

    return this;
  }

  function hasAlignedHourOffset(input) {
    if (!this.isValid()) {
      return false;
    }

    input = input ? createLocal(input).utcOffset() : 0;
    return (this.utcOffset() - input) % 60 === 0;
  }

  function isDaylightSavingTime() {
    return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset();
  }

  function isDaylightSavingTimeShifted() {
    if (!isUndefined(this._isDSTShifted)) {
      return this._isDSTShifted;
    }

    var c = {};
    copyConfig(c, this);
    c = prepareConfig(c);

    if (c._a) {
      var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
      this._isDSTShifted = this.isValid() && compareArrays(c._a, other.toArray()) > 0;
    } else {
      this._isDSTShifted = false;
    }

    return this._isDSTShifted;
  }

  function isLocal() {
    return this.isValid() ? !this._isUTC : false;
  }

  function isUtcOffset() {
    return this.isValid() ? this._isUTC : false;
  }

  function isUtc() {
    return this.isValid() ? this._isUTC && this._offset === 0 : false;
  } // ASP.NET json date format regex


  var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/; // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
  // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
  // and further modified to allow for strings containing both week and day

  var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

  function createDuration(input, key) {
    var duration = input,
        // matching against regexp is expensive, do it on demand
    match = null,
        sign,
        ret,
        diffRes;

    if (isDuration(input)) {
      duration = {
        ms: input._milliseconds,
        d: input._days,
        M: input._months
      };
    } else if (isNumber(input)) {
      duration = {};

      if (key) {
        duration[key] = input;
      } else {
        duration.milliseconds = input;
      }
    } else if (!!(match = aspNetRegex.exec(input))) {
      sign = match[1] === '-' ? -1 : 1;
      duration = {
        y: 0,
        d: toInt(match[DATE]) * sign,
        h: toInt(match[HOUR]) * sign,
        m: toInt(match[MINUTE]) * sign,
        s: toInt(match[SECOND]) * sign,
        ms: toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match

      };
    } else if (!!(match = isoRegex.exec(input))) {
      sign = match[1] === '-' ? -1 : 1;
      duration = {
        y: parseIso(match[2], sign),
        M: parseIso(match[3], sign),
        w: parseIso(match[4], sign),
        d: parseIso(match[5], sign),
        h: parseIso(match[6], sign),
        m: parseIso(match[7], sign),
        s: parseIso(match[8], sign)
      };
    } else if (duration == null) {
      // checks for null or undefined
      duration = {};
    } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
      diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));
      duration = {};
      duration.ms = diffRes.milliseconds;
      duration.M = diffRes.months;
    }

    ret = new Duration(duration);

    if (isDuration(input) && hasOwnProp(input, '_locale')) {
      ret._locale = input._locale;
    }

    return ret;
  }

  createDuration.fn = Duration.prototype;
  createDuration.invalid = createInvalid$1;

  function parseIso(inp, sign) {
    // We'd normally use ~~inp for this, but unfortunately it also
    // converts floats to ints.
    // inp may be undefined, so careful calling replace on it.
    var res = inp && parseFloat(inp.replace(',', '.')); // apply sign while we're at it

    return (isNaN(res) ? 0 : res) * sign;
  }

  function positiveMomentsDifference(base, other) {
    var res = {};
    res.months = other.month() - base.month() + (other.year() - base.year()) * 12;

    if (base.clone().add(res.months, 'M').isAfter(other)) {
      --res.months;
    }

    res.milliseconds = +other - +base.clone().add(res.months, 'M');
    return res;
  }

  function momentsDifference(base, other) {
    var res;

    if (!(base.isValid() && other.isValid())) {
      return {
        milliseconds: 0,
        months: 0
      };
    }

    other = cloneWithOffset(other, base);

    if (base.isBefore(other)) {
      res = positiveMomentsDifference(base, other);
    } else {
      res = positiveMomentsDifference(other, base);
      res.milliseconds = -res.milliseconds;
      res.months = -res.months;
    }

    return res;
  } // TODO: remove 'name' arg after deprecation is removed


  function createAdder(direction, name) {
    return function (val, period) {
      var dur, tmp; //invert the arguments, but complain about it

      if (period !== null && !isNaN(+period)) {
        deprecateSimple(name, 'moment().' + name + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' + 'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
        tmp = val;
        val = period;
        period = tmp;
      }

      val = typeof val === 'string' ? +val : val;
      dur = createDuration(val, period);
      addSubtract(this, dur, direction);
      return this;
    };
  }

  function addSubtract(mom, duration, isAdding, updateOffset) {
    var milliseconds = duration._milliseconds,
        days = absRound(duration._days),
        months = absRound(duration._months);

    if (!mom.isValid()) {
      // No op
      return;
    }

    updateOffset = updateOffset == null ? true : updateOffset;

    if (months) {
      setMonth(mom, get(mom, 'Month') + months * isAdding);
    }

    if (days) {
      set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
    }

    if (milliseconds) {
      mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
    }

    if (updateOffset) {
      hooks.updateOffset(mom, days || months);
    }
  }

  var add = createAdder(1, 'add');
  var subtract = createAdder(-1, 'subtract');

  function getCalendarFormat(myMoment, now) {
    var diff = myMoment.diff(now, 'days', true);
    return diff < -6 ? 'sameElse' : diff < -1 ? 'lastWeek' : diff < 0 ? 'lastDay' : diff < 1 ? 'sameDay' : diff < 2 ? 'nextDay' : diff < 7 ? 'nextWeek' : 'sameElse';
  }

  function calendar$1(time, formats) {
    // We want to compare the start of today, vs this.
    // Getting start-of-today depends on whether we're local/utc/offset or not.
    var now = time || createLocal(),
        sod = cloneWithOffset(now, this).startOf('day'),
        format = hooks.calendarFormat(this, sod) || 'sameElse';
    var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);
    return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
  }

  function clone() {
    return new Moment(this);
  }

  function isAfter(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);

    if (!(this.isValid() && localInput.isValid())) {
      return false;
    }

    units = normalizeUnits(units) || 'millisecond';

    if (units === 'millisecond') {
      return this.valueOf() > localInput.valueOf();
    } else {
      return localInput.valueOf() < this.clone().startOf(units).valueOf();
    }
  }

  function isBefore(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input);

    if (!(this.isValid() && localInput.isValid())) {
      return false;
    }

    units = normalizeUnits(units) || 'millisecond';

    if (units === 'millisecond') {
      return this.valueOf() < localInput.valueOf();
    } else {
      return this.clone().endOf(units).valueOf() < localInput.valueOf();
    }
  }

  function isBetween(from, to, units, inclusivity) {
    var localFrom = isMoment(from) ? from : createLocal(from),
        localTo = isMoment(to) ? to : createLocal(to);

    if (!(this.isValid() && localFrom.isValid() && localTo.isValid())) {
      return false;
    }

    inclusivity = inclusivity || '()';
    return (inclusivity[0] === '(' ? this.isAfter(localFrom, units) : !this.isBefore(localFrom, units)) && (inclusivity[1] === ')' ? this.isBefore(localTo, units) : !this.isAfter(localTo, units));
  }

  function isSame(input, units) {
    var localInput = isMoment(input) ? input : createLocal(input),
        inputMs;

    if (!(this.isValid() && localInput.isValid())) {
      return false;
    }

    units = normalizeUnits(units) || 'millisecond';

    if (units === 'millisecond') {
      return this.valueOf() === localInput.valueOf();
    } else {
      inputMs = localInput.valueOf();
      return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
    }
  }

  function isSameOrAfter(input, units) {
    return this.isSame(input, units) || this.isAfter(input, units);
  }

  function isSameOrBefore(input, units) {
    return this.isSame(input, units) || this.isBefore(input, units);
  }

  function diff(input, units, asFloat) {
    var that, zoneDelta, output;

    if (!this.isValid()) {
      return NaN;
    }

    that = cloneWithOffset(input, this);

    if (!that.isValid()) {
      return NaN;
    }

    zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;
    units = normalizeUnits(units);

    switch (units) {
      case 'year':
        output = monthDiff(this, that) / 12;
        break;

      case 'month':
        output = monthDiff(this, that);
        break;

      case 'quarter':
        output = monthDiff(this, that) / 3;
        break;

      case 'second':
        output = (this - that) / 1e3;
        break;
      // 1000

      case 'minute':
        output = (this - that) / 6e4;
        break;
      // 1000 * 60

      case 'hour':
        output = (this - that) / 36e5;
        break;
      // 1000 * 60 * 60

      case 'day':
        output = (this - that - zoneDelta) / 864e5;
        break;
      // 1000 * 60 * 60 * 24, negate dst

      case 'week':
        output = (this - that - zoneDelta) / 6048e5;
        break;
      // 1000 * 60 * 60 * 24 * 7, negate dst

      default:
        output = this - that;
    }

    return asFloat ? output : absFloor(output);
  }

  function monthDiff(a, b) {
    // difference in months
    var wholeMonthDiff = (b.year() - a.year()) * 12 + (b.month() - a.month()),
        // b is in (anchor - 1 month, anchor + 1 month)
    anchor = a.clone().add(wholeMonthDiff, 'months'),
        anchor2,
        adjust;

    if (b - anchor < 0) {
      anchor2 = a.clone().add(wholeMonthDiff - 1, 'months'); // linear across the month

      adjust = (b - anchor) / (anchor - anchor2);
    } else {
      anchor2 = a.clone().add(wholeMonthDiff + 1, 'months'); // linear across the month

      adjust = (b - anchor) / (anchor2 - anchor);
    } //check for negative zero, return zero if negative zero


    return -(wholeMonthDiff + adjust) || 0;
  }

  hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
  hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

  function toString() {
    return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
  }

  function toISOString(keepOffset) {
    if (!this.isValid()) {
      return null;
    }

    var utc = keepOffset !== true;
    var m = utc ? this.clone().utc() : this;

    if (m.year() < 0 || m.year() > 9999) {
      return formatMoment(m, utc ? 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYYYY-MM-DD[T]HH:mm:ss.SSSZ');
    }

    if (isFunction(Date.prototype.toISOString)) {
      // native implementation is ~50x faster, use it when we can
      if (utc) {
        return this.toDate().toISOString();
      } else {
        return new Date(this.valueOf() + this.utcOffset() * 60 * 1000).toISOString().replace('Z', formatMoment(m, 'Z'));
      }
    }

    return formatMoment(m, utc ? 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]' : 'YYYY-MM-DD[T]HH:mm:ss.SSSZ');
  }
  /**
   * Return a human readable representation of a moment that can
   * also be evaluated to get a new moment which is the same
   *
   * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
   */


  function inspect() {
    if (!this.isValid()) {
      return 'moment.invalid(/* ' + this._i + ' */)';
    }

    var func = 'moment';
    var zone = '';

    if (!this.isLocal()) {
      func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
      zone = 'Z';
    }

    var prefix = '[' + func + '("]';
    var year = 0 <= this.year() && this.year() <= 9999 ? 'YYYY' : 'YYYYYY';
    var datetime = '-MM-DD[T]HH:mm:ss.SSS';
    var suffix = zone + '[")]';
    return this.format(prefix + year + datetime + suffix);
  }

  function format(inputString) {
    if (!inputString) {
      inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
    }

    var output = formatMoment(this, inputString);
    return this.localeData().postformat(output);
  }

  function from(time, withoutSuffix) {
    if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
      return createDuration({
        to: this,
        from: time
      }).locale(this.locale()).humanize(!withoutSuffix);
    } else {
      return this.localeData().invalidDate();
    }
  }

  function fromNow(withoutSuffix) {
    return this.from(createLocal(), withoutSuffix);
  }

  function to(time, withoutSuffix) {
    if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
      return createDuration({
        from: this,
        to: time
      }).locale(this.locale()).humanize(!withoutSuffix);
    } else {
      return this.localeData().invalidDate();
    }
  }

  function toNow(withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
  } // If passed a locale key, it will set the locale for this
  // instance.  Otherwise, it will return the locale configuration
  // variables for this instance.


  function locale(key) {
    var newLocaleData;

    if (key === undefined) {
      return this._locale._abbr;
    } else {
      newLocaleData = getLocale(key);

      if (newLocaleData != null) {
        this._locale = newLocaleData;
      }

      return this;
    }
  }

  var lang = deprecate('moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.', function (key) {
    if (key === undefined) {
      return this.localeData();
    } else {
      return this.locale(key);
    }
  });

  function localeData() {
    return this._locale;
  }

  var MS_PER_SECOND = 1000;
  var MS_PER_MINUTE = 60 * MS_PER_SECOND;
  var MS_PER_HOUR = 60 * MS_PER_MINUTE;
  var MS_PER_400_YEARS = (365 * 400 + 97) * 24 * MS_PER_HOUR; // actual modulo - handles negative numbers (for dates before 1970):

  function mod$1(dividend, divisor) {
    return (dividend % divisor + divisor) % divisor;
  }

  function localStartOfDate(y, m, d) {
    // the date constructor remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0) {
      // preserve leap years using a full 400 year cycle, then reset
      return new Date(y + 400, m, d) - MS_PER_400_YEARS;
    } else {
      return new Date(y, m, d).valueOf();
    }
  }

  function utcStartOfDate(y, m, d) {
    // Date.UTC remaps years 0-99 to 1900-1999
    if (y < 100 && y >= 0) {
      // preserve leap years using a full 400 year cycle, then reset
      return Date.UTC(y + 400, m, d) - MS_PER_400_YEARS;
    } else {
      return Date.UTC(y, m, d);
    }
  }

  function startOf(units) {
    var time;
    units = normalizeUnits(units);

    if (units === undefined || units === 'millisecond' || !this.isValid()) {
      return this;
    }

    var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

    switch (units) {
      case 'year':
        time = startOfDate(this.year(), 0, 1);
        break;

      case 'quarter':
        time = startOfDate(this.year(), this.month() - this.month() % 3, 1);
        break;

      case 'month':
        time = startOfDate(this.year(), this.month(), 1);
        break;

      case 'week':
        time = startOfDate(this.year(), this.month(), this.date() - this.weekday());
        break;

      case 'isoWeek':
        time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1));
        break;

      case 'day':
      case 'date':
        time = startOfDate(this.year(), this.month(), this.date());
        break;

      case 'hour':
        time = this._d.valueOf();
        time -= mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR);
        break;

      case 'minute':
        time = this._d.valueOf();
        time -= mod$1(time, MS_PER_MINUTE);
        break;

      case 'second':
        time = this._d.valueOf();
        time -= mod$1(time, MS_PER_SECOND);
        break;
    }

    this._d.setTime(time);

    hooks.updateOffset(this, true);
    return this;
  }

  function endOf(units) {
    var time;
    units = normalizeUnits(units);

    if (units === undefined || units === 'millisecond' || !this.isValid()) {
      return this;
    }

    var startOfDate = this._isUTC ? utcStartOfDate : localStartOfDate;

    switch (units) {
      case 'year':
        time = startOfDate(this.year() + 1, 0, 1) - 1;
        break;

      case 'quarter':
        time = startOfDate(this.year(), this.month() - this.month() % 3 + 3, 1) - 1;
        break;

      case 'month':
        time = startOfDate(this.year(), this.month() + 1, 1) - 1;
        break;

      case 'week':
        time = startOfDate(this.year(), this.month(), this.date() - this.weekday() + 7) - 1;
        break;

      case 'isoWeek':
        time = startOfDate(this.year(), this.month(), this.date() - (this.isoWeekday() - 1) + 7) - 1;
        break;

      case 'day':
      case 'date':
        time = startOfDate(this.year(), this.month(), this.date() + 1) - 1;
        break;

      case 'hour':
        time = this._d.valueOf();
        time += MS_PER_HOUR - mod$1(time + (this._isUTC ? 0 : this.utcOffset() * MS_PER_MINUTE), MS_PER_HOUR) - 1;
        break;

      case 'minute':
        time = this._d.valueOf();
        time += MS_PER_MINUTE - mod$1(time, MS_PER_MINUTE) - 1;
        break;

      case 'second':
        time = this._d.valueOf();
        time += MS_PER_SECOND - mod$1(time, MS_PER_SECOND) - 1;
        break;
    }

    this._d.setTime(time);

    hooks.updateOffset(this, true);
    return this;
  }

  function valueOf() {
    return this._d.valueOf() - (this._offset || 0) * 60000;
  }

  function unix() {
    return Math.floor(this.valueOf() / 1000);
  }

  function toDate() {
    return new Date(this.valueOf());
  }

  function toArray() {
    var m = this;
    return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
  }

  function toObject() {
    var m = this;
    return {
      years: m.year(),
      months: m.month(),
      date: m.date(),
      hours: m.hours(),
      minutes: m.minutes(),
      seconds: m.seconds(),
      milliseconds: m.milliseconds()
    };
  }

  function toJSON() {
    // new Date(NaN).toJSON() === null
    return this.isValid() ? this.toISOString() : null;
  }

  function isValid$2() {
    return isValid(this);
  }

  function parsingFlags() {
    return extend({}, getParsingFlags(this));
  }

  function invalidAt() {
    return getParsingFlags(this).overflow;
  }

  function creationData() {
    return {
      input: this._i,
      format: this._f,
      locale: this._locale,
      isUTC: this._isUTC,
      strict: this._strict
    };
  } // FORMATTING


  addFormatToken(0, ['gg', 2], 0, function () {
    return this.weekYear() % 100;
  });
  addFormatToken(0, ['GG', 2], 0, function () {
    return this.isoWeekYear() % 100;
  });

  function addWeekYearFormatToken(token, getter) {
    addFormatToken(0, [token, token.length], 0, getter);
  }

  addWeekYearFormatToken('gggg', 'weekYear');
  addWeekYearFormatToken('ggggg', 'weekYear');
  addWeekYearFormatToken('GGGG', 'isoWeekYear');
  addWeekYearFormatToken('GGGGG', 'isoWeekYear'); // ALIASES

  addUnitAlias('weekYear', 'gg');
  addUnitAlias('isoWeekYear', 'GG'); // PRIORITY

  addUnitPriority('weekYear', 1);
  addUnitPriority('isoWeekYear', 1); // PARSING

  addRegexToken('G', matchSigned);
  addRegexToken('g', matchSigned);
  addRegexToken('GG', match1to2, match2);
  addRegexToken('gg', match1to2, match2);
  addRegexToken('GGGG', match1to4, match4);
  addRegexToken('gggg', match1to4, match4);
  addRegexToken('GGGGG', match1to6, match6);
  addRegexToken('ggggg', match1to6, match6);
  addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
    week[token.substr(0, 2)] = toInt(input);
  });
  addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
    week[token] = hooks.parseTwoDigitYear(input);
  }); // MOMENTS

  function getSetWeekYear(input) {
    return getSetWeekYearHelper.call(this, input, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy);
  }

  function getSetISOWeekYear(input) {
    return getSetWeekYearHelper.call(this, input, this.isoWeek(), this.isoWeekday(), 1, 4);
  }

  function getISOWeeksInYear() {
    return weeksInYear(this.year(), 1, 4);
  }

  function getWeeksInYear() {
    var weekInfo = this.localeData()._week;

    return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
  }

  function getSetWeekYearHelper(input, week, weekday, dow, doy) {
    var weeksTarget;

    if (input == null) {
      return weekOfYear(this, dow, doy).year;
    } else {
      weeksTarget = weeksInYear(input, dow, doy);

      if (week > weeksTarget) {
        week = weeksTarget;
      }

      return setWeekAll.call(this, input, week, weekday, dow, doy);
    }
  }

  function setWeekAll(weekYear, week, weekday, dow, doy) {
    var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
        date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);
    this.year(date.getUTCFullYear());
    this.month(date.getUTCMonth());
    this.date(date.getUTCDate());
    return this;
  } // FORMATTING


  addFormatToken('Q', 0, 'Qo', 'quarter'); // ALIASES

  addUnitAlias('quarter', 'Q'); // PRIORITY

  addUnitPriority('quarter', 7); // PARSING

  addRegexToken('Q', match1);
  addParseToken('Q', function (input, array) {
    array[MONTH] = (toInt(input) - 1) * 3;
  }); // MOMENTS

  function getSetQuarter(input) {
    return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
  } // FORMATTING


  addFormatToken('D', ['DD', 2], 'Do', 'date'); // ALIASES

  addUnitAlias('date', 'D'); // PRIORITY

  addUnitPriority('date', 9); // PARSING

  addRegexToken('D', match1to2);
  addRegexToken('DD', match1to2, match2);
  addRegexToken('Do', function (isStrict, locale) {
    // TODO: Remove "ordinalParse" fallback in next major release.
    return isStrict ? locale._dayOfMonthOrdinalParse || locale._ordinalParse : locale._dayOfMonthOrdinalParseLenient;
  });
  addParseToken(['D', 'DD'], DATE);
  addParseToken('Do', function (input, array) {
    array[DATE] = toInt(input.match(match1to2)[0]);
  }); // MOMENTS

  var getSetDayOfMonth = makeGetSet('Date', true); // FORMATTING

  addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear'); // ALIASES

  addUnitAlias('dayOfYear', 'DDD'); // PRIORITY

  addUnitPriority('dayOfYear', 4); // PARSING

  addRegexToken('DDD', match1to3);
  addRegexToken('DDDD', match3);
  addParseToken(['DDD', 'DDDD'], function (input, array, config) {
    config._dayOfYear = toInt(input);
  }); // HELPERS
  // MOMENTS

  function getSetDayOfYear(input) {
    var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
    return input == null ? dayOfYear : this.add(input - dayOfYear, 'd');
  } // FORMATTING


  addFormatToken('m', ['mm', 2], 0, 'minute'); // ALIASES

  addUnitAlias('minute', 'm'); // PRIORITY

  addUnitPriority('minute', 14); // PARSING

  addRegexToken('m', match1to2);
  addRegexToken('mm', match1to2, match2);
  addParseToken(['m', 'mm'], MINUTE); // MOMENTS

  var getSetMinute = makeGetSet('Minutes', false); // FORMATTING

  addFormatToken('s', ['ss', 2], 0, 'second'); // ALIASES

  addUnitAlias('second', 's'); // PRIORITY

  addUnitPriority('second', 15); // PARSING

  addRegexToken('s', match1to2);
  addRegexToken('ss', match1to2, match2);
  addParseToken(['s', 'ss'], SECOND); // MOMENTS

  var getSetSecond = makeGetSet('Seconds', false); // FORMATTING

  addFormatToken('S', 0, 0, function () {
    return ~~(this.millisecond() / 100);
  });
  addFormatToken(0, ['SS', 2], 0, function () {
    return ~~(this.millisecond() / 10);
  });
  addFormatToken(0, ['SSS', 3], 0, 'millisecond');
  addFormatToken(0, ['SSSS', 4], 0, function () {
    return this.millisecond() * 10;
  });
  addFormatToken(0, ['SSSSS', 5], 0, function () {
    return this.millisecond() * 100;
  });
  addFormatToken(0, ['SSSSSS', 6], 0, function () {
    return this.millisecond() * 1000;
  });
  addFormatToken(0, ['SSSSSSS', 7], 0, function () {
    return this.millisecond() * 10000;
  });
  addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
    return this.millisecond() * 100000;
  });
  addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
    return this.millisecond() * 1000000;
  }); // ALIASES

  addUnitAlias('millisecond', 'ms'); // PRIORITY

  addUnitPriority('millisecond', 16); // PARSING

  addRegexToken('S', match1to3, match1);
  addRegexToken('SS', match1to3, match2);
  addRegexToken('SSS', match1to3, match3);
  var token;

  for (token = 'SSSS'; token.length <= 9; token += 'S') {
    addRegexToken(token, matchUnsigned);
  }

  function parseMs(input, array) {
    array[MILLISECOND] = toInt(('0.' + input) * 1000);
  }

  for (token = 'S'; token.length <= 9; token += 'S') {
    addParseToken(token, parseMs);
  } // MOMENTS


  var getSetMillisecond = makeGetSet('Milliseconds', false); // FORMATTING

  addFormatToken('z', 0, 0, 'zoneAbbr');
  addFormatToken('zz', 0, 0, 'zoneName'); // MOMENTS

  function getZoneAbbr() {
    return this._isUTC ? 'UTC' : '';
  }

  function getZoneName() {
    return this._isUTC ? 'Coordinated Universal Time' : '';
  }

  var proto = Moment.prototype;
  proto.add = add;
  proto.calendar = calendar$1;
  proto.clone = clone;
  proto.diff = diff;
  proto.endOf = endOf;
  proto.format = format;
  proto.from = from;
  proto.fromNow = fromNow;
  proto.to = to;
  proto.toNow = toNow;
  proto.get = stringGet;
  proto.invalidAt = invalidAt;
  proto.isAfter = isAfter;
  proto.isBefore = isBefore;
  proto.isBetween = isBetween;
  proto.isSame = isSame;
  proto.isSameOrAfter = isSameOrAfter;
  proto.isSameOrBefore = isSameOrBefore;
  proto.isValid = isValid$2;
  proto.lang = lang;
  proto.locale = locale;
  proto.localeData = localeData;
  proto.max = prototypeMax;
  proto.min = prototypeMin;
  proto.parsingFlags = parsingFlags;
  proto.set = stringSet;
  proto.startOf = startOf;
  proto.subtract = subtract;
  proto.toArray = toArray;
  proto.toObject = toObject;
  proto.toDate = toDate;
  proto.toISOString = toISOString;
  proto.inspect = inspect;
  proto.toJSON = toJSON;
  proto.toString = toString;
  proto.unix = unix;
  proto.valueOf = valueOf;
  proto.creationData = creationData;
  proto.year = getSetYear;
  proto.isLeapYear = getIsLeapYear;
  proto.weekYear = getSetWeekYear;
  proto.isoWeekYear = getSetISOWeekYear;
  proto.quarter = proto.quarters = getSetQuarter;
  proto.month = getSetMonth;
  proto.daysInMonth = getDaysInMonth;
  proto.week = proto.weeks = getSetWeek;
  proto.isoWeek = proto.isoWeeks = getSetISOWeek;
  proto.weeksInYear = getWeeksInYear;
  proto.isoWeeksInYear = getISOWeeksInYear;
  proto.date = getSetDayOfMonth;
  proto.day = proto.days = getSetDayOfWeek;
  proto.weekday = getSetLocaleDayOfWeek;
  proto.isoWeekday = getSetISODayOfWeek;
  proto.dayOfYear = getSetDayOfYear;
  proto.hour = proto.hours = getSetHour;
  proto.minute = proto.minutes = getSetMinute;
  proto.second = proto.seconds = getSetSecond;
  proto.millisecond = proto.milliseconds = getSetMillisecond;
  proto.utcOffset = getSetOffset;
  proto.utc = setOffsetToUTC;
  proto.local = setOffsetToLocal;
  proto.parseZone = setOffsetToParsedOffset;
  proto.hasAlignedHourOffset = hasAlignedHourOffset;
  proto.isDST = isDaylightSavingTime;
  proto.isLocal = isLocal;
  proto.isUtcOffset = isUtcOffset;
  proto.isUtc = isUtc;
  proto.isUTC = isUtc;
  proto.zoneAbbr = getZoneAbbr;
  proto.zoneName = getZoneName;
  proto.dates = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
  proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
  proto.years = deprecate('years accessor is deprecated. Use year instead', getSetYear);
  proto.zone = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
  proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

  function createUnix(input) {
    return createLocal(input * 1000);
  }

  function createInZone() {
    return createLocal.apply(null, arguments).parseZone();
  }

  function preParsePostFormat(string) {
    return string;
  }

  var proto$1 = Locale.prototype;
  proto$1.calendar = calendar;
  proto$1.longDateFormat = longDateFormat;
  proto$1.invalidDate = invalidDate;
  proto$1.ordinal = ordinal;
  proto$1.preparse = preParsePostFormat;
  proto$1.postformat = preParsePostFormat;
  proto$1.relativeTime = relativeTime;
  proto$1.pastFuture = pastFuture;
  proto$1.set = set;
  proto$1.months = localeMonths;
  proto$1.monthsShort = localeMonthsShort;
  proto$1.monthsParse = localeMonthsParse;
  proto$1.monthsRegex = monthsRegex;
  proto$1.monthsShortRegex = monthsShortRegex;
  proto$1.week = localeWeek;
  proto$1.firstDayOfYear = localeFirstDayOfYear;
  proto$1.firstDayOfWeek = localeFirstDayOfWeek;
  proto$1.weekdays = localeWeekdays;
  proto$1.weekdaysMin = localeWeekdaysMin;
  proto$1.weekdaysShort = localeWeekdaysShort;
  proto$1.weekdaysParse = localeWeekdaysParse;
  proto$1.weekdaysRegex = weekdaysRegex;
  proto$1.weekdaysShortRegex = weekdaysShortRegex;
  proto$1.weekdaysMinRegex = weekdaysMinRegex;
  proto$1.isPM = localeIsPM;
  proto$1.meridiem = localeMeridiem;

  function get$1(format, index, field, setter) {
    var locale = getLocale();
    var utc = createUTC().set(setter, index);
    return locale[field](utc, format);
  }

  function listMonthsImpl(format, index, field) {
    if (isNumber(format)) {
      index = format;
      format = undefined;
    }

    format = format || '';

    if (index != null) {
      return get$1(format, index, field, 'month');
    }

    var i;
    var out = [];

    for (i = 0; i < 12; i++) {
      out[i] = get$1(format, i, field, 'month');
    }

    return out;
  } // ()
  // (5)
  // (fmt, 5)
  // (fmt)
  // (true)
  // (true, 5)
  // (true, fmt, 5)
  // (true, fmt)


  function listWeekdaysImpl(localeSorted, format, index, field) {
    if (typeof localeSorted === 'boolean') {
      if (isNumber(format)) {
        index = format;
        format = undefined;
      }

      format = format || '';
    } else {
      format = localeSorted;
      index = format;
      localeSorted = false;

      if (isNumber(format)) {
        index = format;
        format = undefined;
      }

      format = format || '';
    }

    var locale = getLocale(),
        shift = localeSorted ? locale._week.dow : 0;

    if (index != null) {
      return get$1(format, (index + shift) % 7, field, 'day');
    }

    var i;
    var out = [];

    for (i = 0; i < 7; i++) {
      out[i] = get$1(format, (i + shift) % 7, field, 'day');
    }

    return out;
  }

  function listMonths(format, index) {
    return listMonthsImpl(format, index, 'months');
  }

  function listMonthsShort(format, index) {
    return listMonthsImpl(format, index, 'monthsShort');
  }

  function listWeekdays(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
  }

  function listWeekdaysShort(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
  }

  function listWeekdaysMin(localeSorted, format, index) {
    return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
  }

  getSetGlobalLocale('en', {
    dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
    ordinal: function (number) {
      var b = number % 10,
          output = toInt(number % 100 / 10) === 1 ? 'th' : b === 1 ? 'st' : b === 2 ? 'nd' : b === 3 ? 'rd' : 'th';
      return number + output;
    }
  }); // Side effect imports

  hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
  hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);
  var mathAbs = Math.abs;

  function abs() {
    var data = this._data;
    this._milliseconds = mathAbs(this._milliseconds);
    this._days = mathAbs(this._days);
    this._months = mathAbs(this._months);
    data.milliseconds = mathAbs(data.milliseconds);
    data.seconds = mathAbs(data.seconds);
    data.minutes = mathAbs(data.minutes);
    data.hours = mathAbs(data.hours);
    data.months = mathAbs(data.months);
    data.years = mathAbs(data.years);
    return this;
  }

  function addSubtract$1(duration, input, value, direction) {
    var other = createDuration(input, value);
    duration._milliseconds += direction * other._milliseconds;
    duration._days += direction * other._days;
    duration._months += direction * other._months;
    return duration._bubble();
  } // supports only 2.0-style add(1, 's') or add(duration)


  function add$1(input, value) {
    return addSubtract$1(this, input, value, 1);
  } // supports only 2.0-style subtract(1, 's') or subtract(duration)


  function subtract$1(input, value) {
    return addSubtract$1(this, input, value, -1);
  }

  function absCeil(number) {
    if (number < 0) {
      return Math.floor(number);
    } else {
      return Math.ceil(number);
    }
  }

  function bubble() {
    var milliseconds = this._milliseconds;
    var days = this._days;
    var months = this._months;
    var data = this._data;
    var seconds, minutes, hours, years, monthsFromDays; // if we have a mix of positive and negative values, bubble down first
    // check: https://github.com/moment/moment/issues/2166

    if (!(milliseconds >= 0 && days >= 0 && months >= 0 || milliseconds <= 0 && days <= 0 && months <= 0)) {
      milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
      days = 0;
      months = 0;
    } // The following code bubbles up values, see the tests for
    // examples of what that means.


    data.milliseconds = milliseconds % 1000;
    seconds = absFloor(milliseconds / 1000);
    data.seconds = seconds % 60;
    minutes = absFloor(seconds / 60);
    data.minutes = minutes % 60;
    hours = absFloor(minutes / 60);
    data.hours = hours % 24;
    days += absFloor(hours / 24); // convert days to months

    monthsFromDays = absFloor(daysToMonths(days));
    months += monthsFromDays;
    days -= absCeil(monthsToDays(monthsFromDays)); // 12 months -> 1 year

    years = absFloor(months / 12);
    months %= 12;
    data.days = days;
    data.months = months;
    data.years = years;
    return this;
  }

  function daysToMonths(days) {
    // 400 years have 146097 days (taking into account leap year rules)
    // 400 years have 12 months === 4800
    return days * 4800 / 146097;
  }

  function monthsToDays(months) {
    // the reverse of daysToMonths
    return months * 146097 / 4800;
  }

  function as(units) {
    if (!this.isValid()) {
      return NaN;
    }

    var days;
    var months;
    var milliseconds = this._milliseconds;
    units = normalizeUnits(units);

    if (units === 'month' || units === 'quarter' || units === 'year') {
      days = this._days + milliseconds / 864e5;
      months = this._months + daysToMonths(days);

      switch (units) {
        case 'month':
          return months;

        case 'quarter':
          return months / 3;

        case 'year':
          return months / 12;
      }
    } else {
      // handle milliseconds separately because of floating point math errors (issue #1867)
      days = this._days + Math.round(monthsToDays(this._months));

      switch (units) {
        case 'week':
          return days / 7 + milliseconds / 6048e5;

        case 'day':
          return days + milliseconds / 864e5;

        case 'hour':
          return days * 24 + milliseconds / 36e5;

        case 'minute':
          return days * 1440 + milliseconds / 6e4;

        case 'second':
          return days * 86400 + milliseconds / 1000;
        // Math.floor prevents floating point math errors here

        case 'millisecond':
          return Math.floor(days * 864e5) + milliseconds;

        default:
          throw new Error('Unknown unit ' + units);
      }
    }
  } // TODO: Use this.as('ms')?


  function valueOf$1() {
    if (!this.isValid()) {
      return NaN;
    }

    return this._milliseconds + this._days * 864e5 + this._months % 12 * 2592e6 + toInt(this._months / 12) * 31536e6;
  }

  function makeAs(alias) {
    return function () {
      return this.as(alias);
    };
  }

  var asMilliseconds = makeAs('ms');
  var asSeconds = makeAs('s');
  var asMinutes = makeAs('m');
  var asHours = makeAs('h');
  var asDays = makeAs('d');
  var asWeeks = makeAs('w');
  var asMonths = makeAs('M');
  var asQuarters = makeAs('Q');
  var asYears = makeAs('y');

  function clone$1() {
    return createDuration(this);
  }

  function get$2(units) {
    units = normalizeUnits(units);
    return this.isValid() ? this[units + 's']() : NaN;
  }

  function makeGetter(name) {
    return function () {
      return this.isValid() ? this._data[name] : NaN;
    };
  }

  var milliseconds = makeGetter('milliseconds');
  var seconds = makeGetter('seconds');
  var minutes = makeGetter('minutes');
  var hours = makeGetter('hours');
  var days = makeGetter('days');
  var months = makeGetter('months');
  var years = makeGetter('years');

  function weeks() {
    return absFloor(this.days() / 7);
  }

  var round = Math.round;
  var thresholds = {
    ss: 44,
    // a few seconds to seconds
    s: 45,
    // seconds to minute
    m: 45,
    // minutes to hour
    h: 22,
    // hours to day
    d: 26,
    // days to month
    M: 11 // months to year

  }; // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize

  function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
    return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
  }

  function relativeTime$1(posNegDuration, withoutSuffix, locale) {
    var duration = createDuration(posNegDuration).abs();
    var seconds = round(duration.as('s'));
    var minutes = round(duration.as('m'));
    var hours = round(duration.as('h'));
    var days = round(duration.as('d'));
    var months = round(duration.as('M'));
    var years = round(duration.as('y'));
    var a = seconds <= thresholds.ss && ['s', seconds] || seconds < thresholds.s && ['ss', seconds] || minutes <= 1 && ['m'] || minutes < thresholds.m && ['mm', minutes] || hours <= 1 && ['h'] || hours < thresholds.h && ['hh', hours] || days <= 1 && ['d'] || days < thresholds.d && ['dd', days] || months <= 1 && ['M'] || months < thresholds.M && ['MM', months] || years <= 1 && ['y'] || ['yy', years];
    a[2] = withoutSuffix;
    a[3] = +posNegDuration > 0;
    a[4] = locale;
    return substituteTimeAgo.apply(null, a);
  } // This function allows you to set the rounding function for relative time strings


  function getSetRelativeTimeRounding(roundingFunction) {
    if (roundingFunction === undefined) {
      return round;
    }

    if (typeof roundingFunction === 'function') {
      round = roundingFunction;
      return true;
    }

    return false;
  } // This function allows you to set a threshold for relative time strings


  function getSetRelativeTimeThreshold(threshold, limit) {
    if (thresholds[threshold] === undefined) {
      return false;
    }

    if (limit === undefined) {
      return thresholds[threshold];
    }

    thresholds[threshold] = limit;

    if (threshold === 's') {
      thresholds.ss = limit - 1;
    }

    return true;
  }

  function humanize(withSuffix) {
    if (!this.isValid()) {
      return this.localeData().invalidDate();
    }

    var locale = this.localeData();
    var output = relativeTime$1(this, !withSuffix, locale);

    if (withSuffix) {
      output = locale.pastFuture(+this, output);
    }

    return locale.postformat(output);
  }

  var abs$1 = Math.abs;

  function sign(x) {
    return (x > 0) - (x < 0) || +x;
  }

  function toISOString$1() {
    // for ISO strings we do not use the normal bubbling rules:
    //  * milliseconds bubble up until they become hours
    //  * days do not bubble at all
    //  * months bubble up until they become years
    // This is because there is no context-free conversion between hours and days
    // (think of clock changes)
    // and also not between days and months (28-31 days per month)
    if (!this.isValid()) {
      return this.localeData().invalidDate();
    }

    var seconds = abs$1(this._milliseconds) / 1000;
    var days = abs$1(this._days);
    var months = abs$1(this._months);
    var minutes, hours, years; // 3600 seconds -> 60 minutes -> 1 hour

    minutes = absFloor(seconds / 60);
    hours = absFloor(minutes / 60);
    seconds %= 60;
    minutes %= 60; // 12 months -> 1 year

    years = absFloor(months / 12);
    months %= 12; // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js

    var Y = years;
    var M = months;
    var D = days;
    var h = hours;
    var m = minutes;
    var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
    var total = this.asSeconds();

    if (!total) {
      // this is the same as C#'s (Noda) and python (isodate)...
      // but not other JS (goog.date)
      return 'P0D';
    }

    var totalSign = total < 0 ? '-' : '';
    var ymSign = sign(this._months) !== sign(total) ? '-' : '';
    var daysSign = sign(this._days) !== sign(total) ? '-' : '';
    var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';
    return totalSign + 'P' + (Y ? ymSign + Y + 'Y' : '') + (M ? ymSign + M + 'M' : '') + (D ? daysSign + D + 'D' : '') + (h || m || s ? 'T' : '') + (h ? hmsSign + h + 'H' : '') + (m ? hmsSign + m + 'M' : '') + (s ? hmsSign + s + 'S' : '');
  }

  var proto$2 = Duration.prototype;
  proto$2.isValid = isValid$1;
  proto$2.abs = abs;
  proto$2.add = add$1;
  proto$2.subtract = subtract$1;
  proto$2.as = as;
  proto$2.asMilliseconds = asMilliseconds;
  proto$2.asSeconds = asSeconds;
  proto$2.asMinutes = asMinutes;
  proto$2.asHours = asHours;
  proto$2.asDays = asDays;
  proto$2.asWeeks = asWeeks;
  proto$2.asMonths = asMonths;
  proto$2.asQuarters = asQuarters;
  proto$2.asYears = asYears;
  proto$2.valueOf = valueOf$1;
  proto$2._bubble = bubble;
  proto$2.clone = clone$1;
  proto$2.get = get$2;
  proto$2.milliseconds = milliseconds;
  proto$2.seconds = seconds;
  proto$2.minutes = minutes;
  proto$2.hours = hours;
  proto$2.days = days;
  proto$2.weeks = weeks;
  proto$2.months = months;
  proto$2.years = years;
  proto$2.humanize = humanize;
  proto$2.toISOString = toISOString$1;
  proto$2.toString = toISOString$1;
  proto$2.toJSON = toISOString$1;
  proto$2.locale = locale;
  proto$2.localeData = localeData;
  proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
  proto$2.lang = lang; // Side effect imports
  // FORMATTING

  addFormatToken('X', 0, 0, 'unix');
  addFormatToken('x', 0, 0, 'valueOf'); // PARSING

  addRegexToken('x', matchSigned);
  addRegexToken('X', matchTimestamp);
  addParseToken('X', function (input, array, config) {
    config._d = new Date(parseFloat(input, 10) * 1000);
  });
  addParseToken('x', function (input, array, config) {
    config._d = new Date(toInt(input));
  }); // Side effect imports

  hooks.version = '2.24.0';
  setHookCallback(createLocal);
  hooks.fn = proto;
  hooks.min = min;
  hooks.max = max;
  hooks.now = now;
  hooks.utc = createUTC;
  hooks.unix = createUnix;
  hooks.months = listMonths;
  hooks.isDate = isDate;
  hooks.locale = getSetGlobalLocale;
  hooks.invalid = createInvalid;
  hooks.duration = createDuration;
  hooks.isMoment = isMoment;
  hooks.weekdays = listWeekdays;
  hooks.parseZone = createInZone;
  hooks.localeData = getLocale;
  hooks.isDuration = isDuration;
  hooks.monthsShort = listMonthsShort;
  hooks.weekdaysMin = listWeekdaysMin;
  hooks.defineLocale = defineLocale;
  hooks.updateLocale = updateLocale;
  hooks.locales = listLocales;
  hooks.weekdaysShort = listWeekdaysShort;
  hooks.normalizeUnits = normalizeUnits;
  hooks.relativeTimeRounding = getSetRelativeTimeRounding;
  hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
  hooks.calendarFormat = getCalendarFormat;
  hooks.prototype = proto; // currently HTML5 input type only supports 24-hour formats

  hooks.HTML5_FMT = {
    DATETIME_LOCAL: 'YYYY-MM-DDTHH:mm',
    // <input type="datetime-local" />
    DATETIME_LOCAL_SECONDS: 'YYYY-MM-DDTHH:mm:ss',
    // <input type="datetime-local" step="1" />
    DATETIME_LOCAL_MS: 'YYYY-MM-DDTHH:mm:ss.SSS',
    // <input type="datetime-local" step="0.001" />
    DATE: 'YYYY-MM-DD',
    // <input type="date" />
    TIME: 'HH:mm',
    // <input type="time" />
    TIME_SECONDS: 'HH:mm:ss',
    // <input type="time" step="1" />
    TIME_MS: 'HH:mm:ss.SSS',
    // <input type="time" step="0.001" />
    WEEK: 'GGGG-[W]WW',
    // <input type="week" />
    MONTH: 'YYYY-MM' // <input type="month" />

  };
  return hooks;
});
!function (i) {
  "use strict";

  "function" == typeof define && define.amd ? define(["jquery"], i) : "undefined" != typeof exports ? module.exports = i(require("jquery")) : i(jQuery);
}(function (i) {
  "use strict";

  var e = window.Slick || {};
  (e = function () {
    var e = 0;
    return function (t, o) {
      var s,
          n = this;
      n.defaults = {
        accessibility: !0,
        adaptiveHeight: !1,
        appendArrows: i(t),
        appendDots: i(t),
        arrows: !0,
        asNavFor: null,
        prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
        nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
        autoplay: !1,
        autoplaySpeed: 3e3,
        centerMode: !1,
        centerPadding: "50px",
        cssEase: "ease",
        customPaging: function (e, t) {
          return i('<button type="button" />').text(t + 1);
        },
        dots: !1,
        dotsClass: "slick-dots",
        draggable: !0,
        easing: "linear",
        edgeFriction: .35,
        fade: !1,
        focusOnSelect: !1,
        focusOnChange: !1,
        infinite: !0,
        initialSlide: 0,
        lazyLoad: "ondemand",
        mobileFirst: !1,
        pauseOnHover: !0,
        pauseOnFocus: !0,
        pauseOnDotsHover: !1,
        respondTo: "window",
        responsive: null,
        rows: 1,
        rtl: !1,
        slide: "",
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 500,
        swipe: !0,
        swipeToSlide: !1,
        touchMove: !0,
        touchThreshold: 5,
        useCSS: !0,
        useTransform: !0,
        variableWidth: !1,
        vertical: !1,
        verticalSwiping: !1,
        waitForAnimate: !0,
        zIndex: 1e3
      }, n.initials = {
        animating: !1,
        dragging: !1,
        autoPlayTimer: null,
        currentDirection: 0,
        currentLeft: null,
        currentSlide: 0,
        direction: 1,
        $dots: null,
        listWidth: null,
        listHeight: null,
        loadIndex: 0,
        $nextArrow: null,
        $prevArrow: null,
        scrolling: !1,
        slideCount: null,
        slideWidth: null,
        $slideTrack: null,
        $slides: null,
        sliding: !1,
        slideOffset: 0,
        swipeLeft: null,
        swiping: !1,
        $list: null,
        touchObject: {},
        transformsEnabled: !1,
        unslicked: !1
      }, i.extend(n, n.initials), n.activeBreakpoint = null, n.animType = null, n.animProp = null, n.breakpoints = [], n.breakpointSettings = [], n.cssTransitions = !1, n.focussed = !1, n.interrupted = !1, n.hidden = "hidden", n.paused = !0, n.positionProp = null, n.respondTo = null, n.rowCount = 1, n.shouldClick = !0, n.$slider = i(t), n.$slidesCache = null, n.transformType = null, n.transitionType = null, n.visibilityChange = "visibilitychange", n.windowWidth = 0, n.windowTimer = null, s = i(t).data("slick") || {}, n.options = i.extend({}, n.defaults, o, s), n.currentSlide = n.options.initialSlide, n.originalSettings = n.options, void 0 !== document.mozHidden ? (n.hidden = "mozHidden", n.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (n.hidden = "webkitHidden", n.visibilityChange = "webkitvisibilitychange"), n.autoPlay = i.proxy(n.autoPlay, n), n.autoPlayClear = i.proxy(n.autoPlayClear, n), n.autoPlayIterator = i.proxy(n.autoPlayIterator, n), n.changeSlide = i.proxy(n.changeSlide, n), n.clickHandler = i.proxy(n.clickHandler, n), n.selectHandler = i.proxy(n.selectHandler, n), n.setPosition = i.proxy(n.setPosition, n), n.swipeHandler = i.proxy(n.swipeHandler, n), n.dragHandler = i.proxy(n.dragHandler, n), n.keyHandler = i.proxy(n.keyHandler, n), n.instanceUid = e++, n.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/, n.registerBreakpoints(), n.init(!0);
    };
  }()).prototype.activateADA = function () {
    this.$slideTrack.find(".slick-active").attr({
      "aria-hidden": "false"
    }).find("a, input, button, select").attr({
      tabindex: "0"
    });
  }, e.prototype.addSlide = e.prototype.slickAdd = function (e, t, o) {
    var s = this;
    if ("boolean" == typeof t) o = t, t = null;else if (t < 0 || t >= s.slideCount) return !1;
    s.unload(), "number" == typeof t ? 0 === t && 0 === s.$slides.length ? i(e).appendTo(s.$slideTrack) : o ? i(e).insertBefore(s.$slides.eq(t)) : i(e).insertAfter(s.$slides.eq(t)) : !0 === o ? i(e).prependTo(s.$slideTrack) : i(e).appendTo(s.$slideTrack), s.$slides = s.$slideTrack.children(this.options.slide), s.$slideTrack.children(this.options.slide).detach(), s.$slideTrack.append(s.$slides), s.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e);
    }), s.$slidesCache = s.$slides, s.reinit();
  }, e.prototype.animateHeight = function () {
    var i = this;

    if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
      i.$list.animate({
        height: e
      }, i.options.speed);
    }
  }, e.prototype.animateSlide = function (e, t) {
    var o = {},
        s = this;
    s.animateHeight(), !0 === s.options.rtl && !1 === s.options.vertical && (e = -e), !1 === s.transformsEnabled ? !1 === s.options.vertical ? s.$slideTrack.animate({
      left: e
    }, s.options.speed, s.options.easing, t) : s.$slideTrack.animate({
      top: e
    }, s.options.speed, s.options.easing, t) : !1 === s.cssTransitions ? (!0 === s.options.rtl && (s.currentLeft = -s.currentLeft), i({
      animStart: s.currentLeft
    }).animate({
      animStart: e
    }, {
      duration: s.options.speed,
      easing: s.options.easing,
      step: function (i) {
        i = Math.ceil(i), !1 === s.options.vertical ? (o[s.animType] = "translate(" + i + "px, 0px)", s.$slideTrack.css(o)) : (o[s.animType] = "translate(0px," + i + "px)", s.$slideTrack.css(o));
      },
      complete: function () {
        t && t.call();
      }
    })) : (s.applyTransition(), e = Math.ceil(e), !1 === s.options.vertical ? o[s.animType] = "translate3d(" + e + "px, 0px, 0px)" : o[s.animType] = "translate3d(0px," + e + "px, 0px)", s.$slideTrack.css(o), t && setTimeout(function () {
      s.disableTransition(), t.call();
    }, s.options.speed));
  }, e.prototype.getNavTarget = function () {
    var e = this,
        t = e.options.asNavFor;
    return t && null !== t && (t = i(t).not(e.$slider)), t;
  }, e.prototype.asNavFor = function (e) {
    var t = this.getNavTarget();
    null !== t && "object" == typeof t && t.each(function () {
      var t = i(this).slick("getSlick");
      t.unslicked || t.slideHandler(e, !0);
    });
  }, e.prototype.applyTransition = function (i) {
    var e = this,
        t = {};
    !1 === e.options.fade ? t[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : t[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase, !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
  }, e.prototype.autoPlay = function () {
    var i = this;
    i.autoPlayClear(), i.slideCount > i.options.slidesToShow && (i.autoPlayTimer = setInterval(i.autoPlayIterator, i.options.autoplaySpeed));
  }, e.prototype.autoPlayClear = function () {
    var i = this;
    i.autoPlayTimer && clearInterval(i.autoPlayTimer);
  }, e.prototype.autoPlayIterator = function () {
    var i = this,
        e = i.currentSlide + i.options.slidesToScroll;
    i.paused || i.interrupted || i.focussed || (!1 === i.options.infinite && (1 === i.direction && i.currentSlide + 1 === i.slideCount - 1 ? i.direction = 0 : 0 === i.direction && (e = i.currentSlide - i.options.slidesToScroll, i.currentSlide - 1 == 0 && (i.direction = 1))), i.slideHandler(e));
  }, e.prototype.buildArrows = function () {
    var e = this;
    !0 === e.options.arrows && (e.$prevArrow = i(e.options.prevArrow).addClass("slick-arrow"), e.$nextArrow = i(e.options.nextArrow).addClass("slick-arrow"), e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows), e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows), !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
      "aria-disabled": "true",
      tabindex: "-1"
    }));
  }, e.prototype.buildDots = function () {
    var e,
        t,
        o = this;

    if (!0 === o.options.dots) {
      for (o.$slider.addClass("slick-dotted"), t = i("<ul />").addClass(o.options.dotsClass), e = 0; e <= o.getDotCount(); e += 1) t.append(i("<li />").append(o.options.customPaging.call(this, o, e)));

      o.$dots = t.appendTo(o.options.appendDots), o.$dots.find("li").first().addClass("slick-active");
    }
  }, e.prototype.buildOut = function () {
    var e = this;
    e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"), e.slideCount = e.$slides.length, e.$slides.each(function (e, t) {
      i(t).attr("data-slick-index", e).data("originalStyling", i(t).attr("style") || "");
    }), e.$slider.addClass("slick-slider"), e.$slideTrack = 0 === e.slideCount ? i('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(), e.$list = e.$slideTrack.wrap('<div class="slick-list"/>').parent(), e.$slideTrack.css("opacity", 0), !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1), i("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"), e.setupInfinite(), e.buildArrows(), e.buildDots(), e.updateDots(), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), !0 === e.options.draggable && e.$list.addClass("draggable");
  }, e.prototype.buildRows = function () {
    var i,
        e,
        t,
        o,
        s,
        n,
        r,
        l = this;

    if (o = document.createDocumentFragment(), n = l.$slider.children(), l.options.rows > 1) {
      for (r = l.options.slidesPerRow * l.options.rows, s = Math.ceil(n.length / r), i = 0; i < s; i++) {
        var d = document.createElement("div");

        for (e = 0; e < l.options.rows; e++) {
          var a = document.createElement("div");

          for (t = 0; t < l.options.slidesPerRow; t++) {
            var c = i * r + (e * l.options.slidesPerRow + t);
            n.get(c) && a.appendChild(n.get(c));
          }

          d.appendChild(a);
        }

        o.appendChild(d);
      }

      l.$slider.empty().append(o), l.$slider.children().children().children().css({
        width: 100 / l.options.slidesPerRow + "%",
        display: "inline-block"
      });
    }
  }, e.prototype.checkResponsive = function (e, t) {
    var o,
        s,
        n,
        r = this,
        l = !1,
        d = r.$slider.width(),
        a = window.innerWidth || i(window).width();

    if ("window" === r.respondTo ? n = a : "slider" === r.respondTo ? n = d : "min" === r.respondTo && (n = Math.min(a, d)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
      s = null;

      for (o in r.breakpoints) r.breakpoints.hasOwnProperty(o) && (!1 === r.originalSettings.mobileFirst ? n < r.breakpoints[o] && (s = r.breakpoints[o]) : n > r.breakpoints[o] && (s = r.breakpoints[o]));

      null !== s ? null !== r.activeBreakpoint ? (s !== r.activeBreakpoint || t) && (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : (r.activeBreakpoint = s, "unslick" === r.breakpointSettings[s] ? r.unslick(s) : (r.options = i.extend({}, r.originalSettings, r.breakpointSettings[s]), !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e)), l = s) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, !0 === e && (r.currentSlide = r.options.initialSlide), r.refresh(e), l = s), e || !1 === l || r.$slider.trigger("breakpoint", [r, l]);
    }
  }, e.prototype.changeSlide = function (e, t) {
    var o,
        s,
        n,
        r = this,
        l = i(e.currentTarget);

    switch (l.is("a") && e.preventDefault(), l.is("li") || (l = l.closest("li")), n = r.slideCount % r.options.slidesToScroll != 0, o = n ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, e.data.message) {
      case "previous":
        s = 0 === o ? r.options.slidesToScroll : r.options.slidesToShow - o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - s, !1, t);
        break;

      case "next":
        s = 0 === o ? r.options.slidesToScroll : o, r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + s, !1, t);
        break;

      case "index":
        var d = 0 === e.data.index ? 0 : e.data.index || l.index() * r.options.slidesToScroll;
        r.slideHandler(r.checkNavigable(d), !1, t), l.children().trigger("focus");
        break;

      default:
        return;
    }
  }, e.prototype.checkNavigable = function (i) {
    var e, t;
    if (e = this.getNavigableIndexes(), t = 0, i > e[e.length - 1]) i = e[e.length - 1];else for (var o in e) {
      if (i < e[o]) {
        i = t;
        break;
      }

      t = e[o];
    }
    return i;
  }, e.prototype.cleanUpEvents = function () {
    var e = this;
    e.options.dots && null !== e.$dots && (i("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", i.proxy(e.interrupt, e, !0)).off("mouseleave.slick", i.proxy(e.interrupt, e, !1)), !0 === e.options.accessibility && e.$dots.off("keydown.slick", e.keyHandler)), e.$slider.off("focus.slick blur.slick"), !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide), e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide), !0 === e.options.accessibility && (e.$prevArrow && e.$prevArrow.off("keydown.slick", e.keyHandler), e.$nextArrow && e.$nextArrow.off("keydown.slick", e.keyHandler))), e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler), e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler), e.$list.off("touchend.slick mouseup.slick", e.swipeHandler), e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler), e.$list.off("click.slick", e.clickHandler), i(document).off(e.visibilityChange, e.visibility), e.cleanUpSlideEvents(), !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().off("click.slick", e.selectHandler), i(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange), i(window).off("resize.slick.slick-" + e.instanceUid, e.resize), i("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault), i(window).off("load.slick.slick-" + e.instanceUid, e.setPosition);
  }, e.prototype.cleanUpSlideEvents = function () {
    var e = this;
    e.$list.off("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.off("mouseleave.slick", i.proxy(e.interrupt, e, !1));
  }, e.prototype.cleanUpRows = function () {
    var i,
        e = this;
    e.options.rows > 1 && ((i = e.$slides.children().children()).removeAttr("style"), e.$slider.empty().append(i));
  }, e.prototype.clickHandler = function (i) {
    !1 === this.shouldClick && (i.stopImmediatePropagation(), i.stopPropagation(), i.preventDefault());
  }, e.prototype.destroy = function (e) {
    var t = this;
    t.autoPlayClear(), t.touchObject = {}, t.cleanUpEvents(), i(".slick-cloned", t.$slider).detach(), t.$dots && t.$dots.remove(), t.$prevArrow && t.$prevArrow.length && (t.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove()), t.$nextArrow && t.$nextArrow.length && (t.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove()), t.$slides && (t.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function () {
      i(this).attr("style", i(this).data("originalStyling"));
    }), t.$slideTrack.children(this.options.slide).detach(), t.$slideTrack.detach(), t.$list.detach(), t.$slider.append(t.$slides)), t.cleanUpRows(), t.$slider.removeClass("slick-slider"), t.$slider.removeClass("slick-initialized"), t.$slider.removeClass("slick-dotted"), t.unslicked = !0, e || t.$slider.trigger("destroy", [t]);
  }, e.prototype.disableTransition = function (i) {
    var e = this,
        t = {};
    t[e.transitionType] = "", !1 === e.options.fade ? e.$slideTrack.css(t) : e.$slides.eq(i).css(t);
  }, e.prototype.fadeSlide = function (i, e) {
    var t = this;
    !1 === t.cssTransitions ? (t.$slides.eq(i).css({
      zIndex: t.options.zIndex
    }), t.$slides.eq(i).animate({
      opacity: 1
    }, t.options.speed, t.options.easing, e)) : (t.applyTransition(i), t.$slides.eq(i).css({
      opacity: 1,
      zIndex: t.options.zIndex
    }), e && setTimeout(function () {
      t.disableTransition(i), e.call();
    }, t.options.speed));
  }, e.prototype.fadeSlideOut = function (i) {
    var e = this;
    !1 === e.cssTransitions ? e.$slides.eq(i).animate({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }, e.options.speed, e.options.easing) : (e.applyTransition(i), e.$slides.eq(i).css({
      opacity: 0,
      zIndex: e.options.zIndex - 2
    }));
  }, e.prototype.filterSlides = e.prototype.slickFilter = function (i) {
    var e = this;
    null !== i && (e.$slidesCache = e.$slides, e.unload(), e.$slideTrack.children(this.options.slide).detach(), e.$slidesCache.filter(i).appendTo(e.$slideTrack), e.reinit());
  }, e.prototype.focusHandler = function () {
    var e = this;
    e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*", function (t) {
      t.stopImmediatePropagation();
      var o = i(this);
      setTimeout(function () {
        e.options.pauseOnFocus && (e.focussed = o.is(":focus"), e.autoPlay());
      }, 0);
    });
  }, e.prototype.getCurrent = e.prototype.slickCurrentSlide = function () {
    return this.currentSlide;
  }, e.prototype.getDotCount = function () {
    var i = this,
        e = 0,
        t = 0,
        o = 0;
    if (!0 === i.options.infinite) {
      if (i.slideCount <= i.options.slidesToShow) ++o;else for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;
    } else if (!0 === i.options.centerMode) o = i.slideCount;else if (i.options.asNavFor) for (; e < i.slideCount;) ++o, e = t + i.options.slidesToScroll, t += i.options.slidesToScroll <= i.options.slidesToShow ? i.options.slidesToScroll : i.options.slidesToShow;else o = 1 + Math.ceil((i.slideCount - i.options.slidesToShow) / i.options.slidesToScroll);
    return o - 1;
  }, e.prototype.getLeft = function (i) {
    var e,
        t,
        o,
        s,
        n = this,
        r = 0;
    return n.slideOffset = 0, t = n.$slides.first().outerHeight(!0), !0 === n.options.infinite ? (n.slideCount > n.options.slidesToShow && (n.slideOffset = n.slideWidth * n.options.slidesToShow * -1, s = -1, !0 === n.options.vertical && !0 === n.options.centerMode && (2 === n.options.slidesToShow ? s = -1.5 : 1 === n.options.slidesToShow && (s = -2)), r = t * n.options.slidesToShow * s), n.slideCount % n.options.slidesToScroll != 0 && i + n.options.slidesToScroll > n.slideCount && n.slideCount > n.options.slidesToShow && (i > n.slideCount ? (n.slideOffset = (n.options.slidesToShow - (i - n.slideCount)) * n.slideWidth * -1, r = (n.options.slidesToShow - (i - n.slideCount)) * t * -1) : (n.slideOffset = n.slideCount % n.options.slidesToScroll * n.slideWidth * -1, r = n.slideCount % n.options.slidesToScroll * t * -1))) : i + n.options.slidesToShow > n.slideCount && (n.slideOffset = (i + n.options.slidesToShow - n.slideCount) * n.slideWidth, r = (i + n.options.slidesToShow - n.slideCount) * t), n.slideCount <= n.options.slidesToShow && (n.slideOffset = 0, r = 0), !0 === n.options.centerMode && n.slideCount <= n.options.slidesToShow ? n.slideOffset = n.slideWidth * Math.floor(n.options.slidesToShow) / 2 - n.slideWidth * n.slideCount / 2 : !0 === n.options.centerMode && !0 === n.options.infinite ? n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2) - n.slideWidth : !0 === n.options.centerMode && (n.slideOffset = 0, n.slideOffset += n.slideWidth * Math.floor(n.options.slidesToShow / 2)), e = !1 === n.options.vertical ? i * n.slideWidth * -1 + n.slideOffset : i * t * -1 + r, !0 === n.options.variableWidth && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, !0 === n.options.centerMode && (o = n.slideCount <= n.options.slidesToShow || !1 === n.options.infinite ? n.$slideTrack.children(".slick-slide").eq(i) : n.$slideTrack.children(".slick-slide").eq(i + n.options.slidesToShow + 1), e = !0 === n.options.rtl ? o[0] ? -1 * (n.$slideTrack.width() - o[0].offsetLeft - o.width()) : 0 : o[0] ? -1 * o[0].offsetLeft : 0, e += (n.$list.width() - o.outerWidth()) / 2)), e;
  }, e.prototype.getOption = e.prototype.slickGetOption = function (i) {
    return this.options[i];
  }, e.prototype.getNavigableIndexes = function () {
    var i,
        e = this,
        t = 0,
        o = 0,
        s = [];

    for (!1 === e.options.infinite ? i = e.slideCount : (t = -1 * e.options.slidesToScroll, o = -1 * e.options.slidesToScroll, i = 2 * e.slideCount); t < i;) s.push(t), t = o + e.options.slidesToScroll, o += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;

    return s;
  }, e.prototype.getSlick = function () {
    return this;
  }, e.prototype.getSlideCount = function () {
    var e,
        t,
        o = this;
    return t = !0 === o.options.centerMode ? o.slideWidth * Math.floor(o.options.slidesToShow / 2) : 0, !0 === o.options.swipeToSlide ? (o.$slideTrack.find(".slick-slide").each(function (s, n) {
      if (n.offsetLeft - t + i(n).outerWidth() / 2 > -1 * o.swipeLeft) return e = n, !1;
    }), Math.abs(i(e).attr("data-slick-index") - o.currentSlide) || 1) : o.options.slidesToScroll;
  }, e.prototype.goTo = e.prototype.slickGoTo = function (i, e) {
    this.changeSlide({
      data: {
        message: "index",
        index: parseInt(i)
      }
    }, e);
  }, e.prototype.init = function (e) {
    var t = this;
    i(t.$slider).hasClass("slick-initialized") || (i(t.$slider).addClass("slick-initialized"), t.buildRows(), t.buildOut(), t.setProps(), t.startLoad(), t.loadSlider(), t.initializeEvents(), t.updateArrows(), t.updateDots(), t.checkResponsive(!0), t.focusHandler()), e && t.$slider.trigger("init", [t]), !0 === t.options.accessibility && t.initADA(), t.options.autoplay && (t.paused = !1, t.autoPlay());
  }, e.prototype.initADA = function () {
    var e = this,
        t = Math.ceil(e.slideCount / e.options.slidesToShow),
        o = e.getNavigableIndexes().filter(function (i) {
      return i >= 0 && i < e.slideCount;
    });
    e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
      "aria-hidden": "true",
      tabindex: "-1"
    }).find("a, input, button, select").attr({
      tabindex: "-1"
    }), null !== e.$dots && (e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function (t) {
      var s = o.indexOf(t);
      i(this).attr({
        role: "tabpanel",
        id: "slick-slide" + e.instanceUid + t,
        tabindex: -1
      }), -1 !== s && i(this).attr({
        "aria-describedby": "slick-slide-control" + e.instanceUid + s
      });
    }), e.$dots.attr("role", "tablist").find("li").each(function (s) {
      var n = o[s];
      i(this).attr({
        role: "presentation"
      }), i(this).find("button").first().attr({
        role: "tab",
        id: "slick-slide-control" + e.instanceUid + s,
        "aria-controls": "slick-slide" + e.instanceUid + n,
        "aria-label": s + 1 + " of " + t,
        "aria-selected": null,
        tabindex: "-1"
      });
    }).eq(e.currentSlide).find("button").attr({
      "aria-selected": "true",
      tabindex: "0"
    }).end());

    for (var s = e.currentSlide, n = s + e.options.slidesToShow; s < n; s++) e.$slides.eq(s).attr("tabindex", 0);

    e.activateADA();
  }, e.prototype.initArrowEvents = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.off("click.slick").on("click.slick", {
      message: "previous"
    }, i.changeSlide), i.$nextArrow.off("click.slick").on("click.slick", {
      message: "next"
    }, i.changeSlide), !0 === i.options.accessibility && (i.$prevArrow.on("keydown.slick", i.keyHandler), i.$nextArrow.on("keydown.slick", i.keyHandler)));
  }, e.prototype.initDotEvents = function () {
    var e = this;
    !0 === e.options.dots && (i("li", e.$dots).on("click.slick", {
      message: "index"
    }, e.changeSlide), !0 === e.options.accessibility && e.$dots.on("keydown.slick", e.keyHandler)), !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && i("li", e.$dots).on("mouseenter.slick", i.proxy(e.interrupt, e, !0)).on("mouseleave.slick", i.proxy(e.interrupt, e, !1));
  }, e.prototype.initSlideEvents = function () {
    var e = this;
    e.options.pauseOnHover && (e.$list.on("mouseenter.slick", i.proxy(e.interrupt, e, !0)), e.$list.on("mouseleave.slick", i.proxy(e.interrupt, e, !1)));
  }, e.prototype.initializeEvents = function () {
    var e = this;
    e.initArrowEvents(), e.initDotEvents(), e.initSlideEvents(), e.$list.on("touchstart.slick mousedown.slick", {
      action: "start"
    }, e.swipeHandler), e.$list.on("touchmove.slick mousemove.slick", {
      action: "move"
    }, e.swipeHandler), e.$list.on("touchend.slick mouseup.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("touchcancel.slick mouseleave.slick", {
      action: "end"
    }, e.swipeHandler), e.$list.on("click.slick", e.clickHandler), i(document).on(e.visibilityChange, i.proxy(e.visibility, e)), !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), i(window).on("orientationchange.slick.slick-" + e.instanceUid, i.proxy(e.orientationChange, e)), i(window).on("resize.slick.slick-" + e.instanceUid, i.proxy(e.resize, e)), i("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault), i(window).on("load.slick.slick-" + e.instanceUid, e.setPosition), i(e.setPosition);
  }, e.prototype.initUI = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.show(), i.$nextArrow.show()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.show();
  }, e.prototype.keyHandler = function (i) {
    var e = this;
    i.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === i.keyCode && !0 === e.options.accessibility ? e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "next" : "previous"
      }
    }) : 39 === i.keyCode && !0 === e.options.accessibility && e.changeSlide({
      data: {
        message: !0 === e.options.rtl ? "previous" : "next"
      }
    }));
  }, e.prototype.lazyLoad = function () {
    function e(e) {
      i("img[data-lazy]", e).each(function () {
        var e = i(this),
            t = i(this).attr("data-lazy"),
            o = i(this).attr("data-srcset"),
            s = i(this).attr("data-sizes") || n.$slider.attr("data-sizes"),
            r = document.createElement("img");
        r.onload = function () {
          e.animate({
            opacity: 0
          }, 100, function () {
            o && (e.attr("srcset", o), s && e.attr("sizes", s)), e.attr("src", t).animate({
              opacity: 1
            }, 200, function () {
              e.removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading");
            }), n.$slider.trigger("lazyLoaded", [n, e, t]);
          });
        }, r.onerror = function () {
          e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), n.$slider.trigger("lazyLoadError", [n, e, t]);
        }, r.src = t;
      });
    }

    var t,
        o,
        s,
        n = this;
    if (!0 === n.options.centerMode ? !0 === n.options.infinite ? s = (o = n.currentSlide + (n.options.slidesToShow / 2 + 1)) + n.options.slidesToShow + 2 : (o = Math.max(0, n.currentSlide - (n.options.slidesToShow / 2 + 1)), s = n.options.slidesToShow / 2 + 1 + 2 + n.currentSlide) : (o = n.options.infinite ? n.options.slidesToShow + n.currentSlide : n.currentSlide, s = Math.ceil(o + n.options.slidesToShow), !0 === n.options.fade && (o > 0 && o--, s <= n.slideCount && s++)), t = n.$slider.find(".slick-slide").slice(o, s), "anticipated" === n.options.lazyLoad) for (var r = o - 1, l = s, d = n.$slider.find(".slick-slide"), a = 0; a < n.options.slidesToScroll; a++) r < 0 && (r = n.slideCount - 1), t = (t = t.add(d.eq(r))).add(d.eq(l)), r--, l++;
    e(t), n.slideCount <= n.options.slidesToShow ? e(n.$slider.find(".slick-slide")) : n.currentSlide >= n.slideCount - n.options.slidesToShow ? e(n.$slider.find(".slick-cloned").slice(0, n.options.slidesToShow)) : 0 === n.currentSlide && e(n.$slider.find(".slick-cloned").slice(-1 * n.options.slidesToShow));
  }, e.prototype.loadSlider = function () {
    var i = this;
    i.setPosition(), i.$slideTrack.css({
      opacity: 1
    }), i.$slider.removeClass("slick-loading"), i.initUI(), "progressive" === i.options.lazyLoad && i.progressiveLazyLoad();
  }, e.prototype.next = e.prototype.slickNext = function () {
    this.changeSlide({
      data: {
        message: "next"
      }
    });
  }, e.prototype.orientationChange = function () {
    var i = this;
    i.checkResponsive(), i.setPosition();
  }, e.prototype.pause = e.prototype.slickPause = function () {
    var i = this;
    i.autoPlayClear(), i.paused = !0;
  }, e.prototype.play = e.prototype.slickPlay = function () {
    var i = this;
    i.autoPlay(), i.options.autoplay = !0, i.paused = !1, i.focussed = !1, i.interrupted = !1;
  }, e.prototype.postSlide = function (e) {
    var t = this;
    t.unslicked || (t.$slider.trigger("afterChange", [t, e]), t.animating = !1, t.slideCount > t.options.slidesToShow && t.setPosition(), t.swipeLeft = null, t.options.autoplay && t.autoPlay(), !0 === t.options.accessibility && (t.initADA(), t.options.focusOnChange && i(t.$slides.get(t.currentSlide)).attr("tabindex", 0).focus()));
  }, e.prototype.prev = e.prototype.slickPrev = function () {
    this.changeSlide({
      data: {
        message: "previous"
      }
    });
  }, e.prototype.preventDefault = function (i) {
    i.preventDefault();
  }, e.prototype.progressiveLazyLoad = function (e) {
    e = e || 1;
    var t,
        o,
        s,
        n,
        r,
        l = this,
        d = i("img[data-lazy]", l.$slider);
    d.length ? (t = d.first(), o = t.attr("data-lazy"), s = t.attr("data-srcset"), n = t.attr("data-sizes") || l.$slider.attr("data-sizes"), (r = document.createElement("img")).onload = function () {
      s && (t.attr("srcset", s), n && t.attr("sizes", n)), t.attr("src", o).removeAttr("data-lazy data-srcset data-sizes").removeClass("slick-loading"), !0 === l.options.adaptiveHeight && l.setPosition(), l.$slider.trigger("lazyLoaded", [l, t, o]), l.progressiveLazyLoad();
    }, r.onerror = function () {
      e < 3 ? setTimeout(function () {
        l.progressiveLazyLoad(e + 1);
      }, 500) : (t.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"), l.$slider.trigger("lazyLoadError", [l, t, o]), l.progressiveLazyLoad());
    }, r.src = o) : l.$slider.trigger("allImagesLoaded", [l]);
  }, e.prototype.refresh = function (e) {
    var t,
        o,
        s = this;
    o = s.slideCount - s.options.slidesToShow, !s.options.infinite && s.currentSlide > o && (s.currentSlide = o), s.slideCount <= s.options.slidesToShow && (s.currentSlide = 0), t = s.currentSlide, s.destroy(!0), i.extend(s, s.initials, {
      currentSlide: t
    }), s.init(), e || s.changeSlide({
      data: {
        message: "index",
        index: t
      }
    }, !1);
  }, e.prototype.registerBreakpoints = function () {
    var e,
        t,
        o,
        s = this,
        n = s.options.responsive || null;

    if ("array" === i.type(n) && n.length) {
      s.respondTo = s.options.respondTo || "window";

      for (e in n) if (o = s.breakpoints.length - 1, n.hasOwnProperty(e)) {
        for (t = n[e].breakpoint; o >= 0;) s.breakpoints[o] && s.breakpoints[o] === t && s.breakpoints.splice(o, 1), o--;

        s.breakpoints.push(t), s.breakpointSettings[t] = n[e].settings;
      }

      s.breakpoints.sort(function (i, e) {
        return s.options.mobileFirst ? i - e : e - i;
      });
    }
  }, e.prototype.reinit = function () {
    var e = this;
    e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"), e.slideCount = e.$slides.length, e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll), e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0), e.registerBreakpoints(), e.setProps(), e.setupInfinite(), e.buildArrows(), e.updateArrows(), e.initArrowEvents(), e.buildDots(), e.updateDots(), e.initDotEvents(), e.cleanUpSlideEvents(), e.initSlideEvents(), e.checkResponsive(!1, !0), !0 === e.options.focusOnSelect && i(e.$slideTrack).children().on("click.slick", e.selectHandler), e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0), e.setPosition(), e.focusHandler(), e.paused = !e.options.autoplay, e.autoPlay(), e.$slider.trigger("reInit", [e]);
  }, e.prototype.resize = function () {
    var e = this;
    i(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay), e.windowDelay = window.setTimeout(function () {
      e.windowWidth = i(window).width(), e.checkResponsive(), e.unslicked || e.setPosition();
    }, 50));
  }, e.prototype.removeSlide = e.prototype.slickRemove = function (i, e, t) {
    var o = this;
    if (i = "boolean" == typeof i ? !0 === (e = i) ? 0 : o.slideCount - 1 : !0 === e ? --i : i, o.slideCount < 1 || i < 0 || i > o.slideCount - 1) return !1;
    o.unload(), !0 === t ? o.$slideTrack.children().remove() : o.$slideTrack.children(this.options.slide).eq(i).remove(), o.$slides = o.$slideTrack.children(this.options.slide), o.$slideTrack.children(this.options.slide).detach(), o.$slideTrack.append(o.$slides), o.$slidesCache = o.$slides, o.reinit();
  }, e.prototype.setCSS = function (i) {
    var e,
        t,
        o = this,
        s = {};
    !0 === o.options.rtl && (i = -i), e = "left" == o.positionProp ? Math.ceil(i) + "px" : "0px", t = "top" == o.positionProp ? Math.ceil(i) + "px" : "0px", s[o.positionProp] = i, !1 === o.transformsEnabled ? o.$slideTrack.css(s) : (s = {}, !1 === o.cssTransitions ? (s[o.animType] = "translate(" + e + ", " + t + ")", o.$slideTrack.css(s)) : (s[o.animType] = "translate3d(" + e + ", " + t + ", 0px)", o.$slideTrack.css(s)));
  }, e.prototype.setDimensions = function () {
    var i = this;
    !1 === i.options.vertical ? !0 === i.options.centerMode && i.$list.css({
      padding: "0px " + i.options.centerPadding
    }) : (i.$list.height(i.$slides.first().outerHeight(!0) * i.options.slidesToShow), !0 === i.options.centerMode && i.$list.css({
      padding: i.options.centerPadding + " 0px"
    })), i.listWidth = i.$list.width(), i.listHeight = i.$list.height(), !1 === i.options.vertical && !1 === i.options.variableWidth ? (i.slideWidth = Math.ceil(i.listWidth / i.options.slidesToShow), i.$slideTrack.width(Math.ceil(i.slideWidth * i.$slideTrack.children(".slick-slide").length))) : !0 === i.options.variableWidth ? i.$slideTrack.width(5e3 * i.slideCount) : (i.slideWidth = Math.ceil(i.listWidth), i.$slideTrack.height(Math.ceil(i.$slides.first().outerHeight(!0) * i.$slideTrack.children(".slick-slide").length)));
    var e = i.$slides.first().outerWidth(!0) - i.$slides.first().width();
    !1 === i.options.variableWidth && i.$slideTrack.children(".slick-slide").width(i.slideWidth - e);
  }, e.prototype.setFade = function () {
    var e,
        t = this;
    t.$slides.each(function (o, s) {
      e = t.slideWidth * o * -1, !0 === t.options.rtl ? i(s).css({
        position: "relative",
        right: e,
        top: 0,
        zIndex: t.options.zIndex - 2,
        opacity: 0
      }) : i(s).css({
        position: "relative",
        left: e,
        top: 0,
        zIndex: t.options.zIndex - 2,
        opacity: 0
      });
    }), t.$slides.eq(t.currentSlide).css({
      zIndex: t.options.zIndex - 1,
      opacity: 1
    });
  }, e.prototype.setHeight = function () {
    var i = this;

    if (1 === i.options.slidesToShow && !0 === i.options.adaptiveHeight && !1 === i.options.vertical) {
      var e = i.$slides.eq(i.currentSlide).outerHeight(!0);
      i.$list.css("height", e);
    }
  }, e.prototype.setOption = e.prototype.slickSetOption = function () {
    var e,
        t,
        o,
        s,
        n,
        r = this,
        l = !1;
    if ("object" === i.type(arguments[0]) ? (o = arguments[0], l = arguments[1], n = "multiple") : "string" === i.type(arguments[0]) && (o = arguments[0], s = arguments[1], l = arguments[2], "responsive" === arguments[0] && "array" === i.type(arguments[1]) ? n = "responsive" : void 0 !== arguments[1] && (n = "single")), "single" === n) r.options[o] = s;else if ("multiple" === n) i.each(o, function (i, e) {
      r.options[i] = e;
    });else if ("responsive" === n) for (t in s) if ("array" !== i.type(r.options.responsive)) r.options.responsive = [s[t]];else {
      for (e = r.options.responsive.length - 1; e >= 0;) r.options.responsive[e].breakpoint === s[t].breakpoint && r.options.responsive.splice(e, 1), e--;

      r.options.responsive.push(s[t]);
    }
    l && (r.unload(), r.reinit());
  }, e.prototype.setPosition = function () {
    var i = this;
    i.setDimensions(), i.setHeight(), !1 === i.options.fade ? i.setCSS(i.getLeft(i.currentSlide)) : i.setFade(), i.$slider.trigger("setPosition", [i]);
  }, e.prototype.setProps = function () {
    var i = this,
        e = document.body.style;
    i.positionProp = !0 === i.options.vertical ? "top" : "left", "top" === i.positionProp ? i.$slider.addClass("slick-vertical") : i.$slider.removeClass("slick-vertical"), void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === i.options.useCSS && (i.cssTransitions = !0), i.options.fade && ("number" == typeof i.options.zIndex ? i.options.zIndex < 3 && (i.options.zIndex = 3) : i.options.zIndex = i.defaults.zIndex), void 0 !== e.OTransform && (i.animType = "OTransform", i.transformType = "-o-transform", i.transitionType = "OTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.MozTransform && (i.animType = "MozTransform", i.transformType = "-moz-transform", i.transitionType = "MozTransition", void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (i.animType = !1)), void 0 !== e.webkitTransform && (i.animType = "webkitTransform", i.transformType = "-webkit-transform", i.transitionType = "webkitTransition", void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (i.animType = !1)), void 0 !== e.msTransform && (i.animType = "msTransform", i.transformType = "-ms-transform", i.transitionType = "msTransition", void 0 === e.msTransform && (i.animType = !1)), void 0 !== e.transform && !1 !== i.animType && (i.animType = "transform", i.transformType = "transform", i.transitionType = "transition"), i.transformsEnabled = i.options.useTransform && null !== i.animType && !1 !== i.animType;
  }, e.prototype.setSlideClasses = function (i) {
    var e,
        t,
        o,
        s,
        n = this;

    if (t = n.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"), n.$slides.eq(i).addClass("slick-current"), !0 === n.options.centerMode) {
      var r = n.options.slidesToShow % 2 == 0 ? 1 : 0;
      e = Math.floor(n.options.slidesToShow / 2), !0 === n.options.infinite && (i >= e && i <= n.slideCount - 1 - e ? n.$slides.slice(i - e + r, i + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (o = n.options.slidesToShow + i, t.slice(o - e + 1 + r, o + e + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === i ? t.eq(t.length - 1 - n.options.slidesToShow).addClass("slick-center") : i === n.slideCount - 1 && t.eq(n.options.slidesToShow).addClass("slick-center")), n.$slides.eq(i).addClass("slick-center");
    } else i >= 0 && i <= n.slideCount - n.options.slidesToShow ? n.$slides.slice(i, i + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : t.length <= n.options.slidesToShow ? t.addClass("slick-active").attr("aria-hidden", "false") : (s = n.slideCount % n.options.slidesToShow, o = !0 === n.options.infinite ? n.options.slidesToShow + i : i, n.options.slidesToShow == n.options.slidesToScroll && n.slideCount - i < n.options.slidesToShow ? t.slice(o - (n.options.slidesToShow - s), o + s).addClass("slick-active").attr("aria-hidden", "false") : t.slice(o, o + n.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));

    "ondemand" !== n.options.lazyLoad && "anticipated" !== n.options.lazyLoad || n.lazyLoad();
  }, e.prototype.setupInfinite = function () {
    var e,
        t,
        o,
        s = this;

    if (!0 === s.options.fade && (s.options.centerMode = !1), !0 === s.options.infinite && !1 === s.options.fade && (t = null, s.slideCount > s.options.slidesToShow)) {
      for (o = !0 === s.options.centerMode ? s.options.slidesToShow + 1 : s.options.slidesToShow, e = s.slideCount; e > s.slideCount - o; e -= 1) t = e - 1, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t - s.slideCount).prependTo(s.$slideTrack).addClass("slick-cloned");

      for (e = 0; e < o + s.slideCount; e += 1) t = e, i(s.$slides[t]).clone(!0).attr("id", "").attr("data-slick-index", t + s.slideCount).appendTo(s.$slideTrack).addClass("slick-cloned");

      s.$slideTrack.find(".slick-cloned").find("[id]").each(function () {
        i(this).attr("id", "");
      });
    }
  }, e.prototype.interrupt = function (i) {
    var e = this;
    i || e.autoPlay(), e.interrupted = i;
  }, e.prototype.selectHandler = function (e) {
    var t = this,
        o = i(e.target).is(".slick-slide") ? i(e.target) : i(e.target).parents(".slick-slide"),
        s = parseInt(o.attr("data-slick-index"));
    s || (s = 0), t.slideCount <= t.options.slidesToShow ? t.slideHandler(s, !1, !0) : t.slideHandler(s);
  }, e.prototype.slideHandler = function (i, e, t) {
    var o,
        s,
        n,
        r,
        l,
        d = null,
        a = this;
    if (e = e || !1, !(!0 === a.animating && !0 === a.options.waitForAnimate || !0 === a.options.fade && a.currentSlide === i)) if (!1 === e && a.asNavFor(i), o = i, d = a.getLeft(o), r = a.getLeft(a.currentSlide), a.currentLeft = null === a.swipeLeft ? r : a.swipeLeft, !1 === a.options.infinite && !1 === a.options.centerMode && (i < 0 || i > a.getDotCount() * a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
      a.postSlide(o);
    }) : a.postSlide(o));else if (!1 === a.options.infinite && !0 === a.options.centerMode && (i < 0 || i > a.slideCount - a.options.slidesToScroll)) !1 === a.options.fade && (o = a.currentSlide, !0 !== t ? a.animateSlide(r, function () {
      a.postSlide(o);
    }) : a.postSlide(o));else {
      if (a.options.autoplay && clearInterval(a.autoPlayTimer), s = o < 0 ? a.slideCount % a.options.slidesToScroll != 0 ? a.slideCount - a.slideCount % a.options.slidesToScroll : a.slideCount + o : o >= a.slideCount ? a.slideCount % a.options.slidesToScroll != 0 ? 0 : o - a.slideCount : o, a.animating = !0, a.$slider.trigger("beforeChange", [a, a.currentSlide, s]), n = a.currentSlide, a.currentSlide = s, a.setSlideClasses(a.currentSlide), a.options.asNavFor && (l = (l = a.getNavTarget()).slick("getSlick")).slideCount <= l.options.slidesToShow && l.setSlideClasses(a.currentSlide), a.updateDots(), a.updateArrows(), !0 === a.options.fade) return !0 !== t ? (a.fadeSlideOut(n), a.fadeSlide(s, function () {
        a.postSlide(s);
      })) : a.postSlide(s), void a.animateHeight();
      !0 !== t ? a.animateSlide(d, function () {
        a.postSlide(s);
      }) : a.postSlide(s);
    }
  }, e.prototype.startLoad = function () {
    var i = this;
    !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && (i.$prevArrow.hide(), i.$nextArrow.hide()), !0 === i.options.dots && i.slideCount > i.options.slidesToShow && i.$dots.hide(), i.$slider.addClass("slick-loading");
  }, e.prototype.swipeDirection = function () {
    var i,
        e,
        t,
        o,
        s = this;
    return i = s.touchObject.startX - s.touchObject.curX, e = s.touchObject.startY - s.touchObject.curY, t = Math.atan2(e, i), (o = Math.round(180 * t / Math.PI)) < 0 && (o = 360 - Math.abs(o)), o <= 45 && o >= 0 ? !1 === s.options.rtl ? "left" : "right" : o <= 360 && o >= 315 ? !1 === s.options.rtl ? "left" : "right" : o >= 135 && o <= 225 ? !1 === s.options.rtl ? "right" : "left" : !0 === s.options.verticalSwiping ? o >= 35 && o <= 135 ? "down" : "up" : "vertical";
  }, e.prototype.swipeEnd = function (i) {
    var e,
        t,
        o = this;
    if (o.dragging = !1, o.swiping = !1, o.scrolling) return o.scrolling = !1, !1;
    if (o.interrupted = !1, o.shouldClick = !(o.touchObject.swipeLength > 10), void 0 === o.touchObject.curX) return !1;

    if (!0 === o.touchObject.edgeHit && o.$slider.trigger("edge", [o, o.swipeDirection()]), o.touchObject.swipeLength >= o.touchObject.minSwipe) {
      switch (t = o.swipeDirection()) {
        case "left":
        case "down":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide + o.getSlideCount()) : o.currentSlide + o.getSlideCount(), o.currentDirection = 0;
          break;

        case "right":
        case "up":
          e = o.options.swipeToSlide ? o.checkNavigable(o.currentSlide - o.getSlideCount()) : o.currentSlide - o.getSlideCount(), o.currentDirection = 1;
      }

      "vertical" != t && (o.slideHandler(e), o.touchObject = {}, o.$slider.trigger("swipe", [o, t]));
    } else o.touchObject.startX !== o.touchObject.curX && (o.slideHandler(o.currentSlide), o.touchObject = {});
  }, e.prototype.swipeHandler = function (i) {
    var e = this;
    if (!(!1 === e.options.swipe || "ontouchend" in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== i.type.indexOf("mouse"))) switch (e.touchObject.fingerCount = i.originalEvent && void 0 !== i.originalEvent.touches ? i.originalEvent.touches.length : 1, e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold, !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold), i.data.action) {
      case "start":
        e.swipeStart(i);
        break;

      case "move":
        e.swipeMove(i);
        break;

      case "end":
        e.swipeEnd(i);
    }
  }, e.prototype.swipeMove = function (i) {
    var e,
        t,
        o,
        s,
        n,
        r,
        l = this;
    return n = void 0 !== i.originalEvent ? i.originalEvent.touches : null, !(!l.dragging || l.scrolling || n && 1 !== n.length) && (e = l.getLeft(l.currentSlide), l.touchObject.curX = void 0 !== n ? n[0].pageX : i.clientX, l.touchObject.curY = void 0 !== n ? n[0].pageY : i.clientY, l.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(l.touchObject.curX - l.touchObject.startX, 2))), r = Math.round(Math.sqrt(Math.pow(l.touchObject.curY - l.touchObject.startY, 2))), !l.options.verticalSwiping && !l.swiping && r > 4 ? (l.scrolling = !0, !1) : (!0 === l.options.verticalSwiping && (l.touchObject.swipeLength = r), t = l.swipeDirection(), void 0 !== i.originalEvent && l.touchObject.swipeLength > 4 && (l.swiping = !0, i.preventDefault()), s = (!1 === l.options.rtl ? 1 : -1) * (l.touchObject.curX > l.touchObject.startX ? 1 : -1), !0 === l.options.verticalSwiping && (s = l.touchObject.curY > l.touchObject.startY ? 1 : -1), o = l.touchObject.swipeLength, l.touchObject.edgeHit = !1, !1 === l.options.infinite && (0 === l.currentSlide && "right" === t || l.currentSlide >= l.getDotCount() && "left" === t) && (o = l.touchObject.swipeLength * l.options.edgeFriction, l.touchObject.edgeHit = !0), !1 === l.options.vertical ? l.swipeLeft = e + o * s : l.swipeLeft = e + o * (l.$list.height() / l.listWidth) * s, !0 === l.options.verticalSwiping && (l.swipeLeft = e + o * s), !0 !== l.options.fade && !1 !== l.options.touchMove && (!0 === l.animating ? (l.swipeLeft = null, !1) : void l.setCSS(l.swipeLeft))));
  }, e.prototype.swipeStart = function (i) {
    var e,
        t = this;
    if (t.interrupted = !0, 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow) return t.touchObject = {}, !1;
    void 0 !== i.originalEvent && void 0 !== i.originalEvent.touches && (e = i.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== e ? e.pageX : i.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== e ? e.pageY : i.clientY, t.dragging = !0;
  }, e.prototype.unfilterSlides = e.prototype.slickUnfilter = function () {
    var i = this;
    null !== i.$slidesCache && (i.unload(), i.$slideTrack.children(this.options.slide).detach(), i.$slidesCache.appendTo(i.$slideTrack), i.reinit());
  }, e.prototype.unload = function () {
    var e = this;
    i(".slick-cloned", e.$slider).remove(), e.$dots && e.$dots.remove(), e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(), e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(), e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "");
  }, e.prototype.unslick = function (i) {
    var e = this;
    e.$slider.trigger("unslick", [e, i]), e.destroy();
  }, e.prototype.updateArrows = function () {
    var i = this;
    Math.floor(i.options.slidesToShow / 2), !0 === i.options.arrows && i.slideCount > i.options.slidesToShow && !i.options.infinite && (i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === i.currentSlide ? (i.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - i.options.slidesToShow && !1 === i.options.centerMode ? (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : i.currentSlide >= i.slideCount - 1 && !0 === i.options.centerMode && (i.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), i.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")));
  }, e.prototype.updateDots = function () {
    var i = this;
    null !== i.$dots && (i.$dots.find("li").removeClass("slick-active").end(), i.$dots.find("li").eq(Math.floor(i.currentSlide / i.options.slidesToScroll)).addClass("slick-active"));
  }, e.prototype.visibility = function () {
    var i = this;
    i.options.autoplay && (document[i.hidden] ? i.interrupted = !0 : i.interrupted = !1);
  }, i.fn.slick = function () {
    var i,
        t,
        o = this,
        s = arguments[0],
        n = Array.prototype.slice.call(arguments, 1),
        r = o.length;

    for (i = 0; i < r; i++) if ("object" == typeof s || void 0 === s ? o[i].slick = new e(o[i], s) : t = o[i].slick[s].apply(o[i].slick, n), void 0 !== t) return t;

    return o;
  };
});