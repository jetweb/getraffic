var swappableAreaWidth;
var swappableAreaHeight;
var lastUpdated = new Date();
$(document).ready(fixAdditionalVariationImages);

function is_mobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    } else {
        return false;
    }
}

function fixAdditionalVariationImages() {
    $('form.variations_form').on('wc_additional_variation_images_frontend_init', function () {
        $('form.variations_form').off('reset_image');
        $('form.variations_form').off('wc_additional_variation_images_frontend_image_swap_callback');
        $('form.variations_form').on('wc_additional_variation_images_frontend_image_swap_callback', function (e, response) {
            if (!$(this).is(':visible')) {
                return;
            }

            // debounce buggy duplicates
            let diff = new Date().getTime() - lastUpdated.getTime();
            lastUpdated = new Date();
            if (diff < 800) {
                return;
            }

            // Quickview
            if ($(this).parents('.qv-modal').length) {
                let elem = $(this).closest('.modal');
                reloadVariationImagesForQuickview(elem, response);
            } else {
                // Product Page
                if (!wc_additional_variation_images_local.bwc) {
                    var parent = $(wc_additional_variation_images_local.main_images_class).parent();
                    let swappableArea = $('.woocommerce-product-gallery__image.flex-active-slide img');
                    swappableAreaWidth = swappableArea.outerWidth();
                    swappableAreaHeight = swappableArea.outerHeight();
                    let wishlistImage = $(wc_additional_variation_images_local.main_images_class).find('a.whishlist');
                    let badges = $(wc_additional_variation_images_local.main_images_class).find('.yith-wcbm-badge');

                    $(wc_additional_variation_images_local.main_images_class).remove();
                    let newImages = $(response.main_images);
                    let innerHtml = '';
                    newImages.find('.woocommerce-product-gallery__image').each(function() {
                        innerHtml += $(this)[0].outerHTML;
                        // console.log($(this)[0].outerHTML);

                        //this is zoom fix on load my Matat Dev
                        if (!is_mobile()) {
                            var options = IZ.options;
                            var first_img = ".woocommerce-product-gallery__wrapper .flex-active-slide img";

                            var old_value = "";
                            if ( event.newValue != old_value ) {
                                $(".zoomContainer").remove();
                                setTimeout( function() {
                                    $(first_img).first().image_zoom(options);
                                    restart_on_hover($(first_img).first());
                                }, 550);
                            }
                            old_value = event.newValue;

                            function restart_on_hover( elem ) {
                                elem.hover(function(){
                                    if ( $('.zoomContainer').length === 0 ) {
                                        $(this).image_zoom(IZ.options);
                                    }
                                });
                            };
                        }
                    });
                    newImages.children()[0].innerHTML = innerHtml;
                    $.when(parent.prepend(newImages)).then(function () {
                        parent.find('.wp-post-image').each(function () {
                            $(this).attr('data-src', $(this).data('large_image'));
                        });
                        $(wc_additional_variation_images_local.main_images_class).prepend(wishlistImage);
                        //$('.woocommerce-product-gallery__image').width(swappableAreaWidth).height(swappableAreaHeight);
                        //$('.woocommerce-product-gallery__wrapper').width(swappableAreaWidth).height(swappableAreaHeight);
                        setTimeout(function () {
                            $('.flex-viewport').height($('.woocommerce-product-gallery__image.flex-active-slide img').outerHeight());
                            $(wc_additional_variation_images_local.main_images_class).find('.flex-viewport').append(badges);
                        }, 500);
                    });
                } else {
                    $(wc_additional_variation_images_local.gallery_images_class).fadeOut(50, function () {
                        $(this).html(response.gallery_images).hide().fadeIn(100, function () {
                            $.wc_additional_variation_images_frontend.runLightBox();
                        });
                    });
                }
                //$.wc_additional_variation_images_frontend.initProductGallery();
            }
            $(document).trigger('additional_images_fixed');
        });
    });

    $(document).trigger('additional_images_fix_completed');
}

function reloadVariationImagesForQuickview(elem, response) {
    let galleryEl = elem.find('.qv-gallery-images');
    galleryEl.css('display', 'none !important');
    galleryEl.slick('unslick');
    galleryEl.find('.qv-gallery-image').remove();
    $(response.main_images).find('img').each(function () {
        galleryEl.append('<div class="qv-gallery-image"><img src="' + $(this).attr('src') + '" alt=""></div>');
        console.log($(this).attr('src'));
    });
    invokeQuickviewSlickGallery(galleryEl);
}
