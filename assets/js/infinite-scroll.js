let alreadyScrolledIntoView = false;
let infiniteScrollingActive = true;

function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    if ($(elem).length === 0) {
        return false;
    }
    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function infiniteScrolling() {
    $(window).scroll(function() {
        scrolledIntoView = isScrolledIntoView($('.infinite-loader-wrapper')[0]);
        if (!alreadyScrolledIntoView && scrolledIntoView && infiniteScrollingActive) {
            scrollToNextPage();
        }

        alreadyScrolledIntoView = scrolledIntoView;
    });
}

function scrollToNextPage() {
    let nextPage = ($('.products').data('page') + 1);
    $.ajax({
        type: 'get',
        url: location.protocol + '//' + location.host + location.pathname + '/page/' + nextPage + location.search,
        success: function (response) {
            let firstProduct = $(response).find('.product-wrapper').first().data('product-id');
            if ($('.product-wrapper[data-product-id="' + firstProduct + '"]').length > 0) {
                $('.infinite-loader').hide();
                infiniteScrollingActive = false;
            } else {
                $('.products').append($(response).find('.products').children());

                afterUpdateInfiniteScroll();
            }
        },
        error: function() {
            $('.infinite-loader').hide();
            infiniteScrollingActive = false;
        }
    });

    $('.products').data('page', nextPage);
}

function afterUpdateInfiniteScroll() {
    try {
        $('.lmp_products_loading').remove();
        initializeAjaxAddTocart();
        initHoverThumb();
        initQuantityInputs();

        fixAdditionalVariationImages();
        $('.variations_form').each(function () {
            if (!$(this).attr('initialized')) {
                $(this).wc_variation_form();
                $(this).attr('initialized', true);
            }
        });




        $.wc_additional_variation_images_frontend.init();
    } catch (Err) {
        console.log(Err);
    }
}
