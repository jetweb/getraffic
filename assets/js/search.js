jQuery(document).ready(function () {
    let $ = jQuery;
    $('.search-toggle, .x-wrapper').bind('click', function (e) {
        e.preventDefault();
        $('.search-popup-wrap').toggle();
        $('.search-popup-wrap input').focus();
    });

    $('.results-popup-wrapper').hide();

    $('input.search-value').keyup(debounce(
        function() {
            let value = $('input.search-value:visible').val();
            if (!$('input.search-value').is(':visible') || (value.length > 0 && value.length < 3)) {
                return;
            }
            if (value.length === 0) {
                $('.results-popup-wrapper').hide();
                $('.results-popup').html('');
                if ($(window).width() < 768) {
                    $('.search-popup-wrap').hide();
                }
                return;
            }
            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                    action: 'ajaxSearch',
                    s: value
                },
                success: function (response) {
                    if (response) {
                        $('.results-popup-wrapper').show();
                        $('.search-popup-wrap').show();
                        $('.results-popup').html(response);
                    } else {
                        $('.results-popup-wrapper').hide();
                    }
                },
            });
        }, 1000));

    $('input.search-value').keypress(function (e) {
        let value = $(this).val();
        if (e.which == 13) {
            location.href = '/shop/?s=' + value;

            e.preventDefault();
            return false;
        }
    });
    $('.search-icon').click(function (e) {
        let value = $(this).parent().find('input.search-value').val();
		//console.log('.search-icon');
        if (value.length>0) {
            location.href = '/shop/?s=' + value;

            e.preventDefault();
            return false;
        }
    });
});


function debounce(func, wait) {
    var timeout;
    return function() {
        clearTimeout(timeout);
        timeout = setTimeout(func, wait);
    };
}
