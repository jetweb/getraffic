jQuery(document).ready(function () {
    let $ = jQuery;
    $('input.show-shipping-fields').change(function() {
        console.log(this.checked);
        $(".myaccount_shipping_fields").toggle(this.checked);
    });

    $('.form-row-eye img').click(function(){
        if($(this).prevAll("input.woocommerce-Input:first").attr('type') == 'text'){
            $(this).prevAll("input.woocommerce-Input:first").attr('type', 'password');
            $(this).css('display', 'none');
            $(this).prev().css('display', 'block');
        }
        else{
            $(this).prevAll("input.woocommerce-Input:first").attr('type', 'text');
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');

        }
    });

    $(".tm-orders").click(function(){
        $(this).next().toggleClass("active");
        return false;
    });

    $('button.replace').click(function () {
        debugger;
        $('.orders-to-return').hide();
        $('.orders-to-return').slideDown();
        $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחליף');
    });
    $('button.return').click(function () {
    debugger;
        $('.orders-to-return').hide();
        $('.orders-to-return').slideDown();
        $('.returns-toptext').text('אנא בחרי את המוצר אותו תרצי להחזיר');
    });

    $('a.submit-return').bind('click', function () {
        let orderItems = [];
        $(this).parent().find('input:checked').each(function () {
            orderItems.push($(this).data('order-item-id'));
        });

        $.ajax({
            type: 'post',
            url: ajax_url,
            data: {
                action: 'returnItems',
                orderItems
            },
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (response) {
                $('.returns-top').slideUp();
                $('.returns-thanks').css('display', 'block');
            },
        });
        return false;
    });

    $('form.woocommerce-form-login input').on('validate change blur', function(e) {
        validateFormElement($(this), e.type);
    });
    $('form.woocommerce-form-login').submit(function() {
        let valid = true;
        $('form.woocommerce-form-login input').each(function() {
            if (!validateFormElement($(this))) {
                valid = false;
            }
        });

        if (!valid) {
            return false;
        }
    })
});
