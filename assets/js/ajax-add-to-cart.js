$(document).ready(function () {
    initializeAjaxAddTocart();
});

function initializeAjaxAddTocart() {
    if ($('.ajax-atc-disabled').length) {
        return;
    }
    $(document).on('added_to_cart', function(e) {
        console.log('product added to cart triggered');
    });
    $( document.body )
        .off( 'click', '.add_to_cart_button');
    $('.variations_form').on('wc_variation_form', function() {
        $(this).off('click', '.single_add_to_cart_button');
    });
    $(document).off('click', '.single_add_to_cart_button, .add_to_cart_button');
    $(document).on('click', '.single_add_to_cart_button, .add_to_cart_button', function (e) {
        e.preventDefault();

        if ($('.product-type-yith_bundle.product').length === 1) {

            $('.woo-variation-raw-select').each(function() {
                if ($(this).val() === '') {
                    alert('נא לבחור אפשרות מוצר לפני הוספה לסל הקניות');
                    wrong = true;
                    return false;
                }
            });

            return true;
        }

        let errorElem = $('.variation-error:visible');
        errorElem.text('');

        if ( $( this ).is('.disabled') ) {
            if ( $( this ).is('.wc-variation-is-unavailable') ) {
                errorElem.text( wc_add_to_cart_variation_params.i18n_unavailable_text );
            } else if ( $( this ).is('.wc-variation-selection-needed') ) {
                errorElem.text( wc_add_to_cart_variation_params.i18n_make_a_selection_text );
            }

            return false;
        }

        var $thisbutton = $(this),
            $form = $thisbutton.closest('form.cart'),
            id = $thisbutton.val(),
            product_qty = $form.find('input[name=quantity]').val() || $thisbutton.data('quantity') || 1,
            product_id = $form.find('input[name=product_id]').val() || id || $thisbutton.data('product_id'),
            variation_id = $form.find('input[name=variation_id]').val() || 0;

        performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, $form.find('input[name=cartItemToReplace]').val() || 0);

        return false;
    });
}

function performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, cartItemKey = 0) {
    var data = {
        action: 'woocommerce_ajax_add_to_cart',
        product_id: product_id,
        product_sku: '',
        quantity: product_qty,
        variation_id: variation_id,
        cartItemKey: cartItemKey
    };

    $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

    $.ajax({
        type: 'post',
        url: wc_add_to_cart_params.ajax_url,
        data: data,
        beforeSend: function () {
            $thisbutton.removeClass('added').addClass('loading');
        },
        complete: function () {
        },
        success: function (response) {
            if (response.error && response.product_url) {
                window.location = response.product_url;
                return;
            } else {
                $thisbutton.addClass('added').removeClass('loading');
                $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                bindMiniCartFunctions();
            }
        },
    });
}

function updateCartItemQuantity(quantity, itemKey) {
    let data = {
        action: 'woocommerce_ajax_update_quantity',
        quantity: quantity,
        cart_item_key: itemKey
    };

    $.ajax({
        type: 'post',
        url: wc_add_to_cart_params.ajax_url,
        data: data,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            for (fragment in response.fragments) {
                $(fragment).replaceWith(response.fragments[fragment]);
            }
            $(document.body).trigger('wc_fragments_refreshed', [response.fragments, response.cart_hash]);
            bindMiniCartFunctions();
        },
    });
}
