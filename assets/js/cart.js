$(document).ready(function () {
    initializeAttributeDropdowns();
    bindQuantityButtons();
    $(".samples").click(function () {
        $(".samples-wrapper").slideToggle("fast");
        $(this).toggleClass("active");
        return false;
    });
    $(document.body).off('wc_fragments_refreshed');
    $(document.body).on('wc_fragments_refreshed', function () {
        bindQuantityButtons();
        initializeAttributeDropdowns();
    });
    $(document.body).on('cart_page_refreshed', function () {
        bindQuantityButtons();
        initializeAttributeDropdowns();
    });
    let cartButton = $('button[name=update_cart]');
    cartButton.hide();
});

function initializeAttributeDropdowns() {
    $('a.clear-selection').off('click');
    $('a.clear-selection').bind('click', function () {
        $(this).closest('.cart-item-atts').find('select').each(function () {
            $(this).val('-1');
            $(this).trigger('change');
        });
        $(this).hide();
        return false;
    });
    $('a.update-variation').off('click');
    $('a.update-variation').bind('click', function () {
        let cartItemToReplace = $(this).parent().find('.cartItemToReplace').val();
        let variationId = $(this).parent().data('variation_id');
        let productId = $(this).parent().data('product_id');
        let quantity = $(this).parent().parent().find('.quantityHidden').val();
        performAjaxAddToCart(productId, quantity, variationId, $(this), cartItemToReplace);
        return false;
    });
    updateVariationDropdowns();
}

function initialVariationFilter(elem) {
    let productVariations = elem.data('product_variations');
    elem.children('select').each(function () {
        let attribute = $(this).data('attribute-name');
        // Check all options for availability against main variation array
        $(this).find('option:not([value="-1"])').each(function () {
            let found = false;
            productVariations.filter(v => {
                if (decodeURI(v.attributes['attribute_' + attribute]) === decodeURI($(this).val()) || $(this).val() === '-1') {
                    found = true;
                }
            });
            $(this).text($(this).text().replace(oosText, ''));
            if (!found) {
                $(this).text($(this).text() + oosText);
            }
        });
    });
}

function onVariationUpdated(elem) {
    let productVariations = elem.parent().data('product_variations');
    let updateVariation = elem.closest('.cart-item-atts').find('.update-variation');
    let otherAttr = elem.data('attribute-name');
    let otherValu = elem.val();
    let numberofSelects = 1;
    if (elem.siblings('select').length === 0) {
        elem.closest('.cart-item-atts').find('.clear-selection').hide();
        productVariations.filter(v => {
            if (decodeURI(v.attributes['attribute_' + otherAttr]) === decodeURI(otherValu)) {
                found = true;
                elem.parent().data('variation_id', v.variation_id);
            }
        });
    } else if (elem.siblings('select').length === 1) {
        numberofSelects = 2;
        if (otherValu !== '-1') {
            //elem.closest('.cart-item-atts').find('.clear-selection').show();
        }
        elem.siblings('select').each(function () {
            let attribute = $(this).data('attribute-name');
            $(this).find('option:not([value="-1"])').each(function () {
                let value = $(this).val();
                let found = false;
                productVariations.filter(v => {
                    if ((decodeURI(v.attributes['attribute_' + attribute]) === decodeURI(value) || value === '-1') &&
                        (decodeURI(v.attributes['attribute_' + otherAttr]) === decodeURI(otherValu) || otherValu === '-1')) {
                        found = true;
                        if ($(this).is(':selected') && (value !== '-1') && (otherValu !== '-1')) {
                            elem.parent().data('variation_id', v.variation_id);
                        }
                    }
                });
                $(this).text($(this).text().replace(oosText, ''));
                if (!found) {
                    $(this).text($(this).text() + oosText);
                }
            });
        });
    } else {
        // not necessary ATM...
    }
    let isOptionViable = true;
    elem.closest('.cart-item-atts').find('select option:selected').each(function () {
        if ($(this).val() === '-1' || $(this).text().indexOf(oosText) !== -1) {
            isOptionViable = false;
        }
    });
    if (isOptionViable && (elem.parent().data('variation_id') !== elem.parent().data('original_variation_id'))) {
        if (numberofSelects === 1) {
            updateVariation.trigger('click');
        } else {
            updateVariation.css('display', 'block');
        }
    } else {
        updateVariation.hide();
    }
}

function updateVariationDropdowns() {
    $('.cart-item-atts').each(function () {
        initialVariationFilter($(this));
    });
    let selects = $('.cart-item-atts select');
    selects.each(function () {
        onVariationUpdated($(this));
        if (!$(this).attr('alreadyInitialized')) {
            $(this).change(function () {
                onVariationUpdated($(this));
            });
            $(this).attr('alreadyInitialized', true);
        }
    });
}

function bindQuantityButtons() {
    $('.q_arrow_up').bind('click', function () {
        let closestQuantity = $(this).parent().find('input.quantityHidden');
        let newValue = parseInt(closestQuantity.val()) + 1;
        changeQuantity(newValue, closestQuantity);
    });
    $('.q_arrow_down').bind('click', function () {
        let closestQuantity = $(this).parent().find('input.quantityHidden');
        let newValue = parseInt(closestQuantity.val()) - 1;
        if (newValue > 0) {
            changeQuantity(newValue, closestQuantity);
        }
    });
}

function changeQuantity(newQuantity, closestQuantity) {
    let $ = jQuery;
    closestQuantity.val(newQuantity);
    $('button[name=update_cart]').removeAttr('disabled');
    $('button[name=update_cart]').trigger('click');
}

function addCartItemToWishlist(elem) {
    addToWishlist(elem, function () {
        elem.addClass('hidden');
        elem.siblings('.wishlist-icon').removeClass('hidden')
    });
    return false;
}

