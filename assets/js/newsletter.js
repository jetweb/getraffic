jQuery(document).ready(function () {
    $('form.newsletter-form').bind('submit', function () {
        return submitNewsletter($(this));
    });
});


function submitNewsletter(form) {
    let response = form.find('.newsletter-response');
    response.removeClass('error');
    response.text('');
    let email = form.find("#email.newsletter").val();
    if (!validateEmail(email)) {
        response.text('יש להזין כתובת דואל תקינה');
        response.addClass('error');
        return false;
    }

    let approve = form.find('.newsletter-approve');
    if (approve.length && !approve.is(':checked')) {
        response.text('יש להסכים לתנאי השימוש');
        response.addClass('error');
        return false;
    }

    var data = {
        action: 'join-newsletter',
        email: email
    };

    $.ajax({
        type: 'post',
        url: ajax_url,
        data: data,
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (res) {
            if (!res) {
                response.text('חלה שגיאה בהרשמה לניוזלטר');
            } else {
                if (res.error) {
                    response.text(res.error);
                    response.addClass('error');
                    return false;
                }
                response.text('נרשמת בהצלחה!');
            }
        },
    });

    return false;
}
