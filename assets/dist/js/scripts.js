window.$ = window.jQuery || {};
window.slickInitializers = [];

function initSlickCarousel(element, showLeftArrow = true, showRightArrow = true, numberOfSlides = 4, $responsive = [], dots = false) {
  let prevArrow;
  let nextArrow;
  prevArrow = element.parent().find('.images-nav-back');

  if (prevArrow.length === 0) {
    prevArrow = $('<a href=\"javascript:void(0);\" class=\"images-nav-back\">&nbsp;</a>');
    element.before(prevArrow);
  }

  nextArrow = element.parent().find('.images-nav-next');

  if (nextArrow.length === 0) {
    nextArrow = $('<a href=\"javascript:void(0);\" class=\"images-nav-next\">&nbsp;</a>');
    element.before(nextArrow);
  }

  if (!showLeftArrow) {
    prevArrow.css('visibility', 'hidden');
    prevArrow.css('display', 'none !important');
  }

  if (!showRightArrow) {
    nextArrow.css('visibility', 'hidden');
    nextArrow.css('display', 'none !important');
  }

  if (!$responsive) {
    $responsive = [{
      breakpoint: 3220,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 2020,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 1400,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 100,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }];
  }

  let settings = {
    infinite: true,
    rtl: true,
    slidesToShow: numberOfSlides,
    slidesToScroll: 1,
    nextArrow,
    prevArrow,
    mobileFirst: true,
    dots: dots,
    responsive: $responsive
  };
  element.slick(settings);
}

function setMobileMenuHeight() {
  $('#menu-mobile').css('height', $(window).height() - 100);
}

function logUserIn(username, password, security, remember = true, redirect = false) {
  if (!username) {
    $(document.body).trigger('login_error', 'יש להזין כתובת מייל');
    return;
  }

  if (!password) {
    $(document.body).trigger('login_error', 'יש להזין סיסמה');
    return;
  }

  $.ajax({
    type: 'POST',
    dataType: 'json',
    url: ajax_url,
    data: {
      'action': 'ajaxlogin',
      'username': username,
      'password': password,
      'security': security,
      'remember': remember
    },
    success: function (data) {
      if (data.loggedIn) {
        $(document.body).trigger('login_success', data.message);

        if (!redirect) {
          location.reload();
        } else {
          location.href = redirect;
        }
      } else {
        $(document.body).trigger('login_error', data.message);
      }
    }
  });
}

function initRememberScrollMinicart() {
  let lastScrollPosMiniCart = 0;
  $(document.body).on('added_to_cart', function () {
    let miniCart = $('ul.woocommerce-mini-cart');

    if (!miniCart.length) {
      return;
    }

    setTimeout(function () {
      miniCart.animate({
        scrollTop: miniCart[0].scrollHeight
      }, 400);
      setTimeout(function () {
        lastScrollPosMiniCart = miniCart.scrollTop();
      }, 500);
    }, 400);
  });
  $('ul.woocommerce-mini-cart').scroll(function () {
    lastScrollPosMiniCart = $(this).scrollTop();
  });
  $(document.body).on('wc_fragments_refreshed', function () {
    let miniCart = $('ul.woocommerce-mini-cart');
    miniCart.scrollTop(lastScrollPosMiniCart);
    miniCart.scroll(function () {
      lastScrollPosMiniCart = $(this).scrollTop();
    });
  });
}

function validateFormElement(element, type = 'validate') {
  var $this = element,
      $parent = element.closest('.form-row'),
      validated = true,
      validate_required = $parent.is('.validate-required'),
      validate_email = $parent.is('.validate-email'),
      validate_phone = $parent.is('.validate-phone'),
      validate_cell_phone = $parent.is('.validate-cell-phone'),
      validate_no_english = $parent.is('.validate-no-english'),
      validate_length = $parent.is('.validate-length'),
      validate_no_numbers = $parent.is('.validate-no-numbers'),
      validate_numeric = $parent.is('.validate-numeric'),
      event_type = type;

  if ('input' === event_type) {
    $parent.removeClass('woocommerce-invalid woocommerce-invalid-required-field woocommerce-invalid-email woocommerce-validated');
  }

  if ('validate' === event_type || 'change' === event_type || 'blur' === event_type) {
    if (validate_required) {
      if ('checkbox' === $this.attr('type') && !$this.is(':checked')) {
        $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-required-field');
        $parent.attr('error-message', $this.attr('empty-message') || 'זהו שדה חובה');
        validated = false;
      } else if ($this.val() === '') {
        $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-required-field');
        $parent.attr('error-message', $this.attr('empty-message') || 'שדה זה הוא חובה');
        validated = false;
      }
    }

    if (validate_email) {
      if ($this.val()) {
        /* https://stackoverflow.com/questions/2855865/jquery-validate-e-mail-address-regex */
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

        if (!pattern.test($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-email');
          $parent.attr('error-message', $this.attr('invalid-email-message') || 'כתובת המייל אינה תקינה');
          validated = false;
        }
      }
    }

    if (validate_phone) {
      if ($this.val()) {
        if (!validatePhone($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-phone');
          $parent.attr('error-message', $this.attr('invalid-phone-message') || 'מספר הטלפון לא תקין');
          validated = false;
        }
      }
    }

    if (validate_cell_phone) {
      if ($this.val()) {
        if (!validateCellPhone($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-phone');
          $parent.attr('error-message', $this.attr('invalid-phone-message') || 'מספר הטלפון לא תקין');
          validated = false;
        }
      }
    }

    if (validate_no_english) {
      if ($this.val()) {
        if (!noEnglish($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-english');
          $parent.attr('error-message', 'לא ניתן להזין תווים באנגלית');
          validated = false;
        }
      }
    }

    if (validate_length) {
      if ($this.val()) {
        if ($this.attr('min-length') && $this.val().length < $this.attr('min-length')) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-length');
          $parent.attr('error-message', $this.attr('invalid-length-message') || 'נא להזין ערך באורך התקין');
          validated = false;
        }

        if ($this.attr('max-length') && $this.val().length > $this.attr('max-length')) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-length');
          $parent.attr('error-message', $this.attr('invalid-length-message') || 'נא להזין ערך באורך התקין');
          validated = false;
        }
      }
    }

    if (validate_no_numbers) {
      if ($this.val()) {
        if (hasNumberOrSpecialChar($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-numbers');
          $parent.attr('error-message', $this.attr('invalid-numbers-message') || 'לא ניתן להזין מספרים');
          validated = false;
        }
      }
    }

    if (validate_numeric) {
      if ($this.val()) {
        if (!isNumeric($this.val())) {
          $parent.removeClass('woocommerce-validated').addClass('woocommerce-invalid woocommerce-invalid-numeric');
          $parent.attr('error-message', $this.attr('invalid-numeric-message') || 'אפשר להשתמש פה רק במספרים');
          validated = false;
        }
      }
    }

    if (validated) {
      $parent.removeClass('woocommerce-invalid woocommerce-invalid-required-field woocommerce-invalid-email').addClass('woocommerce-validated');
    }

    return validated;
  }
} // Init Sticky Header


$(window).scroll(function () {
  if ($(document).width() < 768) {
    $(".sticky").removeClass('fixed');
    return;
  }

  var top = $(document).scrollTop();

  if (top < 250) {
    $(".sticky").removeClass('fixed');
    $('.header-container').removeClass('buffer');
  } else {
    $(".sticky").addClass('fixed');
    $('.header-container').addClass('buffer');
  }
});
let qvXHR = null;

function fetchQuickView(productId, callback) {
  $('.qv-container').html('');
  qvXHR && qvXHR.abort();
  qvXHR = $.ajax({
    type: 'POST',
    url: ajax_url,
    data: {
      productId,
      action: 'getQuickView'
    },
    dataType: 'json',
    complete: function (response) {
      try {
        $('.qv-container').append(response.responseText);
        afterUpdateInfiniteScroll();
        callback();
      } catch (e) {
        console.log(e);
      }
    }
  });
}

function openQuickView(productId, attributes, imageUrl) {
  fetchQuickView(productId, function () {
    try {
      $('#qvModal' + productId + ' button.single_add_to_cart_button').removeClass('added');
      $('#qvModal' + productId).modal('show');
      $('#qvModal' + productId + ' input[name="cartItemToReplace"]').appendTo('#qvModal' + productId + ' form');

      if (attributes) {
        for (var key in attributes) {
          if (attributes.hasOwnProperty(key)) {
            let elemUlLi = $('#qvModal' + productId + ' ul[data-attribute_name="' + key + '"] li[data-value="' + attributes[key] + '"]');
            let elemSelectOption = $('#qvModal' + productId + ' select[data-attribute_name="' + key + '"] option[value="' + attributes[key] + '"]');

            if (elemUlLi.length > 0) {
              elemUlLi.trigger('click');
            } else if (elemSelectOption.length > 0) {
              elemSelectOption.attr("selected", 'selected');
            }

            console.log(key + " -> " + attributes[key]);
          }
        }
      }

      if (imageUrl) {
        let galleryEl = $('#qvModal' + productId + ' .qv-gallery-images');
        $('#qvModal' + productId + ' .qv-gallery-image').remove();
        galleryEl.append('<div class="qv-gallery-image"><img src="' + imageUrl + '" alt=""></div>');
      }

      initQvCarousel(productId);
      return false;
    } catch (e) {
      console.log(e);
    }
  });
  return false;
}

function initQvCarousel(productId) {
  setTimeout(function () {
    invokeQuickviewSlickGallery(jQuery('#qvModal' + productId + ' div.qv-gallery-images'));
  }, 200);
}

function invokeQuickviewSlickGallery(elem) {
  elem.slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    arrows: false,
    rtl: true
  });

  if (elem.find('.slick-slide').length < 2) {
    elem.find('ul.slick-dots').css('visibility', 'hidden');
  } else {
    elem.find('ul.slick-dots').css('visibility', 'visible');
  }
}

function initQuantityInputs() {
  $('.add-to-cart-form .quantity').each(function () {
    if ($(this).find('.quantity-button').length === 0) {
      $('<div class="quantity-button quantity-up">+</div>' + '<div class="quantity-button quantity-down">-</div>').insertAfter($(this).find('input'));
      var spinner = $(this),
          input = spinner.find('input[type="number"]'),
          btnUp = spinner.find('.quantity-up'),
          btnDown = spinner.find('.quantity-down'),
          min = input.attr('min'),
          max = input.attr('max');
      btnUp.click(function () {
        var oldValue = parseFloat(input.val());

        if (max && oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }

        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });
      btnDown.click(function () {
        var oldValue = parseFloat(input.val());

        if (min && oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }

        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });
    }
  });
}

jQuery(document).ready(function () {
  let $ = jQuery;

  $.support.shrinkWrapBlocks = function () {};

  initQuantityInputs();
  setTimeout(bindMiniCartFunctions, 1000);
  $(document.body).on('wc_fragments_refreshed', function () {
    bindMiniCartFunctions();
  });
  $('.quickview-atc-form select.woo-variation-raw-select').bind('change', function () {
    let me = $(this);
    setTimeout(function () {
      let varid = me.parent().parent().find('input.variation_id').val();
      var data = {
        action: 'get-variation-points',
        variation_id: varid
      };
      $.ajax({
        type: 'post',
        url: wc_add_to_cart_params.ajax_url,
        data: data,
        beforeSend: function () {},
        complete: function () {},
        success: function (response) {
          let spann = me.parent().parent().parent().parent().find('.points-to-earn');
          spann.text(response);
          spann.parent().css('display', 'block');
        }
      });
    }, 100);
  });
  initHomepageSliders();
  infiniteScrolling();
  toggleMobileBackground();
  setMobileMenuHeight();
  adjustMobileBanners();
  initRememberScrollMinicart();
});

function initHomepageSliders() {
  $('ul.slides').each(function () {
    let element = $(this);

    if (element.find('li').length > 1) {
      let prevArrow = $('<a href=\"javascript:void(0);\" class=\"images-nav-back slick-arrow\">&nbsp;</a>');
      let nextArrow = $('<a href=\"javascript:void(0);\" class=\"images-nav-next slick-arrow\">&nbsp;</a>');
      element.before(prevArrow);
      element.after(nextArrow);
      element.slick({
        infinite: true,
        rtl: true,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: element.data('auto-transition') ? true : false,
        autoplaySpeed: element.data('transition-time') * 1000,
        prevArrow,
        nextArrow
      });
    }
  });
}

function bindMiniCartFunctions() {
  $('.set-item-quantity').off('click');
  $('.actual-quantity').off('keydown');
  $('.set-item-quantity').bind('click', function () {
    updateCartItemQuantity($(this).data('item-target-quantity'), $(this).data('item-key'));
  });
  $('.actual-quantity').bind('keydown', function (e) {
    if (e.keyCode === 13) {
      updateCartItemQuantity($(this).val(), $(this).data('item-key'));
    }
  });
}

function addToWishlist(el, callback = null) {
  $.ajax({
    type: 'POST',
    url: yith_wcwl_l10n.ajax_url,
    data: {
      add_to_wishlist: el.data('product-id'),
      product_type: 'simple',
      action: yith_wcwl_l10n.actions.add_to_wishlist_action
    },
    dataType: 'json',
    beforeSend: function () {
      el.siblings('.ajax-loading').css('visibility', 'visible');
    },
    complete: function () {
      el.siblings('.ajax-loading').css('visibility', 'hidden');
    },
    success: function (response) {
      el.find('img').attr('src', el.attr('data-active-src'));
      el.addClass('added');
      el.attr('onclick', 'removeFromWishlist($(this)); return false;');

      if (location.href.indexOf('/wishlist/') !== -1) {
        location.reload();
      } else {
        el.parent().parent().find('.remove').trigger('click');
        updateWishlistCount();
      }

      if (callback) {
        callback();
      }

      if (dataLayer) {
        dataLayer.push({
          'event': 'addedToWishlist',
          'sku': el.attr('data-product_sku')
        });
      }
    }
  });
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validatePhone(phone) {
  let isnum = /^\d+$/.test(phone);

  if (!isnum) {
    return false;
  }

  let ln = phone.match(/\d/g).length;

  if (!(ln > 7 && ln < 11)) {
    return false;
  }

  return true;
}

function validateCellPhone(phone) {
  let isnum = /^\d+$/.test(phone);

  if (!isnum) {
    return false;
  }

  let ln = phone.match(/\d/g).length;

  if (!(ln > 9 && ln < 11)) {
    return false;
  }

  if (phone.substring(0, 2) !== '05') {
    return false;
  }

  return true;
}

function removeFromWishlist(el) {
  $.ajax({
    type: 'POST',
    url: yith_wcwl_l10n.ajax_url,
    data: {
      remove_from_wishlist: el.data('product-id'),
      product_type: 'simple',
      action: yith_wcwl_l10n.actions.remove_from_wishlist_action
    },
    dataType: 'json',
    beforeSend: function () {
      el.siblings('.ajax-loading').css('visibility', 'visible');
    },
    complete: function () {
      el.siblings('.ajax-loading').css('visibility', 'hidden');
      el.removeClass('added');

      if (location.href.indexOf('/wishlist/') !== -1) {
        el.closest('.product-wrapper').hide();

        if ($('.product-wrapper:visible').length === 0) {
          location.reload();
        }
      } else {
        el.find('img').attr('src', el.attr('data-inactive-src'));
        el.attr('onclick', 'addToWishlist($(this)); return false;');
      }

      updateWishlistCount();

      if (dataLayer) {
        dataLayer.push({
          'event': 'removedFromWishlist',
          'sku': el.attr('data-product_sku')
        });
      }
    },
    success: function (response) {}
  });
}

function initHoverThumb() {
  $('.thumb-wrapper').hover(function () {
    let image = $(this).find('a img.thumb');
    let hoverAttr = image.data('hover');

    if (hoverAttr) {
      image.attr('src', hoverAttr);
    }
  }, function () {
    let image = $(this).find('a img.thumb');
    image.attr('src', image.data('original-src'));
  });
  $('.thumb-wrapper').each(function () {
    preloadImage($(this).find('a img.thumb').data('hover'));
  });
}

function preloadImage(url) {
  if (url) {
    var img = new Image();
    img.src = url;
  }
}

function toggleMobileBackground() {
  let bgElem = $('.bg-image');

  if ($(window).width() <= 768) {
    bgElem.css('backgroundImage', bgElem.data('bgmobile'));
  } else {
    bgElem.css('backgroundImage', bgElem.data('bg'));
  }
}

$(window).resize(function () {
  toggleMobileBackground();
  setMobileMenuHeight();
  adjustMobileBanners();
});

function adjustMobileBanners() {
  if ($(window).width() <= 768) {
    $('.archive-banner').each(function () {
      $(this).css('display', $(this).data('show_mobile'));
      $(this).css('gridColumn', $(this).data('mobile-column'));
    });
  } else {
    $('.archive-banner').each(function () {
      $(this).css('display', 'block');
      $(this).css('gridColumn', $(this).data('original-column'));
    });
  }
}

function updateWishlistCount() {
  $.ajax({
    type: 'POST',
    url: ajax_url,
    data: {
      action: 'getWishlistCount'
    },
    dataType: 'json',
    success: function (response) {
      $('span.wishlist-count').html(response);

      if (response > 0) {
        $('.topicon.wishlisticon').addClass('full');
      } else {
        $('.topicon.wishlisticon').removeClass('full');
      }
    }
  });
}

$(document).click(function (e) {
  let className = 'login-popup';

  if (!$(e.target).hasClass(className) && !$(e.target).parent('.' + className).length && !$(e.target).closest('.' + className).length) {
    $('.login-popup').hide();
  }

  className = 'category-filters';

  if (!$(e.target).hasClass(className) && !$(e.target).closest('.' + className).length && !(e.target.className === 'control filter') && !$(e.target).closest('.control.filter').length) {
    $('.category-filters .woocommerce-widget-layered-nav-list:visible').removeClass('visible').parent().find('h2').removeClass('active');
  }

  className = 'mini-cart-wrapper';

  if (!$(e.target).hasClass('widget_shopping_cart_content') && !$(e.target).closest('.widget_shopping_cart_content').length && !$(e.target).hasClass(className) && !$(e.target).closest('.' + className).length && !(e.target.className === 'mini-cart-toggle') && !$(e.target).parent('.mini-cart-toggle').length) {
    toggleMiniCart('close');
  }
});

function noEnglish(text) {
  var letters = /^[A-Za-z]+$/;

  for (var i = 0; i < text.length; i++) {
    if (text.charAt(i).match(letters)) {
      return false;
    }
  }

  return true;
}

function hasNumberOrSpecialChar(text) {
  return !/^[a-z\u0590-\u05fe]+$/i.test(text.replace(/ /g, ''));
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}
function validateOptions() {
  if ($('.product #pa_color option').index("option:selected") == 0) {
    $('.product #pa_color option:first-child').html('יש לבחור צבע');
    $('.product #pa_color').css('color', '#990000');
  } else {
    $('.product #pa_color').css('color', '#000000');
  }

  if ($('.product #pa_size option').index("option:selected") == 0) {
    $('.product #pa_size option:first-child').html('יש לבחור מידה');
    $('.product #pa_size').css('color', '#990000');
  } else {
    $('.product #pa_size').css('color', '#000000');
  }
}

jQuery(document).ready(function ($) {
  $('.product #pa_color').change(validateOptions);
  $('.product #pa_size').change(validateOptions);
  $(document).on('added_to_cart', function (e) {
    console.log('product added to cart triggered');
  });
  $(document.body).off('click', '.add_to_cart_button');
  $('.variations_form').on('wc_variation_form', function () {
    $(this).off('click', '.single_add_to_cart_button');
  });
  $(document).on('click', '.single_add_to_cart_button, .add_to_cart_button', function (e) {
    e.preventDefault();
    let errorElem = $('.variation-error');
    errorElem.text('');

    if ($(this).is('.disabled')) {
      if ($(this).is('.wc-variation-is-unavailable')) {
        errorElem.text(wc_add_to_cart_variation_params.i18n_unavailable_text);
      } else if ($(this).is('.wc-variation-selection-needed')) {
        validateOptions(); // errorElem.text( wc_add_to_cart_variation_params.i18n_make_a_selection_text );
      }

      return false;
    }

    var $thisbutton = $(this),
        $form = $thisbutton.closest('form.cart'),
        id = $thisbutton.val(),
        product_qty = $form.find('input[name=quantity]').val() || $thisbutton.data('quantity') || 1,
        product_id = $form.find('input[name=product_id]').val() || id || $thisbutton.data('product_id'),
        variation_id = $form.find('input[name=variation_id]').val() || 0;
    performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, $form.find('input[name=cartItemToReplace]').val() || 0);
    return false;
  });
});

function performAjaxAddToCart(product_id, product_qty, variation_id, $thisbutton, cartItemKey = 0) {
  var data = {
    action: 'woocommerce_ajax_add_to_cart',
    product_id: product_id,
    product_sku: '',
    quantity: product_qty,
    variation_id: variation_id,
    cartItemKey: cartItemKey
  };
  $(document.body).trigger('adding_to_cart', [$thisbutton, data]);
  $.ajax({
    type: 'post',
    url: wc_add_to_cart_params.ajax_url,
    data: data,
    beforeSend: function () {
      $thisbutton.removeClass('added').addClass('loading');
    },
    complete: function () {
      $thisbutton.addClass('added').removeClass('loading');
    },
    success: function (response) {
      if (response.error & response.product_url) {
        window.location = response.product_url;
        return;
      } else {
        $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
      }
    }
  });
}

function updateCartItemQuantity(quantity, itemKey) {
  let data = {
    action: 'woocommerce_ajax_update_quantity',
    quantity: quantity,
    cart_item_key: itemKey
  };
  $.ajax({
    type: 'post',
    url: wc_add_to_cart_params.ajax_url,
    data: data,
    beforeSend: function () {},
    complete: function () {},
    success: function (response) {
      for (fragment in response.fragments) {
        $(fragment).replaceWith(response.fragments[fragment]);
      }

      $(document.body).trigger('wc_fragments_refreshed', [response.fragments, response.cart_hash]);
      bindMiniCartFunctions();
    }
  });
}
var swappableAreaWidth;
var swappableAreaHeight;
var lastUpdated = new Date(); //jQuery(document).ready(fixAdditionalVariationImages);

function is_mobile() {
  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    return true;
  } else {
    return false;
  }
}

fixAdditionalVariationImages();

function fixAdditionalVariationImages() {
  jQuery('form.variations_form').on('wc_additional_variation_images_frontend_init', function () {
    jQuery('form.variations_form').off('reset_image');
    jQuery('form.variations_form').off('wc_additional_variation_images_frontend_image_swap_callback');
    jQuery('form.variations_form').on('wc_additional_variation_images_frontend_image_swap_callback', function (e, response) {
      if (!jQuery(this).is(':visible')) {
        return;
      } // debounce buggy duplicates


      let diff = new Date().getTime() - lastUpdated.getTime();
      lastUpdated = new Date();

      if (diff < 800) {
        return;
      } // Quickview


      if (jQuery(this).parents('.qv-modal').length) {
        let elem = jQuery(this).closest('.modal');
        reloadVariationImagesForQuickview(elem, response);
      } else {
        // Product Page
        if (!wc_additional_variation_images_local.bwc) {
          var parent = jQuery(wc_additional_variation_images_local.main_images_class).parent();
          let swappableArea = jQuery('.woocommerce-product-gallery__image.flex-active-slide img');
          swappableAreaWidth = swappableArea.outerWidth();
          swappableAreaHeight = swappableArea.outerHeight();
          let wishlistImage = jQuery(wc_additional_variation_images_local.main_images_class).find('a.whishlist');
          let badges = jQuery(wc_additional_variation_images_local.main_images_class).find('.yith-wcbm-badge');
          jQuery(wc_additional_variation_images_local.main_images_class).remove();
          let newImages = jQuery(response.main_images);
          let innerHtml = '';
          newImages.find('.woocommerce-product-gallery__image').each(function () {
            innerHtml += jQuery(this)[0].outerHTML; // console.log(jQuery(this)[0].outerHTML);
            //this is zoom fix on load my Matat Dev

            if (!is_mobile() && typeof(IZ) !== 'undefined') {
              var options = IZ.options;
              var first_img = ".woocommerce-product-gallery__wrapper .flex-active-slide img";
              var old_value = "";

              if (event.newValue != old_value) {
                jQuery(".zoomContainer").remove();
                setTimeout(function () {
                  jQuery(first_img).first().image_zoom(options);
                  restart_on_hover(jQuery(first_img).first());
                }, 550);
              }

              old_value = event.newValue;

              function restart_on_hover(elem) {
                elem.hover(function () {
                  if (jQuery('.zoomContainer').length === 0) {
                    jQuery(this).image_zoom(IZ.options);
                  }
                });
              }

              ;
            }
          });
          newImages.children()[0].innerHTML = innerHtml;
          jQuery.when(parent.prepend(newImages)).then(function () {
            parent.find('.wp-post-image').each(function () {
              jQuery(this).attr('data-src', jQuery(this).data('large_image'));
            });
            jQuery(wc_additional_variation_images_local.main_images_class).prepend(wishlistImage); //jQuery('.woocommerce-product-gallery__image').width(swappableAreaWidth).height(swappableAreaHeight);
            //jQuery('.woocommerce-product-gallery__wrapper').width(swappableAreaWidth).height(swappableAreaHeight);

            setTimeout(function () {
              jQuery('.flex-viewport').height(jQuery('.woocommerce-product-gallery__image.flex-active-slide img').outerHeight());
              jQuery(wc_additional_variation_images_local.main_images_class).find('.flex-viewport').append(badges);
            }, 500);
          });
        } else {
          jQuery(wc_additional_variation_images_local.gallery_images_class).fadeOut(50, function () {
            jQuery(this).html(response.gallery_images).hide().fadeIn(100, function () {
              jQuery.wc_additional_variation_images_frontend.runLightBox();
            });
          });
        } //jQuery.wc_additional_variation_images_frontend.initProductGallery();

      }
    });
  });
  jQuery(document).trigger('additional_images_fix_completed');
}

function reloadVariationImagesForQuickview(elem, response) {
  let galleryEl = elem.find('.qv-gallery-images');
  galleryEl.css('display', 'none !important');
  galleryEl.slick('unslick');
  galleryEl.find('.qv-gallery-image').remove();
  jQuery(response.main_images).find('img').each(function () {
    galleryEl.append('<div class="qv-gallery-image"><img src="' + jQuery(this).attr('src') + '" alt=""></div>');
    console.log(jQuery(this).attr('src'));
  });
  invokeQuickviewSlickGallery(galleryEl);
}
let alreadyScrolledIntoView = false;
let infiniteScrollingActive = true;

function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  if ($(elem).length === 0) {
    return false;
  }

  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  return elemBottom <= docViewBottom && elemTop >= docViewTop;
}

function infiniteScrolling() {
  $(window).scroll(function () {
    scrolledIntoView = isScrolledIntoView($('.infinite-loader-wrapper')[0]);

    if (!alreadyScrolledIntoView && scrolledIntoView && infiniteScrollingActive) {
      scrollToNextPage();
    }

    alreadyScrolledIntoView = scrolledIntoView;
  });
}

function scrollToNextPage() {
  let nextPage = $('.products').data('page') + 1;
  $.ajax({
    type: 'get',
    url: location.protocol + '//' + location.host + location.pathname + '/page/' + nextPage + location.search,
    success: function (response) {
      let firstProduct = $(response).find('.product-wrapper').first().data('product-id');

      if ($('.product-wrapper[data-product-id="' + firstProduct + '"]').length > 0) {
        $('.infinite-loader').hide();
        infiniteScrollingActive = false;
      } else {
        $('.products').append($(response).find('.products').children());
        afterUpdateInfiniteScroll();
      }
    },
    error: function () {
      $('.infinite-loader').hide();
      infiniteScrollingActive = false;
    }
  });
  $('.products').data('page', nextPage);
}

function afterUpdateInfiniteScroll() {
  try {
    $('.lmp_products_loading').remove();
    initializeAjaxAddTocart();
    initHoverThumb();
    initQuantityInputs();
    fixAdditionalVariationImages();
    $('.variations_form').each(function () {
      if (!$(this).attr('initialized')) {
        $(this).wc_variation_form();
        $(this).attr('initialized', true);
      }
    });
    $.wc_additional_variation_images_frontend.init();
  } catch (Err) {
    console.log(Err);
  }
}
let lastOpenedMobileSubMenu = -1;
jQuery(document).ready(function () {
  let $ = jQuery;
  $('.hamburger').bind('click', function (e) {
    $(this).toggleClass('is-active');
    let menu = $('#menu-mobile');

    if (menu.is(":visible")) {
      $('.top-stripe').show();
      menu.slideUp();
    } else {
      $('.top-stripe').hide();
      menu.slideDown();
    }

    if (menu.is(":visible")) {
      $('body').toggleClass('ham');
    }
  });

  if ($('.woocommerce-store-notice').length > 0) {
    setTimeout(function () {
      if ($('.woocommerce-store-notice').is(':visible')) {
        height = $('.woocommerce-store-notice').outerHeight();
        $('body').css('padding-top', height);
      }
    }, 1000);
  }

  $(document).on('click', '.woocommerce-store-notice__dismiss-link', function () {
    $('body').css('padding-top', 0);
  });
  $('#menu-item-484182 a').attr('data-toggle', 'modal').attr('data-target', '#sizeChoose');
  $(document).on('click', '#menu-item-484182', function () {
    return false;
  });
  setCartWidth();
  $('#menu-mobile a.back').bind('click', function () {
    if (lastOpenedMobileSubMenu != -1) {
      $('#menu-mobile ul li').hide();
      $('#menu-mobile ul.submenu').hide();
      $('li[data-item-id="' + lastOpenedMobileSubMenu + '"]').trigger('click');
    } else {
      $('#menu-mobile ul li').show();
      $('#menu-mobile ul.submenu').hide();
      $('.mobile-menu-back-button').hide();
      $('.mobile-login-link').show();
    }

    return false;
  });
  $(document).on('woocommerce_variation_has_changed', function (event) {
    $('select.woo-variation-raw-select').each(function () {
      let elem = $(this).parent().find('li[data-value="' + $(this).val() + '"]');
      setTimeout(function () {
        elem.addClass('selected');
      }, 100);
    });
  });
  $('select.woo-variation-raw-select').bind('change', function (e, value) {
    if ($(this).val() === null && $(this).data('prev-val') !== null) {
      $(this).val($(this).data('prev-val'));
    }

    $(this).data('prev-val', $(this).val());
  });
  $('ul.variable-items-wrapper li').bind('click', function (e) {
    if ($(this).hasClass('disabled')) {
      e.preventDefault();
      return false;
    }
  });
  initHoverThumb();
  $('.mini-cart-toggle').bind('click', e => {
    toggleMiniCart();
  });
  $(window).bind('click', function (e) {
    if (e.target.className === 'close-mini-cart') {
      toggleMiniCart('close');
    }
  });
});

function toggleMiniCart(mode = 'toggle') {
  if (mode === 'toggle') {
    $('.mini-cart-wrapper').toggle(400);
    $('.topicon.minicart').toggleClass('active');
  } else if (mode === 'close') {
    $('.mini-cart-wrapper').hide(400);
    $('.topicon.minicart').removeClass('active');
  }

  $(document).trigger('mini_cart_toggled');
}

jQuery(window).resize(function () {
  setCartWidth();
});

function setCartWidth() {
  if ($(window).width() < 768) {
    $('.mini-cart-wrapper').width($('.menubar-mobile').width());
  } else {
    $('.mini-cart-wrapper').width('auto');
  }
}

function openSubMenuForParent(parentId, backText, backUrl) {
  $('#menu-mobile ul li').hide();
  $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"]').show();
  $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"] li').show();
  lastOpenedMobileSubMenu = $('#menu-mobile ul.submenu[data-item-id="' + parentId + '"]').data('parent-id');
  let linkToAll = $('.mobile-menu-back-button a.to-all');
  linkToAll.text(backText);
  linkToAll.attr('href', backUrl);
  $('.mobile-menu-back-button').show();
  $('.mobile-login-link').hide();
}

$(document).click(function (e) {
  let className = 'we-are-here-box';

  if (!$(e.target).hasClass(className) && !$(e.target).parent('.' + className).length && !$(e.target).closest('.' + className).length) {
    $('.' + className).removeClass('active');
  }
});

var _zaVerSnippet = 5,
    _zaq = _zaq || [];

(function () {
  var w = window,
      d = document;

  w.__za_api = function (a) {
    _zaq.push(a);

    if (typeof __ZA != 'undefined' && typeof __ZA.sendActions != 'undefined') __ZA.sendActions(a);
  };

  var e = d.createElement('script');
  e.type = 'text/javascript';
  e.async = true;
  e.src = ('https:' == d.location.protocol ? 'https://d2xerlamkztbb1.cloudfront.net/' : 'http://wcdn.zoomanalytics.co/') + '19762818-33df/5/widget.js';
  var ssc = d.getElementsByTagName('script')[0];
  ssc.parentNode.insertBefore(e, ssc);
})();
jQuery(document).ready(function () {
  let $ = jQuery;
  $('.search-toggle, .x-wrapper').bind('click', function (e) {
    e.preventDefault();
    $('.search-popup-wrap').toggle();
    $('.search-popup-wrap input').focus();
  });
  $('.results-popup-wrapper').hide();
  $('input.search-value').keyup(debounce(function () {
    let value = $('input.search-value:visible').val();

    if (!$('input.search-value').is(':visible') || value.length > 0 && value.length < 3) {
      return;
    }

    if (value.length === 0) {
      $('.results-popup-wrapper').hide();
      $('.results-popup').html('');

      if ($(window).width() < 768) {
        $('.search-popup-wrap').hide();
      }

      return;
    }

    $.ajax({
      type: 'post',
      url: ajax_url,
      data: {
        action: 'ajaxSearch',
        s: value
      },
      success: function (response) {
        if (response) {
          $('.results-popup-wrapper').show();
          $('.search-popup-wrap').show();
          $('.results-popup').html(response);
        } else {
          $('.results-popup-wrapper').hide();
        }
      }
    });
  }, 1000));
  $('input.search-value').keypress(function (e) {
    let value = $(this).val();

    if (e.which == 13) {
      location.href = '/shop/?s=' + value;
      e.preventDefault();
      return false;
    }
  });
  $('.search-icon').click(function (e) {
    let value = $(this).parent().find('input.search-value').val(); //console.log('.search-icon');

    if (value.length > 0) {
      location.href = '/shop/?s=' + value;
      e.preventDefault();
      return false;
    }
  });
});

function debounce(func, wait) {
  var timeout;
  return function () {
    clearTimeout(timeout);
    timeout = setTimeout(func, wait);
  };
}
jQuery(document).ready(function () {
  $('form.newsletter-form').bind('submit', function () {
    return submitNewsletter($(this));
  });
});

function submitNewsletter(form) {
  let response = form.find('.newsletter-response');
  response.removeClass('error');
  response.text('');
  let email = form.find("#email.newsletter").val();

  if (!validateEmail(email)) {
    response.text('יש להזין כתובת דואל תקינה');
    response.addClass('error');
    return false;
  }

  let approve = form.find('.newsletter-approve');

  if (approve.length && !approve.is(':checked')) {
    response.text('יש להסכים לתנאי השימוש');
    response.addClass('error');
    return false;
  }

  var data = {
    action: 'join-newsletter',
    email: email
  };
  $.ajax({
    type: 'post',
    url: ajax_url,
    data: data,
    beforeSend: function () {},
    complete: function () {},
    success: function (res) {
      if (!res) {
        response.text('חלה שגיאה בהרשמה לניוזלטר');
      } else {
        if (res.error) {
          response.text(res.error);
          response.addClass('error');
          return false;
        }

        response.text('נרשמת בהצלחה!');
      }
    }
  });
  return false;
}
//# sourceMappingURL=scripts.js.map
