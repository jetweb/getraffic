$(document).ready(function () {
    $('.category-topbar .archive-filter ul').wrap("<div class='archive-filter-content'></div>");

    // Opening and closing the filters
    $('.category-topbar .archive-filter .archive-filter-title').bind('click', function () {
        let wasVisible = $(this).parent().find('.archive-filter-content, .price_slider_wrapper').hasClass('visible');
        $('.archive-filter .archive-filter-content, .archive-filter .price_slider_wrapper').removeClass('visible');
        $('.archive-filter, .archive-filter .archive-filter-title').removeClass('active');

        if (!wasVisible) {
            $(this).parent().find('.archive-filter-content, .price_slider_wrapper').toggleClass('visible');
            $(this).toggleClass('active');
            $('.archive-filter').removeClass('active');
            $(this).parent().toggleClass('active');
        }
    });
    $('.mobile-filter-options .archive-filter .archive-filter-title').bind('click', function () {
        $(this).parent().toggleClass('active');
    });

    // Add filter tags to page top
    $('.category-filters li.chosen').each(function () {
        $(this).closest('.archive-filter').addClass('has-chosen-items');
        if ($(this).hasClass('order-item')) {
            if ( $(this).find('a').data('value') !== 'menu_order') {
                $('.filter-tags').append('<div class="filter-tag">' + $(this).find('a').text() + '<a href="" onclick="resetSorting(); return false;" class="remove-filter"><img src="' + xImageUrl + '" /></a></div>');
            }
        } else {
            $('.filter-tags').append('<div class="filter-tag">' + $(this).find('a').data('term_name') + '<a href="' + $(this).find('a').attr('href') + '" class="remove-filter"><img src="' + xImageUrl + '" /></a></div>');
        }
    });

    // Add special price tag
    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('min_price')) {
        $('.filter-tags').append('<div class="filter-tag">' + 'מחיר : ' + '<span class="price-filter-amounts">' + urlParams.get('min_price') + '₪ - ' + urlParams.get('max_price') + '₪</span><a href="" class="remove-filter remove-price-filter"><img src="' + xImageUrl + '" /></a></div>');
    }

    // Opening and closing for mobile UI elements
    $('.category-filter-mobile span, .mobile-filter-container .mobile-filter-close, .mobile-filter-container .mobile-filter-title a.back').bind('click', function () {
        $('.mobile-filter-container').toggle(400);
        return false;
    });
    $('.category-sort-mobile span, .mobile-sort-container .mobile-filter-close, .mobile-sort-container .mobile-filter-title a.back').bind('click', function () {
        $('.mobile-sort-container').toggle(400);
        return false;
    });

	var $onsale = $('.showonsale').clone();
	$($onsale).insertAfter('.desktop-filtering .price_label');

    // Remove for price filter (reset)
    $('a.remove-price-filter').click(() => {
        let urlParams = new URLSearchParams(window.location.search);
        urlParams.delete('min_price');
        urlParams.delete('max_price');
        location.href = window.location.pathname + '?' + urlParams.toString();
        return false;
    });
    $('#onsalechk, .showonsale input').click(() => {
        let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('onsale')) {
         urlParams.delete('onsale');
       location.href = window.location.pathname + '?' + urlParams.toString() ;
	} else {
        location.href = window.location.pathname + '?' + urlParams.toString() + '&onsale=1';

	}
        return false;
    });

    $('ul.gt-orderby li a').click(function(e) {
        e.preventDefault();
        $('.woocommerce-ordering .orderby').val($(this).data('value')).trigger('change');
        return false;
    });

    // Add count of selected attributes
    $('.category-topbar .archive-filter .archive-filter-title').each(function () {
        if ($(this).parent().hasClass('order-by')) {
            return;
        }
        let count = $(this).parent().find('li.chosen').length;
        if (count > 0) {
            $(this).html($(this).html() + '<span class="count">(' + count + ')</span>');
        }
    });

    $('ul.woocommerce-widget-layered-nav-list').each(function() {
        $(this).addClass('filter-for-' + $(this).find('a').data('tax'));
    });

    if (isMultiSelectEnabled) {
        setupMultipleChoiceFiltering();
    }
});

/**
 * Change behaviour of woocommerce filters to allow multiple choice!
 */
function setupMultipleChoiceFiltering() {
    $('.wc-layered-nav-term a').bind('click', function() {
        $(this).closest('li').toggleClass('chosen');
        return false;
    });

    $('.desktop-filtering .archive-filter').each(function() {
        if (!$(this).hasClass('order-by')) {
            $(this).find('.archive-filter-content').append(
                '<div class="archive-filter-buttons">' +
                    '<div class="submitFilter"><a>סינון</a></div>' +
                    '<div class="clearFilter"><a>נקה</a></div>' +
                '</div>');
        }
    });

    $('.mobile-filter-container .archive-filter').each(function() {
        if (!$(this).hasClass('order-by')) {
            $(this).find('ul').append(
                '<li class="archive-filter-buttons">' +
                '<div class="submitFilter"><a>סינון</a></div>' +
                '<div class="clearFilter"><a>נקה</a></div>' +
                '</li>');
        }
    });



    $('.submitFilter a').click(function() {
        let newParam = '';
        let tax = $(this).closest('.archive-filter').find('li:not(.archive-filter-buttons) a').data('tax');
        newParamName = 'filter_' + tax.replace('pa_', '');
        newQueryType = 'query_type_' + tax.replace('pa_', '');
        $(this).closest('.archive-filter').find('li.chosen').each(function() {
            let term = $(this).find('a').data('term_slug');
            newParam += newParam === '' ? '' : ',';
            newParam += decodeURI(term);
        });

        let searchParams = new URLSearchParams(window.location.search);
        searchParams.set(newParamName, newParam);
        searchParams.set(newQueryType, 'or');
        window.location.search = searchParams.toString();
    });

    $('.clearFilter a').click(function() {
        $(this).closest('.archive-filter').find('li.chosen').removeClass('chosen');
        $(this).closest('.archive-filter').find('.submitFilter a').trigger('click');
    });
}


function resetSorting() {
    $('.woocommerce-ordering .orderby').val('menu_order').trigger('change');
}
