var swappableAreaWidth;
var swappableAreaHeight;
var lastUpdated = new Date();
//jQuery(document).ready(fixAdditionalVariationImages);

function is_mobile() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    } else {
        return false;
    }
}
fixAdditionalVariationImages()
function fixAdditionalVariationImages() {
    jQuery('form.variations_form').on('wc_additional_variation_images_frontend_init', function () {

        jQuery('form.variations_form').off('reset_image');
        jQuery('form.variations_form').off('wc_additional_variation_images_frontend_image_swap_callback');
        jQuery('form.variations_form').on('wc_additional_variation_images_frontend_image_swap_callback', function (e, response) {
            if (!jQuery(this).is(':visible')) {
                return;
            }

            // debounce buggy duplicates
            let diff = new Date().getTime() - lastUpdated.getTime();
            lastUpdated = new Date();
            if (diff < 800) {
                return;
            }

            // Quickview
            if (jQuery(this).parents('.qv-modal').length) {
                let elem = jQuery(this).closest('.modal');
                reloadVariationImagesForQuickview(elem, response);
            } else {
                // Product Page
                if (!wc_additional_variation_images_local.bwc) {
                    var parent = jQuery(wc_additional_variation_images_local.main_images_class).parent();
                    let swappableArea = jQuery('.woocommerce-product-gallery__image.flex-active-slide img');
                    swappableAreaWidth = swappableArea.outerWidth();
                    swappableAreaHeight = swappableArea.outerHeight();
                    let wishlistImage = jQuery(wc_additional_variation_images_local.main_images_class).find('a.whishlist');
                    let badges = jQuery(wc_additional_variation_images_local.main_images_class).find('.yith-wcbm-badge');

                    jQuery(wc_additional_variation_images_local.main_images_class).remove();
                    let newImages = jQuery(response.main_images);
                    let innerHtml = '';
                    newImages.find('.woocommerce-product-gallery__image').each(function() {
                        innerHtml += jQuery(this)[0].outerHTML;
                        // console.log(jQuery(this)[0].outerHTML);

                        //this is zoom fix on load my Matat Dev
                        if (!is_mobile()) {
                            var options = IZ.options;
                            var first_img = ".woocommerce-product-gallery__wrapper .flex-active-slide img";

                            var old_value = "";
                            if ( event.newValue != old_value ) {
                                jQuery(".zoomContainer").remove();
                                setTimeout( function() {
                                    jQuery(first_img).first().image_zoom(options);
                                    restart_on_hover(jQuery(first_img).first());
                                }, 550);
                            }
                            old_value = event.newValue;

                            function restart_on_hover( elem ) {
                                elem.hover(function(){
                                    if ( jQuery('.zoomContainer').length === 0 ) {
                                        jQuery(this).image_zoom(IZ.options);
                                    }
                                });
                            };
                        }
                    });
                    newImages.children()[0].innerHTML = innerHtml;
                    jQuery.when(parent.prepend(newImages)).then(function () {
                        parent.find('.wp-post-image').each(function () {
                            jQuery(this).attr('data-src', jQuery(this).data('large_image'));
                        });
                        jQuery(wc_additional_variation_images_local.main_images_class).prepend(wishlistImage);
                        //jQuery('.woocommerce-product-gallery__image').width(swappableAreaWidth).height(swappableAreaHeight);
                        //jQuery('.woocommerce-product-gallery__wrapper').width(swappableAreaWidth).height(swappableAreaHeight);
                        setTimeout(function () {
                            jQuery('.flex-viewport').height(jQuery('.woocommerce-product-gallery__image.flex-active-slide img').outerHeight());
                            jQuery(wc_additional_variation_images_local.main_images_class).find('.flex-viewport').append(badges);
                        }, 500);
                    });
                } else {
                    jQuery(wc_additional_variation_images_local.gallery_images_class).fadeOut(50, function () {
                        jQuery(this).html(response.gallery_images).hide().fadeIn(100, function () {
                            jQuery.wc_additional_variation_images_frontend.runLightBox();
                        });
                    });
                }
                //jQuery.wc_additional_variation_images_frontend.initProductGallery();
            }
        });
    });

    jQuery(document).trigger('additional_images_fix_completed');
}

function reloadVariationImagesForQuickview(elem, response) {
    let galleryEl = elem.find('.qv-gallery-images');
    galleryEl.css('display', 'none !important');
    galleryEl.slick('unslick');
    galleryEl.find('.qv-gallery-image').remove();
    jQuery(response.main_images).find('img').each(function () {
        galleryEl.append('<div class="qv-gallery-image"><img src="' + jQuery(this).attr('src') + '" alt=""></div>');
        console.log(jQuery(this).attr('src'));
    });
    invokeQuickviewSlickGallery(galleryEl);
}
