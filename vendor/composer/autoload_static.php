<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfeb5d5488e4b7b7b555b7c020368b50b
{
    public static $classMap = array (
        'ComposerAutoloaderInitd6aa886d747b02e2e8069dadb27bec27' => __DIR__ . '/../..' . '/modules/Instagram/vendor/composer/autoload_real.php',
        'Composer\\Autoload\\ClassLoader' => __DIR__ . '/../..' . '/modules/Instagram/vendor/composer/ClassLoader.php',
        'Composer\\Autoload\\ComposerStaticInitd6aa886d747b02e2e8069dadb27bec27' => __DIR__ . '/../..' . '/modules/Instagram/vendor/composer/autoload_static.php',
        'CustomCheckout' => __DIR__ . '/../..' . '/modules/CustomCheckout/CustomCheckout.php',
        'CustomMenuWalker' => __DIR__ . '/../..' . '/includes/CustomMenuWalker.php',
        'GT_Subcat_List_Walker' => __DIR__ . '/../..' . '/includes/walkers/gt-subcat-list-walker.php',
        'GetAllHeadersTest' => __DIR__ . '/../..' . '/modules/Instagram/vendor/ralouphie/getallheaders/tests/GetAllHeadersTest.php',
        'GetrafficCategoryFilter' => __DIR__ . '/../..' . '/widgets/filter-by-category.php',
        'Getraffic\\AdminSettingsField' => __DIR__ . '/../..' . '/includes/AdminSettingsField.php',
        'Getraffic\\AdminSettingsPage' => __DIR__ . '/../..' . '/includes/AdminSettingsPage.php',
        'Getraffic\\AdminSettingsSection' => __DIR__ . '/../..' . '/includes/AdminSettingsSection.php',
        'Getraffic\\AjaxControllerInterface' => __DIR__ . '/../..' . '/includes/interfaces/AjaxControllerInterface.php',
        'Getraffic\\AjaxSearch' => __DIR__ . '/../..' . '/modules/AjaxSearch/AjaxSearch.php',
        'Getraffic\\AttributeFilters' => __DIR__ . '/../..' . '/modules/OrderMetaFields/AttributeFilters.php',
        'Getraffic\\AvailabilitySubscription' => __DIR__ . '/../..' . '/modules/AvailabilitySubscription/AvailabilitySubscription.php',
        'Getraffic\\CatalogIntegrationBase' => __DIR__ . '/../..' . '/integrations/CatalogIntegration/CatalogIntegrationBase.php',
        'Getraffic\\ComaxCatalog' => __DIR__ . '/../..' . '/integrations/CatalogIntegration/ComaxCatalog.php',
        'Getraffic\\ConfigurableModule' => __DIR__ . '/../..' . '/includes/interfaces/ConfigurableModule.php',
        'Getraffic\\CustomAddressFields' => __DIR__ . '/../..' . '/modules/CustomAddress/CustomAddressFields.php',
        'Getraffic\\CustomCheckoutFields' => __DIR__ . '/../..' . '/modules/CustomCheckout/CustomCheckoutFields.php',
        'Getraffic\\CustomDateQuery' => __DIR__ . '/../..' . '/includes/CustomDateQuery.php',
        'Getraffic\\CustomMetaBox' => __DIR__ . '/../..' . '/includes/CustomMetaBox.php',
        'Getraffic\\CustomMetaField' => __DIR__ . '/../..' . '/includes/CustomMetaField.php',
        'Getraffic\\CustomMetaQuery' => __DIR__ . '/../..' . '/includes/CustomMetaQuery.php',
        'Getraffic\\CustomMetaQueryField' => __DIR__ . '/../..' . '/includes/CustomMetaQueryField.php',
        'Getraffic\\CustomMyAccount' => __DIR__ . '/../..' . '/includes/CustomMyAccount.php',
        'Getraffic\\CustomPostType' => __DIR__ . '/../..' . '/includes/CustomPostType.php',
        'Getraffic\\CustomSaveAddress' => __DIR__ . '/../..' . '/modules/CustomAddress/CustomSaveAddress.php',
        'Getraffic\\CustomTaxonomy' => __DIR__ . '/../..' . '/includes/CustomTaxonomy.php',
        'Getraffic\\DefactoIntegration' => __DIR__ . '/../..' . '/integrations/Pandora/DeFacto/DefactoIntegration.php',
        'Getraffic\\DefactoMemberFields' => __DIR__ . '/../..' . '/integrations/Pandora/DeFacto/DefactoMemberFields.php',
        'Getraffic\\EmailSettings' => __DIR__ . '/../..' . '/modules/EmailSettings/EmailSettings.php',
        'Getraffic\\GTM' => __DIR__ . '/../..' . '/integrations/GoogleTagManager/GTM.php',
        'Getraffic\\Instagram' => __DIR__ . '/../..' . '/modules/Instagram/Instagram.php',
        'Getraffic\\Logger' => __DIR__ . '/../..' . '/integrations/Logger.php',
        'Getraffic\\MailChimp' => __DIR__ . '/../..' . '/modules/newsletter-providers/MailChimp.php',
        'Getraffic\\MembershipIntegrationBase' => __DIR__ . '/../..' . '/integrations/MembershipIntegration/MembershipIntegrationBase.php',
        'Getraffic\\OrderMetaFields' => __DIR__ . '/../..' . '/modules/OrderMetaFields/OrderMetaFields.php',
        'Getraffic\\PandoraCatalog' => __DIR__ . '/../..' . '/integrations/Pandora/PandoraCatalog.php',
        'Getraffic\\PandoraImages' => __DIR__ . '/../..' . '/integrations/Pandora/PandoraImages.php',
        'Getraffic\\PluginBase' => __DIR__ . '/../..' . '/includes/PluginBase.php',
        'Getraffic\\PluginSettingsBase' => __DIR__ . '/../..' . '/includes/PluginSettingsBase.php',
        'Getraffic\\Seo' => __DIR__ . '/../..' . '/modules/Seo/Seo.php',
        'Getraffic\\ShippingControls' => __DIR__ . '/../..' . '/modules/OrderMetaFields/ShippingControls.php',
        'Getraffic\\Smoove' => __DIR__ . '/../..' . '/modules/newsletter-providers/Smoove.php',
        'Getraffic\\SmooveTriggers' => __DIR__ . '/../..' . '/modules/SmooveTriggers/SmooveTriggers.php',
        'GuzzleHttp\\Client' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Client.php',
        'GuzzleHttp\\ClientInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/ClientInterface.php',
        'GuzzleHttp\\Cookie\\CookieJar' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Cookie/CookieJar.php',
        'GuzzleHttp\\Cookie\\CookieJarInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Cookie/CookieJarInterface.php',
        'GuzzleHttp\\Cookie\\FileCookieJar' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Cookie/FileCookieJar.php',
        'GuzzleHttp\\Cookie\\SessionCookieJar' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Cookie/SessionCookieJar.php',
        'GuzzleHttp\\Cookie\\SetCookie' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Cookie/SetCookie.php',
        'GuzzleHttp\\Exception\\BadResponseException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/BadResponseException.php',
        'GuzzleHttp\\Exception\\ClientException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/ClientException.php',
        'GuzzleHttp\\Exception\\ConnectException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/ConnectException.php',
        'GuzzleHttp\\Exception\\GuzzleException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/GuzzleException.php',
        'GuzzleHttp\\Exception\\RequestException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/RequestException.php',
        'GuzzleHttp\\Exception\\SeekException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/SeekException.php',
        'GuzzleHttp\\Exception\\ServerException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/ServerException.php',
        'GuzzleHttp\\Exception\\TooManyRedirectsException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/TooManyRedirectsException.php',
        'GuzzleHttp\\Exception\\TransferException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Exception/TransferException.php',
        'GuzzleHttp\\HandlerStack' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/HandlerStack.php',
        'GuzzleHttp\\Handler\\CurlFactory' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/CurlFactory.php',
        'GuzzleHttp\\Handler\\CurlFactoryInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/CurlFactoryInterface.php',
        'GuzzleHttp\\Handler\\CurlHandler' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/CurlHandler.php',
        'GuzzleHttp\\Handler\\CurlMultiHandler' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/CurlMultiHandler.php',
        'GuzzleHttp\\Handler\\EasyHandle' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/EasyHandle.php',
        'GuzzleHttp\\Handler\\MockHandler' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/MockHandler.php',
        'GuzzleHttp\\Handler\\Proxy' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/Proxy.php',
        'GuzzleHttp\\Handler\\StreamHandler' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Handler/StreamHandler.php',
        'GuzzleHttp\\MessageFormatter' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/MessageFormatter.php',
        'GuzzleHttp\\Middleware' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Middleware.php',
        'GuzzleHttp\\Pool' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/Pool.php',
        'GuzzleHttp\\PrepareBodyMiddleware' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/PrepareBodyMiddleware.php',
        'GuzzleHttp\\Promise\\AggregateException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/AggregateException.php',
        'GuzzleHttp\\Promise\\CancellationException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/CancellationException.php',
        'GuzzleHttp\\Promise\\Coroutine' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/Coroutine.php',
        'GuzzleHttp\\Promise\\EachPromise' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/EachPromise.php',
        'GuzzleHttp\\Promise\\FulfilledPromise' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/FulfilledPromise.php',
        'GuzzleHttp\\Promise\\Promise' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/Promise.php',
        'GuzzleHttp\\Promise\\PromiseInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/PromiseInterface.php',
        'GuzzleHttp\\Promise\\PromisorInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/PromisorInterface.php',
        'GuzzleHttp\\Promise\\RejectedPromise' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/RejectedPromise.php',
        'GuzzleHttp\\Promise\\RejectionException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/RejectionException.php',
        'GuzzleHttp\\Promise\\TaskQueue' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/TaskQueue.php',
        'GuzzleHttp\\Promise\\TaskQueueInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/promises/src/TaskQueueInterface.php',
        'GuzzleHttp\\Psr7\\AppendStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/AppendStream.php',
        'GuzzleHttp\\Psr7\\BufferStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/BufferStream.php',
        'GuzzleHttp\\Psr7\\CachingStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/CachingStream.php',
        'GuzzleHttp\\Psr7\\DroppingStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/DroppingStream.php',
        'GuzzleHttp\\Psr7\\FnStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/FnStream.php',
        'GuzzleHttp\\Psr7\\InflateStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/InflateStream.php',
        'GuzzleHttp\\Psr7\\LazyOpenStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/LazyOpenStream.php',
        'GuzzleHttp\\Psr7\\LimitStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/LimitStream.php',
        'GuzzleHttp\\Psr7\\MessageTrait' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/MessageTrait.php',
        'GuzzleHttp\\Psr7\\MultipartStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/MultipartStream.php',
        'GuzzleHttp\\Psr7\\NoSeekStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/NoSeekStream.php',
        'GuzzleHttp\\Psr7\\PumpStream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/PumpStream.php',
        'GuzzleHttp\\Psr7\\Request' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/Request.php',
        'GuzzleHttp\\Psr7\\Response' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/Response.php',
        'GuzzleHttp\\Psr7\\Rfc7230' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/Rfc7230.php',
        'GuzzleHttp\\Psr7\\ServerRequest' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/ServerRequest.php',
        'GuzzleHttp\\Psr7\\Stream' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/Stream.php',
        'GuzzleHttp\\Psr7\\StreamDecoratorTrait' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/StreamDecoratorTrait.php',
        'GuzzleHttp\\Psr7\\StreamWrapper' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/StreamWrapper.php',
        'GuzzleHttp\\Psr7\\UploadedFile' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/UploadedFile.php',
        'GuzzleHttp\\Psr7\\Uri' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/Uri.php',
        'GuzzleHttp\\Psr7\\UriNormalizer' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/UriNormalizer.php',
        'GuzzleHttp\\Psr7\\UriResolver' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/psr7/src/UriResolver.php',
        'GuzzleHttp\\RedirectMiddleware' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/RedirectMiddleware.php',
        'GuzzleHttp\\RequestOptions' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/RequestOptions.php',
        'GuzzleHttp\\RetryMiddleware' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/RetryMiddleware.php',
        'GuzzleHttp\\TransferStats' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/TransferStats.php',
        'GuzzleHttp\\UriTemplate' => __DIR__ . '/../..' . '/modules/Instagram/vendor/guzzlehttp/guzzle/src/UriTemplate.php',
        'Instagram\\Api' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Api.php',
        'Instagram\\Exception\\CacheException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Exception/CacheException.php',
        'Instagram\\Exception\\InstagramException' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Exception/InstagramException.php',
        'Instagram\\Hydrator\\Component\\Feed' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Hydrator/Component/Feed.php',
        'Instagram\\Hydrator\\Component\\Media' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Hydrator/Component/Media.php',
        'Instagram\\Hydrator\\HtmlHydrator' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Hydrator/HtmlHydrator.php',
        'Instagram\\Hydrator\\JsonHydrator' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Hydrator/JsonHydrator.php',
        'Instagram\\Storage\\Cache' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Storage/Cache.php',
        'Instagram\\Storage\\CacheManager' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Storage/CacheManager.php',
        'Instagram\\Transport\\HtmlTransportFeed' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Transport/HtmlTransportFeed.php',
        'Instagram\\Transport\\JsonTransportFeed' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Transport/JsonTransportFeed.php',
        'Instagram\\Transport\\TransportFeed' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/src/Instagram/Transport/TransportFeed.php',
        'Instagram\\tests\\ApiTest' => __DIR__ . '/../..' . '/modules/Instagram/vendor/pgrimaud/instagram-user-feed/tests/ApiTest.php',
        'ProductData' => __DIR__ . '/../..' . '/integrations/CatalogIntegration/ProductData.php',
        'Psr\\Http\\Message\\MessageInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/MessageInterface.php',
        'Psr\\Http\\Message\\RequestInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/RequestInterface.php',
        'Psr\\Http\\Message\\ResponseInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/ResponseInterface.php',
        'Psr\\Http\\Message\\ServerRequestInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/ServerRequestInterface.php',
        'Psr\\Http\\Message\\StreamInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/StreamInterface.php',
        'Psr\\Http\\Message\\UploadedFileInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/UploadedFileInterface.php',
        'Psr\\Http\\Message\\UriInterface' => __DIR__ . '/../..' . '/modules/Instagram/vendor/psr/http-message/src/UriInterface.php',
        'Wcal_Checkout_Process' => __DIR__ . '/../..' . '/modules/SmooveTriggers/checkout_process.php',
        'ac_session' => __DIR__ . '/../..' . '/modules/SmooveTriggers/session.php',
        'woocommerce_abandon_cart_cron' => __DIR__ . '/../..' . '/modules/SmooveTriggers/cron/send_to_smoove.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInitfeb5d5488e4b7b7b555b7c020368b50b::$classMap;

        }, null, ClassLoader::class);
    }
}
