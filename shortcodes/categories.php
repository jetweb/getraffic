<?php require_once(__DIR__ . '/categories-printer.php'); ?>
<div class="shop-bycat-bg">
    <div class="cats">
        <div class="container">
            <?php
            $cat_args = [
                'orderby'    => 'name',
                'order'      => 'asc',
                'hide_empty' => false,
                'parent'     => 0,
            ];

            echo printCategories($cat_args, true, '', true);

            ?>
        </div>
    </div>
</div>
