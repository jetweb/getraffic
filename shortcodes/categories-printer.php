<?php

function printCategories($args, $isTopLevel = true, $pluralName = '', $isHomePage = false)
{
    ob_start();
    $product_categories = get_terms('product_cat', $args);
    if (!$isTopLevel) {
        $active = get_queried_object_id() === $args['parent'] ? 'active' : '';
        echo '
            <a class="subcat-link '. $active . '" href="'. get_term_link($args['parent']) .'" data-cat-id="-1">' . $pluralName . '</a>
        ';
    }
    foreach ($product_categories as $cat) {
        if ($isTopLevel) {
            if ($cat->name == 'Uncategorized') {
                continue;
            }
            $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
            $image        = wp_get_attachment_url($thumbnail_id);
            if ($isHomePage) {
                ?>
                <div class="col-md-6">
                    <a href="<?= get_term_link($cat->term_id); ?>">
                        <div class="cat-homepage">
                            <div class="thumb-wrap">
                                <img class="thumb" src="<?php echo wp_get_attachment_image_src($thumbnail_id, 'homepage-cat-thumb')[0]; ?>" alt="">
                            </div>
                            <h2 class="title"><?= $cat->name ?></h2>
                        </div>
                    </a>
                </div>
                <?php
            } else {
                ?>
                <a href="<?= get_term_link($cat->term_id); ?>">
                    <div class="cat">
                        <div class="img-white-border">
                            <div class="theborder"></div>
                            <img src="<?php echo $image; ?>" alt="">
                            <div class="bg"></div>
                        </div>
                        <h2 class="title"><?= $cat->name ?></h2>
                    </div>
                </a>
                <?php
            }
        } else {
            ?>
            <a href="<?= get_term_link($cat->term_id); ?>" class="subcat-link <?php echo get_queried_object_id() === $cat->term_id ? 'active' : '' ?>" data-cat-id="<?= $cat->term_id; ?>">
                <?= $cat->name ?>
            </a>
            <?php
        }
    }

    return ob_get_clean();
}

function printFeaturedProducts($products) {
    foreach ($products as $product) {
        $image = get_the_post_thumbnail_url($product->ID);
        ?>
        <a href="<?= get_permalink($product->ID) ?>">
            <div class="cat">
                <div class="img-white-border">
                    <div class="theborder"></div>
                    <img src="<?php echo $image; ?>" alt="">
                    <div class="bg"></div>
                </div>
                <h2 class="title"><?= $product->post_title ?></h2>
            </div>
        </a>
        <?php
    }
}