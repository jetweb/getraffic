<?php

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/widgets/filter-by-category.php');

// Include ACF fields
include 'includes/acf-fields/slider-and-bg-image.php';
include 'includes/acf-fields/archive-banners.php';
include 'includes/acf-fields/mega-menu-banners.php';
include 'includes/acf-fields/label.php';
include 'includes/acf-fields/mobile-menu-icons.php';
include 'includes/acf-fields/cart-fields.php';
include 'includes/acf-fields/coupons.php';
include 'includes/acf-fields/image-sizes.php';
include 'includes/acf-fields/checkout.php';
include 'includes/acf-fields/my-account.php';
include 'includes/acf-fields/category.php';
include 'includes/acf-fields/points.php';
include 'includes/acf-fields/wish-list.php';
include 'includes/acf-fields/newsletter.php';
include 'includes/acf-fields/admin-fields.php';
include 'includes/acf-fields/product-slider-1.php';

use Getraffic\AttributeFilters;
use Getraffic\AvailabilitySubscription;
use Getraffic\GTM;
use Getraffic\ShippingControls;
use Getraffic\Smoove;
use Getraffic\MailChimp;
use Getraffic\PluginBase;
use Getraffic\AjaxSearch;
use Getraffic\EmailSettings;
use Getraffic\CustomSaveAddress;
use Getraffic\CustomMyAccount;
use Getraffic\CustomAddressFields;
use Getraffic\CustomCheckoutFields;
use Getraffic\CustomPostType;
use Getraffic\CustomTaxonomy;
use Getraffic\CustomMetaBox;
use Getraffic\CustomMetaField;
use Getraffic\OrderMetaFields;
use Getraffic\Seo;

/**
 * Class GT
 */
class GT extends PluginBase
{
    const GETRAFFIC_CLUB_MEMBER_META = 'GETRAFFIC_CLUB_MEMBER';

    private $configrationSections = [
        Smoove::class,
        MailChimp::class,
        EmailSettings::class,
     //   AvailabilitySubscription::class,
        AjaxSearch::class,
		OrderMetaFields::class,
        ShippingControls::class,
        Seo::class,
        AttributeFilters::class,
        GTM::class,
    ];

    private $customAccount;
    private $customAddressFields;
    private $customCheckoutFields;
    private $taxonomyLabels;

    public function __construct()
    {
        if(!isset($_SESSION)) {
//            session_start();
        }

        parent::__construct();
        $this->customAccount        = new CustomMyAccount();
        $this->customAddressFields  = new CustomAddressFields();
        $this->customCheckoutFields = new CustomCheckoutFields($this->customAddressFields);

        if (!function_exists('get_field')) {
            trigger_error("Advanced Custom Fields must be installed for GT Master theme to work.");
            return;
        }

        if (!class_exists('WooCommerce')) {
            trigger_error("WooCommerce must be installed and activated for GT Master theme to work.");
            return;
        }

        $this->registerFrontendCalls();
        $this->registerWidgets();
        $this->initializeCaching();
        $this->registerConfigurableModules();
        $this->registerMenus();
        $this->registerImageSizes();

        // Add theme support
        add_action('after_setup_theme', function () {
            $gallerySize   = gt_get_field('gallery_thumb_size', 'option');
            add_theme_support('woocommerce', [
                'gallery_thumbnail_image_width' => $gallerySize['width'],
                'gallery_thumbnail_image_height' => $gallerySize['height'],
            ]);
            add_theme_support('wc-product-gallery-zoom');
            add_theme_support('wc-product-gallery-lightbox');
            add_theme_support('wc-product-gallery-slider');
            add_theme_support('post-thumbnails');
        });

        // Initialize recently viewed cookies
        add_action('wp', [$this, 'registerRecentlyViewed']);
        add_action( 'init', [$this, 'modifyAccountInformation'] );
        add_shortcode('newsletter', [$this, 'renderNewsletter']);
        add_action('user_register', [$this, 'joinClubOnReg'], 10, 1);
    }

    private function registerFrontendCalls()
    {
        $this->registerFrontendCall('woocommerce_ajax_add_to_cart', 'ajaxAddToCart');
        $this->registerFrontendCall('woocommerce_ajax_update_quantity', 'ajaxUpdateQuantity');
        $this->registerFrontendCall('ajaxlogin', 'ajaxLogin');
        $this->registerFrontendCall('join_club', 'joinClub');
        $this->registerFrontendCall('get-variation-points', 'getPointsByVariationId');
        $this->registerFrontendCall('ajaxSearch', 'ajaxSearch');
        $this->registerFrontendCall('ajax-register-user', 'ajaxRegisterUser');
        $this->registerFrontendCall('validateCheckout', 'validateCheckout');
        $this->registerFrontendCall('getWishlistCount', 'getWishlistCount');
        $this->registerFrontendCall('applyPoints', 'applyPoints');
        $this->registerFrontendCall('getQuickView', 'getQuickView');
    }

    function getQuickView() {
        $productId = $_POST['productId'];
        set_query_var('qvProduct', wc_get_product($productId));
        get_template_part('templates/popups/quick-view');
        exit;
    }

    /**
     * @return array
     */
    function getCustomPostTypes()
    {
        $LOCATIONS_LABELS = [
            'name'               => 'סניפים',
            'singular_name'      => 'סניף',
            'menu_name'          => 'סניפים',
            'parent_item_colon'  => 'סניף אב',
            'all_items'          => 'כל הסניפים',
            'view_item'          => 'הצג סניף',
            'add_new_item'       => 'הוסף סניף חדש',
            'add_new'            => 'הוסף חדש',
            'edit_item'          => 'ערוך סניף',
            'update_item'        => 'עדכן סניף',
            'search_items'       => 'חפש בסניפים',
            'not_found'          => 'לא נמצא',
            'not_found_in_trash' => 'לא נמצא בזבל',
        ];

        // Create custom post type
        $customPostTypes['location'] = (new CustomPostType())
            ->setSlug('location')
            ->setName('location')
            ->setLabels($LOCATIONS_LABELS)
            ->setIcon('dashicons-admin-home')
            ->setCustomTaxonomies([
                'tm_city' => $this->getLocationTaxonomy(),
            ])
            ->setCustomMetaBoxes($this->getCustomMetaBoxesLocations());

        return $customPostTypes;
    }

    private function getCustomMetaBoxesLocations()
    {
        $fields[] = (new CustomMetaField())
            ->setTitle('תיאור')
            ->setName('location_desc')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('כתובת')
            ->setName('location_address')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('טלפון')
            ->setName('location_phone')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('פקס')
            ->setName('location_fax')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('אימייל')
            ->setName('location_email')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('שעות פתיחה א-ה')
            ->setName('location_hours_sun_thu')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('שעות פתיחה ו׳')
            ->setName('location_hours_fri')
            ->setType(CustomMetaField::TYPE_TEXT);

        $fields[] = (new CustomMetaField())
            ->setTitle('שעות פתיחה ש')
            ->setName('location_hours_sat')
            ->setType(CustomMetaField::TYPE_TEXT);

        $customMetaBoxes[] = (new CustomMetaBox())
            ->setName('item_settings_adv')
            ->setPosition('normal')
            ->setTitle('פרטי הסניף')
            ->setFields($fields);

        return $customMetaBoxes;
    }

    private function getLocationTaxonomy()
    {
        return (new CustomTaxonomy())->setName('tm_city')->setSlug('type')->setHierarchical(true)->setLabels($this->getCityTaxonomyLabels());
    }

    protected function getCityTaxonomyLabels()
    {
        if (!$this->taxonomyLabels['city']) {
            $this->taxonomyLabels['city'] =
            [
                'name'               => __('עיר', 'nadlanboard'),
                'singular_name'      => __('עיר', 'nadlanboard'),
                'menu_name'          => __('ערים', 'nadlanboard'),
                'parent_item_colon'  => __('עיר אב', 'nadlanboard'),
                'all_items'          => __('כל הערים', 'nadlanboard'),
                'view_item'          => __('הצג עיר', 'nadlanboard'),
                'add_new_item'       => __('הוסף עיר חדשה', 'nadlanboard'),
                'add_new'            => __('הוסף חדש', 'nadlanboard'),
                'edit_item'          => __('ערוף עיר', 'nadlanboard'),
                'update_item'        => __('עדכן עיר', 'nadlanboard'),
                'search_items'       => __('חפש עיר', 'nadlanboard'),
                'not_found'          => __('לא נמצאה', 'nadlanboard'),
                'not_found_in_trash' => __('לא נמצאה בזבל', 'nadlanboard'),
            ];
        }
        return $this->taxonomyLabels['city'];
    }

    function setCityTaxonomyLabels($labels) {
        $this->taxonomyLabels['city'] = $labels;
    }

    private function registerImageSizes() {
        $carouselSize  = gt_get_field('carousel_thumb_size', 'option');
        $gallerySize   = gt_get_field('gallery_thumb_size', 'option');
        $prodThumbSize = gt_get_field('product_thumb_size', 'option');

        add_image_size('carousel-thumb', $carouselSize['width'], $carouselSize['height'], true);
        add_image_size('gallery_thumbnail', $gallerySize['width'], $gallerySize['height'], true);
        add_image_size('product-thumb', $prodThumbSize['width'], $prodThumbSize['height'], false);
        set_post_thumbnail_size($prodThumbSize['width'], $prodThumbSize['height'], true);

        $thumbnailSizes = gt_get_field('thumbnail_sizes', 'option');
        if (!empty($thumbnailSizes)) {
            foreach ($thumbnailSizes as $size) {
                add_image_size($size['size_name'], $size['width'], $size['height'], true);
            }
        }
    }

    public function setMyAccountLinks($links) {
        $this->customAccount->setLinks($links);
    }

    public function setCustomAddressFields($fields) {
        $this->customAddressFields->setFields($fields);
    }

    public function setCustomCheckoutFields($fields) {
        $this->customCheckoutFields->setFields($fields);
    }

    public function getCustomCheckoutFields() {
        return $this->customCheckoutFields->fields;
    }

    public function getFullFormattedAddress($order) {
        return $this->customCheckoutFields->getFormattedAddress($order);
    }

    /**
     * This is unfortunately the only "sane" way to save both addresses (billing/shipping) on the same form
     */
    public static function modifyAccountInformation()
    {
        global $wp;
        try {
            if (strpos($_SERVER['REQUEST_URI'], 'edit-address') !== false && isset($_POST['action']) && $_POST['action'] === 'edit_address') {
                $wp->query_vars['edit-address'] = 'billing';
                CustomSaveAddress::save_address();
                if (isset($_POST['edit_also_shipping'])) {
                    $wp->query_vars['edit-address'] = 'shipping';
                    CustomSaveAddress::save_address();
                }
                $wp->query_vars['edit-address'] = '';
                wp_safe_redirect(wc_get_endpoint_url('edit-address', '', wc_get_page_permalink('myaccount')));
            }
        } catch (Error $ex) {}
    }

    public function getWishlistCount() {
        if (function_exists('yith_wcwl_count_all_products')) {
            echo yith_wcwl_count_all_products();
        }
        exit;
    }

    public static function enqueueQuickView($productToView)
    {
        global $quickViewProducts;

        $quickViewProducts[] = $productToView;
    }

    public static function printQuickViews() {
        global $quickViewProducts;

        if (!empty($quickViewProducts)) {
            foreach ($quickViewProducts as $productToView) {
                set_query_var('qvProduct', $productToView);
                get_template_part('templates/popups/quick-view');
            }
        }
    }

    private function registerConfigurableModules() {
        foreach ($this->configrationSections as $module) {
            new $module();
            $module::getSettingsPage();
        }
    }

    public function registerWidgets() {
        if (class_exists('GetrafficCategoryFilter')) {
            register_widget('GetrafficCategoryFilter');
        }
        register_sidebar([
            'name'          => 'Archive Page Filters',
            'id'            => 'archive_page_filters',
            'before_widget' => '<div class="archive-filter">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="archive-filter-title widgettitle">',
            'after_title'   => '</div>',
        ]);
    }

    public static function notEmpty($i) {
        return !empty($i);
    }

    public static function filterProduct($product)
    {
        $cats = isset($_REQUEST['filter_category']) ? $_REQUEST['filter_category'] : null;

        $cats = array_filter(explode(',', $cats), 'self::notEmpty');
        if (!$cats) {
            return true;
        }

        $terms = get_the_terms($product->get_id(), 'product_cat');
        foreach ($terms as $term) {
            if (in_array($term->term_id, $cats)) {
                return true;
            }
        }

        return false;
    }

    public function ajaxUpdateQuantity()
    {
        $cart_item_key     = $_POST['cart_item_key'];
        $quantity          = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
        $cartItem          = WC()->cart->get_cart_item($cart_item_key);
        $passed_validation =
            apply_filters('woocommerce_update_cart_validation', true, $cart_item_key, $cartItem, $quantity);

        if ($passed_validation && WC()->cart->set_quantity($cart_item_key, $quantity)) {
            WC_AJAX::get_refreshed_fragments();
        } else {
            $data = [
                'error' => true,
            ];
            wp_send_json($data);
        }

        wp_die();
    }

    public static function removeItemFromCart($cartItemKey) {
        WC()->cart->remove_cart_item($cartItemKey);
    }

    public static function getFirstTopLevelCategory(WC_Product $product) {
        $cat = get_the_terms($product->get_id(), 'product_cat');

        if (is_array($cat)) {
            foreach ($cat as $category) {
                if ($category->parent == 0) {
                    return $category;
                }
            }
        }

        $cat = get_the_terms($product->get_parent_id(), 'product_cat');

        if (is_array($cat)) {
            foreach ($cat as $category) {
                if ($category->parent == 0) {
                    return $category;
                }
            }
        }

        return null;
    }

    public function ajaxAddToCart()
    {
        $product_id        = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
        $quantity          = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
        $variation_id      = absint($_POST['variation_id']);
        $cartItemKey       = $_POST['cartItemKey'];
        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
        $product_status    = get_post_status($product_id);

        if (!empty($cartItemKey)) {
            self::removeItemFromCart($cartItemKey);
        }

        if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {
            do_action('woocommerce_ajax_added_to_cart', $variation_id, $quantity);
            if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                wc_add_to_cart_message([$product_id => $quantity], true);
            }
            WC_AJAX::get_refreshed_fragments();
        } else {
            $data = [
                'error'       => true,
                'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id),
            ];
            wp_send_json($data);
        }

        wp_die();
    }

    public static function getProductThumbnailURL($product)
    {
        return wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()))[0];
    }

    public static function getProductImageURL($product, $size)
    {
        return wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()), $size)[0];
    }

    function getTextDomain()
    {
        return 'getraffic';
    }


    static function getProductMainImageHTML($product)
    {
        $post_thumbnail_id = $product->get_image_id();
        echo wc_get_gallery_image_html($post_thumbnail_id, true);
    }

    static function getCurrentProductMainImageHTML()
    {
        global $product;

        return self::getProductMainImageHTML($product);
    }

    static function enqueueAsset($name, $relativePath, $dependencies, $isScript = false, $renderLate = false, $ver = false)
    {
        $assetUrl = self::getRealAssetUrl($relativePath);
        //$ver      = false;

        if (WP_DEBUG === true) {
            $ver = time();
        }

        if ($isScript) {
            wp_enqueue_script($name, $assetUrl, $dependencies, $ver, $renderLate);
        } else {
            wp_enqueue_style($name, $assetUrl, $dependencies, $ver);
        }
    }

    static function img($relativePath)
    {
        $imagesDir         = '/assets/images/';
        return self::getRealAssetUrl($imagesDir . $relativePath);
    }

    private static function getRealAssetUrl($relativePath) {
        $absolutePathChild = get_stylesheet_directory() . $relativePath;

        return (file_exists($absolutePathChild)) ?
            (get_stylesheet_directory_uri() . $relativePath) :
            (get_template_directory_uri() . $relativePath);
    }

    /**
     * @param WC_Product $product
     *
     * @throws Exception
     */
    public static function printProductBadge($product)
    {
        $showBadge = gt_get_field('show_badge', $product->get_id());
        if ($showBadge === false) {
            return;
        }

        $badgeSettings = gt_get_field('badges_settings', 'option');
        $defCol = $badgeSettings['default_color'];
        $defColDB = 'transparent';

        $label = gt_get_field('product_label', $product->get_id());
        $image = gt_get_field('product_label_image', $product->get_id());
        $color = gt_get_field('product_label_color', $product->get_id());
        if ($product instanceof WC_Product_Variation) {
            $label = gt_get_field('product_label', $product->get_parent_id());
            $image = gt_get_field('product_label_image', $product->get_parent_id());
            $color = gt_get_field('product_label_color', $product->get_parent_id());
        }

        // Product specific tag
        if (!empty($label)) {
            set_query_var('productLabel', $label);
            set_query_var('badgeColor',
                (empty($color) ? (empty($defCol) ? $defColDB : $defCol) : $color)
            );
            set_query_var('badgeImage', $image);
            get_template_part('templates/product/badge/any');
            return;
        } else if (!empty($image)) {
            set_query_var('badgeImage', $image);
            set_query_var('badgeColor', 'transparent');
            get_template_part('templates/product/badge/any');
            return;
        }

        // General tags
        if (!$product->is_in_stock() && $badgeSettings['show_oos_badge']) {
            set_query_var('badgeColor',
                (empty($badgeSettings['oos_color']) ? (empty($defCol) ? $defColDB : $defCol) : $badgeSettings['oos_color'])
            );
            set_query_var('badgeImage', $badgeSettings['oos_image']);
            get_template_part('templates/product/badge/out-of-stock');
            return;
        }
        // Category specific tag
        $catIds = $product->get_category_ids();
        if (empty($catIds)) {
            $catIds = wc_get_product($product->get_parent_id())->get_category_ids();
        }
        foreach ($catIds as $category_id) {
            $fields = gt_get_field('category_badge', 'product_cat_' . $category_id);
            if (isset($fields['show']) && $fields['show'] === true) {
                set_query_var('productLabel', $fields['text']);
                set_query_var('badgeColor',
                    (empty($fields['color']) ? (empty($defCol) ? $defColDB : $defCol) : $fields['color'])
                );
                set_query_var('badgeImage', $fields['image']);
                get_template_part('templates/product/badge/any');
                return;
            }
        }
        if ($product->is_on_sale() && $badgeSettings['show_sale_badge']) {
            set_query_var('badgeColor',
                (empty($badgeSettings['sale_color']) ? (empty($defCol) ? $defColDB : $defCol) : $badgeSettings['sale_color'])
            );
            set_query_var('badgeImage', $badgeSettings['sale_image']);
            get_template_part('templates/product/badge/sale');
            return;
        }
        $created = $product->get_date_created();
        if ((date_diff(new DateTime(), $created)->days < $badgeSettings['badge_new_days']) && $badgeSettings['show_new_badge']) {
            set_query_var('badgeColor',
                (empty($badgeSettings['new_color']) ? (empty($defCol) ? $defColDB : $defCol) : $badgeSettings['new_color'])
            );
            set_query_var('badgeImage', $badgeSettings['new_image']);
            get_template_part('templates/product/badge/new');
            return;
        }
        $bestSelling = self::getBestSellingProducts($badgeSettings['badge_bs_count']);
        if (in_array($product->get_id(), array_keys($bestSelling)) && $badgeSettings['show_bs_badge']) {
            set_query_var('badgeColor',
                (empty($badgeSettings['best_sellers_color']) ? (empty($defCol) ? $defColDB : $defCol) : $badgeSettings['new_color'])
            );
            set_query_var('badgeImage', $badgeSettings['best_sellers_image']);
            get_template_part('templates/product/badge/best-seller');
            return;
        }
    }

    static function getBestSellingProducts($count)
    {
        global $gt_best_selling_products;

        if (!empty($gt_best_selling_products)) {
            return $gt_best_selling_products;
        }

        $products = [];
        $args     = [
            'post_type'      => 'product',
            'meta_key'       => 'total_sales',
            'orderby'        => 'meta_value_num',
            'posts_per_page' => $count,
        ];
        $loop     = new WP_Query($args);
        while ($loop->have_posts()): $loop->the_post();
            global $product;
            $products[$product->get_id()] = $product;
        endwhile;
        wp_reset_query();

        $gt_best_selling_products = $products;
        return $products;
    }

    public function ajaxLogin()
    {
        // First check the nonce, if it fails the function will break
        if ($_SERVER['HTTP_HOST'] !== 'localhost') {
            check_ajax_referer('ajax-login-nonce', 'security');
        }

        // Nonce is checked, get the POST data and sign user on
        $info                  = [];
        $info['user_login']    = $_POST['username'];
        $info['user_password'] = $_POST['password'];
        $info['remember']      = $_POST['remember'];

        $user_signon = wp_signon($info, false);
        if (is_wp_error($user_signon)) {
            echo json_encode(['loggedIn' => false, 'message' => __('כתובת מייל או סיסמה אינם תקינים')]);
        } else {
            echo json_encode(['loggedIn' => true, 'message' => __('התחברת בהצלחה...')]);
        }

        exit();
    }

    public static function getUserOrdersByStatus($status) {
        $args = array(
            'customer_id' => get_current_user_id(),
            'post_status' => $status,
            'post_type' => 'shop_order',
            'return' => 'ids',
        );

        return count(wc_get_orders($args));
    }

    public static function renderWishlistButton($productId, $activeImage, $inactiveImage) {
        set_query_var('product_id', $productId);
        set_query_var('active_image', $activeImage);
        set_query_var('inactive_image', $inactiveImage);
        get_template_part('templates/product/wishlist-button');
    }

    private function initializeCaching()
    {
        if (!function_exists('gt_get_field')) {
            function gt_get_field($key, $object = false) {
                if ($object instanceof WP_Post) {
                    $object = $object->ID;
                }
                $cacheKey = get_queried_object_id() . '__' . $key . '__' . $object;
                $result   = GT::getFromCache($cacheKey);

                if (!$result) {
                    $acfResult = get_field($key, $object);
                    GT::setInCache($cacheKey, $acfResult);

                    return $acfResult;
                }

                return $result;
            }
        }

        try {
            global $gtMemcached;
            if (class_exists('Memcached')) {
                $gtMemcached = new Memcached();
            } else if (class_exists('Memcache')) {
                $gtMemcached = new Memcache();
            } else {
                return false;
            }
            $gtMemcached->addServer('localhost', 11211);
        } catch (Error $ex) {
            trigger_error($ex->getMessage());
        }

        add_action('save_post', [$this, 'flushCache']);
        add_action('edit_term', [$this, 'flushCache']);
        add_action('updated_option',  function( $option_name, $old_value, $value ) {
            if (strpos($option_name, 'options_') !== false) {
                $this->flushCache();
            }
        }, 10, 3);

        return true;
    }

    private static function getCachePrefix() {
        return isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
    }

    public static function getFromCache($key) {
        global $gtMemcached;
        $key = self::getCachePrefix() . $key;
        if ($gtMemcached) {
            $cachedResult = $gtMemcached->get($key);
            if ($cachedResult) {
                return $cachedResult;
            }
        }

        return false;
    }

    public static function setInCache($key, $object) {
        global $gtMemcached;
        $key = self::getCachePrefix() . $key;

        if ($gtMemcached) {
            $gtMemcached->set($key, $object);

            return true;
        }

        return false;
    }

    public function flushCache() {
        global $gtMemcached;
        if ($gtMemcached) {
            $gtMemcached->flush();
        }
    }

    public static function getPointsByVariationId() {
        return self::getPointsToEarnByVariationId($_REQUEST['variation_id']);
    }

    public static function ajaxSearch() {
        try {
            AjaxSearch::ajaxSearch();
        } catch (Error $ex) {
            echo $ex->getMessage();
        }
    }

    public static function getPointsToEarnInCart()
    {
        if (!class_exists('YITH_WC_Points_Rewards_Earning')) {
            return 0;
        }

        return YITH_WC_Points_Rewards_Earning()->calculate_points_on_cart();
    }

    public function applyPoints() {
        if (class_exists('YITH_WC_Points_Rewards_Redemption')) {
            YITH_WC_Points_Rewards_Redemption()->apply_discount();
        }
    }

    public static function getPointsToEarnByVariationId($variationId)
    {
        $product = wc_get_product($variationId);
        echo self::getPointsByProduct($product);
        exit;
    }

    public static function getPointsByProduct($product) {
        if (!class_exists('YITH_WC_Points_Rewards_Earning')) {
            return 0;
        }
        return YITH_WC_Points_Rewards_Earning()->calculate_product_points($product, true);
    }

    public static function printDaySelect()
    {
        ?>
        <select name="bdate_day" id="bdate_day">
            <option value="-1">יום</option>
            <?php for ($i = 1; $i <= 31; ++$i) {
                echo "<option>$i</option>";
            }?>
        </select>
        <?php
    }

    public static function printMonthSelect()
    {
        ?>
        <select name="bdate_month" id="bdate_month">
            <option value="-1">חודש</option>
            <?php for ($i = 1; $i <= 12; ++$i) {
                echo "<option>$i</option>";
            }?>
        </select>
        <?php
    }

    public static function printYearSelect()
    {
        ?>
        <select name="bdate_year" id="bdate_year">
            <option value="-1">שנה</option>
            <?php for ($i = 1920; $i <= date("Y"); ++$i) {
                echo "<option>$i</option>";
            }?>
        </select>
        <?php
    }

    public static function printQuickView($product, $cartItemKey = null)
    {
        set_query_var('qvProduct', $product);
        set_query_var('cartItemKey', $cartItemKey);

        get_template_part('templates/product/quick-view');

        GTM::printDetail($product);
    }

    public function joinClub() {
        session_start();
        if (!is_user_logged_in()) {
            $_SESSION['club_optin'] = 1;
            $_SESSION['bdate_day'] = $_POST['bdate_day'];
            $_SESSION['bdate_month'] = $_POST['bdate_month'];
            $_SESSION['bdate_year'] = $_POST['bdate_year'];
            return;
        }

        if (self::isUserInClub()) {
            return;
        } else {
            self::addUserToClub();
            add_user_meta(get_current_user_id(), 'bdate_day', $_POST['bdate_day']);
            add_user_meta(get_current_user_id(),'bdate_month', $_POST['bdate_month']);
            add_user_meta(get_current_user_id(),'bdate_year', $_POST['bdate_year']);
        }
    }

    function joinClubOnReg($user_id)
    {
        if(!isset($_SESSION))
        {
            session_start();
        }
        if (isset($_SESSION['club_optin'])) {
            self::addUserToClub($user_id);
            add_user_meta($user_id, 'bdate_day', $_SESSION['bdate_day']);
            add_user_meta($user_id,'bdate_month', $_SESSION['bdate_month']);
            add_user_meta($user_id,'bdate_year', $_SESSION['bdate_year']);
        }

        if (isset($_REQUEST['accepts_marketing'])) {
            add_user_meta($user_id, 'accepts_marketing', true);
        }
    }

    public static function showProductAverageRating($product)
    {
        $starFilename = img(round($product->get_average_rating()) . 'star.png');
        echo "<img class=\"stars\" src=\"$starFilename\"/>";
    }

    public static function getEarnedPointsByOrderId($orderId)
    {
        global $wpdb;
        $tableName = $wpdb->prefix . 'rsrecordpoints';
        return $wpdb->get_var("SELECT earnedpoints FROM $tableName WHERE orderid = $orderId");
    }

    public function renderNewsletter() {
        ob_start();
        get_template_part('templates/newsletter');
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }
    public static function renderProduct($product, $showControls = true, $template = 'thumbnail', $spreadVariations = false, $hasQuickView = true, $imageSize = 'product-thumb') {
        $spreadByTerm = 'pa_color';
        if (!$product instanceof WC_Product) {
            return;
        }
        $hoverThumb = '';
        set_query_var('idForWLandQV', $product->get_id());
        set_query_var('showControls', $showControls);
        
        if (($product instanceof WC_Product_Variable) && $spreadVariations && $product->get_attributes()[$spreadByTerm] && $product->get_attributes()[$spreadByTerm]['variation']) {
            $variations = $product->get_available_variations();
            $idx = 0;
            $printedColors = [];
            $existingTerms = wc_get_product_terms($product->get_id(), $spreadByTerm);
            foreach ($variations as $variation) {
                $CLR = $variation['attributes']['attribute_' . $spreadByTerm];
                if ((empty($CLR) && $idx > 0) || isset($printedColors[$CLR])) {
                    continue;
                }
                $foundTerm = false;
                foreach ($existingTerms as $term) {
                    if (urldecode($term->slug) === urldecode($CLR)) {
                        $foundTerm = true;
                    }
                }
                if (!$foundTerm) {
                    continue;
                }

                ++$idx;
                $printedColors[$CLR] = 1;
                $variationObject = wc_get_product($variation['variation_id']);
                $imageIds       = explode(',', get_post_meta($variation['variation_id'], '_wc_additional_variation_images', true));
                if (!empty($imageIds[0])) {
                    $hoverThumb = wp_get_attachment_image_src($imageIds[0], $imageSize)[0];
                }
                set_query_var('thumbHover', $hoverThumb);
                set_query_var('variationDataArray', $variation);
                set_query_var('productToRender', $variationObject);
                set_query_var('thumb', wp_get_attachment_image_src($variationObject->get_image_id(), $imageSize)[0]);

                $attributeName = urldecode(array_keys($variationObject->get_attributes())[0]);
                $attributeSlug = urldecode(array_values($variationObject->get_attributes())[0]);
                $attributeTerm = get_term_by('slug', $attributeSlug, $attributeName);

                set_query_var('description', $attributeTerm ? $attributeTerm->name : $attributeSlug);

                get_template_part('templates/product/' . $template);

                GTM::pushImpression($variationObject);
            }
        } else {
            set_query_var('variationDataArray', null);
            set_query_var('productToRender', $product);
            set_query_var('thumb', wp_get_attachment_image_src(get_post_thumbnail_id($product->get_id()), $imageSize)[0]);
            $imageIds = explode(',', get_post_meta($product->get_id(), '_product_image_gallery', true));
            if (!empty($imageIds[0])) {
                $hoverThumb = wp_get_attachment_image_src($imageIds[0], $imageSize)[0];
            }
            set_query_var('thumbHover', $hoverThumb);
            set_query_var('description', wp_trim_words($product->get_short_description(), 10));
            get_template_part('templates/product/' . $template);

            GTM::pushImpression($product);
        }

        if ($hasQuickView) {
            self::enqueueQuickView($product);
        }
    }

    public static function getRealAttributeValues($product) {
        $values = [];
        if ($product instanceof WC_Product_Variation) {
            foreach ($product->get_attributes() as $taxSlug => $termSlug) {
                $term          = get_term_by('slug', urldecode($termSlug), urldecode($taxSlug));
                if ($term !== null) {
                    $term->realAttributeName = wc_attribute_label($taxSlug);
                    $values[]      = $term;
                }
            }
        }

        return $values;
    }

    public static function renderCrossSellProducts($title, $subtitle, $products, $template = 'thumbnail', $hasQuickview = true, $elementID = 'slick-id') {
        set_query_var('xsell_title', $title);
        set_query_var('xsell_subtitle', $subtitle);
        set_query_var('xsell_products', $products);
        set_query_var('xsell_template', $template);
        set_query_var('element_id', $elementID);
        set_query_var('has_QV', $hasQuickview);
        get_template_part('templates/product/content/similar-products');
    }

    private function getFromPost($key) {
        return isset($_POST[$key]) ? $_POST[$key] : '';
    }

    public function ajaxRegisterUser() {
        if (isset($_POST['first_name'])) {
            $newUserId = GT::registerUser(
                $this->getFromPost('email'),
                $this->getFromPost('password'),
                $this->getFromPost('first_name'),
                $this->getFromPost('last_name'),
                $this->getFromPost('phone'),
                $this->getFromPost('bdate_day'),
                $this->getFromPost('bdate_month'),
                $this->getFromPost('bdate_year')
            );

            if ($newUserId instanceof WP_Error) {
                $validationErrors = $newUserId->get_error_message();
                echo json_encode(['error' => $validationErrors]);
                exit;
            } else {
                echo json_encode(['redirect' => wc_get_account_endpoint_url('')]);
                exit;
            }
        }
    }

    public static function isUserInClub() {
        if (isset($_SESSION['club_optin'])) {
            return true;
        }

        if (!is_user_logged_in()) {
            return false;
        }

        $userMeta = get_user_meta(get_current_user_id(), self::GETRAFFIC_CLUB_MEMBER_META, true);

        return ((!empty($userMeta) && ($userMeta !== false)));
    }

    public static function addUserToClub($user_id = null) {
        if (!is_user_logged_in() || self::isUserInClub()) {
            return;
        }

        if (!$user_id) {
            $user_id = get_current_user_id();
        }

        add_user_meta($user_id, self::GETRAFFIC_CLUB_MEMBER_META, true);
    }

    public static function registerUser(
        $email,
        $password,
        $firstName,
        $lastName,
        $phone,
        $bdayDay,
        $bdayMonth,
        $bdayYear) {

        $newUserId = wc_create_new_customer($email, '', $password);
        if ($newUserId instanceof WP_Error) {
            return $newUserId;
        }

        wp_set_current_user($newUserId);
        wp_set_auth_cookie($newUserId);

        $customer = new \WC_Customer($newUserId);

        update_user_meta( $newUserId, "billing_first_name", $firstName );
        update_user_meta( $newUserId, "billing_last_name", $lastName );
        update_user_meta( $newUserId, "billing_phone", $phone );
        $customer->set_first_name($firstName);
        $customer->set_last_name($lastName);
        $customer->set_billing_phone($phone);

        $customer->save();

        add_user_meta($newUserId, 'bdate_day', $bdayDay);
        add_user_meta($newUserId, 'bdate_month', $bdayMonth);
        add_user_meta($newUserId, 'bdate_year', $bdayYear);

        self::addUserToClub($newUserId);

        return $newUserId;
    }

    private function registerMenus()
    {
        register_nav_menu('header-menu-right', __('Header Menu Right'));
        register_nav_menu('header-menu-left', __('Header Menu Left'));
        register_nav_menu('mobile-menu-bottom', __('Mobile Menu Bottom'));

        register_nav_menu('footer-1', __('Footer Menu 1'));
        register_nav_menu('footer-2', __('Footer Menu 2'));
        register_nav_menu('footer-3', __('Footer Menu 3'));
    }

    public function validateCheckout() {
        (new CustomCheckout())->validateCheckout();
        exit;
    }

    public static function getRecentlyViewedProducts($per_page = 5) {
        // Get WooCommerce Global
        global $woocommerce;

        // Get recently viewed product cookies data
        $viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
        $viewed_products = array_filter( array_map( 'absint', $viewed_products ) );

        if ( empty( $viewed_products ) )
            return __( 'You have not viewed any product yet!', 'rc_wc_rvp' );

        // Create query arguments array
        $query_args = array(
            'posts_per_page' => $per_page,
            'no_found_rows'  => 1,
            'post_status'    => 'publish',
            'post_type'      => 'product',
            'post__in'       => $viewed_products,
            'orderby'        => 'rand'
        );

        // Add meta_query to query args
        $query_args['meta_query'] = array();

        // Check products stock status
        $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();

        // Create a new query
        $r = new WP_Query($query_args);
        $products = [];

        // If query return results
        while ($r->have_posts()) {
            $r->the_post();
            global $product;
            $products[] = $product;
        }

        // Return whole content
        return $products;
    }

    public function registerRecentlyViewed()
    {
        global $post;
        if ( is_product() ){
            $viewed_products = !empty($_COOKIE['woocommerce_recently_viewed']) ? (array) explode('|', $_COOKIE['woocommerce_recently_viewed']) : [];
            $viewed_products[] = $post->ID;
            // manipulate your cookie string here, explode, implode functions
            wc_setcookie( 'woocommerce_recently_viewed', implode('|', $viewed_products));
        }
    }

    public static function getAllCouponsForCurrentUser()
    {
        global $wpdb;
        $couponsForUser = $wpdb->get_results(
            "SELECT meta.`post_id` FROM `" .
            $wpdb->postmeta .
            "` meta WHERE  ( meta.`meta_key` LIKE 'customer_email' AND meta.`meta_value` LIKE '%" .
            wp_get_current_user()->user_email . "%' ) ");
        $couponIds = [];
        if (empty($couponsForUser)) {
            return $couponIds;
        }
        foreach ($couponsForUser as $key => $row) {
            $couponIds[] = $row->post_id;
        }

        $couponargs = [
            'post_type'      => 'shop_coupon',
            'post__in'       => $couponIds,
            'orderby'        => 'title',
            'order'          => 'ASC',
            'posts_per_page' => '-1',
        ];

        $retVal     = [];
        $coupons    = get_posts($couponargs);
        foreach ($coupons as $coupon) {
            $retVal[] = new WC_Coupon($coupon->post_title);
        }

        return $retVal;
    }

    public static function getLoopAddToCartArguments($product) {
        if ( $product ) {
            $defaults = array(
                'quantity'   => 1,
                'class'      => implode( ' ', array_filter( array(
                    'button',
                    'product_type_' . $product->get_type(),
                    $product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                ) ) ),
                'attributes' => array(
                    'data-product_id'  => $product->get_id(),
                    'data-product_sku' => $product->get_sku(),
                    'aria-label'       => $product->add_to_cart_description(),
                    'rel'              => 'nofollow',
                ),
            );

            $args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( [], $defaults ), $product );

            if ( isset( $args['attributes']['aria-label'] ) ) {
                $args['attributes']['aria-label'] = strip_tags( $args['attributes']['aria-label'] );
            }

            return $args;
        }

        return [];
    }
}

global $GT;
$GT = new GT();
