<?php
/**
 * Template Name: Questions and Answers
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container question-container">
    <div class="row col-md-8 col-md-offset-2">
        <div class="question-image"><img src="<?php echo img('questions.png'); ?>"></div>
        <div class="main-questions"><h3>שאלות ותשובות</h3></div>
        <?php         
        $questions =  gt_get_field('main');
        if($questions){
        $i=1;
        foreach ($questions as $question){ 
            echo ($i%2) ? "<div class='question'>" . $question  . "</div>" : "<div  class='answer'>" . $question . "</div>"; 
            $i++;    
        }
        }?>
        <div class="main-questions"><img src="<?php echo img('question-icon-orders.png'); ?>"><h3>הזמנה באתר</h3></div>
        <?php $questions =  gt_get_field('orders');
        if($questions){
            $i=1;
        foreach ($questions as $question){ 
            echo ($i%2) ? "<div class='question'>" . $question  . "</div>" : "<div  class='answer'>" . $question . "</div>"; 
            $i++;    
        }
        }?>
        <div class="main-questions"><img src="<?php echo img('question-icon-payments.png'); ?>"><h3>תשלומים</h3></div>
        <?php $questions =  gt_get_field('payments');
        if($questions){
            $i=1;
        foreach ($questions as $question){ 
            echo ($i%2) ? "<div class='question'>" . $question  . "</div>" : "<div  class='answer'>" . $question . "</div>"; 
            $i++;    
        }
        }?>
        <div class="main-questions"><img src="<?php echo img('question-icon-products.png'); ?>"><h3>מוצרים</h3></div>
        <?php $questions =  gt_get_field('products');
        if($questions){
            $i=1;
        foreach ($questions as $question){ 
            echo ($i%2) ? "<div class='question'>" . $question  . "</div>" : "<div  class='answer'>" . $question . "</div>"; 
            $i++;    
        }
        }?>
    </div>
</div>
<?php get_footer(); ?>
<style>
    .row{
        margin-bottom: 100px;
    }
    .main-questions{
        width: 100%;
        padding: 20px 0;
        border-bottom: 1px solid #000;
    }
    .main-questions img{
        float: right;
        padding: 0 0 0 15px;
    }
    .main-questions h3{
        margin: 5px 0 0 0;
    }
    .question-image{
        width: 100%;
        text-align: center;
    }
    .question-container h3{
/*
        margin-top: 50px;
        border-bottom: 1px solid #000;
        width: 100%;
        margin-bottom: 0;
        padding-bottom: 20px;
*/
    }
    .question{
        width: 100%;
        border-bottom: 1px solid #000;
        padding: 20px 0;
        cursor: pointer;
    }
    .question.active{
        border-bottom: none;
        padding-right: 20px;
    }
    .answer{
        display: none;
        border-bottom: 1px solid #000;
        padding: 20px 20px 20px 0;
    }
    .active{
        background: #f0dde3;
    }
</style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        let $ = jQuery;
        $(".question").click(function(){   
            if ($(this).hasClass("active")){
                $(this).removeClass("active")
            }else{$(this).addClass('active');}

            $(this).next().toggle('fast'); 
        });
    });
</script>