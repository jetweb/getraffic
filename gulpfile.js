"use strict";

/**
 * CLI commands:
 *
 * @site-url - The site URL (only needed on a production task)
 * @is-rtl - When the css start direction is RTL
 *
 * gulp - Will run default task and will watch
 *  ex: gulp --is-rtl
 *
 * gulp production - Will run production task
 *  NOTE: To use this task you must pass the {site-url} parameter
 *  ex: gulp production --site-url="http://localhost/project-name" --is-rtl
 *
 * Using .env
 *  NODE_ENV
 *  CSS_START_DIRECTION ?? trl || ltr
 *  SITE_URL
 */

/**
 * Load plugins
 * ----------------------------------------
 */
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const {
    src,
    dest,
    watch,
    series,
    parallel
}
    = require('gulp');

const concat                = require('gulp-concat');
const sass                  = require('gulp-sass');
sass.compiler               = require('node-sass');
const globImporter          = require('node-sass-glob-importer');
const concatCss             = require('gulp-concat-css');
const sourcemaps            = require('gulp-sourcemaps');
const babel                 = require('gulp-babel');
const plumber               = require('gulp-plumber');
const minifyJS              = require('gulp-minify');
//const uglify                = require('gulp-uglify');
const uglify                = require('gulp-uglify-es').default;
const postcss               = require('gulp-postcss');
const uncss                 = require('postcss-uncss');
const mediaQueriesSplitter  = require('gulp-media-queries-splitter');
const cleanCSS              = require('gulp-clean-css');
const path                  = require('path');
const fs                    = require('fs');
const fse                   = require('fs-extra')
const argv                  = require('yargs').argv;
const del                   = require('del');
const livereload            = require('gulp-livereload');
const glob                  = require('glob');
const axios                 = require('axios');
const curlirize             = require('axios-curlirize');

// This module is an axios third-party module to log any axios request as a curl command in the console
curlirize( axios );

/**
 * Constants List
 * ----------------------------------------
 */
const ASSETS_DIR =          path.join(__dirname, 'assets');
const DIST_DIR =            path.join(ASSETS_DIR, 'dist');
const BUILD_DIR =           path.join(ASSETS_DIR, 'build');
const SRC_DIR =             path.join(ASSETS_DIR, 'src');
const VENDOR_DIR =          path.join(ASSETS_DIR, 'vendor');
const SCSS_DIR =            path.join(SRC_DIR, 'scss');
const SASS_CONFIG_DIR =     path.join(SCSS_DIR, 'config');
const SASS_CONFIG_FILE =    path.join(SASS_CONFIG_DIR, '_config.scss');
const CSS_DIST_DIR =        path.join(DIST_DIR, 'css');
const JS_DIST_DIR =         path.join(DIST_DIR, 'js');
const CSS_BUILD_DIR =       path.join(BUILD_DIR, 'css');
const JS_BUILD_DIR =       path.join(BUILD_DIR, 'js');
const SITEMAP_FILE =        path.join(__dirname, 'sitemap.json');

//const uncssignore           = require(path.join(VENDOR_DIR, 'uncssignore'));

const uncssignore = [
    ///.site-navbar.menu-open\ [a-zA-Z0-9\-\_\:\(\)\ \.]+/,
    /^.site\-navbar([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/,
    /.pp\-[a-zA-Z0-9\-\_\:\(\)\ \.]+/,
    /^.overlay([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/,
    /^.search--open([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/,
    /^@([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/, // -> keyframes
    /.slick\-[a-zA-Z0-9\-\_\:\(\)\ \.]+/,
    /^(.page-recommended[\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)|.page-recommended/, //
    /^.fade([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/,
    /^(.dropdown-menu[\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)|.dropdown-menu$/,
    /^(.modal[\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)|.modal$/,
    /^(.sr-only[\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)|.sr-only/,
    /^.-pre-([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/, // ex: -pre-active
    /^.-pre-([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)([\ \a-zA-Z\-_0-9\>\+\.\:\(\)]+)$/, // ex: -pre-active::before
];

/**
 * Gulp tasks utilities
 * ----------------------------------------
 */

const get_memory_usage = function () {
    const used = process.memoryUsage();
    const calc = ( memory ) => parseFloat(  ( memory  / (1024 * 1024) ) ).toFixed(2);
    return {
        rss: calc( used.rss ),
        heapTotal: calc( used.heapTotal ),
        heapUsed: calc( used.heapUsed ),
        external: calc( used.external )
    }
};

// Clean assets css
const clean = (dir) => ( async() => await del(dir) )();

// Initial minify css
const minify = () => cleanCSS( {level: {1: {specialComments: 0}}} );

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

// Create fiels by media queries
const media_queries_splitter_init = ( file_basename ) => {
    const direction = argv.direction;
    return mediaQueriesSplitter([
        {media:['none', {minUntil: '767px'}], filename: `${file_basename}.${direction}.css`},
        {media: [{min: '768px', minUntil: '991px'}], filename: `${file_basename}-above-768.${direction}.css`},
        {media: [{min: '992px', minUntil: '1199px'}], filename: `${file_basename}-above-992.${direction}.css`},
        {media: [{min: '1200px', minUntil: '1599px'}], filename: `${file_basename}-above-1200.${direction}.css`},
        {media: [{min: '1600px'}], filename: `${file_basename}-above-1600.${direction}.css`},
    ])
};

// Initial uncss
const uncss_init = html => postcss( [uncss({html, ignore: uncssignore})] );

const site_map_parse = function ( sitemap_file_path ) {
    console.log('Run site map parse');
    const sitemap_file = JSON.parse( fs.readFileSync(sitemap_file_path) )
        .filter( ( page_html, index ) => {
            console.log('Run site map parse index:', index);
            return page_html
                .toString()
                .trim()
                .length > 0
        });
    return ( () => sitemap_file )
};

const get_uncss_result = function ( ) {
    if( argv.is_uncss ) {
        const sitemap_result = ('site_map_parse' in argv) ? argv.site_map_parse() : site_map_parse( SITEMAP_FILE )();
        console.log('Run uncss with memory usage of:', get_memory_usage() );
        return uncss_init( sitemap_result );
    }
    return require('through2').obj((chunk, enc, cb) => cb(null, chunk) );
};

/**
 * Gulp handle tasks functions
 * ----------------------------------------
 */

const init_default = async function ( done ) {
    argv.is_uncss = false;
    argv.site_url = process.env.SITE_URL;
    argv.is_rtl = ( 'is-rtl' in argv );

    if( argv.is_rtl ) {
        argv.direction = 'rtl';
    }
    else {
        argv.direction = 'ltr';
        argv.site_url = argv.site_url.concat('/en');
    }
    done();
};

// Initial global uncss variablesf
const init_production = async function ( done ) {
    fs.existsSync(SITEMAP_FILE) && fs.unlinkSync(SITEMAP_FILE);
    argv.is_uncss = true;
    console.log( 'Start initial uncss mode...' );
    done();
};

const get_sitemap = async ( url ) => {
    const response = await axios.get(url, {
        method: 'GET',
        responseType: 'json',
    });
    return response.data;
};

const sitemap = async function(cb) {
    const url = `${argv.site_url}?show_sitemap`;
    console.log( 'Start sitemap url:', url );
    const output = await get_sitemap(url, SITEMAP_FILE);
    fs.writeFileSync( SITEMAP_FILE, JSON.stringify(output) );
    console.log('Start site map parse');
    while( !fs.existsSync( SITEMAP_FILE) ) {
        console.log('Sitemap file does not exists');
        sleep('500')
    }
    argv.site_map_parse = site_map_parse( SITEMAP_FILE );
    console.log(`Finish site map parse entities:`, argv.site_map_parse().length );
    cb();
};

const icons = function() {
    const plugins_src = [
        path.join(VENDOR_DIR, 'fontawesome/dist/css/**/*.css'),
        path.join(SCSS_DIR, `icons.scss`)
    ];

    let task = (
        src(plugins_src, { base: SCSS_DIR })
            .pipe( concatCss(`icons.css`) )
    );

    return task.pipe( minify() ).pipe( dest(CSS_DIST_DIR) );
};


const vendor_styles = function() {

    const vendor_prefix = 'vendor.';

    const get_vendor = ( vendor ) => path.join(VENDOR_DIR, vendor );

    const styles = [
        get_vendor('bootstrap/css/bootstrap.min.css' ),
        get_vendor('bootstrap/css/bootstrap-rtl.css' ),
        get_vendor('ekko/ekko-lightbox.css' ),
        get_vendor('slicknav/slicknav.min.css'),
        get_vendor('slick-carousel/slick.css'),
        get_vendor('jquery-ui/jquery-ui.min.css'),
    ];


    const plugins_src = {
        styles,
    };

    const init = (name) => {
        return new Promise(resolve => {
            let task = (
                src( plugins_src[name] )
                    .pipe( concatCss(`${vendor_prefix + name.toString().replace(/_/g, '-')}.${argv.direction}.css`) )
                    .pipe( dest(CSS_BUILD_DIR) )
                    //.pipe( sourcemaps.init() )
                    .pipe( minify() )
                    //.pipe( sourcemaps.write('.') )
                    .pipe( dest(CSS_DIST_DIR) )
                    .on('end', () => resolve(task) )
            );
        });
    };

    const vendors_tasks_async = async () => Promise.all( Object.keys(plugins_src).map( async ( vendor_name ) => await init( vendor_name ) ) );

    return ( async() => await vendors_tasks_async() )();
};

const styles = function (cb) {
    const file_basename = `main`;
    const file_path = path.join(SCSS_DIR, `${file_basename}.scss`);

    const get_file = ( file ) => path.join( SRC_DIR, `scss/${file}` );

    let task = src( [
        get_file('modal.css'),
        get_file('slider.css'),
        get_file('menu.css'),
        get_file('topbar.css'),
        get_file('mini-cart.css'),
        get_file('login-popup.css'),
        get_file('sticky-header.css'),
        get_file('quick-view.css'),
        get_file('cart.scss'),
        get_file('style.scss'),
    ] );

    task = (
        task
        .pipe( sass().on('error', sass.logError) )
        .pipe( concatCss(`main.css`) )
        //.pipe( media_queries_splitter_init(file_basename) )

            .pipe( minify() )
    );

    if( argv.is_uncss ) {
        task = (
            task
            .pipe( dest(CSS_BUILD_DIR) )
            .pipe( minify() )
        );
    }

    return task.pipe( dest(CSS_DIST_DIR) );
};

const vendor_scripts = function() {

    const get_vendor = ( vendor ) => path.join(VENDOR_DIR, vendor );

    const scripts_src = [
        'node_modules/promise-polyfill/dist/polyfill.min.js',
        'node_modules/@babel/polyfill/dist/polyfill.min.js',

        get_vendor('jquery-ui/jquery-ui.min.js'),
        get_vendor('popper/popper.min.js'),
        get_vendor('bootstrap/js/bootstrap.min.js'),
        get_vendor('ekko/ekko-lightbox.min.js'),
        get_vendor('slicknav/jquery.slicknav.min.js'),
        get_vendor('moment/moment.js'),
        get_vendor('slick-carousel/slick.min.js')
    ];

    return (
        src( scripts_src )
        .pipe( plumber() )
        .pipe( babel() )
        .pipe( concat('chunk-vendors.js') )
        .pipe( dest( JS_BUILD_DIR ) )
        .pipe( uglify() )
        .pipe( dest( JS_DIST_DIR ) )
    );
};

const scripts = () => {

    const get_file = ( file ) => path.join( SRC_DIR, `js/${file}` );

    let task = (
        src( [
            get_file('functions.js'),
            get_file('ajax-add-to-cart.js'),
            get_file('additional-variation-images-fix.js'),
            get_file('infinite-scroll.js'),
            get_file('header.js'),
            get_file('search.js'),
            get_file('newsletter.js'),
        ] )
            .pipe( plumber() )
            .pipe( babel() )
            .pipe( concat('scripts.js') )
    );

    return (
        task
            .pipe( sourcemaps.init({loadMaps: true}) )
            .pipe( dest( JS_BUILD_DIR ) )
            //.pipe( uglify() )
            .pipe( sourcemaps.write('.') )
            .pipe( dest( JS_DIST_DIR ) )
    );

};

const concat_scripts = () => {
    return (
        src([
            path.join(JS_BUILD_DIR, 'chunk-vendors.js'),
            path.join(JS_BUILD_DIR, 'scripts.js')
        ])
            .pipe( plumber() )
            .pipe( uglify() )
            .pipe( concat('bundle.js') )
            .pipe( dest( JS_DIST_DIR ) )
    )
};

const reload = (done) => {
    livereload.reload();
    done();
};

const watching = function () {
    watch( './assets/src/**/*.scss' , { delay: 500 }, series(styles) );
    watch( './assets/src/js/**/*.js', series(scripts) );
};

/**
 * Gulp tasks list
 * ----------------------------------------
 */

const build = parallel( vendor_styles, styles, vendor_scripts, scripts);

exports.js = series(
    init_default,
    scripts,
    reload
);

exports.default = series(
    init_default,
    build,
);

exports.production = series(
    init_default,
    init_production,
    sitemap,
    build,
    concat_scripts
);



