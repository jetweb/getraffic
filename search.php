<?php

use Getraffic\AjaxSearch;

/**
 * Template Name: Search Results Index
 *
 * The template for displaying pages without the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
global $wp_query;
get_header();
$products = AjaxSearch::getMatchingProducts($_REQUEST['s'], -1);
?>
<div class="container" id="primary">
    <?php
    if (empty($products)) {
        ?>
    
    <div class="container">
        <div class="content">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="nothing-found" dir="rtl">לא הצלחנו למצוא את מה שחיפשת. רוצה לנסות שוב?</h2>
                </div>
            </div>
        </div>
    </div>
    <?php
        //TODO: remove! use `renderCrossSellProducts` instead!
$posts = gt_get_field('homepage_best_sellers', 2);
$products = [];
foreach ($posts['best_sellers'] as $post) {
    $products[] = wc_get_product($post->ID);
}
?>
<div class="container xsell-container">
    <h1 class="best-match">BEST SELLERS</h1>
    <h2 class="best-match">מוזמנת גם לצפות במוצרים הכי נמכרים שלנו</h2>
    <div class="position-relative">
        <div class="xsell-products">
            <?php
            foreach ($products as $product) {
                echo '<div>';
                GT::renderProduct($product);
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>

        <?php } else {
        ?>
        <div class="search-word"><?php echo $_REQUEST['s']; ?></div>
        <p class="results-found"> נמצאו <?= $wp_query->found_posts; ?> תוצאות חיפוש</p>
        <div class="products">
            <?php
            foreach ($products as $product) {
                    if (isset($product) && $product->exists()) {
                        GT::renderProduct($product);
                        get_template_part('templates/popups/quick-view');
                    } else {
                        ?>
                        <div class="product-wrapper">
                            <div class="thumb-wrapper">
                                <a href="<?php the_permalink(); ?>">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="" class="thumb">
                                </a>
                            </div>
                            <div class="product-info">
                                <a href="<?php the_permalink(); ?>"><h1 class="title"><?php the_title(); ?></h1></a>
                                <h2 class="description"><?php echo wp_trim_words(get_the_excerpt(), 10); ?></h2>
                            </div>
                        </div>
            <?php }
            } ?>
        </div>
    <?php } ?>
</div>

<style>
    .search-word, .nothing-found {
        font-size: 28px;
        text-align: center;
    }
    p.results-found {
        font-weight: 100;
        margin-bottom: 40px;
    }

</style>

<?php get_footer(); ?>
<script>
    jQuery(document).ready(function () {
        let $ = jQuery;
        initSlickCarousel($('.xsell-products'), false, true, 4, [
            {
                breakpoint: 2220,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 10,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]);
    });
</script>

<style>
    .text-404{
    margin-bottom: 0px;
    }
    .prod-thumb-bottom{
        left: -9999px;
    }
    
    h2.best-match {
        text-align: left;
        margin-bottom: 35px;
    }

    h1.best-match {
        text-align: left;
        font-size: 80px;
        font-weight: 300;
        margin-top: 35px;
    }

    .xsell-container {
        position: relative;
        margin: 80px auto;
    }

    .xsell-products {
        display: flex;
    }

    .xsell-products .thumb-wrapper {
        width: 350px;
        height: 350px;
    }

    .product-wrapper {
        display: inline-block;
        width: auto;
    }
    @media screen and (max-width: 768px) {
        .xsell-products .thumb-wrapper {
            width: 230px;
            height: 230px;
        }
    }
</style>