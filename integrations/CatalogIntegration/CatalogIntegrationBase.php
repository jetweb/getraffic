<?php

namespace Getraffic;

abstract class CatalogIntegrationBase
{
    protected $logger;

    private $storedMainSKUs = [];

    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function createOrUpdateProduct(\ProductData $productData)
    {
        //*** Handle main product details and creation ***//
        $isVariarion = (!empty($productData->getParentSku()) && ($productData->getParentSku() !== $productData->getSku()));
        $mainSKU     = $isVariarion ? $productData->getParentSku() : $productData->getSku();
        $id          = wc_get_product_id_by_sku($mainSKU);
        if (empty($id)) {
            if (!$isVariarion) {
                $product = new \WC_Product();
                $product->set_status('draft');
            } else {
                $product = new \WC_Product_Variable();
                $product->set_status('draft');
            }
            $product->set_sku($mainSKU);
            $this->logger->log('creating new product with SKU #' . $mainSKU);
        } else {
            $product = wc_get_product($id);
            if (!$isVariarion) {
                $this->logger->log('Product already exists with SKU #' . $mainSKU . ' - updating');
            }
        }
        // Only manage stock on product level if no attributes
        if (!$isVariarion) {
            $product->set_manage_stock(true);
            $product->set_stock_quantity($productData->getStockAmount());
        } else {
            $product->set_manage_stock(false);
            $product->set_backorders('no');
        }
        if (!$isVariarion || !in_array($mainSKU, $this->storedMainSKUs)) {
            $product->set_name($productData->getTitle());
            $product->set_regular_price((string) ($productData->getPrice()));
            $product->set_description($productData->getDescription());
            //*** Handle Taxonomies (categories) ***//
            $catTermId    = 0;
            $catSubTermId = 0;
            if (!empty($productData->getCategory())) {
                $catTermId = $this->getOrCreateCategory($productData->getCategory());
            }
            if (!empty($productData->getCategory())) {
                $catSubTermId = $this->getOrCreateCategory($productData->getSubCategory(), $catTermId);
            }
            //*** Save it ***//
            $id = $product->save();
            wp_set_object_terms($product->get_id(), [(int) $catTermId, (int) $catSubTermId], 'product_cat', true);
            $this->logger->log('stored product #' . $id . ' with SKU #' . $mainSKU);
            if ($isVariarion) {
                $this->storedMainSKUs[] = $mainSKU;
            }
        } else {
            // just save the stock management
            $product->save();
        }
        //*** If this is a variable product - create this as a variation ***//
        if ($isVariarion) {
            $variation_id = wc_get_product_id_by_sku($productData->getSku());
            if (empty($variation_id)) {
                $variation_post = [
                    'post_title'  => $product->get_title(),
                    'post_name'   => 'product-' . $product->get_id() . '-variation',
                    'post_status' => 'publish',
                    'post_parent' => $product->get_id(),
                    'post_type'   => 'product_variation',
                    'guid'        => $product->get_permalink(),
                ];
                $variation_id   = wp_insert_post($variation_post);
                $this->logger->log('Creating variation #' . $productData->getSku() . ' for main product SKU #' . $mainSKU);
            } else {
                $this->logger->log('Updating variation #' . $productData->getSku() . ' for main product SKU #' . $mainSKU);
            }
            // Sometimes that also happens
            if (!empty($productData->getAttributes())) {
                // Get an instance of the WC_Product_Variation object
                $variation = new \WC_Product_Variation($variation_id);
                foreach ($productData->getAttributes() as $attrName => $attrValue) {
                    $this->upsertVariationAttribute($product, $variation_id, $attrName, $attrValue, $attrName === 'size');
                }
                $variation->set_sku($productData->getSku());
                $variation->set_name($productData->getTitle());
                $variation->set_regular_price((string) ($productData->getPrice()));
                $variation->set_description($productData->getDescription());
                $variation->set_stock_quantity($productData->getStockAmount());
                if ($productData->getStockAmount() == 0) {
                    $variation->set_stock_status('outofstock');
                }
                $variation->set_backorders('no');
                $variation->set_manage_stock(true);
                $variation->save();
            }
        }

        return $product;
    }

    private function getOrCreateCategory($name, $parentId = 0)
    {
        $catTerm = str_replace(' ', '-', trim($name));
        $result  = term_exists($catTerm, 'product_cat');
        if (!$result) {
            $result = wp_insert_term(
                $catTerm,
                'product_cat',
                [
                    'description' => $catTerm,
                    'slug'        => $catTerm,
                    'parent'      => $parentId,
                ]
            );
        }
        if ($result instanceof \WP_Error) {
            $this->logger->log('Error : ' . $result->get_error_message());

            return 0;
        }
        $catTermId = $result['term_id'];

        return $catTermId;
    }

    /**
     * @param \WC_Product_Variable $product
     * @param                      $variation_id
     * @param                      $attribute
     * @param                      $term_name
     */
    private function upsertVariationAttribute($product, $variation_id, $attribute, $term_name, $forVariations = false)
    {
        $taxonomy = 'pa_' . $attribute; // The attribute taxonomy
        // If taxonomy doesn't exists we create it (Thanks to Carl F. Corneil)
        $this->ensureAttributeAndTermExist($taxonomy, $attribute);
        // Check if the Term name exist and if not we create it.
        $termArray = term_exists($term_name, $taxonomy);
        if (empty($termArray)) {
            $termArray = wp_insert_term($term_name, $taxonomy);
        }
        $term_slug = get_term_by('name', $term_name, $taxonomy)->slug; // Get the term slug
        $this->upsertMainProductAttribute($product, $taxonomy, (int) $termArray['term_id'], (int) $termArray['term_taxonomy_id'], $forVariations);
        // Set/save the attribute data in the product variation
        update_post_meta($variation_id, 'attribute_' . $taxonomy, $term_slug);
    }

    /**
     * @param \WC_Product $product
     * @param             $taxonomy
     * @param             $term_id
     * @param             $term_taxonomy_id
     * @param bool        $forVariations
     */
    private function upsertMainProductAttribute($product, $taxonomy, $term_id, $term_taxonomy_id, $forVariations = false)
    {
        if ($product instanceof \WC_Product_Variation) {
            return; // This should not happen
        }
        $attributes = $product->get_attributes();
        $options    = [];
        if (in_array($taxonomy, array_keys($attributes))) {
            $options = $attributes[$taxonomy]['options'];
            if (!in_array($term_id, $options)) {
                $options[] = $term_id;
            }
        } else {
            $options[] = $term_id;
        }
        $attributeObject = new \WC_Product_Attribute();
        $attributeObject->set_id($term_taxonomy_id);
        $attributeObject->set_name($taxonomy);
        $attributeObject->set_options($options);
        $attributeObject->set_variation($forVariations);
        $attributes[$taxonomy] = $attributeObject;
        $product->set_attributes($attributes);
        $product->save();
    }

    private function ensureAttributeAndTermExist($taxonomy, $attribute)
    {
        if (!taxonomy_exists($taxonomy)) {
            wc_create_attribute([
                'name' => ucfirst($attribute),
                'slug' => sanitize_title($attribute),
            ]);
            register_taxonomy(
                $taxonomy,
                ['product', 'product_variation'],
                [
                    'hierarchical' => false,
                    'label'        => ucfirst($attribute),
                    'query_var'    => true,
                    'rewrite'      => ['slug' => sanitize_title($attribute)], // The base slug
                ]
            );
        }
    }

    public function removeProductsNotInArrayBySKU($array)
    {
        $products = wc_get_products([
            'posts_per_page' => -1,
        ]);
        foreach ($products as $product) {
            if (in_array($product->get_sku(), $array)) {
                $this->logger->log($product->get_sku() . ' exists in array, skipping...');
            } else {
                $this->logger->log($product->get_sku() . ' is missing from array, deleting...');
                $this->removeProduct($product);
            }
        }
    }

    public function updateProductsStockBySKU($array)
    {
        foreach ($array as $sku => $amount) {
            $id      = wc_get_product_id_by_sku($sku);
            $product = wc_get_product($id);
            if (empty($product)) {
                $this->logger->log('SKU not found in local databse! : ' . $sku);
                continue;
            }
            // Only manage stock on product level if no attributes
            if (!$product instanceof \WC_Product_Variable) {
                $product->set_manage_stock(true);
                $product->set_stock_quantity($amount);
                if ($amount == 0) {
                    $product->set_stock_status('outofstock');
                }
                $product->save();
                $this->logger->log('updated ' . $amount . ' items in stock for SKU ' . $product->get_sku());
            } else {
                $product->set_manage_stock(false);
                $product->set_backorders('no');
                $this->logger->log('not managing stock info for parent variable with SKU ' . $product->get_sku());
            }
        }

        return true;
    }

    public function removeProduct(\WC_Product $product)
    {
        $prodID = $product->get_id();
        if ($product->is_type('variable')) {
            foreach ($product->get_children() as $child_id) {
                $child = wc_get_product($child_id);
                if ($child) {
                    $child->delete(true);
                    $this->logger->log('successfully deleted child product with ID ' . $child_id);
                }
            }
        } elseif ($product->is_type('grouped')) {
            foreach ($product->get_children() as $child_id) {
                $child = wc_get_product($child_id);
                $child->set_parent_id(0);
                $child->save();
            }
        }
        $product->delete(true);
        $success = $product->get_id() > 0 ? false : true;
        if ($success) {
            $this->logger->log('successfully deleted product with ID ' . $prodID);

            return true;
        } else {
            $this->logger->log('failed deleting product with ID ' . $prodID);

            return false;
        }
    }
}
