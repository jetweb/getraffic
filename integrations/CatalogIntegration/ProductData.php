<?php

class ProductData
{
    private $sku;
    private $parentSku;
    private $title;
    private $price;
    private $salePrice;
    private $category;
    private $subCategory;
    private $description;
    private $attributes;
    private $stockAmount;

    public function addAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }

    /**
     * @param mixed $sku
     *
     * @return ProductData
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $title
     *
     * @return ProductData
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     *
     * @return ProductData
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param mixed $salePrice
     *
     * @return ProductData
     */
    public function setSalePrice($salePrice)
    {
        $this->salePrice = $salePrice;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * @param mixed $category
     *
     * @return ProductData
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $subCategory
     *
     * @return ProductData
     */
    public function setSubCategory($subCategory)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * @param mixed $description
     *
     * @return ProductData
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @param mixed $parentSku
     *
     * @return ProductData
     */
    public function setParentSku($parentSku)
    {
        $this->parentSku = $parentSku;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getParentSku()
    {
        return $this->parentSku;
    }

    /**
     * @param mixed $stockAmount
     *
     * @return ProductData
     */
    public function setStockAmount($stockAmount)
    {
        $this->stockAmount = $stockAmount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStockAmount()
    {
        return $this->stockAmount;
    }
}
