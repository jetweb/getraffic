$(document).on('wc_fragments_refreshed removed_from_cart added_to_cart', function() {
    $.ajax({
        type: 'post',
        url: ajax_url,
        data: {
            action: 'get_gtm_bufferred_event'
        },
        beforeSend: function () {
        },
        complete: function () {
        },
        success: function (response) {
            try {
                dataLayer.push(JSON.parse(response));
            } catch (Err) {
                console.log(Err);
            }
        },
    });

});

$(document).ready(function() {
    $('.product-wrapper').click(function() {
        try {
            if ($(this).data('gtmclickpayload')) {
                dataLayer.push(JSON.parse($(this).data('gtmclickpayload')));
            }
        } catch (Err) {
            console.log(Err);
        }
    })
});
