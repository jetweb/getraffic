<?php

namespace Getraffic;

use GT;
use WC_Order;

class GTM implements ConfigurableModule
{
    const SETTINGS_PAGE_ID = 'google-tag-manager-settings-page';
    private $settings;

    public function __construct()
    {
        $this->settings = get_option(self::SETTINGS_PAGE_ID);

        add_action("wp_ajax_get_gtm_bufferred_event", [$this, 'get_gtm_bufferred_event']);
        add_action("wp_ajax_nopriv_get_gtm_bufferred_event", [$this, 'get_gtm_bufferred_event']);
        add_action('wp_init', function () {

        });

        add_action('wp_head', function () {
            ?>
            <script>
                dataLayer = [
                    <?php if (is_user_logged_in()) { ?>
                    {
                        'userId': <?= get_current_user_id() ?>,
                        'userEmail': '<?= get_user_by('id', get_current_user_id())->user_email; ?>',
                    }
                    <?php } ?>
                ];
            </script>
            <?php

            wp_enqueue_script('gtm-getraffic', get_template_directory_uri() . '/integrations/GoogleTagManager/GTM.js');
        });

        add_action('wp_head', function () {
            ?>
            <!-- Google Tag Manager -->
            <script>(function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({
                        'gtm.start':
                            new Date().getTime(), event: 'gtm.js'
                    });
                    var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', '<?= $this->settings['gtm-id'] ?>');</script>
            <!-- End Google Tag Manager -->
            <?php
        });

        add_action('after_body_start', function () {
            ?>
            <!-- Google Tag Manager (noscript) -->
            <noscript>
                <iframe src="https://www.googletagmanager.com/ns.html?id=<?= $this->settings['gtm-id'] ?>"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe>
            </noscript>
            <!-- End Google Tag Manager (noscript) -->
            <div class="gtm-layer"></div>
            <?php
        });

        add_action('wp_footer', function () {
            self::printImpressions();
        });

        add_action('woocommerce_after_single_product', function () {
            global $product;
            self::printDetail($product);
        });

        add_action('woocommerce_remove_cart_item', function ($cart_item_key) {
            $cart_item = WC()->cart->get_cart_item( $cart_item_key );
            if ( $cart_item ) {
                $removedProduct  = wc_get_product($cart_item['product_id']);
                $category = GT::getFirstTopLevelCategory($removedProduct);
                $catName = '';
                if ($category) {
                    $catName = $category->name;
                }
                $_SESSION['gtmBufferredEvent'] = '
                {
                    "event": "removedFromCart",
                        "ecommerce": {
                            "currencyCode": "ILS",
                            "remove": {
                            "products": [{
                                "name": "'. $removedProduct->get_name() . '",
                                "id": "'. $removedProduct->get_id() . '",
                                "price": "'. $removedProduct->get_price() . '",
                                "brand": "'. get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] . '",
                                "category": "'. $catName . '",
                                "quantity": '. $cart_item['quantity'] . '
                            }]
                        }
                    }
                }';
            }
        }, 10, 1);

        add_action('woocommerce_ajax_added_to_cart', function ($product_id, $quantity) {
            if (!isset($_SESSION)) { session_start(); }
			if (!$product_id) $product_id=$_REQUEST['product_id'];
            $addedProduct  = wc_get_product($product_id);
            $category = GT::getFirstTopLevelCategory($addedProduct);
            $catName = '';
            if ($category) {
                $catName = $category->name;
            }
            $_SESSION['gtmBufferredEvent'] = '
            {
                "event": "addedToCart",
                    "ecommerce": {
                        "currencyCode": "ILS",
                        "add": {
                        "products": [{
                            "name": "'. $addedProduct->get_name() . '",
                            "id": "'. $addedProduct->get_id() . '",
                            "price": "'. $addedProduct->get_price() . '",
                            "brand": "'. get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] . '",
                            "category": "'. $catName . '",
                            "quantity": '. $quantity . '
                        }]
                    }
                }
            }';
        }, 10, 2);
    }

    public static function getClickPayload($product)
    {
        global $theCounter;
        if (empty($theCounter)) {
            $theCounter = 1;
        } else {
            ++$theCounter;
        }

        $list  = 'Category Page';
        if (is_product()) {
            $list = 'Product Page';
        } else if (is_category()) {
            $list = 'Category Page';
        } else if (is_search()) {
            $list = 'Search Results';
        }

        $category = GT::getFirstTopLevelCategory($product);
        $catName = '';
        if ($category) {
            $catName = $category->name;
        }

        $payload = '
            
            {
                "event": "productClick",
                    "ecommerce": {
                        "currencyCode": "ILS",
                        "click": {
                        "actionField": {"list": "'. $list .'"},
                        "products": [{
                            "name": "'. $product->get_name() . '",
                            "id": "'. $product->get_id() . '",
                            "price": "'. $product->get_price() . '",
                            "brand": "'. get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] . '",
                            "category": "'. $catName . '",
                            "position": "'. $theCounter .'"
                        }]
                    }
                }
            }';

        return htmlentities($payload);
    }

    public function get_gtm_bufferred_event() {
        if (!isset($_SESSION)) { session_start(); }
        echo $_SESSION['gtmBufferredEvent'];
        $_SESSION['gtmBufferredEvent'] = null;
        exit;
    }

    public static function printImpressions()
    {
        global $gtmProductImpressions;
        $index = 1;
        $list  = 'Category Page';
        if (is_product()) {
            $list = 'Product Page';
        } else if (is_category()) {
            $list = 'Category Page';
        } else if (is_search()) {
            $list = 'Search Results';
        }
        ?>
        <script>
            if (dataLayer) {
                dataLayer.push({
                    'event': 'ProductImpression',
                    'ecommerce': {
                        'currencyCode': 'ILS',
                        'impressions': [
                            <?php
                            if ($gtmProductImpressions && is_array($gtmProductImpressions)) {                        /** @var \WC_Product $product */
                            foreach ($gtmProductImpressions as $product) {
                            ?>
                            {
                                'name': '<?=str_replace('\'', '\\\'', $product->get_name())?>',
                                'id': '<?= $product->get_id(); ?>',
                                'price': '<?= $product->get_price(); ?>',
                                'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                                'category': '<?php
                                    $category = GT::getFirstTopLevelCategory($product);
                                    if ($category) {
                                        echo str_replace('\'', '\\\'', $category->name);
                                    }
                                    ?>',
                                'position': <?=$index++;?>,
                                'list': '<?=$list?>'
                            },
                            <?php } } ?>
                        ]
                    }
                });
            }
        </script>
        <?php
        if (is_product()) {
            global $product;

            $content_ids = [$product->get_id()];
            if ($product instanceof \WC_Product_Variable) {
                $content_ids = '[';
                foreach ($product->get_available_variations() as $variation) {
                    $content_ids .= $variation['variation_id'] . ',';
                }
                $content_ids .= ']';
            }

            ?>
            <script>
                dataLayer.push({
                    'name': '<?=str_replace('\'', '\\\'', $product->get_name())?>',
                    "event": "contentView",
                    "contentType": "product",
                    "value": '<?= $product->get_price(); ?>',
                    'id': '<?= $product->get_id(); ?>',
                    'content_ids': <?= $content_ids ?>,
                    'price': '<?= $product->get_price(); ?>',
                    'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                })
            </script>
            <?php
        }
    }

    public static function printDetail(\WC_Product $product)
    {
        $content_ids = [$product->get_id()];
        if ($product instanceof \WC_Product_Variable) {
            $content_ids = '[';
            foreach ($product->get_available_variations() as $variation) {
                $content_ids .= $variation['variation_id'] . ',';
            }
            $content_ids .= ']';
        }
        ?>
        <script>
            if (dataLayer) {
                dataLayer.push({
                    'event': 'ProductDetail',
                    'ecommerce': {
                        'currencyCode': 'ILS',
                        'detail': {
                            'products': [{
                                'name': '<?=str_replace('\'', '\\\'', $product->get_name())?>',
                                'id': '<?=$product->get_id()?>',
                                'content_ids': <?= $content_ids ?>,
                                'price': '<?=$product->get_price()?>',
                                'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                                'category': '<?php
                                    $category = GT::getFirstTopLevelCategory($product);
                                    if ($category) {
                                        echo str_replace('\'', '\\\'', $category->name);
                                    }
                                    ?>',
                            }]
                        }
                    }
                });
            }
        </script>
        <?php
    }

    public static function pushImpression(\WC_Product $product)
    {
        global $gtmProductImpressions;
        if (empty($gtmProductImpressions)) {
            $gtmProductImpressions = [];
        }
        $gtmProductImpressions[] = $product;
    }

    public static function pushCheckoutStep($stepNumber) {
        ?>
        <script>
            function pushCheckoutStep(stepNum) {
                if (dataLayer) {
                    dataLayer.push({
                        'event': 'checkout',
                        'ecommerce': {
                            'currencyCode': 'ILS',
                            'checkout': {
                                'actionField': {'step': stepNum, 'option': '<?php echo is_user_logged_in() ? 'Authenticated' : 'אנונימי' ?>'},
                                'products': [
                                    <?php
                                    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                                    ?>
                                    {
                                        'name': '<?=str_replace('\'', '\\\'', $_product->get_name())?>',
                                        'id': '<?= $_product->get_id() ?>',
                                        'price': '<?= $_product->get_price() ?>',
                                        'quantity': '<?= $cart_item['quantity'] ?>',
                                        'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                                        'category': '<?php
                                            $category = GT::getFirstTopLevelCategory($_product);
                                            if ($category) {
                                                echo str_replace('\'', '\\\'', $category->name);
                                            }
                                            ?>',
                                    },
                                    <?php } ?>
                                ]
                            }
                        }
                    });
                }
            }
        </script>
        <?php
    }

    public static function pushOrderCompleted($order) {
        if (!$order instanceof WC_Order) {
            return;
        }
        foreach ($order->get_meta_data() as $meta) {
            if ($meta->key === 'pushed_to_gtm') {
                return;
            }
        }
        if ($order->get_status() === 'completed' || $order->get_status() === 'processing' || $order->get_status() === 'on-hold') {
            ?>
            <script>
                console.log("should dataLayer event purchase", dataLayer, {
                    'event': 'purchase',
                    'ecommerce': {
                        'currencyCode': 'ILS',
                        'purchase': {
                            'actionField': {
                                'id': '<?= $order->get_id(); ?>',
                                'revenue': '<?= $order->get_total(); ?>',
                                'shipping': '<?= $order->get_shipping_total(); ?>',
                                // 'coupon': 'SUMMER_SALE'
                            },
                            'products': [
                                <?php
                                $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
                                foreach ($order_items as $item_id => $item) {
                                $_product = wc_get_product($item['product_id']);
                                if (!empty($item['variation_id'])) {
                                    $_product = wc_get_product($item['variation_id']);
                                }
                                ?>
                                {
                                    'name': '<?=str_replace('\'', '\\\'', $item->get_name())?>',
                                    'id': '<?= $_product->get_id(); ?>',
                                    'price': '<?= $item->get_total(); ?>',
                                    'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                                    'category': '<?php
                                        $category = GT::getFirstTopLevelCategory($_product);
                                        if ($category) {
                                            echo str_replace('\'', '\\\'', $category->name);
                                        }
                                        ?>',
                                    'quantity': <?= $item->get_quantity(); ?>,
                                },
                                <?php } ?>
                            ]
                        }
                    }
                });
                if (dataLayer) {
                    dataLayer.push({
                        'event': 'purchase',
                        'ecommerce': {
                            'currencyCode': 'ILS',
                            'purchase': {
                                'actionField': {
                                    'id': '<?= $order->get_id(); ?>',
                                    'revenue': '<?= $order->get_total(); ?>',
                                    'shipping': '<?= $order->get_shipping_total(); ?>',
                                    // 'coupon': 'SUMMER_SALE'
                                },
                                'products': [
                                    <?php
                                    $order_items = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
                                    foreach ($order_items as $item_id => $item) {
                                    $_product = wc_get_product($item['product_id']);
                                    if (!empty($item['variation_id'])) {
                                        $_product = wc_get_product($item['variation_id']);
                                    }
                                    ?>
                                    {
                                        'name': '<?=str_replace('\'', '\\\'', $item->get_name())?>',
                                        'id': '<?= $_product->get_id(); ?>',
                                        'price': '<?= $item->get_total(); ?>',
                                        'brand': '<?= get_option(self::SETTINGS_PAGE_ID)['gtm-brand'] ?>',
                                        'category': '<?php
                                            $category = GT::getFirstTopLevelCategory($_product);
                                            if ($category) {
                                                echo str_replace('\'', '\\\'', $category->name);
                                            }
                                            ?>',
                                        'quantity': <?= $item->get_quantity(); ?>,
                                    },
                                    <?php } ?>
                                ]
                            }
                        }
                    });
                }
            </script>
            <?php

             $order->add_meta_data('pushed_to_gtm', true);
             $order->save();
        }
    }

    public static function getSettingsPage()
    {
        /** Register Admin Settings Page */
        $section = new AdminSettingsSection();
        $section
            ->setName('general-settings')
            ->setTitle('הגדרות Google Tag Manager')
            ->setSectionInfo('Google Tag Manager Settings')
            ->setFields(
                [
                    (new AdminSettingsField())
                        ->setName('gtm-id')
                        ->setTitle('GTM ID (GTM-XXXXXXX)'),
                    (new AdminSettingsField())
                        ->setName('gtm-brand')
                        ->setTitle('Default Brand Name'),
                ]
            );

        return new AdminSettingsPage(
            self::SETTINGS_PAGE_ID,
            'הגדרות Google Tag Manager',
            'הגדרות Google Tag Manager',
            'menu-settings-gt-gtm',
            [$section]);
    }
}
