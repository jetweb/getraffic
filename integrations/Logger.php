<?php

namespace Getraffic;

class Logger {
    private $filepath = null;
    private $handle = null;
    private $echo = false;
    private $dateformat = 'd/m/Y H:i:s';

    public function __construct($filepath = null, $echo = false)
    {
        $this->echo = $echo;
        if (php_sapi_name() !== 'cli' && $this->echo === true) {
            ob_implicit_flush(true);
            ob_start();
        }

        $this->filepath = $filepath;

        if ($this->filepath) {
            $this->make_dir(dirname($this->filepath));
            if (!file_exists($this->filepath)) {
                fclose(fopen($this->filepath, 'w'));
            }
        }
    }

    public function log($message) {
        if (php_sapi_name() !== 'cli' && $this->echo === true) {
            echo date($this->dateformat) . ' - ' . $message . PHP_EOL;
            ob_flush();
            flush();
        }

        if (!$this->filepath) {
            echo date($this->dateformat) . ' - ' . $message . (php_sapi_name() === 'cli' ? PHP_EOL : '<br />');
            return;
        }

        $fp = fopen($this->filepath, 'a');
        fwrite($fp, date($this->dateformat) . ' - ' . $message . PHP_EOL);
        fclose($fp);
    }

    private function make_dir( $path, $permissions = 0777 ) {
        return is_dir( $path ) || mkdir( $path, $permissions, true );
    }
}
