$(document).ready(function () {
    let url = new URL(location.href);
    let filterName = 'filter_category';
    let s = url.searchParams.get(filterName);
    if (s) {
        let cats = s.split(',');
        for (cat in cats) {
            $('a[data-category-id="' + cats[cat] + '"]').parent().addClass('chosen');
        }
    }
    $('ul.product-categories li:not(.chosen) a').click(function () {
        let catId = $(this).data('category-id');
        let url = new URL(location.href);
        let s = url.searchParams.get(filterName);
        if (s) {
            s += ',' + catId;
        } else {
            s = catId;
        }
        url.searchParams.set(filterName, s);
        location.href = url;
        return false;
    });
    $('ul.product-categories li.chosen a').click(function () {
        let catId = $(this).data('category-id');
        let url = new URL(location.href);
        let s = url.searchParams.get(filterName);
        if (!s) {
            return;
        }
        s = s.replace(catId, '');
        url.searchParams.set(filterName, s);
        location.href = url;
        return false;
    });
});