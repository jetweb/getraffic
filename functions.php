<?php
include 'GT.php';
include 'includes/CustomMenuWalker.php';

/** Scripts and Styles */
function bootstrapstarter_enqueue_styles()
{
    $ver = '20202605';
    if (WP_DEBUG === true) {
        $ver = time();
    }

    $ver = time();
    //wp_enqueue_style('font-most-secondary', get_template_directory_uri() . '/assets/fonts/Assistant/style.css', [], $ver);
    //wp_enqueue_style('font-secondary', get_template_directory_uri() . '/assets/fonts/Assistant/style.css', [], $ver);
    wp_enqueue_style('font-main', get_template_directory_uri() . '/assets/fonts/NarkisBlockMFW/style.css', [], $ver);

    //wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css', [], $ver);
    //wp_enqueue_style('bootstrap-rtl', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap-rtl.css', [], $ver);
    $dependencies = [
        'bootstrap',
        'bootstrap-rtl',
    ];

    //wp_enqueue_style('ekko-css', get_template_directory_uri() . '/assets/vendor/ekko/ekko-lightbox.css', $dependencies, $ver);
    //wp_enqueue_style('slicknav-css', get_template_directory_uri() . '/assets/vendor/slicknav/slicknav.min.css', $dependencies, $ver);


   // wp_enqueue_style('modal', get_template_directory_uri() . '/assets/src/scss/modal.css', $dependencies, $ver);
   // wp_enqueue_style('slider', get_template_directory_uri() . '/assets/src/scss/slider.css', $dependencies, $ver);
   // wp_enqueue_style('menu', get_template_directory_uri() . '/assets/src/scss/menu.css', $dependencies, $ver);
   // wp_enqueue_style('topbar', get_template_directory_uri() . '/assets/src/scss/topbar.css', $dependencies, $ver);
   // wp_enqueue_style('mini-cart', get_template_directory_uri() . '/assets/src/scss/mini-cart.css', $dependencies, $ver);
    //wp_enqueue_style('login-popup', get_template_directory_uri() . '/assets/src/scss/login-popup.css', $dependencies, $ver);
    //wp_enqueue_style('sticky-header', get_template_directory_uri() . '/assets/src/scss/sticky-header.css', $dependencies, $ver);
    //wp_enqueue_style('qv-css', get_template_directory_uri() . '/assets/src/scss/quick-view.css', [], $ver);

    //wp_enqueue_style('slick-carousel', get_template_directory_uri() . '/assets/vendor/slick-carousel/slick.css', [], $ver);

    //GT::enqueueAsset('modal', '/assets/src/scss/modal.css', $dependencies, $ver);
    //GT::enqueueAsset('slider', '/assets/src/scss/slider.css', $dependencies, $ver);
   // GT::enqueueAsset('menu', '/assets/src/scss/menu.css', $dependencies, $ver);
    //GT::enqueueAsset('topbar', '/assets/src/scss/topbar.css', $dependencies, $ver);
    //GT::enqueueAsset('mini-cart', '/assets/src/scss/mini-cart.css', $dependencies, $ver);
    //GT::enqueueAsset('login-popup', '/assets/src/scss/login-popup.css', $dependencies, $ver);
    //GT::enqueueAsset('sticky-header', '/assets/src/scss/sticky-header.css', $dependencies, $ver);
    //GT::enqueueAsset('qv-css', '/assets/src/scss/quick-view.css', [], $ver);

    //GT::enqueueAsset('slick-carousel', '/assets/vendor/slick-carousel/slick.css', [], $ver);

   wp_enqueue_style( 'fontawesome-css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

    wp_enqueue_style( 'vendor', get_template_directory_uri() . '/assets/dist/css/vendor.styles.rtl.css', [], $ver);
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/dist/css/main.css', [], $ver);

    //wp_enqueue_style('master-style', get_template_directory_uri() . '/style.css',[], $ver);
    //wp_enqueue_style('master-style-mobile', get_template_directory_uri() . '/mobile.css', [], $ver);
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style-child.css?v=1.1', [], $ver);
    //wp_enqueue_style('child-mobile-style', get_stylesheet_directory_uri() . '/mobile.css',[], $ver);

    //wp_enqueue_style('jquery-ui-css', get_template_directory_uri() . '/assets/vendor/jquery-ui/jquery-ui.min.css', [], $ver);

    //GT::enqueueAsset('cart', '/assets/src/scss/cart.css', [], $ver);
    //wp_enqueue_style('cart', get_template_directory_uri() . '/assets/src/scss/cart.css', [], $ver);

}

function bootstrapstarter_enqueue_scripts()
{
    $ver = '20202605';
    if (WP_DEBUG === true) {
        $ver = time();
    }
    $ver = time();
    wp_enqueue_script( 'wc-add-to-cart-variation' );

    $dependencies = ['jquery'];

    wp_enqueue_script('chunk-vendors', get_template_directory_uri() . '/assets/dist/js/chunk-vendors.js', ['jquery'], $ver, true);
    wp_enqueue_script('scripts', get_template_directory_uri() . '/assets/dist/js/scripts.js', ['chunk-vendors'], $ver, true);

    //wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/assets/vendor/jquery-ui/jquery-ui.min.js', ['jquery']);
    //wp_enqueue_script('popper', get_template_directory_uri() . '/assets/vendor/popper/popper.min.js', $dependencies, $ver, true);
    //wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/js/bootstrap.min.js', $dependencies, $ver, true);
    //wp_enqueue_script('ekko-js', get_template_directory_uri() . '/assets/vendor/ekko/ekko-lightbox.min.js', $dependencies, $ver, true);
    //wp_enqueue_script('slicknav', get_template_directory_uri() . '/assets/vendor/slicknav/jquery.slicknav.min.js', $dependencies, $ver, true);
    //wp_enqueue_script('moment', get_template_directory_uri() . '/assets/vendor/moment/moment.js', $dependencies, $ver, true);
    //wp_enqueue_script('slickslider', get_template_directory_uri() . '/assets/vendor/slick-carousel/slick.min.js', $dependencies, $ver, true);

   // wp_enqueue_script('functions', get_template_directory_uri() . '/assets/src/js/functions.js', $dependencies, $ver, true);
   // wp_enqueue_script('ajax-add-to-cart', get_template_directory_uri() . '/assets/src/js/ajax-add-to-cart.js', $dependencies, $ver, true);
    //wp_enqueue_script('variations-fix', get_template_directory_uri() . '/assets/src/js/additional-variation-images-fix.js', $dependencies, $ver, true);
    //wp_enqueue_script('infinite-scroll', get_template_directory_uri() . '/assets/src/js/infinite-scroll.js', $dependencies, $ver, true);
    //wp_enqueue_script('header-js', get_template_directory_uri() . '/assets/src/js/header.js', $dependencies, $ver, true);
    //wp_enqueue_script('search-js', get_template_directory_uri() . '/assets/src/js/search.js', $dependencies, $ver, true);
    //wp_enqueue_script('newsletter-js', get_template_directory_uri() . '/assets/src/js/newsletter.js', $dependencies, $ver, true);

    //GT::enqueueAsset('header-js', '/assets/src/js/header.js', $dependencies, true);
    //GT::enqueueAsset('search-js', '/assets/src/js/search.js', $dependencies, true);
    //GT::enqueueAsset('newsletter-js', '/assets/src/js/newsletter.js', $dependencies, true);
}

add_filter( 'wc_additional_variation_images_custom_swap', '__return_true');

add_action('wp_enqueue_scripts', 'bootstrapstarter_enqueue_styles');
add_action('wp_enqueue_scripts', 'bootstrapstarter_enqueue_scripts');

/** Sidebar and widgets */
function clean_widgets_init()
{
    register_sidebar([
        'name'          => 'Blog Sidebar',
        'id'            => 'sidebar_1',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => 'Footer 1',
        'id'            => 'footer_1',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => 'Footer 2',
        'id'            => 'footer_2',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => 'Footer 3',
        'id'            => 'footer_3',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => 'Footer 4',
        'id'            => 'footer_4',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);

    register_sidebar([
        'name'          => 'Footer 5',
        'id'            => 'footer_5',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ]);
}

add_action('widgets_init', 'clean_widgets_init');
// Add Thumbnail Support




function img($filename)
{
    return GT::img($filename);
}

function category_list()
{
    ob_start();
    get_template_part('shortcodes/categories');

    return ob_get_clean();
}

add_shortcode('category-list', 'category_list');



add_filter('woocommerce_breadcrumb_defaults', 'wcc_change_breadcrumb_delimiter');
function wcc_change_breadcrumb_delimiter($defaults)
{
    // Change the breadcrumb delimeter from '/' to '>'
    $defaults['delimiter'] = ' &gt; ';

    return $defaults;
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'page_title' => 'הגדרות תבנית עיצוב',
        'menu_title' => 'הגדרות תבנית עיצוב',
        'capability' => 'edit_posts',
        'menu_slug'  => 'theme-general-settings',
    ]);

    acf_add_options_page([
        'page_title' => 'הגדרות עגלת קניות',
        'menu_title' => 'הגדרות עגלת קניות',
        'capability' => 'edit_posts',
        'menu_slug'  => 'cart-settings',
    ]);

    acf_add_options_page([
        'page_title' => 'נקודות, קופה ותשלום',
        'menu_title' => 'נקודות, קופה ותשלום',
        'capability' => 'edit_posts',
        'menu_slug'  => 'checkout-settings',
    ]);

    acf_add_options_page([
        'page_title' => 'הגדרות החשבון שלי',
        'menu_title' => 'הגדרות החשבון שלי',
        'capability' => 'edit_posts',
        'menu_slug'  => 'my-account-settings',
    ]);

    acf_add_options_page([
        'page_title' => 'ניהול badges',
        'menu_title' => 'ניהול badges',
        'capability' => 'edit_posts',
        'menu_slug'  => 'product-badges-settings',
    ]);

    acf_add_options_page([
        'page_title' => 'הגדרות עמוד קטגוריה',
        'menu_title' => 'הגדרות עמוד קטגוריה',
        'capability' => 'edit_posts',
        'menu_slug'  => 'my-archive-settings',
    ]);
}

add_filter('woocommerce_add_to_cart_fragments', 'add_cart_count', 10, 1);
function add_cart_count($fragments)
{
    $fragments['span.mini-count'] = '<span class="mini-count">' . WC()->cart->get_cart_contents_count() . '</span>';
    return $fragments;
}



add_filter( 'woocommerce_single_product_carousel_options', 'slider_options' );
function slider_options( $options ) {
    $options['directionNav'] = true;
    //$options['controlNav'] = 'control-thumbs';
    return $options;
}

remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display');
if (defined('PRESERVE_WOO_STYLES') && PRESERVE_WOO_STYLES === true) {

} else {
    add_filter('woocommerce_enqueue_styles', '__return_empty_array');
}

function extractFields($name, $object) {
    $fields = gt_get_field($name);
    if (!$fields && is_object($object) && isset($object->term_id)) {
        $fields = gt_get_field($name, 'product_cat_' . $object->term_id);
    }
    if (!$fields && is_shop()) {
        $fields = gt_get_field($name, get_option( 'woocommerce_shop_page_id' ));
    }

    return $fields;
}

function layered_nav_add_icons($term_html, $term, $link, $count)
{
    $image_url = '';
    if (function_exists('woo_variation_swatches')) {
        $attachment_id = absint(get_term_meta($term->term_id, 'product_attribute_image', true));
        $image_size    = woo_variation_swatches()->get_option('attribute_image_size');
        $image_url     = wp_get_attachment_image_url($attachment_id, apply_filters('wvs_product_attribute_image_size', $image_size));
    }
    $link = apply_filters('woocommerce_layered_nav_link', $link, $term, $term->taxonomy);
    if ($image_url) {
        return '
        <a class="layered-image attr-' . $term->taxonomy . '" data-tax="' . $term->taxonomy . '" data-term_slug="' . $term->slug . '" data-term_name="' . esc_html($term->name) . '" rel="nofollow" href="' . $link . '"><img src="' . $image_url . '" alt=""></a> <span class="count">(' . $count . ')</span>
        ';
    } else {
        return '
        <a class="layered-button attr-' . $term->taxonomy . '" data-tax="' . $term->taxonomy . '" data-term_slug="' . $term->slug . '" data-term_name="' . esc_html($term->name) . '" rel="nofollow" href="' . $link . '">' . esc_html($term->name) . '</a> <span class="count">(' . $count . ')</span>
        ';
    }
}
add_filter('woocommerce_layered_nav_term_html', 'layered_nav_add_icons', 10, 4);

add_action( 'woocommerce_product_query', 'on_sale_products_in_product_category_archives', 20, 2 );
function on_sale_products_in_product_category_archives( $q, $query ) {

	if (is_shop() || is_page('shop')) {

		if( ! isset($_GET['onsale']) )
			return;

		if( $_GET['onsale'] === '1' ){

		$product_ids_on_sale = wc_get_product_ids_on_sale();

		$q->set( 'post__in', $product_ids_on_sale );
		}

	}
    // Only on product category archive pages
    if( is_admin() || is_search() || ! is_product_category() )
        return;

    if( ! isset($_GET['onsale']) )
        return;

    if( $_GET['onsale'] === '1' ){
        // Get the queried product category term ID
        $term_id = get_queried_object()->term_id; 

        // Get related "on sale" product Ids for this product category ID
        global $wpdb;
        $product_ids = $wpdb->get_col( "
            SELECT p.ID FROM {$wpdb->prefix}posts as p
            INNER JOIN {$wpdb->prefix}term_relationships as tr ON p.ID = tr.object_id
            INNER JOIN {$wpdb->prefix}term_taxonomy as tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
            INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
            WHERE ( p.ID IN ( SELECT DISTINCT p2.ID FROM {$wpdb->prefix}posts as p2
                INNER JOIN {$wpdb->prefix}postmeta as pm ON p2.ID = pm.post_id
                INNER JOIN {$wpdb->prefix}postmeta as pm2 ON p2.ID = pm2.post_id
                AND pm.meta_value != pm2.meta_value
                WHERE p2.post_type = 'product' AND p2.post_status = 'publish'
                AND pm.meta_key LIKE '_regular_price' AND pm2.meta_key LIKE '_sale_price'
                AND pm2.meta_value != ''
            ) OR p.ID IN ( SELECT DISTINCT p3.post_parent FROM {$wpdb->prefix}posts as p3
                INNER JOIN {$wpdb->prefix}postmeta as pm3 ON p3.ID = pm3.post_id
                INNER JOIN {$wpdb->prefix}postmeta as pm4 ON p3.ID = pm4.post_id
                AND pm3.meta_value != pm4.meta_value
                WHERE p3.post_type = 'product_variation' AND p3.post_status = 'publish'
                AND pm3.meta_key LIKE '_regular_price' AND pm4.meta_key LIKE '_sale_price'
                AND pm4.meta_value != ''  AND p3.post_parent != 0
            ) ) AND tt.taxonomy LIKE 'product_cat' AND tt.term_id = $term_id
        " );

        // Set those products IDs in the WP_Query
        $q->set('post__in', $product_ids);
    }
}


add_filter( 'woocommerce_login_redirect', function ( $redirect, $user ) {
    if (isset($_REQUEST['return_url'])) {
        return $_REQUEST['return_url'];
    }
    return $redirect;
}, 10, 3 );


add_filter( 'woocommerce_email_customer_details_fields', function ( $fields, $sent_to_admin, WC_Order $order ){
    global $GT;

    if (class_exists('WC_Pelecard_Transaction')) {
        $transactions = get_post_meta($order->get_id(), '_transaction_data');
        foreach ($transactions as $transaction_data) {
            $transaction = new WC_Pelecard_Transaction(null, $transaction_data);
            $cardNum     = $transaction->CreditCardNumber;
            $last4digits = '';
            if ($cardNum && strlen($cardNum) > 3) {
                $last4digits = substr($cardNum, strlen($cardNum) - 4);
            }
            if ($last4digits) {
                $fields['creditcard4num'] = [
                    'label' => 'כרטיס אשראי - 4 ספרות אחרונות',
                    'value' => $last4digits,
                ];
            }
            if ($transaction->CardHolderName) {
                $fields['CardHolderName'] = [
                    'label' => 'כרטיס אשראי - שם',
                    'value' => $transaction->CardHolderName
                ];
            }
            if ($transaction->CardHolderID) {
                $fields['CardHolderID'] = [
                    'label' => ' ת.ז. לקוח',
                    'value' => $transaction->CardHolderID
                ];
            }
        }
    }

    $fields['phone'] = [
        'label' => 'מספר טלפון',
        'value' => $order->get_billing_phone()
    ];

    $fields['email'] = [
        'label' => 'אימייל',
        'value' => $order->get_billing_email()
    ];

    $fields['full_address'] = [
        'label' => 'כתובת מלאה',
        'value' => $GT->getFullFormattedAddress($order)
    ];

    $fields['notes'] = [
        'label' => 'הערות',
        'value' => $order->get_customer_note()
    ];

    return $fields;
}, 10, 3 );



include 'includes/already_printed.php';
require_once 'includes/shipping_hooks.php';
