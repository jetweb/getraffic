<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php get_template_part('templates/header/head'); ?>
<body <?php bloginfo('charset'); ?> <?php body_class(); ?>>
<?php do_action('after_body_start'); ?>
<div class="no-padding no-margin header-container">
    <div class="sticky">
        <?php get_template_part('templates/header/topbar'); ?>
        <?php get_template_part('templates/header/menu/index'); ?>
    </div>
    <?php get_template_part('templates/header/bg-image'); ?>
    <?php get_template_part('templates/header/slider'); ?>
</div>

<?php get_template_part('templates/header/script/additional'); ?>
