<?php

function updateShippingMethods($array) {
    ob_start();
    get_template_part('templates/checkout/shipping');
    $ob = ob_get_contents();
    ob_end_clean();
    $array['.checkout-shipping'] = $ob;

    return $array;
}
add_filter('woocommerce_update_order_review_fragments', 'updateShippingMethods');

add_filter( 'woocommerce_package_rates', 'unset_shipping_when_free_is_available', 10, 2 );
function unset_shipping_when_free_is_available( $rates, $package ) {
    $freeExists = false;
    foreach (array_keys($rates) as $key) {
        if (strpos($key, 'free') !== false) {
            $freeExists = true;
        }
    }

    if ($freeExists) {
        foreach (array_keys($rates) as $key) {
            if (strpos($key, 'flat_rate') !== false) {
                unset($rates[$key]);
            }
        }
    }

    return $rates;
}

add_action("admin_menu", function() {
    add_menu_page("Shipping method settings", "Shipping method settings","manage_options", "shipping_method_description", function (){
        include "shipping_settings.php";
    },plugins_url('/customplugin/img/icon.png'));
});
