<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class CustomTaxonomy
 *
 * @package WPHelper
 */
class CustomTaxonomy
{
    /**
     * @var array
     */
    private $labels = [];

    /**
     * @var string
     */
    private $slug = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var bool
     */
    private $multiple = false;

    /**
     * @var bool
     */
    private $hierarchical = false;

    /**
     * @param array $labels
     *
     * @return CustomTaxonomy
     */
    public function setLabels($labels)
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param string $slug
     *
     * @return CustomTaxonomy
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $name
     *
     * @return CustomTaxonomy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param boolean $multiple
     *
     * @return CustomTaxonomy
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @return array|int|\WP_Error
     */
    public function getItems($level = 0)
    {
        if ($this->isHierarchical()) {
            $terms = get_terms($this->getName(), [
                'hide_empty'   => false,
                'hierarchical' => true,
                'parent'       => 0,
            ]);

            foreach ($terms as $term) {
                $childTerms = get_terms($this->getName(), [
                    'hide_empty'   => false,
                    'hierarchical' => true,
                    'parent'       => $term->term_id,
                ]);

                $term->children = $childTerms;
            }

            return $terms;
        } else {
            return get_terms($this->getName(), ['hide_empty' => false]);
        }
    }

    public function renderAdmin($post)
    {
        $terms         = get_terms($this->getName(), ['hide_empty' => false]);
        $termsSelected = wp_get_object_terms($post->ID, $this->getName(), ['orderby' => 'term_id', 'order' => 'ASC']);
        $name          = '';
        if (!is_wp_error($termsSelected)) {
            if (isset($termsSelected[0]) && isset($termsSelected[0]->name)) {
                $name = $termsSelected[0]->name;
            }
        }
        ?><select title="<?= $this->getName() ?>" name="<?= $this->getName() ?>">
        <option value="<?= $this->getName() . '_nonce' ?>">-- <?= $this->getLabels()['singular_name'] ?> --</option><?php
        foreach ($terms as $term) {
            //if ($this->isHierarchical() == false || $term->parent != 0) {
            if (true) {
                ?>
                    <option <?php selected($term->name, $name); ?> value="<?= $term->term_id ?>"><?php esc_html_e($term->name); ?></option>
                <?php
            }
        }
        ?></select><br><?php
    }

    /**
     * @param boolean $hierarchical
     *
     * @return CustomTaxonomy
     */
    public function setHierarchical($hierarchical)
    {
        $this->hierarchical = $hierarchical;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isHierarchical()
    {
        return $this->hierarchical;
    }
}
