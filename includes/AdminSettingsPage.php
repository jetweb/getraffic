<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class PluginSettings
 *
 * @package WPHelper
 */
class AdminSettingsPage
{
    /**
     * @var array
     */
    protected $sections = [];

    /**
     * @var string
     */
    protected $pageId;

    /**
     * @var string
     */
    protected $pageTitle;

    /**
     * @var string
     */
    protected $menuTitle;

    /**
     * @var string
     */
    protected $slug;

    /**
     * PluginSettingsBase constructor.
     *
     * @param $sections
     */
    public function __construct($pageId, $pageTitle, $menuTitle, $slug, $sections)
    {
        $this->pageId = $pageId;
        $this->pageTitle = $pageTitle;
        $this->menuTitle = $menuTitle;
        $this->slug = $slug;
        $this->sections = $sections;

        add_action('admin_menu', [$this, 'addPluginPage']);
        add_action('admin_init', [$this, 'pageInit']);
    }

    public function addPluginPage()
    {
        // This page will be under "Settings"
        add_options_page(
            $this->pageTitle,
            $this->menuTitle,
            'edit_posts',
            $this->pageId,
            [$this, 'renderAdminPage']
        );
    }

    public function pageInit()
    {
        register_setting(
            $this->pageId, // Option group
            $this->pageId // Option name
        );

        /** @var AdminSettingsSection $section */
        foreach ($this->sections as $section) {
            add_settings_section(
                $section->getName(), // ID
                $section->getTitle(), // Title
                [$section, 'printSectionInfo'], // Callback
                $this->pageId // Page
            );

            /** @var AdminSettingsField $field */
            foreach ($section->getFields() as $field) {
                add_settings_field(
                    $field->getName(), // ID
                    $field->getTitle(), // Title
                    [$field, 'renderField'], // Callback
                    $this->pageId, // Page
                    $section->getName(), // Section
                    ['optionGroup' => $this->pageId]
                );
            }
        }
    }

    public function renderAdminPage()
    {
        ?>
        <div class="wrap">
            <h1><?= $this->menuTitle ?></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields($this->pageId);
                do_settings_sections($this->pageId);
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * @return array
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @param array $sections
     *
     * @return PluginSettingsBase
     */
    public function setSections($sections)
    {
        $this->sections = $sections;

        return $this;
    }
}