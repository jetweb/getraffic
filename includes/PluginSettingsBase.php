<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class PluginSettings
 *
 * @package WPHelper
 */
abstract class PluginSettingsBase
{
    /**
     * @var array
     */
    protected $sections = [];

    private $options;


    /**
     * PluginSettingsBase constructor.
     *
     * @param $sections
     */
    public function __construct($sections)
    {
        $this->sections = $sections;

        add_action('admin_menu', [$this, 'addPluginPage']);
        add_action('admin_init', [$this, 'pageInit']);
    }

    public function addPluginPage()
    {
        // This page will be under "Settings"
        add_options_page(
            $this->getPageTitle(),
            $this->getMenuTitle(),
            'edit_posts',
            $this->getPageId(),
            [$this, 'renderAdminPage']
        );
    }

    public function pageInit()
    {
        register_setting(
            $this->getPageId(), // Option group
            $this->getPageId() // Option name
        );

        /** @var AdminSettingsSection $section */
        foreach ($this->sections as $section) {
            add_settings_section(
                $section->getName(), // ID
                $section->getTitle(), // Title
                [$section, 'printSectionInfo'], // Callback
                $this->getPageId() // Page
            );

            /** @var AdminSettingsField $field */
            foreach ($section->getFields() as $field) {
                add_settings_field(
                    $field->getName(), // ID
                    $field->getTitle(), // Title
                    [$field, 'renderField'], // Callback
                    $this->getPageId(), // Page
                    $section->getName(), // Section
                    ['optionGroup' => $this->getPageId()]
                );
            }
        }
    }

    public function renderAdminPage()
    {
        ?>
        <div class="wrap">
            <h1><?= $this->getMenuTitle() ?></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields($this->getPageId());
                do_settings_sections($this->getPageId());
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    abstract protected function getPageId();

    abstract protected function getPageTitle();

    abstract protected function getMenuTitle();

    abstract protected function getSlug();

    /**
     * @return array
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @param array $sections
     *
     * @return PluginSettingsBase
     */
    public function setSections($sections)
    {
        $this->sections = $sections;

        return $this;
    }
}