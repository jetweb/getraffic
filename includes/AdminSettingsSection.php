<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class AdminSettingsSection
 *
 * @package WPHelper
 */
class AdminSettingsSection
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var string
     */
    private $sectionInfo = '';

    /**
     * @param string $name
     *
     * @return AdminSettingsSection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $title
     *
     * @return AdminSettingsSection
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param array $fields
     *
     * @return AdminSettingsSection
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    public function printSectionInfo()
    {
        echo $this->sectionInfo;
    }

    /**
     * @param string $sectionInfo
     *
     * @return AdminSettingsSection
     */
    public function setSectionInfo($sectionInfo)
    {
        $this->sectionInfo = $sectionInfo;

        return $this;
    }

    /**
     * @return string
     */
    public function getSectionInfo()
    {
        return $this->sectionInfo;
    }
}
