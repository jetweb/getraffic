<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

require_once(__DIR__ . '/CustomPostType.php');
require_once(__DIR__ . '/CustomTaxonomy.php');
require_once(__DIR__ . '/CustomMetaBox.php');
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
require_once(__DIR__ . '/../vendor/autoload.php');

use WP_Query;

/**
 * Class PluginBase
 *
 * @package WPHelper
 */
abstract class PluginBase implements AjaxControllerInterface
{
    /**
     * @var int
     */
    const DB_VERSION = 1.0;

    /**
     * @var string
     */
    const DB_VERSION_OPTION = 'PluginDbVersion';

    /**
     * Wpdb
     */
    protected $db;

    /**
     * PluginBase constructor.
     */
    public function __construct()
    {
        // Save local reference to the wpdb
        global $wpdb;
        $this->db = $wpdb;

        add_action('add_meta_boxes', [$this, 'addCustomMetaBoxes']);
        add_action('save_post', [$this, 'customMetaSave'], 10, 3);
        add_action('plugins_loaded', [$this, 'initTextDomain']);
        if (method_exists($this, 'enqueueAdminScripts')) {
            add_action('admin_enqueue_scripts', [$this, 'enqueueAdminScripts']);
        }
        add_action('init', [$this, 'init']);
        add_action('save_post', [$this, 'postStatusChanged'], 10, 3);
    }

    /**
     * Install the database
     */
    public static function install()
    {
        static::installDatabase();
    }

    /**
     * Initializes the text domain for translation
     */
    public function initTextDomain()
    {
        load_plugin_textdomain($this->getTextDomain(), false, dirname(plugin_basename(__FILE__)) . '/../languages/');
    }

    /**
     * Drop the database tables
     */
    public static function uninstall()
    {
        global $wpdb;
        foreach (static::getUninstallSqlSequences() as $sql) {
            $wpdb->get_results($sql);
        }

        // Update database version option
        delete_option(static::DB_VERSION_OPTION);
    }

    public static function installDatabase()
    {
        $installed_ver = get_option(static::DB_VERSION);

        if ($installed_ver != static::DB_VERSION) {
            foreach (static::getInstallSqlSequences() as $sql) {
                dbDelta($sql);
            }
        }

        // Update database version option
        update_option(static::DB_VERSION_OPTION, static::DB_VERSION);
    }

    /**
     * You might want to call these function from the theme's `functions.php` to get it to work smoothly
     */
    public function init()
    {
        $this->registerPostTypes();
        $this->addCustomMetaBoxes();
    }

    /**
     * Registers the custom post type in Wordpress
     */
    private function registerPostTypes()
    {
        if (!$this->getCustomPostTypes()) {
            return;
        }

        /** @var CustomPostType $customPostType */
        foreach ($this->getCustomPostTypes() as $customPostType) {
            register_post_type($customPostType->getName(),
                [
                    'labels'            => $customPostType->getLabels(),
                    'public'            => true,
                    'show_in_menu'      => true,
                    'has_archive'       => true,
                    'show_in_admin_bar' => true,
                    'menu_icon'         => $customPostType->getIcon(),
                    'rewrite'           => ['slug' => $customPostType->getSlug()],
                ]
            );

            add_post_type_support($customPostType->getName(), 'thumbnail');

            //flush_rewrite_rules();
        }

        $taxonomiesToPostTypes = [];
        $customTaxonomies      = [];
        foreach ($this->getCustomPostTypes() as $customPostType) {
            /** @var CustomTaxonomy $customTaxonomy */
            foreach ($customPostType->getCustomTaxonomies() as $customTaxonomy) {
                $taxonomiesToPostTypes[$customTaxonomy->getName()][] = $customPostType->getName();
                $customTaxonomies[$customTaxonomy->getName()]        = $customTaxonomy;
            }
        }

        /** @var CustomTaxonomy $customTaxonomy */
        foreach ($customTaxonomies as $customTaxonomy) {
            register_taxonomy(
                $customTaxonomy->getName(),
                $taxonomiesToPostTypes[$customTaxonomy->getName()],
                [
                    'labels'            => $customTaxonomy->getLabels(),
                    'rewrite'           => ['slug' => $customTaxonomy->getSlug()],
                    'capabilities'      => [
                        'manage_terms' => 'manage_categories',
                        'edit_terms'   => 'manage_categories',
                        'delete_terms' => 'manage_categories',
                        'assign_terms' => 'edit_posts',
                    ],
                    'query_var'         => true,
                    'show_ui'           => true,
                    'show_admin_column' => true,
                    'meta_box_cb'       => $customTaxonomy->isMultiple() ? false : [$customTaxonomy, 'renderAdmin'],
                    'hierarchical'      => $customTaxonomy->isHierarchical(),
                ]
            );
        }
    }


    public function addCustomMetaBoxes()
    {
        if (!$this->getCustomPostTypes()) {
            return;
        }

        /** @var CustomPostType $customPostType */
        foreach ($this->getCustomPostTypes() as $customPostType) {
            /** @var CustomMetaBox $customMetaBox */
            foreach ($customPostType->getCustomMetaBoxes() as $customMetaBox) {
                add_meta_box(
                    $customMetaBox->getName(),
                    $customMetaBox->getTitle(),
                    [$customMetaBox, "generateMarkup"],
                    $customPostType->getName(),
                    $customMetaBox->getPosition(),
                    "high",
                    null
                );
            }
        }
    }

    public function customMetaSave($post_id, $post, $update)
    {
        // Check if not an autosave.
        if (wp_is_post_autosave($post_id)) {
            return 0;
        }

        // Check if not a revision.
        if (wp_is_post_revision($post_id)) {
            return 0;
        }

        if ($post->post_status == 'auto-draft' || $post->post_status == 'draft') {
            return 0;
        }

        /** @var CustomPostType $customPostType */
        foreach ($this->getCustomPostTypes() as $customPostType) {
            /** @var CustomMetaBox $customMetaBox */
            foreach ($customPostType->getCustomMetaBoxes() as $customMetaBox) {
                if (!isset($_POST['eventmeta_nonce_' . $customMetaBox->getName()])) {
                    continue;
                }
                if (!wp_verify_nonce($_POST['eventmeta_nonce_' . $customMetaBox->getName()], $customMetaBox->getName())) {
                    continue;
                }
                if (!current_user_can('edit_post', $post_id)) {
                    continue;
                }

                /** @var CustomMetaField $field */
                foreach ($customMetaBox->getFields() as $field) {
                    if (isset($_POST[$field->getName()])) {
                        $postValue = $_POST[$field->getName()];
                        if ($field->getType() == CustomMetaField::TYPE_DATE) {
                            $dateValue = \DateTime::createFromFormat('d-m-Y', $postValue);
                            $postValue = $dateValue !== false ? $dateValue->format('U') : '';
                        }
                        update_post_meta($post_id, $field->getName(), $postValue);
                    } else {
                        delete_post_meta($post_id, $field->getName());
                    }
                }
            }

            /** @var CustomTaxonomy $taxonomy */
            foreach ($customPostType->getCustomTaxonomies() as $taxonomy) {
                if ((isset($_POST[$taxonomy->getName()])) && ($_POST[$taxonomy->getName()] !== $taxonomy->getName() . '_nonce')) {
                    wp_set_object_terms($post_id, (int) $_POST[$taxonomy->getName()], $taxonomy->getName(), false);
                }
            }
        }

        return $post_id;
    }

    public function postStatusChanged($post_id, $post)
    {
        if ($post->post_status === 'publish') {
            $this->postSavedListener($post->ID, $post);
        }
    }

    /**
     * @param CustomPostType $customPostType
     * @param int            $pageSize
     * @param int            $page
     */
    public function queryCutsomPostType($args)
    {
        $posts = [];
        $query = new WP_Query($args);

        if ($query->have_posts()) {
            foreach ($query->get_posts() as $post) {
                $post->fields = [];

                // Initialize custom post fields
                /** @var CustomMetaBox $customMetaBox */
                foreach ($this->getCustomPostTypes()[$post->post_type]->getCustomMetaBoxes() as $customMetaBox) {
                    /** @var CustomMetaField $customField */
                    foreach ($customMetaBox->getFields() as $customField) {
                        // Populate the custom fields of this post type
                        $post->fields[$customField->getName()] = $this->getPostMeta($post->ID, $customField);
                    }
                }

                $post->thumbnail_url = get_the_post_thumbnail_url($post->ID, 'medium_large');

                // Initialize custom post taxonomies
                $post->taxonomies = [];
                /** @var CustomTaxonomy $customTaxonomy */
                foreach ($this->getCustomPostTypes()[$post->post_type]->getCustomTaxonomies() as $customTaxonomy) {
                    $post->taxonomies[$customTaxonomy->getName()] = wp_get_object_terms($post->ID, $customTaxonomy->getName(), ['orderby' => 'term_id', 'order' => 'ASC']);
                }

                // Set date format
                $post->formatted_date = date('d.m.Y', strtotime($post->post_date));
                $posts[] = $post;
            }
        }

        return $posts;
    }

    /**
     * @param                 $postId
     * @param CustomMetaField $field
     */
    private function getPostMeta($postId, $field)
    {
        $metaValue = get_post_meta($postId, $field->getName(), true);
        if ($metaValue) {
            if ($field->getType() === CustomMetaField::TYPE_DATE) {
                $dateValue = \DateTime::createFromFormat('U', $metaValue);
                if ($dateValue) {
                    return $dateValue->format('d-m-Y');
                }

                return $metaValue;
            }

            return $metaValue;
        }

        return false;
    }

    /**
     * @return array
     */
    static function getInstallSqlSequences()
    {
        return [];
    }

    /**
     * @return array
     */
    static function getUninstallSqlSequences()
    {
        return [];
    }

    abstract function getTextDomain();

    /**
     * Has to be an associative array with the key as the name of the post type!
     *
     * @return array
     */
    abstract function getCustomPostTypes();

    /**
     * @param $array
     * @param $key
     * @param $default
     *
     * @return mixed
     */
    static function getOrDefault($array, $key, $default)
    {
        return isset($array[$key]) ? $array[$key] : $default;
    }

    public function registerFrontendCall($action, $method)
    {
        add_action("wp_ajax_$action", [$this, $method]);
        add_action("wp_ajax_nopriv_$action", [$this, $method]);
    }

    protected function postSavedListener($post_id, $post)
    {
    }
}

