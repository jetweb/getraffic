<?php

add_action( 'wcdn_loop_content', 'bu_wcdn_mark_print', 20, 2 );
function bu_wcdn_mark_print($order, $template_type) {
	global $wpdb;
	$my_order_id = $order->id; //get_id();
	add_post_meta($my_order_id, 'already_printed', 'yes');
}

add_action('manage_shop_order_posts_custom_column', function($column) {
    global $post;
    if ('order_already_printed' === $column) {
        echo get_post_meta($post->ID, 'already_printed', true);
    }
}, 5);



add_filter( 'manage_edit-shop_order_columns', function ( $columns ) {
    $columns['order_already_printed'] = 'הודפס';
    return $columns;
});

function admin_inline_js(){
    ?>
    <script type='text/javascript'>
        jQuery(document).ready(function ($) {
            $('td.order_already_printed').each(function () {
                if ($(this).text() == 'yes') {
                    $(this).html('<span style="background:red;padding:5px;color:#000;">הודפס</span>').show();
                    $('#order_already_printed,.order_already_printed, .column-order_already_printed').show();
                    $(this).parent().find('.print-preview-button.delivery-note').css('opacity', '0.3');
                }
            });
            $('.print-preview-button.delivery-note').click(function () {
                $(this).css('opacity', '0.3');
            });

            $('.column-order_already_printed').show();
        });
    </script>
    <?php
}
add_action( 'admin_print_footer_scripts', 'admin_inline_js' );


add_action('restrict_manage_posts', function($post_type) {
    if ($post_type !== 'shop_order') {
        return;
    }
    ?>
    <select name="is_already_printed">
        <option value="">סנן לפי הזמנות שהודפסו</option>
        <option <?= (isset($_GET['is_already_printed']) && $_GET['is_already_printed'] === 'yes') ? 'selected="selected"' : ''; ?> value="yes">הודפס</option>
        <option <?= (isset($_GET['is_already_printed']) && $_GET['is_already_printed'] === 'no') ? 'selected="selected"' : ''; ?> value="no">לא הודפס</option>
    </select>
    <?php
});


add_action('pre_get_posts', function($query) {
    if (isset($_GET['is_already_printed'])) {
        if ($_GET['is_already_printed'] === 'yes') {
            $meta_query = array(
                array(
                    'key'=>'already_printed',
                    'value'=>'yes',
                    'compare'=>'=',
                ),
            );
            $query->set('meta_query',$meta_query);
        } else if ($_GET['is_already_printed'] === 'no') {
            $meta_query = array(
                array(
                    'key'=>'already_printed',
                    'compare'=>'NOT EXISTS',
                ),
            );
            $query->set('meta_query',$meta_query);
        }
    }


    return $query;
});
