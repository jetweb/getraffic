<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class CustomMetaQueryField
 *
 * @package WPHelper
 */
class CustomMetaQueryField
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var array
     */
    private $values = [];

    /**
     * @var array
     */
    private $visibleValues = [];

    /**
     * @param string $values
     *
     * @return CustomMetaQueryField
     */
    public function setValues($values)
    {
        $this->values = $values;

        return $this;
    }

    /**
     * @return string
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CustomMetaQueryField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param array $visibleValues
     *
     * @return CustomMetaQueryField
     */
    public function setVisibleValues($visibleValues)
    {
        $this->visibleValues = $visibleValues;

        return $this;
    }

    /**
     * @return array
     */
    public function getVisibleValues()
    {
        return $this->visibleValues;
    }
}
