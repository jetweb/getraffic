<?php

    foreach ($_POST as $key => $value) {
        if (strpos($key, 'shipping_description_') === 0) {
            //$exp = explode(':', substr($key, 21));
            update_option($key, $value);
        }
    }

?>


<form method="post">
    <?php
    $methods = WC()->shipping->get_shipping_methods();
    foreach (WC_Shipping_Zones::get_zones() as $zone) {
        foreach ($zone['shipping_methods'] as $method) {
            $keyName = 'shipping_description_'. $method->id . ':' . $method->instance_id;
            ?>
            <h3><?= $method->title; ?></h3>
            תיאור שיטת המשלוח : <input type="text" name="<?= $keyName ?>" value="<?= get_option($keyName) ?>"/>
            <?php
            //var_dump($method);
        }
    } ?>
    <br>

    <h3>איסוף עצמי</h3>
    <?php
        $keyNamePlus = 'shipping_description_local_pickup_plus';
    ?>
    תיאור שיטת המשלוח : <input type="text" name="<?= $keyNamePlus ?>" value="<?= get_option($keyNamePlus) ?>"/>
    <br><br>
    <button>שמירה</button>
</form>
