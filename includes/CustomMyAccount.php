<?php

namespace Getraffic;

class CustomMyAccount
{
    public $links = [
        'orders'       => [
            'title' => 'ההזמנות שלי',
        ],
        'edit-address' => [
            'title' => 'כתובת למשלוח / חיוב',
        ],
        'edit-account' => [
            'title' => 'פרטי התחברות',
        ],
        'mywishlist'   => [
            'title'    => 'wishlist',
            'template' => 'myaccount/mywishlist.php',
        ],
        'coupons'      => [
            'title'    => 'זיכויים / קופוני הנחה',
            'template' => 'myaccount/coupons.php',
        ],
        'points'       => [
            'title'    => 'נקודות / הטבות',
            'template' => 'myaccount/points.php',
        ],
        'returns'      => [
            'title'    => 'החזרות והחלפות',
            'template' => 'myaccount/returns.php',
        ],
    ];

    public function __construct()
    {
        $this->init();
        add_filter('woocommerce_account_menu_items', [$this, 'getMenuItems']);
    }

    public function getMenuItems() {
        $retVal = [];
        foreach ($this->links as $link => $data) {
            $retVal[$link] = $data['title'];
        }

        return $retVal;
    }

    public function init()
    {
        foreach ($this->links as $link => $data) {
            if (isset($data['template'])) {
                add_rewrite_endpoint($link, EP_PAGES);
                $template = $data['template'];
                add_action('woocommerce_account_' . $link . '_endpoint', function () use($template) {
                    wc_get_template($template);
                });
            }
        }


        add_filter("woocommerce_get_query_vars", function ($vars) {
            foreach ($this->links as $link => $data) {
                $vars[$link] = $link;
            }
            return $vars;
        });

        add_filter( 'body_class',function ( $classes ) {
            $add = $this->getEndpointName();

            if($add){
                $classes[] = $add;
            }

            return $classes;
        });
    }

    private function getEndpointName() {
        if(is_account_page() && !is_wc_endpoint_url()){
            return "gt_woo_ep_main";
        }else{
            if(is_wc_endpoint_url()){
                $WC_Query = new \WC_Query();
                return "gt_woo_ep_" . $WC_Query->get_current_endpoint();
            }else{
                return false;
            }
        }
    }

    public function setLinks($links)
    {
        $this->links = $links;
    }
}
