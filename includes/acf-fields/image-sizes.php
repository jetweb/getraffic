<?php
if (function_exists('acf_add_local_field_group')):
    acf_add_local_field_group([
        'key'                   => 'group_5d53f6b40bd14',
        'title'                 => 'גודל תמונות thumbnails',
        'fields'                => [
            [
                'key'               => 'field_5d53f6d8eeb3d',
                'label'             => 'גודל thumbnails',
                'name'              => 'thumbnail_sizes',
                'type'              => 'repeater',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => [
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ],
                'collapsed'         => '',
                'min'               => 0,
                'max'               => 0,
                'layout'            => 'table',
                'button_label'      => '',
                'sub_fields'        => [
                    [
                        'key'               => 'field_5d53f71deeb3e',
                        'label'             => 'size name',
                        'name'              => 'size_name',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5d53f768eeb3f',
                        'label'             => 'width',
                        'name'              => 'width',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5d53f76ceeb40',
                        'label'             => 'height',
                        'name'              => 'height',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => '',
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                ],
            ],
            [
                'key'               => 'field_5d53f7fddb313',
                'label'             => 'גודל תמונות קרוסלת מוצרים (carousel-thumb)',
                'name'              => 'carousel_thumb_size',
                'type'              => 'group',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => [
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ],
                'layout'            => 'table',
                'sub_fields'        => [
                    [
                        'key'               => 'field_5d53f81fdb314',
                        'label'             => 'width',
                        'name'              => 'width',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 280,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5d53f822db315',
                        'label'             => 'height',
                        'name'              => 'height',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 300,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                ],
            ],
            [
                'key'               => 'field_5d53f8e0db316',
                'label'             => 'גודל תמונות עמוד קטגוריה (product-thumb)',
                'name'              => 'product_thumb_size',
                'type'              => 'group',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => [
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ],
                'layout'            => 'table',
                'sub_fields'        => [
                    [
                        'key'               => 'field_5d53fafcb3d90',
                        'label'             => 'width',
                        'name'              => 'width',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 500,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5d53faffb3d91',
                        'label'             => 'height',
                        'name'              => 'height',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 500,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                ],
            ],
            [
                'key'               => 'field_5d53f947db319',
                'label'             => 'גודל תמונות גלריית מוצרים (gallery-thumb)',
                'name'              => 'gallery_thumb_size',
                'type'              => 'group',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => [
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ],
                'layout'            => 'table',
                'sub_fields'        => [
                    [
                        'key'               => 'field_5d53fae9b3d8e',
                        'label'             => 'width',
                        'name'              => 'width',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 270,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                    [
                        'key'               => 'field_5d53faedb3d8f',
                        'label'             => 'height',
                        'name'              => 'height',
                        'type'              => 'text',
                        'instructions'      => '',
                        'required'          => 0,
                        'conditional_logic' => 0,
                        'wrapper'           => [
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ],
                        'default_value'     => 270,
                        'placeholder'       => '',
                        'prepend'           => '',
                        'append'            => '',
                        'maxlength'         => '',
                    ],
                ],
            ],
        ],
        'location'              => [
            [
                [
                    'param'    => 'options_page',
                    'operator' => '==',
                    'value'    => 'theme-general-settings',
                ],
            ],
        ],
        'menu_order'            => 0,
        'position'              => 'normal',
        'style'                 => 'default',
        'label_placement'       => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen'        => '',
        'active'                => 1,
        'description'           => '',
    ]);
endif;