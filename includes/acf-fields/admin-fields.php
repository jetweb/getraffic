<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5cec1d87ad191',
	'title' => 'מוצרים',
	'fields' => array(
		array(
			'key' => 'field_5ce7277db58cd',
			'label' => 'מיקום במחסן',
			'name' => 'warehouse_location',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => 'מיקום המוצר במחסן',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'show_column' => 1,
			'show_column_sortable' => 1,
			'show_column_weight' => 1000,
			'allow_quickedit' => 1,
			'allow_bulkedit' => 1,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'product',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'seamless',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;