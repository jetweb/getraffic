<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class CustomPostType
 *
 * @package WPHelper
 */
class CustomPostType
{
    /**
     * @var array
     */
    private $labels = [];

    /**
     * @var string
     */
    private $slug = '';

    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $icon = '';

    /**
     * @var array
     */
    private $customTaxonomies = [];

    /**
     * @var array
     */
    private $customMetaBoxes = [];

    /**
     * @param array $labels
     *
     * @return CustomPostType
     */
    public function setLabels($labels)
    {
        $this->labels = $labels;

        return $this;
    }

    /**
     * @return array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param string $slug
     *
     * @return CustomPostType
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $name
     *
     * @return CustomPostType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param array $customTaxonomies
     *
     * @return CustomPostType
     */
    public function setCustomTaxonomies($customTaxonomies)
    {
        $this->customTaxonomies = $customTaxonomies;

        return $this;
    }

    /**
     * @return array
     */
    public function getCustomTaxonomies()
    {
        return $this->customTaxonomies;
    }

    /**
     * @param array $customMetaBoxes
     *
     * @return CustomPostType
     */
    public function setCustomMetaBoxes($customMetaBoxes)
    {
        $this->customMetaBoxes = $customMetaBoxes;

        return $this;
    }

    /**
     * @return array
     */
    public function getCustomMetaBoxes()
    {
        return $this->customMetaBoxes;
    }

    /**
     * @param string $icon
     *
     * @return CustomPostType
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}