<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class AjaxController
 *
 * @package WPHelper
 */
interface AjaxControllerInterface
{
    public function registerFrontendCall($action, $method);
}