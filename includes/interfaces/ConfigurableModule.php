<?php

namespace Getraffic;

interface ConfigurableModule
{
    public static function getSettingsPage();
}