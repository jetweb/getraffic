<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

use DateTime;

/**
 * Class CustomDateQuery
 *
 * @package WPHelper
 */
class CustomDateQuery
{
    /**
     * @var DateTime
     */
    private $fromDate;

    /**
     * @var DateTime
     */
    private $toDate;

    /**
     * @var bool
     */
    private $range = true;

    /**
     * @var bool
     */
    private $creationDate = true;

    /**
     * @var bool
     */
    private $initialized = false;

    /**
     * @var int
     */
    private $monthsAgo = 1;

    /**
     * @var string
     */
    private $metaKey = '';

    /**
     * @param DateTime $fromDate
     *
     * @return CustomDateQuery
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param DateTime $toDate
     *
     * @return CustomDateQuery
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @param boolean $range
     *
     * @return CustomDateQuery
     */
    public function setRange($range)
    {
        $this->range = $range;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isRange()
    {
        return $this->range;
    }

    /**
     * @param boolean $creationDate
     *
     * @return CustomDateQuery
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Whether the query is on the wordpress creation date, or a custom meta date field
     * @return boolean
     */
    public function isCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param int $monthsAgo
     *
     * @return CustomDateQuery
     */
    public function setMonthsAgo($monthsAgo)
    {
        $this->monthsAgo = $monthsAgo;

        return $this;
    }

    /**
     * @return int
     */
    public function getMonthsAgo()
    {
        return $this->monthsAgo;
    }

    /**
     * @return array
     */
    public function toWordpressDateQueryArray()
    {
        if ($this->isCreationDate())
        {
            if ($this->isRange()) {
                return [
                    'after'     => [
                        'day'   => $this->fromDate->format('d'),
                        'month' => $this->fromDate->format('m'),
                        'year'  => $this->fromDate->format('Y'),
                    ],
                    'before'    => [
                        'day'   => $this->toDate->format('d'),
                        'month' => $this->toDate->format('m'),
                        'year'  => $this->toDate->format('Y'),
                    ],
                    'inclusive' => true
                ];
            } else {
                return [
                    ['after' => "$this->monthsAgo months ago", 'inclusive' => true]
                ];
            }
        } else {
            if ($this->isRange()) {
                return [
                    'relation' => 'AND',
                    array(
                        'key'     => $this->getMetaKey(),
                        'value'   => $this->fromDate->setTime(0, 0, 0)->format('U'),
                        'compare' => '>='
                    ),
                    array(
                        'key'     => $this->getMetaKey(),
                        'value'   => $this->toDate->setTime(23, 59, 59)->format('U'),
                        'compare' => '<='
                    )];
            } else {
                return [
                    'relation' => 'AND',
                    array(
                        'key'     => $this->getMetaKey(),
                        'value'   => date('U', strtotime("+$this->monthsAgo months")),
                        'compare' => '<='
                    ),
                    array(
                        'key'     => $this->getMetaKey(),
                        'value'   => (new DateTime())->format('U'),
                        'compare' => '>='
                    )];
            }
        }
    }

    /**
     * @param boolean $initialized
     *
     * @return CustomDateQuery
     */
    public function setInitialized($initialized)
    {
        $this->initialized = $initialized;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isInitialized()
    {
        return $this->initialized;
    }

    /**
     * @param string $metaKey
     *
     * @return CustomDateQuery
     */
    public function setMetaKey($metaKey)
    {
        $this->metaKey = $metaKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }
}