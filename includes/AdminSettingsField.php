<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class AdminSettingsField
 *
 * @package WPHelper
 */
class AdminSettingsField
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $type = 'text';

    /**
     * @var string
     */
    private $markup = '';

    /**
     * @param string $name
     *
     * @return AdminSettingsField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $title
     *
     * @return AdminSettingsField
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $type
     *
     * @return AdminSettingsField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * @param string $makrup
     *
     * @return AdminSettingsField
     */
    public function setMarkup($markup)
    {
        $this->markup = $markup;

        return $this;
    }

    /**
     * @return string
     */
    public function getMarkup()
    {
        return $this->markup;
    }

    public function renderField($args)
    {
        $options = get_option($args['optionGroup']);
        if ($this->getType() == 'text') {
            printf('<input type="text" name="' . $args['optionGroup'] . '[' . $this->getName() . ']" value="%s" />',
                isset($options[$this->getName()]) ? $options[$this->getName()] : '');
        } else if ($this->getType() == 'textarea') {
            printf('<textarea name="' . $args['optionGroup'] . '[' . $this->getName() . ']" >%s</textarea>',
                isset($options[$this->getName()]) ? $options[$this->getName()] : '');
        } else if ($this->getType() == 'checkbox') {
            printf('<input type="checkbox" name="' . $args['optionGroup'] . '[' . $this->getName() . ']" %s />',
                isset($options[$this->getName()]) ? 'checked="checked"' : '');
        } else if ($this->getType() == 'custom') {
            echo $this->markup;
        }
    }
}