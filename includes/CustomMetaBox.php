<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class CustomMetaBox
 *
 * @package WPHelper
 */
class CustomMetaBox
{
    /**
     * @var string
     */
    private $name = '';

    /**
     * @var string
     */
    private $title = '';

    /**
     * @var string
     */
    private $type = '';

    /**
     * @var string
     */
    private $position = '';

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @param string $name
     *
     * @return CustomMetaBox
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $title
     *
     * @return CustomMetaBox
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $type
     *
     * @return CustomMetaBox
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $position
     *
     * @return CustomMetaBox
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param array $fields
     *
     * @return CustomMetaBox
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * This is called by Wordpress and will generate the markup automatically for this meta box.
     * @param $post
     */
    public function generateMarkup($post)
    {
        $values = get_post_custom($post->ID);
        ?><input type="hidden" name="eventmeta_nonce_<?= $this->getName() ?>" id="eventmeta_nonce_<?= $this->getName() ?>" value="<?php echo wp_create_nonce($this->getName()) ?>" /><?php

        /** @var CustomMetaField $field */
        foreach ($this->getFields() as $field) {
            switch ($field->getType()) {
                case CustomMetaField::TYPE_TEXT:
                    $text = isset($values[$field->getName()]) ? esc_attr($values[$field->getName()][0]) : '';
                    ?>
                    <p>
                        <label for="<?= $field->getName() ?>"><?= $field->getTitle() ?></label>
                        <input type="text" name="<?= $field->getName() ?>" id="<?= $field->getName() ?>" value="<?= $text; ?>"/>
                    </p>
                    <?php
                    break;
                case CustomMetaField::TYPE_CHECKBOX:
                    $value = isset($values[$field->getName()]) ? esc_attr($values[$field->getName()][0]) : '';
                    ?>
                    <label for="<?= $field->getName() ?>">
                        <input type="checkbox" name="<?= $field->getName() ?>" id="<?= $field->getName() ?>" <?= $value !== '' ? checked($value, 'on') : '' ?> />
                        <?= $field->getTitle() ?>
                    </label>
                    <?php
                    break;
                case CustomMetaField::TYPE_DROPDOWN:
                    $storedKey = isset($values[$field->getName()]) ? esc_attr($values[$field->getName()][0]) : '';
                    ?>
                    <p>
                        <label for="<?= $field->getName() ?>" class="prfx-row-title"><?= $field->getTitle() ?></label>
                        <select name="<?= $field->getName() ?>" id="<?= $field->getName() ?>">
                            <?php foreach ($field->getOptions() as $key => $value) { ?>
                            <option value="<?= $key ?>" <?= $storedKey !== '' ? selected($key, $storedKey) : '' ?>>
                                <?= $value ?>
                            </option>';
                            <?php } ?>
                        </select>
                    </p>
                    <?php
                    break;
                case CustomMetaField::TYPE_DATE:
                    $storedKey = isset($values[$field->getName()]) ? esc_attr($values[$field->getName()][0]) : '';
                    ?>
                    <p>
                        <label for="<?= $field->getName() ?>" class="prfx-row-title"><?= $field->getTitle() ?>
                            <input id="<?= $field->getName() ?>" name="<?= $field->getName() ?>" value="<?= \DateTime::createFromFormat('U', $storedKey) !== false ? \DateTime::createFromFormat('U', $storedKey)->format('d-m-Y') : '' ?>" class="admin-datepicker" />
                        </label>
                    </p>
                    <?php
                    break;
                default:
                    break;
            }
        }
    }
}