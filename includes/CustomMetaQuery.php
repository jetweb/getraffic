<?php
/**
 * Wp-Helper
 *
 * @author    Yaniv Tabibi <yan_tb@yahoo.com>
 * @copyright Copyright (c) 2017 Yaniv Tabibi (http://www.yanivt.com)
 */

namespace Getraffic;

/**
 * Class CustomMetaQuery
 *
 * @package WPHelper
 */
class CustomMetaQuery
{
    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var bool
     */
    private $isTaxonomy = true;

    /**
     * @var string
     */
    private $relation = 'OR';

    /**
     * @param array $fields
     *
     * @return CustomMetaQuery
     */
    public function setFields($fields)
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param $field
     */
    public function addField($name, $field)
    {
        $this->fields[$name] = $field;
    }

    public function toWordpressMetaQueryArray()
    {
        $retVal = [];

        if ($this->isTaxonomy()) {
            $retVal['relation'] = $this->relation;
            /** @var CustomMetaQueryField $field */
            foreach ($this->fields as $field) {
                $retVal[] = [
                    'taxonomy' => $field->getName(),
                    'field'    => 'term_id',
                    'terms'    =>  $field->getValues(),
                    'operator' => 'IN'
                ];
            }
        } else {

        }

        return $retVal;
    }

    /**
     * @param boolean $isTaxonomy
     *
     * @return CustomMetaQuery
     */
    public function setIsTaxonomy($isTaxonomy)
    {
        $this->isTaxonomy = $isTaxonomy;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTaxonomy()
    {
        return $this->isTaxonomy;
    }

    /**
     * @param string $relation
     *
     * @return CustomMetaQuery
     */
    public function setRelation($relation)
    {
        $this->relation = $relation;

        return $this;
    }

    /**
     * @return string
     */
    public function getRelation()
    {
        return $this->relation;
    }
}