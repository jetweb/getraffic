<?php
/**
 * Template Name: 404
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 container-404">
                <img class="image-404" src="<?php echo img('oooops.png'); ?>">
                <div class="text-404">
                    <p>ככה זה נראה כשמשהו משתבש.</p>
                    <p>אבל הנה הזדמנות להכיר את המוצרים הכי
                        נמכרים שלנו!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
//TODO: remove! use `renderCrossSellProducts` instead!
$posts = gt_get_field('homepage_best_sellers', 2);
$products = [];
foreach ($posts['best_sellers'] as $post) {
    $products[] = wc_get_product($post->ID);
}
?>
<div class="container xsell-container">
    <h1 class="best-match">BEST SELLER</h1>
    <div class="position-relative">
        <div class="xsell-products">
            <?php
            foreach ($products as $product) {
                echo '<div>';
                GT::renderProduct($product);
                echo '</div>';
            }
            ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
<script>
    jQuery(document).ready(function () {
        let $ = jQuery;
        initSlickCarousel($('.xsell-products'), false, true, 4, [
            {
                breakpoint: 2220,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1920,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 10,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]);
    });
</script>

<style>
    .text-404{
    margin-bottom: 0px;
    }
    .prod-thumb-bottom{
        left: -9999px;
    }
    
    h2.best-match {
        text-align: left;
        margin-bottom: 35px;
    }

    h1.best-match {
        text-align: left;
        font-size: 80px;
        font-weight: 300;
        margin-top: 35px;
    }

    .xsell-container {
        position: relative;
        margin: 80px auto;
    }

    .xsell-products {
        display: flex;
    }

    .xsell-products .thumb-wrapper {
        width: 350px;
        height: 350px;
    }

    .product-wrapper {
        display: inline-block;
        width: auto;
    }
    @media screen and (max-width: 768px) {
        .xsell-products .thumb-wrapper {
            width: 230px;
            height: 230px;
        }
    }
</style>