<?php
/**
 * Template Name: Checkout Page
 *
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */

if (strpos($_SERVER['REQUEST_URI'], 'order-received') !== false) {
    get_template_part('templates/checkout/thank-you');
    exit;
}

$wrapperElement = 'form';
if (strpos($_SERVER['REQUEST_URI'], 'order-pay') !== false) {
    $wrapperElement = 'div';
}

GT::enqueueAsset('checkout', '/assets/src/src/checkout.css', [], false, true);
?>

<div class="woocommerce">
    <<?=$wrapperElement?> name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row full-height">
                <div class="col-md-7 col-md-offset-1 checkout-wizard-container">
                    <?php if (have_posts()) {
                        while (have_posts()) : the_post();
                            the_content(); endwhile;
                    } ?>
                </div>
                <div class="col-md-4 checkout-cart-container">
                    <?php do_action('woocommerce_checkout_order_review'); ?>
                </div>
            </div>
        </div>
    </<?=$wrapperElement?>>
</div>

<?php
if (strpos($_SERVER['REQUEST_URI'], 'order-pay') === false) {
    \Getraffic\GTM::pushCheckoutStep(1);
}
?>
<?php get_template_part('templates/footer/ajax-url'); ?>
<?php wp_footer(); ?>

