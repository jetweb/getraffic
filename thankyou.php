<?php
/**
 * Template Name: Thank You
 *  *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>
<div class="container thankyou">
    <div class="col-lg-12 thankyou-lines">
        <h1>STAY IN TOUCH</h1>
        <p class="thankyou-image"><a href="#"><img src="<?php echo img('thankyou-arrows.png'); ?>"/></a></p>
        <p class="thankyou-toptext">!DONE</p>
        <p class="thankyou-text">תודה על הפנייה<br/>נחזור אלייך בהקדם</p>
        <p class="thankyou-link"><a href="#"><<< המשיכי ברכישה  </a></p>
    </div>
</div>
<?php get_footer(); ?>
