<?php
/**
 * Template Name: עמוד הצטרפות למודעון
 *
 * The home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package    WordPress
 * @subpackage Blank
 */
get_header();
?>

<?php get_template_part('templates/registration/join-us'); ?>

<?php get_footer(); ?>
