<?php
/**
 * Template Name: With Sidebar
 *
 * The template for displaying pages with the sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Blank
 */

get_header(); ?>

<div class="row">
    <div class="col-lg-9">
        <?php
        if ( have_posts() ) {
            while ( have_posts() ) : the_post();
                ?>
                <div class="blog-post">
                    <h2 class="blog-post-title"><?php the_title(); ?></h2>
                    <p class="blog-post-meta"><?php the_date(); ?> by <?php the_author(); ?></p>
                    <?php the_content(); ?>
                </div><!-- /.blog-post -->
                <?php
            endwhile;
        }
        ?>
    </div>

    <div class="col-lg-3">
        <?php get_sidebar() ?>
    </div>
</div>


<?php get_footer(); ?>
