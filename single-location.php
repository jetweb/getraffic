<?php
/**
 * Template Name: Single Location
 *
 * Template for displaying the location page
 *
 * @package    WordPress
 * @subpackage Blank
 */

get_header(); ?>
<div class="container">
    <div class="content">
        <div class="row">
            <div class="col-lg-12">
                <?php
                if (have_posts()) {
                    while (have_posts()) :
                        the_post();
                        $fields = $GT->queryCutsomPostType(['post_type' => 'location', 'p' => get_the_ID()])[0]->fields;
                    endwhile;
                }
                ?>
                    <h2 class="location-title"><?php echo $fields['location_desc']; ?> <?php the_title(); ?></h2>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
